<?php

if (!file_exists('includes/bootstrap.inc')) {
  if (!empty($_SERVER['DOCUMENT_ROOT']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/includes/bootstrap.inc')) {
    chdir($_SERVER['DOCUMENT_ROOT']);
  } elseif (preg_match('@^(.*)[\\\\/]sites[\\\\/][^\\\\/]+[\\\\/]modules[\\\\/]([^\\\\/]+[\\\\/])?elysia(_cron)?$@', getcwd(), $r) && file_exists($r[1] . '/includes/bootstrap.inc')) {
    chdir($r[1]);
  } else {
    die("Fatal Error: Can't locate bootstrap.inc.");
  }
}

include_once './includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$uri = $_SERVER['REQUEST_URI'];
$filters = $_GET['filters'];
$path_aux = 'search/apachesolr_search/';
$path_filtros = '';
$tids_rubro = array();
$tids_provincia = array();
$tids_marca_autos = array();
$tids_marca_motos = array();
$tids_marca_utilitarios = array();
$tids_marca_4x4 = array();
$taxonomias[34] = 'rubro';
$taxonomias[5] = 'provincia';
$taxonomias[1] = 'marca_autos';
$taxonomias[2] = 'marca_motos';
$taxonomias[3] = 'marca_utilitarios';
$taxonomias[4] = 'marca_4x4';
foreach ($taxonomias as $vid => $nombre) {
  $nombre = 'tids_'.$nombre;
  $cached = cache_get('taxonomy-tids-vid-'.$vid, 'cache_especial');
  if($cached && !empty($cached->data)) {
    $$nombre = $cached->data;
  } else {
    $tids = array();
    $tree = taxonomy_get_tree($vid);
    foreach ($tree as $term) {
      $tids[] = $term->tid;
    }
    $$nombre = $tids;
    cache_set('taxonomy-tids-vid-'.$vid, $$nombre, 'cache_especial', time() + 86400); // Un dia.      
  }
}
if(strpos($uri, '/search/apachesolr_search')!==FALSE) {
  $pattern = '/(^| |-)([^\s]*):("([^"]*)"|([^ ]*))/';
  preg_match_all($pattern, $filters, $matches);
  $filtros = $matches[0];
  foreach($filtros as $parte) {
    $filtro = explode(':', $parte);
    if(count($filtro)==2) {
      $param = trim($filtro[0]); // Puede comenzar con espacio vacio.
      $valor = str_replace('"', '', $filtro[1]);
      switch($param) {
        case 'uid':
          $path_filtros['f'][] = 'is_uid:'.$valor;
          break;
        case 'tid':
          if(in_array($valor, $tids_rubro)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_34:'.$valor;
          }
          if(in_array($valor, $tids_provincia)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_5:'.$valor;
          }
          if(in_array($valor, $tids_marca_autos)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_1:'.$valor;
          }
          if(in_array($valor, $tids_marca_motos)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_2:'.$valor;
          }
          if(in_array($valor, $tids_marca_utilitarios)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_3:'.$valor;
          }
          if(in_array($valor, $tids_marca_4x4)) {
            $path_filtros['f'][] = 'im_taxonomy_vid_4:'.$valor;
          }
          break;
        case 'ss_operacion':
          $path_filtros['f'][] = 'ss_operacion:'.$valor;
          break;
        case 'ss_rol':
          $path_filtros['f'][] = 'ss_rol:'.$valor;
          break;
        case 'ss_usado_v':
          $path_filtros['f'][] = 'ss_usado_v:'.$valor;
          break;
        case 'ss_combustible_v':
          $path_filtros['f'][] = 'ss_combustible_v:'.$valor;
          break;
        case 'ss_tipo_barrio':
          $path_filtros['f'][] = 'ss_tipo_barrio:'.$valor;
          break;
        case 'ss_cantidad_dormitorios':
          $path_filtros['f'][] = 'ss_cantidad_dormitorios:'.$valor;
          break;
      }
    }
  }
}
if(!empty($path_filtros)){
  foreach($path_filtros['f'] as $filtroKey => $filtroValor) {
    if($filtroKey == 0){
      $path_aux .= '?f['.$filtroKey.']='.$filtroValor;
    } else {
      $path_aux .= '&f['.$filtroKey.']='.$filtroValor;
    }
  }
}
$path = 'http://clasificados.lavoz.com.ar/'.$path_aux;
drupal_goto($path, NULL, NULL, 301);