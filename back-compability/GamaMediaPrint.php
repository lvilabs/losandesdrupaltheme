<?php

if (!file_exists('includes/bootstrap.inc')) {
  if (!empty($_SERVER['DOCUMENT_ROOT']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/includes/bootstrap.inc')) {
    chdir($_SERVER['DOCUMENT_ROOT']);
  } elseif (preg_match('@^(.*)[\\\\/]sites[\\\\/][^\\\\/]+[\\\\/]modules[\\\\/]([^\\\\/]+[\\\\/])?elysia(_cron)?$@', getcwd(), $r) && file_exists($r[1] . '/includes/bootstrap.inc')) {
    chdir($r[1]);
  } else {
    die("Fatal Error: Can't locate bootstrap.inc.");
  }
}

include_once './includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$anuncio_id_keys = array('anuncioId', 'anuncioid', 'Anuncioid', 'AnuncioId');
$anuncio_id = 0;
$nodo_nid = 0;
$node_path = '';

foreach ($anuncio_id_keys as $key) {
  $anuncio_id = isset($_GET[$key]) ? (integer) $_GET[$key] : 0;
  if($anuncio_id > 0) {
    break;
  }
}
$rubro_autos = clasificados_rubros_obtener_rubro_raiz_tipo_aviso('aviso_auto');
if($anuncio_id > 0) {
  $sql = "SELECT p.nid FROM {content_field_aviso_papel_id} p, {clvi_sitio_origen} s, {term_node} tn WHERE field_aviso_papel_id_value = %d AND p.nid=s.nid AND s.sitio_id = %d AND tn.nid = p.nid AND tn.tid = %d";
  $nodo_nid = (integer) db_result(db_query($sql, $anuncio_id, SITIO_LOCAL_ID, $rubro_autos->tid));
}

if($nodo_nid > 0) {
  $node_path = "http://clasificados.lavoz.com.ar/imprimir?nid=$nodo_nid";
} else {
  $node_path = url('<front>', array('absolute' => true, 'base_url' => 'http://clasificados.lavoz.com.ar'));
}
drupal_goto($node_path, NULL, NULL, 301);
