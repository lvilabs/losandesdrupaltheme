<?php

if (!file_exists('includes/bootstrap.inc')) {
  if (!empty($_SERVER['DOCUMENT_ROOT']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/includes/bootstrap.inc')) {
    chdir($_SERVER['DOCUMENT_ROOT']);
  } elseif (preg_match('@^(.*)[\\\\/]sites[\\\\/][^\\\\/]+[\\\\/]modules[\\\\/]([^\\\\/]+[\\\\/])?elysia(_cron)?$@', getcwd(), $r) && file_exists($r[1] . '/includes/bootstrap.inc')) {
    chdir($r[1]);
  } else {
    die("Fatal Error: Can't locate bootstrap.inc.");
  }
}

include_once './includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$tipo_id_keys = array('TipInm', 'tipinm', 'Tipnm', 'tipInm');
$tipo_id = 0;
$operacion_id_keys = array('TipOpe', 'tipope', 'Tipope', 'tipOpe');
$operacion_id = 0;
$dormitorios_id_keys = array('pEitCantDormitorios', 'peitcantdormitorios');
$dormitorios_id = 0;
$usuario_id_keys = array('UsrId', 'usrid', 'Usrid', 'usrId');
$usuario_id = 0;
$rubro = FALSE;
$operacion = FALSE;
$tipo_barrio = FALSE;
$dormitorios = FALSE;
$uid = FALSE;
$path = '';

foreach($tipo_id_keys as $key) {
  $tipo_id = isset($_GET[$key]) ? (integer) $_GET[$key] : 0;
  if($tipo_id > 0) {
    break;
  }
}
foreach($operacion_id_keys as $key) {
  $operacion_id = isset($_GET[$key]) ? (integer) $_GET[$key] : 0;
  if($operacion_id > 0) {
    break;
  }
}
foreach($dormitorios_id_keys as $key) {
  $dormitorios_id = isset($_GET[$key]) ? (integer) $_GET[$key] : 0;
  if($dormitorios_id > 0) {
    break;
  }
}
foreach($usuario_id_keys as $key) {
  $usuario_id = isset($_GET[$key]) ? $_GET[$key] : FALSE;
  if($usuario_id != FALSE) {
    break;
  }
}

switch($tipo_id) {
  case 87: // 'Casas'
    $rubro = 6331;
    break;
  case 88: // Country y Barrio Cerrado
    $tipo_barrio = 'Country';
    break;
  case 89: // 'Departamentos'
    $rubro = 6334;
    break;
  case 90: // '"Terrenos y Lotes"'
    $rubro = 6333;
    break;
  case 91: // '"Locales"'
    $rubro = 6335;
    break;
  case 92: //Veraneo
    break;
}
switch($operacion_id) {
  case 41:
    $operacion = 'Venta';
    break;
  case 42: // Country y Barrio Cerrado
    $operacion = 'Compra';
    break;
  case 43:
    $operacion = '"Alquileres"';
    break;
  case 44:
    $operacion = '"Alquileres Pedidos"';
    break;
  case 45:
    $operacion = '"Alquileres Temporarios"';
    break;
  case 54: //Tiempo Compartido
    $operacion = '"Tiempo Compartido"';
    break;
}
switch($dormitorios_id) {
  case 9:
    $dormitorios = '"1 Dormitorio"';
    break;
  case 10:
    $dormitorios = '"2 Dormitorios"';
    break;
  case 11:
    $dormitorios = '"3 Dormitorios"';
    break;
  case 12:
    $dormitorios = '"4 Dormitorios"';
    break;
  case 13:
    $dormitorios = '"Más de 4 Dormitorios"';
    break;
  case 147:
    $dormitorios = 'Monoambiente';
    break;
}
$path_aux = 'search/apachesolr_search/';
$path_filtros = array();
if($usuario_id!=FALSE) {
  if(is_numeric($usuario_id)) { // Es un cuit
    $uid = clasificados_get_uid_por_cuit($usuario_id);
  } else { // Es un nombre de usuario (email).
    $sql = "SELECT uid FROM {users} WHERE name = '%s'";
    $uid = (integer) db_result(db_query($sql, $usuario_id));
  }
}
if($rubro!=FALSE || $operacion!=FALSE || $tipo_barrio!=FALSE || $uid!=FALSE) {
  if($uid!=FALSE)
    $path_filtros['f'][] = 'is_uid:'.$uid;
}
if($rubro!=FALSE) {
  $path_filtros['f'][] = 'im_taxonomy_vid_34:6330';
  $path_filtros['f'][] = 'im_taxonomy_vid_34:'.$rubro;
  if($tipo_id==87 || $tipo_id==89) {
    if($dormitorios!=FALSE) {
      $path_filtros['f'][] = 'ss_cantidad_dormitorios:'.$dormitorios;
    }
  }
} else {
  $path_filtros['f'][] = 'im_taxonomy_vid_34:6330';
}
if($tipo_barrio!=FALSE) {
  $path_filtros['f'][] = 'ss_tipo_barrio:'.$tipo_barrio;
}
if($operacion!=FALSE) {
  $path_filtros['f'][] = 'ss_operacion:'.$operacion;
}
if(!empty($path_filtros)){
  foreach($path_filtros['f'] as $filtroKey => $filtroValor) {
    if($filtroKey == 0){
      $path_aux .= '?f['.$filtroKey.']='.$filtroValor;
    } else {
      $path_aux .= '&f['.$filtroKey.']='.$filtroValor;
    }
  }
}
$path = 'http://clasificados.lavoz.com.ar/'.$path_aux;
drupal_goto($path, NULL, NULL, 301);