//FAQ Behaviour
$(document).ready(function(){

  $('div.pregunta2 a').click(function(event){
    event.preventDefault();
    var div_pregunta = $(this).parent();
    var div_contenedor = div_pregunta.parent();
    var div_texto = div_pregunta.next();
    var change_alto = false;

    if(div_contenedor.hasClass('busqueda_left') || div_contenedor.hasClass('busqueda_right')) {
      change_alto = true;
    }

    div_texto.toggle();
    if(div_texto.css('display') != 'none') {
      div_pregunta.removeClass('closed').addClass('opened');
      if(change_alto) {
        div_contenedor.css('height', '197px');
      }
    } else {
      div_pregunta.removeClass('opened').addClass('closed');
      if(change_alto) {
        div_contenedor.css('height', '48px');
      }
    }
  });

  $('.busqueda_left').css('height', '48px');
  $('.busqueda_right').css('height', '48px');
  $('div.pregunta2').next().hide();
  
  //Ayuda de reposicionamientos
  var loc = document.location.href.split('#');
  if(loc[1] != undefined && loc[1] == 'reposicionamiento')
    $('div.pregunta2.opened').next().show();
  
});