
jQuery(document).ready(function($){

  if($('#edit-field-aviso-rango-cuotas-0-value').length <= 0) {
    return; //Si no encuentro el Textarea de Rango de Cuotas entonces este Script no hace nada.
    //Es decir todo lo programado queda anulado.
  }

  $('.quitar-rango-cuota').bind('click', WebPage.QuitarRangoCuota);

  $('.agregar-rango-cuota').bind('click', function(event) {
    event.preventDefault();
    var btn = $(this);
    var elCuotaDesde = $('#rango-desde-select');
    var elCuotaHasta = $('#rango-hasta-select');
    var elPrecio = $('#rango-precio-text');

    var cuotaDesde = parseInt(elCuotaDesde.val());
    var cuotaHasta = parseInt(elCuotaHasta.val());
    var precio = parseFloat(elPrecio.val());

    var valid = true;
    var errorMessage = '';
    if(isNaN(cuotaDesde) || cuotaDesde <= 0) {
      errorMessage = errorMessage + "Debe seleccionar la cuota \"desde\".\n";
      valid = false;
    }
    if(isNaN(cuotaHasta) || cuotaHasta <= 0) {
      errorMessage = errorMessage + "Debe seleccionar la cuota \"hasta\".\n";
      valid = false;
    }
    if(valid == true && cuotaDesde > cuotaHasta) {
      errorMessage = errorMessage + "La cuota \"desde\" debe ser inferior a la cuota \"hasta\".\n";
      valid = false;
    }
    if(isNaN(precio) || precio <= 0) {
      errorMessage = errorMessage + "El precio ingresado es incorrecto. Si desea utilizar decimales, utilice el punto (.) como separador decimal.\n";
      valid = false;
    }
    if(valid == false) {
      alert(errorMessage);
      return;
    }
    var newRango = WebPage.RangoCuotaMarkup(cuotaDesde, cuotaHasta, precio);
    var elNewRango = $(newRango);
    elNewRango.find('.quitar-rango-cuota').bind('click', WebPage.QuitarRangoCuota);
    $('#rangos-cuotas').append(elNewRango);

    elCuotaDesde.val('0');
    elCuotaHasta.val('0');
    elPrecio.val('');

  });

  $('#node-form').bind('submit', function (e) {
    //e.preventDefault();
    var txtRangoCuotas = $('#edit-field-aviso-rango-cuotas-0-value');
    txtRangoCuotas.val('');
    $('#rangos-cuotas').find('.rango-cuota').each(function(index) {
      if(index == 0) {
        return; //continue with next item
      }
      var item = $(this);
      var cuotaDesde = item.find('.rango-desde').text();
      var cuotaHasta = item.find('.rango-hasta').text();
      var precio     = item.find('.rango-precio').text();
      cuotaDesde = cuotaDesde.replace(item.find('.rango-desde span').text(), '');
      cuotaDesde = cuotaDesde.replace(' ', '');
      cuotaHasta = cuotaHasta.replace(item.find('.rango-hasta span').text(), '');
      cuotaHasta = cuotaHasta.replace(' ', '');
      precio = precio.replace(item.find('.rango-precio span').text(), '');
      precio = precio.replace(' ', '');
      txtRangoCuotas.val(txtRangoCuotas.val() + 'Cuota ' + cuotaDesde + ' a ' + cuotaHasta + ' $ ' + precio + "\n");
    });
  });
});

WebPage.RangoCuotaMarkup = function (cuotaDesde, cuotaHasta, precio) {
  var markup = [
  '<div class="rango-cuota draggable">',
  '  <div class="rango-desde"><span>Desde:</span> {CuotaDesde}</div>',
  '  <div class="rango-hasta"><span>Hasta:</span> {CuotaHasta}</div>',
  '  <div class="rango-precio"><span>$:</span> {Precio}</div>',
  '  <div class="rango-accion"><a href="#" class="quitar-rango-cuota">Quitar</a></div>',
  '</div>',
  '<div class="clearfix"></div>'
  ];

  var markupTxt = markup.join("\n");

  markupTxt = markupTxt.replace('{CuotaDesde}', cuotaDesde);
  markupTxt = markupTxt.replace('{CuotaHasta}', cuotaHasta);
  markupTxt = markupTxt.replace('{Precio}', precio);

  return markupTxt;
};

WebPage.QuitarRangoCuota = function(event) {
  event.preventDefault();
  var btn = $(this);
  var elRangoCuota = btn.parent().parent();
  elRangoCuota.next().remove();
  elRangoCuota.remove();
};