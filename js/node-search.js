function verMas(content, id, cantidad){
  // Mostrar link ver mas y ocultar filtro que superen la cantiada
  var li = content.find('li');
  var i = 1;
  for(i;li.size()>=i;i++) {
    if(i>=cantidad){
      li.eq(i).hide();
    }
  }
  var boton = '<a href="javascript:;" id="btnVermas'+id+'" class="apachesolr-showhide" rel="noindex" title="Ver mas filtros">Ver M&aacute;s</a><a href="javascript:;" id="btnReducido'+id+'" class="apachesolr-showhide" style="display:none;" rel="noindex" title="Reducir cantidad de filtros">Ver reducido</a>';
  content.append(boton);
  if($('#btnVermas'+id).length>0){
    $('#btnVermas'+id+':gt(0)').remove();
  }
  // Ocultar link ver mas y mostrar todos los filtros
  $('#btnVermas'+id).click(function(){
    var cont = cantidad;
    for(var i = 0; i<500;i++){
      if(id == 'btnVermasProvincia')
        content.find('li').eq(i).show();
      else
        content.find('li').eq(cont+i).show();
    }
    cont=cont+500;
    if(content.find('li').size()<=cont){
      $('#btnVermas'+id).hide();
      $('#btnReducido'+id).show();
    }
  });
  // Mostrar link ver mas y ocultar los filtros que superen la cantidad
  $('#btnReducido'+id).click(function(){
    for(var i = cantidad; i<500;i++){
      content.find('li').eq(i).hide();
    }
    $('#btnVermas'+id).show();
    $('#btnReducido'+id).hide();
  });
}

telefonoFormValidate = function(phone){
  var valid = false;
  //var matches = array();
  var numbers = phone.match(/\d/g);
  if(!numbers){
    valid = true;
  } else {
    if(numbers.length < 4) {
      //no es valido porque el número es muy corto (no tiene la cantidad necesaria de numeros)
    } else if(phone.match(/^[0-9 \-\(\)\.\+\/\*#]+$/)) {
      if(phone.match(/[\-\(\)\.\+\/\*#]{3,}/)) {
        //no es valido porque tiene más de 3 caracteres especiales juntos.
      } else {
        var max_allowed = numbers - 2;
        valid = true;
        /*for(i=0; i<10; i++) {
          if(substr_count(phone, "{i}") > max_allowed) {
            valid = false;
            //no es valido porque repitio el mismo número varias veces. (numbers-1 es el mismo numero)
            break;
          }
        }*/
      }
    }
  }
  return valid;
};
mailFormValidate = function(mail){
  var valid = false;
  if(mail.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/))
    valid = true;
  return valid;
};
ajaxEnvioContacto = function () {
  
  var buscador = '';
  if($("#edit-contactar-vendedor-buscador").length > 0)
    buscador = $("#edit-contactar-vendedor-buscador").val();
  
  var datos_post = {
    contactar_vendedor_nombre: $("#edit-contactar-vendedor-nombre").val(),
    contactar_vendedor_telefono: $("#edit-contactar-vendedor-telefono").val(),
    contactar_vendedor_mail: $("#edit-contactar-vendedor-mail").val(),
    contactar_vendedor_consulta: $("#edit-contactar-vendedor-consulta").val(),
    contactar_vendedor_aviso: $("#edit-contactar-vendedor-aviso").val(),
    contactar_vendedor_estado: $("#edit-contactar-vendedor-estado").val(),
    contactar_vendedor_buscador: buscador,
    contactar_vendedor_name: ''
  }
  if($("#contactar-vendedor-formulario #edit-captcha-sid").length>0) {
    datos_post.captcha_sid = $("#contactar-vendedor-formulario #edit-captcha-sid").val();
    datos_post.captcha_response = $("#contactar-vendedor-formulario #edit-captcha-response").val();
    datos_post.captcha_token = $("#contactar-vendedor-formulario #edit-captcha-token").val();
  }
  if($('#contactar-vendedor-formulario .g-recaptcha').length>0) {
    datos_post['g-recaptcha-response'] = $('#g-recaptcha-response').val();
  }
  $("#contactar-vendedor-formulario input").removeClass('error');
  $("#contactar-vendedor-formulario textarea").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").html('');
  
  $.ajax({
      type: "POST",
      data: datos_post,
      dataType: "json",
      cache: false,
      url: "/ajax/carga-formulario_contacto/submit",
      success: function (data) {
        var error = false;
        if(data.error) {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
          error = true;
        } else {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
          // Evento contacto ga
          send_event_ga('contacto', 'resultado', 'desktop');
        }
        $('#contactar-vendedor-formulario .messages').show('');
        $("#edit-contactar-vendedor-submit").attr("value", "Consultar");
        $("#edit-contactar-vendedor-submit").removeAttr('disabled');
        if(error == false)
          setTimeout(colorboxClose, 3000);
      },
      error: function (e, t, n) {
        if (t === "timeout") {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
        } else {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
        }
        $('#contactar-vendedor-formulario .messages').show('');
        $("#edit-contactar-vendedor-submit").attr("value", "Consultar");
        $("#edit-contactar-vendedor-submit").removeAttr('disabled');
      }
  })
};
colorboxClose = function () {
  $('#contactar-vendedor-formulario .messages').hide('');
  $.colorbox.close()
}

jQuery(document).ready(function($){
  
  $('#orden ul li').each(function(){
    var orden_li = $(this);
    var orden_a = orden_li.find('a');
    if(orden_a.html()=='KM')
        $('<li class="combustible" style="width: 100px;">Combustible</li>').insertAfter(orden_li);
    //Elimino el orden por espacio
    if(orden_a.html() == 'Espacio'){
      orden_li.remove();
    } else {
      //Elimino el orden por orden inicial
      if(orden_a.html() == 'Orden Inicial'){
        orden_li.remove();
      } else {
        //Elimino el orden por orden inicial alojamientos
        if(orden_a.html() == 'Orden Inicial Alojamientos'){
          orden_li.remove();
        } else {
          //Agrego flechas a los ordenes
          if(orden_a.attr('class') != 'active') {
            var server = $("link[rel^='dns-prefetch']").attr('href'); // Tomamos el valor de static
            if(server == undefined)
              server = '';
            else
              server = 'http:'+server;
            $('<img src="'+server+'/misc/arrow-desc.png" alt="ordenar" title="orden descendente" width="13" height="13">').insertBefore(orden_a);
          }
          //Reordeno los filtros
          if(orden_a.html() == 'AÑO'){
            $('#orden ul').prepend('<li>'+orden_li.html()+'</li>');
            orden_li.remove();
          }
        }
      }
    }
  });

  //Ocultar secciones del paginador
  if($("li.pager-first"))
    $("li.pager-first").hide("");
  if($("li.pager-last"))
    $("li.pager-last").hide("");

  //Buscador por palabras en el Search
  var el_buscador_text = $('#texto_busqueda');
  var buscador_empty_text = 'Buscar por palabra';
  if(el_buscador_text.val() == '') {
    el_buscador_text.val(buscador_empty_text);
  }
  el_buscador_text.focus(function() {
    if(el_buscador_text.val() == buscador_empty_text) {
      el_buscador_text.css('color', 'black');
      el_buscador_text.val('');
    }
  });
  el_buscador_text.blur(function() {
    if(el_buscador_text.val() == '') {
      el_buscador_text.css('color', 'gray');
      el_buscador_text.val(buscador_empty_text);
    }
  });

  $("#search-block-form").submit(function(event) {
    if(el_buscador_text.val() == buscador_empty_text) {
      el_buscador_text.val('');
    }
  });

  slider_km = $("#slider-km");
  if(slider_km.length != 0) { // Existe el div id=slider-km
    var km_max = $("#edit-km-max").val()-0;
    if(!($("#edit-km-desde").val()))
      $("#edit-km-desde").val(0);
    if(!($("#edit-km-hasta").val()))
      $("#edit-km-hasta").val(km_max);
    $("#edit-km-desde").attr('readonly', 'readonly');
    $("#edit-km-hasta").attr('readonly', 'readonly');
    $("#edit-km-desde").css('border', 'none');
    $("#edit-km-hasta").css('border', 'none');
    slider_km.slider({
      range: true, // necessary for creating a range slider
      min: 0, // minimum range of slider
      max: km_max, //maximimum range of slider
      step: 5000,
      values: [($("#edit-km-desde").val()?$("#edit-km-desde").val():0), ($("#edit-km-hasta").val()?$("#edit-km-hasta").val():km_max)], //initial range of slider
      slide: function(event, ui) { // This event is triggered on every mouse move during slide.
        $("#edit-km-desde").val(ui.values[0]);
        $("#edit-km-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){//This event is triggered when the user stops sliding.
        $("#edit-km-desde").val(ui.values[0]);
        $("#edit-km-hasta").val(ui.values[1]);
      }
    });
  }
  slider_anio = $("#slider-anio");
  if(slider_anio.length != 0) { // Existe el div id=slider-anio
    var anio_min = $("#edit-anio-min").val()-0;
    var anio_max = (new Date).getFullYear();
    if(isNaN($("#edit-anio-desde").val()) || !$("#edit-anio-desde").val())
      $("#edit-anio-desde").val(anio_min);
    if(isNaN($("#edit-anio-hasta").val()) || !$("#edit-anio-hasta").val())
      $("#edit-anio-hasta").val(anio_max);
    $("#edit-anio-desde").attr('readonly', 'readonly');
    $("#edit-anio-hasta").attr('readonly', 'readonly');
    $("#edit-anio-desde").css('border', 'none');
    $("#edit-anio-hasta").css('border', 'none');
    slider_anio.slider({
      range: true,
      min: anio_min,
      max: anio_max,
      step: 1,
      values: [($("#edit-anio-desde").val()?$("#edit-anio-desde").val():anio_min), ($("#edit-anio-hasta").val()?$("#edit-anio-hasta").val():anio_max)],
      slide: function(event, ui) {
        $("#edit-anio-desde").val(ui.values[0]);
        $("#edit-anio-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){
        $("#edit-anio-desde").val(ui.values[0]);
        $("#edit-anio-hasta").val(ui.values[1]);
      }
    });
  }
  /*slider_precio = $("#slider-precio");
  if(slider_precio.length != 0) { // Existe el div id=slider-precio
    var precio_max = $("#edit-precio-max").val()-0;
    if(isNaN($("#edit-precio-desde").val())  || !$("#edit-precio-desde").val())
      $("#edit-precio-desde").val(0);
    if(isNaN($("#edit-precio-hasta").val()) || !$("#edit-precio-hasta").val())
      $("#edit-precio-hasta").val(precio_max);
    $("#edit-precio-desde").attr('readonly', 'readonly');
    $("#edit-precio-hasta").attr('readonly', 'readonly');
    $("#edit-precio-desde").css('border', 'none');
    $("#edit-precio-hasta").css('border', 'none');
    slider_precio.slider({
      range: true,
      min: 0,
      max: precio_max,
      step: 500,
      values: [($("#edit-precio-desde").val()?$("#edit-precio-desde").val():0), ($("#edit-precio-hasta").val()?$("#edit-precio-hasta").val():precio_max)],
      slide: function(event, ui) {
        $("#edit-precio-desde").val(ui.values[0]);
        $("#edit-precio-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){
        $("#edit-precio-desde").val(ui.values[0]);
        $("#edit-precio-hasta").val(ui.values[1]);
      }
    });
  }*/
  slider_sup = $("#slider-sup");
  if(slider_sup.length != 0) { // Existe el div id=slider-sup
    var sup_max = $("#edit-sup-max").val()-0;
    if(!($("#edit-sup-desde").val()))
      $("#edit-sup-desde").val(0);
    if(!($("#edit-sup-hasta").val()))
      $("#edit-sup-hasta").val(sup_max);
    $("#edit-sup-desde").attr('readonly', 'readonly');
    $("#edit-sup-hasta").attr('readonly', 'readonly');
    $("#edit-sup-desde").css('border', 'none');
    $("#edit-sup-hasta").css('border', 'none');
    slider_sup.slider({
      range: true, // necessary for creating a range slider
      min: 0, // minimum range of slider
      max: sup_max, //maximimum range of slider
      step: 100,
      values: [($("#edit-sup-desde").val()?$("#edit-sup-desde").val():0), ($("#edit-sup-hasta").val()?$("#edit-sup-hasta").val():sup_max)], //initial range of slider
      slide: function(event, ui) { // This event is triggered on every mouse move during slide.
        $("#edit-sup-desde").val(ui.values[0]);
        $("#edit-sup-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){//This event is triggered when the user stops sliding.
        $("#edit-sup-desde").val(ui.values[0]);
        $("#edit-sup-hasta").val(ui.values[1]);
      }
    });
  }
  
  //Ver mas filtro Provincia
  if($('.facetapi-facet-im_taxonomy_vid_5').find('li').size()>10){
    verMas($('.facetapi-facet-im_taxonomy_vid_5'), 'btnVermasProvincia', 10);
  }
  //Ver mas filtro Rubro
  if($('.facetapi-facet-im_taxonomy_vid_34').find('li').size()>10){
    verMas($('.facetapi-facet-im_taxonomy_vid_34'), 'Rubro', 10);
    // Ocultamos links mal indexados
    /*var rubrosLi = $('#block-apachesolr_search-im_vid_34 > .content > .item-list > ul > li');
    var i = 1;
    for(i;rubrosLi.size()>=i;i++) {
      rubrosLi.eq(i).hide();
    }*/
  }
  //Ver mas filtro Tienda
  if($('.facetapi-facet-ss_nombre_tienda').find('li').size()>10){
    verMas($('.facetapi-facet-ss_nombre_tienda'), 'Tienda', 10);
  }
  //Ver mas filtro Talle
  if($('.facetapi-facet-sm_talle').find('li').size()>5){
    verMas($('.facetapi-facet-sm_talle'), 'Talle', 5);
  }
  
  // Si filtro Provincia no esta aplicado y estamos en producto, mostramos solo Cordoba.
  if((window.location.href.indexOf("im_taxonomy_vid_34:6106") != -1 || window.location.href.indexOf("im_taxonomy_vid_34%3A6106") != -1) &&  $('.facetapi-facet-im_taxonomy_vid_5 .expanded').length==0){
    var provinciasLi = $('.facetapi-facet-im_taxonomy_vid_5 li');
    var i = 0;
    if(provinciasLi.length < 10){
      verMas($('.facetapi-facet-im_taxonomy_vid_5'), 'btnVermasProvincia', 10);
    }
    for(i;provinciasLi.length>i;i++) {
      var provinciaTexto = provinciasLi[i].outerHTML;
      if(provinciaTexto.indexOf("Córdoba") == -1)
        provinciasLi.eq(i).hide();
    } 
  }
  
  // Por un problema de estilos, eliminamos el mensaje de cantidad de avisos encontrados del footer
  // si no se muestra el paginador
  if($(".paginado.cards").length > 0){
    if($(".paginado .item-list").length == 0)
      $(".paginado .Gris").html("");
  }
  
  //Quitar filtro valoracion
  $('.facetapi-facet-im_valoracion .facetapi-checkbox').click(function(){
    if($(this).attr('checked') == false) {
      window.location.href = $(this).siblings('a').attr('href');
    }
  });
  
  // Quitar filtro alquileres temporarios al aplicar filtro de casas o deptos
  if($('#block-clasificados_apachesolr-link_alojamientos').length > 0) {
    if($('.facetapi-facet-ss_operacion').length > 0) {
      $('.facetapi-facet-ss_operacion li a').each(function(index) {
        if($(this).text().indexOf('Alquileres Temporarios') > -1)
          $(this).hide('');
      });
    }
  }
  
  // Quitar filtro casas y dptos al aplicar filtro alquiler temporario
  if($('.facetapi-facet-ss_operacion').length > 0) {
    if($('.facetapi-facet-ss_operacion li').length == 1 && $('.facetapi-facet-ss_operacion li').html().indexOf('Alquileres Temporarios') > -1) {
      if($('.facetapi-facet-im_taxonomy_vid_34 ul.expanded').length > 0) {
        $('.facetapi-facet-im_taxonomy_vid_34 ul.expanded li a').each(function(index) {
          if($(this).text().indexOf('Casas') > -1 || $(this).text().indexOf('Departamentos') > -1)
            $(this).hide('');
        });
      }
    }
  }
  
  // Mostramos modal bienvenida alojamientos
  var url = window.location.href;
  var hash = url.substring(url.indexOf("#")+1);
  if(hash == 'modal_bienvenida') {
    $.colorbox({
      href:'#modal_bienvenida_buscador',
      inline: true,
      width: "50%"
    });
  }
  
  // Contactar
  $('.link-search-contacto').click(function(){
    var alto_modal = 400;
    if($('.g-recaptcha').length > 0)
      alto_modal = 470;
    $('#edit-contactar-vendedor-aviso').val($(this).attr('data-nid'));
    $('#contacto-titulo').html($(this).attr('data-title'));
    $('#contactar-vendedor-formulario .messages').hide('');
    $.colorbox({
      href:'#modal-contactar-buscador',
      inline: true,
      width: 340,
      height: alto_modal
    });
  });
  $('#contactar-vendedor-formulario').submit(function(event) {
    event.preventDefault();
    //Validacion de campos
    var envio = true;
    var mensaje = '';
    if ($("#edit-contactar-vendedor-nombre").val() == ""){
      $("#edit-contactar-vendedor-nombre").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if($("#edit-contactar-vendedor-telefono").val() == "" && $("#edit-contactar-vendedor-mail").val() == ""){
      $("#edit-contactar-vendedor-telefono").addClass('error');
      $("#edit-contactar-vendedor-mail").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    } else {
      if($("#edit-contactar-vendedor-telefono").val() != "") {
        if(telefonoFormValidate($("#edit-contactar-vendedor-telefono").val()) === false){
          $("#edit-contactar-vendedor-telefono").addClass('error');
          mensaje = 'Campo no válido';
          envio = false;
        }
      }
      if($("#edit-contactar-vendedor-mail").val() != "") {
        if(mailFormValidate($("#edit-contactar-vendedor-mail").val()) === false){
          $("#edit-contactar-vendedor-mail").addClass('error');
          mensaje = 'Campo no válido';
          envio = false;
        }
      }
    }
    if($("#edit-contactar-vendedor-consulta").val() == "") {
      $("#edit-contactar-vendedor-consulta").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if($("#edit-captcha-response").length>0 && $("#edit-captcha-response").val() == "") {
      $("#contactar-vendedor-formulario input[name=captcha_response]").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if(envio == false) {
      return false;
    } else {
      $("#edit-contactar-vendedor-submit").attr("value", "Enviando...");
      $("#edit-contactar-vendedor-submit").attr('disabled', 'disabled');
      ajaxEnvioContacto();
    }
  });
  
  // Agregamos clase a banners middle de productos
  if($('.CajaAviso').length >= 1) {
    $('.CajaAviso').siblings('.banner').addClass('resultProducto');
  }
  
});
