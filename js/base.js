/**
* Base js.
*/
AUTOSLAVOZ_PROVINCIA_CBA_TAXONOMY_TID = 3173;
AUTOSLAVOZ_CIUDAD_CBA_TAXONOMY_TID = 3194;

AUTOSLAVOZ_PROVINCIA_MENDOZA_TAXONOMY_TID = 3182;
AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID = 3729;
AUTOSLAVOZ_ESPACIO_GRATUITO = 5;

AUTOSLAVOZ_CREDITO_RECUADRO_COLOR = 1011;
AUTOSLAVOZ_CREDITO_12_FOTOS = 3506662;
AUTOSLAVOZ_CREDITO_FOTO_LISTADO = 3506667;

AUTOSLAVOZ_CREDITO_PAPELWEB_NID = 7597;
AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID = 1029997;
AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID = 1030068;
AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID = 2387500;
AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID = 2442744;
CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID = 38;
CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID = 40;

TWITTER_API_URL = "http://cdn.api.twitter.com/1/urls/count.json",
TWEET_URL = "https://twitter.com/intent/tweet";
    
jQuery(function(){
  // bind change event to select
  $('#form-comercios').submit(function(event) {
    event.preventDefault();
    var url = $('#select-comercio').val(); // get selected option value
    if (url) { // require url to have value
      window.location = url; // open url
    }
  });
});

function ocultar_alerta(){
  jQuery('#alerta').hide();
}

function get_cookie(nombre) { 
  var re=new RegExp(nombre+"=[^;]+", "i"); //construct RE to search for target name/value pair
  if (document.cookie.match(re)) //if cookie found
    return document.cookie.match(re)[0].split("=")[1] //return its value
  return false;
}

function format_number(number){
  var num = number.replace(/\./g,'');
  num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
  num = num.split('').reverse().join('').replace(/^[\.]/,'');
  return num;
}

function cxense_add_custom_parameters(group, value) {
  var cX = cX || {}; cX.callQueue = cX.callQueue || [];
  cX.callQueue.push(['setSiteId', '9222331896993589875']);
  cX.callQueue.push(['setCustomParameters', { 'estadisticas': true }]);
  cX.callQueue.push(['sendPageViewEvent', { useAutoRefreshCheck: false }]);
  //cX.callQueue.push(['sendPageViewEvent', { 'location': 'http://clasificados.lavoz.com.ar/administrar/estadisticas_globales' }]);
  //(function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
  //e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
  //t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');

}

/* Paginas vistas cxense */
function cxense_send_pageview_event(path) {
  cX.callQueue.push(['setSiteId', '9222331896993589875']);
  cX.callQueue.push(['sendPageViewEvent', { 'location': path }]);
}

/* Eventos cxense */
function send_event_cx(type, customParameters, origin) {
  //console.log(type + ', ' + customParameters + ', ' + origin);
  customParameters = customParameters || {};
  origin = origin || 'gcl-website';
  if (typeof cX !== 'undefined') {
    cX.callQueue.push(['setEventAttributes', {
      origin: origin,
      persistedQueryId: '652c9e049df18eee90bbad0955aefd2c0feb9082'
    }]);
    cX.callQueue.push(['sendEvent', type, customParameters]);
  }
}

/* Eventos Analytics */
function send_event_ga(category, action, label) {
  category = category || 'evento_sin_categoria';
  action = action || 'evento_sin_accion';
  label = label || '';
  if (typeof(ga) !== 'undefined') {
    ga('REDLaVoz.send', 'event', category, action, label);
  }
}

$(document).ready(function(){

  if(typeof($.colorbox) != 'undefined')
    $.extend($.colorbox.settings, Drupal.settings.colorbox);

  var share_aviso = $('#share-aviso');
  
  if(share_aviso.length > 0) {
    $.colorbox({
      href:'#share-aviso',
      inline: true,
      width: "25%"
    });
  } else {
    var sysmsgs = $('#system-messages div.messages');
    if(sysmsgs.length > 0) {
      var sysmsgsinline = $(".system-messages-inline");
      sysmsgsinline.colorbox({
        inline: true,
        width: "25%"
      });
      sysmsgsinline.click();
    }
  }
  // Nuevo Colorbox
  if(window.location.hash == '#modal-publicacion'){
    $.colorbox({
      href:'#modal-publicacion-rubros',
      inline: true,
      width: 700,
    });
  }
  //Mostrar modal login
  $('.ingresar-btn').click(function(){
    $.colorbox({
      href:'#block-user-0',
      inline: true,
      width: "30%"
    });
  });
  //Mostrar modal publicacion de avisos
  $('.publicar-btn').click(function(){
    $.colorbox({
      href:'#modal-publicacion-rubros',
      inline: true,
      width: "700px"
    });
  });
  
  //Paginador de destacados bloque derecho
  if($("#destacados-block").html() != null){
    $(".pager").hide("");
    WebPage.RecargarPremiumsBlockTimerId = setTimeout("WebPage.CargarPremiumsBlock()", WebPage.TiempoRecargaPremiumsBlock);
  }
  
  //Quitar visitas del aviso legal
  $('.page-legales ul.links').remove();

  //Iniciar buscador del header
  WebPage.ConfigurarBuscadorEnHeader();
  
  if(Drupal.settings.mostrar_modal_publicacion_rubros==true)
    $('li.publicar a').click();
    
  //Cookie redireccionar a version mobil
  var clasificadoslavozmovil= document.cookie.match ( '(^|;) ?clasificadoslavozmovil=([^;]*)(;|$)' );
	if(clasificadoslavozmovil != null) {
		jQuery('#alerta').show();
		jQuery('#alerta #warningbrowser').html("<a href='/servicio_movil/unset_cookie'><b>IR A VERSIÓN MOVIL</b></a>");
	}

  var clasificadoslavozlogin= document.cookie.match ( '(^|;) ?login=([^;]*)(;|$)' );
	if(clasificadoslavozlogin != null) {
    $.post('/clasificados_content/login', { version: 5}, function(responseText) {
        if(responseText=='false') {
          $(".SinLoguear .Top ul .ingresar").show();
          $(".SinLoguear .Top ul .Registrarse").show();
          $(".SinLoguear .Top ul .ContentLogin").show();
        }
        else {
          if($(".SinLoguear .Top ul .region-login-superior").length > 0) {
            $(".SinLoguear .Top ul .region-login-superior").html(responseText);
          } else {
            $('<div class="region region-login-superior"></div>').insertAfter($('.ContentLogin'));
            $(".SinLoguear .Top ul .region-login-superior").html(responseText);
          }
          $(".SinLoguear .Top ul .region-login-superior").show();
        }
      },
      "html"
    );
  } else {
    $(".SinLoguear .Top ul .ingresar").show();
    $(".SinLoguear .Top ul .Registrarse").show();
    $(".SinLoguear .Top ul .ContentLogin").show();
  }
  
  //Loguin con redireccion a publicacion de aviso
  $('#publicacion-login-user').click(function(event) {
    var url_vars = window.location.pathname.split("/");
    var url_login = window.location.pathname + '?destination=node/' + url_vars[2] + '/publicacion?share=1';
    $("#edit-name").val($("#user-login-mail").val());
    $('#user-login-form').attr('action', url_login);
  });
  
  $(".facebook-share").each(function() {
    var elem = $(this);
    url = encodeURIComponent(elem.attr("data-url") || document.location.href);
    
    // Get count and set it as the inner HTML of .count
    $.getJSON("http://graph.facebook.com/?id=" + url, function(data) {
      if(data.shares!=undefined)
        elem.find(".count").html(data.shares);
      else
        elem.find(".count").html(0);
    });
  });
  // Arma la url para hacer un tweet.
  $(".tweet").each(function() {
    var elem = $(this),
    // Use current page URL as default link 
    url = encodeURIComponent(elem.attr("data-url")|| $("link[rel='canonical']").attr("href") || document.location.href),
    // Use page title as default tweet message
    text = elem.attr("data-text") || document.title,
    via = elem.attr("data-via") || "";
    //related = encodeURIComponent(elem.attr("data-related")) || "",
    //hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";
    
    // Set href to tweet page
    elem.attr({
        href: TWEET_URL + "?original_referer=" +
                encodeURIComponent(document.location.href) +
                "&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via,
        target: "_blank"
    });
  });
  
  //Tooltips
  if($(".tooltip").length > 0){
    Opentip.styles.myErrorStyle = {
      extends: "dark",
    };
    if($(".tooltip.btn-rep").length>0)
      new Opentip(".tooltip.btn-rep", "Botón para reposicionamiento masivo de todos los avisos con destaques. La cantidad de resposicionamiento diario dependerá de cuántos créditos tengas contratados.", { style: "myErrorStyle" });
    if($(".tooltip.msg-rep-res").length>0)
      new Opentip(".tooltip.msg-rep-res", "Botón para reposicionamiento masivo de todos los avisos con destaques. La cantidad de resposicionamiento diario dependerá de cuántos créditos tengas contratados.", { style: "myErrorStyle" });
    $(".tooltip.link-rep").each(function( index ) {
      if($(this).hasClass("deshabilitado")) {
        new Opentip(".tooltip.link-rep.msg-" + index + '"', "Este aviso ya fue reposicionado hoy. Podrás volver a reposicionarlo el día de mañana.", { style: "myErrorStyle" });
      } else {
        new Opentip(".tooltip.link-rep.msg-" + index + '"', "Botón para reposicionar este aviso en los resultados. Sólo puede ser utilizado una vez por día.", { style: "myErrorStyle" });
      }
    });
    $(".tooltip.link-rubro-papel").each(function( index ) {
      new Opentip(".tooltip.link-rubro-papel.msg-" + index + '"', $('.tooltip.link-rubro-papel.msg-' + index).attr('alt'), { style: "myErrorStyle" });
    });
  }
  
  // Historial de avisos visitados
  var history = null;
  try {
    history = JSON.parse(localStorage.getItem('history'));
  } catch(err) {

  }
  if (history === null) { history = []; }
  // Si estoy en un aviso, lo agrego al historial
  if(window.location.pathname.indexOf("/avisos/") != -1) {
    // Incorporamos el aviso al array que almacenaremos en local.
    var date = new Date();
    var aviso = {nid:$('#aviso-nid').html(), url:window.location.pathname, titulo:$('.AvisoTitulo h2').html(), imagen:$('#multimedia_fotos a img').attr('src'), precio:$('.ContentPrecio h2 span').html(), category:$('meta[name="cXenseParse:gcl-categories"]').attr("content"), fecha:date.getTime()};
    // Si el aviso no posee nid (aviso finalizado), no lo almacenamos
    if(aviso.nid != undefined && aviso.category != undefined) {
      // Si el aviso existe en el array lo reemplazamos
      var existe = -1;
      for (var i=0; i<history.length; i++) { 
        if(history[i]['nid'] == $('#aviso-nid').html()){ 
          existe = i;
        }
      }
      if(existe != -1){
        history.splice(existe, 1);
      } else {
        // Guardamos ultimos 60 avisos vistos
        if(history.length == 60)
          history.splice(0,1);
      }
      history.unshift(aviso);
      // Guardamos en local (localStorage) el array history
      try {
        localStorage.setItem('history', JSON.stringify(history));
      } catch(err) {

      }
    }
  }
  
  //SubMenu Tiendas
  $("#subMenuTiendas li.link").hover(
    function() {
      $("#subMenuTiendas li.link").removeClass('activo');
      $(this).addClass('activo');
      $("#subMenuTiendas .sub." + $(this).attr("data-section")).show("");
    }, function() {
      $("#subMenuTiendas .sub." + $(this).attr("data-section")).hide("");
      $("#subMenuTiendas li.link").removeClass('activo');
    }
  );
  
  var stickyBanner = function() {
    $('.banner.sticky.lateral').each(function() {
      originalTop = $(this).data('original-top');
      marginTop = $(this).data('margin-top');
      if(originalTop==undefined) {
        marginTop = $(this).css('margin-top').replace('px', '');
        originalTop = $('.banner.sticky.lateral').offset().top-(parseInt(marginTop));
        $(this).data('margin-top', parseInt(marginTop));
        $(this).data('original-top', parseInt(originalTop));
      }
      var scrollTop = $(window).scrollTop(); // our current vertical position from the top
      // if we've scrolled more than the navigation, change its position to fixed to stick to top,
      // otherwise change it back to relative
      var marginPie = $('.pie').offset().top;
      var altoPaquetes = 0;
      if($('.sosInmobiliaria').length > 0) {
        altoPaquetes = $('.sosInmobiliaria').height() + 25;
      }
      if($('.sosConcesionaria').length > 0) {
        altoPaquetes = $('.sosConcesionaria').height() + 25;
      }
      if($('.sosAlojamiento').length > 0) {
        altoPaquetes = $('.sosAlojamiento').height() + 25;
      }
      if($('.sosComercio').length > 0) {
        altoPaquetes = $('.sosComercio').height() + 25;
      }
      var altoPie = $('.pie').height() + 23 + altoPaquetes;
      var altoBanner = 600;
      var marginTopsite = $('#dfp-topsite').offset().top;
      var frenoPie = marginPie - altoPie - altoBanner;
      var marginLateral = marginPie - altoPie - altoBanner - marginTopsite;
      if (scrollTop > originalTop && scrollTop < frenoPie) { 
        $(this).addClass('sticky-applied');
        $(this).css('margin-top', '0px');
      } else {
        $(this).removeClass('sticky-applied');
        if(scrollTop > frenoPie && frenoPie > 0)
          $(this).css('margin-top', marginLateral+'px');
        else
          $(this).css('margin-top', marginTop+'px');
      }
    }); 
  };
  stickyBanner();
  // and run it again every time you scroll
  $(window).scroll(function() {
    stickyBanner();
  });
  
  var desplazarSitioBanner = function() {
    if($('#dfp-lateral_1').length >= 1) {
      if($(window).width() < 1300)
        $('.banner.sticky.lateral').hide('');
      else
        $('.Contenido').addClass('ConBannerLateral');  
    }
  };
  desplazarSitioBanner();
  //setTimeout(desplazarSitioBanner, 1000);
  
  $('.link-info-tienda').click(function(){
    $.colorbox({
      href:'#modal-info-tienda',
      inline: true,
      width: 700,
    });
  });
  
  // Contenido historial
  if($("#content-historial").length > 0){
    history = localStorage.getItem('history');
    if (history === null) {
      $("#content-historial").html('<div class="messageSinDatos">No registras avisos visitados en las últimas horas.</div>');
    } else {
      $.ajax({
        url: '/ajax/listado_historial',
        type: 'post',
        data: {historial: history},
        dataType: 'html',
        success: function(html) {
          $("#content-historial").html(html);
        }
      });
    }
  }
  
  // Precios UVA
  if($('.search-precio-uva').length > 0) {
    $('.search-precio-uva').each(function(index) {
      if($(this).attr('data-precio') != '' && $(this).attr('data-precio') != 'consultar') {
        var id_aviso = $(this).attr('data-aviso');
        $.get( "/ajax/obtener_precios_uva/" + $(this).attr('data-precio') + "/" + $(this).attr('data-moneda'), function( data ) {
          if(data != '' && data != '0,00')
            $('.search-precio-uva.' + id_aviso).html(data + ' UVAS');
          else
            $('.search-precio-uva.' + id_aviso).html('');        
        });
      }
    });
  }
  //Toco pagina de concesionarias
  if(document.title.startsWith('Concesionarias que publican')) {
    document.title = 'Concesionarias que publican en Clasificados Los Andes - Clasificados Los Andes';
    $('h1.title').html('CONCESIONARIAS UQE PUBLICAN EN CLASIFICADOS LOS ANDES');
  }

});
