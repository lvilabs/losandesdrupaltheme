var genero = '';
var deporte = '';
  
function redirigir_aviso(tipo){
  var url_tipo = tipo;
  window.location = url_tipo;
}

/** 
* Calcular el total de lo seleccionado
* 
*/
function calcular_total(){
  var total = "0";
  var precio = "0.00";
  $('.detalle-precio').each(function() {
    if($(this).html() != "Sin costo" && $(this).html() != "Disponible"){
      precio = $(this).html().split("$");
      total = parseFloat(total) + parseFloat(precio[1]); 
    }
  });
  arr = String(total).split(".");
  if(arr[1]) {
    if(arr[1].length==1)
      $('.total-precio').html(total+"0");
    else
      $('.total-precio').html(total);
  } else {
    $('.total-precio').html(total+".00");
  }
  if(total == 0)
    $('.detalle-seleccion').hide();
}

/** 
* Quitar espacio seleccionado
* 
* @param int espacioId Id del espacio
* 
*/
function eliminar_espacio(espacioId){
  if($('#edit-publicado').val() == 1){
    $('.espacios .form-radio').each(function() {
      if($(this).val() == espacioId){
        $(this).attr('checked', false);
        $('.seleccionado .form-radio').attr('checked', true);
        validacion_espacio_mejora($('.seleccionado .form-radio').val());
      } 
    });
  } else {
    $('#edit-espacios-5').attr('checked', true);
  }
  $('.lista-detalle-espacios').html('');
  validacion_espacio_mejora(AUTOSLAVOZ_ESPACIO_GRATUITO);
  calcular_total();
}

/** 
* Quitar credito seleccionado
* 
* @param int creditoId Id del credito
* 
*/
function eliminar_credito(creditoId){
  $('.form-checkbox').each(function() {
    var nodoId = $(this).attr('name').split("_");
    if(!isNaN(parseInt(nodoId[1]))){
      if(nodoId[1] == creditoId){
        $(this).attr('checked', false);
        $('.detalle-'+creditoId).remove();
      }
    }else{
      if(nodoId[2] == creditoId){
        $(this).attr('checked', false);
        $('.detalle-'+creditoId).remove();
      }
    }  
  });
  $('.mejoras .form-radio').each(function() {
    if($(this).val() == creditoId){
      $(this).attr('checked', false);
      var categoria = $('#categoria-'+creditoId).html();
      $('.detalle-'+categoria).remove();
    }
  });
  if(creditoId == AUTOSLAVOZ_CREDITO_PAPELWEB_NID){
    $('#content-papel').fadeOut("slow");
  }
  calcular_total();
}

/** 
* Agregar datos del espacio al detalle de publicacion
* 
* @param int espacioId Id del espacio
* 
*/
function agregar_costo_espacio(espacioId){
  var titulo = $('#titulo-'+espacioId).html();
  var costo = $('#precio-'+espacioId).html();
  var disponible = $('#disponibilidad-'+espacioId).html();
  var costo_texto = "";
  
  if(disponible > 0) {
    $('.detalle-seleccion').hide();
    return;
  } else {
    $('.detalle-seleccion').show();
    costo_texto = '$'+costo;
  }
  
  //Detalle
  if(espacioId == AUTOSLAVOZ_ESPACIO_GRATUITO)
    $('.lista-detalle-espacios').html('<div class="lista-seleccion">'+titulo+' <span class="detalle-precio">Sin costo</span></div>');
  else
    $('.lista-detalle-espacios').html('<div class="lista-seleccion">'+titulo+' <span class="detalle-precio">'+costo_texto+'</span><a href="javascript:void(0)" onclick="eliminar_espacio('+espacioId+')" id="quitar-'+espacioId+'">-</a></div>');
    
  //Total
  calcular_total();
}

/** 
* Agregar datos del credito al detalle de publicacion
* 
* @param int creditoId Id del credito
* @param bool estado Checkeado o no
* 
*/
function agregar_costo_credito(creditoId, estado){
  var categoria = $('#categoria-'+creditoId).html();
  if (!estado){
    if (categoria != null)
      $('.detalle-'+categoria).remove();
    else
      $('.detalle-'+creditoId).remove();
  } else {
    if($('.credito_'+creditoId+' .seleccionado').html() == null && $('.credito_'+creditoId+' .comprado').html() == null){
      var titulo = $('#titulo-'+creditoId).html();
      var costo = $('#precio-'+creditoId).html();
      var disponible = $('#disponibilidad-'+creditoId).html();
      var costo_texto = '';
      if(disponible > 0) {
        return;
      } else {
        costo_texto = '$'+costo;
        $('.detalle-seleccion').show(); 
      }
      
      //Detalle
      if (categoria != null){
        $('.detalle-'+categoria).remove();
        $('.lista-detalle-creditos').append('<div class="lista-seleccion detalle-'+categoria+'">'+categoria+ ' - ' +titulo+' <span class="detalle-precio">'+costo_texto+'</span><a href="javascript:void(0)" onclick="eliminar_credito(\''+creditoId+'\')" id="quitar-'+categoria+'">-</a></div>');
      } else {
        $('.lista-detalle-creditos').append('<div class="lista-seleccion detalle-'+creditoId+'">'+titulo+' <span class="detalle-precio">'+costo_texto+'</span><a href="javascript:void(0)" onclick="eliminar_credito(\''+creditoId+'\')" id="quitar-'+creditoId+'">-</a></div>');
      }
    }
  }  
  //Total
  calcular_total();
}

/** 
* Validar que no se seleccione una mejora que ya posee un espacio
* 
* @param int espacioId Id del espacio
* 
*/
function validacion_espacio_mejora(espacioId){
  //Super Premium/Vidriera/Premium -> Quitar Remarco Color, 12 Fotos, Foto Listado
  var tituloEspacio = $('#titulo-'+espacioId);
  var arrEspaciosPremiums = ["Super Premium", "Premium", "Vidriera"];
  if(jQuery.inArray(tituloEspacio.html(), arrEspaciosPremiums) != -1){
    if($('#edit-espacios-'+espacioId).attr('checked')){
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).hide();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS).hide();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).hide();
      //$('.Banderola .form-radio').attr('disabled', true);
      if($('#edit-publicado').val() != 1){
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('checked', false);
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('checked', false);
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('checked', false);
        //$('.Banderola .form-radio').attr('checked', false);
      }
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_RECUADRO_COLOR, false);
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_12_FOTOS, false);
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_FOTO_LISTADO, false);
      //$('.detalle-Banderola').remove();
    }
  }
  //Estandar -> Quitar Remarco color, Foto Listado
  if($('#titulo-'+espacioId).html() == "Estandar"){
    if($('#edit-espacios-'+espacioId).attr('checked')){
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).hide();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS).show();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).hide();
      if($('#edit-publicado').val() != 1){
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('checked', false);
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('checked', false);
      }
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_RECUADRO_COLOR, false);
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_FOTO_LISTADO, false);
    }
  }
  // Basico -> Quitar Remarco color
  if($('#titulo-'+espacioId).html() == "Basico"){
    if($('#edit-espacios-'+espacioId).attr('checked')){
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).show();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS).show();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('disabled', true);
      $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).hide();
      if($('#edit-publicado').val() != 1){
        $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('checked', false);
      }
      agregar_costo_credito(AUTOSLAVOZ_CREDITO_FOTO_LISTADO, false);
    }
  }
  // Gratuito -> Quitar Remarco color
  if($('#titulo-'+espacioId).html() == "Gratuito"){
    if($('#edit-espacios-'+espacioId).attr('checked')){
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).show();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS).show();
      $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('disabled', false);
      $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).show();
    }
  }  
  //Si esta adquirido o en espera de pago lo dejo deshabilitado
  if($('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR+' .seleccionado').html() != null || $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR+' .comprado').html() != null){
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).attr('disabled', true);
    $('.credito_'+AUTOSLAVOZ_CREDITO_RECUADRO_COLOR).show();
  }
  if($('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS+' .seleccionado').html() != null || $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS+' .comprado').html() != null){
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_12_FOTOS).attr('disabled', true);
    $('.credito_'+AUTOSLAVOZ_CREDITO_12_FOTOS).show();
  }
  if($('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO+' .seleccionado').html() != null || $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO+' .comprado').html() != null){
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).attr('disabled', true);
    $('.credito_'+AUTOSLAVOZ_CREDITO_FOTO_LISTADO).show();
  }
}

//**
//* Inhabilitar campos del formulario que no deben ser modificados por el usuario
//*
//*/
//function inhabilitar_campos(){
//  var corte = 0;
//  var habilita_gratuito = 0;
//  //Espacios
//  $('.espacios .form-radio').each(function() {
//    if (corte == 0){
//      if($(this).attr('checked') == true){
//        if($('#reusable-'+$(this).val()).html() == 1){
//          $(this).attr('disabled', false);
//          habilita_gratuito = 1;
//        }
//        validacion_espacio_mejora($(this).val());
//        corte = 1;
//      }
//    } else {
//      //Si el espacio es reutilizable, lo dejo habilitado
//      if($('#reusable-'+$(this).val()).html() == 1){
//        $(this).attr('disabled', false);
//      } else {
//        $(this).attr('disabled', true);
//      }
//    }
//  });
//  if(habilita_gratuito)
//    $('#edit-espacios-'+AUTOSLAVOZ_ESPACIO_GRATUITO).attr('disabled', false);
//}

/** 
* Unifica los teléfonos de contactos en un solo registro
* 
*/
function unificar_telefonos_contacto() {
  var tel_unificado = '';
  if($("#tel_vendedor_1").val() != '')
    tel_unificado = $("#tel_vendedor_1").val();
  if($("#tel_vendedor_2").val() != '')
    tel_unificado = tel_unificado + ' / ' + $("#tel_vendedor_2").val();
  if($("#tel_vendedor_3").val() != '')
    tel_unificado = tel_unificado + ' / ' + $("#tel_vendedor_3").val();
  $("#edit-field-aviso-tel-vendedor-0-value").val(tel_unificado);
}

jQuery(document).ready(function($){
  //Ocultar boton resumen
  $(".teaser-button").hide("");
  $("#edit-teaser-include-wrapper").hide("");

  $('#select-tipos-contenido').ajaxStart(function() {
    $(this).attr('disabled', 'disabled');
  });
  $('#select-tipos-contenido').ajaxComplete(function() {
    $(this).removeAttr('disabled');
  });
  $("#select-tipos-contenido").change(function(){
    var padre = $(this).parent();
    var form_elements = $('#node-form').find('input, textarea, select');
    form_elements.attr('disabled', 'disabled');
    padre.find(".description span strong").text($(this).find("option:selected").text());
    padre.find(".description").show();
    redirigir_aviso($(this).val());
  });
  
  if($("#edit-field-aviso-operacion-value").length > 0) {
    // Mostrar/Ocutar precio UVA
    if($("#edit-field-aviso-operacion-value").val()==2) {
      $(".precio-uva").show();
      $(".CotizacionUva").show();
    } else {
      $(".precio-uva").hide();
      $(".CotizacionUva").hide();
    }
  }
  $("#edit-field-aviso-operacion-value").change(function() {
    // Mostrar/Ocutar zona turistica y equipamiento temporario
    if($("#edit-field-aviso-operacion-value").val()==5) {
      $(".zona_turistica").show();
      $(".equipamiento_temporario").show();
    } else {
      $(".zona_turistica").hide();
      $(".equipamiento_temporario").hide();
    }
    // Mostrar/Ocutar precio UVA
    if($("#edit-field-aviso-operacion-value").val()==2) {
      $(".precio-uva").show();
      $(".CotizacionUva").show();
    } else {
      $(".precio-uva").hide();
      $(".CotizacionUva").hide();
      $("#edit-field-aviso-precio-uva-value-1").val(0);
      $("#edit-field-aviso-precio-uva-value-1").attr('checked', '');
    }
  });
  
  // Calcular precios UVA
  if($("#edit-field-aviso-precio-uva-value-1").length > 0) {
    if($("#edit-field-aviso-precio-uva-value-1").attr('checked') == true)
      mostrar_precio_uva();
    $('#edit-field-aviso-precio-0-value').change(function() {
      $('.mont-convertido-uva').html('<a href="javascript:void(0);" onclick="mostrar_precio_uva();">Recalcular UVAS</a>');
    });
    $('input[name=field_aviso_moneda[value]]').change(function() {
      $('.mont-convertido-uva').html('<a href="javascript:void(0);" onclick="mostrar_precio_uva();">Recalcular UVAS</a>');
    });
  }
  $("#edit-field-aviso-precio-uva-value-1").click(function() {
    if($(this).attr('checked') == true)
      mostrar_precio_uva();
    else
      $('.mont-convertido-uva').html('');
  });
  
  //Habilitar campo kilometros y anio
  var mydate=new Date();
  var year=mydate.getYear();
  year+=1900;
  if($("#edit-field-aviso-usado-value-0").attr("checked")){
    $("#edit-field-aviso-kilometros-0-value").attr("disabled", true);
    $("#edit-field-aviso-kilometros-0-value").addClass("disabled");
    $(".sin-kilometro").hide("");
    $("#edit-field-aviso-usado-anio-0-value-year option[value="+year+"]").attr("selected",true);
    $("#edit-field-aviso-usado-anio-0-value-year").attr("disabled", true);
    $("#edit-field-aviso-usado-anio-0-value-year").addClass("disabled");
  }
  $("#edit-field-aviso-usado-value-0").click(function(){
    if($(this).attr("checked")){
      $("#edit-field-aviso-kilometros-0-value").attr("disabled", true);
      $("#edit-field-aviso-kilometros-0-value").addClass("disabled");
      $("#edit-field-aviso-kilometros-0-value").removeClass("error");
      if($("#edit-field-aviso-kilometros-0-value-wrapper label .form-required").html() != null)
        $("#edit-field-aviso-kilometros-0-value-wrapper label span").remove();
      $(".sin-kilometro").hide("");
      $("#edit-field-aviso-usado-anio-0-value-year option[value="+year+"]").attr("selected",true);
      $("#edit-field-aviso-usado-anio-0-value-year").attr("disabled", true);
      $("#edit-field-aviso-usado-anio-0-value-year").addClass("disabled");
    }    
  });
  $("#edit-field-aviso-usado-value-1").click(function(){
    if($(this).attr("checked")){
      $("#edit-field-aviso-kilometros-0-value").removeAttr("disabled");
      $("#edit-field-aviso-kilometros-0-value").removeClass("disabled");
      if($("#edit-field-aviso-kilometros-0-value-wrapper label .form-required").html() == null)
        $("#edit-field-aviso-kilometros-0-value-wrapper label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
      $(".sin-kilometro").show("");
      $("#edit-field-aviso-usado-anio-0-value-year").removeAttr("disabled");
      $("#edit-field-aviso-usado-anio-0-value-year").removeClass("disabled");
    }    
  });
  $("#edit-field-aviso-sin-kilometros-value-1").click(function(){
    if($(this).attr("checked")){
      $("#edit-field-aviso-kilometros-0-value").attr("disabled", true);
      $("#edit-field-aviso-kilometros-0-value").addClass("disabled");
      $("#edit-field-aviso-kilometros-0-value").removeClass("error");
      if($("#edit-field-aviso-kilometros-0-value-wrapper label .form-required").html() != null)
        $("#edit-field-aviso-kilometros-0-value-wrapper label span").remove();
    } else {
      $("#edit-field-aviso-kilometros-0-value").removeAttr("disabled");
      $("#edit-field-aviso-kilometros-0-value").removeClass("disabled");
      if($("#edit-field-aviso-kilometros-0-value-wrapper label .form-required").html() == null)
        $("#edit-field-aviso-kilometros-0-value-wrapper label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
    }
  });
  if($("#edit-field-aviso-sin-kilometros-value-1").attr("checked")){
    $("#edit-field-aviso-kilometros-0-value").attr("disabled", true);
    $("#edit-field-aviso-kilometros-0-value").addClass("disabled");
    $("#edit-field-aviso-kilometros-0-value").removeClass("error");
  }
  
  //Habilitar campo Zona
  if($('.readonly-text.ciudad').val() == 'Mendoza')
    $('.zona').show("");
  else
    $('.zona').hide("");
  $('#edit-taxonomy-5-hierarchical-select-selects-1').live('change', function (){
    if($(this).val() == AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID)
      $('.zona').show("slow");
    else
      $('.zona').hide("slow");
  });
  
  // Ordenar combo Provincias
  if($("#edit-taxonomy-5-hierarchical-select-selects-0").length > 0) {
    var selectToSort = $("#edit-taxonomy-5-hierarchical-select-selects-0");
    selectToSort.children('option').each(function(index) {
      if($(this).html() == 'Mendoza')
        $(this).prependTo(selectToSort);
    });
    var selectTop = selectToSort.children('option')[1];
    selectToSort.prepend(selectTop);
  }
  
  //Deshabilitar campo Etapa de construccion
  if($("#edit-field-aviso-estrenar-value-1").attr("checked")){
    $(".etapa-construccion").hide("");
    $("#edit-field-aviso-condicion-value").val(1);
  }
  $("#edit-field-aviso-estrenar-value-1").click(function(){
    if($(this).attr("checked")){
      $(".etapa-construccion").hide("slow");
      $("#edit-field-aviso-condicion-value").val(1);
    } else {
      $('.etapa-construccion').show("slow");
      $("#edit-field-aviso-condicion-value").val(0);
    }
  });
  
  //Marcar campos requeridos
  $("#edit-body-wrapper label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  
  //Contador Caracteres titulo
  var options = {
      'maxCharacterSize': 80,
      'displayFormat' : 'Caracteres restantes: <span>#left</span> de #max'
  };
  $('#edit-title').textareaCount(options);
  
  //Deshabilitar stock si se selecciona a pedido
  if($("#edit-apedido-venta-1").length > 0) {
    if($("#edit-apedido-venta-1").attr("checked")){
      $("#edit-cantidad-venta").attr("disabled", "disable");
      $("#edit-cantidad-venta").css('background-color', '#D6D6D6');
    }
    $("#edit-apedido-venta-1").click(function(){
      if($(this).attr("checked")){
        $("#edit-cantidad-venta").attr("disabled", "disable");
        $("#edit-cantidad-venta").val(0);
        $("#edit-cantidad-venta").css('background-color', '#D6D6D6');
      } else {
        $("#edit-cantidad-venta").removeAttr("disabled");
        $("#edit-cantidad-venta").css('background-color', 'white');
      }
    });
  }
  // En productos mostramos stock por defecto o stock*talle si es subrubro moda.
  $("#edit-taxonomy-34-hierarchical-select-selects-1").live('change', function() {
      if($(this).val()==6275) {
        var div = $('#edit-cantidad-venta-wrapper').parent();
        div.hide();
        var div = $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).parent();
        $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).val(genero);
        div.show(); // Mostramos Genero
        var div = $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).parent();
        $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).val(deporte);
        div.show(); // Mostramos Deporte
        var div = $('fieldset.talles').parent();
        div.show();
      } else {
        var div = $('#edit-cantidad-venta-wrapper').parent();
        div.show();
        var div = $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).parent();
        genero = $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).val();
        $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).val('');
        div.hide(); // Ocultamos Genero
        var div = $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).parent();
        deporte = $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).val();
        $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).val('');
        div.hide(); // Ocultamos Deporte
        var div = $('fieldset.talles').parent();
        div.hide();
      }
  });
  genero = $('#edit-taxonomy-'+CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID).val();
  deporte = $('#edit-taxonomy-'+CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID).val();
  // Ocultamos/mostramos stock*talle cuando carga la pagina.
  $("#edit-taxonomy-34-hierarchical-select-selects-1").change();
  
  // Ocultar/Mostrar telefono whatsapp
  if($("#edit-field-aviso-ocultar-whatsapp-value-1").length > 0) {
    if($("#edit-field-aviso-ocultar-whatsapp-value-1").attr("checked")){
      $("#telefono_whatsapp").attr("value", "");
      $("#telefono_whatsapp").css('background-color', '#D6D6D6');
    }
    $("#edit-field-aviso-ocultar-whatsapp-value-1").click(function(){
      if($(this).attr("checked")){
        $("#telefono_whatsapp").attr("value", "");
        $("#telefono_whatsapp").css('background-color', '#D6D6D6');
      } else {
        $("#telefono_whatsapp").css('background-color', 'white');
        if($("#telefono_whatsapp").attr("data-hidden") != '')
          $("#telefono_whatsapp").attr("value", $("#telefono_whatsapp").attr("data-hidden"));
      }
    });
  }
  
  // Actualizar campo teléfono de contacto
  if($("#tel_vendedor_1").length > 0) {
    $("#tel_vendedor_1").blur(function() {
      unificar_telefonos_contacto();
    });
    $("#tel_vendedor_2").blur(function() {
      unificar_telefonos_contacto();
    });
    $("#tel_vendedor_3").blur(function() {
      unificar_telefonos_contacto();
    });
  }
    
  /*********** Form publicacion ****************/
  //Si el aviso ya esta publicado
  if($('#edit-publicado').val() == 1){
    $('.lista-detalle-espacios').empty();
    $('.lista-detalle-creditos').empty();
    $('.total-precio').html('0.00');
    //inhabilitar_campos();
  }
  
  //Seleccion de espacio
  $(".espacios .form-radio").click(function(){
    if($('.seleccionado .form-radio').val() != $(this).val()){
      agregar_costo_espacio($(this).val());
    } else {
      $('.lista-detalle-espacios').empty();
      calcular_total();
    }
    //Validaciones entre Espacios y Mejoras
    validacion_espacio_mejora($(this).val());
  });
  //Seleccion de credito
  $(".mejoras .form-radio").click(function(){
    agregar_costo_credito($(this).val(), $(this).attr('checked'));
  });
  $(".mejoras .form-checkbox").click(function(){
    var nodoId = $(this).attr('name').split("_");
    if(!isNaN(parseInt(nodoId[1])))
      agregar_costo_credito(nodoId[1], $(this).attr('checked'));
    else
      agregar_costo_credito(nodoId[2], $(this).attr('checked'));
  });
  //Seleccionados en la recarga
  $(".espacios .form-radio").each(function(){
    if($(this).attr('checked') && $('.seleccionado .form-radio').val() != $(this).val())
      agregar_costo_espacio($(this).val());
    if($(this).attr('checked') && $('.seleccionado .form-radio').val() == $(this).val())
      validacion_espacio_mejora($(this).val());
  });
  $(".mejoras .form-radio").each(function(){
    if($(this).attr('checked'))
      agregar_costo_credito($(this).val(), $(this).attr('checked'));
  });
  $(".mejoras .form-checkbox").each(function(){
    if($(this).attr('checked') && $('.seleccionado .form-checkbox').attr('name') != $(this).attr('name')){
      var nodoId = $(this).attr('name').split("_");
      if(!isNaN(parseInt(nodoId[1])))
        agregar_costo_credito(nodoId[1], $(this).attr('checked'));
      else
        agregar_costo_credito(nodoId[2], $(this).attr('checked'));
    }
  });
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('checked') == true){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_NID, 1);
    $('#content-papel').fadeIn("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked') == true){
    $('#content-papel').fadeIn("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked') == true){
    $('#content-papel').fadeIn("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).attr('checked') == true){
    $('#content-papel').fadeIn("slow");
  }
  
  //Espacios y creditos ya comprados
  $('.comprado .form-radio').attr('disabled', true);
  $('.comprado .form-checkbox').attr('disabled', true);
  $('.comprado .form-select').attr('disabled', true);
  $('.comprado #edit-text-papel').attr('disabled', true);
  $('.comprado #edit-text-papel-concesionaria').attr('disabled', true);
  $('.comprado #edit-text-papel-inmobiliaria').attr('disabled', true);
  $('.comprado #edit-text-papel-comercio').attr('disabled', true);
  $('.comprado #edit-text-papel-clasifoto').attr('disabled', true);
  //Espacios y creditos ya adquirido
  $('.seleccionado .form-checkbox').attr('disabled', true);
  $('.seleccionado .form-select').attr('disabled', true);
  $('.seleccionado #edit-text-papel').attr('disabled', true);
  $('.seleccionado #edit-text-papel-concesionaria').attr('disabled', true);
  $('.seleccionado #edit-text-papel-inmobiliaria').attr('disabled', true);
  $('.seleccionado #edit-text-papel-comercio').attr('disabled', true);
  $('.seleccionado #edit-text-papel-clasifoto').attr('disabled', true);
  $('.seleccionado #edit-label-clasifoto').attr('disabled', true);
  $('.seleccionado #edit-pie-clasifoto').attr('disabled', true);

  //Mostrar/Ocultar Textarea de Papel
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('checked') == true || $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('checked') == 'checked'){
    $('#content-papel').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
  } else {
    $('#content-papel').fadeOut("slow");
    $('.papel-previsualizar').fadeOut("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked') == true || $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked') == 'checked'){
    $('#content-papel-concesionaria').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
  } else {
    $('#content-papel-concesionaria').fadeOut("slow");
    $('.papel-previsualizar').fadeOut("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked') == true || $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked') == 'checked'){
    $('#content-papel-inmobiliaria').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
  } else {
    $('#content-papel-inmobiliaria').fadeOut("slow");
    $('.papel-previsualizar').fadeOut("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked') == true || $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked') == 'checked'){
    $('#content-papel-comercio').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
  } else {
    $('#content-papel-comercio').fadeOut("slow");
    $('.papel-previsualizar').fadeOut("slow");
  }
  if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).attr('checked') == true || $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).attr('checked') == 'checked'){
    $('#content-papel-clasifoto').fadeIn("slow");
    $('.papel-previsualizar-clasifoto').fadeIn("slow");
  } else {
    $('#content-papel-clasifoto').fadeOut("slow");
    $('.papel-previsualizar-clasifoto').fadeOut("slow");
  }
  $('.comprado #content-papel').show();
  $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).click(function(){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_NID, $(this).attr('checked'));
    if($(this).attr("checked") == true || $(this).attr("checked") == 'checked'){
      $('#content-papel').fadeIn("slow");
      $('.papel-previsualizar').fadeIn("slow");
    } else {
      $('#content-papel').fadeOut("slow");
      $('.papel-previsualizar').fadeOut("slow");
    }
  });
  $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).click(function(){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID, $(this).attr('checked'));
    if($(this).attr("checked") == true || $(this).attr("checked") == 'checked'){
      $('#content-papel-concesionaria').fadeIn("slow");
      $('.papel-previsualizar').fadeIn("slow");
    } else {
      $('#content-papel-concesionaria').fadeOut("slow");
      $('.papel-previsualizar').fadeOut("slow");
    }
  });
  $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).click(function(){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID, $(this).attr('checked'));
    if($(this).attr("checked") == true || $(this).attr("checked") == 'checked'){
      $('#content-papel-inmobiliaria').fadeIn("slow");
      $('.papel-previsualizar').fadeIn("slow");
    } else {
      $('#content-papel-inmobiliaria').fadeOut("slow");
      $('.papel-previsualizar').fadeOut("slow");
    }
  });
  $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).click(function(){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID, $(this).attr('checked'));
    if($(this).attr("checked") == true || $(this).attr("checked") == 'checked'){
      $('#content-papel-comercio').fadeIn("slow");
      $('.papel-previsualizar').fadeIn("slow");
    } else {
      $('#content-papel-comercio').fadeOut("slow");
      $('.papel-previsualizar').fadeOut("slow");
    }
  });
  $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).click(function(){
    agregar_costo_credito(AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID, $(this).attr('checked'));
    if($(this).attr("checked") == true || $(this).attr("checked") == 'checked'){
      $('#content-papel-clasifoto').fadeIn("slow");
      $('.papel-previsualizar-clasifoto').fadeIn("slow");
    } else {
      $('#content-papel-clasifoto').fadeOut("slow");
      $('.papel-previsualizar-clasifoto').fadeOut("slow");
    }
  });
  
  //Contador Textarea de Papel
  var options = {
      'maxCharacterSize': 120,
      'displayFormat' : 'Caracteres restantes: <span>#left</span> de #max'
  };
  $('#edit-text-papel').textareaCount(options);
  $('#edit-text-papel-concesionaria').textareaCount(options);
  $('#edit-text-papel-inmobiliaria').textareaCount(options);
  $('#edit-text-papel-comercio').textareaCount(options);
  $('.originalTextareaInfo').css('width','auto');
  //Evitar simbolos texto papel
  $("#edit-text-papel, #edit-text-papel-concesionaria, #edit-text-papel-inmobiliaria, #edit-text-papel-comercio").keypress(function(event){
    if(event.keyCode == 60 || event.keyCode == 62)
      event.returnValue = false;
  });
  
  $('#confirmacion-cancelar').click(function(e) {
    $.colorbox.close();
  });
  $('#confirmacion-publicar').click(function(e) {
    form_was_validate = true;
    $.colorbox.close();
    $("#publicacion-avisos-form-opciones-publicacion").submit();
  });

  //Validador numeros text
  $('#edit-field-aviso-kilometros-0-value').numeric("null");
  $('#edit-field-aviso-precio-0-value').numeric(",");
  
  $(".preview").click(function(e) {
    var es_clasifoto = false;
    if($(this).hasClass('preview-clasifoto')) {
      es_clasifoto = true;
    }
    $(this).attr('disabled', true);
    var texto = '';
    var textarea_id = '';
    if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked')) {
      texto = $('#edit-text-papel-concesionaria').val();
      textarea_id = '#edit-text-papel-concesionaria';
    } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked')) {
      texto = $('#edit-text-papel-inmobiliaria').val();
      textarea_id = '#edit-text-papel-inmobiliaria';
    } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked')) {
      texto = $('#edit-text-papel-comercio').val();
      textarea_id = '#edit-text-papel-comercio';
    } else {
      texto = $('#edit-text-papel').val();
      textarea_id = '#edit-text-papel';
    }
    if(es_clasifoto) {
      texto = $('#edit-text-papel-clasifoto').val();
      textarea_id = '#edit-text-papel-clasifoto';
    } 
    if(es_clasifoto) {
      texto = formatear_texto_papel(texto, 'clasifoto');
    } else {
      texto = formatear_texto_papel(texto, 'lineal');
    }
    if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked')) {
      $('#edit-text-papel-concesionaria').val(texto);
    } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked')) {
      $('#edit-text-papel-inmobiliaria').val(texto);
    } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked')) {
      $('#edit-text-papel-comercio').val(texto);
    } else if(es_clasifoto) {
      $('#edit-text-papel-clasifoto').val(texto);
    } else {
      $('#edit-text-papel').val(texto);
    }
    if(es_clasifoto) {
      $('.simulacion-papel-clasifoto').html('<img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif">');
      var label = $('#edit-label-clasifoto').val();
      var titulo = $('#edit-titulo-clasifoto').val();
      var pie = $('#edit-pie-clasifoto').val();
      var foto = $('#edit-foto-clasifoto').val();
      var nid = $('#edit-nid-clasifoto').val();
      $.ajax({
        type: 'POST',
        url: '/ajax/web-papel/datos-grafica-clasifoto',
        dataType: 'json',
        data: { texto: texto, label: label, titulo: titulo, pie: pie, foto: foto, nid: nid },
        success: function(response) {
          var error = '';
          if(response.con_foto!=true) {
            error = 'El aviso no tiene foto, debe editarlo y agregar una foto.';
          }
          if(response.validacion_label_largo!=true) {
            error += 'La etiqueta tiene un texto largo.';
          }
          if(response.validacion_titulo_largo!=true) {
            error += 'El título es largo.';
          }
          if(response.cant_lineas>3) {
            error += 'La descripción del aviso es muy larga, intente reducir algunas palabras.';
          }
          if(response.validacion_palabra_larga!=true) {
            error += 'La descripción tiene alguna palabra larga, no debe superar los 20 caracteres.';
          }
          if(response.validacion_pie_largo!=true) {
            error += 'El teléfono es largo.';
          }
          $('.simulacion-papel-clasifoto').html('<img src="/'+response.path_grafica+'" title="Simulación del texto en papel, no implica que la impresión en papel será igual"><br><br><strong>'+error+'</strong>');
          $(".preview-clasifoto").attr('disabled', false);
          $(textarea_id).val(response.texto_convertido);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert('Ocurrió un error, intente nuevamente');
          $('.simulacion-papel-clasifoto').html('');
          $(".preview-clasifoto").attr('disabled', false);
        }
      });
    } else {
      $('.simulacion-papel').html('<img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif">');
      $.post('/ajax/web-papel/datos-grafica', { texto: texto }, function(response) {
        var error = '';
        if(response.cant_lineas>3) {
          error = 'El aviso ocupa más de 3 líneas, intente reducir algunas palabras.';
        }
        if(response.validacion_palabra_larga!=true) {
          error = 'El aviso tiene alguna palabra larga, no debe superar los 20 caracteres.';
        }
        $('.simulacion-papel').html('<img src="/'+response.path_grafica+'" title="Simulación del texto en papel, no implica que la impresión en papel será igual"><br><br><strong>'+error+'</strong>');
        $(".preview").attr('disabled', false);
        $(textarea_id).val(response.texto_convertido);
        },
        "json"
      );
    }
  });
  
  var form_was_submited = false;
  var form_was_validate = false;
  //Desactivar boton submit en publicacion
  $("#publicacion-avisos-form-opciones-publicacion").submit(function(e){
    if(!form_was_submited) {
      form_was_submited = true;
      if(!form_was_validate) {
        if(($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('checked') == true ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked') == true ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked') == true ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked') == true ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).attr('checked') == true) && (
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('disabled') == false ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('disabled') == false ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('disabled') == false ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('disabled') == false ||
            $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID).attr('disabled') == false
        )) {
          $('#confirmacion-publicar').attr('disabled', 'disabled'); // Deshabilitamos el botón publicar hasta comprobar si se pasa de 3 líneas.
          var es_lineal = false;
          var es_clasifoto = false;
          var checkboxes = $('.credito-papel input[type="checkbox"]');
          if(checkboxes.length>0) {
            for(i=0; i<checkboxes.length; i++) {
              var obj = $(checkboxes[i]);
              if(obj.attr('checked')==true && obj.attr('disabled')==false) {
                if(obj.parents().hasClass('.credito-papel-clasifoto')) {
                  es_clasifoto = true;
                } else {
                  es_lineal = true;
                }
              }
            }
          }
          var texto = '';
          var texto_clasifoto = '';
          var textarea_id = '';
          if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked')) {
            texto = $('#edit-text-papel-concesionaria').val();
            textarea_id = '#edit-text-papel-concesionaria';
          } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked')) {
            texto = $('#edit-text-papel-inmobiliaria').val();
            textarea_id = '#edit-text-papel-inmobiliaria';
          } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked')) {
            texto = $('#edit-text-papel-comercio').val();
            textarea_id = '#edit-text-papel-comercio';
          } else {
            texto = $('#edit-text-papel').val();
            textarea_id = '#edit-text-papel';
          }e.preventDefault();
          if(es_lineal) {
            texto = formatear_texto_papel(texto, 'lineal');
          }
          if(es_clasifoto) {
            texto_clasifoto = $('#edit-text-papel-clasifoto').val();
            texto_clasifoto = formatear_texto_papel(texto_clasifoto, 'clasifoto');
            $('#edit-text-papel-clasifoto').val(texto_clasifoto);
          }
          if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID).attr('checked')) {
            $('#edit-text-papel-concesionaria').val(texto);
          } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked')) {
            $('#edit-text-papel-inmobiliaria').val(texto);
          } else if($('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID).attr('checked')) {
            $('#edit-text-papel-comercio').val(texto);
          } else {
            $('#edit-text-papel').val(texto);
          }
          $('#confirmacion-texto').html('<img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif">');
          var alto = 370;
          if(es_clasifoto) {
            alto = 560;
          }
          $.colorbox({
            href:'#modal-confirmacion-publicacion-papel',
            inline: true,
            width: 400,
            height: alto
          });
           $('#confirmacion-texto').html('');
          var lineal_ok = false;
          var clasifoto_ok = false;
          if(es_clasifoto==true) {
            var label = $('#edit-label-clasifoto').val();
            var titulo = $('#edit-titulo-clasifoto').val();
            var pie = $('#edit-pie-clasifoto').val();
            var foto = $('#edit-foto-clasifoto').val();
            var nid = $('#edit-nid-clasifoto').val();
            $.ajax({
              type: 'POST',
              url: '/ajax/web-papel/datos-grafica-clasifoto',
              dataType: 'json',
              data: { texto: texto_clasifoto, label: label, titulo: titulo, pie: pie, foto: foto, nid: nid },
              success: function(response) {
                var error = '';
                if(response.con_foto!=true) {
                  error = 'El aviso no tiene foto, debe editarlo y agregar una foto.';
                }
                if(response.validacion_label_largo!=true) {
                  error += 'La etiqueta tiene un texto largo.';
                }
                if(response.validacion_titulo_largo!=true) {
                  error += 'El título es largo.';
                }
                if(response.cant_lineas>3) {
                  error += 'La descripción del aviso es muy larga, intente reducir algunas palabras.';
                }
                if(response.validacion_palabra_larga!=true) {
                  error += 'La descripción tiene alguna palabra larga, no debe superar los 20 caracteres.';
                }
                if(response.validacion_pie_largo!=true) {
                  error += 'El teléfono es largo.';
                }
                if(label.length<3) {
                  error += 'La etiqueta es corta.';
                }
                if(titulo.length<3) {
                  error += 'El título es corto.';
                }
                if(texto_clasifoto.length<40) {
                  error += 'El texto es muy corto.';
                }
                $('#confirmacion-texto').append('<img src="/'+response.path_grafica+'"><br><br><strong>'+error+'</strong><br>');
                $('#edit-text-papel-clasifoto').val(response.texto_convertido);
                if(error=='') {
                  clasifoto_ok = true;
                }
                if(error=='' && (!es_lineal || lineal_ok)) {
                  $('#confirmacion-publicar').attr('disabled', false);
                }
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('#confirmacion-texto').html('Ocurrió un error, intente nuevamente');
              }
            });
          }
          if(es_lineal==true) {
            $.post('/ajax/web-papel/datos-grafica', { texto: texto }, function(response) {
                var error = '';
                if(response.cant_lineas>3) {
                  error = 'El aviso ocupa más de 3 líneas, intente reducir algunas palabras para que ocupe menos.';
                } else {
                  if(response.validacion_palabra_larga!=true) {
                    error = 'El aviso tiene alguna palabra larga, no debe superar los 20 caracteres.';
                  } else {
                    lineal_ok = true;
                    if(!es_clasifoto || clasifoto_ok) {
                      $('#confirmacion-publicar').attr('disabled', false);
                    }
                  }
                }
                $('#confirmacion-texto').append('<img src="/'+response.path_grafica+'"><br><br><strong>'+error+'</strong><br>');
                $(textarea_id).val(response.texto_convertido);
              },
              "json"
            );
          }
          e.preventDefault();
          form_was_submited = false;
          return false;
        }
      }
      $("#edit-submit-publicacion").attr('value', 'Enviando...');
    } else {
      e.preventDefault();
    }
  });

  //Desactivar boton submit en creación de nodo aviso
  /*$("#node-form").submit(function(e){
    
    console.log(e.keyCode);
    
    if(!form_was_submited || e.keyCode == 13) {
      form_was_submited = true;
      $("#edit-submit").attr('value', 'Enviando...');
    } else {
      e.preventDefault();
    }
  });*/
  window.onbeforeunload = envioSubmit;
  function envioSubmit() {
    if(!form_was_submited) {
      form_was_submited = true;
      $("#edit-submit").attr('value', 'Enviando...');
      $("#edit-submit").attr('disabled', 'disabled');
    } else {
      $("#node-form").submit(function(e){
        e.preventDefault();
      });
    }
  }
  
  $("#node-form").submit(function(e){
    // Mostramos mensaje si el usuario no guardó los datos del calendario
    if($('.marca_calendario_editado').length >= 1) {
      if($('#DOPBCP-start-date-view0').val() != '' && $('.marca_calendario_editado').val() != 1) {
        $.colorbox({
          href:'#modal_edicion_calendario',
          inline: true,
          width: "25%",
          onClosed:function(){
            $('html, body').animate({
              scrollTop: $('#calendario-backend').offset().top
            }, 500);
          }
        });
        e.preventDefault();
      }
    }
  });
    
  // Adaptación para swfupload
  if(Drupal.settings.swfupload_settings) {
    $('.swfupload_button div.center').html('Agregar imágenes');
    var ancho_boton = $('.swfupload_button div.center').width();
    if(ancho_boton<30)
      ancho_boton = 30;
    Drupal.settings.swfupload_settings['edit-field-aviso-fotos'].button_width = ancho_boton+40;
    Drupal.settings.swfupload_settings['edit-field-aviso-fotos'].file_upload_limit = 25;
    Drupal.settings.swfupload_settings['edit-field-aviso-fotos'].file_queue_limit = 5;
    var peso = Drupal.settings.swfupload_settings['edit-field-aviso-fotos'].file_size_limit;
    peso = peso.substr(0,  peso.length-2);
    $('#peso_maximo_field_aviso_fotos').html(Math.round(peso*1024)+'Kb');
    var extensiones = Drupal.settings.swfupload_settings['edit-field-aviso-fotos'].file_types;
    extensiones = extensiones.replace(/;/g, ' ');
    extensiones = extensiones.replace(/(\*|\.)/g, '');
    $('#extensiones_perimitidas_field_aviso_fotos').html(extensiones);
    
    console.log(Drupal.settings.swfupload_settings);
    
    SWFUpload.prototype.fileQueueError = function(file, code, message) {
      switch (code) {
        case -110: // The file selected is too large
          //var max_file_mbs = ref.getMbs(ref.settings.file_size_limit);
          //var file_mbs = ((file.size / 1024) / 1024);
          //ref.displayMessage(Drupal.t('The file size (!num1 MB) exceeds the file size limit (!num2 MB)!', {'!num1':file_mbs.toFixed(2), '!num2':max_file_mbs.toFixed(2)}), 'error');
          Drupal.swfu['edit-field-aviso-fotos'].displayMessage('El archivo seleccionado es demasiado grande', 'error');
          break;
        case -100: // Se seleccionaron muchos archivos
          Drupal.swfu['edit-field-aviso-fotos'].displayMessage('Se seleccionaron demasiados archivos a la vez', 'error');
        break;
        default:
          break;
      };
    };
    if(!FlashDetect.installed) {
      $('.swfupload_button').hide();
      $('.MiCuenta fieldset .Fotos').append('<div class="swfupload mensaje">Su navegador web debe tener el plugin de Flash para subir imágenes</div>');
    }
  }
  if(Drupal.settings.plup) {
    Drupal.settings.plup.field_aviso_fotos.max_file_size = '6MB';
    Drupal.settings.plup.field_aviso_fotos.max_files = 25;
    Drupal.settings.plup.field_aviso_fotos.resize.quality = 85;
    $('#edit-field-aviso-fotos').append('<div class="description">Peso máximo de la imagen: <em>6MB</em><br>Tamaño recomendado: <em>628px x 418px</em><br>Extensiones permitidas: <em>jpg jpeg</em><br><strong>Puede reordenar las imágenes arrastrándolas con el mouse</strong></div>');
    if(!FlashDetect.installed && !jQuery.support.opacity) { // Para IE < 9  y sin flash.
      Drupal.settings.plup.field_aviso_fotos.max_file_size = '300KB';
      $('#edit-field-aviso-fotos .description').html('Peso máximo de la imagen: <em>300Kb</em><br>Tamaño recomendado: <em>628px x 418px</em><br>Extensiones permitidas: <em>jpg jpeg</em><br>Recomndamos utilizar otro navegador o instalar el plugin de flash para realizar carga simultánea de imágenes.');
    }
    if(!jQuery.support.opacity) { // Para IE < 9.
        $('#edit-field-aviso-fotos .plup-remove-item').html('Borrar');//Necesario para IE7.
        $('#edit-field-aviso-fotos-plup-select').html('Agregar imágenes');//Necesario para IE7.
    }
    plupload.addI18n({
      'File size error.': 'El archivo excede el peso m&aacute;ximo permitido.'
    });
  }
  
  // Menu lateral
  var anchor = location.hash;
  var id = 0;
  if(anchor == '#menu-info') id = 0;
  if(anchor == '#menu-fotos') id = 1;
  if(anchor == '#menu-calendario') id = 2;
  if($("#edit-tabs").length){
    $( "#edit-tabs" ).tabs({
      active: id
    }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
  }
  
  // Habilitar/Desahabilitar pagos opcionales
  // Garantia
  if($('#edit-field-aviso-garantia-value-1').attr('checked') == undefined) {
    $('#deposito-monto').show('fast');
    $('#edit-field-aviso-garantia-monto-0-value').val('');
  }
  $('#edit-field-aviso-garantia-value-1').click(function(){
    if($('#edit-field-aviso-garantia-value-1').attr('checked') == 'checked') {
      $('#deposito-monto').hide('fast');
    } else {
      $('#deposito-monto').show('fast');
      $('#edit-field-aviso-garantia-monto-0-value').val('');
    }
  });
  // Limpieza
  if($('#edit-field-aviso-limpieza-value-1').attr('checked') == 'checked' || $('#edit-field-aviso-limpieza-value-2').attr('checked') == 'checked') {
    $('#limpieza-monto').show('fast');
    $('#edit-field-aviso-limpieza-monto-0-value').val('');
  }
  $('#edit-field-aviso-limpieza-value-1').click(function(){
    $('#limpieza-monto').show('fast');
    $('#edit-field-aviso-limpieza-monto-0-value').val('');
  });
  $('#edit-field-aviso-limpieza-value-2').click(function(){
    $('#limpieza-monto').show('fast');
    $('#edit-field-aviso-limpieza-monto-0-value').val('');
  });
  $('#edit-field-aviso-limpieza-value-3').click(function(){
    $('#limpieza-monto').hide('fast');
  });
  // Desayuno
  if($('#edit-field-aviso-desayuno-value-1').attr('checked') == 'checked' || $('#edit-field-aviso-desayuno-value-2').attr('checked') == 'checked') {
    $('#desayuno-monto').show('fast');
    $('#edit-field-aviso-desayuno-monto-0-value').val('');
  }
  $('#edit-field-aviso-desayuno-value-1').click(function(){
    $('#desayuno-monto').show('fast');
    $('#edit-field-aviso-desayuno-monto-0-value').val('');
  });
  $('#edit-field-aviso-desayuno-value-2').click(function(){
    $('#desayuno-monto').show('fast');
    $('#edit-field-aviso-desayuno-monto-0-value').val('');
  });
  $('#edit-field-aviso-desayuno-value-3').click(function(){
    $('#desayuno-monto').hide('fast');
  });
  // Cochera
  if($('#edit-field-aviso-cobro-cochera-value-1').attr('checked') == 'checked' || $('#edit-field-aviso-cobro-cochera-value-2').attr('checked') == 'checked') {
    $('#cochera-monto').show('fast');
    $('#edit-field-aviso-cochera-monto-0-value').val('');
  }
  $('#edit-field-aviso-cobro-cochera-value-1').click(function(){
    $('#cochera-monto').show('fast');
    $('#edit-field-aviso-cochera-monto-0-value').val('');
  });
  $('#edit-field-aviso-cobro-cochera-value-2').click(function(){
    $('#cochera-monto').show('fast');
    $('#edit-field-aviso-cochera-monto-0-value').val('');
  });
  $('#edit-field-aviso-cobro-cochera-value-3').click(function(){
    $('#cochera-monto').hide('fast');
  });
  
  $('.input-box #edit-field-aviso-ocultar-tel-value-1').attr('checked', 'checked');
  $('.input-box #edit-field-aviso-ocultar-mail-value-1').attr('checked', 'checked');
  
  $('.mensaje-button-submit').click(function(){
    $("#node-form").submit();
  });
  
  // Tooltip
  if($('.tooltip-ayuda').length>0) {
    $('.tooltip-ayuda').tooltip();
  }

  if(window.location.hash=='#papel-publicacion-concesionaria') {
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked', 'checked');
    $('#content-papel-concesionaria').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
    if($('.espacios :checked').val()==undefined) {
      $("input[name=espacios][value=" + AUTOSLAVOZ_ESPACIO_GRATUITO + "]").attr('checked', 'checked');
    }
  }
  if(window.location.hash=='#publicacion-papel-inmobiliaria') {
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked', 'checked');
    $('#content-papel-inmobiliaria').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
    if($('.espacios :checked').val()==undefined) {
      $("input[name=espacios][value=" + AUTOSLAVOZ_ESPACIO_GRATUITO + "]").attr('checked', 'checked');
    }
  }
  if(window.location.hash=='#publicacion-papel-comercio') {
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID).attr('checked', 'checked');
    $('#content-papel-comercio').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
    if($('.espacios :checked').val()==undefined) {
      $("input[name=espacios][value=" + AUTOSLAVOZ_ESPACIO_GRATUITO + "]").attr('checked', 'checked');
    }
  }
  if(window.location.hash=='#publicacion-papel') {
    $('#edit-credito-'+AUTOSLAVOZ_CREDITO_PAPELWEB_NID).attr('checked', 'checked');
    $('#content-papel').fadeIn("slow");
    $('.papel-previsualizar').fadeIn("slow");
    if($('.espacios :checked').val()==undefined) {
      $("input[name=espacios][value=" + AUTOSLAVOZ_ESPACIO_GRATUITO + "]").attr('checked', 'checked');
    }
  }
  
  // Generar Mapa
  if($('.section-node-edit').length >= 1 || $('.section-node-add').length >= 1) {
    OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {                
      defaultHandlerOptions: {
          'single': true,
          'double': false,
          'pixelTolerance': 0,
          'stopSingle': false,
          'stopDouble': false
      },
      initialize: function(options) {
          this.handlerOptions = OpenLayers.Util.extend(
              {}, this.defaultHandlerOptions
          );
          OpenLayers.Control.prototype.initialize.apply(
              this, arguments
          ); 
          this.handler = new OpenLayers.Handler.Click(
              this, {
                  'click': this.trigger
              }, this.handlerOptions
          );
      }, 
      trigger: function(e) {
        var lonlat = map.getLonLatFromPixel(e.xy);
        lonlat.transform(map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
        add_map_marker(lonlat.lon, lonlat.lat, 0);
      }
    });
    var latitud = '';
    var longitud = '';
    if($('#edit-field-aviso-mapa-0-locpick-user-latitude').length >= 1 && $('#edit-field-aviso-mapa-0-locpick-user-latitude').val() != '')
      latitud = $('#edit-field-aviso-mapa-0-locpick-user-latitude').val();
    if($('#edit-field-aviso-mapa-0-locpick-user-longitude').length >= 1 && $('#edit-field-aviso-mapa-0-locpick-user-longitude').val() != '')
      longitud = $('#edit-field-aviso-mapa-0-locpick-user-longitude').val();  
    genera_mapa(latitud, longitud); 
  }
      
});

/* Funciones Mapa */
var map;
var markers;
var country = 'Argentina';
var region = '';
var county = '';
function genera_mapa(latitud, longitud){
  map = new OpenLayers.Map("multimedia_mapas");
  map.addLayer(new OpenLayers.Layer.OSM());
  if(latitud != '' && longitud != '') {
    var lonLat = new OpenLayers.LonLat(longitud, latitud)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          );
    markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);
    markers.addMarker(new OpenLayers.Marker(lonLat));
    var zoom = 14;
  } else {
    var lonLat = new OpenLayers.LonLat(-68.844343, -32.88937)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          );
    var zoom = 11;
  }
  map.setCenter (lonLat, zoom);
  var click = new OpenLayers.Control.Click();
  map.addControl(click);
  click.activate();  
}
function add_map_marker(lon, lat, zoom) {
  if(markers != undefined)
    markers.destroy();
  var lonLat = new OpenLayers.LonLat(lon, lat)
    .transform(
      new OpenLayers.Projection("EPSG:4326"),
      map.getProjectionObject()
  );
  markers = new OpenLayers.Layer.Markers("Markers");
  map.addLayer(markers);
  var marker = new OpenLayers.Marker(lonLat);
  markers.addMarker(marker);
  if(zoom != 0)
    map.setCenter (lonLat, zoom);
  $('#edit-field-aviso-mapa-0-locpick-user-latitude').val(lat);
  $('#edit-field-aviso-mapa-0-locpick-user-longitude').val(lon);
}
function agregar_mapa(){
  $('.recomendacion').hide("");
  $('.opciones-geomap').hide("");
  $('.opciones-geomap li').remove();
  if($('#edit-taxonomy-5-hierarchical-select-selects-0 option:selected').html() == undefined) {
    region = $('#edit-taxonomy-5-hierarchical-select-selects-0').val();
    county = $('#edit-taxonomy-5-hierarchical-select-selects-1').val();
  } else {
    region = $('#edit-taxonomy-5-hierarchical-select-selects-0 option:selected').html();
    county = $('#edit-taxonomy-5-hierarchical-select-selects-1 option:selected').html();
  }
  var address = $('#edit-field-aviso-calle-0-value').val();
  if(region == 'Uruguay')
    country = 'Uruguay';
  if(region == 'Brasil')
    country = 'Brasil';
  if(county == 'Córdoba')
    county = 'La Capital';
  var housenumber = $('#edit-field-aviso-calle-altura-0-value').val();
  if(housenumber.toString().length > 2) { 
    var quitar = housenumber.substr(-2,2);
    if(quitar < 50) { 
      housenumber = parseInt(housenumber,10) - quitar;
    } else {
      var sum = 100 - quitar;
      housenumber = parseInt(housenumber,10) + sum;
    }                
  } else {
    if(housenumber < 50) 
      housenumber = 0;
    else 
      housenumber = 100;
  }
  if(region == '') {
    $('.reco-message').html("Se requiere la provincia para poder geolocalizarla.");
    $('.recomendacion').show("slow");
    return false;
  }
  if(county == '') {
    $('.reco-message').html("Se requiere la ciudad para poder geolocalizarla.");
    $('.recomendacion').show("slow");
    return false;
  }
  if(address == '') {
    $('.reco-message').html("Se requiere el nombre de la calle para poder geolocalizarla.");
    $('.recomendacion').show("slow");
    return false;
  }
  var queryString = "{address: '" + address + " " + housenumber + "', county: '" + county + "', region: '" + region + "', country: '" + country + "'";
  OpenLayers.Request.GET({
      url: "https://api.openrouteservice.org/geocode/search",
      scope: this,
      failure: this.requestFailure,
      success: this.requestSuccess,
      params: {
          api_key: '5b3ce3597851110001cf6248303b43c0ac2f4aad8898e0fc2d8cfa1a',
          text: queryString,
          'focus.point.lat': -32.88937,
          'focus.point.lon': -68.844343,
          size: 40,
          sources: 'osm'
      }
  });
}
function requestSuccess(response) {
  var features = JSON.parse(response.responseText);
  var direcciones = [];
  var count = 0;
  for(i = 0; i < features.features.length; i++) {
    // Solo buscamos los puntos de sudamerica
    if(features.features[i].properties.continent == 'América del Sur') {
      // Solo del pais, provincia seleccionados
      if(country == features.features[i].properties.country && region == features.features[i].properties.region) {
        // Solo de la ciudad seleccionada
        if(county != '') {
          if(county == features.features[i].properties.county)
            direcciones.push(i);
        } else {
          direcciones.push(i);
        }
        count = count + 1;
      }
    }
    // Mostramos solo 10 resultados
    if(count >= 10)
      break;
  }
  if(direcciones.length <= 0) {
    $('.reco-message').html("La dirección no se pudo geolocalizar, revisa los datos o marcala haciendo click en el mapa.");
    $('.recomendacion').show("slow");
  } else {
    if(direcciones.length == 1) {
      var lon = features.features[direcciones[0]].geometry.coordinates[0];
      var lat = features.features[direcciones[0]].geometry.coordinates[1];
      add_map_marker(lon, lat, 17);
    } else {
      for(i = 0; i < direcciones.length; i++) {
        var direccion = features.features[direcciones[i]].properties.name;
        direccion = direccion + ', ' + features.features[direcciones[i]].properties.county + ', ' + features.features[direcciones[i]].properties.region+ ', ' + features.features[direcciones[i]].properties.country;
        $('.opciones-geomap').append('<li><a href="javascript:add_map_marker(' + features.features[direcciones[i]].geometry.coordinates[0] + ', ' + features.features[direcciones[i]].geometry.coordinates[1] + ', 17);">' + direccion +'</a></li>');
        $('.opciones-geomap').show('slow');
      }
    }
  }
}
function requestFailure(response) {
  $('.reco-message').html("Error al comunicarse con sevicio OpenLS, marque haciendo click en el mapa.");
  $('.recomendacion').show("");
}

function formatear_texto_papel(texto, tipo) {
  if(tipo=='lineal' || tipo=='clasifoto') {
    texto = texto.replace(/\r?\n|\r/g, ""); // Removemos los saltos de línea.
    texto = texto.replace(/ +(?= )/g,''); // Removemos múltiples espacios
    var palabras = texto.split(' ');
    for(i=1; i<palabras.length; i++) {
      var palabra = palabras[i];
      if(palabra.length>3) {
        // Si la palabra es mayor a 3 caracteres y está en mayúscula la capitalizamos.
        palabra = palabra.charAt(0)+(palabra.substr(1)).toLowerCase();
        palabras[i] = palabra;
      }
    }
    texto = '';
    for(i=0; i<palabras.length; i++) {
      texto += palabras[i]+' ';
    }
    texto = texto.trim();
  }
  return texto;
}

function cambio_contenedor_imagenes() {
  if($('.media-cta.sin-imagenes').length >= 1) {
    $('.media-cta').removeClass('sin-imagenes');
    $('.media-cta').addClass('con-imagenes');
    $('.fa-camera').hide('');
  }
}

function cambio_menu_lateral(id){
  $( "#edit-tabs" ).tabs({
    active: id
  }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
  $('body').scrollTop(0);
}

function mostrar_precio_uva() {
  if($('#edit-field-aviso-precio-0-value').val() != '') {
    $.get( "/ajax/obtener_precios_uva/" + $('#edit-field-aviso-precio-0-value').val() + "/" + $('input[name=field_aviso_moneda[value]]:checked', '#node-form').val(), function( data ) {
      if(data != '' && data != 0)
        $('.mont-convertido-uva').html('<span class="monto">' + data + ' UVAS</span>');
      else
        $('.mont-convertido-uva').html('<span class="error">No se pudo calcular el precio UVA, intente mas tarde.</span>');        
    });
  }
}