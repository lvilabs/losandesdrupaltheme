/*

  ----------------------------------------------------------------------------------------------------
  Accessible News Slider
  ----------------------------------------------------------------------------------------------------

  Author:
  Brian Reindel

  Author URL:
  http://blog.reindel.com

  License:
  Unrestricted. This script is free for both personal and commercial use.

  20111017 1557 JFabian
  Se modifico el script para brindar una mejor funcionalidad en el nuevo Sitio de Autos.
  - Se quito el "View All".
  - Se corrigio el "siguiente" para que no permita hacer más clicks de lo necesario.
  - Se muestran siempre los botones "anterior" y "siguiente", pero solo funcionan cuando es necesario.

*/

jQuery.fn.accessNews = function( settings ) {
  settings = jQuery.extend({
    headline: "Top Stories",
    speed   : "normal",
    slideBy : 2
  }, settings);
  return this.each(function() {
    jQuery.fn.accessNews.run( jQuery( this ), settings );
  });
};
jQuery.fn.accessNews.run = function( $this, settings ) {
  jQuery( ".javascript_css", $this ).css( "display", "none" );
  var ul = jQuery( "ul.Slider:eq(0)", $this );
  var li = ul.children();
  if ( li.length > settings.slideBy ) {
    var $next = jQuery( ".next span.slide-button", $this );
    var $back = jQuery( ".back span.slide-button", $this );
    var liWidth = jQuery( li[0] ).outerWidth(true);
    var liMaxWidth = liWidth * (li.length - 1);
    var animating = false;
    ul.width( (li.length * liWidth) );
    $next.click(function() {
      if ( !animating ) {
        animating = true;
        offsetLeft = parseInt( ul.css( "left" ) ) - ( liWidth * settings.slideBy );
        if ( offsetLeft + ul.width() > 0 ) {
          if ( parseInt( ul.css( "left" ) ) + liMaxWidth > 0 ) {
            ul.animate({
              left: offsetLeft
            }, settings.speed, function() {
              animating = false;
            });
          } else {
            animating = false;
          }
        } else {
          animating = false;
        }
      }
      return false;
    });
    $back.click(function() {
      if ( !animating ) {
        animating = true;
        offsetRight = parseInt( ul.css( "left" ) ) + ( liWidth * settings.slideBy );
        if ( offsetRight + ul.width() <= ul.width() ) {
          ul.animate({
            left: offsetRight
          }, settings.speed, function() {
            animating = false;
          });
        } else {
          animating = false;
        }
      }
      return false;
    });
    $back.css( "display", "block" );
    $next.css( "display", "block" );
  }
};