WebPage.OnClickFotoPrincipal = function(event) {
  event.preventDefault();
  var linkFoto = $(this);
  var fotoKey = linkFoto.attr('data-foto-key');
  var foto_original = $('#foto-oculta-'+fotoKey).attr('href');
  //$.colorbox({href:foto_original,rel:'gallery-ficha-oculta'});
  $('#foto-oculta-'+fotoKey).click();
}

WebPage.OnClickFotoChica = function(event) {
  event.preventDefault();
  var linkFoto = $(this);
  var fotoKey = linkFoto.attr('data-foto-key');
  var mediaContentId = linkFoto.attr('data-media-content-id');

  if(!fotoKey || !mediaContentId) {
    return false; //si no estan definidas las propiedades custom
  }

  var divAFP = $('#'+mediaContentId);
  var currentLink = divAFP.find('a.poplight');

  if(currentLink.attr('data-foto-key') == fotoKey) {
    return false;
  }

  currentLink.remove();
  divAFP.append(WebPage.FotosMedium[fotoKey]);
  $('a.poplight').click(WebPage.OnClickFotoPrincipal);
}

WebPage.SliderSettings = {
  headline : "Imágenes en total",
  speed : "slow",
  slideBy : 6
};
if(Drupal.settings.tipo_aviso == 'aviso_producto' || Drupal.settings.tipo_aviso == 'aviso_servicio' || Drupal.settings.tipo_aviso == 'aviso_rural')
  WebPage.SliderSettings.slideBy = 5; // Para los casos de producto, servicio o rural.
WebPage.SliderRenderedStatus = {};
WebPage.ShowMultimedia = function() {
  var solapaItem = $(this);
  var mediaContentClickedId   = solapaItem.attr('data-media-content-id');
  var solapas = $('div.Multimedia ul.Solapas li');
  var sliders = $('div.accessible_news_slider');
  
  solapas.each(function(index) {
    var solapaEl = $(this);
    var mediaContentId   = solapaEl.attr('data-media-content-id');
    var multimediaEl = $('#'+mediaContentId);
    if(mediaContentId == mediaContentClickedId) {
      solapaEl.addClass('Act');
      multimediaEl.show();
      if(mediaContentId == 'multimedia_mapas'){
        //Generamos el mapa si no está generado ya
        if($.trim(multimediaEl.html()) == ''){
          WebPage.GetMapaFicha();
        }
      }
    } else {
      solapaEl.removeClass('Act') ;
      multimediaEl.hide();
    }
  });

  sliders.each(function(index) {
    var sliderEl = $(this);
    if(sliderEl.attr('data-media-content-id') == mediaContentClickedId) {
      sliderEl.show();
      if(! WebPage.SliderRenderedStatus.hasOwnProperty(mediaContentClickedId)) {
        //hacemos render del slider del media solo una vez
        sliderEl.accessNews(WebPage.SliderSettings);
        WebPage.SliderRenderedStatus[mediaContentClickedId] = 'render-ok';
      }
    } else {
      if(!sliderEl.hasClass('siempre_visible'))
        sliderEl.hide();
    }
  });
}
WebPage.GetMapaFicha = function() {
  map = new OpenLayers.Map("multimedia_mapas");
  map.addLayer(new OpenLayers.Layer.OSM());
  var lonLat = new OpenLayers.LonLat( Clasificados.GmapSetingsLongitude ,Clasificados.GmapSetingsLatitude )
        .transform(
          new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
          map.getProjectionObject() // to Spherical Mercator Projection
        );
  var zoom = Clasificados.GmapSetingsZoom;
  var markers = new OpenLayers.Layer.Markers( "Markers" );
  map.addLayer(markers);
  markers.addMarker(new OpenLayers.Marker(lonLat));
  map.setCenter (lonLat, zoom);
  
  /*
  map = new OpenLayers.Map("multimedia_mapas");
  //var mapnik         = new OpenLayers.Layer.Stamen("toner-background");
  var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
  var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
  var position       = new OpenLayers.LonLat(Clasificados.GmapSetingsLongitude, Clasificados.GmapSetingsLatitude).transform( fromProjection, toProjection);
  var zoom           = Clasificados.GmapSetingsZoom; 

  //map.addLayer(mapnik);
  map.setCenter(position, zoom);

  var markers = new OpenLayers.Layer.Markers( "Markers" );
  map.addLayer(markers);

  var icon1 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);
  var icon2 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-gold.png', size, offset);
  var icon3 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-green.png', size, offset);

  var lonLat1 = new OpenLayers.LonLat(Clasificados.GmapSetingsLongitude, Clasificados.GmapSetingsLatitude).transform(
    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
    map.getProjectionObject() // to Spherical Mercator Projection
  );
  
  var marker1 = new OpenLayers.Marker(lonLat1, icon1);
  var size = new OpenLayers.Size(21,25);
  var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
  marker1.icon.size = size;
  marker1.icon.offset = offset;

  var feature = new OpenLayers.Feature(markers, lonLat1);
  feature.closeBox = true;
  feature.popupClass = OpenLayers.Class(OpenLayers.Popup.AnchoredBubble, { autoSize: true });
  feature.data.popupContentHTML = '<p>Marker1<p>';
  feature.data.overflow = "hidden";

  marker1.feature = feature;

  var markerClick = function (evt) {
    if (this.popup == null) {
      this.popup = this.createPopup(this.closeBox);
      map.addPopup(this.popup);
      this.popup.show();
    } else {
      this.popup.toggle();
    }
    OpenLayers.Event.stop(evt);
  };
  marker1.events.register("mousedown", feature, markerClick);

  markers.addMarker(marker1);

  // markers.addMarker(marker2, icon2);

  // markers.addMarker(marker3, icon3);
  */
  
}

jQuery(document).ready(function($){
  
  var nid = 0;
  if($('#aviso-nid').length>0) {
    nid = $('#aviso-nid').html();
  } else {
    var url = window.location.pathname.split("/");
    if(url[1] == 'node') {
      nid = url[2];
    } else {
      nid = url[3];
    }
  }
  
  $('div.Multimedia ul.Solapas li').click(WebPage.ShowMultimedia);
  if($('div.Multimedia ul.Solapas li').length > 0) {
    $('div.Multimedia ul.Solapas li')[0].click(); //hacemos click en la primer solapa.
  } else {
    $('.accessible_news_slider').accessNews(WebPage.SliderSettings);
  }
  var cant_anterior = WebPage.SliderSettings.slideBy;
  WebPage.SliderSettings.slideBy = 2;
  $('#block-clasificados-datos_vendedor').accessNews(WebPage.SliderSettings);
  WebPage.SliderSettings.slideBy = cant_anterior;
  $('a.poplight').click(WebPage.OnClickFotoPrincipal);

  $('a.linkFotoChica').click(WebPage.OnClickFotoChica);
  
  var recomendarForm = $('#recomendar-amigo-entry-form');
  if(recomendarForm.length > 0) {
    $('div.Herramientas > span.iconos > a.enviar').click(function(event) {
      event.preventDefault();
      $('#recomendar-amigo-information-wrapper').html('');
      recomendarForm.find(':text').val('');
    });
  }
  var reportarAbusoForm = $('#abuso-reporte-form');
  if(reportarAbusoForm.length > 0) {
    $('div.Herramientas > span.iconos > a.enviar').click(function(event) {
      event.preventDefault();
      $('#reportar-abuso-information-wrapper').html('');
      reportarAbusoForm.find(':text').val('');
    });
  }
  
  var tooltip = $('.FichaAuto .Marco .EspecificacionesA .denuncia .alerta');
  
  $('.abuso').mousemove(function(e) { 
    var x = e.clientX, y = e.clientY;
    tooltip.css('top', (y - 35) + 'px');
    tooltip.css('left', (x + 20) + 'px');
    tooltip.css('display', 'block');
  });
  $('.abuso').mouseout(function() { 
    tooltip.css('display', 'none');
  });
  
  // Nuevo Colorbox
  $('.abuso').click(function(){
    if($("#content-formulario-abuso").length != 0) {
      $.colorbox({
        href:'#reportar-abuso-form',
        inline: true,
        width: 440,
      });
      if($("#abuso-reporte-form").length != 0) {
        // Ya se cargo el html.
        return;
      }
      $.ajax({
        url: '/ajax/carga-formulario_abuso/' + nid,
        dataType: 'html',
        success: function(data) {
          $("#content-formulario-abuso").html(data);
          Drupal.attachBehaviors();
          var t = window.location.pathname.substr(1);
          $("#abuso-reporte-form").attr("action", "/" + t + "?rc=1");
          $.colorbox.resize();
        }
      });
    } else {
      $.colorbox({
        href:'#reportar-abuso-form',
        inline: true,
        width: 440,
      });
    }
  });
  $('.enviar-mail').click(function(){
    if($("#content-formulario-recomendar_amigo").length != 0) {
      $.colorbox({
        href:'#recomendar-amigo-form',
        inline: true,
        width: "520"
      });
      if($("#content-formulario-recomendar_amigo form").length != 0) {
        // Ya se cargo el html.
        return;
      }
      $.ajax({
        url: '/ajax/carga-formulario-recomendar-amigo/' + nid,
        dataType: 'html',
        success: function(data) {
          $("#content-formulario-recomendar_amigo").html(data);
          Drupal.attachBehaviors();
          var t = window.location.pathname.substr(1);
          $("#recomendar-amigo-form").attr("action", "/" + t + "?rc=1");
          $.colorbox.resize();
        }
      });
    } else {
      $.colorbox({
        href:'#recomendar-amigo-form',
        inline: true,
        width: "520"
      });
    }
  });
    
  //Frmulario de contacto flotante
 /* var sum_img_contacto = 0;
  var sum_banner_top = 0;
  var sum_cabezal_micrositio = 0;
  //Si hay banner, muevo el top del formaulrio
  if($(".region-page-arriba") != undefined && $(".region-page-arriba").height() != null){
    $(".CR").css('margin-top', 360);
    sum_banner_top = $(".region-page-arriba").height();
  }
  //Si tiene logo de la empresa, le sumo el alto
  if($("#block-contactar_vendedor-formulario_contacto img") != undefined && $("#block-contactar_vendedor-formulario_contacto img").height() != null)
    sum_img_contacto = $("#block-contactar_vendedor-formulario_contacto img").height();
  //Si tiene cabezal de micrositios, le sumo el alto
  if($("#content-cabezal") != undefined && $("#content-cabezal").height() != null){
    sum_cabezal_micrositio = $("#content-cabezal").height();
    var sum_top_formulario = sum_cabezal_micrositio + 14;
    $('#block-contactar_vendedor-formulario_contacto').css('top', sum_top_formulario + 'px');
    $('#block-block-53').css('margin-top', sum_cabezal_micrositio + 'px');
  }
  $.lockfixed(".region-page-columna-derecha", {
    offset: {
      top: -90 - sum_cabezal_micrositio, //-90
      bottom: $(".pie").height() + sum_img_contacto + sum_banner_top + sum_cabezal_micrositio  + 360, //360
      forcemargin: true
    }
  });
  */
  $.ajax({
    url: '/ajax/publicacion/contador_visitas/' + nid,
    dataType: 'html',
    success: function(data) {
      $("#contador-visitas").html(data);
    }
  });
/*  $.ajax({
    url: '/ajax/publicacion/ofertas_similares/' + nid,
    dataType: 'html',
    success: function(data) {
      $("#block-publicacion_avisos-ofertas_similares").html(data);
    }
  });
*/
  $('.gallery-ficha-oculta').colorbox({ rel:'gallery-ficha-oculta', title: ''});
  
  $('.link-contacto-vendedor').click(function(){
    $.colorbox({
      href:'#modal-contacto-vendedor',
      inline: true,
    });
  });
  $('.link-saber-mas').click(function(){
    $.colorbox({
      href:'#modal-saber-mas',
      inline: true,
      width: 700
    });
  });
  $('.link-datos-comercio').click(function(){
    $.colorbox({
      href:'#modal-datos-comercio',
      inline: true,
      width: 700
    });
  });
  $('.link-forma-envio').click(function(){
    $.colorbox({
      href:'#modal-forma-envio',
      inline: true,
      width: 800
    });
  });
  $('.link-calculadora-cuotas').click(function(){
    $.colorbox({
      href:'#calculadora-cuotas-form',
      inline: true,
      width: 700,
      height: 500,
      scrolling: false
    });
  });
  
  if($('.precio-uva').length > 0) {
    ficha_mostrar_precio_uva();
  }
  
});

WebPage.FotosMedium = new Array();

function ficha_mostrar_precio_uva() {
  if($('.precio-visible span').attr('content') != '' && $('.precio-visible span').attr('content') != 'consultar') {
    var moneda = 1;
    if($('.ContentPrecio meta').attr('content') == 'USD')
      moneda = 2;
    $.get( "/ajax/obtener_precios_uva/" + $('.precio-visible span').attr('content') + "/" + moneda, function( data ) {
      if(data != '' && data != '0,00')
        $('.precio-uva').html(data + ' UVAS');
      else
        $('.precio-uva').html('');        
    });
  }
}