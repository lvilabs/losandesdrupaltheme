function mostrar_modal_reserva(){
  
  var datos = { checkin: $('#DOPBCPCalendar-check-in0').val(), 
                checkout: $('#DOPBCPCalendar-check-out0').val(),
              };
  
  $.ajax({
    method: "GET",
    data: datos,
    url: "/clasificados/ajax/usuario-logueado",
    }).done(function(data) {
      if(data) {
        $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('position', 'relative');
        $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('top', '0px');
        $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('margin-left', '0px');
        $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('width', '94%');
        $('#DOPBCPCalendar-sidebar-column-wrapper-2-0').show('');
        $('.solicitud-reserva-confirmar').hide('');
        $('.btn-vendedor').hide('');
        $('.DOPBCPCalendar-sidebar').addClass('colorboxOpen');
        $.colorbox({
          href:'.DOPBCPCalendar-sidebar',
          inline: true,
          width: "680px",
          onComplete:function(){
            topLateral = $(".booking-section").position().top;
            initLeftLateral = $(".booking-section").position().left;
            if(initLeftLateral >= 900) leftLateral = initLeftLateral - 530;
            if(initLeftLateral < 900) leftLateral = initLeftLateral - 550;
            if(topLateral <= 206.5) topLateral = 133.5;
            $(".bloqueo-transparencia").css('top', topLateral+'px');
            $(".bloqueo-transparencia").css('left', leftLateral+'px');
            $(".bloqueo-transparencia").show('');
          },
          onClosed:function(){
            $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('position', 'absolute');
            
            var topLateral = $(".booking-section").position().top;
            var topTotal = topLateral + 98;
            if(topLateral == 406.5) topTotal = 304.5;
            if(topLateral == 110) topTotal = 402.5;
            $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('top', topTotal+'px');
                        
            $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('left', 'initial');
            $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('margin-left', '681px');
            $('.DOPBCPCalendar-sidebar.dopbcp-style1-medium').css('width', '309px');
            $('#DOPBCPCalendar-sidebar-column-wrapper-2-0').css('display','none');
            $('.solicitud-reserva-confirmar').show('');
            $('.btn-vendedor').show('');
            $('.DOPBCPCalendar-sidebar').removeClass('colorboxOpen');
            $(".bloqueo-transparencia").hide('');
          }
        });
      } else {
        $.colorbox({
          href:'#block-user-0',
          inline: true,
          width: "30%"
        });
      }
  });
}

function mostrar_modal_contactos(){
  $('#block-contactar_vendedor-formulario_contacto').show('');
  $.colorbox({
    href:'#block-contactar_vendedor-formulario_contacto',
    inline: true,
    width: "30%",
    height: "600px",
    onClosed:function(){
      $('#block-contactar_vendedor-formulario_contacto').css('display','none');
    }
  });
}

function mostrar_modal_confirmacion(){
  $.colorbox({
    href:'#modal_confirmacion',
    inline: true,
    width: "50%"
  });
}

function redimensionar_alto_reserva(solicitud = 0){
  var contenido = $('.DOPBCPCalendar-sidebar').css('height');
  if(solicitud) {
    var val_cont = contenido.split('px');
    total = parseInt(val_cont[0]) + 60;
    contenido = total+'px';
  }
  $('.booking-section .extras').css('height', contenido);
}

jQuery(document).ready(function($){
  
  if(typeof($.lockfixed) != 'undefined') {
    $.lockfixed(".booking-section", {
      offset: {
        top: 10,
        bottom: 0, 
        forcemargin: true
      }
    });
    $(document).scroll(function() {
      if($('.DOPBCPCalendar-sidebar.colorboxOpen').length == 0) {
        topLateral = $(".booking-section").position().top;
        topTotal = topLateral + 98;
        if(topLateral == 406.5) topTotal = 304.5;
        if(topLateral == 110) topTotal = 402.5;
        $('.DOPBCPCalendar-sidebar').css('top', topTotal+'px');
      }
    });
  }
  
  var tooltipCamas = $('.tooltips-camas');
  $('.seccion-camas').mousemove(function(e) { 
    tooltipCamas.css('display', 'block');
  });
  $('.seccion-camas').mouseout(function() { 
    tooltipCamas.css('display', 'none');
  });
  
});

