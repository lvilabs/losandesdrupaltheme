/** 
* Actualizar el monto a pagar
* 
* @param int paqueteId Id del paquete
* 
*/
function actualizar_monto(paqueteId){
  var monto = $('.precio-'+paqueteId).val();
  $('.costo label').html('$ '+monto);
}

jQuery(document).ready(function($){
  $('.productos .form-radio').click(function(){
    actualizar_monto($(this).val());
  });

  var divPaquetes = $('.ContentProd .paquete');
  divPaquetes.hide();

  $('.ContentProd .select-categoria').click(function(event){
    event.preventDefault();
    var clicked = $(this);
    var classes = clicked.attr('class').split(' ');
    $.each(classes, function(index, value) {
      if(value == 'select-categoria') {
        return 1; //skip this value and continue
      }
      divPaquetes.each(function() {
        if($(this).hasClass(value)) {
          if(this.style.display && this.style.display == 'none') {
            $(this).show();
          } else {
            $(this).hide();
          }
          return false;
        }
      });
    });
  });

})