var form_was_submited = false;
telefonoFormValidate = function(phone){
  var valid = false;
  //var matches = array();
  var numbers = phone.match(/\d/g);
  if(!numbers){
    valid = true;
  } else {
    if(numbers.length < 4) {
      //no es valido porque el número es muy corto (no tiene la cantidad necesaria de numeros)
    } else if(phone.match(/^[0-9 \-\(\)\.\+\/\*#]+$/)) {
      if(phone.match(/[\-\(\)\.\+\/\*#]{3,}/)) {
        //no es valido porque tiene más de 3 caracteres especiales juntos.
      } else {
        var max_allowed = numbers - 2;
        valid = true;
      }
    }
  }
  return valid;
};
mailFormValidate = function(mail){
  var valid = false;
  if(mail.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/))
    valid = true;
  return valid;
};
mensajeFormSubmit = function(status, msg){
  $(".form-mensaje").removeClass('error', 'ok');
  if(status == 'error')
    $(".form-mensaje").addClass('error');  
  else
    $(".form-mensaje").addClass('ok');    
  $(".form-mensaje").html(msg);
  $(".form-mensaje").show();
};
ajaxEnvioContacto = function () {
  
  var ofertas_similares = 0;
  
  var datos_post = {
    contactar_vendedor_nombre: $("#edit-contactar-vendedor-nombre").val(),
    contactar_vendedor_telefono: $("#edit-contactar-vendedor-telefono").val(),
    contactar_vendedor_mail: $("#edit-contactar-vendedor-mail").val(),
    contactar_vendedor_consulta: $("#edit-contactar-vendedor-consulta").val(),
    contactar_vendedor_aviso: $("#edit-contactar-vendedor-aviso").val(),
    contactar_vendedor_estado: $("#edit-contactar-vendedor-estado").val(),
    contactar_vendedor_name: $("#edit-contactar-vendedor-name").val(),
    contactar_vendedor_ofertas_similares: ofertas_similares
  }
  if($("#contactar-vendedor-formulario #edit-captcha-sid").length>0) {
    datos_post.captcha_sid = $("#contactar-vendedor-formulario #edit-captcha-sid").val();
    datos_post.captcha_response = $("#contactar-vendedor-formulario #edit-captcha-response").val();
    datos_post.captcha_token = $("#contactar-vendedor-formulario #edit-captcha-token").val();
  }
  if($('#contactar-vendedor-formulario .g-recaptcha').length>0) {
    datos_post['g-recaptcha-response'] = $('#g-recaptcha-response').val();
  }
  $("#contactar-vendedor-formulario input").removeClass('error');
  $("#contactar-vendedor-formulario textarea").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").html('');
  $.ajax({
      type: "POST",
      data: datos_post,
      dataType: "json",
      cache: false,
      url: "/ajax/carga-formulario_contacto/submit",
      success: function (data) {
        if(data.error) {
          for(i=0; i<data.error_campos.length; i++) {
            $("#contactar-vendedor-formulario [name="+data.error_campos[i]+"]").addClass('error');
          }
          mensajeFormSubmit('error', data.mensaje);
          $("#edit-contactar-vendedor-submit").attr("value", "Enviar");
        } else {
          mensajeFormSubmit('ok', data.mensaje);
          $("#edit-contactar-vendedor-submit").attr("value", "Enviar");
          $("#edit-contactar-vendedor-submit").remove();
        }
        form_was_submited = false;
      },
      error: function (e, t, n) {
        if (t === "timeout") {
          //mensaje_modal("body", "El servidor no responde, intente más tarde. Gracias.");
          mensajeFormSubmit('error', 'El servidor no responde, intente más tarde. Gracias.');
          form_was_submited = false;
          $("#edit-contactar-vendedor-submit").attr("value", "Enviar");
        } else {
          mensajeFormSubmit('error', 'Ocurrió un error al procesar la petición, intente más tarde. Gracias.');
          form_was_submited = false;
          $("#edit-contactar-vendedor-submit").attr("value", "Enviar");
        }
      }
  })
};

jQuery(document).ready(function($){
  
  bind_contactar_vendedor_form_submit();
  bind_ver_telefono_click();
  bind_contacto_whatsapp_click();
  bind_enviar_mail_click();
  
  //Carga de formulario de contacto via ajax
  if($("#content-formulario-contacto").length != 0){
    
    var nid = 0;
    if($('#aviso-nid').length>0) {
      nid = $('#aviso-nid').html();
    } else {
      var url = window.location.pathname.split("/");  
      if(url[1] == 'node') {
        nid = url[2];
      } else {
        nid = url[3];
      }
    }
    
    $.ajax({
      url: '/ajax/carga-formulario_contacto/' + nid,
      dataType: 'html',
      success: function(data) {
        var origin = window.location.origin + '/ajax/carga-formulario_contacto/' + nid;
        var href = window.location.href;
        data = data.replace(origin, href);
        $("#content-formulario-contacto").html(data);
        Drupal.attachBehaviors();
        var t = window.location.pathname.substr(1);
        $("#contactar-vendedor-formulario").attr("action", "/" + t + "?rc=1");
        bind_contactar_vendedor_form_submit();
        bind_ver_telefono_click();
        bind_contacto_whatsapp_click();
        bind_enviar_mail_click();
      }
    });
  }
});

function bind_ver_telefono_click() {
  $('#ver_telefono').click(function() {
    var nid = $('#edit-contactar-vendedor-aviso').val();
    $.ajax({
      url: '/contacto/telefono-visto/'+nid+'/contacto_telefono',
      dataType: 'json',
      success: function(data) {
      }
    });  
    $('#ver_telefono').hide();
    $('#telefono_vendedor').fadeIn(2000);
    return false;
  });
}

function bind_contacto_whatsapp_click() {
  $('.botonC.whatsapp').click(function() {
    var win = window.open($(this).attr('href'), '_blank');
    win.focus();
    var nid = $('#edit-contactar-vendedor-aviso').val();
    $.ajax({
      url: '/contacto/telefono-visto/'+nid+'/contacto_whatsapp',
      dataType: 'json',
      success: function(data) {
      }
    });
    return false;
  });
}

function bind_enviar_mail_click() {
  $('#envio_mail_vendedor').click(function() {
    var nid = $('#edit-contactar-vendedor-aviso').val();
    $.ajax({
      url: '/contacto/telefono-visto/'+nid+'/contacto_mail',
      dataType: 'json',
      success: function(data) {
      }
    });
  });
}

function bind_contactar_vendedor_form_submit() {
  //Envio de formulario via ajax
  $("#contactar-vendedor-formulario-ajax, #contactar-vendedor-formulario").submit(function (e) {
    $(".form-mensaje").hide();
    // Limpiamos clase error de campos
    $("#edit-contactar-vendedor-nombre").removeClass('error');
    $("#edit-contactar-vendedor-telefono").removeClass('error');
    $("#edit-contactar-vendedor-mail").removeClass('error');
    $("#edit-contactar-vendedor-consulta").removeClass('error');
    
    //Validacion de campos
    var envio = true;
    var mensaje = '';
    if ($("#edit-contactar-vendedor-nombre").val() == ""){
      $("#edit-contactar-vendedor-nombre").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if($("#edit-contactar-vendedor-telefono").val() == "" && $("#edit-contactar-vendedor-mail").val() == ""){
      $("#edit-contactar-vendedor-telefono").addClass('error');
      $("#edit-contactar-vendedor-mail").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    } else {
      if($("#edit-contactar-vendedor-telefono").val() != "") {
        if(telefonoFormValidate($("#edit-contactar-vendedor-telefono").val()) === false){
          $("#edit-contactar-vendedor-telefono").addClass('error');
          mensaje = 'Campo no válido';
          envio = false;
        }
      }
      if($("#edit-contactar-vendedor-mail").val() != "") {
        if(mailFormValidate($("#edit-contactar-vendedor-mail").val()) === false){
          $("#edit-contactar-vendedor-mail").addClass('error');
          mensaje = 'Campo no válido';
          envio = false;
        }
      }
    }
    if($("#edit-contactar-vendedor-consulta").val() == "") {
      $("#edit-contactar-vendedor-consulta").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if($("#edit-captcha-response").length>0 && $("#edit-captcha-response").val() == "") {
      $("#contactar-vendedor-formulario input[name=captcha_response]").addClass('error');
      mensaje = 'Campos requeridos';
      envio = false;
    }
    if(envio == false) {
      mensajeFormSubmit('error', mensaje);
      return false;
    } else {
      if (!form_was_submited) {
        form_was_submited = true;
        $("#edit-contactar-vendedor-submit").attr("value", "Enviando...");
        ajaxEnvioContacto();
      }
    }
    e.preventDefault();
  });
  $('#edit-contactar-vendedor-nombre').click(function() {
    $("#edit-contactar-vendedor-nombre").removeClass('error');
  });
  $('#edit-contactar-vendedor-telefono').click(function() {
    $("#edit-contactar-vendedor-telefono").removeClass('error');
  });
  $('#edit-contactar-vendedor-mail').click(function() {
    $("#edit-contactar-vendedor-mail").removeClass('error');
  });
  $('#edit-contactar-vendedor-consulta').click(function() {
    $("#edit-contactar-vendedor-consulta").removeClass('error');
  });
  if($('#edit-captcha-response').length>0) {
    $('#edit-captcha-response').click(function() {
      $('#edit-captcha-response').removeClass('error');
    });
  }

  if(jQuery.support.placeholder) {
    return;
  }

  var i = 0;
  var theForm = $('#block-contactar_vendedor-formulario_contacto form');
  var labels = $('#block-contactar_vendedor-formulario_contacto label');
  var theLabel = null;
  var theText  = '';
  var theInput = null;
  for(i=0; i <labels.length; i++) {
    theLabel = $(labels[i]);
    //theText  = theLabel.text().replace(/\:( )?(\*)?/g, '');
    theInput = theLabel.next('input');

    if(theInput.length <= 0) {
      //El "label" de Consulta es el único que no tiene un "input" como "next", sino que tiene dos "textarea" (el mismo textarea).
      theInput = theForm.find('textarea');
    }
    theText  = theInput.attr('placeholder');

    theInput.attr('title', theText);
    if(theInput.val() == '') {
      theInput.val(theText);
    }
    theInput.focus(function(){
      var thisInput = $(this);
      if(thisInput.val() == thisInput.attr('placeholder')) {
        thisInput.val('');
      }
    });
    theInput.blur(function(){
      var thisInput = $(this);
      if(thisInput.val() == '') {
        thisInput.val(thisInput.attr('placeholder'));
      }
    });
  }

  var theInputs = null;
  theForm.submit(function() {
    theInputs = theForm.find('input');
    for(i=0; i < theInputs.length; i++) {
      theInput = $(theInputs[i]);
      if(theInput.val() == theInput.attr('placeholder')) {
        theInput.val('');
      }
    }
    theInputs = theForm.find('textarea');
    for(i=0; i < theInputs.length; i++) {
      theInput = $(theInputs[i]);
      if(theInput.val() == theInput.attr('placeholder')) {
        theInput.val('');
      }
    }
    return true;
  });
}