var avisoDiv = '';
var cantidadAvisos = 0;
var cantidadAvisosDer = 0;
var addMix = 0;
var addClassPar = 0;
var classContenedor = '';
$(document).ready(function() {
  avisoDiv = $('div.CajaAviso');
  
  var sliders = $('div.slider_tienda');
  sliders.each(function(index) {
    var sliderEl = $(this);
    Losandes.SliderSettings.slideBy = 3;
    sliderEl.accessNews(Losandes.SliderSettings);
  });
  Losandes.DestacadosRequestData();
});
var Losandes = Losandes || {};
Losandes.SliderSettings = {
  headline : "Productos en total",
  speed : "slow",
  slideBy : 2
};
Losandes.DestacadosRequestData = function () {
  if(Losandes.DestacadosProductos) {
    if(Losandes.DestacadosProductos!==null) {
      var f = new Date();
      for (var i in Losandes.DestacadosProductos) {
        var item = Losandes.DestacadosProductos[i];
        cantidadAvisos = item.cantidad_avisos;
        var t = 1;
        if(cantidad_js > 1) {
          t = Math.floor((Math.random()*cantidad_js)+1);
        }
        //var reemplazo = item.block+'_'+t;
        var pattern_js = item.pattern_js;
        var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
        $.ajax({
          url: Uri,
          dataType: 'script',
          cache: true,
          async: false,
          success: function(data) {
            addMix = 0;
            addClassPar = 0;
            classContenedor = item.class;
            if(item.slide) {
              Losandes.CargarDestacadosSlide();
              setTimeout("Losandes.CargarSlide('div.slider_ofertas ."+item.class+"', "+item.cantidad_mostrar+");", 2000);
            } else {
              Losandes.CargarDestacados();
            }
          },
          error: function(){
            avisoDiv.show();
          }
        });
      }
    }
  }
  if(!Losandes.DestacadosConfig) {
    return;
  }
  if(Losandes.DestacadosConfig.pattern_js_ppal != undefined){
    var cantidad_js = Losandes.DestacadosConfig.cantidad_js_ppal || 0;
  } else {
    var cantidad_js = Losandes.DestacadosConfig.cantidad_js || 0;
  }
  
  if(cantidad_js <= 0) {
    return;
  }
  var t = 1;
  if(cantidad_js > 1) {
    t = Math.floor((Math.random()*cantidad_js)+1);
  }
  
  var f = new Date();
  
  if(Losandes.DestacadosConfig.block == 'principal'){
    cantidadAvisos = 7;
    var pattern_js_ppal = Losandes.DestacadosConfig.pattern_js_ppal;
    var Uri = pattern_js_ppal.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        addMix = 0;
        addClassPar = 0;
        classContenedor = 'contentAvisos';
        Losandes.CargarDestacados();
        Losandes.DestacadosMix();
      },
      error: function(){
        avisoDiv.show();
      }
    });
  } else if(Losandes.DestacadosConfig.block == 'inmuebles') {
    cantidadAvisos = 3;
    var pattern_js = Losandes.DestacadosConfig.pattern_js;
    var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        addClassPar = 0;
        classContenedor = 'contentAvisos';
        Losandes.CargarDestacados();
        //Emprendimientos
        if(Losandes.DestacadosConfigEmp) {
          Losandes.DestacadosEmprendimiento();
        }
        //Departamentos
        if(Losandes.DestacadosConfigDpto) {
          Losandes.DestacadosDepartamento();
        }
        //Alquielres Temporales
        if(Losandes.DestacadosConfigAlqTmp) {
          Losandes.DestacadosAlqTmp();
        }
      },
      error: function(){
        avisoDiv.show();
      }
    });
  } else {
    cantidadAvisos = 6;
    var pattern_js = Losandes.DestacadosConfig.pattern_js;
    var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        addClassPar = 1;
        classContenedor = 'contentAvisos';
        Losandes.CargarDestacados();
      },
      error: function(){
        avisoDiv.show();
      }
    });
  }
}
Losandes.DestacadosMix = function () {
    cantidadAvisos = 3;
    var cantidad_js_mix = Losandes.DestacadosConfig.cantidad_js_mix || 0;
    if(cantidad_js_mix <= 0) {
      $('div.CajaAviso:even').addClass('Par');
      return;
    }
    var t = 1;
    if(cantidad_js_mix > 1) {
      t = Math.floor((Math.random()*cantidad_js_mix)+1);
    }
    var f = new Date();
    var pattern_js_mix = Losandes.DestacadosConfig.pattern_js_mix;
    var Uri = pattern_js_mix.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        addMix = 1;
        addClassPar = 1;
        classContenedor = 'contentAvisos';
        Losandes.CargarDestacados();
      }
    });
}
Losandes.DestacadosEmprendimiento = function () {
    cantidadAvisos = 3;
    var cantidad_js = Losandes.DestacadosConfigEmp.cantidad_js || 0;
    if(cantidad_js <= 0) {
      $('div.CajaAviso:even').addClass('Par');
      return;
    }
    var t = 1;
    if(cantidad_js > 1) {
      t = Math.floor((Math.random()*cantidad_js)+1);
    }
    var f = new Date();
    var pattern_js = Losandes.DestacadosConfigEmp.pattern_js;
    var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        classContenedor = 'contentAvisosEmp';
        Losandes.CargarDestacados();
      }
    });
}
Losandes.DestacadosDepartamento = function () {
    cantidadAvisos = 3;
    var cantidad_js = Losandes.DestacadosConfigDpto.cantidad_js || 0;
    if(cantidad_js <= 0) {
      $('div.CajaAviso:even').addClass('Par');
      return;
    }
    var t = 1;
    if(cantidad_js > 1) {
      t = Math.floor((Math.random()*cantidad_js)+1);
    }
    var f = new Date();
    var pattern_js = Losandes.DestacadosConfigDpto.pattern_js;
    var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        classContenedor = 'contentAvisosDpto';
        Losandes.CargarDestacados();
      }
    });
}
Losandes.DestacadosAlqTmp = function () {
    cantidadAvisosDer = 2;
    var cantidad_js = Losandes.DestacadosConfigAlqTmp.cantidad_js || 0;
    if(cantidad_js <= 0) {
      $('div.CajaAviso:even').addClass('Par');
      return;
    }
    var t = 1;
    if(cantidad_js > 1) {
      t = Math.floor((Math.random()*cantidad_js)+1);
    }
    var f = new Date();
    var pattern_js = Losandes.DestacadosConfigAlqTmp.pattern_js;
    var Uri = pattern_js.replace("<ORDINAL>", t)+'?f='+f.getDate() + (f.getMonth() +1) + f.getFullYear();
    $.ajax({
      url: Uri,
      dataType: 'script',
      cache: true,
      success: function(data) {
        classContenedor = 'contentAvisosAlqTmp';
        Losandes.CargarDestacadosDer();
      }
    });
}
Losandes.CargarDestacados = function () {
    
  if(!Losandes.AvisosData) {
    avisoDiv.show();
    return;
  }
  data = Losandes.AvisosData;
  var avisosEl = $('div.'+classContenedor);
  var url_path = data[0].url_path;
  
  //Randoms
  var rndN = 0;
  var avisos_rnd = [];
  var randoms_values = {}, property_name = '';
  var v = 0, i = 0, m = 0;
  if(data.length && data.length > cantidadAvisos+1) {
    m = cantidadAvisos;
  } else {
    avisoDiv.show();
    m = 0; //no hago random ya que no hay suficientes avisos
    return;
  }
  while(avisos_rnd.length < m){
    rndN = Math.floor((Math.random()*(data.length-1))+1);
    property_name = 'custom_' + rndN;
    if(! randoms_values.hasOwnProperty(property_name)) {
      avisos_rnd[avisos_rnd.length] = rndN;
      randoms_values[property_name] = rndN;
    }
  }
    
  if(addMix == 0)
    avisosEl.children().remove();

  for(i=0; i<avisos_rnd.length; i++) {
    v = avisos_rnd[i];
    data[v].url_path = url_path;
    if(addMix == 1){
      var number = Math.floor(Math.random() * $('div.CajaAviso').length);
      $('div.CajaAviso:eq('+number+')').hide().after(Losandes.EstructuraAvisoDestacado(data[v])).fadeIn(1000);
    } else {
      avisosEl.hide().append(Losandes.EstructuraAvisoDestacado(data[v])).fadeIn(1000);
    }
  }
  if(addClassPar == 1)
    $('div.CajaAviso:even').addClass('Par');
}
Losandes.EstructuraAvisoDestacado = function (aviso) {
  var kilometraje = "";
  var anio = "";
  var combustible = "";
  if(aviso.usado != undefined){
    if(aviso.usado == "0 Km"){
      kilometraje = aviso.usado;
    } else {
      if(aviso.kilometros != "") {
        kilometraje = aviso.kilometros + " Km";
      }
    }
    anio = aviso.anio;
    combustible = aviso.combustible;
  }
  var imagcache_class = 'ficha_aviso_314_211';
  var width = 314;
  var height = 211;
  var content_class = '';
  if(Losandes.DestacadosConfig) {
    if(Losandes.DestacadosConfig.block == 'inmuebles') {
      var imagcache_class = 'ficha_aviso_mobile_220_148';
      var width = 220;
      var height = 148;
      var content_class = 'new';
      aviso.url += '?cx_level=detacados_inmuebles';
    }
    if(Losandes.DestacadosConfig.block == 'autos'){
      aviso.url += '?cx_level=detacados_autos';
    }
    if(Losandes.DestacadosConfig.block == 'principal'){
      aviso.url += '?cx_level=detacados_home';
    }
    if(Losandes.DestacadosConfig.block == 'productos') {
      var imagcache_class = 'imagecache-ficha_aviso_200_133_sc';
      var width = 200;
      var height = 133;
    }
  }
  var htmlCompra = '';
  var htmlCarrito = '';
  if(aviso.disponible_venta==1) {
    htmlCompra = '<a href="/comprar/'+aviso.nid+'/datos" class="comprar">Comprar</a>';
    htmlCarrito = '<span class="Carrito"></span>';
  }
  var url_split = aviso.url;
  var alt_img = url_split.split("/");
  alt_img = alt_img[0]+' de '+alt_img[1]+' '+aviso.titulo+' a '+aviso.precio;
  var htmlAviso = [
    '<div class="CajaAviso '+content_class+'">',
    htmlCarrito,
    ' <a href="'+aviso.url+'" title="'+aviso.titulo+'">',
    '   <div class="imgAviso">',
    '    <img src="'+aviso.foto+'" alt="'+alt_img+'" title="'+aviso.titulo+'" class="imagecache imagecache-'+imagcache_class+'" width="'+width+'" height="'+height+'">',
    '   </div>',
    '   <div class="AvisoDescripcion">',
    '     <h4>'+aviso.titulo+'</h4>',
    '     <p>'+anio+' '+kilometraje+' '+combustible+'</p>',
    '     <span class="precio">'+aviso.precio+'</span>',
    htmlCompra,
    '   </div>',
    ' </a>',
    '</div>',
    ''
  ].join("\n");
  return htmlAviso;
}
/** DESTACADOS COLUMNA DERECHA */
Losandes.CargarDestacadosDer = function () {
    
  if(!Losandes.AvisosData) {
    avisoDiv.show();
    return;
  }
  data = Losandes.AvisosData;
  var avisosEl = $('div.'+classContenedor);
  var url_path = data[0].url_path;
  
  //Randoms
  var rndN = 0;
  var avisos_rnd = [];
  var randoms_values = {}, property_name = '';
  var v = 0, i = 0, m = 0;
  if(data.length && data.length > cantidadAvisosDer+1) {
    m = cantidadAvisosDer;
  } else {
    avisoDiv.show();
    m = 0; //no hago random ya que no hay suficientes avisos
    return;
  }
  while(avisos_rnd.length < m){
    rndN = Math.floor((Math.random()*(data.length-1))+1);
    property_name = 'custom_' + rndN;
    if(! randoms_values.hasOwnProperty(property_name)) {
      avisos_rnd[avisos_rnd.length] = rndN;
      randoms_values[property_name] = rndN;
    }
  }
    
  if(addMix == 0)
    avisosEl.children().remove();

  for(i=0; i<avisos_rnd.length; i++) {
    v = avisos_rnd[i];
    data[v].url_path = url_path;
    if(addMix == 1){
      var number = Math.floor(Math.random() * $('div.CajaAviso').length);
      $('div.CajaAviso:eq('+number+')').hide().after(Losandes.EstructuraAvisoDestacado(data[v])).fadeIn(1000);
    } else {
      avisosEl.hide().append(Losandes.EstructuraAvisoDestacadoDer(data[v])).fadeIn(1000);
    }
  }
  if(addClassPar == 1)
    $('div.CajaAviso:even').addClass('Par');
}
Losandes.EstructuraAvisoDestacadoDer = function (aviso) {
  var kilometraje = "";
  var anio = "";
  var combustible = "";
  if(aviso.usado != undefined){
    if(aviso.usado == "0 Km"){
      kilometraje = aviso.usado;
    } else {
      if(aviso.kilometros != "") {
        kilometraje = aviso.kilometros + " Km";
      }
    }
    anio = aviso.anio;
    combustible = aviso.combustible;
  }
  var imagcache_class = 'ficha_aviso_314_211';
  var width = 314;
  var height = 211;
  var content_class = '';
  if(Losandes.DestacadosConfig.block == 'inmuebles') {
    var imagcache_class = 'ficha_aviso_mobile_220_148';
    var width = 220;
    var height = 148;
    var content_class = 'newDer';
  }
  var url_split = aviso.url;
  var alt_img = url_split.split("/");
  alt_img = alt_img[0]+' de '+alt_img[1]+' '+aviso.titulo+' a '+aviso.precio;
  var htmlAviso = [
    '<div class="CajaAviso '+content_class+'">',
    ' <a href="'+aviso.url+'" title="'+aviso.titulo+'">',
    '   <div class="imgAviso">',
    '     <img src="'+aviso.foto+'" alt="'+alt_img+'" title="'+aviso.titulo+'" class="imagecache imagecache-'+imagcache_class+'" width="'+width+'" height="'+height+'">',
    '   </div>',
    '   <div class="AvisoDescripcion">',
    '     <h4>'+aviso.titulo+'</h4>',
    '     <p>'+anio+' '+kilometraje+' '+combustible+'</p>',
    '     <span class="precio">'+aviso.precio+'</span>',
    '   </div>',
    ' </a>',
    '</div>',
    ''
  ].join("\n");
  return htmlAviso;
}

Losandes.CargarDestacadosSlide = function () {return;//BloqueTiendas
  if(!Losandes.AvisosData) {
    avisoDiv.show();
    return;
  }
  data = Losandes.AvisosData;
  var avisosEl = $('ul.'+classContenedor);
  var url_path = data[0].url_path;
  
  //Randoms
  var rndN = 0;
  var avisos_rnd = [];
  var randoms_values = {}, property_name = '';
  var v = 0, i = 0, m = 0;
  if(data.length && data.length > cantidadAvisos+1) {
    m = cantidadAvisos;
  } else {
    avisoDiv.show();
    m = 0; //no hago random ya que no hay suficientes avisos
    return;
  }
  while(avisos_rnd.length < m){
    rndN = Math.floor((Math.random()*(data.length-1))+1);
    property_name = 'custom_' + rndN;
    if(! randoms_values.hasOwnProperty(property_name)) {
      avisos_rnd[avisos_rnd.length] = rndN;
      randoms_values[property_name] = rndN;
    }
  }
    
  if(addMix == 0)
    avisosEl.children().remove();

  for(i=0; i<avisos_rnd.length; i++) {
    v = avisos_rnd[i];
    data[v].url_path = url_path;
    if(addMix == 1){
      var number = Math.floor(Math.random() * $('ul.CajaAviso').length);
      $('ul.CajaAviso:eq('+number+')').hide().after(Losandes.EstructuraAvisoDestacadoSlide(data[v])).fadeIn(1000);
    } else {
      avisosEl.hide().append(Losandes.EstructuraAvisoDestacadoSlide(data[v])).fadeIn(1000);
    }
  }
  if(addClassPar == 1)
    $(avisoDiv+':even').addClass('Par');console.log('Fin CargarDestacadosSlide ');
}
Losandes.EstructuraAvisoDestacadoSlide = function (aviso) {
  var kilometraje = "";
  var anio = "";
  var combustible = "";
  if(aviso.usado != undefined){
    if(aviso.usado == "0 Km"){
      kilometraje = aviso.usado;
    } else {
      if(aviso.kilometros != "") {
        kilometraje = aviso.kilometros + " Km";
      }
    }
    anio = aviso.anio;
    combustible = aviso.combustible;
  }
  var imagecache_class = 'ficha_aviso_314_211';
  var width = 314;
  var height = 211;
  var content_class = '';
  if(Losandes.DestacadosConfig) {
    if(Losandes.DestacadosConfig.block == 'inmuebles') {
      var imagecache_class = 'ficha_aviso_mobile_220_148';
      var width = 220;
      var height = 148;
      var content_class = 'new';
      aviso.url += '?cx_level=detacados_inmuebles';
    }
    if(Losandes.DestacadosConfig.block == 'autos'){
      aviso.url += '?cx_level=detacados_autos';
    }
    if(Losandes.DestacadosConfig.block == 'principal'){
      aviso.url += '?cx_level=detacados_home';
    }
  }
  var url_split = aviso.url;
  var alt_img = url_split.split("/");
  alt_img = alt_img[0]+' de '+alt_img[1]+' '+aviso.titulo+' a '+aviso.precio;
  var htmlAviso = [
    '<li>',
    '<div class="CajaAviso '+content_class+'">',
    ' <a href="'+aviso.url+'" title="'+aviso.titulo+'">',
    '   <div class="imgAviso">',
    '     <img src="'+aviso.foto+'" alt="'+alt_img+'" title="'+aviso.titulo+'" class="imagecache imagecache-'+imagecache_class+'" width="'+width+'" height="'+height+'">',
    '   </div>',
    '   <div class="AvisoDescripcion">',
    '     <h4>'+aviso.titulo+'</h4>',
    '     <p>'+anio+' '+kilometraje+' '+combustible+'</p>',
    '     <span class="precio">'+aviso.precio+'</span>',
    '   </div>',
    ' </a>',
    '</div>',
    '</li>'
  ].join("\n");
  return htmlAviso;
}

Losandes.CargarSlide = function (div, cantidad) {
  Losandes.SliderSettings.slideBy = cantidad;
  var sliderEl = $(div).parent().parent();
  sliderEl.accessNews(Losandes.SliderSettings);
}