jQuery(document).ready(function($){
  
  //Eliminar mensaje de contrasenia
  $(".password-description").remove();
  
  //Mostrar ciudades y barrios de cba
  if($("#edit-profile-provincia").val() != AUTOSLAVOZ_PROVINCIA_MENDOZA_TAXONOMY_TID){
    $(".Row.clearfix.ciudad").hide();
    $(".Row.clearfix.barrio").hide();
  }
  if($("#edit-profile-ciudad").val() != AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
    $(".Row.clearfix.barrio").hide();
  }
  $("#edit-profile-provincia").change(function(){
    if($(this).val() == AUTOSLAVOZ_PROVINCIA_MENDOZA_TAXONOMY_TID){
      $(".Row.clearfix.ciudad").show("slow");
      if($("#edit-profile-ciudad").val() == AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
        $(".Row.clearfix.barrio").show("slow");
      }
    } else {
      $(".Row.clearfix.ciudad").hide("slow");
      $("#edit-profile-ciudad").val("0");
      $(".Row.clearfix.barrio").hide("slow");
      $("#edit-profile-barrio").val("0");
    }
  });
  $("#edit-profile-ciudad").change(function(){
    if($(this).val() == AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
      $(".Row.clearfix.barrio").show("slow");
    } else {
      $(".Row.clearfix.barrio").hide("slow");
      $("#edit-profile-barrio").val("0");
    }
  });
  if($("#edit-profile-domicilio-fiscal-provincia").val() != AUTOSLAVOZ_PROVINCIA_MENDOZA_TAXONOMY_TID){
    $(".Row.clearfix.ciudad-fiscal").hide();
    $(".Row.clearfix.barrio-fiscal").hide();
  }
  if($("#edit-profile-domicilio-fiscal-ciudad").val() != AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
    $(".Row.clearfix.barrio-fiscal").hide();
  }
  $("#edit-profile-domicilio-fiscal-provincia").change(function(){
    if($(this).val() == AUTOSLAVOZ_PROVINCIA_MENDOZA_TAXONOMY_TID){
      $(".Row.clearfix.ciudad-fiscal").show("slow");
      if($("#edit-profile-domicilio-fiscal-ciudad").val() == AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
        $(".Row.clearfix.barrio-fiscal").show("slow");
      }
    } else {
      $(".Row.clearfix.ciudad-fiscal").hide("slow");
      $("#edit-profile-domicilio-fiscal-ciudad").val("0");
      $(".Row.clearfix.barrio-fiscal").hide("slow");
      $("#edit-profile-domicilio-fiscal-barrio").val("0");
    }
  });
  $("#edit-profile-domicilio-fiscal-ciudad").change(function(){
    if($(this).val() == AUTOSLAVOZ_CIUDAD_MENDOZA_TAXONOMY_TID){
      $(".Row.clearfix.barrio-fiscal").show("slow");
    } else {
      $(".Row.clearfix.barrio-fiscal").hide("slow");
      $("#edit-profile-domicilio-fiscal-barrio").val("0");
    }
  });
  
  //Mostrar direccion fiscal
  if($("#edit-profile-domicilio-fiscal-mismo").attr("checked")){
    $(".domicilio-fiscal").hide();
  }
  $("#edit-profile-domicilio-fiscal-mismo").click(function(){
    if($(this).attr("checked")){
      $(".domicilio-fiscal").hide("slow");
    } else {
      $(".domicilio-fiscal").show("slow");
    }
  });
  
  //Mostrar nombre sucursal
  if($("#edit-profile-sucursal-No").attr("checked")){
    $(".Row.clearfix.nombre-sucursal").hide();
  }
  $("#edit-profile-sucursal-No").click(function(){
    $(".Row.clearfix.nombre-sucursal").hide("slow");
  });
  $("#edit-profile-sucursal-Si").click(function(){
    $(".Row.clearfix.nombre-sucursal").show("slow");
  });
  
  
  //Marcar campos requeridos
  $(".Row.clearfix.sexo label:first").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.condiciones label.option").prepend('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.sucursal .form-item label:first").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.nombre-sucursal label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix .sucursal-calle label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix .sucursal-altura label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.cp label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.provincia label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.ciudad label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.barrio label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.tipo-concesionaria label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.telefono-sec label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.razon-social label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.cuit label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.iva label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.cp-fiscal label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.provincia-fiscal label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.ciudad-fiscal label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  $(".Row.clearfix.barrio-fiscal label").append('<span class="form-required" title="Este campo es obligatorio.">*</span>');
  
  //Validacion de campos
  $("#edit-profile-telefono-principal").keypress(function(event) {
    tecla = (document.all) ? event.keyCode : event.which;
    if (tecla==8 || tecla==0 || (event.ctrlKey && tecla==118) || (event.ctrlKey && tecla==99)) return true;
    patron =/[0-9 \-\(\)\.\+\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
  });
  $("#edit-profile-telefono-secundario").keypress(function(event) {
    tecla = (document.all) ? event.keyCode : event.which;
    if (tecla==8 || tecla==0 || (event.ctrlKey && tecla==118) || (event.ctrlKey && tecla==99)) return true;
    patron =/[0-9 \-\(\)\.\+\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
  });
  
  //Cargar numero WhatsApp
  if($('#edit-profile-telefono-whatsapp').length >= 1) {
    if($('#edit-profile-telefono-whatsapp').val() != '') {
      var whatsapp = $('#edit-profile-telefono-whatsapp').val();
      whatsapp = whatsapp.split('+549');
      $('#telefono_whatsapp').val(whatsapp[1]);
    }
  }
  
});