WebPage.CargarContactosDelAviso = function (event) {
  event.preventDefault();
  
  var aviso_id = parseInt($(this).attr('aviso-id'));
  var div_contactos = $('#contacto-del-aviso-'+aviso_id);

  var new_content = null;
  var old_content = null;

  var url_ajax = '/views/ajax?js=1&view_args='+aviso_id+'&view_path=contactos_recibidos_por_aviso%2F'+aviso_id+'&view_name=contactos_recibidos&view_display_id=page_2&view_base_path=contactos_recibidos_por_aviso&view_dom_id=1&pager_element=0';

  if(div_contactos.find('a.CerrarContactosDelAviso').length <= 0) {
    $.ajax({
      url: url_ajax,
      dataType: 'json',
      success: function(data) {
        new_content = $(data.display);
        if(new_content.length <= 0) {
          return;
        }
        div_contactos.empty();
        div_contactos.append(new_content);
        div_contactos.append($('<div class="clearfix"></div>'));
        if(window.location.pathname == '/administrar/mis-avisos-colaborador.html') {
          div_contactos.append($('<span class="dialogo"></span><div class="acciones clearfix"><a href="#" class="CerrarContactosDelAviso" aviso-id="'+aviso_id+'">cerrar</a>&nbsp;<a href="/contactos_recibidos_colaboradores">ver más</a></div>'));
        } else {
          div_contactos.append($('<span class="dialogo"></span><div class="acciones clearfix"><a href="#" class="CerrarContactosDelAviso" aviso-id="'+aviso_id+'">cerrar</a>&nbsp;<a href="/contactos_recibidos/?nid='+aviso_id+'">ver más</a></div>'));
        }
        $('a.CerrarContactosDelAviso').click(WebPage.CerrarContactosDelAviso);
      }
    });
  }

  div_contactos.toggle();
};

WebPage.CerrarContactosDelAviso = function (event) {
  event.preventDefault();

  var aviso_id = parseInt($(this).attr('aviso-id'));
  var div_contactos = $('#contacto-del-aviso-'+aviso_id);

  div_contactos.toggle();
};

function aplicarFiltroDesdeHasta(referencia){
  var desde = $("#edit-" + referencia + "-desde").val();
  var hasta = $("#edit-" + referencia + "-hasta").val();
  var campo_desde = $("#edit-" + referencia + "-desde").attr('name');
  var campo_hasta = $("#edit-" + referencia + "-hasta").attr('name');
  if(desde == '' || hasta == '')
    return false;
  if(parseInt(desde) >= parseInt(hasta))
    return false;
  var url = $("#" + referencia + "-url").val() + '&' + campo_desde + '=' + desde + '&' + campo_hasta + '=' + hasta;
  window.location.href=url;
}

jQuery(document).ready(function($){
  $('a.VerContactosDelAviso').click(WebPage.CargarContactosDelAviso);
  
  $("#views-exposed-form-listado-avisos-admin-page-1").submit(function(event) {
    event.preventDefault();
  });
  $("#views-exposed-form-listado-avisos-admin-page-2").submit(function(event) {
    event.preventDefault();
  });
  //Filtro texto
  $('#edit-submit-body').click(function(event) {
    event.preventDefault();
    var texto = $("#edit-body").val();
    if(texto == '')
      return false;
    var url = $("#edit-body-url").val() + '&body=' + texto;
    window.location.href=url;
  });
  $('#edit-reset-body').click(function(event) {
    var url = $("#edit-body-url").val();
    window.location.href=url;
  });
  $('#edit-submit-title').click(function(event) {
    event.preventDefault();
    var texto = $("#edit-title").val();
    if(texto == '')
      return false;
    var url = $("#edit-title-url").val() + '&title=' + texto;
    window.location.href=url;
  });
  $('#edit-reset-title').click(function(event) {
    event.preventDefault();
    var url = $("#edit-title-url").val();
    window.location.href=url;
  });
  
  //Filtros desde/hasta
  //Precio
  $('#edit-submit-precio').click(function(event) {
    var referencia = $(this).attr('name');
    aplicarFiltroDesdeHasta(referencia);
  });
  $('#edit-reset-precio').click(function(event) {
    var referencia = $(this).attr('name');
    var url = $("#" + referencia + "-url").val();
    window.location.href=url;
  });
  //Kilometros
  $('#edit-submit-kilometros').click(function(event) {
    var referencia = $(this).attr('name');
    aplicarFiltroDesdeHasta(referencia);
  });
  $('#edit-reset-kilometros').click(function(event) {
    var referencia = $(this).attr('name');
    var url = $("#" + referencia + "-url").val();
    window.location.href=url;
  });
  //Anios
  $('#edit-submit-anio').click(function(event) {
    var referencia = $(this).attr('name');
    aplicarFiltroDesdeHasta(referencia);
  });
  $('#edit-reset-anio').click(function(event) {
    var referencia = $(this).attr('name');
    var url = $("#" + referencia + "-url").val();
    window.location.href=url;
  });
  //Siperficie terreno
  $('#edit-submit-superficie').click(function(event) {
    var referencia = $(this).attr('name');
    aplicarFiltroDesdeHasta(referencia);
  });
  $('#edit-reset-superficie').click(function(event) {
    var referencia = $(this).attr('name');
    var url = $("#" + referencia + "-url").val();
    window.location.href=url;
  });
  
  slider_km = $("#slider-kilometros");
  if(slider_km.length != 0) { // Existe el div id=slider-km
    var km_max = $("#edit-kilometros-max").val()-0;
    if(!($("#edit-kilometros-desde").val()))
      $("#edit-kilometros-desde").val(0);
    if(!($("#edit-kilometros-hasta").val()))
      $("#edit-kilometros-hasta").val(km_max);
    $("#edit-kilometros-desde").attr('readonly', 'readonly');
    $("#edit-kilometros-hasta").attr('readonly', 'readonly');
    $("#edit-kilometros-desde").css('border', 'none');
    $("#edit-kilometros-hasta").css('border', 'none');
    slider_km.slider({
      range: true, // necessary for creating a range slider
      min: 0, // minimum range of slider
      max: km_max, //maximimum range of slider
      step: 5000,
      values: [($("#edit-kilometros-desde").val()?$("#edit-kilometros-desde").val():0), ($("#edit-kilometros-hasta").val()?$("#edit-kilometros-hasta").val():km_max)], //initial range of slider
      slide: function(event, ui) { // This event is triggered on every mouse move during slide.
        $("#edit-kilometros-desde").val(ui.values[0]);
        $("#edit-kilometros-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){//This event is triggered when the user stops sliding.
        $("#edit-kilometros-desde").val(ui.values[0]);
        $("#edit-kilometros-hasta").val(ui.values[1]);
      }
    });
  }
  slider_anio = $("#slider-anio");
  if(slider_anio.length != 0) { // Existe el div id=slider-anio
    var anio_min = $("#edit-anio-min").val()-0;
    var anio_max = (new Date).getFullYear();
    if(isNaN($("#edit-anio-desde").val()) || !$("#edit-anio-desde").val())
      $("#edit-anio-desde").val(anio_min);
    if(isNaN($("#edit-anio-hasta").val()) || !$("#edit-anio-hasta").val())
      $("#edit-anio-hasta").val(anio_max);
    $("#edit-anio-desde").attr('readonly', 'readonly');
    $("#edit-anio-hasta").attr('readonly', 'readonly');
    $("#edit-anio-desde").css('border', 'none');
    $("#edit-anio-hasta").css('border', 'none');
    slider_anio.slider({
      range: true,
      min: anio_min,
      max: anio_max,
      step: 1,
      values: [($("#edit-anio-desde").val()?$("#edit-anio-desde").val():anio_min), ($("#edit-anio-hasta").val()?$("#edit-anio-hasta").val():anio_max)],
      slide: function(event, ui) {
        $("#edit-anio-desde").val(ui.values[0]);
        $("#edit-anio-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){
        $("#edit-anio-desde").val(ui.values[0]);
        $("#edit-anio-hasta").val(ui.values[1]);
      }
    });
  }
  /*slider_precio = $("#slider-precio");
  if(slider_precio.length != 0) { // Existe el div id=slider-precio
    var precio_max = $("#edit-precio-max").val()-0;
    if(isNaN($("#edit-precio-desde").val())  || !$("#edit-precio-desde").val())
      $("#edit-precio-desde").val(0);
    if(isNaN($("#edit-precio-hasta").val()) || !$("#edit-precio-hasta").val())
      $("#edit-precio-hasta").val(precio_max);
    $("#edit-precio-desde").attr('readonly', 'readonly');
    $("#edit-precio-hasta").attr('readonly', 'readonly');
    $("#edit-precio-desde").css('border', 'none');
    $("#edit-precio-hasta").css('border', 'none');
    slider_precio.slider({
      range: true,
      min: 0,
      max: precio_max,
      step: 500,
      values: [($("#edit-precio-desde").val()?$("#edit-precio-desde").val():0), ($("#edit-precio-hasta").val()?$("#edit-precio-hasta").val():precio_max)],
      slide: function(event, ui) {
        $("#edit-precio-desde").val(ui.values[0]);
        $("#edit-precio-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){
        $("#edit-precio-desde").val(ui.values[0]);
        $("#edit-precio-hasta").val(ui.values[1]);
      }
    });
  }*/
  slider_sup = $("#slider-superficie");
  if(slider_sup.length != 0) { // Existe el div id=slider-sup
    var sup_max = $("#edit-superficie-max").val()-0;
    if(!($("#edit-superficie-desde").val()))
      $("#edit-superficie-desde").val(0);
    if(!($("#edit-superficie-hasta").val()))
      $("#edit-superficie-hasta").val(sup_max);
    $("#edit-superficie-desde").attr('readonly', 'readonly');
    $("#edit-superficie-hasta").attr('readonly', 'readonly');
    $("#edit-superficie-desde").css('border', 'none');
    $("#edit-superficie-hasta").css('border', 'none');
    slider_sup.slider({
      range: true, // necessary for creating a range slider
      min: 0, // minimum range of slider
      max: sup_max, //maximimum range of slider
      step: 100,
      values: [($("#edit-superficie-desde").val()?$("#edit-superficie-desde").val():0), ($("#edit-superficie-hasta").val()?$("#edit-superficie-hasta").val():sup_max)], //initial range of slider
      slide: function(event, ui) { // This event is triggered on every mouse move during slide.
        $("#edit-superficie-desde").val(ui.values[0]);
        $("#edit-superficie-hasta").val(ui.values[1]);
      },
      stop: function(event, ui){//This event is triggered when the user stops sliding.
        $("#edit-superficie-desde").val(ui.values[0]);
        $("#edit-superficie-hasta").val(ui.values[1]);
      }
    });
  }
  
  //Modal Informativo
  if($("#bkg").length){
    $("#bkg").click(function () {
      //$("#popover").hide('300', function () { 
        $("#popover").hide();
        $("#bkg").fadeOut("500"); 
        //if($("#popover.Agenda").length > 0) {
          //$(".precio-uva").css("position", "initial");
          //$("li.mis-datos").css("z-index", "0");
          //$("li.contactos").css("background", "initial");
          //$("li.contactos").css("padding", "0px");
          //$("li.contactos").css("margin-top", "0px");
          //$("li.resumen a").attr("href", "/administrar/principal");
          //$("#popover.Colaboradores").hide();
        //}
        /*if($("#popover.EstadisticasResumen").length > 0) {
          $(".link-estadisticas-globales").css("position", "initial");
          $(".link-estadisticas-globales").css("z-index", "0");
          $(".link-estadisticas-globalesn").css("background", "initial");
          $("#popover.Colaboradores").hide();
        }*/
      //});
    });
    if (document.getElementById('bkg').style.visibility == 'hidden') {
      document.getElementById('bkg').style.visibility = '';
      $("#bkg").hide();
    }
    $("#bkg").fadeIn(500, "linear", function () { $("#popover").show(800, "swing"); });
    if($("#popover.Agenda").length > 0) {
      /*
      var path = window.location.pathname;
      if(path.indexOf('node/add') != -1)
        $("#popover.Agenda").css('top', 2050);
            
      $('html, body').animate({
          scrollTop: 1950
      }, 1000);
      */
      //$(".precio-uva").css("position", "absolute");
      //$(".precio-uva").css("background-color", "#FFF");
      //$(".precio-uva").css("padding", "5px");
      //$(".precio-uva .form-item").css("margin-bottom", "0px");
      //$(".precio-uva").css("background", "inherit");
      //$("li.contactos").css("background-color", "#008ACE");
      //$("li.mis-datos a").attr("href", "/administrar/estadisticas_globales_autos");
      //$("li.contactos").css("right", "599px");
      //$(".precio-uva").css("z-index", "9999");
      //$("ul.contactos-links").css("top", "36px");
      //$("ul.contactos-links").show("");
      /*var href = $("li.contactos > a").attr("href");
      $("li.contactos > a").attr("href", href + '#respuesta');*/
    }
    if($("#popover.EstadisticasResumen").length > 0) {
      $(".link-estadisticas-globales").css("position", "absolute");
      $(".link-estadisticas-globales").css("z-index", "9999");
    }
  }
  
  if($("#tabs-booking").length){
    $( function() {
      $( "#tabs-booking" ).tabs({
        active: false,
        collapsible: true
      }).addClass( "ui-helper-clearfix" );
    });
  }
  
});