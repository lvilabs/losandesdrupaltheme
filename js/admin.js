//Contactos Vendedor editar accion
function editar_accion_contacto(id_cv){
  var id_cv = id_cv;
  $('#contacto-accion-'+id_cv).hide();
  $('#contacto-accion-select-'+id_cv).show();
  $("#contacto-accion-select-"+id_cv).focus();
  $('#contacto-accion-select-'+id_cv).blur(function(event) {
    var accion = $("#contacto-accion-select-"+id_cv).val();
    $.ajax({
			type: "GET",
			url: "/ajax/contacto-cambio-accion?id_cv=" + id_cv + "&accion=" + accion
		});
    if(accion == '')
      $('#contacto-accion-'+id_cv).html('Sin acción');
    else
      $('#contacto-accion-'+id_cv).html(accion);
    $('#contacto-accion-'+id_cv).show();
    $('#contacto-accion-select-'+id_cv).hide();
  });
}
//Contactos Vendedor editar remitente
function editar_remitente_contacto(id_cv){
  var id_cv = id_cv;
  $('#contacto-remitente-'+id_cv).hide("");
  $('#contacto-remitente-text-'+id_cv).show("");
  $("#contacto-remitente-text-"+id_cv).focus();
  $('#contacto-remitente-text-'+id_cv).blur(function(event) {
    var remitente = $("#contacto-remitente-text-"+id_cv).val();
    if(remitente != ''){
      $.ajax({
        type: "GET",
        url: "/ajax/contacto-cambio-remitente?id_cv=" + id_cv + "&remitente=" + remitente
      });
    }
    if(remitente == '')
      $('#contacto-remitente-'+id_cv).html('Sin especificar');
    else
      $('#contacto-remitente-'+id_cv).html(remitente);
    $('#contacto-remitente-'+id_cv).show();
    $('#contacto-remitente-text-'+id_cv).hide();
  });
}
//marcar contacto leido vis ajax
function ajax_contacto_leido(id_cv){
  var id_cv = id_cv;
  $.ajax({
    type: "GET",
    url: "/ajax/contacto/leido/" + id_cv,
    beforeSend: function() {
      $('.consulta-loading').show("");
      $('.consultas-sin-leer').hide("");
    },
    success: function(data) {
      $('.consultas-sin-leer').html(data);
      //Boton responder, marca contacto como leido via ajax
      $('.contacto-respuesta-ajax').click(function(event) {
        var contacto = $(this).attr('id').split('-');    
        ajax_contacto_leido(contacto[1]);
      });
      //Boton marca contacto como leido via ajax
      $('.contacto-leido-ajax').click(function(event) {
        var contacto = $(this).attr('id').split('-');
        ajax_contacto_leido(contacto[1]);
      });
      $.ajax({
        type: "GET",
        url: "/ajax/contacto/cantidad_no_leidos",
        success: function(data) {
          $('.content-cantidad-consultas').html(data);
        },
      });
    },
    complete: function() {
      $('.consulta-loading').hide("");
      $('.consultas-sin-leer').show("");
    }
  });
}
//republicacion masiva de avisos
function ajax_republicacion_masiva(uid){
  $.ajax({
    type: "GET",
    url: "/republicacion-manual-avisos/" + uid,
    beforeSend: function() {
      $('.republicacion-masiva').html("Reposicionando...");
      $('.republicacion-masiva').attr('disabled','disabled');
    },
    success: function(data) {
    },
    complete: function() {
      window.location.href = "/administrar/mis-avisos.html";
    }
  });
}
//Generar reserva desde el administrador de avisos
function adminGenerarReserva(nid, startDate, endDate) {
  var urlAjax = '/clvi_booking/save_data_admin/' + nid + '/' + startDate + '/' + endDate;
  $.ajax({
  url: urlAjax,
  cache: false})
  .done(function(data) {
  });
}

//Estadisticas de tipologias mas vistas
function ajax_estadisticas_vistas_totales(inputs, interna){
  var dias = inputs.diasVisitasTotales;
  var postKeys = {'interna' : interna, 'dias' : dias};
  $.ajax({
    type: "POST",
    url: "/ajax/estadisticas_globales/vistos_totales",
    data: postKeys,
    dataType: "text",
    beforeSend: function() {
    },
    success: function(data) {
      $('.contadorTotal').html(data);
    },
    complete: function() {
    }
  });
}

//Estadisticas de tipologias mas vistas
function ajax_estadisticas_mas_vistos(inputs, interna){
  var dias = inputs.diasMasVisto;
  var registros = inputs.cantidadMasVisto;
  var zonas = '';
  var localidades = '';
  var barrios = '';
  var tipologias = '';
  var operaciones = '';
  var marcas = '';
  var modelos = '';
  var anios = '';
  if(typeof inputs.zonaMasVisto !== 'undefined') {
    zonas = inputs.zonaMasVisto;
  }
  if(typeof inputs.localidadMasVisto !== 'undefined') {
    localidades = inputs.localidadMasVisto;
  }
  if(typeof inputs.barrioMasVisto !== 'undefined') {
    barrios = inputs.barrioMasVisto;
  }
  if(typeof inputs.tipologiaMasVisto !== 'undefined') {
    tipologias = inputs.tipologiaMasVisto;
  }
  if(typeof inputs.operacionMasVisto !== 'undefined') {
    operaciones = inputs.operacionMasVisto;
  }
  if(typeof inputs.marcaMasVisto !== 'undefined') {
    marcas = inputs.marcaMasVisto;
  }
  if(typeof inputs.modeloMasVisto !== 'undefined') {
    modelos = inputs.modeloMasVisto;
  }
  if(typeof inputs.anioMasVisto !== 'undefined') {
    anios = inputs.anioMasVisto;
  }
  var postKeys = {'interna' : interna, 'dias' : dias, 'registros': registros, 'zonas': zonas, 'localidades': localidades, 'barrios': barrios, 'tipologias': tipologias, 'operaciones': operaciones, 'marcas': marcas, 'modelos': modelos, 'anios': anios};
  $.ajax({
    type: "POST",
    url: "/ajax/estadisticas_globales/mas_vistos",
    data: postKeys,
    dataType: "text",
    beforeSend: function() {
      $('#listadoMasVistos').hide();
      $('.globalLoading').show();
    },
    success: function(data) {
      $('#listadoMasVistos').html(data);
    },
    complete: function() {
      $('.globalLoading').hide();
      $('#listadoMasVistos').show();
    }
  });
}

//Estadisticas de tipologias mas publicadas
function ajax_estadisticas_tipologias_publicadas(inputs, interna){
  var tipologias = '';
  var operaciones = '';
  var marcas = '';
  var modelos = '';
  var anios = '';
  if(typeof inputs.tipologiaPublicadas !== 'undefined') {
    tipologias = inputs.tipologiaPublicadas;
  }
  if(typeof inputs.operacionPublicadas !== 'undefined') {
    operaciones = inputs.operacionPublicadas;
  }
  if(typeof inputs.marcaPublicadas !== 'undefined') {
    marcas = inputs.marcaPublicadas;
  }
  if(typeof inputs.modeloPublicadas !== 'undefined') {
    modelos = inputs.modeloPublicadas;
  }
  if(typeof inputs.anioPublicadas !== 'undefined') {
    anios = inputs.anioPublicadas;
  }
  var postKeys = {'interna' : interna, 'tipologias': tipologias, 'operaciones': operaciones, 'marcas': marcas, 'modelos': modelos, 'anios': anios};
  $.ajax({
    type: "POST",
    url: "/ajax/estadisticas_globales/tipologias_publicadas",
    data: postKeys,
    dataType: "text",
    beforeSend: function() {
      
    },
    success: function(data) {
      var tipologiasPublicadas = JSON.parse(data);
      if(tipologiasPublicadas.length !== 0) {
        var color_grafico2 = ['#F7464A','#46BFBD','#FDB45C','#949FB1','#4D5360','#86B2DB','#F7D443','#9392C8','#2869B5','#B070AD'];
        var highlight_grafico2 = ['#FF5A5E','#5AD3D1','#FFC870','#A8B3C5','#616774','#96C3EA','#FFE16B','#A3A2DB','#2E7AD1','#C48DC1'];
        var pieData = '';
        var legend = '<ul>';
        for(i=0;i<tipologiasPublicadas.length; i++) { 
          if(pieData == '')
            pieData = pieData + '[{"value": '+ tipologiasPublicadas[i].cantidad + ',"color": "' + color_grafico2[i] + '","highlight": "' + highlight_grafico2[i] + '","label": "' + tipologiasPublicadas[i].tipologia + '"}';
          else
            pieData = pieData + ',{"value": '+ tipologiasPublicadas[i].cantidad + ',"color": "' + color_grafico2[i] + '","highlight": "' + highlight_grafico2[i] + '","label": "' + tipologiasPublicadas[i].tipologia + '"}';
          legend = legend + '<li><span style="background-color:' + color_grafico2[i] +';"></span>' + tipologiasPublicadas[i].tipologia + ' - ' + tipologiasPublicadas[i].cantidad + '</li>';
        }
        pieData = pieData + ']';
        pieData = JSON.parse(pieData);
        legend = legend + '</ul>';  
        $('#canvas2').remove();
        $('#contentGrafico').html('<canvas id="canvas2" height="200" width="280"></canvas>');
        var ctx2 = document.getElementById("canvas2").getContext("2d");
        window.myPie = new Chart(ctx2).Pie(pieData);
      } else {
        $('#canvas2').remove();
        $('#contentGrafico').html('<div class="messageSinDatos">Tus filtros no encontraron resultados. Prueba quitar algunos.</div>');
        var legend = '';
      }
      $('#legendDiv').html(legend);
    },
    complete: function() {
      
    }
  });
}

//Estadisticas de tipologias mas destacadas
function ajax_estadisticas_mas_destacadas(inputs, interna){
  var registros = inputs.cantidadMasDestacada;
  var zonas = '';
  var localidades = '';
  var barrios = '';
  var tipologias = '';
  var operaciones = '';
  var marcas = '';
  var modelos = '';
  var anios = '';
  if(typeof inputs.zonaMasDestacada !== 'undefined') {
    zonas = inputs.zonaMasDestacada;
  }
  if(typeof inputs.localidadMasDestacada !== 'undefined') {
    localidades = inputs.localidadMasDestacada;
  }
  if(typeof inputs.barrioMasDestacada !== 'undefined') {
    barrios = inputs.barrioMasDestacada;
  }
  if(typeof inputs.tipologiaMasDestacada !== 'undefined') {
    tipologias = inputs.tipologiaMasDestacada;
  }
  if(typeof inputs.operacionMasDestacada !== 'undefined') {
    operaciones = inputs.operacionMasDestacada;
  }
  if(typeof inputs.marcaMasDestacada !== 'undefined') {
    marcas = inputs.marcaMasDestacada;
  }
  if(typeof inputs.modeloMasDestacada !== 'undefined') {
    modelos = inputs.modeloMasDestacada;
  }
  if(typeof inputs.anioMasDestacada !== 'undefined') {
    anios = inputs.anioMasDestacada;
  }
  var postKeys = {'interna' : interna, 'registros': registros, 'zonas': zonas, 'localidades': localidades, 'barrios': barrios, 'tipologias': tipologias, 'operaciones': operaciones, 'marcas': marcas, 'modelos': modelos, 'anios': anios};
  $.ajax({
    type: "POST",
    url: "/ajax/estadisticas_globales/mas_destacadas",
    data: postKeys,
    dataType: "text",
    beforeSend: function() {
      $('#listadoMasDestacada').hide();
      $('.globalLoadingDestacada').show();
    },
    success: function(data) {
      $('#listadoMasDestacada').html(data);
    },
    complete: function() {
      $('.globalLoadingDestacada').hide();
      $('#listadoMasDestacada').show();
    }
  });
}

//Estadisticas de precios promedio por tipologias
function ajax_estadisticas_precios_promedio(inputs, interna){
  var localidades = '';
  var barrios = '';
  var tipologias = '';
  var marcas = '';
  var modelos = '';
  var anios = '';
  if(typeof inputs.localidadPrecios !== 'undefined') {
    localidades = inputs.localidadPrecios;
  }
  if(typeof inputs.barrioPrecios !== 'undefined') {
    barrios = inputs.barrioPrecios;
  }
  if(typeof inputs.tipologiaPrecios !== 'undefined') {
    tipologias = inputs.tipologiaPrecios;
  }
  if(typeof inputs.marcaPrecios !== 'undefined') {
    marcas = inputs.marcaPrecios;
  }
  if(typeof inputs.modeloPrecios !== 'undefined') {
    modelos = inputs.modeloPrecios;
  }
  if(typeof inputs.anioPrecios !== 'undefined') {
    anios = inputs.anioPrecios;
  }
  var postKeys = {'interna' : interna, 'localidades': localidades, 'barrios': barrios, 'tipologias': tipologias, 'marcas': marcas, 'modelos': modelos, 'anios': anios};
  $.ajax({
    type: "POST",
    url: "/ajax/estadisticas_globales/precios_promedio",
    data: postKeys,
    dataType: "text",
    beforeSend: function() {
      $('.estadisticas_precios').hide();
      $('.globalLoadingPrecios').show();
    },
    success: function(data) {
      $('.estadisticas_precios').html(data);
    },
    complete: function() {
      $('.globalLoadingPrecios').hide();
      $('.estadisticas_precios').show();
    }
  });
}

//Procesar datos POST del formulario de estadisitcas
function estadisticas_procesar_datos_form(datosPost) {
  var inputs = {};
  $.each(datosPost, function(i, field) {
    if(field.name.indexOf('_') != -1) {
      if(field.value != '') {
        var agrupador = field.name.split('_');
        agrupador = agrupador[0];
        if(typeof inputs[agrupador] === 'undefined')
          inputs[agrupador] = field.value;
        else
          inputs[agrupador] = inputs[agrupador] + ',' + field.value;
      }
    } else {
      inputs[field.name] = field.value;
    }
  });
  return inputs;
}

//Cambiar estado de interesado
function cambio_estado_interesado(id, estado) {
  $.ajax({
    type: "GET",
    url: "/ajax/cambio_estado_interesado/" + id + "/" + estado,
    dataType: "text",
    success: function(data) {
      if(data != '') {
        $('#estado-'+ id).html(data);
        var data_calss = data.replace(' ','-');
        $('#row-table-'+ id).removeClass();
        $('#row-table-'+ id).addClass('row-table');
        $('#row-table-'+ id).addClass(data_calss);
      }
      $('#select-estado-'+ id).hide('');
      $('#estado-'+ id).show('');
      $('.img-select').show('');
    }
  });
}

// Mostrar alertas de nuevos contactos
function alertas_nuevos_contactos(uid, acceso_contactos, acceso_interesados) {
  $.ajax({
    type: "GET",
    url: "/ajax/alerta_nuevos_contactos/" + uid + "/" + acceso_contactos + "/" + acceso_interesados,
    success: function(data) {
      var totales = JSON.parse(data);
      alertas_mostrar_nuevos_contactos(totales);
      totales['ingreso'] = Math.floor(Date.now() / 1000);
      totales['ingresoContactos'] = acceso_contactos;
      totales['ingresoInteresados'] = acceso_interesados;
      $stringify = JSON.stringify(totales);
      Cookies.set('clviAlertaContactos', $stringify);
    }
  });
}

function alertas_mostrar_nuevos_contactos(totales) {
  var contador_total = 0;
  var contador_contactos = 0;
  var contador_interesados = 0;
  if(totales.contactos > 0) {
    contador_contactos = totales.contactos;
    contador_total = totales.contactos;
    $('#alerta-contactos').html(contador_contactos);
    $('#alerta-contactos').show();
  }
  if(totales.interesados > 0) {
    contador_interesados = totales.interesados;
    contador_total = parseInt(contador_total) + parseInt(totales.interesados);
    $('#alerta-interesados').html(contador_interesados);
    $('#alerta-interesados').show();
  }
  if(contador_total > 0) {
    $('#alerta-contactos-totales').html(contador_total);
    $('#alerta-contactos-totales').show();
  }
}

$(document).ready(function() {
  //Detalle de Espacios adquiridos
  $('div.row > div.VerMas').bind("mouseenter mouseleave", function(e) {
    var detalleId = $(this).attr('data-detalle-id');
    $('#'+detalleId).toggle();
    $('.msg-estado-cuenta').toggle();
  });
  //--
  $('div.ContentEspacio > div.VerMas').bind("mouseenter mouseleave", function(e) {
    var detalleId = $(this).attr('data-detalle-id');
    if($('#'+detalleId).attr('style') == 'display: none;')
      $('#'+detalleId).show();
    else
      $('#'+detalleId).hide();
  });
  $('div.grupo > div.VerMas').bind("mouseenter mouseleave", function(e) {
    var detalleId = $(this).attr('data-detalle-id');
    if($('#'+detalleId).attr('style') == 'display: none;')
      $('#'+detalleId).show();
    else
      $('#'+detalleId).hide();
  });
  $('h3.TitleMejoras').bind("click", function(e) {
    if($('.FlechaMejoras').hasClass("asc")){
      $('.FlechaMejoras').addClass("des");
      $('.FlechaMejoras').removeClass("asc");
    } else {
      $('.FlechaMejoras').addClass("asc");
      $('.FlechaMejoras').removeClass("des");
    }
    $('.ListaMejoras').toggle("slow");
  });
  
  //Mis Avisos - Select All
  $('#edit-objects-selector-wrapper').empty();
  $('#views-bulk-operations-select').parent().prepend('<div id="objects-selector-wrapper-new">Seleccionar: <a href="#" id="autos-vbo-todos">Todos</a> - <a href="#" id="autos-vbo-ninguno">Ninguno</a></div>');
  var formSelector = $('#views-bulk-operations-form--1');
  $('#autos-vbo-todos').click(function(event) {
    event.preventDefault();
    $('input:checkbox.vbo-select', formSelector).each(function() {
      this.checked = true;
    });
  });
  $('#autos-vbo-ninguno').click(function(event) {
    event.preventDefault();
    $('input:checkbox.vbo-select', formSelector).each(function() {
      this.checked = false;
    });
  });
  //En caso de error por publicación en paralelo, volvemos a enviar el formulario.
  var reenvio = $('#system-messages .publicacion_paralelo').html();
  if(reenvio!=null) {
    $('.messages.status').html('El servidor está ocupado en este momento. Procesando la publicación... Espere...');
    $('.messages.error').replaceWith('');
    setTimeout("$('#publicacion-avisos-form-opciones-publicacion').submit()", 3500);
  }
  $('#edit-vid').change(function(event) {
    event.preventDefault();
    $('.hierarchical-select-wrapper-wrapper').hide();
    if($('#edit-vid').val()=='1') {
      $('.hierarchical-select-wrapper-wrapper .hierarchical-select-wrapper-for-name-edit-marca-modelo-autos').parent().show();
    } else if($('#edit-vid').val()=='2') {
      $('.hierarchical-select-wrapper-wrapper .hierarchical-select-wrapper-for-name-edit-marca-modelo-motos').parent().show();
    } else if($('#edit-vid').val()=='3') {
      $('.hierarchical-select-wrapper-wrapper .hierarchical-select-wrapper-for-name-edit-marca-modelo-utilitarios').parent().show();
    } else if($('#edit-vid').val()=='4') {
      $('.hierarchical-select-wrapper-wrapper .hierarchical-select-wrapper-for-name-edit-marca-modelo-4x4').parent().show();
    }
  });
  $('#edit-vid').trigger("change");
  $('.hierarchical-select-wrapper-wrapper input').attr('readonly', true);
  //Boton responder, marca contacto como leido
  $('.contacto-respuesta').click(function(event) {
    var contacto = $(this).attr('id').split('-');
    if(contacto[2]){
      $.ajax({
        type: "GET",
        url: "/contacto/leido/" + contacto[1],
        success: function(data) {
          window.location.reload();
        }
      });
    }
  });
  //Boton responder, marca contacto como leido via ajax
  $('.contacto-respuesta-ajax').click(function(event) {
    var contacto = $(this).attr('id').split('-');
    ajax_contacto_leido(contacto[1]);
  });
  //Boton marca contacto como leido via ajax
  $('.contacto-leido-ajax').click(function(event) {
    var contacto = $(this).attr('id').split('-');
    ajax_contacto_leido(contacto[1]);
  });
  
  //Menu mas links
  $(".cuenta").hover(function () {
    $(this).children("ul.cuenta-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.cuenta-links").slideUp('fast');
  });
  $(".afiliados").hover(function () {
    $(this).children("ul.afiliados-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.afiliados-links").slideUp('fast');
  });
  $(".agenda").hover(function () {
    $(this).children("ul.agenda-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.agenda-links").slideUp('fast');
  });
  $(".mis-datos").hover(function () {
    $(this).children("ul.mis-datos-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.mis-datos-links").slideUp('fast');
  });
  $(".contactos").hover(function () {
    $(this).children("ul.contactos-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.contactos-links").slideUp('fast');
  });
  $(".tienda").hover(function () {
    $(this).children("ul.tienda-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.tienda-links").slideUp('fast');
  });
  $(".reservas").hover(function () {
    $(this).children("ul.reservas-links").slideDown('fast').show();
  }, function () {
    $(this).children("ul.reservas-links").slideUp('fast');
  });
  
  //Menu flotante
  if(typeof($.lockfixed) != 'undefined') {
    $.lockfixed(".menuSolapas", {
      offset: {
        top: 0,
        bottom: 0, //$(".pie").height() + sum_banner_top + 360,
        forcemargin: true
      }
    });
  }
  
  //Boton republicacion masiva
  $('.republicacion-masiva.republicar').click(function(event) {
    event.preventDefault();
    var uid = $(this).attr('id').split('-');
    ajax_republicacion_masiva(uid);
  });
  
  /* ESTADISTICAS GLOBALES */
  $('#estadisticas-globales-reporte').submit(function( event ) {
    var inputs = estadisticas_procesar_datos_form($(this).serializeArray());
    if(inputs.reporte == 'VistaTotales'){
      event.preventDefault();
      ajax_estadisticas_vistas_totales(inputs, 'inmuebles');
    }
    if(inputs.reporte == 'MasVisto'){
      event.preventDefault();
      ajax_estadisticas_mas_vistos(inputs, 'inmuebles');
    }
    if(inputs.reporte == 'Publicadas'){
      event.preventDefault();
      ajax_estadisticas_tipologias_publicadas(inputs, 'inmuebles');
    }
    if(inputs.reporte == 'MasDestacada'){
      event.preventDefault();
      ajax_estadisticas_mas_destacadas(inputs, 'inmuebles');
    }
    if(inputs.reporte == 'PrecioPromedio'){
      event.preventDefault();
      ajax_estadisticas_precios_promedio(inputs, 'inmuebles');
    }
  });
  $('#estadisticas-globales-reporte-autos').submit(function( event ) {
    var inputs = estadisticas_procesar_datos_form($(this).serializeArray());
    if(inputs.reporte == 'VistaTotales'){
      event.preventDefault();
      ajax_estadisticas_vistas_totales(inputs, 'autos');
    }
    if(inputs.reporte == 'MasVisto'){
      event.preventDefault();
      ajax_estadisticas_mas_vistos(inputs, 'autos');
    }
    if(inputs.reporte == 'Publicadas'){
      event.preventDefault();
      ajax_estadisticas_tipologias_publicadas(inputs, 'autos');
    }
    if(inputs.reporte == 'MasDestacada'){
      event.preventDefault();
      ajax_estadisticas_mas_destacadas(inputs, 'autos');
    }
    if(inputs.reporte == 'PrecioPromedio'){
      event.preventDefault();
      ajax_estadisticas_precios_promedio(inputs, 'autos');
    }
    
  });
  
  $('#filtroVisitasTotales a').click(function(event) {
    event.preventDefault();
    $('#filtroVisitasTotales a').removeClass('int');
    $(this).addClass('int');
    $('.contadorTotal').html('-');
    var cantidad_dias = $(this).attr('data-dias');
    $('#hiddenDiasVisitasTotales').val(cantidad_dias);
    $('#tipoReporte').val('VistaTotales');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    $('.visitasTiempo').html($(this).html());
    cxense_add_custom_parameters('cx_estadisticas', 'vistas_totales_' + cantidad_dias + '_dias');
  });
  
  
  $('#filtroDiasMasVisto a').click(function(event) {
    event.preventDefault();
    $('#filtroDiasMasVisto a').removeClass('int');
    $(this).addClass('int');
    var cantidad_dias = $(this).attr('data-dias');
    $('#hiddenDiasMasVisto').val(cantidad_dias);
    $('#tipoReporte').val('MasVisto');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    cxense_add_custom_parameters('cx_estadisticas', 'mas_vistos_' + cantidad_dias + '_dias');
  });
  $('#filtroCantidadMasVisto').change(function() {
    var cantidad_registros = $(this).val();
    $('#hiddenCantidadMasVisto').val(cantidad_registros);
    $('#tipoReporte').val('MasVisto');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
  });
  $('#filtroCantidadMasDestacada').change(function() {
    var cantidad_registros = $(this).val();
    $('#hiddenCantidadMasDestacada').val(cantidad_registros);
    $('#tipoReporte').val('MasDestacada');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
  });
  
  $(".dropdown dt a").click(function(event) {
    event.preventDefault();
    var contenedor = $(this).attr('data-select');
    $("#"+contenedor).slideToggle('fast');
  });
  $(".dropdown dd ul li a").click(function() {
    $(".dropdown dd ul").hide();
  });
  $('.mutliSelect input[type="checkbox"]').click(function() {
    var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(), title = $(this).attr('data-name') + ",";
    var contenedor = $(this).attr('data-content');
    if ($(this).is(':checked')) {
      var html = '<span title="' + title + '">' + title + '</span>';
      $('#'+contenedor+' .multiSel').append(html);
      $('#'+contenedor+' .hida').hide();
      $(this).parent().css('background-color','#008ACF');
      $('#'+contenedor).addClass('filterSelected');
      // Mostrar barrios de cordoba
      if($(this).context.value == 'Córdoba')
        $('.barriosCordoba').show();
      // Mostrar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).show();
      }      
    } else {
      $('span[title="' + title + '"]').remove();
      var ret = $('#'+contenedor+' .hida');
      $('.dropdown dt a#'+contenedor).append(ret);
      if($('#'+contenedor+' .multiSel').html() == '') {
        $('#'+contenedor+' .hida').show();
        $('#'+contenedor).removeClass('filterSelected');
      }
      $(this).parent().css('background-color','initial');
      // Ocultar barrios de cordoba
      if($(this).context.value == 'Córdoba') {
        $('.barriosCordoba').hide();
        $('.barriosCordoba input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });        
      }
      // Ocultar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).hide();
        $('.'+clase+' input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });
      }
    }
  });
  $('.mutliSelectPublicadas input[type="checkbox"]').click(function() {
    var title = $(this).closest('.mutliSelectPublicadas').find('input[type="checkbox"]').val(), title = $(this).attr('data-name') + ",";
    var contenedor = $(this).attr('data-content');
    if ($(this).is(':checked')) {
      var html = '<span title="' + title + '">' + title + '</span>';
      $('#'+contenedor+' .multiSel').append(html);
      $('#'+contenedor+' .hida').hide();
      $(this).parent().css('background-color','#008ACF');
      $('#'+contenedor).addClass('filterSelected');
      // Mostrar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).show();
      } 
    } else {
      $('span[title="' + title + '"]').remove();
      var ret = $('#'+contenedor+' .hida');
      $('.dropdown dt a#'+contenedor).append(ret);
      if($('#'+contenedor+' .multiSel').html() == '') {
        $('#'+contenedor+' .hida').show();
        $('#'+contenedor).removeClass('filterSelected');
      }
      $(this).parent().css('background-color','initial');
      // Ocultar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).hide();
        $('.'+clase+' input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });
      }
    }
  });
  $('.mutliSelectDestacada input[type="checkbox"]').click(function() {
    var title = $(this).closest('.mutliSelectDestacada').find('input[type="checkbox"]').val(), title = $(this).attr('data-name') + ",";
    var contenedor = $(this).attr('data-content');
    if ($(this).is(':checked')) {
      var html = '<span title="' + title + '">' + title + '</span>';
      $('#'+contenedor+' .multiSel').append(html);
      $('#'+contenedor+' .hida').hide();
      $(this).parent().css('background-color','#008ACF');
      $('#'+contenedor).addClass('filterSelected');
      // Mostrar barrios de cordoba
      if($(this).context.value == 'Córdoba')
        $('.barriosCordoba').show();
      // Mostrar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).show();
      } 
    } else {
      $('span[title="' + title + '"]').remove();
      var ret = $('#'+contenedor+' .hida');
      $('.dropdown dt a#'+contenedor).append(ret);
      if($('#'+contenedor+' .multiSel').html() == '') {
        $('#'+contenedor+' .hida').show();
        $('#'+contenedor).removeClass('filterSelected');
      }
      $(this).parent().css('background-color','initial');
      // Ocultar barrios de cordoba
      if($(this).context.value == 'Córdoba') {
        $('.barriosCordoba').hide();
        $('.barriosCordoba input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });        
      }
      // Ocultar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).hide();
        $('.'+clase+' input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });
      }
    }
  });
  $('.mutliSelectPrecios input[type="checkbox"]').click(function() {
    var title = $(this).closest('.mutliSelectPrecios').find('input[type="checkbox"]').val(), title = $(this).attr('data-name') + ",";
    var contenedor = $(this).attr('data-content');
    if ($(this).is(':checked')) {
      
      // Localidades precio selector simple
      if(contenedor == 'multiLocalidadPrecios') {
        $(this).closest('.mutliSelectPrecios').find('li').each(function() {
          $(this).find('label input').attr('checked', false);
          $(this).find('label').css('background-color','initial');
          $('#'+contenedor+' .multiSel').html('');
          $('#'+contenedor).removeClass('filterSelected');
        });
        $(this).attr('checked', true);
      }
      
      var html = '<span title="' + title + '">' + title + '</span>';
      $('#'+contenedor+' .multiSel').append(html);
      $('#'+contenedor+' .hida').hide();
      $(this).parent().css('background-color','#008ACF');
      $('#'+contenedor).addClass('filterSelected');
      // Mostrar barrios de cordoba
      if($(this).context.value == 'Córdoba')
        $('.barriosCordoba').show();
      // Mostrar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).show();
      } 
    } else {
      $('span[title="' + title + '"]').remove();
      var ret = $('#'+contenedor+' .hida');
      $('.dropdown dt a#'+contenedor).append(ret);
      if($('#'+contenedor+' .multiSel').html() == '') {
        $('#'+contenedor+' .hida').show();
        $('#'+contenedor).removeClass('filterSelected');
      }
      $(this).parent().css('background-color','initial');
      // Ocultar barrios de cordoba
      if($(this).context.value == 'Córdoba') {
        $('.barriosCordoba').hide();
        $('.barriosCordoba input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });        
      }
      // Ocultar modelos de marca
      if($(this).context.id.indexOf('marca') != -1) {
        var clase = $(this).context.id;
        $('.'+clase).hide();
        $('.'+clase+' input[type="checkbox"]').each(function() {
          if ($(this).is(':checked')) {
            $(this).attr('checked', false);
            $(this).parent().css('background-color','initial');
            var titleBarrio = $(this).attr('data-name') + ",";
            $('span[title="' + titleBarrio + '"]').remove();
            if($('#'+contenedor+' .multiSel').html() == '') {
              $('#'+contenedor+' .hida').show();
              $('#'+contenedor).removeClass('filterSelected');
            }
          }
        });
      }
    }
  });
  $('.quitarAll').click(function() {
    var contenedor = $(this).attr('data-content');
    $(this).siblings('li').each(function() {
      $(this).find('label input').attr('checked', false);
      $(this).find('label').css('background-color','initial');
      $('#'+contenedor+' .hida').show();
      $('#'+contenedor+' .multiSel').html('');
      $('#'+contenedor).removeClass('filterSelected');
    });
    $(this).parent().slideUp('fast');
  });
  
  $('#filtroMasVisto').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('MasVisto');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    $(".mutliSelect ul").slideUp('fast');
    cxense_add_custom_parameters('estadisticas', 'filtroMasVisto');
  });
  $('#filtroPublicadas').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('Publicadas');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    $(".mutliSelectPublicadas ul").slideUp('fast');
    cxense_add_custom_parameters('estadisticas', 'filtroPublicadas');
  });
  $('#filtroMasDestacada').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('MasDestacada');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    $(".mutliSelectDestacada ul").slideUp('fast');
    cxense_add_custom_parameters('estadisticas', 'filtroMasDestacada');
  });
  $('#filtroPrecios').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('PrecioPromedio');
    if($('#estadisticas-globales-reporte').length > 0)
      $('#estadisticas-globales-reporte').submit();
    if($('#estadisticas-globales-reporte-autos').length > 0)
      $('#estadisticas-globales-reporte-autos').submit();
    $(".mutliSelectPrecios ul").slideUp('fast');
    cxense_add_custom_parameters('estadisticas', 'filtroPromedio');
  });
  
  $('#exportarMasVistos').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('MasVistoExportar');
    if($('#estadisticas-globales-reporte').length > 0) {
      $('#estadisticas-globales-reporte').attr('action', '/estadisticas_globales/exportar_excel/mas_vistos');
      $('#estadisticas-globales-reporte').submit();
    }
    if($('#estadisticas-globales-reporte-autos').length > 0) {
      $('#estadisticas-globales-reporte-autos').attr('action', '/estadisticas_globales/exportar_excel/mas_vistos');
      $('#estadisticas-globales-reporte-autos').submit();
    }
    cxense_add_custom_parameters('estadisticas', 'exportar_mas_vistos');
  });
  $('#exportarMasDestacada').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('MasDestacadaExportar');
    if($('#estadisticas-globales-reporte').length > 0) {
      $('#estadisticas-globales-reporte').attr('action', '/estadisticas_globales/exportar_excel/mas_destacadas');
      $('#estadisticas-globales-reporte').submit();
    }
    if($('#estadisticas-globales-reporte-autos').length > 0) {
      $('#estadisticas-globales-reporte-autos').attr('action', '/estadisticas_globales/exportar_excel/mas_destacadas');
      $('#estadisticas-globales-reporte-autos').submit();
    }
    cxense_add_custom_parameters('estadisticas', 'exportar_mas_destacada');
  });
  $('#exportarPrecios').click(function(event) {
    event.preventDefault();
    $('#tipoReporte').val('PrecioPromedioExportar');
    if($('#estadisticas-globales-reporte').length > 0) {
      $('#estadisticas-globales-reporte').attr('action', '/estadisticas_globales/exportar_excel/precios_promedio');
      $('#estadisticas-globales-reporte').submit();
    }
    if($('#estadisticas-globales-reporte-autos').length > 0) {
      $('#estadisticas-globales-reporte-autos').attr('action', '/estadisticas_globales/exportar_excel/precios_promedio');
      $('#estadisticas-globales-reporte-autos').submit();
    }
    cxense_add_custom_parameters('estadisticas', 'exportar_precio_promedio');
  });
  
  $('.recomendacion.globales').click(function(event) {
    $(this).children('p').slideToggle("slow");
    if($(this).hasClass('close')) {
      $(this).removeClass('close');
      $(this).addClass('open');
    } else {
      $(this).removeClass('open');
      $(this).addClass('close');
    }
  });
  
  // Colapsar interesados
  $('.desplegar-interesados').click(function(event) {
    var aviso = $(this).attr('data-id');
    $('.on-' + aviso).hide('');
    $('.off-' + aviso).show('');
  });
  $('.colapsar-interesados').click(function(event) {
    var aviso = $(this).attr('data-id');
    $('.off-' + aviso).hide('');
    $('.on-' + aviso).show('');
  });
  
  // Estado interesados
  $('.img-select').click(function(event) {
    var id = $(this).attr('data-id');
    $('#estado-' + id).hide('');
    $(this).hide('');
    $('#select-estado-' + id).show('');
    $('#select-estado-' + id).focus();
    $('#select-estado-' + id).blur(function(event) {
      $(this).hide('');
      $('#estado-' + id).show('');
      $('.img-select').show('');
    });
  });
  $('.estado-interesado').change(function(event) {
    cambio_estado_interesado($(this).attr('data-id'), $(this).val());
  });
  
  // Matricula Profesionales Servicios
  if($('#matricula-profesional').length > 0){
    setInterval(function(){
      if($('#edit-taxonomy-34-hierarchical-select-selects-1 option:selected').text() == 'Oficios y Profesiones') {
        $('#matricula-profesional').show();
      } else {
        $('#matricula-profesional').hide();
      }
    }, 3000);
  }
  
  // Cookie ultimo acceso listado de contactos 
  if($('.page-contactos-recibidos').length > 0) {
    var totales = '';
    if (Cookies.get('clviAlertaContactos'))
      totales = JSON.parse(Cookies.get('clviAlertaContactos'));
    totales['ingresoContactos'] = Math.floor(Date.now() / 1000);
    totales['contactos'] = 0;
    $stringify = JSON.stringify(totales);
    Cookies.set('clviAlertaContactos', $stringify);
  }
  // Cookie ultimo acceso listado de interesados 
  if($('.page-administrar-contactos-interesados').length > 0) {
    var totales = '';
    if (Cookies.get('clviAlertaContactos'))
      totales = JSON.parse(Cookies.get('clviAlertaContactos'));
    totales['ingresoInteresados'] = Math.floor(Date.now() / 1000);
    totales['interesados'] = 0;
    $stringify = JSON.stringify(totales);
    Cookies.set('clviAlertaContactos', $stringify);
  }
  
  // Alerta nuevos contactos
  if (Cookies.get('login') && Cookies.get('login') != 0) {
    var user = Cookies.get('login');
    var acceso_dia = Math.floor(Date.now() / 1000) - 86400; // -1 dia
    var diferencia_minutos = Math.floor(Date.now() / 1000) - 900; // -15 minutos
    var acceso_contactos = acceso_dia;
    var acceso_interesados = acceso_dia;
    // Existe cookie?
    if (Cookies.get('clviAlertaContactos')) {
      var alerta = JSON.parse(Cookies.get('clviAlertaContactos'));
      // Ingreso admin seteado?
      if(typeof alerta.ingreso !== 'undefined' && alerta.ingreso != '') {
        // Pasaron 15 minutos ultimo seteo de ingreso?
        if(alerta.ingreso < diferencia_minutos) {
          if(typeof alerta.ingresoContactos !== 'undefined' && alerta.ingresoContactos != '')
            acceso_contactos = alerta.ingresoContactos;
          if(typeof alerta.ingresoInteresados !== 'undefined' && alerta.ingresoInteresados != '')
            acceso_interesados = alerta.ingresoInteresados;
          alertas_nuevos_contactos(user, acceso_contactos, acceso_interesados);
        } else {
          alertas_mostrar_nuevos_contactos(alerta);
        }
      } else {
        if(typeof alerta.ingresoContactos !== 'undefined' && alerta.ingresoContactos != '')
          acceso_contactos = alerta.ingresoContactos;
        if(typeof alerta.ingresoInteresados !== 'undefined' && alerta.ingresoInteresados != '')
          acceso_interesados = alerta.ingresoInteresados;
        alertas_nuevos_contactos(user, acceso_contactos, acceso_interesados);
      }
    } else {
      alertas_nuevos_contactos(user, acceso_dia, acceso_dia);
    }
  }
  
});
