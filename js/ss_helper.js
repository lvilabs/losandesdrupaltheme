/**
* ss_helper js.
*/

/* Espacio para atributos y configuraciones de la página */
WebPage = window.WebPage || {};

//Paginador avisos premium block derecho
WebPage.TiempoRecargaPremiumsBlock = 30000; //miliseconds
WebPage.RecargarPremiumsBlockTimerId = false;
WebPage.CargarPremiumsBlock = function () {

  if(WebPage.RecargarPremiumsBlockTimerId) {
    clearTimeout(WebPage.TimerId);
    WebPage.RecargarPremiumsBlockTimerId = false;
  }
  var content_area = $('#destacados-block');
  var paginador = $('.pager-current');
  var pagina_to_load = 1;
  var paginas = new Array();
  var new_content = null;
  var old_content = null;

  if(paginador.length > 0) {
    paginas = paginador.html().split(" of ");
    pagina_to_load = paginas[0];
  }
  url_final = 'views/ajax?js=1&page='+pagina_to_load+'&view_name=listado_publicaciones&view_display_id=block_1&view_args=&view_path=registro-particular&view_base_path=home&view_dom_id=1&pager_element=0'

  $.ajax({
    url: url_final,
    dataType: 'json',
    success: function(data) {
      new_content = $(data.display);
      if(new_content.length <= 0) {
        return;
      }
      new_content.css('display', 'none');
      old_content = content_area.find('div:first');
      content_area.append(new_content);
      old_content.fadeOut(1000, function() {
        old_content.remove();
        new_content.fadeIn(1000, function() {
          WebPage.RecargarPremiumsBlockTimerId = setTimeout("WebPage.CargarPremiumsBlock()", WebPage.TiempoRecargaPremiumsBlock);
        });
      });
      $(".pager").hide("");
    }
  });
};

WebPage.ConfigurarBuscadorEnHeader = function () {

  var elForm = $('#buscador_header');
  var inpBuscarTexto = elForm.find('input[name="bsc_palabra"]');
  var inpBuscarSitio = elForm.find('select[name="bsc_sitio"]');

  if(elForm.length <= 0 || inpBuscarTexto.length <= 0 || inpBuscarSitio.length <= 0) {
    return; //no existe el formulario de búsqueda.
  }

  //Hacemos que aparezca/desaparezca el texto por defecto
  var EmptyText = inpBuscarTexto.attr('data-default-text') || '';
  if(inpBuscarTexto.val() == '') {
    inpBuscarTexto.val(EmptyText);
  }
  inpBuscarTexto.focus(function() {
    if(inpBuscarTexto.val() == EmptyText) {
      inpBuscarTexto.css('color', 'black');
      inpBuscarTexto.val('');
    }
  });
  inpBuscarTexto.blur(function() {
    if(inpBuscarTexto.val() == '') {
      inpBuscarTexto.css('color', 'gray');
      inpBuscarTexto.val(EmptyText);
    }
  });

  //Comportamiento del formulario
  elForm.submit(function(event) {
    event.preventDefault();
    var Text2Search = '';
    var uriSearch = '';
    var RubrosTIDs = Drupal.settings.RubrosTIDs;
    var RubroKeySelected = '';
    if(inpBuscarTexto.val() != EmptyText) {
      Text2Search = inpBuscarTexto.val();
      Text2Search = encodeURIComponent(Text2Search);
    }
    var home = '';
    if(window.location.pathname=='/')
        home = 'home';
    else if(window.location.pathname=='/autos')
        home = 'autos';
    else if(window.location.pathname=='/inmuebles')
        home = 'inmuebles';
    else if(window.location.pathname=='/productos')
        home = 'productos';
    if(home!='')
      home = '_'+home;

    RubroKeySelected = inpBuscarSitio.val();
    switch(RubroKeySelected) {
      case 'empleos':
        var cx_level = '&cx_level=buscador_general'+home;
        uriSearch = 'http://empleos.clasificadoslavoz.com.ar/buscar?key=' + Text2Search+ cx_level;
        break;
      default:
        if(! RubroKeySelected /*|| ! RubrosTIDs.hasOwnProperty(RubroKeySelected)*/) {
          var cx_level = '?cx_level=buscador_general'+home;
          uriSearch = '/search/apachesolr_search/' + Text2Search + cx_level;
        } else {
          var cx_level = '&cx_level=buscador_general'+home;
          uriSearch = '/search/apachesolr_search/' + Text2Search + '?f[0]=im_taxonomy_vid_34:' + RubroKeySelected+ cx_level;
        }
    }
    location.href = uriSearch;
  });

};
//How can I test for native support of the placeholder attribute?
//http://stackoverflow.com/questions/3937818/jquery-how-to-test-is-the-browser-supports-the-native-placeholder-attribute
jQuery.support.placeholder = (function(){
  var i = document.createElement('input');
  return 'placeholder' in i;
})();