/**
 * Permite que el Menú de Servicios del Header Global La Voz sea desplegable.
 */
var desplegar_red_sitios = false;
var desplegar_red_autos = false;
var desplegar_red_inmuebles = false;
var desplegar_red_productos = false;
var desplegar_red_servicios = false;
var desplegar_login = false;
jQuery(document).ready(function(){
  jQuery('#menuServicios').mouseover(function(){
    jQuery('div.Secciones').show();
    jQuery('.Hdredlvi li#menuServicios a.desplegableServicios').css({'background-color':'#fff','-moz-border-radius':'4px 4px 0 0','-webkit-border-radius':'4px 4px 0 0','color':'#333B4E','text-decoration':'none'});
  });				
  jQuery('#menuServicios').mouseout(function(){
    jQuery('div.Secciones').hide();
    jQuery('.Hdredlvi li#menuServicios a.desplegableServicios').css({'background':'url("http://www.clasificadoslavoz.com.ar/sites/clasificadoslavoz.com.ar/themes/principal/img/ic_Flechita_desplegable.gif") no-repeat scroll right 6px transparent','margin-right':'5px','padding-right':'20px','color':'#FFFFFF'});
  });
  jQuery('.red-sitios').mouseenter(function() {
    desplegar_red_sitios = true;
    jQuery('.red-sitios .dropdown-menu').show();
  });
  jQuery('.red-sitios').mouseleave(function() {
    desplegar_red_sitios = false;
    setTimeout('cerrar_submenu(\'.red-sitios .dropdown-menu\', \'red_sitios\');', 250);
  });
  $('.region-login-superior').live('mouseover', function() {
      desplegar_login = true;
      jQuery('.region-login-superior .dropdown-login').show();
  });
  $('.region-login-superior').live('mouseout', function() {
      desplegar_login = false;
      setTimeout('cerrar_submenu(\'.region-login-superior .dropdown-login\', \'login\');', 250);
  });
  jQuery('.red-autos').mouseenter(function() {
    desplegar_red_autos = true;
    jQuery('.red-autos .dropdown-menu').show();
  });
  jQuery('.red-autos').mouseleave(function() {
    desplegar_red_autos = false;
    setTimeout('cerrar_submenu(\'.red-autos .dropdown-menu\', \'red-autos\');', 150);
  });
  jQuery('.red-inmuebles').mouseenter(function() {
    desplegar_red_inmuebles = true;
    jQuery('.red-inmuebles .dropdown-menu').show();
  });
  jQuery('.red-inmuebles').mouseleave(function() {
    desplegar_red_inmuebles = false;
    setTimeout('cerrar_submenu(\'.red-inmuebles .dropdown-menu\', \'red-inmuebles\');', 150);
  });
  jQuery('.red-productos').mouseenter(function() {
    desplegar_red_productos = true;
    jQuery('.red-productos .dropdown-menu').show();
  });
  jQuery('.red-productos').mouseleave(function() {
    desplegar_red_productos = false;
    setTimeout('cerrar_submenu(\'.red-productos .dropdown-menu\', \'red-productos\');', 150);
  });
  jQuery('.red-servicios').mouseenter(function() {
    desplegar_red_servicios = true;
    jQuery('.red-servicios .dropdown-menu').show();
  });
  jQuery('.red-servicios').mouseleave(function() {
    desplegar_red_servicios = false;
    setTimeout('cerrar_submenu(\'.red-servicios .dropdown-menu\', \'red-servicios\');', 150);
  });
});

function cerrar_submenu(elemento, nombre) {
  if(desplegar_red_sitios && nombre=='red_sitios')
    return;
  if(desplegar_red_autos && nombre=='red-autos')
    return;
  if(desplegar_red_inmuebles && nombre=='red-inmuebles')
    return;
  if(desplegar_red_productos && nombre=='red-productos')
    return;
  if(desplegar_red_servicios && nombre=='red-servicios')
    return;
  if(desplegar_login && nombre=='login')
    return;
  $(elemento).fadeOut('fast', function() {
  });
}