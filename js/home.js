WebPage.TiempoRecargaParaPremiums = 30000; //30000 miliseconds
WebPage.RecargarPremiumsTimerId = false;
WebPage.CargarPremiums = function () {
  
  if(WebPage.RecargarPremiumsTimerId) {
    clearTimeout(WebPage.RecargarPremiumsTimerId);
    WebPage.RecargarPremiumsTimerId = false;
  }
  
  WebPage.RecargarPremiumsTimerId = setTimeout("WebPage.CargarPremiums()", WebPage.TiempoRecargaParaPremiums);
  
  //Trato de deducir en que home me encuentro: Clasificados Home, Pre Home Autos o Pre Home Inmuebles
  var view_display_id = '';
  var view_path = '';
  if($('.view-display-id-page_1').length > 0) {
    view_display_id = 'page_1';
    view_path = 'home';
  } else if ($('.view-display-id-page_2').length > 0) {
    view_display_id = 'page_2';
    view_path = 'autos';
  } else if ($('.view-display-id-page_3').length > 0) {
    view_display_id = 'page_3';
    view_path = 'inmuebles';
  } else {
    $('div.contentAvisos').fadeOut(1000, function() { $('div.contentAvisos').children().remove(); Losandes.DestacadosRequestData();});
    return; //exit porque no existe ninguna vista conocida
  }

  var content_area = $('#content-area');
  var paginador = $('.pager-current');
  var pagina_to_load = 1;
  var paginas = new Array();
  var new_content = null;
  var old_content = null;

  if(paginador.length > 0) {
    paginas = paginador.html().split(" of ");
    pagina_to_load = paginas[0];
  }

  url_final = 'views/ajax?js=1&page='+pagina_to_load+'&view_name=listado_publicaciones&view_display_id='+view_display_id+'&view_args=&view_path='+view_path+'&view_base_path='+view_path+'&view_dom_id=1&pager_element=0'

  $.ajax({
    url: url_final,
    dataType: 'json',
    success: function(data) {
      new_content = $(data.display);
      if(new_content.length <= 0) {
        return;
      }
      new_content.css('display', 'none');
      old_content = content_area.find('div:first');
      content_area.append(new_content);
      old_content.fadeOut(1000, function() {
        old_content.remove();
        new_content.fadeIn(1000, function() {
          WebPage.RecargarPremiumsTimerId = setTimeout("WebPage.CargarPremiums()", WebPage.TiempoRecargaParaPremiums);
        });
      });
    }
  });
}

function obtener_marcas_por_rubro(rubro) {
  $.post("carga-select-marca",
    {id_tipo:rubro},
    function(data){
      $("#select_marcas").html(data);
      
      $("#modelo").html('<option value="0" selected="selected">Todos</option>');
      $("#marca").change(function(){
        if ($(this).val() != 0){
          obtener_modelos_por_marca($(this).val(), rubro);
        } 
      });
    }
  );
  if (Drupal.settings.rubro_tiene_modelo[rubro])
    $("#modelo").show("slow");
  else
    $("#modelo").hide("slow");
}
function obtener_modelos_por_marca(marca, rubro) {
  if (Drupal.settings.rubro_tiene_modelo[rubro]){
    $.post("carga-select-modelos",
      {id_marca:marca,id_rubro:rubro},
      function(data){
        $("#select_modelos").html(data);
      }
    ); 
  }
}
function obtener_operaciones_por_rubro(rubro) {
  var select = '<select id="operacion" class="select-negro">';
  if(rubro==Drupal.settings.nid_rubro_emprendimientos) {
    select += '<option value="Venta" selected="selected">Venta</option>';
  } else {
    select += '<option value="" selected="selected">Todos</option>';
    select += '<option value="Venta">Venta</option>';
    select += '<option value="Alquileres">Alquileres</option>';
    if(rubro!=6331 && rubro!=6334) {
      select += '<option value="Alquileres Temporarios">Alquileres Temporarios</option>';
    }
    select += '<option value="Tiempo Compartido">Tiempo Compartido</option>';
  }
  select += '</select>';
  $("#select_operaciones").html(select);
}
$(document).ready(function(){
  WebPage.RecargarPremiumsTimerId = setTimeout("WebPage.CargarPremiums()", WebPage.TiempoRecargaParaPremiums);
  
  if($("#marca").val() !== undefined){
    //Obtener marcas del rubro inicial
    obtener_marcas_por_rubro($("#tipos").val());
    //Onchange como de rubros
    $("#tipos").change(function(){
      obtener_marcas_por_rubro($(this).val());
    });
  }
  
  if($("#operacion").val() !== undefined){
    //Obtener operaciones del rubro inicial
    obtener_operaciones_por_rubro($("#tipos").val());
    //Onchange combo de rubros
    $("#tipos").change(function(){
      obtener_operaciones_por_rubro($(this).val());
    });
  }
  
  var el_buscador_text = $('#texto_busqueda');
  var buscador_empty_text = 'Buscar por palabra';
  if(el_buscador_text.val() == '') {
    el_buscador_text.val(buscador_empty_text);
  }
  el_buscador_text.focus(function() {
    if(el_buscador_text.val() == buscador_empty_text) {
      el_buscador_text.css('color', 'black');
      el_buscador_text.val('');
    }
  });
  el_buscador_text.blur(function() {
    if(el_buscador_text.val() == '') {
      el_buscador_text.css('color', 'gray');
      el_buscador_text.val(buscador_empty_text);
    }
  });

  //Click del boton a buscar
  $("#boton_buscar").click(function() {
    $('#buscador_home').submit();
  });
  $("#buscador_home").submit(function(event) {
    event.preventDefault();
    var controlesTID = ['#rubro-padre', '#tipos', '#marca', '#modelo'];
    var filtro = [];
    var control = null;
    var ctrOperacion = $("#operacion");
    for (var i=0; i < controlesTID.length; i++) {
      control = $(controlesTID[i]);
      if(control.length > 0 && control.val() > 0) {
        if(control.selector == '#tipos' || control.selector == '#rubro-padre') {
          filtro.push('im_taxonomy_vid_34:' + control.val());
        } else {
          if(control.selector == '#marca' || control.selector == '#modelo') {
            var vid = 0;
            switch($(controlesTID[1]).val()){
              // 4x4
              case '6325':
                vid = 4;
                break;
              // Repuestos
              case '6329':
                vid = 15;
                break;
              // Autos
              case '6324':
                vid = 1;
                break;
              // Motos
              case '6326':
                vid = 2;
                break;
              // Planes
              case '6328':
                vid = 14;
                break;
              // Utilitarios
              case '6327':
                vid = 3;
                break;
            }
            if(vid != 0)
              filtro.push('im_taxonomy_vid_' + vid + ':' + control.val());
          }
        } 
      }
    }
    if (ctrOperacion.length > 0 && ctrOperacion.val().length > 0) {
      filtro.push('ss_operacion:'+ctrOperacion.val()+'');
    }
    var home = 'autos';
    if(filtro.length == 0) {
      $("#filtros").remove();
      var cx_level = '?cx_level=buscador_home_'+home;
      location.href = $("#buscador_home").attr("action")+cx_level;
    } else {
      var text_filtros = '';
      for(i in filtro){
        if(text_filtros == '')
          text_filtros = '?f[' + i + ']=' + filtro[i];  
        else
          text_filtros = text_filtros + '&f[' + i +']=' + filtro[i];  
      }
      var cx_level = '&cx_level=buscador_home_'+home;
      location.href = $("#buscador_home").attr("action") + text_filtros + cx_level;
    }
  });

  $("#mostrar_buscador_clasifotos").click(function() {
    $(this).hide();
    $('#buscador_clasifotos').show();
  });

  //DOMINIO
  var el_dominio_text = $('#dominio');
  var dominio_empty_text = 'Dominio';
  if(el_dominio_text.val() == '') {
    el_dominio_text.val(dominio_empty_text);
  }
  el_dominio_text.focus(function() {
    if(el_dominio_text.val() == dominio_empty_text) {
      el_dominio_text.val('');
    }
  });
  el_dominio_text.blur(function() {
    if(el_dominio_text.val() == '') {
      el_dominio_text.val(dominio_empty_text);
    }
  });
  
  //Mostrar historial de avisos visitados en home
  if($('body.front').length) {
    var history = JSON.parse(localStorage.getItem('history'));
    if (history !== null) {
      var categorias = [];
      var nids = [];
      var count = 0;
      for (var i=0; i<history.length; i++) { 
        /* var html = '';
        html += '<li><div class="CajaAviso "><div class="imgAviso">';
        html += '<a href="' + history[i]['url'] + '?cx_level=relacionados_home" title="' + history[i]['titulo'] + '"> <img src="' + history[i]['imagen'] + '" alt="Imagen del aviso" title="' + history[i]['titulo'] + '" class="imagecache" width="314" height="211"> </a>';
        html += '</div><div class="AvisoDescripcion">';
        html += '<h5><a href="' + history[i]['url'] + '?cx_level=relacionados_home" title="' + history[i]['titulo'] + '">' + history[i]['titulo'] + '</a></h5>';
        html += '<span class="precio">' + history[i]['precio'].replace(/<br>/g, " ") + '</span>';        
        html += '</div></div></li>';          
        $('.bloque_historial_avisos').prepend(html); */
        if(count >= 3) break;
        categorias[i] = history[i]['category'];
        nids[i] = history[i]['nid'];
        count = count + 1;
      }
      categorias.reverse();
      var lista_categorias = '';
      categorias.forEach(function(categoria){
        if(lista_categorias == '')
          lista_categorias = categoria;
        else 
          lista_categorias = lista_categorias + ',' + categoria;
      });
      var lista_nids = '';
      nids.forEach(function(nid){
        if(lista_nids == '')
          lista_nids = nid;
        else 
          lista_nids = lista_nids + ',' + nid;
      });
      // Mostrar avisos relacionados a los del historial
      $.ajax({
        type: "POST",
        url: '/ajax-block-relacionados-historial-usuario', 
        data: {categorias: lista_categorias, nids : lista_nids, interna : 'front'},
        dataType: 'html',
        success: function(data) {
          if(data != '')
            $('#block-clasificados-historial_visitas_usuario').prepend(data);
        }
      });
    } else {
      // Mostrar productos vendibes mas vistos
      $.ajax({
        type: "POST",
        url: '/ajax-block-relacionados-sin-historial', 
        data: {interna : 'front'},
        dataType: 'html',
        success: function(data) {
          if(data != '')
            $('#block-clasificados-historial_visitas_usuario').prepend(data);
        }
      });
    }
  }
  
  //Mostrar historial de avisos visitados en home tienda
  if($('body.page-productos').length) {
    var interna = '';
    var sin_historial = 0;
    var history = JSON.parse(localStorage.getItem('history'));
    if (history !== null) {
      var categorias = [];
      var nids = [];
      var count = 0;
      for (var i=0; i<history.length; i++) { 
        if(count >= 3) break;
        if(history[i]['category'] != undefined) {
          var array_categorias = history[i]['category'].split("/");
          if(array_categorias[1] == 'productos') {
            categorias[i] = history[i]['category'];
            nids[i] = history[i]['nid'];
            count = count + 1;
          }
        }
      }
      categorias.reverse();
      var lista_categorias = '';
      categorias.forEach(function(categoria){
        if(lista_categorias == '')
          lista_categorias = categoria;
        else 
          lista_categorias = lista_categorias + ',' + categoria;
      });
      var lista_nids = '';
      nids.forEach(function(nid){
        if(lista_nids == '')
          lista_nids = nid;
        else 
          lista_nids = lista_nids + ',' + nid;
      });
      if(lista_nids != '') {
        // Mostrar avisos relacionados a los del historial
        $.ajax({
          type: "POST",
          url: '/ajax-block-relacionados-historial-usuario', 
          data: {categorias: lista_categorias, nids : lista_nids, interna : 'productos'},
          dataType: 'html',
          success: function(data) {
            if(data != '')
              $('#block-clasificados-historial_visitas_usuario').prepend(data);
          }
        });
      } else {
        sin_historial = 1;
      }
    } else {
      sin_historial = 1;
    }
    if(sin_historial) {
      // Mostrar productos vendibes mas vistos
      $.ajax({
        type: "POST",
        url: '/ajax-block-relacionados-sin-historial', 
        data: {interna : 'productos'},
        dataType: 'html',
        success: function(data) {
          if(data != '')
            $('#block-clasificados-historial_visitas_usuario').prepend(data);
        }
      });
    }
  }
  
  //Mostrar historial de avisos visitados en home alojamientos
  if($('body.page-alojamientos').length) {
    var interna = '';
    var sin_historial = 0;
    var history = JSON.parse(localStorage.getItem('history'));
    if (history !== null) {
      var categorias = [];
      var nids = [];
      var count = 0;
      for (var i=0; i<history.length; i++) { 
        if(count >= 3) break;
        if(history[i]['category'] != undefined) {
          var array_categorias = history[i]['category'].split("/");
          if(array_categorias[1] == 'alojamientos') {
            categorias[i] = history[i]['category'];
            nids[i] = history[i]['nid'];
            count = count + 1;
          }
        }
      }
      categorias.reverse();
      /*var lista_categorias = '';
      categorias.forEach(function(categoria){
        if(lista_categorias == '')
          lista_categorias = categoria;
        else 
          lista_categorias = lista_categorias + ',' + categoria;
      });*/
      var lista_nids = '';
      nids.forEach(function(nid){
        if(lista_nids == '')
          lista_nids = nid;
        else 
          lista_nids = lista_nids + ',' + nid;
      });
      if(lista_nids != '') {
        // Mostrar avisos relacionados a los del historial
        $.ajax({
          type: "POST",
          url: '/ajax-block-relacionados-historial-usuario', 
          data: {categorias: categorias[0], nids : lista_nids, interna : 'alojamientos'},
          dataType: 'html',
          success: function(data) {
            if(data != '') {
              $('.contentAvisosAlojamientos').html(data);
            } else {
              // Mostrar alojamientos destacados
              $.ajax({
                type: "POST",
                url: '/ajax-block-relacionados-sin-historial', 
                data: {interna : 'alojamientos'},
                dataType: 'html',
                success: function(data) {
                  if(data != '')
                    $('.contentAvisosAlojamientos').html(data);
                }
              });
            }
          }
        });
      } else {
        sin_historial = 1;
      }
    } else {
      sin_historial = 1;
    }
    if(sin_historial) {
      // Mostrar alojamientos destacados
      $.ajax({
        type: "POST",
        url: '/ajax-block-relacionados-sin-historial', 
        data: {interna : 'alojamientos'},
        dataType: 'html',
        success: function(data) {
          if(data != '')
            $('.contentAvisosAlojamientos').html(data);
        }
      });
    }
  }
  
  //Modal info tiendas
  $(".Info span").hover(function() {
    var tienda = $(this).attr("class");
    setTimeout(function() {
      $('.tienda-' + tienda).css("height", "initial");
      $('.' + tienda).hide("");
    },1000);
  });
  
  // Carrusel responsive con paginador
  $(".rslides").responsiveSlides({
    auto: true,             // Boolean: Animate automatically, true or false
    speed: 500,             // Integer: Speed of the transition, in milliseconds
    timeout: 5000,          // Integer: Time between slide transitions, in milliseconds
    pager: true,            // Boolean: Show pager, true or false
    nav: false,             // Boolean: Show navigation, true or false
    random: false,          // Boolean: Randomize the order of the slides, true or false
    pause: false,           // Boolean: Pause on hover, true or false
    pauseControls: true,    // Boolean: Pause when hovering controls, true or false
    prevText: "Previous",   // String: Text for the "previous" button
    nextText: "Next",       // String: Text for the "next" button
    maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
    navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
    manualControls: "",     // Selector: Declare custom pager navigation
    namespace: "rslides",   // String: Change the default namespace used
    before: function(){},   // Function: Before callback
    after: function(){}     // Function: After callback
  });
  
  // Carrusel responsive con navegacion
  $(".rslidesNav").responsiveSlides({
    auto: true,
    speed: 500,
    timeout: 5000, 
    pager: false,
    nav: true,
    namespace: "callbacks",
    before: function(){},
    after: function(){}
  });
  
});

function eliminarHistorial(){
  localStorage.removeItem('history');
  $('#block-clasificados-historial_visitas_usuario').hide('');
  $.colorbox.close();
}

