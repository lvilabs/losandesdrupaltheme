<?php
  //Busco rubros de avisos
  $array_rubros = clasificados_rubros_obtener_rubros('aviso-auto');
  $tid_rubro = current($array_rubros);
  $rubro_padre = current(taxonomy_get_parents($tid_rubro->tid));
?>
<input type="hidden" value="<?php print $rubro_padre->tid; ?>" id="rubro-padre" name="rubro-padre" />
<form action="/search/apachesolr_search" method="get" id="buscador_home">
  <input type="hidden" value="" id="filtros" name="filters" />
  <fieldset>
    <select id="tipos" class="select-negro">
      <option value="0">Todos</option>
      <?php foreach($array_rubros as $key => $rubro){ ?>
        <option value="<?php print $key; ?>" class=""><?php print $rubro->title; ?></option>
      <?php } ?>
    </select>
  </fieldset>
  <fieldset>
    <div id="select_marcas">
      <select id="marca" class="select-negro">
        <option value="0">Todos</option>
      </select>
    </div>
  </fieldset>
  <fieldset>
    <div id="select_modelos">
      <select id="modelo" class="select-negro">
        <option value="0" selected="selected">Todos</option>								
      </select>
    </div>
  </fieldset>
  <div class="Boton">
    <div class="Cv Tl"></div>
    <div class="Cv Tr"></div>
    <div class="Cv Bl"></div>
    <div class="Cv Br"></div>
    <input type="button" value="Buscar" id="boton_buscar" class="boton-naranja" />
  </div>
</form>