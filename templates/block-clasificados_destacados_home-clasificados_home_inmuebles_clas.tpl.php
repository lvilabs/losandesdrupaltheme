<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="AvisosDestacadosSlide clearfix">
    <div class="Title">
    	<hr>
      <a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330" title="Avisos destacados de inmuebles" rel="search"><h2>Inmuebles destacados</h2></a>
    </div>
    <div class="navigation">
      <ul>
        <li><strong>Buscar por</strong></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6334" title="Departamentos" rel="search">Departamentos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6331" title="Casas" rel="search">Casas</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6333" title="Terrenos y Lotes" rel="search">Terrenos y Lotes</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6335" title="Locales" rel="search">Locales</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6336" title="Oficinas" rel="search">Oficinas</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6339" title="Cocheras" rel="search">Cocheras</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6337" title="Galpones y Depósitos" rel="search">Galpones y Depósitos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=im_taxonomy_vid_34:6332" title="Emprendimientos" rel="search">Emprendimientos</a></li>
      </ul>
      <div class="destacados-ver-mas"><a href="/inmuebles" title="Ver más avisos de inmuebles">Ver Más</a></div>
    </div>
    <div class="accessible_news_slider slider_tienda">
<?php if(count($content['avisos'])>3) : ?>
      <div class="Botones">
        <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
      </div>
<?php endif; ?>
      <div class="lista">
        <ul class="clearfix bloque_avisos <?php (count($content['avisos'])>3)? print 'Slider' : print 'no-Slider';?>">
<?php for($i=0; $i<count($content['avisos']); $i++) : ?>
          <li>
            <div class="CajaAviso ">
              <a href="<?php print $content['avisos'][$i]['url']; ?>?cx_level=destacados_home_inmuebles" title="<?php print $content['avisos'][$i]['titulo']; ?>">
                <div class="imgAviso">
                  <?php
                    $alt_img = explode('/', $content['avisos'][$i]['url']);
                    $alt_img = $alt_img[0].' de '.$alt_img[1].' '.$content['avisos'][$i]['titulo'].' a '.$content['avisos'][$i]['precio'];
                  ?>
                  <img src="<?php print $content['avisos'][$i]['foto']; ?>" alt="<?php print $alt_img; ?>" title="<?php print $content['avisos'][$i]['titulo']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211">
                </div>
                <div class="AvisoDescripcion">
                  <h4><?php print $content['avisos'][$i]['titulo']; ?></h4>
                  <p>
                  </p>
                  <span class="precio"><?php print $content['avisos'][$i]['precio']; ?></span>
                </div>
              </a>
            </div>
          </li>
<?php endfor; ?>
        </ul>
      </div> 
    </div>
  </div>
</div>