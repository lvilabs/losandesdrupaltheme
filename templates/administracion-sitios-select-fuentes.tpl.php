<?php
  //print_r($form);
?>
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/configuracion.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/font-awesome/css/font-awesome.min.css">
<!-- Page Content -->
<div id="main" class="container-fluid page-colores">                        
  <!-- Boxed Container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="col-lg-12">
          <div class="title-section colores-one">
            <h1>Elegí una paleta de colores</h1>
            <h4>Elegí para tu web la combinación de colores más</br>apropiada para tu negocio.</h4>
          </div>
        </div>
        <?php 
        $contador = 0;
        foreach($form['paletas'] as $kpaleta => $paleta) { 
          if(strpos($kpaleta, '#') === FALSE) { 
        ?>
          <?php if($contador == 0) { ?>
          <div class="col-md-11 col-md-offset-1">
          <?php } ?>
            <div class="col-md-4 plantilla">
              <a href="#" class="select-paleta" id="paletas-<?php print $paleta['#return_value']; ?>">
                <img class="img-responsive img-hover" src="/sites/all/modules/custom_new/administracion_sitios/img/paletas/paleta_colores_<?php print $paleta['#return_value']; ?>.jpg" alt="">
              </a>
              <div class="col-md-12 selector" style="min-height: 44px;">
                <h2 class="selected-fuente" id="selected-paletas-<?php print $paleta['#return_value']; ?>" style="<?php if($paleta['#return_value'] != $paleta['#default_value']) print 'display:none;'; ?>">
                  <i class="fa fa-check-circle-o"></i>
                </h2>
              </div> 
            </div>
          <?php $contador++; if($contador == 3) { $contador = 0;?>
          </div>
          <?php } ?>
          <?php } ?>
        <?php } ?>
        <?php if($contador != 0 && $contador < 3) { ?>
          </div>
        <?php } ?>                                     
      </div>
      <div class="col-lg-6 content-tipografias">
        <div class="col-lg-12">
          <div class="title-section colores-two">
            <h1>
                Elegí las tipografías
            </h1>
            <h4> Elegí para tu web la combinación de tipografías más</br>apropiada para tu negocio.
            </h4>
          </div>
        </div>         
        <div class="col-md-9 col-md-offset-2">
          <h1>Títulos</h1>
          <!-- Titulos opciones -->
          <div class="btn-group">
            <?php 
            $default_fuente_titulo = $form['fuentes_titulos']['#default_value'];
            $options_titulo = array();
            foreach($form['fuentes_titulos']['#options'] as $kfuente_titulo => $fuente_titulo) { 
              if($default_fuente_titulo == ''){
                $default_fuente_titulo = $fuente_titulo;
              } elseif($default_fuente_titulo == $kfuente_titulo) {
                $default_fuente_titulo = $fuente_titulo;
              }
              $options_titulo[] = array('valor' => $kfuente_titulo, 'titulo' => $fuente_titulo);
            }
            ?>
            <button id="select-fuente-titulo" class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><?php print $default_fuente_titulo; ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <?php foreach($options_titulo as $option_titulo) { ?>
                <li><a href="javascript:void(0);" class="fuente_titulo select-<?php print $option_titulo['valor']; ?>" id="<?php print $option_titulo['valor']; ?>"><?php print $option_titulo['titulo']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <h1>Párrafos</h1>
          <!-- Titulos opciones -->
          <div class="btn-group">
            <?php 
            $default_fuente_parrafos = $form['fuentes_parrafos']['#default_value'];
            $options_parrafos = array();
            foreach($form['fuentes_parrafos']['#options'] as $kfuente_parrafos => $fuente_parrafos) { 
              if($default_fuente_parrafos == ''){
                $default_fuente_parrafos = $fuente_parrafos;
              } elseif($default_fuente_parrafos == $kfuente_parrafos) {
                $default_fuente_parrafos = $fuente_parrafos;
              }
              $options_parrafos[] = array('valor' => $kfuente_parrafos, 'titulo' => $fuente_parrafos);
            }
            ?>
            <button id="select-fuente-parrafo" class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><?php print $default_fuente_parrafos; ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <?php foreach($options_parrafos as $option_parrafo) { ?>
                <li><a href="javascript:void(0);" class="fuente_parrafo select-<?php print $option_parrafo['valor']; ?>" id="<?php print $option_parrafo['valor']; ?>"><?php print $option_parrafo['titulo']; ?></a></li>
              <?php } ?>
            </ul>
          </div>                        
        </div>
      </div>                
    </div>
    <div class="col-md-4 col-md-offset-4 button-footer">
      <?php print drupal_render($form['fuentes-submit']); ?>
    </div>             
  </div>
  <!-- /.row -->
  <div style="display:none;"><?php print drupal_render($form); ?></div>
</div>
<!-- /.container -->
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/fuentes.js"></script>