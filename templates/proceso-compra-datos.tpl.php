<?php
unset($form['extra1']);
unset($form['extra2']);
//$form['extra1']['#title'] = 'Persona Fallecida';
//$form['comentarios']['#title'] = 'Texto de la Cita o Comentarios';
?>
<div class="currentBuy">
  <h3>Estás comprando:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'ficha_aviso_120_90_sc', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
    
    <h2><?php print $form['nodo']['#title']; ?></h2>
    <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
    <div class="calculadora clear">
      <input type="hidden" id="precio_venta" value="<?php print $form['nodo']['#precio']; ?>">
      <input type="hidden" id="vendedor_uid" value="<?php print $form['vendedor_uid']['#value']; ?>">
      <div class="pagando"></div>
      <span class="cargando"></span>
      <div class="cuotas final" id="monto-cuotas-div" >
        <div class="contado DN">
          <div class="label-contado">Precio Contado</div>
          <div class="label-pagos">Efectivo, tarjeta de débito o tarjeta de crédito en 1 pago</div>
          <div class="precio-contado"></div>
        </div>
        <div class="tarjeta DN">
          <div class="label-cuotas">
            <div class="cantidad-cuota"></div> 
            <div class="costo-cuota"></div>
          </div>
          <div class="label-ptf"></div>
          <div class="label-cft"></div>
          <div class="label-costo">Costo financiero total</div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if($form['nodo']['#disponible_venta']==true): ?>
<div class="clearfix avance">
  <div class="item activo"><span class="numero">1</span> Datos de contacto</div>
  <div class="item"><span class="numero">2</span> Datos de entrega</div>
  <div class="item"><span class="numero">3</span> Confirmación</div>
</div>

<div class="consigna">Complete los datos para continuar con el proceso de compra</div>
<div class="clearfix">
<div class="form-item"><?php print drupal_render($form['nombre']); ?></div>
<div class="form-item"><?php print drupal_render($form['apellido']); ?></div>
<div class="form-item"><?php print drupal_render($form['telefono']); ?></div>
<div class="form-item"><?php print drupal_render($form['email']); ?></div>
<?php if(isset($form['documento'])) : ?>
  <div class="form-item"><?php print drupal_render($form['documento']); ?></div>
<?php endif; ?>
<div class="form-item"><?php print drupal_render($form['extra1']); ?></div>
<div class="form-item"><?php print drupal_render($form['comentarios']); ?></div>
<div class="form-item"><?php print drupal_render($form['captcha_google']); ?></div>
<div class="form-item"><?php print drupal_render($form['submit']); ?></div>
</div>
<?php
endif;
print drupal_render_children($form);
?>