<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  
  <div class="Content AvisosDestacados clearfix">
    <div class="Sombra">
      <div class="clearfix">
        <h2><strong>Anuncios Destacados</strong></h2>
      </div>
    </div>
    <div class="clearfix contentAvisos">
  <?php $count=0; ?>
  <?php foreach($content as $aviso): ?>
  <?php   $count++; ?>
      <div class="CajaAviso <?php print ($count%2==0?'Par':''); ?>">
        <div class="imgAviso">
          <a href="<?php print $aviso['url']; ?>" title="<?php print $aviso['title']; ?>">
            <img src="<?php print $aviso['foto']; ?>" alt="Imagen del aviso" title="<?php print $aviso['title']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211">
          </a>
        </div>
        <div class="AvisoDescripcion">
          <h4><a href="<?php print $aviso['url']; ?>" title="<?php print $aviso['title']; ?>"><?php print $aviso['title']; ?></a></h4>
          <?php
            //Kilometraje
            $kilometraje = "";
            if(isset($aviso['usado']) && $aviso['usado'] == "0 Km"){
              $kilometraje = $aviso['usado'];
            } elseif(isset($aviso['kilometros']) && $aviso['kilometros'] != "") {
              $kilometraje = $aviso['kilometros']." Km";
            }
            if(!isset($aviso['anio']))
              $aviso['anio'] = '';
            if(!isset($aviso['combustible']))
              $aviso['combustible'] = '';
          ?>
          <p><?php print $aviso['anio']." ".$kilometraje." ".$aviso['combustible']; ?></p>
          <span class="precio"><?php print $aviso['precio']; ?></span>
        </div>
      </div>
  <?php endforeach; ?>
    </div>
  </div>
  
</div>