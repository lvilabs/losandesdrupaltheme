
<p><strong>Estimado !username:</strong></p>
<p>Queremos informarte que <strong>!interesado</strong> !titulo<br />
<a href="!aviso_url">!aviso_title</a>.</p>
<p>!consulta</p>
<p>Los datos de contacto de <strong>!interesado</strong> son los siguientes:</p>
<p>Teléfono: !contacto_telefono</p>
<p>E-mail: !contacto_email</p>
<p>Link al Anuncio: <a href="!aviso_url">!aviso_url</a></p>

<table height="50px" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: #e00018" width="100%">
  <tbody>
  <tr>
    <td width="100%" style="font-size:10.5pt;font-weight:normal;font-family:Arial,sans-serif;padding:10px;color:#FFFFFF">
      <center><strong>
       <a style="color:#FFF;text-decoration: none;" href="mailto:!contacto_email?Subject=!asunto" target="_blank">Para responder a este mail, haga clic <span style="color:#FFF;text-decoration: underline;">aquí</span></a>
      </strong>
      </center>
    </td>
  </tr>
  </tbody>
</table>