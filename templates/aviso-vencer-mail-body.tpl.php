<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="620" cellspacing="0" cellpadding="0" border="1" style="width:465.0pt;background:white;border-collapse:collapse;border:none">
          <tbody>
            <tr>
              <td style="border:none;background:transparent;padding:0cm 0cm 0cm 0cm" colspan="2">
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Hola !usuario
                </span></b></p>
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Tenés avisos que vencen el !fecha_fin.</span></b></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Información de los avisos</span></p>
                <br>
                !tabla_avisos
                <br>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Si publicaste sin registrarte, ingresá <a target="_blank" href="!perfil_url"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">acá</span></a> y termina de completar tus datos para acceder a tu administrador.</span></p>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Si ya eres usuario del sitio, logueate <a target="_blank" href="!link_login"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">acá</span></a> para republicarlo. y termina de completar tus datos para acceder a tu administrador.</span></p>
                <p>
                <br>
                <span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><strong>Destacalo para aparecer en los primeros lugares de las búsquedas y ampliá tus posibilidades de ventas. Cuanto más destacados, más chances tenés.</strong></span><br>
                <br>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
        </span></p>
      </td>
    </tr>
  </tbody>
</table>