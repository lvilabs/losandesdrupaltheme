<?php
if(!is_array($espacios)) return;
$x=0;
if(empty($espacios)):
?>
<div class="odd">No hay espacios disponibles</div>
<?php
endif;
foreach($espacios as $item) :
?>
  <div class="<?php print ($x%2==0)?'odd':'even'; ?>">
  <?php
    print 'Disponible '.$item['cantidad'].' de '.$item['total'].' aplicable a avisos de: ';
    $rubro_ids = split(',', $item['rubros']);
    for($i=0; $i<count($rubro_ids); $i++) {

      print ($i!=0)? '; ':' ';
      if(array_key_exists($rubro_ids[$i], $rubros)) {
        print $rubros[$rubro_ids[$i]]->title;
      }
    }
    if($item['reusable']==1)
      print ' (reutilizable)';
  ?>
  </div>
  <?php
    $x++;
endforeach;

?>