<?php
global $user;

$arg = arg(0); $prehome = 0; $prehome_autos = 0; $prehome_inmuebles = 0; $prehome_productos = 0; $interna_tiendas = 0; $buscador_inmuebles = 0;
$texto_buscado = '';
$rubro_tid_seleccionado = 0;

if($title == 'Home Autos') {
  $prehome = 1;
  $prehome_autos = 1;
  $rubro_tid_seleccionado = 6323;
}
if($title == 'Home Inmuebles') {
  $prehome = 1;
  $prehome_inmuebles = 1;
  $rubro_tid_seleccionado = 6330;
}
if(arg(0) == 'productos' && count(arg()) == 1) {
  $prehome = 1;
  $prehome_productos = 1;
}

// Detectamos internas de tiendas
if(arg(0) == 'node' && is_numeric(arg(1))) {
  $nodo = node_load(arg(1));
  if($nodo->type == 'aviso_producto') {
    $interna_tiendas = 1;
  }
}
if(arg(0) == 'comprar') {
  $interna_tiendas = 1;
}
$env_id = apachesolr_default_environment();
if(apachesolr_has_searched($env_id)) {
  $query = apachesolr_current_query($env_id);
  if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, 6106)){
    $interna_tiendas = 1;
  }
  if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, 6330)){
    $buscador_inmuebles = 1;
  }
}
if($prehome_productos || $interna_tiendas) {
  $rubros = clasificados_rubros_obtener_rubros_hijos(6106);
  $orden_rubros = array(6186 => array('class' => 'hogar-muebles', 'title' => 'Hogar y Muebles')
                        ,6154 => array('class' => 'electrodomesticos', 'title' => 'Electrodomésticos')
                        ,6275 => array('class' => 'moda', 'title' => 'Moda')
                        //,7369 => array('class' => 'construccion', 'title' => 'Construcción')
                        ,6218 => array('class' => 'informatica', 'title' => 'Informática')
                        //,6262 => array('class' => 'bebes', 'title' => 'Bebés')
                        ,7788 => array('class' => 'libros', 'title' => 'Libros')
                        //,6255 => array('class' => 'celulares', 'title' => 'Celulares')
                        ,7099 => array('class' => 'accesorios-autos', 'title' => 'Accesorios Autos'));
  //$exclude_rubros = array(6186 => 'hogar-muebles',6154 => 'electrodomesticos');
  if(apachesolr_has_searched($env_id)) {
    $termino_busqueda = $query->getParams();
    $texto_buscado = $termino_busqueda['q'];
  }
  // Obtenemos los rubros de avisos vendibles publicados
  $rubros_usados = clasificados_rubros_avisos_vendibles();
} else {
  $rubros = clasificados_rubros_obtener_grupos();
  $env_id = apachesolr_default_environment();
  if(apachesolr_has_searched($env_id)) {
    $termino_busqueda = $query->getParams();
    $texto_buscado = $termino_busqueda['q'];
    foreach($rubros as $tid => $name) {
      if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, $tid)) {
        $rubro_tid_seleccionado = $tid;
        break;
      }
    }
  }
}

$array_rubros = clasificados_rubros_obtener_rubros();
foreach($array_rubros as $rubro) {
  if($rubro->title == 'Productos')
    $rubro_producto = $rubro->tid;
  if($rubro->title == 'Servicios')
    $rubro_servicio = $rubro->tid;
  if($rubro->title == 'Rurales')
    $rubro_rural = $rubro->tid;
}
$count = 0;
?>

<div class="Hd Ancho">
	<div class="ContentMenu">
		<div class="Borde">
			<div class="Middle clearfix">
				<div class="LogoClasificados">
					<a href="https://clasificados.losandes.com.ar/" title="Clasificados Los Andes" rel="index" id="logo"><img title="Clasificados Los Andes" src="<?php print file_create_url('sites/clasificados.losandes.com.ar/themes/losandes/img/logo_clasificados_los_andes.png'); ?>" alt="Logo de Clasificados Los Andes"/>
          </a>
				</div>
			</div>
			<div class="Top">
				<div class="Ancho">
					<div class="Caja_Solapas Caja_Header">
						<div class="Solapas">
							<ul class="Inner clearfix">
								
                <div class="Buscador">
                  <!-- <form id="buscador_header" action="/search/apachesolr_search" method="get">
                    <input name="bsc_palabra" type="text" value="<?php print $texto_buscado; ?>" placeholder="Encontrá lo que necesites" class="termino" />
                    <div style="display: none;">
                      <select name="bsc_sitio" class="categoria">
                        <option value="">Todos los Rubros</option>
                        <?php foreach($rubros as $tid =>
                         $name): ?>
                        <option value="<?php print $tid; ?>"<?php if($tid == $rubro_tid_seleccionado) { print ' selected="selected"'; } ?>
                        ><?php print $name; ?>
                        </option>
                        <?php   $count++; ?>
                        <?php   if($count == 2): ?>
                        <option value="empleos">Empleos</option>
                        <?php   endif; ?>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <input name="submit" type="submit" id="submit" value="" class="submit"/>
                  </form> -->
                </div>
                
								<!-- START LOGIN SUPERIOR REGION -->
								<?php print $login_superior; ?>
								<!-- END LOGIN SUPERIOR REGION -->
								<div class="ContentLogin ">
									<li class="ingresar DN"><a rel="noindex" href="javascript:void(0);" class="ingresar-btn" title="Ingresar">INGRESAR</a></li>
                                    <li class="Registrarse DN"><a href="<?php print url('registro-particular', array('alias'=>TRUE)); ?>" title="Registrarse">REGISTRARSE</a></li>
								</div>
							</ul>
						</div>
						<!--Fin solapas-->
					</div>
				</div>
			</div>
			<!-- FIN-->
		</div>
	</div>
  
  <div class="menu-container">
    <div class="col-full">
      <ul class="fl-left">
        <li class="<?php if($title == 'Home Inmuebles') print 'menu-active'; ?>">
          <a href="https://clasificados.losandes.com.ar/inmuebles">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/inmuebles.png" class="menu-icon"> 
            <span class="mr1 menu-text">Inmuebles</span>
          </a>  
        </li>
        <li class="<?php if($title == 'Home Autos') print 'menu-active'; ?>">
          <a href="https://clasificados.losandes.com.ar/vehiculos">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/automotores.png" class="menu-icon"> 
            <span class="mr1 menu-text">Automotores</span>
          </a>  
        </li>
        <li class="<?php if(arg(0) == 'productos') print 'menu-active'; ?>">
          <a href="https://clasificados.losandes.com.ar/productos">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/articulos.png" class="menu-icon"> 
            <span class="mr1 menu-text">Artículos</span>
          </a>  
        </li>
        <li>
          <a href="https://clasificados.losandes.com.ar/servicios/empleos">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/empleo.png" class="menu-icon"> 
            <span class="mr1 menu-text">Empleos</span>
          </a>  
        </li>
        <li>
          <a href="https://clasificados.losandes.com.ar/servicios">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/servicios.png" class="menu-icon"> 
            <span class="mr1 menu-text">Servicios</span>
          </a>  
        </li>

        <li>
          <a href="https://clasificadospapel.losandes.com.ar/microsite/viewVarious/words//m/educacionales" target="_blank">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/educacionales.png" class="menu-icon"> 
            <span class="mr1 menu-text">Educacionales</span>
          </a>  
        </li>

        <li>
          <a href="https://clasificadospapel.losandes.com.ar/microsite/viewVarious/words//m/edictos-judiciales" target="_blank">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/legales.png" class="menu-icon"> 
            <span class="mr1 menu-text">Edictos</span>
          </a>  
        </li>

        <li>
          <a href="https://clasificadospapel.losandes.com.ar" target="_blank">  
            <img src="/sites/clasificados.losandes.com.ar/themes/losandes/img/iconos_top/avisospapel.png" class="menu-icon"> 
            <span class="mr1 menu-text">Avisos Papel</span>
          </a>  
        </li>

     </ul>
     <a rel="noindex" href="javascript:void(0);" title="Publicación de avisos gratuitos" class="btn btn-destacado fl-right publicar-btn">PUBLICÁ GRATIS</a>
    </div>
  </div>
  <!-- 
  <div class="menu-container" style="height:35px;">
      <div style="top:30%" class="col-full">
          <ul class="fl-left">
              <li><a target="_blank" style="text-transform:none;font-size: 14px;" href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/educacionales"  title="Educacionales Los Andes"><i class="fas fa-pencil-alt"></i>Educacionales</a></li>
              <li><a target="_blank" style="text-transform:none;font-size: 14px;" href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/edictos-judiciales" title="Edictos Judiciales Los Andes"><i class="fas fa-gavel"></i>Edictos Judiciales</a></li>
              <li><a target="_blank" style="text-transform:none;font-size: 14px;" href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/empleos" title="Empleos Los Andes"><i class="fas fa-user-tie"></i>Empleos</a></li>
              <li><a target="_blank" style="text-transform:none;font-size: 14px;" href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/farmacias" title="Farmacias Los Andes"><i class="fas fa-medkit"></i>Farmacias</a></li>
              <li><a target="_blank" style="text-transform:none;font-size: 14px;" href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/guia-profesional" title="Guia Profesional Los Andes"><i class="fas fa-address-book"></i>Guía Profesional</a></li>
          </ul>
      </div>
  </div>
  -->

  <?php if($is_front): ?>
    <h1>Clasificados Los Andes – Avisos clasificados para comprar y vender en Mendoza</h1>
  <?php elseif($prehome_autos): ?>
    <h1>Clasificados Los Andes – Venta de autos, motos, 4x4, repuesto y accesorios en Mendoza</h1>
  <?php elseif($prehome_inmuebles): ?>
    <h1>Clasificados Los Andes – Alquiler y venta de inmuebles, departamentos y casas en Mendoza</h1>
  <?php endif; ?>
  
  <?php if($is_front): ?>
    <!-- Promociones home  -->
    <?php 
      $module = 'clasificados';
      $delta = 'carrusel_promociones';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
  <?php endif; ?>
  
  <?php if($prehome_autos): ?>
    <!-- Promociones home autos  -->
    <?php 
      $module = 'clasificados';
      $delta = 'carrusel_promociones_auto';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
  <?php endif; ?>
  
  <?php if($prehome_inmuebles): ?>
    <!-- Promociones home inmuebles -->
    <?php 
      $module = 'clasificados';
      $delta = 'carrusel_promociones_inmuebles';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
  <?php endif; ?>
  
  <?php if($prehome_productos): ?>
    <h1>Clasificados Los Andes – Tienda online, compra de productos en Mendoza</h1>
    <!-- Promociones home productos -->
    <?php 
      $module = 'clasificados';
      $delta = 'carrusel_promociones_productos';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
  <?php endif; ?>
  
  <?php if($is_front || $prehome_autos || $prehome_inmuebles || $prehome_productos): ?>
    <!-- <div class="clf-network">
      <div class="col-full">
        <ul>
          <li><a href="/inmuebles" title="Home clasificados de inmuebles"><i class="fas fa-home"></i><span>Inmuebles</span></a></li>
          <li><a href="/autos" title="Home clasificados de vehículos"><i class="fas fa-car-alt"></i><span>Vehículos</span></a></li>
          <li><a href="/productos" title="Home clasificados de productos"><i class="fas fa-gift"></i><span>Productos</span></a></li>
          <li><a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/empleos" title="Sitio de empleos de Los Andes" target="_blank"><i class="fas fa-briefcase"></i></i><span>Empleos</span></a></li>
          <li><a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/educacionales" title="Sitio educacionales de Los Andes" target="_blank"><i class="fas fa-book"></i></i><span>Educacionales</span></a></li>
          <li><a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/edictos-judiciales" title="Sitio de legales de Los Andes" target="_blank"><i class="fas fa-balance-scale"></i><span>Legales</span></a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017" title="Resultado de búsqueda de servicios" rel="search"><i class="far fa-handshake"></i><span>Servicios</span></a></li>
        </ul>
      </div>
    </div> -->
  <?php endif; ?>
  
  <!-- Modal Selector Rubros Publicacion -->
  <!-- 
  <div class="content-modal-publicacion-rubros">
    <div id="modal-publicacion-rubros">
      <div class="title">¿Qué deseás publicar?</div>
      <a href="/node/add/aviso-auto" title="Publicá tu vehículo" class="first">
        <i class="fa fa-car"></i>
        <div class="publicacion-rubro">Vehículos</div>
      </a>
      <a href="/node/add/aviso-casa" title="Publicá tu casa o departamento">
        <i class="fa fa-home"></i>
        <div class="publicacion-rubro home">Inmuebles</div>
      </a>
      <a href="/node/add/aviso-servicio?res=empleos" title="Publicá empleos solicitados u ofrecidos">
        <i class="fas fa-briefcase"></i>
        <div class="publicacion-rubro ">Empleos</div>
      </a>
      <a href="/node/add/aviso-producto" title="Publicá tu producto" >
        <i class="fa fa-gift"></i>
        <div class="publicacion-rubro home">Productos</div>
      </a>
      <a href="/node/add/aviso-servicio" title="Publicá tu servicio" class="fourth">
        <i class="fa fa-wrench"></i>
        <div class="publicacion-rubro">Servicios</div>
      </a>
      <a href="/node/add/aviso-rural" title="Publicá tu rural">
          <i class="fa fa-tractor"></i>
        <div class="publicacion-rubro">Rurales</div>
      </a>
    </div>
  </div>
  -->

  <div class="content-modal-publicacion-rubros">
    <div id="modal-publicacion-rubros">
      <div class="title"><h5>Seleccioná el rubro en el que querés publicar</h5></div>
      <a href="https://clasificados.losandes.com.ar/publicar/vehiculos" title="Publicá tu automotor" class="first">
        <i class="fas fa-car"></i>
        <div class="publicacion-rubro">Automotores</div>
      </a>
      <a href="https://clasificados.losandes.com.ar/publicar/inmuebles" title="Publicá tu casa o departamento">
        <i class="fas fa-city"></i>
        <div class="publicacion-rubro ">Inmuebles</div>
      </a>
      <a href="/node/add/aviso-servicio?res=empleos" title="Publicá empleos solicitados u ofrecidos">
        <i class="fas fa-briefcase"></i>
        <div class="publicacion-rubro ">Empleos</div>
      </a>
      <a href="https://clasificados.losandes.com.ar/publicar/productos" title="Publicá tu artículo" class="first">
        <i class="fas fa-gift"></i>
        <div class="publicacion-rubro home">Artículos</div>
      </a>
      <a href="https://clasificados.losandes.com.ar/publicar/servicios" title="Publicá tu servicio">
        <i class="fas fa-wrench"></i>
        <div class="publicacion-rubro">Servicios</div>
      </a>
      <!-- <a href="/node/add/aviso-rural" title="Publicá tu rural">
        <i class="fas fa-tractor"></i>
        <div class="publicacion-rubro">Rurales</div>
      </a> -->
    </div>
  </div>


</div>
