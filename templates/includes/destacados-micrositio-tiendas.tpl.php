<div class="clearfix contentAvisosTiendas" style="display: block;">
<?php
  $primero = 1;
  foreach($destacados as $aviso){
    if($primero > 2) break;
    if(arg(1) != $aviso['nid']) {
?>
  <div class="col-md-6 left">
    <div class="img-portfolio">
      <div class="imgAviso">
        <a href="/<?php print $aviso['path']; ?>">
          <span></span>
          <img class="img-responsive img-hover" src="/<?php print imagecache_create_path('ficha_aviso_173_115_sc', $aviso['filepath']); ?>" alt="">
        </a>
      </div>
      <div class="info-section">
        <h3>
          <a href="/<?php print $aviso['path']; ?>"><?php print $aviso['title']; ?></a>
        </h3>
        <h4 class="price"><?php print $aviso['precio']; ?><span class="precio-anterior"><?php print $aviso['precio_anterior']; ?></span></h4>
      </div>
    </div>  
  </div>
<?php $primero++; 
    } 
  }
?>
</div>