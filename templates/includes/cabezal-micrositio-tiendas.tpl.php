<div id="content-cabezal-tienda">
  <header class="container">
    <!-- Navigation -->
    <div class="top-img"></div>
    <nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" id="files-logo" href="<?php print $dominio_clvi; ?>" title="<?php print $usuario->profile_nombre_comercial; ?>"><img alt="<?php print $usuario->profile_nombre_comercial; ?>" src="<?php print '/'.$usuario->picture; ?>" title="Logo de <?php print $usuario->profile_nombre_comercial; ?>" width="150"></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li <?php if(empty($seccion)) print 'class="active"'; ?>>
            <a href="<?php print $dominio_clvi; ?>" rel="search" title="Resultado de búsqueda de <?php print $usuario->profile_nombre_comercial; ?>">Nuestros avisos</a>
          </li>
          <?php if($vendedor->texto_info_comercio!='') : ?>
          <!-- <li <?php if($seccion == 'quienes-somos') print 'class="active"'; ?>>
            <a href="javascript:void(0);" class="link-info-tienda" rel="noindex">Nuestra tienda</a>
          </li> -->
          <?php endif; ?>
          <li <?php if($seccion == 'contacto') print 'class="active"'; ?>>
            <a href="<?php print $dominio_clvi; ?>/contacto" title="Contacot de <?php print $usuario->profile_nombre_comercial; ?>">Contacto</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
  </header>
</div>

<div class="DN">
  <?php if($vendedor->texto_info_comercio!='') : ?>
    <div id="modal-info-tienda">
    <h2><?php print $usuario->profile_nombre_comercial; ?></h2>
      <p><?php print nl2br($vendedor->texto_info_comercio); ?></p>
    </div>
  <?php endif; ?>
</div>