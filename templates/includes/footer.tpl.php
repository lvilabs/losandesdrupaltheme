<div class="pie">
	<div class="caja">
		<div class="logoClasificados clearfix">
        	<a title="Clasificados Los Andes" href="https://clasificados.losandes.com.ar/" rel="index"><span class="DN">Clasificados</span></a>
		</div>
		<div class="clearfix">
			<div class="dele">
                <dl class="clearfix">
                    <dd><a href="https://clasificados.losandes.com.ar/" title="Inicio" rel="index">Inicio</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/vehiculos" title="Automotores">Automotores</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/inmuebles" title="Inmuebles">Inmuebles</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/productos" title="Empleos">Artículos</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/servicios/empleos" title="Servicios" rel="search">Empleos</a> <span>|</span></dd>
                    <!-- <dd><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6000" title="Rurales" rel="search">Rurales</a> <span>|</span></dd> -->
                    <dd><a href="https://clasificados.losandes.com.ar/servicios" title="Servicios" rel="search">Servicios</a></dd>
                </dl>
                <dl class="clearfix">
                    <dd><a rel="noindex" href="javascript:void(0);" title="Publicá cualquier producto o servicio" class="publicar-btn">Publicar aviso</a> <span>|</span></dd>
                    <dd><a target="_blank"  href="https://www.losandes.com.ar/receptorias/" title="Receptorías" rel="follow">Receptorías</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/faq" title="Ayuda - Preguntas Frecuentes">Ayuda</a> <span>|</span></dd>
                    <dd><a href="https://clasificados.losandes.com.ar/contacto" title="Contáctenos">Contáctenos</a> <span>|</span></dd>
                    <dd><i class="fas fa-phone"></i> 0261 4-491249</dd>
                </dl>
			</div>
		</div>
        <div class="logoLavoz clearfix">
        	<a title="Los Andes" href="https://clasificados.losandes.com.ar/" rel="follow"><span class="DN">Los Andes</span></a>
		</div>
        
        <div class="clearfix">
			<div class="dele">
                <dl class="clearfix">
                    <dd><a target="_blank" href="https://www.facebook.com/LosAndesDiario" title="Los Andes FB" rel="follow"><span style="font-size: 1.8em;color:white;"><i class="fab fa-facebook-square"></i></span></a></dd>
                    <dd><a target="_blank" href="https://twitter.com/LosAndesDiario" title="Los Andes Twitter" rel="follow"><span style="font-size: 1.8em;color:white;"><i class="fab fa-twitter-square"></i></span></a></dd>
                    <dd><a target="_blank" href="https://www.instagram.com/LosAndesDiario" title="Los Andes IG" rel="follow"><span style="font-size: 1.8em;color:white;"><i class="fab fa-instagram"></i></span></a></dd>
                    <dd><a target="_blank" href="https://www.youtube.com/user/LosAndesDiario" title="Los Andes YT" rel="follow"><span style="font-size: 1.8em;color:white;"><i class="fab fa-youtube-square"></i></span></a></dd>
                    <dd><a target="_blank" href="https://new.losandes.com.ar/funebres_web/" title="Los Andes Funebres" rel="follow"><span style="font-size: 1.8em;color:white;"><i class="fas fa-dove"></i></span></a></dd>
                </dl>
			</div>
		</div>
	</div>
	<div class="derechos">
    <p>&copy; 1995 - <?php print date('Y'); ?> Todos los derechos reservados.</p>
      <p class="LogOtros">
      <a target="_blank" href="https://www.cmi.com.ar/" title="CMI Medios Regionales" rel="noindex"><img class="img-otros" height="22" border="0" alt="Cmi" src="<?php print file_create_url('sites/clasificados.losandes.com.ar/themes/losandes/img/footer_cmi.png'); ?>" title="CMI Medios Regionales" /></a>
      <a target="_blank" href="https://adepa.org.ar/" title="Comscore" rel="noindex"><img height="22" border="0" class="img-otros" alt="Comscore" src="<?php print file_create_url('sites/clasificados.losandes.com.ar/themes/losandes/img/footer_adepa.png'); ?>" title="Comscore"/></a>
      <a target="_blank" href="https://www.jus.gov.ar/datos-personales.aspx" title="Direccion Nacional de Proteccion de Datos Personales" rel="noindex"><img height="22" border="0" class="img-otros" alt="DNPDP" src="<?php print file_create_url('sites/clasificados.losandes.com.ar/themes/losandes/img/footer_datos.png'); ?>" title="Direccion Nacional de Proteccion de Datos Personales" /></a>
      <a target="_blank" href="http://www.iabargentina.com.ar/" title="Interactive Advertising Bureau" rel="noindex"><img height="22" border="0" alt="IAB" src="<?php print file_create_url('sites/clasificados.losandes.com.ar/themes/losandes/img/footer_iab.png'); ?>" title="Interactive Advertising Bureau" /></a>
		</p>
        <p>
        <a style="font-size:14px;color:#8a8a8a;" target="_blank" href="https://losandes.com.ar/page/terms-and-conditions" title="Terminos y Condiciones" rel="noindex">Términos y Políticas de Privacidad</a>
        </p>
    </div>
</div>

<div class="content-script-footer">
  <?php if(isset($frase_busqueda)): ?>
  <script type="text/javascript">
    (function() {
      var cx = '003201315050113052054:mcog5p7jbni';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
          '//www.google.com/cse/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    })();
  </script>
  <?php endif; ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-2721986-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-2721986-2');
</script>
<!-- Comscore script begin -->  
<script type="text/javascript">
var _sf_async_config={uid:15747,domain:"clasificadoslavoz.com.ar"};
(function(){
  function loadChartbeat() {
    window._sf_endpt=(new Date()).getTime();
    var e = document.createElement('script');
    e.setAttribute('language', 'javascript');
    e.setAttribute('type', 'text/javascript');
    e.setAttribute('src',
       (("https:" == document.location.protocol) ? "https://a248.e.akamai.net/chartbeat.download.akamai.com/102508/" : "http://static.chartbeat.com/") +
       "js/chartbeat.js");
    document.body.appendChild(e);
  }
  var oldonload = window.onload;
  window.onload = (typeof window.onload != 'function') ?
     loadChartbeat : function() { oldonload(); loadChartbeat(); };
})();
</script>
<!-- Cxense script begin -->
<div id="cX-root"></div>
<script type="text/javascript">
var cX = cX || {}; cX.callQueue = cX.callQueue || [];
cX.callQueue.push(['setSiteId', '9222331896993589875']);
var user_id = get_cookie('login');
if (user_id!=false && user_id!=undefined) {
  cX.callQueue.push(['setCustomParameters', { 'registered': 'true' }]);
  cX.callQueue.push(['addExternalId', { 'id': 'cl-'+user_id, 'type': 'gcl'}]);
  var gcz_user_id = get_cookie('gcz_user_id');
  if (gcz_user_id!=false && gcz_user_id!=undefined) {
    cX.callQueue.push(['addExternalId', { 'id': gcz_user_id, 'type': 'gcz'}]);
  }
}
cX.callQueue.push(['sendPageViewEvent', { useAutoRefreshCheck: false }]);
</script>

<?php if(isset($cxense_events)) print $cxense_events; ?>

<script type="text/javascript">
(function() { try { var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript'; scriptEl.async = 'async';
scriptEl.src = ('https:' == document.location.protocol) ? 'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
var targetEl = document.getElementsByTagName('script')[0]; targetEl.parentNode.insertBefore(scriptEl, targetEl); } catch (e) {};} ());
</script>
<!-- Cxense script end -->
<!-- Facebook Pixel Code -->
<?php
//include dirname(__FILE__).'/script-facebook.tpl.php';
?>
<!-- FB -->
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>
</div>
