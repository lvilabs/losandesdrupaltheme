<?php 
global $user;

$permisos = usuarios_colaboradores_get_permisos_user($user->uid);
if(!isset($_GET['destination']) || strpos($_GET['destination'], '?share=1') === false){ ?>
   
    <div class="menuSolapas clearfix">
      <ul class="accesos">
<?php if(in_array('Ver resumen', $permisos)) { ?>
        <li class="<?php print ($template_files[0]=='page-administrar' && $template_files[1]=='page-administrar-principal')?'active':''; ?>">
          <a href="/administrar/principal" title="Resumen">Resumen</a>
        </li>
<?php } ?>
        <li class="<?php print (($template_files[0]=='page-administrar' && $template_files[1]!='page-administrar-agenda') || (isset($node) && sitio_local_es_nodo_aviso($node->type) && isset($template_files[2]) && in_array($template_files[2], array('page-node-edit', 'page-node-clone', 'page-node-eliminar', 'page-node-despublicar'))))?'active':''; ?>">
          <a href="/administrar/mis-avisos-colaborador.html" title=" Lista de avisos ">Lista de avisos</a>
        </li>
<?php if(in_array('Crear avisos', $permisos)) { ?>        
        <li class="<?php print ((isset($template_files[1]) && $template_files[1]=='page-node-add') || (isset($template_files[2]) && $template_files[2]=='page-node-publicacion'))?'active':''; ?>">
          <?php if(arg(2) == "publicacion") { ?>
            <a title=" Publicación del aviso " href="/node/<?php print arg(1); ?>/publicacion">Publicá</a>
          <?php } else { ?>
            <!-- <a title="Crear Aviso" href="?width=640&height=450&inline=true&title= #modal-publicacion-rubros" class="colorbox-inline">Publicá</a> -->
            <a title="Crear Aviso" rel="nofollow"  href="javascript:void(0);" class="publicar-btn">Publicá</a>
          <?php } ?>
        </li>
<?php } ?>        
<?php if(isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') { ?>
        <li class="<?php print (isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') ? 'active' : ''; ?>"><a title="Pagar por DineroMail" href="/dineromail/<?php print arg(1); ?>/confirmar">DineroMail</a></li>
<?php } ?>
<?php if(in_array('Ver Consultas', $permisos)) { ?>
        <li class="<?php print ($template_files[0]=='page-contactos_recibidos_colaboradores')?'active':''; ?>"><a href="/contactos_recibidos_colaboradores" title=" Contactos ">Contactos</a></li>
<?php } ?>
        <li class="more cuenta <?php print (in_array($template_files[0], array('page-estado_cuenta', 'page-compra-paquetes', 'page-compra-papel', 'page-abono-mensual')) || ($template_files[2] == 'page-administrar-ventas-comprador' && $template_files[3] == 'page-admin'))? 'active' : ''; ?>">
<?php if(in_array('Utilizar espacios', $permisos)) { ?>          
          <a href="/abono-mensual" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
<?php } else { ?>
          <a href="/administrar/ventas/comprador" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
<?php } ?>
          <ul class="more-links cuenta-links" style="display: none;">
<?php if(in_array('Utilizar espacios', $permisos)) { ?>
            <li><a href="/abono-mensual" title="Abono Mensual">Abono Mensual</a></li>
            <li><a href="/compra-paquetes" title="Compras de Paquetes">Compras de Paquetes</a></li>
            <li><a href="/estado_cuenta" title="Compras de espacios y mejoras realizadas">Compras de Espacios y Mejoras</a></li>
<?php } ?>
            <li><a href="/administrar/ventas/comprador" title="Compras de Productos">Compras de Productos</a></li>
          </ul>
        </li>

        
<?php if(in_array('Administrar agenda', $permisos) || in_array('Ver agenda', $permisos)) { ?>
          <li class="more agenda <?php print ($links_concesionaria['agenda']['activo'] == 1)?'active':''; ?>">
            <a href="/administrar/agenda/propietarios" title="Agenda">Agenda</a>
            <ul class="more-links agenda-links" style="display: none;">
              <li><a href="/administrar/agenda/propietarios" title="Propietarios">Propietarios</a></li>
              <li><a href="/administrar/agenda/vendedores" title="Vendedores">Vendedores</a></li>
              <li><a href="/administrar/agenda/interesados" title="Interesados">Interesados</a></li>
            </ul>
          </li>
<?php } ?>        
          <li class="more mis-datos <?php print ($links_concesionaria['mis-datos']['activo'] == 1)?'active':''; ?>">
            <a href="/user/<?php print $user->uid; ?>/edit" title="Mis datos">Cuenta</a>
            <ul class="more-links mis-datos-links" style="display: none;">
              <li><a href="/user/<?php print $user->uid; ?>/edit" title="Mis datos de usuario">Datos Usuario</a></li>
              <li><a href="/user/<?php print $user->uid; ?>/edit/Datos Personales" title="Mis datos personales">Datos Personales</a></li>
            </ul>
          </li>
      </ul>
    </div>
<?php } ?>