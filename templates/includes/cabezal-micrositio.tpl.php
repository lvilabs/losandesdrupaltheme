<?php 
//$array_dominio = administracion_sitios_explode_dominio($datos_sitio['dominio']); 
$seccion = arg(2);
?>
<!-- <link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/css/bootstrap.min.css"/> -->
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/css/cabezal-bootstrap.css"/>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/index.css"/>

<!-- Paletas de colores -->
<?php if($datos_sitio['paleta_color'] == 2){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/paleta-colores-2.css"/>
<?php } ?>
<?php if($datos_sitio['paleta_color'] == 3){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/paleta-colores-3.css"/>
<?php } ?>

<!-- Fuentes titulos -->
<?php if($datos_sitio['fuente_titulos'] == 'mavenPro'){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/fuente-titulo-mavenPro.css"/>
<?php } ?>
<?php if($datos_sitio['fuente_titulos'] == 'arvo'){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/fuente-titulo-arvo.css"/>
<?php } ?>
<?php if($datos_sitio['fuente_titulos'] == 'titilliumWeb'){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/templates/<?php print $datos_sitio['template']; ?>/cabezal/css/fuente-titulo-titilliumWeb.css"/>
<?php } ?>

<?php
$time = 1111;
if(file_exists($usuario->picture)) {
  $time = filemtime($usuario->picture);
}
?>
<?php if($datos_sitio['template'] == 'template1'){ ?>

<div id="content-cabezal">
  <header class="container">
    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" id="files-logo" href="<?php print $dominio_clvi; ?>" title="<?php print $usuario->profile_nombre_comercial; ?>"><img src="<?php print '/'.$usuario->picture; ?>?<?php print $time; ?>" alt="Logo de <?php print $usuario->profile_nombre_comercial; ?>" title="Logo de <?php print $usuario->profile_nombre_comercial; ?>" height="106"></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li <?php if(empty($seccion)) print 'class="active"'; ?>>
            <a href="<?php print $dominio_clvi; ?>" title="Avisos de <?php print $usuario->profile_nombre_comercial; ?>">AVISOS</a>
          </li>
          <li <?php if($seccion == 'quienes-somos') print 'class="active"'; ?>>
            <a href="<?php print $dominio_clvi; ?>/quienes-somos" title="Quienes somos <?php print $usuario->profile_nombre_comercial; ?>">NOSOTROS</a>
          </li>
          <li <?php if($seccion == 'contacto') print 'class="active"'; ?>>
            <a href="<?php print $dominio_clvi; ?>/contacto" title="Contacto <?php print $usuario->profile_nombre_comercial; ?>">CONTACTOS</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
  </header>
  <?php if(!empty($datos_sitio['imagen_micrositio'])) { ?>
    <div id="myCarousel" class="container carousel slide">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="fill" id="files-principal"><img src="/<?php print $datos_sitio['imagen_micrositio']; ?>" alt="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>" title="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>"></div>
          <div class="carousel-caption">
            <h2 id="nombre-comercial"><?php print $usuario->profile_nombre_comercial; ?></h2>
            <p>
              <span id="calle"><?php print $usuario->profile_calle; ?></span>  <span id="altura"><?php print $usuario->profile_altura; ?></span>.</br>
              <span id="ciudad"><?php print $usuario->profile_ciudad; ?></span><span id="barrio"><?php if(!empty($usuario->profile_barrio)) print ' - '.$usuario->profile_barrio; ?></span>.
            </p>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>

<?php } elseif($datos_sitio['template'] == 'template2') { ?>

<div id="content-cabezal">
  <!-- Header Content -->
  <header class="bg-black">
    <div class="container">        
      <!-- Navigation -->
      <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a class="navbar-brand" id="files-logo" href="<?php print $dominio_clvi; ?>" title="<?php print $usuario->profile_nombre_comercial; ?>"><img src="<?php print '/'.$usuario->picture; ?>?<?php print $time; ?>" alt="Logo de <?php print $usuario->profile_nombre_comercial; ?>" title="Logo de <?php print $usuario->profile_nombre_comercial; ?>" height="106"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li <?php if(empty($seccion)) print 'class="active"'; ?>>
              <a href="<?php print $dominio_clvi; ?>" title="Avisos de <?php print $usuario->profile_nombre_comercial; ?>">AVISOS</a>
            </li>
            <li <?php if($seccion == 'quienes-somos') print 'class="active"'; ?>>
              <a href="<?php print $dominio_clvi; ?>/quienes-somos" title="Quienes somos <?php print $usuario->profile_nombre_comercial; ?>">NOSOTROS</a>
            </li>
            <li <?php if($seccion == 'contacto') print 'class="active"'; ?>>
              <a href="<?php print $dominio_clvi; ?>/contacto" title="Contacto de <?php print $usuario->profile_nombre_comercial; ?>">CONTACTOS</a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </nav>
    </div>
  </header> 
  <!-- Header Carousel -->
  <?php if(!empty($datos_sitio['imagen_micrositio'])) { ?>  
    <div id="Carousel-home" class="container carousel slide">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="fill" id="files-principal"><img alt="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>" title="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>" src="/<?php print $datos_sitio['imagen_micrositio']; ?>"></div>
          <!-- <div class="carousel-caption">
            <h2>Casona La Martina</h2>
            <p>
              Tristan Malbran 4355 planta alta.</br>
              CÓRDOBA.
            </p>
          </div> -->
        </div>
      </div>     
    </div>  
  <?php } ?>  
</div>

<?php } elseif($datos_sitio['template'] == 'template3') { ?>

<div id="content-cabezal">
  <!-- Header Content -->
  <header>
    <div class="nav-top">
      <div class="container">        
        <!-- Navigation -->
        <nav class="navbar navbar-default" role="navigation">
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li <?php if(empty($seccion)) print 'class="active"'; ?>><a href="<?php print $dominio_clvi; ?>" title="Avisos de <?php print $usuario->profile_nombre_comercial; ?>">AVISOS</a></li>
              <li <?php if($seccion == 'quienes-somos') print 'class="active"'; ?>><a href="<?php print $dominio_clvi; ?>/quienes-somos" title="Quienes somos <?php print $usuario->profile_nombre_comercial; ?>">NOSOTROS</a></li>
              <li <?php if($seccion == 'contacto') print 'class="active"'; ?>><a href="<?php print $dominio_clvi; ?>/contacto" title="Contacto de <?php print $usuario->profile_nombre_comercial; ?>">CONTACTOS</a></li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
        </nav>
      </div>
      <!-- /.container -->
    </div>
    <div class="nav-white">
      <div class="container">
        <a class="navbar-brand" id="files-logo" href="<?php print $dominio_clvi; ?>" title="<?php print $usuario->profile_nombre_comercial; ?>">
        <img alt="Logo de <?php print $usuario->profile_nombre_comercial; ?>" title="Logo de <?php print $usuario->profile_nombre_comercial; ?>" src="<?php print '/'.$usuario->picture; ?>?<?php print $time; ?>" height="106">
        </a>
      </div>
      <!-- /.container -->
    </div>
  </header>
  <!-- Header Carousel -->
  <?php if(!empty($datos_sitio['imagen_micrositio'])) { ?>  
    <div id="Carousel-home" class="carousel slide">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="fill"><img class="center-block" alt="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>" title="Banner principal de <?php print $usuario->profile_nombre_comercial; ?>" src="/<?php print $datos_sitio['imagen_micrositio']; ?>"></div>
          <div class="container carousel-caption">
            <h2><?php (isset($usuario->profile_nombre_comercial) && !empty($usuario->profile_nombre_comercial))? print $usuario->profile_nombre_comercial : print 'Nombre Comercial'; ?></h2>
            <p>
              <?php (isset($usuario->profile_calle) && !empty($usuario->profile_calle))? print $usuario->profile_calle : print 'Calle'; ?>
              <?php (isset($usuario->profile_altura) && !empty($usuario->profile_altura))? print $usuario->profile_altura : print 'altura'; ?>
              <?php (isset($usuario->profile_ciudad) && !empty($usuario->profile_ciudad))? print ' - '.$usuario->profile_ciudad : print ' - Ciudad - Barrio'; ?>
              <?php if(!empty($usuario->profile_barrio) && $usuario->profile_barrio != 'Otro') print ' - '.$usuario->profile_barrio; ?></br>
            </p>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>  
</div>

<?php } ?>

<script>
  $(document).ready(function() {
    var str = $('.CR').css('margin-top');
    if(str != undefined){
      var cr_top = str.replace(/[^.0-9]/g,'');
      cr_top = parseFloat(cr_top);
      $('.CR').css('margin-top', $('.microsite-content').height() + cr_top);
    }
  });
</script>