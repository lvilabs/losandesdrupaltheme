<?php
/**
 * @file views-bulk-operations-table.tpl.php
 * Template to display a VBO as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
global $user;
$icons_path = path_to_theme().'/img/icons/admin';
$destination = trim($_SERVER["REQUEST_URI"],'/');
$edit_sitio = 0;
if(administracion_sitios_access())
  $edit_sitio = 1;
$count_msg = 0;
?>

<?php
if(in_array($user->uid, array(134061, 194264))) {

// Detectamos si el usuario tiene avisos publicados en espacios reusables
$mostrar_new = 0;
$camt_avisos_reusables = publicacion_avisos_cantidad_aviso_espacio_reusable($user->uid);
if($camt_avisos_reusables > 0){
  if(!isset($_COOKIE['infoNewReposicion'])){ 
    setcookie('infoNewReposicion', 1, time() + 60);
    $mostrar_new = 1;
  } else {
    if($_COOKIE['infoNewReposicion'] < 5)
      $mostrar_new = 1;
    $count = $_COOKIE['infoNewReposicion'];
    setcookie('infoNewReposicion', $count+1, time() + 60);
  }
}

// Obtenemos cantidad de creditos republicacion disponibles
$datos_rep = publicacion_avisos_datos_republicacion_masiva($user->uid);
if($datos_rep['con_paquete']) {
?>
  <div class="republicaciones">
<?php  if($datos_rep['cantidad'] > 0) { ?>
    <button class="republicacion-masiva republicar tooltip btn-rep" id="<?php print $user->uid; ?>">Reposicionar todos mis Destaques</button>
    <div class="republicacion-mensajes">
      <span class="tooltip msg-rep-res">Te <?php ($datos_rep['cantidad'] == 1) ? print 'queda' : print 'quedan'; ?> <?php print $datos_rep['cantidad']; ?> <?php ($datos_rep['cantidad'] == 1) ? print 'reposicionamiento masivo' : print 'reposicionamientos masivos'; ?> para usar hoy</span><br>
      <a href="/faq#reposicionamiento" class="republicacion-ayuda">¿Qué es esto?</a>
    </div>
<?php } else { ?>
    <a href="javascript:void(0);" class="republicacion-masiva deshabilitado tooltip btn-rep" id="tooltip-rep">Reposicionar todos mis Destaques</a>
    <div class="republicacion-mensajes deshabilitado">
      <span class="tooltip msg-rep-res">No tienes más reposiciones en este día</span><br>
      <a href="/faq#reposicionamiento" class="republicacion-ayuda">¿Qué es esto?</a>
    </div>
<?php   } ?>
  </div>
<?php } 
}
?>

<?php if(!in_array('particular', $user->roles)){ ?>
  <?php profile_load_profile($user); ?>
  <?php if(isset($user->profile_tipo_concesionaria) && $user->profile_tipo_concesionaria == 'Concesionaria') { ?>
    <div class="export-exel"><img src="/<?php print path_to_theme(); ?>/img/logo_excel.png" alt="Exportar avisos publicados a Excel" width="20" height="20"><a href="mis-avisos-clasificados-lavoz-autos.xls">Exportar avisos publicados a Excel</a></div>
  <?php } elseif(isset($user->profile_tipo_concesionaria) && $user->profile_tipo_concesionaria == 'Inmobiliaria') { ?>
    <div class="export-exel"><img src="/<?php print path_to_theme(); ?>/img/logo_excel.png" alt="Exportar avisos publicados a Excel" width="20" height="20"><a href="mis-avisos-inmuebles.xls">Exportar avisos publicados a Excel</a></div>
  <?php } else { ?>
    <div class="export-exel"><img src="/<?php print path_to_theme(); ?>/img/logo_excel.png" alt="Exportar avisos publicados a Excel" width="20" height="20"><a href="mis-avisos-clasificados-lavoz.xls">Exportar avisos publicados a Excel</a></div>
  <?php } ?>
<?php } ?>

<?php print $header['select'] ?>

<?php if(!in_array('concesionaria terceros', $user->roles)){ ?>
<div class="ayuda-moderado"><span></span>Avisos en moderación.</div>
<div class="ayuda-rechazado"><span></span>Avisos rechazados.</div>
<?php } ?>

<div class="ListadoAdministrador">
  <div class="clearfix"></div>
<?php foreach ($rows as $count => $row): ?>
<?php
  $nid = $row['nid'];
  $node = node_load($nid);
  if(! $node) {
    continue;
  }
  node_prepare($node, TRUE);
  $estado_sid = workflow_node_current_state($node);
  
  $workflow_choices = workflow_field_choices($node);
  $esta_publicado = ($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO);

  $tiene_fecha_inicio_futura = FALSE;
  $fecha_inicio_ts = strtotime($node->field_aviso_fecha_inicio[0]['value']);
  if($fecha_inicio_ts !== FALSE && $fecha_inicio_ts > time()) {
    $tiene_fecha_inicio_futura = TRUE;
  }

  $allowed_publicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_PUBLICADO, $workflow_choices);
  $allowed_despublicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_CANCELADO, $workflow_choices);

  if($tiene_fecha_inicio_futura) {
    $allowed_publicar = FALSE;
    $allowed_despublicar = FALSE;
  }

  $class_status_aviso = '';
  if($node->_workflow == WORKFLOW_AVISOS_ESTADO_PENDIENTE){
    $class_status_aviso = 'aviso_moderado';
  } elseif($node->_workflow == WORKFLOW_AVISOS_ESTADO_RECHAZADO){
    $class_status_aviso = 'aviso_rechazado';
  }
  
  if(module_exists('administracion_sitios') && $edit_sitio){
    //Aviso publicado en sitio
    if(administracion_sitios_get_aviso_destacado($nid))
      print '<div class="etiqueta_destacado">Destacado en mi sitio</div>';
  }
  
  $espacio_reusable = 0;
  $republicado = 0;
  if(in_array($user->uid, array(134061, 194264))) {
    $aviso_reusable = publicacion_avisos_aviso_espacio_reusable($nid);
    if($aviso_reusable->reusable){
      //Detectamos si el aviso no tiene republicaciones en el dia para habilitar el link
      $republicado = publicacion_avisos_republicacion_diaria($nid);
      $espacio_reusable = 1;
    }
  }
?>
<div class="AvisoItem AvisoItem-<?php print $node->nid; ?> <?php print $class_status_aviso; ?>">
  <div class="SelectBulk"><?php print $row['select']; ?></div>
  <div class="NodeTeaser"><?php echo node_view($node, TRUE, FALSE, FALSE); ?></div>
  <div class="Acciones">
    <div class="Botones">
<?php if($estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE): ?>      
      <div class="BotonEditar">
      <a href="<?php print url('node/'.$node->nid.'/edit', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Editar Aviso" class="grey-button pcb">
        <span>Editar</span>
      </a>
      </div>
<?php endif; ?>
<?php if(!in_array($node->type, variable_get('clasificados_rubros_ocultos', array())) && user_access('clone own nodes') && !in_array($estado_sid, array(WORKFLOW_AVISOS_ESTADO_PENDIENTE, WORKFLOW_AVISOS_ESTADO_RECHAZADO))) : ?>
      <div class="BotonClonar">
      <a href="<?php print url('node/'.$node->nid.'/clone', array('alias'=>TRUE)); ?>" title="Copiar Aviso" class="grey-button pcb">
        <span>Copiar</span>
      </a>
      </div>
<?php endif; ?>
<?php if($espacio_reusable && in_array($user->uid, array(134061, 194264))): ?>
<?php if($mostrar_new): ?>
      <div class="msg-nevo">Nuevo</div>
<?php endif; ?>
      <div class="BotonRepublicacion">
<?php if(!$republicado): ?>
      <a href="<?php print url('republicacion-individual-aviso/'.$node->nid.'/'.$user->uid, array('query' => array('destination' => $destination))); ?>" class="red-button pcb tooltip link-rep msg-<?php print $count_msg; ?>">
        <span>Reposicionar</span>
      </a>
<?php else: ?>
      <a href="javascript:void(0);" class="red-button pcb deshabilitado tooltip link-rep msg-<?php print $count_msg; ?>"><span>Reposicionar</span></a>
<?php endif; ?>
      </div>
<?php endif; ?>
<?php if($estado_sid != WORKFLOW_AVISOS_ESTADO_PUBLICADO && $allowed_publicar): ?>
      <div class="BotonPublicar">
      <a href="<?php print url('node/'.$node->nid.'/publicacion', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Publicar el Aviso" class="green-button pcb">
        <span>Publicar</span>
      </a>
      </div>
<?php elseif($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO && !in_array($node->type, variable_get('clasificados_rubros_ocultos', array()))): ?>
      <div class="BotonMejoras">
      <a href="<?php print url('node/'.$node->nid.'/publicacion', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Destacá tu aviso" class="orange-button pcb">
        <span>Destacá tu aviso</span>
      </a>
      </div>
<?php endif; ?>
<?php if($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO && $allowed_despublicar): ?>
      <div class="BotonDespublicar">
      <a href="<?php print url('node/'.$node->nid.'/despublicar', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Despublicar el Aviso" class="red-button pcb">
        <span>Despublicar</span>
      </a>
      </div>
<?php endif; ?>
<?php if($node->type == 'aviso_alquiler_temporario'): ?>      
      <div class="BotonDespublicar">
      <a href="<?php print url('node/'.$node->nid.'/edit', array('query' => array('destination' => $destination), 'alias'=>TRUE, 'fragment' => 'menu-calendario')); ?>" title="Editar Calendario" class="grey-button pcb">
        <span>Calendario</span>
      </a>
      </div>
<?php endif; ?>
<?php
global $user;
$usuario_id = $user->uid;
$rubro = clasificados_rubros_node_obtener_rubro_term($node);
$array_creditos = _publicacion_avisos_creditos_adquiridos_usuario_aviso($usuario_id, $rubro->tid);

$publicacion_papel = '';
if(in_array('particular', $user->roles) && $allowed_publicar) {
  $publicacion_papel = 'publicacion-papel';
} else if(in_array('concesionaria terceros', $user->roles) && $allowed_publicar) {
  if(isset($user->profile_tipo_concesionaria) && $user->profile_tipo_concesionaria == 'Inmobiliaria' && array_key_exists(AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID, $array_creditos)) {
  $publicacion_papel = 'publicacion-papel-inmobiliaria';
  } else if(in_array('concesionaria terceros', $user->roles) && isset($user->profile_tipo_concesionaria) && $user->profile_tipo_concesionaria == 'Concesionaria' && array_key_exists(AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID, $array_creditos)) {
    $publicacion_papel = 'publicacion-papel-concesionaria';
  } else if(in_array('concesionaria terceros', $user->roles) && isset($user->profile_tipo_concesionaria) && $user->profile_tipo_concesionaria == 'Comercio' && array_key_exists(AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID, $array_creditos)) {
    $publicacion_papel = 'publicacion-papel-comercio';
  }
}
?>
<?php if(!$tiene_fecha_inicio_futura && $estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE && !empty($publicacion_papel)): ?>
      <div class="BotonPublicar">
      <a href="<?php print url('node/'.$node->nid.'/publicacion', array('query' => array('destination' => $destination), 'alias'=>TRUE, 'fragment'=>$publicacion_papel)); ?>" title="Publicar en papel" class="green-button pcb">
        <span>Pub. Papel</span>
      </a>
      </div>
<?php endif; ?>
<?php if(! $tiene_fecha_inicio_futura && $estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE): ?>
      <div class="BotonEliminar">
      <a href="<?php print url('node/'.$node->nid.'/eliminar', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Eliminar el Aviso" class="red-button pcb">
        <span>Eliminar</span>
      </a>
      </div>
<?php endif; ?>
    </div>
  </div>
</div>
<div class="clearfix"></div>

<?php if($node->type == 'aviso_alquiler_temporario'): ?>
  <div id="masinfo-del-aviso-<?php print $node->nid; ?>" class="CalendariosDelAviso" style="display: none;">
    <div class="BoxResultado Borde">
      <div class="listadoAdminCalendar" id="calendario-frontend-<?php print $node->nid; ?>"></div>
<script>
$('#calendario-<?php print $node->nid; ?>').click(function(event) {  
  // if($('#masinfo-del-aviso-<?php print $node->nid; ?>').css('display') == 'none') {
    $('#masinfo-del-aviso-<?php print $node->nid; ?>').show('');
    $('#calendario-frontend-<?php print $node->nid; ?>').DOPFrontendBookingCalendarPRO({'loadURL': '/clvi_booking/load_data/<?php print $node->nid; ?>', 'sendURL': '/clvi_booking/save_data/<?php print $node->nid; ?>', 'view': 'true'});
  /* } else {
    $('#masinfo-del-aviso-<?php print $node->nid; ?>').hide('');
  }*/
});
</script>
    </div>
    <span class="dialogo"></span>
  </div>
  <div id="contacto-del-aviso-<?php print $node->nid; ?>" class="ContactosDelAviso contactosAlojamiento" style="display: none;"></div>
<?php else: ?>
  <div id="contacto-del-aviso-<?php print $node->nid; ?>" class="ContactosDelAviso" style="display: none;"></div>
<?php endif; ?>

<div class="clearfix"></div>
<?php $count_msg++; endforeach; ?>
</div>

<?php if(isset($_COOKIE['infoAdminAvisos'])){ ?>
<!-- <div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
<div id="popover" class="popover left" style="top: 418px; left: 477px;">
  <div class="arrow"></div>
  <h3 class="popover-title">Nuevos filtros</h3>
  <div class="popover-content">Con los nuevos filtros, ahora podés buscar más facil entre tus avisos.</div>
</div> -->
<?php setcookie('infoAdminAvisos', 1, time() + 60); 
 //unset($_COOKIE['infoAdminAvisos']); 
 } //Caduca en 10 años?>