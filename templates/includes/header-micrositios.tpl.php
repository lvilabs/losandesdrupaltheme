<?php if(!$es_sitios_principal) { ?>
<!-- Navigation -->
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      <?php if ($logo): ?>
        <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img alt="<?php print t('Home'); ?>" src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/logo-clasificado.png" >
        </a>
      <?php endif; ?>
    </div>
    <div class="col-lg-6">
      <div class="SinLoguear pull-right"><div class="Top"><ul class="Inner"><div class="region-login-superior"></div></ul></div></div> 
    </div>
  </div>
</div>

<?php } else { ?>

<div class="row">
  <div class="mobile-12 desk-4 columns">
    <span class="logo"><a class="topLink" href="/"></a></span>
  </div>
  <div class="mobile-12 desk-8 columns">
    <div class="login-links hide-sm-small">
      <div class="SinLoguear pull-right"><div class="Top"><ul class="Inner"></ul></div></div> 
      <!-- <a href="#" id="showModal" class="btn btn-success">Login</a> <span>|</span>
      <a  href="#veneficios">Registrarse</a> -->
    </div>
    <nav class="hide-sm-small">
      <a class="topLink" href="#inicio">Inicio</a> <span>|</span>
      <a class="topLink" href="#beneficios">Beneficios</a> <span>|</span>
      <a class="topLink" href="#ventajas">Ventajas</a> <span>|</span>
      <a class="topLink" href="#colores">Colores</a> <span>|</span>
      <a class="topLink" href="#planes">Planes</a>
    </nav>
    <nav class="naver custom-naver reset show-sm-small" data-naver-options='{"maxWidth":"740px"}'>
      <a href="#" id="showModal" class="btn btn-success">Login</a>
      <a  href="#veneficios">Registrarse</a>
      <a class="topLink" href="#inicio">Inicio</a>
      <a class="topLink" href="#beneficios">Beneficios</a>
      <a class="topLink" href="#ventajas">Ventajas</a>
      <a class="topLink" href="#colores">Colores</a>
      <a class="topLink" href="#planes">Planes</a>
    </nav>
  </div>
</div>
<?php } ?>