<?php
/**
 * @file views-bulk-operations-table.tpl.php
 * Template to display a VBO as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
global $user;
$icons_path = path_to_theme().'/img/icons/admin';
$destination = trim($_SERVER["REQUEST_URI"],'/');
$permisos = usuarios_colaboradores_get_permisos_user($user->uid);

if(!in_array('Despublicar avisos', $permisos) || !in_array('Eliminar avisos', $permisos)){
  drupal_add_js('
  $(document).ready(function($){
    $("#objects-selector-wrapper-new").css("display", "none");
    $("#views-bulk-operations-select").css("display", "none");
  });
  ', 'inline');
}

?>

<?php
if(in_array($user->uid, array(134061, 194264))) {

// Detectamos si el usuario tiene avisos publicados en espacios reusables
$mostrar_new = 0;
$camt_avisos_reusables = publicacion_avisos_cantidad_aviso_espacio_reusable($user->uid);
if($camt_avisos_reusables > 0){
  if(!isset($_COOKIE['infoNewReposicion'])){ 
    setcookie('infoNewReposicion', 1, time() + 60);
    $mostrar_new = 1;
  } else {
    if($_COOKIE['infoNewReposicion'] < 5)
      $mostrar_new = 1;
    $count = $_COOKIE['infoNewReposicion'];
    setcookie('infoNewReposicion', $count+1, time() + 60);
  }
}

// Obtenemos cantidad de creditos republicacion disponibles
$datos_rep = publicacion_avisos_datos_republicacion_masiva($user->uid);
if($datos_rep['con_paquete']) {
?>
  <div class="republicaciones">
<?php  if($datos_rep['cantidad'] > 0) { ?>
    <button class="republicacion-masiva republicar tooltip btn-rep" id="<?php print $user->uid; ?>">Reposicionar todos mis Destaques</button>
    <div class="republicacion-mensajes">
      <span class="tooltip msg-rep-res">Te <?php ($datos_rep['cantidad'] == 1) ? print 'queda' : print 'quedan'; ?> <?php print $datos_rep['cantidad']; ?> <?php ($datos_rep['cantidad'] == 1) ? print 'reposicionamiento masivo' : print 'reposicionamientos masivos'; ?> para usar hoy</span><br>
      <a href="/faq#reposicionamiento" class="republicacion-ayuda">¿Qué es esto?</a>
    </div>
<?php } else { ?>
    <a href="javascript:void(0);" class="republicacion-masiva deshabilitado tooltip btn-rep" id="tooltip-rep">Reposicionar todos mis Destaques</a>
    <div class="republicacion-mensajes deshabilitado">
      <span class="tooltip msg-rep-res">No tienes más reposiciones en este día</span><br>
      <a href="/faq#reposicionamiento" class="republicacion-ayuda">¿Qué es esto?</a>
    </div>
<?php   } ?>
  </div>
<?php } 
}
?>

<?php print $header['select'] ?>

<div class="ListadoAdministrador">
  <div class="clearfix"></div>
<?php foreach ($rows as $count => $row): ?>
<?php
  $nid = $row['nid'];
  $node = node_load($nid);
  if(! $node) {
    continue;
  }
  node_prepare($node, TRUE);
  $estado_sid = workflow_node_current_state($node);
  
  $workflow_choices = workflow_field_choices($node);
  $esta_publicado = ($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO);

  $tiene_fecha_inicio_futura = FALSE;
  $fecha_inicio_ts = strtotime($node->field_aviso_fecha_inicio[0]['value']);
  if($fecha_inicio_ts !== FALSE && $fecha_inicio_ts > time()) {
    $tiene_fecha_inicio_futura = TRUE;
  }

  $allowed_publicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_PUBLICADO, $workflow_choices);
  $allowed_despublicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_CANCELADO, $workflow_choices);

  if($tiene_fecha_inicio_futura) {
    $allowed_publicar = FALSE;
    $allowed_despublicar = FALSE;
  }

  $class_status_aviso = '';
  if($node->_workflow == WORKFLOW_AVISOS_ESTADO_PENDIENTE){
    $class_status_aviso = 'aviso_moderado';
  } elseif($node->_workflow == WORKFLOW_AVISOS_ESTADO_RECHAZADO){
    $class_status_aviso = 'aviso_rechazado';
  }
  
  $espacio_reusable = 0;
  $republicado = 0;
  if(in_array($user->uid, array(134061, 194264))) {
    $aviso_reusable = publicacion_avisos_aviso_espacio_reusable($nid);
    if($aviso_reusable->reusable){
      //Detectamos si el aviso no tiene republicaciones en el dia para habilitar el link
      $republicado = publicacion_avisos_republicacion_diaria($nid);
      $espacio_reusable = 1;
    }
  }
?>
<div class="AvisoItem AvisoItem-<?php print $node->nid; ?> <?php print $class_status_aviso; ?>">
<?php if(in_array('Despublicar avisos', $permisos) && in_array('Eliminar avisos', $permisos)){ ?>  
  <div class="SelectBulk"><?php print $row['select']; ?></div>
<?php } ?>
  <div class="NodeTeaser"><?php echo node_view($node, TRUE, FALSE, FALSE); ?></div>
  <!-- <div class="Informacion">
    <strong>Estado</strong>: <?php print $estado_nombre; ?><br />
    <strong>Destaque</strong>: <?php print ($tipo_espacio_node) ? $tipo_espacio_node->title : 'Ninguno'; ?><br />
    <strong>Publicado el</strong>: <?php print $node->field_aviso_fecha_inicio[0]['view']; ?><br />
    <strong>Finaliza el</strong>: <?php print $node->field_aviso_fecha_fin[0]['view']; ?><br />
    <strong>Visitas</strong>: <?php print $visitas_count; ?> <strong><?php print $cantidad_vistas_telefono>0?'('.$cantidad_vistas_telefono.' vistas teléfono)':''; ?></strong><br />
    <strong>Contactos</strong>: <?php print $contactos_count; ?><br />
    <strong>N° de Aviso</strong>: <?php print $node->nid; ?>
  </div> -->
  <div class="Acciones">
    <div class="Botones">
<?php if(in_array('Editar avisos', $permisos) && $estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE) : ?>
      <div class="BotonEditar">
      <a href="<?php print url('node/'.$node->nid.'/edit', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Editar Aviso" class="grey-button pcb">
        <span>Editar</span>
      </a>
      </div>
<?php endif; ?>
<?php if(!in_array($node->type, variable_get('clasificados_rubros_ocultos', array())) && user_access('clone own nodes') && in_array('Crear avisos', $permisos) && !in_array($estado_sid, array(WORKFLOW_AVISOS_ESTADO_PENDIENTE, WORKFLOW_AVISOS_ESTADO_RECHAZADO))) : ?>
      <div class="BotonClonar">
      <a href="<?php print url('node/'.$node->nid.'/clone', array('alias'=>TRUE)); ?>" title="Copiar Aviso" class="grey-button pcb">
        <span>Copiar</span>
      </a>
      </div>
<?php endif; ?>
<?php if($estado_sid != WORKFLOW_AVISOS_ESTADO_PUBLICADO && $allowed_publicar && (in_array('Publicar avisos', $permisos) || in_array('Utilizar espacios', $permisos))): ?>
      <div class="BotonPublicar">
      <a href="<?php print url('node/'.$node->nid.'/publicacion', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Publicar el Aviso" class="green-button pcb">
        <span>Publicar</span>
      </a>
      </div>
<?php endif; ?>
<?php if($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO && !in_array($node->type, variable_get('clasificados_rubros_ocultos', array())) && (in_array('Publicar avisos', $permisos) || in_array('Utilizar espacios', $permisos))): ?>
      <div class="BotonMejoras">
      <a href="<?php print url('node/'.$node->nid.'/publicacion', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Destacá tu aviso" class="orange-button pcb">
        <span>Destacá tu aviso</span>
      </a>
      </div>
<?php endif; ?>
<?php if($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO && $allowed_despublicar && in_array('Despublicar avisos', $permisos)): ?>
      <div class="BotonDespublicar">
      <a href="<?php print url('node/'.$node->nid.'/despublicar', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Despublicar el Aviso" class="red-button pcb">
        <span>Despublicar</span>
      </a>
      </div>
<?php endif; ?>
<?php if(! $tiene_fecha_inicio_futura && in_array('Eliminar avisos', $permisos) && $estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE): ?>
      <div class="BotonEliminar">
      <a href="<?php print url('node/'.$node->nid.'/eliminar', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Eliminar el Aviso" class="red-button pcb">
        <span>Eliminar</span>
      </a>
      </div>
<?php endif; ?>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div id="contacto-del-aviso-<?php print $node->nid; ?>" class="ContactosDelAviso" style="display: none;">

</div>
<div class="clearfix"></div>
<?php endforeach; ?>
</div>