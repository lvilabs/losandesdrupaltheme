<?php if($datos_sitio['template'] == 'template1'){ ?>

<div class="clearfix contentAvisosEmp" style="display: block;">
<?php
  $primero = 1;
  foreach($destacados as $aviso){
    if($primero > 4) break;
?>
  <div class="col-md-3">
    <div class="img-portfolio">
      <a href="/<?php print $aviso['path']; ?>">
        <img class="img-responsive img-hover" src="/<?php print imagecache_create_path('noticia_208_117', $aviso['filepath']); ?>" alt="">
      </a>
      <div class="col-md-6 info-section">
        <h3>
          <a href="/<?php print $aviso['path']; ?>"><?php print $aviso['title']; ?></a>
        </h3>
        <p><?php print $aviso['rubro']; ?></p>
        <p><?php if($aviso['barrio'] != '') print $aviso['barrio']; else print $aviso['ciudad']; ?></p>
      </div>
      <div class="col-md-6 price-section text-center">
        <h4 class="price"><?php print $aviso['precio']; ?></h4>
        <a href="/<?php print $aviso['path']; ?>" class="btn btn-primary">VER DETALLES</a>
      </div>
    </div>  
  </div>
<?php $primero++; } ?>
</div>

<?php } elseif($datos_sitio['template'] == 'template2'){ ?>

<div class="clearfix contentAvisosEmp" style="display: block;">
<?php
  $primero = 1;
  foreach($destacados as $aviso){
    if($primero > 4) break;
?>
  <div class="col-md-3 item-portfolio">
    <div class="img-portfolio">
      <div class="camera-icon">
        <i class="fa fa-camera"></i>
      </div>                         
      <a href="/<?php print $aviso['path']; ?>">
        <img class="img-responsive img-hover" src="/<?php print imagecache_create_path('noticia_208_117', $aviso['filepath']); ?>" alt="">
      </a>
    </div>  
    <div class="col-md-6 col-sm-6 info-section">
      <h3>
        <a href="/<?php print $aviso['path']; ?>"><?php print $aviso['title']; ?></a>
      </h3>
      <p><?php print $aviso['rubro']; ?></p>
      <p><?php if($aviso['barrio'] != '') print $aviso['barrio']; else print $aviso['ciudad']; ?></p>
    </div>
    <div class="col-md-6 col-sm-6 price-section text-right">
      <h4 class="price"><?php print $aviso['precio']; ?></h4>
      <a href="/<?php print $aviso['path']; ?>" class="btn btn-primary btn-black">VER DETALLES</a>
    </div>
  </div>
<?php $primero++; } ?>
</div>

<?php } elseif($datos_sitio['template'] == 'template3'){ ?>

<div class="clearfix contentAvisosEmp" style="display: block;">
<?php
  $primero = 1;
  foreach($destacados as $aviso){
    if($primero > 5) break;
?>
  <div class="col-md-15 col-sm-4">
    <div class="item-portfolio">
      <div class="img-portfolio">                      
        <a href="/<?php print $aviso['path']; ?>">
          <img class="img-responsive img-hover" src="/<?php print imagecache_create_path('noticia_208_117', $aviso['filepath']); ?>" alt="">
        </a>
      </div>  
      <div class="info-section">
        <h3><a href="/<?php print $aviso['path']; ?>"><?php print $aviso['title']; ?></a></h3>
        <p>Precio: <?php print $aviso['precio']; ?></p>
        <a href="/<?php print $aviso['path']; ?>" class="btn btn-primary">Ver más detalles</a>
      </div>
    </div>
  </div>
<?php $primero++; } ?>
</div>

<?php } ?>