<?php
/**
 * @file views-bulk-operations-table.tpl.php
 * Template to display a VBO as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
?>

<?php if(!in_array('particular', $user->roles)){ ?>
  <div class="export-exel"><img src="/<?php print path_to_theme(); ?>/img/logo_excel.png" alt="Exportar consultas a Excel" width="20" height="20"><a href="/contactos_recibidos/mis-contactos-clasificadoslavoz.xls">Exportar consultas a Excel</a></div>
<?php } ?>

<table class="<?php print $class; ?>">
   <?php if (!empty($title)) : ?>
     <caption><?php print $title; ?></caption>
   <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($header as $key => $value): ?>
        <?php if ($key == 'select') { ?>
          <th class="select"><?php print $value ?></th>
        <?php } else { ?>
          
        <?php } ?>
      <?php endforeach; ?>
          <th class="header-mensajes">Mensajes</th>
          <th> </th>
          <th class="header-acciones">Acciones</th>
    </tr>
  </thead>
</table>
<div>
    <?php foreach ($rows as $count => $row): ?>
      <?php
        $tipo_contacto = 'real';
        if(strpos($row['contacto_body'], 'vista de teléfono') !== FALSE) {
          $tipo_contacto = 'telefono';
        } elseif(strpos($row['contacto_body'], 'de WhatsApp') !== FALSE) {
          $tipo_contacto = 'whatsapp';
        } elseif(strpos($row['contacto_body'], 'vía E-mail') !== FALSE) {
          $tipo_contacto = 'mail';
        } elseif(strpos($row['contacto_body'], 'vía SMS') !== FALSE) {
          $tipo_contacto = 'sms';
        }
      ?>
      <div class="<?php print implode(' ', $row_classes[$count]); ?> <?php print $row['contacto_estado']==1?'no-leido':''; ?> <?php print $tipo_contacto; ?>">
        <?php if($tipo_contacto == 'telefono') { ?>
          <i class="fa fa-phone"></i>
        <?php } elseif($tipo_contacto == 'whatsapp') { ?>
          <i class="fab fa-whatsapp"></i>
        <?php } elseif($tipo_contacto == 'mail') { ?>
          <i class="far fa-envelope"></i>
        <?php } elseif($tipo_contacto == 'sms') { ?>
          <i class="far fa-comment-alt"></i>
        <?php } ?>
        <?php foreach ($row as $field => $content): ?>
          <?php if ($field == 'select' && !isset($fields[$field])) { ?>
          <div class="views-field select">
            <?php print $content; ?>
          </div>
          <?php } ?>
        <?php endforeach; ?>
        <?php
        $aviso = node_load(array('nid' => $row['nid']));
        $show_response_button = FALSE;
        if(! valid_email_address($row['contacto_email'])) {
          $mail_to_print = $row['contacto_email'];
        } else {
          $to = $row['contacto_email'];
          $show_response_button = TRUE;
          $fecha = substr($row['created'], 0, 10);
          $title = trim(drupal_html_to_text($aviso->title));
          $subject = 'Respuesta a tu consulta';
          $body = trim("Consulta realizada el {$fecha} sobre el Aviso \"{$title}\":\n".drupal_html_to_text($row['contacto_body']));
          $body = "\n\n".str_replace("\r\n", "\n", $body);

          $mail_to_print = '';
          //El utf8_decode es necesario porque la comunicación entre el mailto y el cliente de correo usualmente es en ASCII o ISO-8859-1
          //Se utiliza rawurlencode porque necesitamos que el espacio " " sea transformado en "%20" y no en "+" (esto último lo haria el urlencode normal).
          $mail_to_print_mailto = 'mailto:'.$to.'?subject='.rawurlencode(utf8_decode($subject)).'&body='.rawurlencode(utf8_decode($body));
          $mail_to_print = '<a href="'.$mail_to_print_mailto.'">'.$to.'</a>';
          $mail_to_print_button = '<a href="'.$mail_to_print_mailto.'" class="form-submit contacto-respuesta" id="respuesta-'.$row['id_cv'].'-'.$row['contacto_estado'].'">Responder</a>';
        }
        if(!empty($aviso->field_aviso_fotos[0]['filepath']))
          $foto = $aviso->field_aviso_fotos[0]['filepath'];
        else
          $foto = 'imagefield_default_images/RB_noimagen.jpg';
        ?>
        <div class="node-type-contacto-vendedor <?php print $classes; ?> clearfix">
          <div class="content contacto-vendedor">
            <div class="aviso-titulo"><span class="mini">Aviso:</span><?php print l($aviso->title, 'https://clasificados.losandes.com.ar/'.$aviso->path, array('attributes' => array('target' => '_blank'))); ?></div>
            <div class="aviso-foto"><?php print theme('imagecache', 'ficha_aviso_76_57', $foto, 'Foto del aviso', 'Foto del aviso'); ?></div>
            <div class="nombre"><span class="mini">Nombre:</span><?php print $row['contacto_nombre']; ?></div>
            <div class="fecha"><span class="mini">Fecha:</span><?php print $row['created']; ?></div>
            <div class="telefono"><span class="mini">Teléfono:</span><?php print $row['contacto_telefono']; ?></div>
            <div class="email"><span class="mini">Email:</span><?php print $mail_to_print; ?></div>
            <div class="accion">
              <span class="mini">Acción:</span>
              <select name="contacto_accion_select" class="form-select" id="contacto-accion-select-<?php print $row['id_cv']; ?>" style="display:none;">
                <option value=""></option>
                <option value="Respondido">Respondido</option>
                <option value="Hablar">Hablar</option>
                <option value="Llamar otro día">Llamar otro día</option>
                <option value="Contestado telefónicamente">Contestado telefónicamente</option>
              </select>
              <a class="contacto-accion-edit" id="contacto-accion-<?php print $row['id_cv']; ?>" href="javascript:void(0);" onClick="javascript:editar_accion_contacto(<?php print $row['id_cv']; ?>);"><?php if(empty($row['contacto_accion'])) print 'Sin acción'; else print $row['contacto_accion']; ?></a>
            </div>
            <div class="remitente">
              <span class="mini">Respondido por:</span>
              <input name="contacto_remitente_text" type="text" id="contacto-remitente-text-<?php print $row['id_cv']; ?>" style="display:none;">
              <a class="contacto-accion-edit" id="contacto-remitente-<?php print $row['id_cv']; ?>" href="javascript:void(0);" onClick="javascript:editar_remitente_contacto(<?php print $row['id_cv']; ?>);"><?php if(empty($row['contacto_remitente'])) print 'Sin especificar'; else print $row['contacto_remitente']; ?></a>
            </div>
            <div class="consulta"><span class="mini">Consulta:</span><p><?php print nl2br($row['contacto_body']); ?></p></div>
          </div>
        </div>
        <div class="acciones">
          <br />
          <?php if($row['contacto_estado']==1) { ?>
          <a href="/contacto/leido/<?php print $row['id_cv']; ?>?destination=<?php print request_uri(); ?>" class="form-submit">Marcar como leído</a>
          <?php } else if($row['contacto_estado']==2) { ?>
          <a href="/contacto/no-leido/<?php print $row['id_cv']; ?>?destination=<?php print request_uri(); ?>" class="form-submit">Marcar como no leído</a>
          <?php } ?>
<?php if($show_response_button): ?>
          <br />
          <?php print $mail_to_print_button; ?>
<?php endif; ?>
        </div>
      </div>
    <?php endforeach; ?>
</div>