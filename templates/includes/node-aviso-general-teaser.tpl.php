<?php 
global $user; 
$mostrar_contactos = 1; 
if(in_array('soporte fotografico', $user->roles)) {
  $mostrar_contactos = 0;
} elseif(in_array('colaborador inmobiliaria', $user->roles)) {
  $permisos = usuarios_colaboradores_get_permisos_user($user->uid);
  if(!in_array('Ver Consultas', $permisos))
    $mostrar_contactos = 0;
}

$visual_aviso->tipo_espacio_css_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_SUPER_PREMIUM_NID, 'Superpremium', $visual_aviso->tipo_espacio_css_class);
$visual_aviso->tipo_espacio_css_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_PREMIUM_NID, 'Premium', $visual_aviso->tipo_espacio_css_class);
$visual_aviso->tipo_espacio_css_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_A_NID, 'Estandar', $visual_aviso->tipo_espacio_css_class);
$visual_aviso->tipo_espacio_css_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_B_NID, 'Estandar', $visual_aviso->tipo_espacio_css_class);
$visual_aviso->tipo_espacio_css_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_C_NID, 'Estandar', $visual_aviso->tipo_espacio_css_class);

?>
<div class="BoxResultado Borde <?php print $visual_aviso->tipo_espacio_css_class; ?> <?php print $visual_aviso->destacado_color_css_class; ?>">
<!-- Nombre del tipo de clase "unico" "comoNuevo" "imperdible" -->
<?php if($visual_aviso->etiqueta_banderola_css_class != ''): ?>
  <span class="etiqueta <?php print $visual_aviso->etiqueta_banderola_css_class; ?>"></span>
<?php endif; ?>
  <div class="Left">
    <div class="foto">
      <a href="<?php print 'https://clasificados.losandes.com.ar/' . $node->path; /*url($node->path, array('alias'=>TRUE));*/ ?>"><span></span><?php print theme('imagecache', 'ficha_aviso_120_90', $visual_aviso->first_foto['filepath']); ?></a>
<?php if(isset($visual_aviso->multimedia['fotos']) && $visual_aviso->multimedia['fotos']['quantity'] > 0): ?>
        <span class="cintillo"><?php print $visual_aviso->multimedia['fotos']['quantity']; ?></span>
<?php endif; ?>
      <?php if(isset($node->disponible_venta) && $node->disponible_venta==1) { ?>
        <span class="DisponibleVenta"></span>
      <?php } ?>
    </div>
  </div>
  <div class="Right">
    <div class="avatar">
      <div class="Banner">
        <!-- user picture <?php //isn't neccesary in admin pages ?> -->
      </div>
        <p>- Particular -</p>
    </div>
  </div>
  <div class="Info clearfix">
    <div class="Left ItemGeneral">
      <div class="Modelo">
        <h2><a href="<?php print 'https://clasificados.losandes.com.ar/' . $node->path; /*url($node->path, array('alias'=>TRUE));*/ ?>"><?php print truncate_utf8($node->title, 55, TRUE, TRUE); ?></a></h2>
      </div>
      <div class="DescripcionLeft">
        <p><strong>N° Aviso:</strong> <?php print $node->nid; ?></p>
      </div>
<?php foreach ($visual_aviso->atributos as $key => $item): ?>
    <?php if($key == 'km' && $item == 'No Especificado'): ?>  
      <div class="<?php print $key; ?>">Km <?php print $item; ?></div>
    <?php elseif($key != 'combustible'): ?>
      <div class="<?php print $key; ?>"><?php print $item; ?></div>
    <?php endif; ?>
<?php endforeach; ?>
    </div>
    <div class="Descripcion"><!-- <p><?php //print $visual_aviso->cuerpo_teaser; ?></p> -->
      <div class="DescripcionLeft">
        <strong>Estado</strong>: <?php print $visual_aviso->estado_workflow; ?>
        <p><strong>Publicado el</strong>: <?php print $visual_aviso->publicado; ?></p>
        <p><strong>Finaliza el</strong>: <?php print $visual_aviso->finaliza; ?></p>
      </div>
      <div class="DescripcionRight">
        <p><strong>Destaque</strong>: <?php print $visual_aviso->destaque; ?></p>
        <p><strong>Visitas</strong>: <?php print $visual_aviso->visitas; ?> 
        
        <?php if($mostrar_contactos): ?>
          <?php if($visual_aviso->vistas_telefono > 0) : ?>
            (<span class="text-datos" data-ot="<?php print $visual_aviso->vistas_telefono; ?> vistas de teléfono" data-ot-delay="0"><?php print $visual_aviso->vistas_telefono; ?> <i class="fa fa-phone"></i> </span>)
          <?php endif; ?>
          <?php if($visual_aviso->vistas_whatsapp > 0) : ?>
            (<span class="text-datos" data-ot="<?php print $visual_aviso->vistas_whatsapp; ?> contactar por WhatsApp" data-ot-delay="0"><?php print $visual_aviso->vistas_whatsapp; ?> <i class="fab fa-whatsapp"></i></span>)
          <?php endif; ?>
          </p>
          <p><strong>Contactos</strong>: <?php print $visual_aviso->contactos; ?></p>
        <?php endif; ?>
        
      </div>
    </div>
<?php if($node->type == 'aviso_alquiler_temporario'): ?>    
    <?php 
      $reservas_pendientes = clvi_booking_aviso_reservas_pendientes($node->nid);
      $fecha_proxima = 0;
      foreach($reservas_pendientes as $reserva) {
        if($fecha_proxima == 0 || $reserva['created'] < $fecha_proxima)
          $fecha_proxima = $reserva['created'];
      }
      $fecha_fin_reserva = $fecha_proxima + 24*60*60;
      if(!empty($reservas_pendientes)) : ?>
        <div class="links-reservas">
          <i class="fa fa-circle"></i>
          <a class="link-reservas" href="/administrar/reservas/recibidas?status=1&title=<?php print $node->title; ?>"><?php print count($reservas_pendientes); (count($reservas_pendientes) == 1)? print ' Reserva Pendiente' : print ' Reservas Pendientes'; ?></a>
          <br>
          <p class="status-pending fl-right">
            <i class="fa fa-clock-o"></i>Caducan en: <span id="countdown-<?php print $node->nid; ?>" class="countdown dsCountDown ds-white"></span>
          </p>
        </div>
<script>
  jQuery(document).ready(function($){
    var fechaFin = new Date(<?php print $fecha_fin_reserva; ?>*1000);
    $('#countdown-<?php  print $node->nid; ?>').dsCountDown({
      endDate: new Date(fechaFin),
      titleDays: '',
      titleHours: '',
      titleMinutes: '',
      titleSeconds: ''
    });
    $('.ds-days').hide();
  });
</script>        
    <?php endif; ?>   
    <div class="link-calendario" id="calendario-<?php print $node->nid; ?>" aviso-id="<?php print $node->nid; ?>">
      <i class="fa fa-calendar-check-o"></i>
      <span>Ver calendario</span> 
    </div>
<?php endif; ?>    
  </div>
  <h3 class='pie-aviso-teaser'>
    <?php print implode(' - ', $visual_aviso->pie_information); ?>
  </h3>
<?php if(isset($node->field_aviso_precio_uva) &&  $node->field_aviso_precio_uva[0]['value']): ?>    
  <div class="search-precio-uva <?php print $node->nid; ?>" data-precio="<?php print $node->field_aviso_precio[0]['value']; ?>" data-moneda="<?php print $node->field_aviso_moneda[0]['value']; ?>" data-aviso="<?php print $node->nid; ?>"></div>
<?php endif; ?>    
</div>