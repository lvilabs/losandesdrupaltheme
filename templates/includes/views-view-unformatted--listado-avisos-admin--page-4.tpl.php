<?php
/**
 * @file views-bulk-operations-table.tpl.php
 * Template to display a VBO as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
global $user;
$id_cliente = arg(2);
$icons_path = path_to_theme().'/img/icons/admin';
$destination = trim($_SERVER["REQUEST_URI"],'/');
$edit_sitio = 0;
if(administracion_sitios_access())
  $edit_sitio = 1;
$count_msg = 0;
?>

<?php print $header['select'] ?>

<?php $cliente = user_load($id_cliente); ?>
<div class="titulo-vista">Avisos de: <?php print $cliente->name; ?></div>

<?php if(!in_array('concesionaria terceros', $user->roles)){ ?>
<div class="ayuda-moderado"><span></span>Avisos en moderación.</div>
<div class="ayuda-rechazado"><span></span>Avisos rechazados.</div>
<?php } ?>

<div class="ListadoAdministrador">
  <div class="clearfix"></div>
<?php foreach ($rows as $nid): ?>
<?php
  $nid = trim($nid);
  $node = node_load($nid);
  if(! $node) {
    continue;
  }
  node_prepare($node, TRUE);
  $estado_sid = workflow_node_current_state($node);
  
  $workflow_choices = workflow_field_choices($node);
  $esta_publicado = ($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO);

  $tiene_fecha_inicio_futura = FALSE;
  $fecha_inicio_ts = strtotime($node->field_aviso_fecha_inicio[0]['value']);
  if($fecha_inicio_ts !== FALSE && $fecha_inicio_ts > time()) {
    $tiene_fecha_inicio_futura = TRUE;
  }

  $allowed_publicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_PUBLICADO, $workflow_choices);
  $allowed_despublicar = array_key_exists(WORKFLOW_AVISOS_ESTADO_CANCELADO, $workflow_choices);

  if($tiene_fecha_inicio_futura) {
    $allowed_publicar = FALSE;
    $allowed_despublicar = FALSE;
  }

  $class_status_aviso = '';
  if($node->_workflow == WORKFLOW_AVISOS_ESTADO_PENDIENTE){
    $class_status_aviso = 'aviso_moderado';
  } elseif($node->_workflow == WORKFLOW_AVISOS_ESTADO_RECHAZADO){
    $class_status_aviso = 'aviso_rechazado';
  }
?>
<div class="AvisoItem AvisoItem-<?php print $node->nid; ?> <?php print $class_status_aviso; ?>">
  <div class="NodeTeaser"><?php echo node_view($node, TRUE, FALSE, FALSE); ?></div>
  <div class="Acciones">
    <div class="Botones">
<?php if($estado_sid != WORKFLOW_AVISOS_ESTADO_PENDIENTE): ?>      
      <div class="BotonEditar">
      <a href="<?php print url('node/'.$node->nid.'/edit', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Editar Aviso" class="grey-button pcb">
        <span>Editar</span>
      </a>
      </div>
<?php endif; ?>
<?php if(!in_array($node->type, variable_get('clasificados_rubros_ocultos', array())) && user_access('clone own nodes') && !in_array($estado_sid, array(WORKFLOW_AVISOS_ESTADO_PENDIENTE, WORKFLOW_AVISOS_ESTADO_RECHAZADO))) : ?>
      <div class="BotonClonar">
      <a href="<?php print url('node/'.$node->nid.'/clone', array('alias'=>TRUE)); ?>" title="Copiar Aviso" class="grey-button pcb">
        <span>Copiar</span>
      </a>
      </div>
<?php endif; ?>
<?php if($estado_sid == WORKFLOW_AVISOS_ESTADO_PUBLICADO && $allowed_despublicar): ?>
      <div class="BotonDespublicar">
      <a href="<?php print url('node/'.$node->nid.'/despublicar', array('query' => array('destination' => $destination), 'alias'=>TRUE)); ?>" title="Despublicar el Aviso" class="red-button pcb">
        <span>Despublicar</span>
      </a>
      </div>
<?php endif; ?>
    </div>
  </div>
</div>
<div class="clearfix"></div>

<?php if($node->type == 'aviso_alquiler_temporario'): ?>
  <div id="masinfo-del-aviso-<?php print $node->nid; ?>" class="CalendariosDelAviso" style="display: none;">
    <div class="BoxResultado Borde">
      <div class="listadoAdminCalendar" id="calendario-frontend-<?php print $node->nid; ?>"></div>
<script>
$('#calendario-<?php print $node->nid; ?>').click(function(event) {  
  // if($('#masinfo-del-aviso-<?php print $node->nid; ?>').css('display') == 'none') {
    $('#masinfo-del-aviso-<?php print $node->nid; ?>').show('');
    $('#calendario-frontend-<?php print $node->nid; ?>').DOPFrontendBookingCalendarPRO({'loadURL': '/clvi_booking/load_data/<?php print $node->nid; ?>', 'sendURL': '/clvi_booking/save_data/<?php print $node->nid; ?>', 'view': 'true'});
  /* } else {
    $('#masinfo-del-aviso-<?php print $node->nid; ?>').hide('');
  }*/
});
</script>
    </div>
    <span class="dialogo"></span>
  </div>
<?php endif; ?>

<div class="clearfix"></div>
<?php $count_msg++; endforeach; ?>
</div>