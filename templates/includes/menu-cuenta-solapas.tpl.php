<?php 
global $user;
if(!isset($_GET['destination']) || strpos($_GET['destination'], '?share=1') === false){ 
  if(!isset($user->profile_tipo_concesionaria)) {
    profile_load_profile($user);
  }
  $perfil = $user;
  $links_general = array();
  $links_inmobiliaria = array();
  $links_concesionaria = array();
  $links_tienda = array();

  //link colaboradores
  if(module_exists('usuarios_colaboradores') && array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){ 
    $activo = 0;
    if($template_files[1]=='page-lista-colaboradores')
      $activo = 1;
    $links_inmobiliaria['colaboradores'] = array('name' => 'Colaboradores',
                                            'title' => 'Colaboradores',
                                            'url' => '/lista/colaboradores/'.$user->uid,
                                            'activo' => $activo
                                          );
    //$links_concesionaria['colaboradores'] = $links_inmobiliaria['colaboradores'];
  }
  //link agenda
  if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && module_exists('agenda_contactos')) {
    if(in_array($perfil->profile_tipo_concesionaria, array('Inmobiliaria','Concesionaria'))){
      $activo = 0;
      if($template_files[1]=='page-administrar-agenda')
        $activo = 1;
      $links_inmobiliaria['agenda'] = array('name' => 'Agenda',
                                      'title' => 'Agenda',
                                      'url' => '/administrar/agenda/propietarios',
                                      'activo' => $activo
                                    );
      $links_concesionaria['agenda'] = $links_inmobiliaria['agenda'];
      
      if((array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && $perfil->profile_tipo_concesionaria=='Inmobiliaria')) {
        $links_inmobiliaria['propietarios'] = array('name' => 'Propietarios',
                                        'title' => 'Agenda de Propietarios',
                                        'url' => '/administrar/agenda/propietarios',
                                        'activo' => $activo
                                      );
      }
      if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && in_array($perfil->profile_tipo_concesionaria, array('Inmobiliaria','Concesionaria'))) {
        $links_inmobiliaria['vendedores'] = array('name' => 'Vendedores',
                                        'title' => 'Agenda de Vendedores',
                                        'url' => '/administrar/agenda/vendedores',
                                        'activo' => $activo
                                      );
        $links_concesionaria['vendedores'] = $links_inmobiliaria['vendedores'];
      }
      if((array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && in_array($perfil->profile_tipo_concesionaria, array('Inmobiliaria','Concesionaria')))) {
        $links_inmobiliaria['interesados'] = array('name' => 'Interesados',
                                        'title' => 'Agenda de Interesados',
                                        'url' => '/administrar/agenda/interesados',
                                        'activo' => $activo
                                      );
        $links_concesionaria['interesados'] = $links_inmobiliaria['interesados'];
      }
    }
  }
  //link seguros
  /*if($perfil->profile_vendedor_seguros==1) {
    $activo = 0;
    if($template_files[1]=='page-administrar-seguros')
      $activo = 1;
    $links_concesionaria['seguros'] = array('name' => 'Seguros',
                                    'title' => 'Seguros Compara en Casa',
                                    'url' => '/administrar/seguros',
                                    'activo' => $activo
                                  );
    $links_inmobiliaria['seguros'] = $links_concesionaria['seguros'];
  }*/
  //link creditos
  /*if(module_exists('creditos')) {
    $financiera_uid = variable_get('creditos_financiera_uid', 0);
    if((array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && $perfil->profile_tipo_concesionaria=='Concesionaria') || $financiera_uid==$user->uid) {
      $activo = 0;
      if($template_files[1]=='page-administrar-creditos')
        $activo = 1;
      $links_concesionaria['creditos'] = array('name' => 'Créditos',
                                      'title' => 'Créditos Prendarios',
                                      'url' => '/administrar/creditos',
                                      'activo' => $activo
                                    );
      $links_inmobiliaria['creditos'] = $links_concesionaria['creditos'];
    }
  }*/
  //link mi web
  if(module_exists('administracion_sitios') && administracion_sitios_access()){
    $mi_sitio = administracion_sitios_get_all($user->uid);
    $url_mi_sitio = '/administrar/sitio/editor';

    $activo = 0;
    if($template_files[1]=='page-administrar-sitio')
      $activo = 1;
    $links_general['web'] = array('name' => 'Mi página web',
                                    'title' => 'Mi página web',
                                    'url' => $url_mi_sitio,
                                    'activo' => $activo
                                  );
    $links_inmobiliaria['web'] = $links_general['web'];
    $links_concesionaria['web'] = $links_general['web'];
    $links_tienda['web'] = $links_general['web'];
  }
  //link mis datos
  $activo = 0;
  if($template_files[0]=='page-user')
    $activo = 1;
  $links_general['mis-datos'] = array('name' => 'Cuenta',
                                          'title' => 'Mis datos',
                                          'url' => '/user/'.$user->uid.'/edit',
                                          'activo' => $activo
                                        );
  $links_inmobiliaria['mis-datos'] = $links_general['mis-datos'];
  $links_concesionaria['mis-datos'] = $links_general['mis-datos'];
  $links_tienda['mis-datos'] = $links_general['mis-datos'];
  
  $links_general['cuenta'] = array('name' => 'Datos Usuario',
                                          'title' => 'Mis datos de usuario',
                                          'url' => '/user/'.$user->uid.'/edit',
                                          'activo' => $activo
                                        );
  $links_inmobiliaria['cuenta'] = $links_general['cuenta'];
  $links_concesionaria['cuenta'] = $links_general['cuenta'];
  $links_tienda['cuenta'] = $links_general['cuenta'];
  
  $links_general['datos-personales'] = array('name' => 'Datos Personales',
                                          'title' => 'Mis datos personales',
                                          'url' => '/user/'.$user->uid.'/edit/Datos Personales',
                                          'activo' => $activo
                                        );
  $links_inmobiliaria['datos-personales'] = $links_general['datos-personales'];
  $links_concesionaria['datos-personales'] = $links_general['datos-personales'];
  $links_tienda['datos-personales'] = $links_general['datos-personales'];
  
  if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)) {
    $links_inmobiliaria['datos-fiscales'] = array('name' => 'Datos Fiscales',
                                            'title' => 'Mis datos Fiscales',
                                            'url' => '/user/'.$user->uid.'/edit/Datos Fiscales',
                                            'activo' => $activo
                                          );
    $links_concesionaria['datos-fiscales'] = $links_inmobiliaria['datos-fiscales'];
    $links_tienda['datos-fiscales'] = $links_inmobiliaria['datos-fiscales'];
  }
  //link Compras Espacios y Mejoras
  $activo = 0;
  if($template_files[0]=='page-estado_cuenta')
    $activo = 1;
  $links_general['compras'] = array('name' => 'Compras de Espacios y Mejoras',
                                          'title' => 'Compras de espacios y mejoras realizadas',
                                          'url' => '/estado_cuenta',
                                          'activo' => $activo
                                        );
  $links_concesionaria['compras'] = $links_general['compras'];
  $links_inmobiliaria['compras'] = $links_general['compras'];
  $links_tienda['compras'] = $links_inmobiliaria['compras'];
  //link ventas y zonas  
  if(module_exists('ventas') && ventas_es_vendedor($user->uid)) {
    $activo = 0;
    if($template_files[1]=='page-administrar-ventas' && $template_files[2]!='page-administrar-ventas-zonas')
      $activo = 1;
    $links_tienda['ventas'] = array('name' => 'Ventas',
                                    'title' => 'Pedidos',
                                    'url' => '/administrar/ventas/pedidos',
                                    'activo' => $activo
                                  );
    $activo = 0;
    if($template_files[1]=='page-administrar-ventas' && $template_files[2]=='page-administrar-ventas-zonas')
      $activo = 1;
    $links_tienda['zonas'] = array('name' => 'Zonas',
                                    'title' => 'Administrar zonas',
                                    'url' => '/administrar/ventas/zonas',
                                    'activo' => $activo
                                  );
    $links_tienda['lugares'] = array('name' => 'Lugares de entrega',
                                    'title' => 'Administrar lugares',
                                    'url' => '/administrar/ventas/lugares',
                                    'activo' => $activo
                                  );
                                  
    if($user->uid == SITIO_LOCAL_UID_COLECCIONES) {
      $links_tienda['destacados'] = array('name' => 'Avisos destacados',
                                          'title' => 'Avisos destacados home',
                                          'url' => '/administrar/destacados/home',
                                          'activo' => $activo
                                        );                              
    }
    
  }
  //link Abono mensual
  $activo = 0;
  if($template_files[0]=='page-compra-papel')
    $activo = 1;
  $links_general['abono'] = array('name' => 'Abono Mensual',
                                  'title' => 'Abono Mensual',
                                  'url' => '/abono-mensual',
                                  'activo' => $activo
                                );
  $links_inmobiliaria['abono'] = $links_general['abono'];
  $links_concesionaria['abono'] = $links_general['abono'];
  $links_tienda['abono'] = $links_general['abono'];
  //link compra de paquetes
  $activo = 0;
  if($template_files[0]=='page-compra-paquetes')
    $activo = 1;
  $links_general['paquetes'] = array('name' => 'Compras de Paquetes',
                                  'title' => 'Compras de paquetes',
                                  'url' => '/compra-paquetes',
                                  'activo' => $activo
                                );
  $links_inmobiliaria['paquetes'] = $links_general['paquetes'];
  $links_concesionaria['paquetes'] = $links_general['paquetes'];
  $links_tienda['paquetes'] = $links_general['paquetes'];
  //link Consultas Recibidas
  $activo = 0;
  if($template_files[0]=='page-contactos_recibidos')
    $activo = 1;
  $links_general['consultas'] = array('name' => 'Contactos Recibidos',
                                  'title' => 'Listado de contactos recibidos',
                                  'url' => '/contactos_recibidos',
                                  'activo' => $activo
                                );
  $links_inmobiliaria['consultas'] = $links_general['consultas'];
  $links_concesionaria['consultas'] = $links_general['consultas'];
  //link Consultas realizadas
  $activo = 0;
  if($template_files[0]=='page-compra-papel')
    $activo = 1;
  $links_general['consultas-realizadas'] = array('name' => 'Contactos Realizados',
                                  'title' => 'Listado de contactos realizados',
                                  'url' => '/administrar/ventas/comprador/consultas',
                                  'activo' => $activo
                                );
  $links_inmobiliaria['consultas-realizadas'] = $links_general['consultas-realizadas'];
  $links_concesionaria['consultas-realizadas'] = $links_general['consultas-realizadas'];
  
  //link Interesados
  $activo = 0;
  if($template_files[2]=='page-administrar-contactos-interesados')
    $activo = 1;
  $links_general['interesados'] = array('name' => 'Interesados',
                                  'title' => 'Listado de interesados por aviso',
                                  'url' => '/administrar/contactos/interesados',
                                  'activo' => $activo
                                );
  $links_inmobiliaria['interesados'] = $links_general['interesados'];
  $links_concesionaria['interesados'] = $links_general['interesados'];  
  
if((array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) || array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_WEB, $user->roles)) && $perfil->profile_tipo_concesionaria=='Concesionaria') { ?> 
  
  <!-- Menu Concesionarias -->
  <div class="menuSolapas clearfix">
    <ul class="accesos">
<?php if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){ ?>
        <li class="<?php print ($template_files[0] == 'page-administrar' && in_array($template_files[1], array('page-administrar-principal', 'page-administrar-estadisticas_globales_autos')))?'active':''; ?> resumen"><a href="/administrar/principal" title=" Resumen ">Resumen</a></li>
<?php } ?>
      <li class="<?php print (($template_files[0]=='page-administrar' && !in_array($template_files[1], array('page-administrar-agenda', 'page-administrar-seguros', 'page-administrar-sitio', 'page-administrar-creditos', 'page-administrar-ventas', 'page-administrar-principal', 'page-administrar-reservas', 'page-administrar-estadisticas_globales_autos', 'page-administrar-historial', 'page-administrar-contactos'))) || (isset($node) && sitio_local_es_nodo_aviso($node->type) && isset($template_files[2]) && in_array($template_files[2], array('page-node-edit', 'page-node-clone', 'page-node-eliminar', 'page-node-despublicar'))))?'active':''; ?>">
        <?php
          $url_avisos = '/administrar/mis-avisos.html';
          //Si el usuario es Inmobiliaria o Concesionaria, aplicamos el filtro a la url
          if($perfil->profile_tipo_concesionaria == 'Inmobiliaria')
            $url_avisos = '/administrar/mis-avisos.html?term_rubros=6330';
          elseif($perfil->profile_tipo_concesionaria == 'Concesionaria')
            $url_avisos = '/administrar/mis-avisos.html?term_rubros=6323';
        ?>
        <a href="<?php print_r($url_avisos); ?>" title=" Mis avisos ">Mis avisos</a>
      </li>
      <li class="<?php print ((isset($template_files[1]) && $template_files[1]=='page-node-add') || (isset($template_files[2]) && $template_files[2]=='page-node-publicacion'))?'active':''; ?>">
        <?php if(arg(2) == "publicacion") { ?>
          <a title=" Publicación del aviso " href="/node/<?php print arg(1); ?>/publicacion">Publicá</a>
        <?php } else { ?>
          <!-- <a title="Crear Aviso" href="?width=640&height=190&inline=true&title= #modal-publicacion-rubros" class="colorbox-inline">Publicá</a> -->
          <a title="Crear Aviso" rel="nofollow"  href="javascript:void(0);" class="publicar-btn">Publicá</a>
        <?php } ?>
      </li>
<?php if(isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') { ?>
      <li class="<?php print (isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') ? 'active' : ''; ?>"><a title="Pagar por DineroMail" href="/dineromail/<?php print arg(1); ?>/confirmar">DineroMail</a></li>
<?php } ?>
      <li class="more contactos <?php print ($template_files[0]=='page-contactos_recibidos' || $template_files[3]=='page-administrar-ventas-comprador-consultas' || $template_files[2]=='page-administrar-contactos-interesados')?'active':''; ?>">
        <a href="/contactos_recibidos" title=" Contactos ">Contactos</a>
        <ul class="more-links contactos-links" style="display: none;">
          <?php if(isset($links_concesionaria['consultas'])) { ?>
            <li><a href="<?php print $links_concesionaria['consultas']['url']; ?>" title="<?php print $links_concesionaria['consultas']['title']; ?>"><?php print $links_concesionaria['consultas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['consultas-realizadas'])) { ?>
            <li><a href="<?php print $links_concesionaria['consultas-realizadas']['url']; ?>" title="<?php print $links_concesionaria['consultas-realizadas']['title']; ?>"><?php print $links_concesionaria['consultas-realizadas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['interesados']) && array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)) { ?>
            <li><a href="<?php print $links_concesionaria['interesados']['url']; ?>" title="<?php print $links_concesionaria['interesados']['title']; ?>"><?php print $links_concesionaria['interesados']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <li class="more cuenta <?php print (in_array($template_files[0], array('page-estado_cuenta', 'page-compra-paquetes', 'page-compra-papel', 'page-abono-mensual')) || ($template_files[2] == 'page-administrar-ventas-comprador' && $template_files[3] == 'page-admin'))? 'active' : ''; ?>">
        <a href="/abono-mensual" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
        <ul class="more-links cuenta-links" style="display: none;">
          <?php if(isset($links_concesionaria['abono'])) { ?>
            <li><a href="<?php print $links_concesionaria['abono']['url']; ?>" title="<?php print $links_concesionaria['abono']['title']; ?>"><?php print $links_concesionaria['abono']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['paquetes'])) { ?>
            <li><a href="<?php print $links_concesionaria['paquetes']['url']; ?>" title="<?php print $links_concesionaria['paquetes']['title']; ?>"><?php print $links_concesionaria['paquetes']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['compras'])) { ?>
            <li><a href="<?php print $links_concesionaria['compras']['url']; ?>" title="<?php print $links_concesionaria['compras']['title']; ?>"><?php print $links_concesionaria['compras']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['compras-productos'])) { ?>
            <li><a href="<?php print $links_concesionaria['compras-productos']['url']; ?>" title="<?php print $links_concesionaria['compras-productos']['title']; ?>"><?php print $links_concesionaria['compras-productos']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['papel'])) { ?>
            <li><a href="<?php print $links_concesionaria['papel']['url']; ?>" title="<?php print $links_concesionaria['papel']['title']; ?>"><?php print $links_concesionaria['papel']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <li class="more afiliados <?php print (in_array($template_files[1], array('page-administrar-seguros','page-administrar-creditos', 'page-administrar-sitio')))?'active':''; ?>">
        <a href="javascript: void(0);" title="Oportunidades">Oportunidades</a>
        <ul class="more-links afiliados-links" style="display: none;">
          <?php if(isset($links_concesionaria['seguros'])) { ?>
            <li><a href="<?php print $links_concesionaria['seguros']['url']; ?>" title="<?php print $links_concesionaria['seguros']['title']; ?>"><?php print $links_concesionaria['seguros']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_concesionaria['creditos'])) { ?>
            <li><a href="<?php print $links_concesionaria['creditos']['url']; ?>" title="<?php print $links_concesionaria['creditos']['title']; ?>"><?php print $links_concesionaria['creditos']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php if(isset($links_concesionaria['agenda'])) { ?>
        <li class="more agenda <?php print ($links_concesionaria['agenda']['activo'] == 1)?'active':''; ?>">
          <a href="/administrar/agenda/vendedores" title="<?php print $links_concesionaria['agenda']['title']; ?>"><?php print $links_concesionaria['agenda']['name']; ?></a>
          <ul class="more-links agenda-links" style="display: none;">
            <?php if(isset($links_concesionaria['propietarios'])) { ?>
              <li><a href="<?php print $links_concesionaria['propietarios']['url']; ?>" title="<?php print $links_concesionaria['propietarios']['title']; ?>"><?php print $links_concesionaria['propietarios']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['vendedores'])) { ?>
              <li><a href="<?php print $links_concesionaria['vendedores']['url']; ?>" title="<?php print $links_concesionaria['vendedores']['title']; ?>"><?php print $links_concesionaria['vendedores']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['interesados'])) { ?>
              <li><a href="<?php print $links_concesionaria['interesados']['url']; ?>" title="<?php print $links_concesionaria['interesados']['title']; ?>"><?php print $links_concesionaria['interesados']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <?php if(isset($links_concesionaria['mis-datos'])) { ?>
        <li class="more mis-datos <?php print ($links_concesionaria['mis-datos']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_concesionaria['mis-datos']['url']; ?>" title="<?php print $links_concesionaria['mis-datos']['title']; ?>"><?php print $links_concesionaria['mis-datos']['name']; ?></a>
          <ul class="more-links mis-datos-links" style="display: none;">
            <?php if(isset($links_concesionaria['cuenta'])) { ?>
              <li><a href="<?php print $links_concesionaria['cuenta']['url']; ?>" title="<?php print $links_concesionaria['cuenta']['title']; ?>"><?php print $links_concesionaria['cuenta']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['datos-personales'])) { ?>
              <li><a href="<?php print $links_concesionaria['datos-personales']['url']; ?>" title="<?php print $links_concesionaria['datos-personales']['title']; ?>"><?php print $links_concesionaria['datos-personales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['datos-fiscales'])) { ?>
              <li><a href="<?php print $links_concesionaria['datos-fiscales']['url']; ?>" title="<?php print $links_concesionaria['datos-fiscales']['title']; ?>"><?php print $links_concesionaria['datos-fiscales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['datos-valoracion'])) { ?>
              <li><a href="<?php print $links_concesionaria['datos-valoracion']['url']; ?>" title="<?php print $links_concesionaria['datos-valoracion']['title']; ?>"><?php print $links_concesionaria['datos-valoracion']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <?php if(isset($links_concesionaria['reservas'])) { ?>
        <li class="more reservas <?php print ($links_concesionaria['reservas']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_concesionaria['reservas']['url']; ?>" title="<?php print $links_concesionaria['reservas']['title']; ?>"><?php print $links_concesionaria['reservas']['name']; ?></a>
          <ul class="more-links reservas-links" style="display: none;">
            <?php if(isset($links_concesionaria['reservas-realizadas'])) { ?>
              <li><a href="<?php print $links_concesionaria['reservas-realizadas']['url']; ?>" title="<?php print $links_concesionaria['reservas-realizadas']['title']; ?>"><?php print $links_concesionaria['reservas-realizadas']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_concesionaria['reservas-recibidas'])) { ?>
              <li><a href="<?php print $links_concesionaria['reservas-recibidas']['url']; ?>" title="<?php print $links_concesionaria['reservas-recibidas']['title']; ?>"><?php print $links_concesionaria['reservas-recibidas']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
    </ul>
  </div>
  <!-- Fin Menu Concesionarias -->

<?php } elseif((array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) || array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_WEB, $user->roles)) && $perfil->profile_tipo_concesionaria=='Inmobiliaria') { ?>
  
  <!-- Menu Inmobiliarias -->
  <div class="menuSolapas clearfix">
    <ul class="accesos">
<?php if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){ ?>
        <li class="<?php print ($template_files[0]=='page-administrar' && in_array($template_files[1], array('page-administrar-principal', 'page-administrar-estadisticas_globales')))?'active':''; ?> resumen"><a href="/administrar/principal" title=" Resumen ">Resumen</a></li>
<?php } ?>
      <li class="<?php print (($template_files[0]=='page-administrar' && !in_array($template_files[1], array('page-administrar-agenda', 'page-administrar-seguros', 'page-administrar-sitio', 'page-administrar-creditos', 'page-administrar-ventas', 'page-administrar-principal', 'page-administrar-reservas', 'page-administrar-estadisticas_globales', 'page-administrar-historial', 'page-administrar-contactos'))) || (isset($node) && sitio_local_es_nodo_aviso($node->type) && isset($template_files[2]) && in_array($template_files[2], array('page-node-edit', 'page-node-clone', 'page-node-eliminar', 'page-node-despublicar'))))?'active':''; ?>">
        <?php
          $url_avisos = '/administrar/mis-avisos.html';
          //Si el usuario es Inmobiliaria o Concesionaria, aplicamos el filtro a la url
          if($perfil->profile_tipo_concesionaria == 'Inmobiliaria')
            $url_avisos = '/administrar/mis-avisos.html?term_rubros=6330';
          elseif($perfil->profile_tipo_concesionaria == 'Concesionaria')
            $url_avisos = '/administrar/mis-avisos.html?term_rubros=6323';
        ?>
        <a href="<?php print_r($url_avisos); ?>" title=" Mis avisos ">Mis avisos</a>
      </li>
      <li class="<?php print ((isset($template_files[1]) && $template_files[1]=='page-node-add') || (isset($template_files[2]) && $template_files[2]=='page-node-publicacion'))?'active':''; ?>">
        <?php if(arg(2) == "publicacion") { ?>
          <a title=" Publicación del aviso " href="/node/<?php print arg(1); ?>/publicacion">Publicá</a>
        <?php } else { ?>
          <!-- <a title="Crear Aviso" href="?width=640&height=190&inline=true&title= #modal-publicacion-rubros" class="colorbox-inline">Publicá</a> -->
          <a title="Crear Aviso" rel="nofollow"  href="javascript:void(0);" class="publicar-btn">Publicá</a>
        <?php } ?>
      </li>
<?php if(isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') { ?>
      <li class="<?php print (isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') ? 'active' : ''; ?>"><a title="Pagar por DineroMail" href="/dineromail/<?php print arg(1); ?>/confirmar">DineroMail</a></li>
<?php } ?>
      <li class="more contactos <?php print ($template_files[0]=='page-contactos_recibidos' || $template_files[3]=='page-administrar-ventas-comprador-consultas' || $template_files[2]=='page-administrar-contactos-interesados')?'active':''; ?>">
        
        <span id="alerta-contactos-totales" class="DN alerta"></span>
        
        <a href="/contactos_recibidos" title=" Contactos ">Contactos</a>
        <ul class="more-links contactos-links" style="display: none;">
          <?php if(isset($links_inmobiliaria['consultas'])) { ?>
            
            <span id="alerta-contactos" class="DN alerta int">30</span>
            
            <li><a href="<?php print $links_inmobiliaria['consultas']['url']; ?>" title="<?php print $links_inmobiliaria['consultas']['title']; ?>"><?php print $links_inmobiliaria['consultas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['consultas-realizadas'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['consultas-realizadas']['url']; ?>" title="<?php print $links_inmobiliaria['consultas-realizadas']['title']; ?>"><?php print $links_inmobiliaria['consultas-realizadas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['interesados']) && array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)) { ?>
            
            <span id="alerta-interesados" class="DN alerta int">20</span>
            
            <li><a href="<?php print $links_inmobiliaria['interesados']['url']; ?>" title="<?php print $links_inmobiliaria['interesados']['title']; ?>"><?php print $links_inmobiliaria['interesados']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <li class="more cuenta <?php print (in_array($template_files[0], array('page-estado_cuenta', 'page-compra-paquetes', 'page-compra-papel', 'page-abono-mensual')) || ($template_files[2] == 'page-administrar-ventas-comprador' && $template_files[3] == 'page-admin'))? 'active' : ''; ?>">
        <a href="/abono-mensual" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
        <ul class="more-links cuenta-links" style="display: none;">
          <?php if(isset($links_inmobiliaria['abono'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['abono']['url']; ?>" title="<?php print $links_inmobiliaria['abono']['title']; ?>"><?php print $links_inmobiliaria['abono']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['paquetes'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['paquetes']['url']; ?>" title="<?php print $links_inmobiliaria['paquetes']['title']; ?>"><?php print $links_inmobiliaria['paquetes']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['compras'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['compras']['url']; ?>" title="<?php print $links_inmobiliaria['compras']['title']; ?>"><?php print $links_inmobiliaria['compras']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['compras-productos'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['compras-productos']['url']; ?>" title="<?php print $links_inmobiliaria['compras-productos']['title']; ?>"><?php print $links_inmobiliaria['compras-productos']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_inmobiliaria['papel'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['papel']['url']; ?>" title="<?php print $links_inmobiliaria['papel']['title']; ?>"><?php print $links_inmobiliaria['papel']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php if(isset($links_inmobiliaria['agenda'])) { ?>
        <li class="more agenda <?php print ($links_inmobiliaria['agenda']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_inmobiliaria['agenda']['url']; ?>" title="<?php print $links_inmobiliaria['agenda']['title']; ?>"><?php print $links_inmobiliaria['agenda']['name']; ?></a>
          <ul class="more-links agenda-links" style="display: none;">
            <?php if(isset($links_inmobiliaria['propietarios'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['propietarios']['url']; ?>" title="<?php print $links_inmobiliaria['propietarios']['title']; ?>"><?php print $links_inmobiliaria['propietarios']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['vendedores'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['vendedores']['url']; ?>" title="<?php print $links_inmobiliaria['vendedores']['title']; ?>"><?php print $links_inmobiliaria['vendedores']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['interesados'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['interesados']['url']; ?>" title="<?php print $links_inmobiliaria['interesados']['title']; ?>"><?php print $links_inmobiliaria['interesados']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <?php if(isset($links_inmobiliaria['mis-datos'])) { ?>
        <li class="more mis-datos <?php print ($links_inmobiliaria['mis-datos']['activo'] == 1 || $template_files[1] == 'page-lista-colaboradores')?'active':''; ?>">
          <a href="<?php print $links_inmobiliaria['mis-datos']['url']; ?>" title="<?php print $links_inmobiliaria['mis-datos']['title']; ?>"><?php print $links_inmobiliaria['mis-datos']['name']; ?></a>
          <ul class="more-links mis-datos-links" style="display: none;">
            <?php if(isset($links_inmobiliaria['cuenta'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['cuenta']['url']; ?>" title="<?php print $links_inmobiliaria['cuenta']['title']; ?>"><?php print $links_inmobiliaria['cuenta']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['datos-personales'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['datos-personales']['url']; ?>" title="<?php print $links_inmobiliaria['cuenta']['title']; ?>"><?php print $links_inmobiliaria['datos-personales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['datos-fiscales'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['datos-fiscales']['url']; ?>" title="<?php print $links_inmobiliaria['datos-fiscales']['title']; ?>"><?php print $links_inmobiliaria['datos-fiscales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['colaboradores'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['colaboradores']['url']; ?>" title="<?php print $links_inmobiliaria['colaboradores']['title']; ?>"><?php print $links_inmobiliaria['colaboradores']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['datos-valoracion'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['datos-valoracion']['url']; ?>" title="<?php print $links_inmobiliaria['datos-valoracion']['title']; ?>"><?php print $links_inmobiliaria['datos-valoracion']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <!-- <li class="more afiliados <?php print (in_array($template_files[1], array('page-administrar-seguros','page-administrar-creditos', 'page-administrar-sitio')))?'active':''; ?>">
        <a href="javascript: void(0);" title="Oportunidades">Oportunidades</a>
        <ul class="more-links afiliados-links" style="display: none;">
          <?php if(isset($links_inmobiliaria['seguros'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['seguros']['url']; ?>" title="<?php print $links_inmobiliaria['seguros']['title']; ?>"><?php print $links_inmobiliaria['seguros']['name']; ?></a></li>
          <?php } ?>      
          <?php if(isset($links_inmobiliaria['creditos'])) { ?>
            <li><a href="<?php print $links_inmobiliaria['creditos']['url']; ?>" title="<?php print $links_inmobiliaria['creditos']['title']; ?>"><?php print $links_inmobiliaria['creditos']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li> -->
      <?php if(isset($links_inmobiliaria['reservas'])) { ?>
        <li class="more reservas <?php print ($links_inmobiliaria['reservas']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_inmobiliaria['reservas']['url']; ?>" title="<?php print $links_inmobiliaria['reservas']['title']; ?>"><?php print $links_inmobiliaria['reservas']['name']; ?></a>
          <ul class="more-links reservas-links" style="display: none;">
            <?php if(isset($links_inmobiliaria['reservas-realizadas'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['reservas-realizadas']['url']; ?>" title="<?php print $links_inmobiliaria['reservas-realizadas']['title']; ?>"><?php print $links_inmobiliaria['reservas-realizadas']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_inmobiliaria['reservas-recibidas'])) { ?>
              <li><a href="<?php print $links_inmobiliaria['reservas-recibidas']['url']; ?>" title="<?php print $links_inmobiliaria['reservas-recibidas']['title']; ?>"><?php print $links_inmobiliaria['reservas-recibidas']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
    </ul>
  </div>
  <!-- Fin Menu Inmobiliarias -->

<?php } elseif(ventas_es_vendedor($user->uid)) { ?>
  
  <!-- Menu Tiendas -->
  <div class="menuSolapas clearfix">
    <ul class="accesos">
      <?php if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){ ?>
        <li class="<?php print ($template_files[0]=='page-administrar' && $template_files[1] == 'page-administrar-principal')?'active':''; ?>"><a href="/administrar/principal" title=" Resumen ">Resumen</a></li>
      <?php } ?>
      <li class="<?php print (($template_files[0]=='page-administrar' && !in_array($template_files[1], array('page-administrar-agenda', 'page-administrar-seguros', 'page-administrar-sitio', 'page-administrar-creditos', 'page-administrar-ventas', 'page-administrar-principal', 'page-administrar-reservas', 'page-administrar-destacados', 'page-administrar-historial'))) || (isset($node) && sitio_local_es_nodo_aviso($node->type) && isset($template_files[2]) && in_array($template_files[2], array('page-node-edit', 'page-node-clone', 'page-node-eliminar', 'page-node-despublicar'))))?'active':''; ?>">
        <a href="/administrar/mis-avisos.html" title=" Mis avisos ">Mis avisos</a>
      </li>
      <li class="<?php print ((isset($template_files[1]) && $template_files[1]=='page-node-add') || (isset($template_files[2]) && $template_files[2]=='page-node-publicacion'))?'active':''; ?>">
        <?php if(arg(2) == "publicacion") { ?>
          <a title=" Publicación del aviso " href="/node/<?php print arg(1); ?>/publicacion">Publicá</a>
        <?php } else { ?>
          <!-- <a title="Crear Aviso" href="?width=640&height=190&inline=true&title= #modal-publicacion-rubros" class="colorbox-inline">Publicá</a> -->
          <a title="Crear Aviso" rel="nofollow"  href="javascript:void(0);" class="publicar-btn">Publicá</a>
        <?php } ?>
      </li>
<?php if(isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') { ?>
      <li class="<?php print (isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') ? 'active' : ''; ?>"><a title="Pagar por DineroMail" href="/dineromail/<?php print arg(1); ?>/confirmar">DineroMail</a></li>
<?php } ?>
      <li class="<?php print ($template_files[0]=='page-contactos_recibidos' || $template_files[3]=='page-administrar-ventas-comprador-consultas')?'active':''; ?>"><a href="/contactos_recibidos" title=" Contactos ">Contactos</a></li>
      <li class="more cuenta <?php print (in_array($template_files[0], array('page-estado_cuenta', 'page-compra-paquetes', 'page-compra-papel', 'page-abono-mensual')) || ($template_files[2] == 'page-administrar-ventas-pedidos' && $template_files[3] == 'page-admin'))? 'active' : ''; ?>">
        <a href="/abono-mensual" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
        <ul class="more-links cuenta-links" style="display: none;">
          <?php if(isset($links_tienda['abono'])) { ?>
            <li><a href="<?php print $links_tienda['abono']['url']; ?>" title="<?php print $links_tienda['abono']['title']; ?>"><?php print $links_tienda['abono']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_tienda['compras'])) { ?>
            <li><a href="<?php print $links_tienda['compras']['url']; ?>" title="<?php print $links_tienda['compras']['title']; ?>"><?php print $links_tienda['compras']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_tienda['paquetes'])) { ?>
            <li><a href="<?php print $links_tienda['paquetes']['url']; ?>" title="<?php print $links_tienda['paquetes']['title']; ?>"><?php print $links_tienda['paquetes']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_tienda['ventas'])) { ?>
            <li><a href="<?php print $links_tienda['ventas']['url']; ?>" title="<?php print $links_tienda['ventas']['title']; ?>"><?php print $links_tienda['ventas']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php if(isset($links_tienda['mis-datos'])) { ?>
        <li class="more mis-datos <?php print ($links_tienda['mis-datos']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_tienda['mis-datos']['url']; ?>" title="<?php print $links_tienda['mis-datos']['title']; ?>"><?php print $links_tienda['mis-datos']['name']; ?></a>
          <ul class="more-links mis-datos-links" style="display: none;">
            <?php if(isset($links_tienda['cuenta'])) { ?>
              <li><a href="<?php print $links_tienda['cuenta']['url']; ?>" title="<?php print $links_tienda['cuenta']['title']; ?>"><?php print $links_tienda['cuenta']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_tienda['datos-personales'])) { ?>
              <li><a href="<?php print $links_tienda['datos-personales']['url']; ?>" title="<?php print $links_tienda['cuenta']['title']; ?>"><?php print $links_tienda['datos-personales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_tienda['datos-fiscales'])) { ?>
              <li><a href="<?php print $links_tienda['datos-fiscales']['url']; ?>" title="<?php print $links_tienda['datos-fiscales']['title']; ?>"><?php print $links_tienda['datos-fiscales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_tienda['datos-valoracion'])) { ?>
              <li><a href="<?php print $links_tienda['datos-valoracion']['url']; ?>" title="<?php print $links_tienda['datos-valoracion']['title']; ?>"><?php print $links_tienda['datos-valoracion']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <li class="more tienda <?php print (in_array($template_files[2], array('page-administrar-ventas-zonas', 'page-administrar-ventas-lugares', 'page-administrar-destacados-home')))? 'active' : ''; ?>">
        <a href="javascript: void(0);" title="Tienda Virtual">Tienda Virtual</a>
        <ul class="more-links tienda-links" style="display: none;">
          <?php if(isset($links_tienda['zonas'])) { ?>
            <li><a href="<?php print $links_tienda['zonas']['url']; ?>" title="<?php print $links_tienda['zonas']['title']; ?>"><?php print $links_tienda['zonas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_tienda['lugares'])) { ?>
            <li><a href="<?php print $links_tienda['lugares']['url']; ?>" title="<?php print $links_tienda['lugares']['title']; ?>"><?php print $links_tienda['lugares']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_tienda['destacados'])) { ?>
            <li><a href="<?php print $links_tienda['destacados']['url']; ?>" title="<?php print $links_tienda['lugares']['title']; ?>"><?php print $links_tienda['destacados']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php if(isset($links_tienda['reservas'])) { ?>
        <li class="more reservas <?php print ($links_tienda['reservas']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_tienda['reservas']['url']; ?>" title="<?php print $links_tienda['reservas']['title']; ?>"><?php print $links_tienda['reservas']['name']; ?></a>
          <ul class="more-links reservas-links" style="display: none;">
            <?php if(isset($links_tienda['reservas-realizadas'])) { ?>
              <li><a href="<?php print $links_tienda['reservas-realizadas']['url']; ?>" title="<?php print $links_tienda['reservas-realizadas']['title']; ?>"><?php print $links_tienda['reservas-realizadas']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_tienda['reservas-recibidas'])) { ?>
              <li><a href="<?php print $links_tienda['reservas-recibidas']['url']; ?>" title="<?php print $links_tienda['reservas-recibidas']['title']; ?>"><?php print $links_tienda['reservas-recibidas']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
    </ul>
  </div>
  <!-- Fin Menu Tiendas -->

<?php } elseif(array_key_exists(AUTOSLAVOZ_ROL_SOPORTE_FOTOGRAFICO, $user->roles)) { ?>
  
  <!-- Menu Soporte Fotografico -->
  <div class="menuSolapas clearfix">
    <ul class="accesos">
      <li class="<?php if(arg(1) == 'usuarios_soporte_fotografico') print 'active'; ?>">
        <a href="/administrar/usuarios_soporte_fotografico" title=" Selección de Usuario ">Selección de Usuario</a>
      </li>
      <li class="<?php if(arg(1) == 'avisos_soporte_fotografico') print 'active'; ?>">
        <a href="/administrar/avisos_soporte_fotografico/<?php print arg(2); ?>" title=" Lista de avisos ">Lista de avisos</a>
      </li>
    </ul>
  </div>
  <!-- Fin Menu general -->
  
<?php } else { ?>

  <!-- Menu general -->
  <div class="menuSolapas clearfix">
    <ul class="accesos">
      <li class="<?php print (($template_files[0]=='page-administrar' && !in_array($template_files[1], array('page-administrar-agenda', 'page-administrar-seguros', 'page-administrar-sitio', 'page-administrar-creditos', 'page-administrar-ventas', 'page-administrar-principal', 'page-administrar-reservas', 'page-administrar-historial'))) || (isset($node) && sitio_local_es_nodo_aviso($node->type) && isset($template_files[2]) && in_array($template_files[2], array('page-node-edit', 'page-node-clone', 'page-node-eliminar', 'page-node-despublicar'))))?'active':''; ?>">
        <a href="/administrar/mis-avisos.html" title=" Mis avisos ">Mis avisos</a>
      </li>
      <li class="<?php print ((isset($template_files[1]) && $template_files[1]=='page-node-add') || (isset($template_files[2]) && $template_files[2]=='page-node-publicacion'))?'active':''; ?>">
        <?php if(arg(2) == "publicacion") { ?>
          <a title=" Publicación del aviso " href="/node/<?php print arg(1); ?>/publicacion">Publicá</a>
        <?php } else { ?>
          <!-- <a title="Crear Aviso" href="?width=640&height=190&inline=true&title= #modal-publicacion-rubros" class="colorbox-inline">Publicá</a> -->
          <a title="Crear Aviso" rel="nofollow"  href="javascript:void(0);" class="publicar-btn">Publicá</a>
        <?php } ?>
      </li>
<?php if(isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') { ?>
      <li class="<?php print (isset($template_files[2]) && $template_files[2]=='page-dineromail-confirmar') ? 'active' : ''; ?>"><a title="Pagar por DineroMail" href="/dineromail/<?php print arg(1); ?>/confirmar">DineroMail</a></li>
<?php } ?>
      <li class="more contactos <?php print ($template_files[0]=='page-contactos_recibidos' || $template_files[3]=='page-administrar-ventas-comprador-consultas')?'active':''; ?>">
        <a href="/contactos_recibidos" title=" Contactos ">Contactos</a>
        <ul class="more-links contactos-links" style="display: none;">
          <?php if(isset($links_general['consultas'])) { ?>
            <li><a href="<?php print $links_general['consultas']['url']; ?>" title="<?php print $links_general['consultas']['title']; ?>"><?php print $links_general['consultas']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_general['consultas-realizadas'])) { ?>
            <li><a href="<?php print $links_general['consultas-realizadas']['url']; ?>" title="<?php print $links_general['consultas-realizadas']['title']; ?>"><?php print $links_general['consultas-realizadas']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <li class="more cuenta <?php print (in_array($template_files[0], array('page-estado_cuenta', 'page-compra-paquetes', 'page-compra-papel', 'page-abono-mensual')) || ($template_files[2] == 'page-administrar-ventas-comprador' && $template_files[3] == 'page-admin'))? 'active' : ''; ?>">
        <a href="/abono-mensual" title="Estado de cuenta, compras realizadas">Estado de cuenta</a>
        <ul class="more-links cuenta-links" style="display: none;">
          <?php if(isset($links_general['abono'])) { ?>
            <li><a href="<?php print $links_general['abono']['url']; ?>" title="<?php print $links_general['abono']['title']; ?>"><?php print $links_general['abono']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_general['paquetes'])) { ?>
            <li><a href="<?php print $links_general['paquetes']['url']; ?>" title="<?php print $links_general['paquetes']['title']; ?>"><?php print $links_general['paquetes']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_general['compras'])) { ?>
            <li><a href="<?php print $links_general['compras']['url']; ?>" title="<?php print $links_general['compras']['title']; ?>"><?php print $links_general['compras']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_general['compras-productos'])) { ?>
            <li><a href="<?php print $links_general['compras-productos']['url']; ?>" title="<?php print $links_general['compras-productos']['title']; ?>"><?php print $links_general['compras-productos']['name']; ?></a></li>
          <?php } ?>
          <?php if(isset($links_general['papel'])) { ?>
            <li><a href="<?php print $links_general['papel']['url']; ?>" title="<?php print $links_general['papel']['title']; ?>"><?php print $links_general['papel']['name']; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php if(isset($links_general['mis-datos'])) { ?>
        <li class="more mis-datos <?php print ($links_general['mis-datos']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_general['mis-datos']['url']; ?>" title="<?php print $links_general['mis-datos']['title']; ?>"><?php print $links_general['mis-datos']['name']; ?></a>
          <ul class="more-links mis-datos-links" style="display: none;">
            <?php if(isset($links_general['cuenta'])) { ?>
              <li><a href="<?php print $links_general['cuenta']['url']; ?>" title="<?php print $links_general['cuenta']['title']; ?>"><?php print $links_general['cuenta']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_general['datos-personales'])) { ?>
              <li><a href="<?php print $links_general['datos-personales']['url']; ?>" title="<?php print $links_general['cuenta']['title']; ?>"><?php print $links_general['datos-personales']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_general['datos-valoracion'])) { ?>
              <li><a href="<?php print $links_general['datos-valoracion']['url']; ?>" title="<?php print $links_general['datos-valoracion']['title']; ?>"><?php print $links_general['datos-valoracion']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <?php if(isset($links_general['reservas'])) { ?>
        <li class="more reservas <?php print ($links_general['reservas']['activo'] == 1)?'active':''; ?>">
          <a href="<?php print $links_general['reservas']['url']; ?>" title="<?php print $links_general['reservas']['title']; ?>"><?php print $links_general['reservas']['name']; ?></a>
          <ul class="more-links reservas-links" style="display: none;">
            <?php if(isset($links_general['reservas-realizadas'])) { ?>
              <li><a href="<?php print $links_general['reservas-realizadas']['url']; ?>" title="<?php print $links_general['reservas-realizadas']['title']; ?>"><?php print $links_general['reservas-realizadas']['name']; ?></a></li>
            <?php } ?>
            <?php if(isset($links_general['reservas-recibidas'])) { ?>
              <li><a href="<?php print $links_general['reservas-recibidas']['url']; ?>" title="<?php print $links_general['reservas-recibidas']['title']; ?>"><?php print $links_general['reservas-recibidas']['name']; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
    </ul>
  </div>
  <!-- Fin Menu general -->
  
  <?php } ?>

<?php } ?>

<!-- Eliminar cookies viejas -->
<?php 
if (isset($_COOKIE['infoAdminInteresados'])) {
  unset($_COOKIE['infoAdminInteresados']);
  setcookie('infoAdminInteresados', null, -1, '/');
}
?>

<!-- Mensaje ayuda -->
<?php /*if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && in_array($perfil->profile_tipo_concesionaria, array('Concesionaria'))){ ?>
  <?php if($template_files[1] == 'page-administrar-principal'){ ?>
    <?php if(!isset($_COOKIE['infoAdminEstadisticasAutosCuenta'])){ ?>
      <div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
      <div id="popover" class="popover bottom EstadisticasResumen" style="top: 109px; left: 369px;">
        <div class="arrow"></div>
        <h3 class="popover-title">Nuevas Estadísticas Globales</h3>
        <div class="popover-content">Ahora podés armar tus propios informes con los reportes que te ofrece el sitio.</div>
      </div>
      <?php   setcookie('infoAdminEstadisticasAutosCuenta', 1, time() + 86400, "/"); //Caduca en 1 dias ?>
    <?php } ?>
  <?php } elseif($template_files[1] != 'page-administrar-estadisticas_globales_autos') { ?>
    <?php if(!isset($_COOKIE['infoAdminEstadisticasAutos'])){ ?>
      <div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
      <div id="popover" class="popover bottom Estadisticas" style="top: 50px; left: 132px;">
        <div class="arrow"></div>
        <h3 class="popover-title">Nuevas Estadísticas Globales</h3>
        <div class="popover-content">Ahora podés armar tus propios informes con los reportes que te ofrece el sitio.</div>
      </div>
      <?php   setcookie('infoAdminEstadisticasAutos', 1, time() + 86400, "/"); //Caduca en 1 dias ?>
    <?php } ?>
  <?php } ?>
<?php }*/ ?>

<!-- Mensaje ayuda -->
<?php /* if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){ ?>
  <?php if(!isset($_COOKIE['infoAdminInteresados'])){ ?>
    <div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
    <div id="popover" class="popover bottom Agenda" style="top: 14px; left: 324px;">
      <div class="arrow"></div>
      <h3 class="popover-title">¡Nuevas funcionalidades!</h3>
      <div class="popover-content"><strong>Registro de Interesados:</strong><br>Ahora podrás ver un reporte de usuarios que se interesaron en tu anuncio.<br>Son usuarios que entraron a tu anuncio pero no generaron una interacción. <br><br>
      <strong>Alerta de nuevas acciones sobre el anuncio:</strong><br>En la pestaña de contactos vas a ver una notificación que te indica la cantidad de acciones sobre tu aviso que aún no has revisado. </div>
      <!-- <div class="popover-navigation txtRight"><button class="button" data-role="next">Aceptar</button></div> -->
    </div>
    <?php setcookie('infoAdminInteresados', 1, time() + 86400, "/"); //Caduca en 1 dias ?>
  <?php } ?>
<?php } */ ?>