<?php 
global $user;
$adslot = array();

$arg0 = arg(0);
$arg1 = arg(1);
$arg2 = arg(2);
$es_prehome = FALSE;
$es_resultado = FALSE;
$es_ficha = FALSE;
$es_admin = FALSE;
$es_nota = FALSE;
$target = array();

if($arg0 == 'node' && !empty($arg1)) {
  $nodo = node_load($arg1);
  if(isset($nodo->type) && $nodo->type == 'page'){
    if(in_array($nodo->title, array('Home Autos','Home Inmuebles'))) {
      $es_prehome = TRUE;
      switch($nodo->title){
        case 'Home Autos':
          $target[] = array('key' => 'clasificados_tipo', 'value' => 'vehiculos');
          break;
        case 'Home Inmuebles':
          $target[] = array('key' => 'clasificados_tipo', 'value' => 'inmuebles');
          break;
      }
    }
  }
  if(isset($nodo->type) && $nodo->type == 'nota') {
    $es_nota = TRUE;
    $target[] = array('key' => 'clasificados_tipo', 'value' => 'nota');
  }
  if(isset($nodo->type) && strpos($nodo->type, 'aviso_') !== FALSE){
    $es_ficha = TRUE;
    $tipos = array();
    if(in_array($nodo->type, array('aviso_4x4', 'aviso_auto', 'aviso_repuesto', 'aviso_moto', 'aviso_plan_ahorro', 'aviso_utilitario'))) {      
      $tipos[] = 'vehiculos';
      $marca = _autoslavoz_node_obtener_marca_term($nodo);
      if(!empty($marca))
        $target[] = array('key' => 'clasificados_marca', 'value' => strtolower(str_replace(' ', '', $marca->name)));
    }
    if(in_array($nodo->type, array('aviso_campo', 'aviso_casa', 'aviso_cochera', 'aviso_departamento', 'aviso_emprendimiento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina', 'aviso_terreno_lote', 'aviso_rural'))) {      
      $tipos[] = 'inmuebles';
      $operacion = clasificados_obtener_operacion($nodo->field_aviso_operacion[0]['value']);
      if(!empty($operacion))
        $target[] = array('key' => 'clasificados_oper', 'value' => strtolower(str_replace(' ', '', $operacion)));
    }
    if(in_array($nodo->type, array('aviso_producto', 'aviso_servicio', 'aviso_rural'))) {      
      $rubros = clasificados_taxonomy_get_node_terms_for_vocabulary($nodo, CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID);
      if($rubros === FALSE)
        $rubros = array();
      $rubro = '';
      $depth = 0;
      foreach ($rubros as $term) {
        if($depth == 1) {
          $rubro = $term->name;
          break;
        }
        $depth++;
      }
      $rubro = clasificados_eliminar_caracteres_especiales(strtolower(str_replace(' ', '', $rubro)));
      $target[] = array('key' => 'clasificados_rubro', 'value' => $rubro);
    }
    switch($nodo->type){   
      case 'aviso_4x4':
        $tipos[] = '4x4';
        break;
      case 'aviso_auto':
        $tipos[] = 'autos';
        break;
      case 'aviso_repuesto':
        $tipos[] = 'repuestos';
        break;
      case 'aviso_moto':
        $tipos[] = 'motos';
        break;
      case 'aviso_plan_ahorro':
        $tipos[] = 'planesahorro';
        break;
      case 'aviso_utilitario':
        $tipos[] = 'utilitarios';
        break;
      case 'aviso_campo':
        $tipos[] = 'campos';
        break;  
      case 'aviso_casa':
        $tipos[] = 'casas';
        break;  
      case 'aviso_cochera':
        $tipos[] = 'cocheras';
        break;  
      case 'aviso_departamento':
        $tipos[] = 'departamentos';
        break;  
      case 'aviso_emprendimiento':
        $tipos[] = 'emprendimientos';
        break;  
      case 'aviso_galpon_deposito':
        $tipos[] = 'depositos';
        break;  
      case 'aviso_local':
        $tipos[] = 'locales';
        break;  
      case 'aviso_oficina':
        $tipos[] = 'oficinas';
        break;
      case 'aviso_rural':
        $tipos[] = 'rurales';
        break; 
      case 'aviso_terreno_lote':
        $tipos[] = 'terrenos';
        break;
      case 'aviso_alquiler_temporario':
        $tipos[] = 'alojamientos';
        break;
      case 'aviso_producto':
        $tipos[] = 'productos';
        break;
      case 'aviso_servicio':
        $tipos[] = 'servicios';
        break;
    }
    $tipos_text = '';
    foreach($tipos as $tipo) {
      if($tipos_text == '')
        $tipos_text = $tipo;
      else
        $tipos_text .= "', '".$tipo;
    }
    if($tipos_text != '')
      $target[] = array('key' => 'clasificados_tipo', 'value' => $tipos_text);
  }
}

if($arg0 == 'search' || ($arg0 == 'sitio' && !in_array($arg2, array('quienes-somos', 'contacto')) )) {
  $es_resultado = TRUE;
  $env_id = apachesolr_default_environment();
  $query = apachesolr_current_query($env_id);
  
  $query_filtros = array();
  if(!empty($query))
    $query_filtros = $query->getFilters();
  // Detecatmos que no sea micrositio
  $micrositio = 0;
  foreach($query_filtros as $filtro) {
    if($filtro['#name'] == 'is_uid') $micrositio = 1;
  }
  
  $query_param = array();
  if(!empty($query))
    $query_param = $query->getParams();
  $busqueda = '';
  if(isset($query_param['q']))
    $busqueda = strtolower($query_param['q']);
  
  if($micrositio) {
    $es_resultado = FALSE;
  } else {
    $path = clasificados_cxense_path_category_search($query_filtros);
    $rubros = explode('/', $path);
    if(isset($rubros[1]) && $rubros[1] == 'vehiculos') {
      $tipos[] = 'vehiculos';
      if(isset($rubros[2])) {
        switch($rubros[2]){   
          case '4x4 - 4x2 y todo terreno':
            $tipos[] = '4x4';
            break;
          case 'autos':
            $tipos[] = 'autos';
            break;
          case 'accesorios y repuestos':
            $tipos[] = 'repuestos';
            break;
          case 'motos - cuadriciclos y náutica':
            $tipos[] = 'motos';
            break;
          case 'planes de ahorro':
            $tipos[] = 'planesahorro';
            break;
          case 'utilitarios - pick-up y camiones':
            $tipos[] = 'utilitarios';
            break;
        }
      }
      $marca_busqueda = 0;
      if($busqueda != '') {
        $target_data = clasificados_banners_get_target_key($busqueda);
        if(!empty($target_data)) { 
          $target[] = $target_data;
          $marca_busqueda = 1;
        }
      }
      if(isset($rubros[3]) && !$marca_busqueda) {
        $target[] = array('key' => 'clasificados_marca', 'value' => strtolower(str_replace(' ', '', $rubros[3])));
      }
    }
    if(isset($rubros[1]) && $rubros[1] == 'inmuebles') {
      $tipos[] = 'inmuebles';
      if(isset($rubros[2])) {
        switch($rubros[2]){   
          case 'campos':
            $tipos[] = 'campos';
            break;  
          case 'casas':
            $tipos[] = 'casas';
            break;  
          case 'cocheras':
            $tipos[] = 'cocheras';
            break;  
          case 'departamentos':
            $tipos[] = 'departamentos';
            break;  
          case 'emprendimientos':
            $tipos[] = 'emprendimientos';
            break;  
          case 'galpones y depósitos':
            $tipos[] = 'depositos';
            break;  
          case 'locales':
            $tipos[] = 'locales';
            break;  
          case 'oficinas':
            $tipos[] = 'oficinas';
            break; 
          case 'terrenos y lotes':
            $tipos[] = 'terrenos';
            break;
        }
      }
      if(isset($rubros[3])) {
        $target[] = array('key' => 'clasificados_oper', 'value' => strtolower(str_replace(' ', '', $rubros[3])));
      }
    }
    if(isset($rubros[1]) && in_array($rubros[1], array('productos', 'servicios', 'rurales'))) {      
      $tipos[] = $rubros[1];
      if(isset($rubros[2])) {
        $target[] = array('key' => 'clasificados_rubro', 'value' => strtolower(str_replace(' ', '', $rubros[2])));
      }
    }
    if(isset($rubros[1]) && $rubros[1] == 'alojamientos') {      
      $tipos[] = $rubros[1];
    }
    $tipos_text = '';
    foreach($tipos as $tipo) {
      if($tipos_text == '')
        $tipos_text = $tipo;
      else
        $tipos_text .= "', '".$tipo;
    }
    if($tipos_text != '')
      $target[] = array('key' => 'clasificados_tipo', 'value' => $tipos_text);
  }
}

if($arg0 == 'productos') {
  $es_prehome = TRUE;
  $target[] = array('key' => 'clasificados_tipo', 'value' => 'productos');
}
if($arg0 == 'alojamientos') {
  $es_prehome = TRUE;
  $target[] = array('key' => 'clasificados_tipo', 'value' => 'alojamientos');
}

if($arg0 == 'administrar') {
  $es_admin = TRUE;
  $target[] = array('key' => 'clasificados_tipo', 'value' => 'administrador');
  if(in_array('concesionaria terceros', $user->roles)){
    if(!isset($user->profile_tipo_concesionaria)) {
      profile_load_profile($user);
    }
    $tipo_usuario = $user->profile_tipo_concesionaria;
    if($tipo_usuario=='Concesionaria'){
      $target[] = array('key' => 'clasificados_rubro', 'value' => 'concesionaria');
    } elseif($tipo_usuario=='Inmobiliaria') {
      $target[] = array('key' => 'clasificados_rubro', 'value' => 'inmobiliaria');
    } 
  } else {
    $target[] = array('key' => 'clasificados_rubro', 'value' => 'particular');  
  }
}

if(drupal_is_front_page()) {  
  $adslot[] = array('adunit'  => 'dfp-anuncio_previo',
                    'adblock' => '/6955764/clasificados/home/anuncio_previo',
                    'adsizes' => '[1000, 680]');
  $adslot[] = array('adunit'  => 'dfp-middle_1',
                    'adblock' => '/6955764/clasificados/home/middle_1',
                    'adsizes' => '[651, 100]');
  $adslot[] = array('adunit'  => 'dfp-middle_2',
                    'adblock' => '/6955764/clasificados/home/middle_2',
                    'adsizes' => '[651, 100]');
  $adslot[] = array('adunit'  => 'dfp-middle_3',
                    'adblock' => '/6955764/clasificados/home/middle_3',
                    'adsizes' => '[651, 100]');
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/home/topsite_expandible',
                    'adsizes' => '[970, 80]');
  $adslot[] = array('adunit'  => 'dfp-lateral_1',
                    'adblock' => '/6955764/clasificados/home/lateral_1',
                    'adsizes' => '[[300, 600],[120, 600]]');
  $adslot[] = array('adunit'  => 'dfp-zocalo',
                    'adblock' => '/6955764/clasificados/home/zocalo',
                    'adsizes' => '[970, 80]');
  $adslot[] = array('adunit'  => 'dfp-oportunidad1',
                    'adblock' => '/6955764/clasificados/home/oportunidad1',
                    'adsizes' => '[100, 100]');
  $adslot[] = array('adunit'  => 'dfp-oportunidad2',
                    'adblock' => '/6955764/clasificados/home/oportunidad2',
                    'adsizes' => '[100, 100]');
  $adslot[] = array('adunit'  => 'dfp-oportunidad3',
                    'adblock' => '/6955764/clasificados/home/oportunidad3',
                    'adsizes' => '[100, 100]');
  $adslot[] = array('adunit'  => 'dfp-oportunidad4',
                    'adblock' => '/6955764/clasificados/home/oportunidad4',
                    'adsizes' => '[100, 100]');                    
  $adslot[] = array('adunit'  => 'dfp-oportunidad5',
                    'adblock' => '/6955764/clasificados/home/oportunidad5',
                    'adsizes' => '[100, 100]');                    
  $adslot[] = array('adunit'  => 'dfp-oportunidad6',
                    'adblock' => '/6955764/clasificados/home/oportunidad6',
                    'adsizes' => '[100, 100]');                    
} elseif($es_prehome) {
  $adslot[] = array('adunit'  => 'dfp-anuncio_previo',
                    'adblock' => '/6955764/clasificados/portadas/anuncio_previo',
                    'adsizes' => '[1000, 680]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_1',
                    'adblock' => '/6955764/clasificados/portadas/middle_1',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_2',
                    'adblock' => '/6955764/clasificados/portadas/middle_2',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_3',
                    'adblock' => '/6955764/clasificados/portadas/middle_3',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-rectangle_1',
                    'adblock' => '/6955764/clasificados/portadas/rectangle_1',
                    'adsizes' => '[300, 250]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/portadas/topsite_expandible',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-lateral_1',
                    'adblock' => '/6955764/clasificados/portadas/lateral_1',
                    'adsizes' => '[[300, 600],[120, 600]]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-zocalo',
                    'adblock' => '/6955764/clasificados/portadas/zocalo',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
} elseif($es_resultado) {
  $adslot[] = array('adunit'  => 'dfp-anuncio_previo',
                    'adblock' => '/6955764/clasificados/resultados/anuncio_previo',
                    'adsizes' => '[1000, 680]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_1',
                    'adblock' => '/6955764/clasificados/resultados/middle_1',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_2',
                    'adblock' => '/6955764/clasificados/resultados/middle_2',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-middle_3',
                    'adblock' => '/6955764/clasificados/resultados/middle_3',
                    'adsizes' => '[651, 100]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-rectangle_1',
                    'adblock' => '/6955764/clasificados/resultados/rectangle_1',
                    'adsizes' => '[[120, 600], [300, 250]]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/resultados/topsite_expandible',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-lateral_1',
                    'adblock' => '/6955764/clasificados/resultados/lateral_1',
                    'adsizes' => '[[300, 600],[120, 600]]',
                    'target'  => $target
                    );   
  $adslot[] = array('adunit'  => 'dfp-zocalo',
                    'adblock' => '/6955764/clasificados/resultados/zocalo',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
} elseif($es_ficha) {
  $adslot[] = array('adunit'  => 'dfp-anuncio_previo',
                    'adblock' => '/6955764/clasificados/ficha/anuncio_previo',
                    'adsizes' => '[1000, 680]',
                    'target'  => $target
                    );
  /*
  $adslot[] = array('adunit'  => 'dfp-rectangle',
                    'adblock' => '/6955764/clasificados/ficha/rectangle',
                    'adsizes' => '[300, 250]',
                    'target'  => $target
                    );
  */
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/ficha/topsite_expandible',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-zocalo',
                    'adblock' => '/6955764/clasificados/ficha/zocalo',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
} elseif($es_admin) {
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/portadas/topsite_expandible',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
} elseif($es_nota) {
  $adslot[] = array('adunit'  => 'dfp-topsite',
                    'adblock' => '/6955764/clasificados/portadas/topsite_expandible',
                    'adsizes' => '[970, 80]',
                    'target'  => $target
                    );
  $adslot[] = array('adunit'  => 'dfp-rectangle_1',
                    'adblock' => '/6955764/clasificados/portadas/rectangle_1',
                    'adsizes' => '[300, 250]',
                    'target'  => $target
                    );
}
?> 
<script>
    var ads = { styles: {}, light:true, loadGPT:true };
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>
<script async='async' src='https://cdn.jsdelivr.net/gh/adcase/adcase.js@2/dist/adcase.js'></script>
<script>
  googletag.cmd.push(function() {
    <?php foreach($adslot as $slot) { ?>      
      googletag.defineSlot('<?php print $slot['adblock']; ?>', <?php print $slot['adsizes']; ?>, '<?php print $slot['adunit']; ?>')
      <?php if (isset($slot['target']) && !empty($slot['target'])){ ?>
        <?php foreach($slot['target'] as $target_slot) { ?>
        .setTargeting('<?php print $target_slot['key']; ?>', ['<?php print $target_slot['value']; ?>'])
        <?php } ?>
      <?php } ?>
      .addService(googletag.pubads())
    <?php } ?>
    googletag.pubads().setTargeting('adcase', ads.logData);
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true, true);
    googletag.pubads().setCentering(true);
    googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        ads.slotRendered(event);
    });
    googletag.enableServices();
  });
</script>