<?php
global $user;
$secion = arg(2);
?>
<div class="tabs">
  <ul class="tabs primary clearfix">
<?php if($user->profile_tipo_concesionaria == 'Inmobiliaria'){ ?>    
    <li <?php if($secion == 'propietarios' || $secion == 'propietario') print 'class="active"';?>><a href="/administrar/agenda/propietarios" <?php if($secion == 'propietarios' || $secion == 'propietario') print 'class="active"';?>><span class="tab">Propietarios</span></a></li>
<?php } ?>
<?php if(!in_array('colaborador inmobiliaria', $user->roles)){ ?>
    <li <?php if($secion == 'vendedores' || $secion == 'vendedor') print 'class="active"';?>><a href="/administrar/agenda/vendedores" <?php if($secion == 'vendedores' || $secion == 'vendedor') print 'class="active"';?>><span class="tab">Vendedores</span></a></li>
<?php } ?>
<?php if($user->profile_tipo_concesionaria == 'Inmobiliaria'){ ?>    
    <li <?php if($secion == 'inversores' || $secion == 'inversor') print 'class="active"';?>><a href="/administrar/agenda/inversores" <?php if($secion == 'inversores' || $secion == 'inversor') print 'class="active"';?>><span class="tab">Inversores</span></a></li>
<?php } ?>
  </ul>
</div>