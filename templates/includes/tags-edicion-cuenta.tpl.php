<?php
global $user;
$secion = arg(3);
$destination = '';
if(isset($_GET['destination'])) $destination = '?destination='.$_GET['destination'];
?>
<div class="tabs">
  <ul class="tabs primary clearfix">
    <li <?php if($secion == '') print 'class="active"';?>><a href="/user/<?php print $user->uid; ?>/edit<?php print $destination; ?>" <?php if($secion == '') print 'class="active"';?>><span class="tab">Datos Usuario</span></a></li>
    <li <?php if($secion == 'Datos Personales') print 'class="active"';?>><a href="/user/<?php print $user->uid; ?>/edit/Datos%20Personales<?php print $destination; ?>" <?php if($secion == 'Datos Personales') print 'class="active"';?>><span class="tab">Datos Personales</span></a></li>
    <?php if(in_array("concesionaria web", $user->roles) || in_array("concesionaria terceros", $user->roles)){ ?>
      <li <?php if($secion == 'Datos Fiscales') print 'class="active"';?>><a href="/user/<?php print $user->uid; ?>/edit/Datos%20Fiscales<?php print $destination; ?>" <?php if($secion == 'Datos Fiscales') print 'class="active"';?>><span class="tab">Datos Fiscales</span></a></li>
    <?php } ?>  
  </ul>
</div>