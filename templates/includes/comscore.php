<?php
/*
 * Stript para detectar el path para digital analytix, comScore (Ex Certifica)
 * Se debe incluir este script en todos los archivos template que sean page.
 */

$comscore = 'otros';
$rubro_raiz = 'home';
if($is_front) {
  $comscore = 'index';
} else if($es_ficha) {
  $marca_term = _autoslavoz_node_obtener_marca_term($node);
  $rubro_term = clasificados_rubros_obtener_rubro_raiz_tipo_aviso($node->type);

  $rubros_terms = clasificados_taxonomy_get_node_terms_for_vocabulary($node, CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID);

  if($rubros_terms === FALSE) {
    $rubros_terms = array();
  }
  $i = 0;
  $subrubro = 'otros';
  foreach ($rubros_terms as $tid => $term) {
    if($i==0) {
      $i++;
      continue;
    }
    else {
      $subrubro = $term->name;
      break;
    }
  }
  $rubro_raiz = $rubro_term->name;
  if($marca_term !== FALSE)
    $comscore = 'ficha.'.$subrubro.'.'.$marca_term->name;
  else
    $comscore = 'ficha.'.$subrubro;
} else if(isset($node) && $node->type == 'nota') {
  $comscore = 'nota';
} else {
  switch($template_files[0]) {
    case 'page-productos':
        $rubro_raiz = 'productos';
        $comscore = 'prehome';
        break;
    case 'page-tiendas_lavoz':
        $rubro_raiz = 'ventaonline';
        $comscore = 'tiendas';
        break;
    case 'page-comprar':
        $rubro_raiz = 'ventaonline';
        $comscore = 'comprar';
        if($template_files[2]=='page-comprar-datos')
          $comscore .= '.datos';
        else if($template_files[2]=='page-comprar-entrega')
          $comscore .= '.entrega';
        else if($template_files[2]=='page-comprar-pago')
          $comscore .= '.pago';
        else if($template_files[2]=='page-comprar-ok')
          $comscore .= '.ok';
        else if($template_files[2]=='page-comprar-cancel')
          $comscore .= '.cancel';
        else if($template_files[2]=='page-comprar-pending')
          $comscore .= '.pending';
        break;
    case 'page-registro-particular':
      $comscore = 'registro.particular';
      break;
    case 'page-registro-comercio':
      $comscore = 'registro.comercio';
      break;
    case 'page-contact':
      $comscore = 'contacto';
      break;
    case 'page-guias':
    case 'page-noticias':
      $comscore = 'noticias';
      break;
    case 'page-user':
      if($template_files[1]=='page-user-password') {
        $comscore = 'olvide_contrasena';
      } else if($template_files[2]=='page-user-edit') {
        $comscore = 'micuenta.modificardatos';
      }
      break;
    case 'page-imprimir':
      $comscore = 'imprimirficha';
      break;
    case 'page-faq':
      $comscore = 'ayuda';
      break;
    case 'page-taxonomy':
      if($template_files[1]=='page-taxonomy-term') {
        $cantidad = trim(strip_tags(theme('block', (object) $block_current)));
        $cantidad_array = explode('(-)', $cantidad);
        $cantidad = (integer) $cantidad_array[0];
        $terms = taxonomy_get_parents_all(arg(2));

        $term = current($terms);
        $comscore = 'resultados&ns_search_term='.$term->name.'&ns_search_result=1';
      }
      break;
    case 'page-search':
      $env_id = apachesolr_default_environment();
      $query = apachesolr_current_query($env_id);
      if(!is_object($query)) {
        $comscore = 'resultados';
        break;
      }
      $response = apachesolr_static_response_cache($query->getSearcher());
      $cantidad = $response->response->numFound;
      if(isset($_GET['filters'])){
        $comscore = 'resultados&ns_search_term='.$_GET['filters'].'&ns_search_result='.$cantidad;
      }else{
        $comscore = 'resultados&ns_search_term='.end(explode('/',request_uri())).'&ns_search_result='.$cantidad;
      }
      $rubros = clasificados_rubros_obtener_grupos();     
      foreach($rubros as $tid => $valor) {
        if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, $tid)) {
          $rubro_raiz = $valor;
        }
      }
      break;
    default:
      if($template_files[2] == 'page-prehome-autos'){
        $rubro_raiz = 'vehiculos';
        $comscore = 'prehome';
      }
      if($template_files[2] == 'page-prehome-inmuebles'){
        $rubro_raiz = 'inmuebles';
        $comscore = 'prehome';
      }
      if(in_array('page-administrar-mis-avisos.html', $template_files)) {
        $comscore = 'micuenta.misavisos';
      } else if(in_array('page-node-add', $template_files) && (!isset($node) || sitio_local_es_nodo_aviso($node))) {
        $comscore = 'micuenta.crearnuevoanuncio';
      } else if(in_array('page-admin', $template_files)) {
        $comscore = 'micuenta';
        if($template_files[1]=='page-administrar-ventas')
          $comscore = 'micuenta.ventas';
      } else if(isset($template_files[1])) {
        if($template_files[1]=='page-node-240697')
          $comscore = 'avisolegal';
      }
  }
}
$comscore = $rubro_raiz.'.'.$comscore;
$comscore = strtolower($comscore);
$comscore = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($comscore, ENT_QUOTES, 'UTF-8'));
$comscore = preg_replace('@[^a-z0-9_./&=;]+@', '', $comscore);
?>
<div class="DN"><img title="Comscore" src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906399&amp;ns_site=clasificadoslavoz-comar&amp;name=<?php echo $comscore; ?>" height="1" width="1" alt="*">
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "6906409" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0];
    s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img title="Comscore" alt="*" src="http://b.scorecardresearch.com/p?c1=2&c2=6906409&cv=2.0&cj=1" />
</noscript>
</div>