<footer class="copyright">
  <div class="container">
    <div class="col-lg-12 text-right">
      <a href="http://www.lavoz.com.ar/lvilab/"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/logo-lavoz.png"></a><a href="http://www.mobydigital.com/es/">&nbsp;&nbsp;&nbsp;<img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/logo-mobydigital.png"></a>
    </div>
  </div>
</footer>
<div style="visibility: hidden;">
  <?php if(isset($termino_busqueda)): ?>
  <script type="text/javascript">
    (function() {
      var cx = '003201315050113052054:mcog5p7jbni';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
          '//www.google.com/cse/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    })();
  </script>
  <?php endif; ?>
  <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1407167-4', 'auto');
  <?php if(isset($frase_busqueda)): ?>
  ga('set', 'page', '/search/apachesolr_search/?s=<?php print $frase_busqueda; ?>&filtro=<?php if(isset($rubro_busqueda)) print $rubro_busqueda; ?>');
  <?php endif; ?>
  ga('send', 'pageview');
  </script>
<script type="text/javascript">
var _sf_async_config={uid:15747,domain:"clasificadoslavoz.com.ar"};
(function(){
  function loadChartbeat() {
    window._sf_endpt=(new Date()).getTime();
    var e = document.createElement('script');
    e.setAttribute('language', 'javascript');
    e.setAttribute('type', 'text/javascript');
    e.setAttribute('src',
       (("https:" == document.location.protocol) ? "https://a248.e.akamai.net/chartbeat.download.akamai.com/102508/" : "http://static.chartbeat.com/") +
       "js/chartbeat.js");
    document.body.appendChild(e);
  }
  var oldonload = window.onload;
  window.onload = (typeof window.onload != 'function') ?
     loadChartbeat : function() { oldonload(); loadChartbeat(); };
})();
</script>
<!-- Cxense script begin -->
<!-- <div id="cX-root" style="display:none"></div>
<script type="text/javascript">
var cX = cX || {}; cX.callQueue = cX.callQueue || [];
cX.callQueue.push(['setSiteId', '9222331896993589875']);
var user_id = get_cookie('login');
if (user_id!=false) {
  cX.callQueue.push(['setCustomParameters', { 'registered': 'true' }]);
  cX.callQueue.push(['addExternalId', { 'id': 'cl-'+user_id, 'type': 'gcl'}]);
}
cX.callQueue.push(['sendPageViewEvent', { useAutoRefreshCheck: false }]);
</script>
<script type="text/javascript">
(function() { try { var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript'; scriptEl.async = 'async';
scriptEl.src = ('https:' == document.location.protocol) ? 'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
var targetEl = document.getElementsByTagName('script')[0]; targetEl.parentNode.insertBefore(scriptEl, targetEl); } catch (e) {};} ());
</script> -->
<!-- Cxense script end -->
<!-- FB -->
<?php include dirname(__FILE__).'/script-facebook.tpl.php'; ?>
<!-- FB -->
</div>

