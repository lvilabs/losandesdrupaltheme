<?php
  global $user;
  $datos_config = administracion_sitios_get_all($user->uid);
  
  //Seccion
  $array_seccion = explode('?sec=', $_SERVER['REQUEST_URI']); 
  $seccion = '';
  if(isset($array_seccion[1]))
    $seccion = $array_seccion[1];

  $plantillas_act = 0;
  $fuentes_act = 0;
  $fuentes_des = 1;
  $multimedia_act = 0;
  $multimedia_des = 1;
  $contactos_act = 0;
  $contactos_des = 1;
  $alta_act = 0;
  $alta_des = 1;
  if(!$datos_config['wizard_completo']){
    $plantillas_act = 1;
    if(!empty($datos_config['template'])){
      $fuentes_act = 1;
      $fuentes_des = 0;
      if(!empty($datos_config['paleta_color']) || !empty($datos_config['fuente_titulos']) || !empty($datos_config['fuente_parrafos'])){
        $multimedia_act = 1;
        $multimedia_des = 0;
        if(!empty($datos_config['imagen_principal']) || !empty($datos_config['enlaces_externos']) || !empty($datos_config['analitycs'])){
          $contactos_act = 1;
          $contactos_des = 0;
        }
      }
    }
  } else {
    $alta_des = 0;
    $fuentes_des = 0;
    $multimedia_des = 0;
    $contactos_des = 0;
    if($seccion == '' || $seccion == 'plantillas')
      $plantillas_act = 1;
    if($seccion == 'fuentes')
      $fuentes_act = 1;
    if($seccion == 'multimedia')
      $multimedia_act = 1;
    if($seccion == 'contactos')
      $contactos_act = 1;
  }
?>
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="container collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div class="container">
      <ul class="nav navbar-nav">
        <li id="menu-plantillas" class="plantillas <?php if($plantillas_act) print 'Act'; ?>">
          <a href="/administrar/sitio/editor?sec=plantillas" title="Plantillas"><span><i class="fas fa-file-code"></i></span>Plantillas</a>
        </li>
        <li id="menu-fuentes-colores" class="colores <?php if($fuentes_act) print 'Act'; ?>">
          <?php if($fuentes_des) { ?>
            <div title="Colores"><span><i class="fas fa-eye-dropper"></i></span>Colores</div>
          <?php } else { ?>
            <a href="/administrar/sitio/editor?sec=fuentes" title="Colores"><span><i class="fas fa-eye-dropper"></i></span>Colores</a>
          <?php } ?>
        </li>
        <li id="menu-fuentes-tipografia" class="tipografias <?php if($fuentes_act) print 'Act'; ?>">
          <?php if($fuentes_des) { ?>
            <div title="Tipografías"><span><i class="fa fa-font"></i></span>Tipografías</div>
          <?php } else { ?>
            <a href="/administrar/sitio/editor?sec=fuentes" title="Tipografías"><span><i class="fa fa-font"></i></span>Tipografías</a>
          <?php } ?>
        </li>
        <li id="menu-multimedia-imagenes" class="imagenes <?php if($multimedia_act) print 'Act'; ?>">
          <?php if($multimedia_des) { ?>
            <div title="Imágenes"><span><i class="fas fa-image"></i></span>Imágenes</div>
          <?php } else { ?>
            <a href="/administrar/sitio/editor?sec=multimedia" title="Imágenes"><span><i class="fas fa-image"></i></span>Imágenes</a>
          <?php } ?>
        </li>
        <li id="menu-multimedia-links" class="links <?php if($multimedia_act) print 'Act'; ?>">
          <?php if($multimedia_des) { ?>
            <div title="Links externos"><span><i class="fa fa-link"></i></span>Links</div>
          <?php } else { ?>
            <a href="/administrar/sitio/editor?sec=multimedia" title="Links externos"><span><i class="fa fa-link"></i></span>Links</a>
          <?php } ?>
        </li>
        <li id="menu-contactos-info" class="nosotros <?php if($contactos_act) print 'Act'; ?>">
          <?php if($contactos_des) { ?>
            <div title="Info"><span><i class="fa fa-users"></i></span>Info</div>
          <?php } else { ?>  
            <a href="/administrar/sitio/editor?sec=contactos" title="Info"><span><i class="fa fa-users"></i></span>Info</a>
          <?php } ?>
        </li> 
        <li id="menu-contactos-contactos" class="contactenos <?php if($contactos_act) print 'Act'; ?>">
          <?php if($contactos_des) { ?>
            <div title="Contáctenos"><span><i class="fa fa-map-marker"></i></span>Contáctenos</div>
          <?php } else { ?>  
            <a href="/administrar/sitio/editor?sec=contactos" title="Contáctenos"><span><i class="fa fa-map-marker"></i></span>Contáctenos</a>
          <?php } ?>
        </li> 
        <li id="menu-alta" class="ok Act" style="<?php if($alta_des) print 'display:none'; ?>">
          <a href="/administrar/sitio/editor?sec=alta" title="Alta del sitio"><span><i class="fa fa-check"></i></span></a>
        </li>                                                                        
      </ul>
    </div>
  </div>
  <!-- /.navbar-collapse -->
</nav>