<?php
  //Busco rubros de avisos
  $array_rubros = clasificados_rubros_obtener_rubros('aviso-casa');
  foreach($array_rubros as $rubro){
    if($rubro->title == 'Casas')
      $rubro_casa = $rubro->tid;
    if($rubro->title == 'Departamentos')
      $rubro_depto = $rubro->tid;
    if($rubro->title == 'Terrenos y Lotes')
      $rubro_lote = $rubro->tid;
    if($rubro->title == 'Locales')
      $rubro_local = $rubro->tid;
    if($rubro->title == 'Oficinas')
      $rubro_oficina = $rubro->tid;
  }
  $rubro_padre = current(taxonomy_get_parents($rubro_casa));
?>
<div class="Content MasBuscados MasBuscados2Col clearfix">
	<div class="CI clearfix">
		<div class="explorar">
			<h4><strong>Más buscado</strong></h4>
		</div>
		<div class="CLM" >
			<div class="CajaBuscado">
				<ul>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_depto; ?>&f[2]=ss_operacion%3AAlquileres" title="Buscar departamento en alquiler" rel="search">Departamento en alquiler </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_depto; ?>&f[2]=ss_operacion%3AVenta" title="Buscar departamento en venta" rel="search">Departamento en venta </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_operacion%3AVenta" title="Buscar casa en venta" rel="search">Casa en venta </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_operacion%3AAlquileres" title="Buscar casas en alquiler" rel="search">Casas en alquiler </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_lote; ?>&f[2]=ss_operacion%3AVenta" title="Buscar terrenos y lotes en venta" rel="search">Terrenos y Lotes en venta </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_oficina; ?>&f[2]=ss_operacion%3AAlquileres" title="Buscar oficina en alquiler" rel="search">Oficina en alquiler </a></li>
				</ul>
			</div>
		</div>
		<div class="CMM">
			<div class="CajaBuscado">
				<ul>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_lote; ?>&f[2]=ss_tipo_barrio%3ACountry" title="Buscar terrenos en country" rel="search">Terrenos en Country</a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_local; ?>&f[2]=ss_operacion%3AAlquileres" title="Buscar locales en alquiler" rel="search">Locales en Alquiler  </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_tipo_barrio%3ACountry" title="Buscar casas en country" rel="search">Casas en Country</a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_oficina; ?>&f[2]=ss_operacion%3AVenta" title="Buscar oficinas en venta" rel="search">Oficinas en Venta </a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_depto; ?>&f[2]=ss_operacion%3AVenta&fq[0]=fs_precio%3A%5B0%20TO%2080000%5D&fq[1]=is_moneda_k%3A2" title="Buscar departamentos venta menos de U$S80.000" rel="search">Departamentos Venta menos de U$S80.000 </a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_operacion%3AVenta&fq[0]=fs_precio%3A%5B0%20TO%20120000%5D&fq[1]=is_moneda_k%3A2" title="Buscar casa venta menos de U$S120.000" rel="search">Casa Venta menos de U$S120.000 </a></li>
				</ul>
			</div>
		</div>
		<div class="CRM">
			<div class="CajaBuscado">
				<ul>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_depto; ?>&f[2]=im_taxonomy_vid_5%3A3173&f[3]=im_taxonomy_vid_5%3A3194&f[4]=im_taxonomy_vid_5%3A5034" title="Buscar departamento en Nueva Córdoba" rel="search">Departamento en Nueva Córdoba </a></li>
					<li><a href="search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_depto; ?>&f[2]=im_taxonomy_vid_5%3A3173&f[3]=im_taxonomy_vid_5%3A3194&f[4]=im_taxonomy_vid_5%3A4951" title="Buscar departamento en General Paz" rel="search">Departamento en General Paz </a></li>
          <li><a href="search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[2]=im_taxonomy_vid_5%3A3173&f[3]=im_taxonomy_vid_5%3A3456" title="Buscar propiedades en Carlos Paz" rel="search">Propiedades en Carlos Paz </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_local; ?>&f[2]=im_taxonomy_vid_5%3A3173&f[3]=im_taxonomy_vid_5%3A3194&f[4]=im_taxonomy_vid_5%3A4910" title="Buscar locales en el centro" rel="search">Locales en el centro </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_zona%3ANorte" title="Buscar casas en zona norte" rel="search">Casas en Zona Norte </a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_casa; ?>&f[2]=ss_zona%3ASur" title="Buscar casas en zona sur" rel="search">Casas en Zona Sur </a></li>
				</ul>
			</div>
		</div>
	</div>
</div>