<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
  //Obtenemos los avisos mix a agregar
  /* $avisos_mix = clasificados_obtener_avisos_mix_en_home();
  if(count($avisos_mix) > 3) {
    $avisos_mix_random_keys = array_rand($avisos_mix, 3);
  } else {
    $avisos_mix_random_keys = array_keys($avisos_mix);
  }
  $avisos_mix_join = array();
  foreach($avisos_mix_random_keys as $aviso_key) {
    if(!isset($avisos_mix[$aviso_key])) continue;
    $aviso = $avisos_mix[$aviso_key];
    $aviso['title'] = truncate_utf8($aviso['title'], 45, FALSE, TRUE);
    $aviso['field_aviso_fotos_fid'] = l(theme('imagecache', 'ficha_aviso_314_211', $aviso['filepath']), 'node/'.$aviso['nid'], array('html' => TRUE));
    $aviso['field_aviso_moneda_value'] = $aviso['field_aviso_moneda_option'];
    $avisos_mix_join[] = $aviso;
  }
  //agregamos los avisos mix
  $avisos_vista = $view->display_handler->default_display->view->style_plugin->rendered_fields;
  if(count($avisos_mix_join) > 0) {
    $cortar = 10 - count($avisos_mix_join);
    $avisos_vista =  array_slice($avisos_vista, 0, $cortar);
    $avisos_vista = array_merge($avisos_vista, $avisos_mix_join);
    shuffle($avisos_vista);
  }
  $avisos = array();
  foreach($avisos_vista as $aviso) {
    if(preg_match('/RB_noimagen/', $aviso['field_aviso_fotos_fid'])) {
      $aviso['foto'] = l(theme('imagecache', 'ficha_aviso_314_211', 'imagefield_default_images/RB_noimagen_314x211.jpg'), 'node/'.$aviso['nid'], array('html' => TRUE));
    } else {
      $aviso['foto'] = $aviso['field_aviso_fotos_fid'];
    }
    $fnode = new stdClass(); //fake node
    $fnode->type = $aviso['type'];
    $fnode->field_aviso_precio[0]['value'] = $aviso['field_aviso_precio_value'];
    $fnode->field_aviso_ocultar_precio[0]['value'] = $aviso['field_aviso_ocultar_precio_value'];
    $fnode->field_aviso_moneda[0]['view'] = $aviso['field_aviso_moneda_value'];
    $fnode->field_aviso_precio_hasta[0]['value'] = $aviso['field_aviso_precio_hasta'];
    $aviso['precio'] = publicacion_avisos_obtener_precio_aviso_display($fnode);
    $avisos[] = $aviso;
  }
  $count = 0; */
?>
<div class="Content AvisosDestacados clearfix">
  <div class="Sombra">
    <div class="clearfix">
      <h2><img src="<?php print file_create_url(path_to_theme().'/img/anuncions_destacados.gif'); ?>" alt="Anuncios Destacados"></h2>
    </div>
  </div>
  <div class="clearfix">
<?php foreach($avisos as $aviso): ?>
<?php   $count++; ?>
    <div class="CajaAviso <?php print ($count%2==0?'Par':''); ?>">
      <div class="imgAviso">
<?php     print $aviso['foto']; ?>
      </div>
      <div class="AvisoDescripcion">
        <h4><?php print $aviso['title']; ?></h4>
        <?php
          //Kilometraje
          $kilometraje = "";
          if($aviso['field_aviso_usado_value'] == "O Km"){
            $kilometraje = $aviso['field_aviso_usado_value'];
          } elseif($aviso['field_aviso_kilometros_value'] != "") {
            $kilometraje = $aviso['field_aviso_kilometros_value']." Km";
          }
        ?>
        <p><?php print $aviso['field_aviso_usado_anio_value']." ".$kilometraje." ".$aviso['field_aviso_combustible_value']; ?></p>
        <span class="precio"><?php print $aviso['precio']; ?></span>
      </div>
    </div>
<?php endforeach; ?>
  </div>
</div>