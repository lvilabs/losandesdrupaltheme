<?php $content_part = explode('<p>', $content); ?>
<?php echo str_repeat("<div class='indented'>", $comment->depth);?>

<?php if($comment->pid == 0) : ?>

<article class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?> clearfix">
  <div class="rev-avatar fl-left">
    <?php if(empty($comment->picture)): ?>
      <a href="#" class="av-default" rel="noindex"><i class="fa fa-user"></i>
        <p><?php print $comment->usuario; ?></p>
      </a>
    <?php else: ?>
      <a href="#" rel="noindex"><img src="/<?php print $comment->picture; ?>">
        <p><?php print $comment->usuario; ?></p>
      </a>
    <?php endif; ?>
  </div>
  <div class="rev-content fl-left">
    <?php print $comment->fivestar_view; ?>
    <h5 class="mt-15"><?php print strip_tags($title); ?></h5>
    <p class="mt-15"><?php print '<p>'.$content_part[1]; ?></p>
    <?php if ($links && clvi_reputacion_mostrar_link_replica($comment->cid)): ?>
      <?php print $links; ?>
    <?php endif; ?>
  </div>
</article>

<?php else: ?>

<div class="rev-answer">
  <div class="rev-avatar fl-left">
    <?php if(empty($comment->picture)): ?>
      <a href="#" class="av-default" rel="noindex"><i class="fa fa-user"></i>
        <p><?php print $comment->usuario; ?></p>
      </a>
    <?php else: ?>
      <a href="#" rel="noindex"><img src="/<?php print $comment->picture; ?>">
        <p><?php print $comment->usuario; ?></p>
      </a>
    <?php endif; ?>
  </div>
  <div class="rev-resp fl-left content-expandible">
    <p class="bold"><?php print $comment->usuario; ?> respondió a esta opinión</p>
    <p><?php print '<p>'.$content_part[1]; ?></p>
  </div>
  <!-- <div class="controls mt-10">
    <span class="exp-more">Ver más</span>
    <span class="exp-less">Ver menos</span>
  </div> -->
</div>

<?php endif; ?>

<?php echo str_repeat("</div>", $comment->depth);?>
