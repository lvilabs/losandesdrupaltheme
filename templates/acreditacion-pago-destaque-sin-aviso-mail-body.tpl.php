<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;border-collapse:collapse">
          <tbody>
            <tr>
              <td valign="top" style="background:#fdfdfd;padding:7.5pt 7.5pt 7.5pt 7.5pt">
                <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
                  <tbody>
                    <tr>
                      <td style="padding:0cm 0cm 0cm 0cm">
                        <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">¡Felicitaciones!
                        <u></u><u></u></span></b></p>
                        <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!usuario, la acreditación del pago del destaque se realizó con éxito.<u></u><u></u></span></b></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table width="632" cellspacing="0" cellpadding="0" border="0" style="width:474.0pt">
                  <tbody>
                    <tr>
                      <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><u></u>&nbsp;<u></u></span></p>
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Información de la compra<u></u><u></u></span></p>
                        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
                          <tbody>
                            <tr>
                              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
                                <table width="89%" cellpadding="0" border="0" style="width:89.7%">
                                  <tbody>
                                    <tr>
                                      <td>
                                        <p><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Destaque/Mejora<u></u><u></u></span></i></p>
                                      </td>
                                      <td>
                                        <p><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Precio<u></u><u></u></span></i></p>
                                      </td>
                                    </tr>
                                    !tabla
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><br>
                        La compra no está vinculada a ningún aviso por lo que las mejoras y destaques quedarán disponibles para una publicación posterior.
                        Se podrá aplicar a avisos de tipo: !rubros.<u></u><u></u></span></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p><span style="font-size:10.0pt"><u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>