<?php
$color_grafico2 = array(0 => '#F7464A', 1 => '#46BFBD', 2 => '#FDB45C', 3 => '#949FB1', 4 => '#4D5360', 5 => '#86B2DB', 6 => '#F7D443', 7 => '#9392C8', 8 => '#2869B5', 9 => '#B070AD');
$highlight_grafico2 = array(0 => '#FF5A5E', 1 => '#5AD3D1', 2 => '#FFC870', 3 => '#A8B3C5', 4 => '#616774', 5 => '#96C3EA', 6 => '#FFE16B', 7 => '#A3A2DB', 8 => '#2E7AD1', 9 => '#C48DC1');
?>
<script>
  var pieData = [
    <?php $i = 0; foreach($form['globales']['cantidad_tipologia']['#value'] as $tipo) { 
      $legendPie[] = array('label' => $tipo['tipologia'],
                            'color' => $color_grafico2[$i],
                            'cantidad' => $tipo['cantidad']
                          );
    ?>
    {
      value: <?php print $tipo['cantidad']; ?>,
      color:"<?php print $color_grafico2[$i]; ?>",
      highlight: "<?php print $highlight_grafico2[$i]; ?>",
      label: "<?php print $tipo['tipologia']; ?>"
    },
    <?php $i++; } ?>
  ];
  window.onload = function(){
		<?php if(!empty($form['globales']['cantidad_tipologia']['#value'])) { ?>
      var ctx2 = document.getElementById("canvas2").getContext("2d");
      window.myPie = new Chart(ctx2).Pie(pieData);
    <?php } ?>
	}
</script>

<div class="solapa-estadisticas">
  <a href="/administrar/principal">Estadísticas de Mi Cuenta</a>
  <a class="int" href="/administrar/estadisticas_globales_autos">Estadísticas Globales</a>
</div>

<input type="hidden" id="tipoReporte" name="reporte" value="" />
<div class="Columnas MiCuentaAdmin clearfix">
  <div class="CD">
<?php if(!empty($form['globales']['mas_vistas_tipologia']['#value'])) { ?>
<!--Modulo-->
    <div class="panel-block conBorde masVistas">
      
      <h2 class="content-cantidad-consultas">Modelos más Vistos</h2>
      
      <div id="filtroDiasMasVisto" class="filtros_dias">
        <a href="#" data-dias="1">Ayer</a>
        <a href="#" data-dias="7">7 días</a>
        <a href="#" data-dias="15">15 días</a>
        <a href="#" data-dias="30" class="int">30 días</a>
      </div>
      <input type="hidden" id="hiddenDiasMasVisto" name="diasMasVisto" value="30" />
      
      <div class="recomendacion globales open">
        <strong>Detalle de esta estadística</strong>
        <p>Conocé qué es lo más buscado en nuestro sitio para mejorar tus publicaciones.</p>
      </div>
      
      <div class="filtros_selects">
        <?php if(!empty($form['globales']['select_marcas']['#value'])) { ?>
          <dl id="selectMarcaMasVisto" class="dropdown"> 
            <dt>
              <a id="multiMarcaMasVisto" data-select="MarcaMasVisto" href="#">
                <span class="hida">Marca y Modelo</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelect">
                <ul id="MarcaMasVisto">
                  <li class="quitarAll" data-content="multiMarcaMasVisto"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_marcas']['#value'] as $marca) { ?>
                    <li><label for="marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><input type="checkbox" data-content="multiMarcaMasVisto" name="marcaMasVisto_<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" data-name="<?php print $marca['marca']; ?>" id="marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" value="<?php print $marca['id']; ?>" /><?php print $marca['marca']; ?></label>
                      <?php if(!empty($form['globales']['select_modelos']['#value'])) { ?>
                        <?php foreach($form['globales']['select_modelos']['#value'] as $modelo) { ?>
                          <?php
                            $modelo_final = explode('-', $modelo['id']);
                            if(isset($modelo_final[1]) && !empty($modelo_final[1]) && $modelo_final[0] == $marca['id']) {
                          ?>
                            <li class="modelos marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><label for="modelo-<?php print $modelo_final[1]; ?>"><input type="checkbox" data-content="multiMarcaMasVisto" name="modeloMasVisto_<?php print $modelo_final[1]; ?>" data-name="<?php print $modelo_final[1]; ?>" id="modelo-<?php print $modelo_final[1]; ?>" value="<?php print $marca['id'].'-'.$modelo_final[1]; ?>" /><?php print $modelo_final[1]; ?></label></li>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_anio']['#value'])) { ?>
          <dl id="selectAnioMasVisto" class="dropdown"> 
            <dt>
              <a id="multiAnioMasVisto" data-select="AnioMasVisto" href="#">
                <span class="hida">Año</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelect">
                <ul id="AnioMasVisto">
                  <li class="quitarAll" data-content="multiAnioMasVisto"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_anio']['#value'] as $anio) { ?>
                    <li><label for="<?php print $anio; ?>"><input type="checkbox" data-content="multiAnioMasVisto" name="anioMasVisto_<?php print $anio; ?>" data-name="<?php print $anio; ?>" id="<?php print $anio; ?>" value="<?php print $anio; ?>" /><?php print $anio; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <a class="linkFiltro" id="filtroMasVisto" href="#"><i class="fa fa-search"></i></a>
      </div>
      
      <div class="consultas-sin-leer">
        <table id="listadoMasVistos" class="consultasResumen" width="100%">
          <tr>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          <?php $i = 1; $position_class = ''; foreach($form['globales']['mas_vistas_tipologia']['#value'] as $mas_vistas) { ?>
            <?php if($i == 1) {
                $position_class = 'first';
              } elseif($i == 2) {
                $position_class = 'second';
              } else {
                $position_class = '';
              }
            ?>
            <tr class="<?php print $position_class; ?>">
              <td class="contacto"><?php print $mas_vistas['posicion']; ?></td>
              <td class="consulta"><?php print $mas_vistas['tipologia']; ?></td>
              <td class="consulta"><?php print number_format($mas_vistas['cantidad'], 0, '', '.'); ?></td>
            </tr>
          <?php $i++; } ?>
        </table>
        <div class="globalLoading"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
      </div>
      
      <div class="acciones_footer">
        <div class="select_cantidad">
          <select id="filtroCantidadMasVisto">
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
          <input type="hidden" id="hiddenCantidadMasVisto" name="cantidadMasVisto" value="3" />
        </div>
        <div class="link_exportacion"><a id="exportarMasVistos" href="#">Exportar a Excel</a></div>
      </div>
      
    </div>
<!--fin Modulo-->
<?php } ?>
<?php if(!empty($form['globales']['mas_destacados']['#value'])) { ?>
<!--Modulo-->
    <div class="panel-block conBorde masVistas">
      
      <h2 class="content-cantidad-consultas">Modelos mejor Posicionados</h2>
      
      <div class="recomendacion globales open">
        <strong>Detalle de esta estadística</strong>
        <p>Aplicá de manera eficiente tus destaques consultando este reporte y hacé que tu aviso esté vigente en las búsquedas. ¡Recordá la importancia de la republicación de tus propiedades!</p>
      </div>
      
      <div class="filtros_selects">
        <?php if(!empty($form['globales']['select_marcas']['#value'])) { ?>
          <dl id="selectMarcaMasDestacada" class="dropdown"> 
            <dt>
              <a id="multiMarcaMasDestacada" data-select="MarcaMasDestacada" href="#">
                <span class="hida">Marca y Modelo</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelectDestacada">
                <ul id="MarcaMasDestacada">
                  <li class="quitarAll" data-content="multiMarcaMasDestacada"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_marcas']['#value'] as $marca) { ?>
                    <li><label for="destacada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><input type="checkbox" data-content="multiMarcaMasDestacada" name="marcaMasDestacada_<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" data-name="<?php print $marca['marca']; ?>" id="destacada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" value="<?php print $marca['id']; ?>" /><?php print $marca['marca']; ?></label>
                      <?php if(!empty($form['globales']['select_modelos']['#value'])) { ?>
                        <?php foreach($form['globales']['select_modelos']['#value'] as $modelo) { ?>
                          <?php
                            $modelo_final = explode('-', $modelo['id']);
                            if(isset($modelo_final[1]) && !empty($modelo_final[1]) && $modelo_final[0] == $marca['id']) {
                          ?>
                            <li class="modelos destacada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><label for="destacada_modelo-<?php print $modelo_final[1]; ?>"><input type="checkbox" data-content="multiMarcaMasDestacada" name="modeloMasDestacada_<?php print $modelo_final[1]; ?>" data-name="<?php print $modelo_final[1]; ?>" id="destacada_modelo-<?php print $modelo_final[1]; ?>" value="<?php print $marca['id'].'-'.$modelo_final[1]; ?>" /><?php print $modelo_final[1]; ?></label></li>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_anio']['#value'])) { ?>
          <dl id="selectAnioMasDestacada" class="dropdown"> 
            <dt>
              <a id="multiAnioMasDestacada" data-select="AnioMasDestacada" href="#">
                <span class="hida">Año</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelectDestacada">
                <ul id="AnioMasDestacada">
                  <li class="quitarAll" data-content="multiAnioMasDestacada"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_anio']['#value'] as $anio) { ?>
                    <li><label for="destacada_<?php print $anio; ?>"><input type="checkbox" data-content="multiAnioMasDestacada" name="anioMasDestacada_<?php print $anio; ?>" data-name="<?php print $anio; ?>" id="destacada_<?php print $anio; ?>" value="<?php print $anio; ?>" /><?php print $anio; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <a class="linkFiltro" id="filtroMasDestacada" href="#"><i class="fa fa-search"></i></a>
      </div>
      
      <div class="consultas-sin-leer">
        <table id="listadoMasDestacada" class="consultasResumen" width="100%">
          <tr>
            <th></th>
            <th></th>
            <th>SPremium</th>
            <th>Premium</th>
            <th>Estandar</th>
            <th>Basico</th>
          </tr>
          <?php $i = 1; $position_class = ''; foreach($form['globales']['mas_destacados']['#value'] as $mas_vistas) { ?>
            <?php if($i == 1) {
                $position_class = 'first';
              } elseif($i == 2) {
                $position_class = 'second';
              } else {
                $position_class = '';
              }
            ?>
            <tr class="<?php print $position_class; ?>">
              <td class="contacto"><?php print $mas_vistas['posicion']; ?></td>
              <td class="consulta"><?php print $mas_vistas['tipologia']; ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['superpremium'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['premium'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['estandar'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['basico'], 0, '', '.'); ?></td>
            </tr>
          <?php $i++; } ?>
        </table>
        <div class="globalLoadingDestacada"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
      </div>
      
      <div class="acciones_footer">
        <div class="select_cantidad">
          <select id="filtroCantidadMasDestacada">
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
          <input type="hidden" id="hiddenCantidadMasDestacada" name="cantidadMasDestacada" value="3" />
        </div>
        <div class="link_exportacion"><a id="exportarMasDestacada" href="#">Exportar a Excel</a></div>
      </div>
      
    </div>
<!--fin Modulo-->
<?php } ?>
  </div>
  
  <?php if(!empty($form['globales']['mas_vistas']['#value'])) { ?>
    <div class="CR">
      <div class="panel-block conBorde visitasTotales">
        <div class="textoTotal">Total vistas avisos de <span class="visitasTiempo">Ayer</span></div>
        <div class="contadorTotal"><?php print number_format($form['globales']['mas_vistas']['#value'][0]['cantidad'], 0, '', '.'); ?></div>
        <div id="filtroVisitasTotales" class="filtros_dias totales">
          <a href="#" data-dias="1" class="int">Ayer</a>
          <a href="#" data-dias="7">7 días</a>
          <a href="#" data-dias="15">15 días</a>
          <a href="#" data-dias="30" class="end">30 días</a>
        </div>
        <input type="hidden" id="hiddenDiasVisitasTotales" name="diasVisitasTotales" value="1" />
      </div>
    </div>
  <?php } ?>
  
  <div class="CR">
    
    <?php if(!empty($form['globales']['cantidad_tipologia']['#value'])) { ?>
      <div class="panel-block conBorde">
        <h2>Modelos Publicados</h2>
        
        <div class="recomendacion globales right open">
          <strong>Detalle de esta estadística</strong>
          <p>Conocé cuántos modelos similares a los tuyos están publicadas en el sitio.</p>
        </div>
        
        <div class="filtros_selects">
          <?php if(!empty($form['globales']['select_marcas']['#value'])) { ?>
            <dl id="selectMarcaPublicadas" class="dropdown"> 
              <dt>
                <a id="multiMarcaPublicadas" data-select="MarcaPublicadas" href="#">
                  <span class="hida">Marca y Modelo</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPublicadas">
                  <ul id="MarcaPublicadas">
                    <li class="quitarAll" data-content="multiMarcaPublicadas"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_marcas']['#value'] as $marca) { ?>
                      <li><label for="publicada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><input type="checkbox" data-content="multiMarcaPublicadas" name="marcaPublicadas_<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" data-name="<?php print $marca['marca']; ?>" id="publicada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" value="<?php print $marca['id']; ?>" /><?php print $marca['marca']; ?></label>
                        <?php if(!empty($form['globales']['select_modelos']['#value'])) { ?>
                          <?php foreach($form['globales']['select_modelos']['#value'] as $modelo) { ?>
                            <?php
                              $modelo_final = explode('-', $modelo['id']);
                              if(isset($modelo_final[1]) && !empty($modelo_final[1]) && $modelo_final[0] == $marca['id']) {
                            ?>
                              <li class="modelos publicada_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><label for="publicada_modelo-<?php print $modelo_final[1]; ?>"><input type="checkbox" data-content="multiMarcaPublicadas" name="modeloPublicadas_<?php print $modelo_final[1]; ?>" data-name="<?php print $modelo_final[1]; ?>" id="publicada_modelo-<?php print $modelo_final[1]; ?>" value="<?php print $marca['id'].'-'.$modelo_final[1]; ?>" /><?php print $modelo_final[1]; ?></label></li>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <?php if(!empty($form['globales']['select_anio']['#value'])) { ?>
            <dl id="selectAnioPublicadas" class="dropdown"> 
              <dt>
                <a id="multiAnioPublicadas" data-select="AnioPublicadas" href="#">
                  <span class="hida">Año</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPublicadas">
                  <ul id="AnioPublicadas">
                    <li class="quitarAll" data-content="multiAnioPublicadas"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_anio']['#value'] as $anio) { ?>
                      <li><label for="publicada_<?php print $anio; ?>"><input type="checkbox" data-content="multiAnioPublicadas" name="anioPublicadas_<?php print $anio; ?>" data-name="<?php print $anio; ?>" id="publicada_<?php print $anio; ?>" value="<?php print $anio; ?>" /><?php print $anio; ?></label></li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <a class="linkFiltro" id="filtroPublicadas" href="#"><i class="fa fa-search"></i></a>
        </div>
        
        <div id="contentGrafico"><canvas id="canvas2" height="200" width="280"></canvas></div>
        <div id="legendDiv">
          <ul>
          <?php foreach($legendPie as $legend) { ?>
            <li><span style="background-color:<?php print $legend['color']; ?>;"></span><?php print $legend['label']; ?> - <?php print $legend['cantidad']; ?></li>
          <?php } ?>
          </ul>
        </div>
      </div>
    <?php } ?>
    
  </div>
  
  
  <div class="CT">
    <?php if(!empty($form['globales']['precio_promedio']['#value'])) { ?>
      <div class="panel-block conBorde" style="float: left;">
        <h2>Precios Promedio por Modelo y Año</h2>
        
        <div class="recomendacion globales open">
          <strong>Detalle de esta estadística</strong>
          <p>Conocé el valor promedio de los vehículos de acuerdo a su modelo y año. ¡Es una gran referencia para conocer cómo estás compitiendo con el valor de tu vehículo!</p>
        </div>
        
        <div class="filtros_selects reportePrecios">
          <?php if(!empty($form['globales']['select_marcas']['#value'])) { ?>
            <dl id="selectMarcaPrecios" class="dropdown"> 
              <dt>
                <a id="multiMarcaPrecios" data-select="MarcaPrecios" href="#">
                  <span class="hida">Marca y Modelo</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPrecios">
                  <ul id="MarcaPrecios">
                    <li class="quitarAll" data-content="multiMarcaPrecios"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_marcas']['#value'] as $marca) { ?>
                      <li><label for="precios_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><input type="checkbox" data-content="multiMarcaPrecios" name="marcaPrecios_<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" data-name="<?php print $marca['marca']; ?>" id="precios_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>" value="<?php print $marca['id']; ?>" /><?php print $marca['marca']; ?></label>
                        <?php if(!empty($form['globales']['select_modelos']['#value'])) { ?>
                          <?php foreach($form['globales']['select_modelos']['#value'] as $modelo) { ?>
                            <?php
                              $modelo_final = explode('-', $modelo['id']);
                              if(isset($modelo_final[1]) && !empty($modelo_final[1]) && $modelo_final[0] == $marca['id']) {
                            ?>
                              <li class="modelos precios_marca-<?php print strtolower(str_replace(' ','-', $marca['id'])); ?>"><label for="precios_modelo-<?php print $modelo_final[1]; ?>"><input type="checkbox" data-content="multiMarcaPrecios" name="modeloPrecios_<?php print $modelo_final[1]; ?>" data-name="<?php print $modelo_final[1]; ?>" id="precios_modelo-<?php print $modelo_final[1]; ?>" value="<?php print $marca['id'].'-'.$modelo_final[1]; ?>" /><?php print $modelo_final[1]; ?></label></li>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <?php if(!empty($form['globales']['select_anio']['#value'])) { ?>
            <dl id="selectAnioPrecios" class="dropdown"> 
              <dt>
                <a id="multiAnioPrecios" data-select="AnioPrecios" href="#">
                  <span class="hida">Año</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPrecios">
                  <ul id="AnioPrecios">
                    <li class="quitarAll" data-content="multiAnioPrecios"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_anio']['#value'] as $anio) { ?>
                      <li><label for="precios_<?php print $anio; ?>"><input type="checkbox" data-content="multiAnioPrecios" name="anioPrecios_<?php print $anio; ?>" data-name="<?php print $anio; ?>" id="precios_<?php print $anio; ?>" value="<?php print $anio; ?>" /><?php print $anio; ?></label></li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <a class="linkFiltro" id="filtroPrecios" href="#"><i class="fa fa-search"></i></a>
          
          <div class="link_exportacion right"><a id="exportarPrecios" href="#">Exportar a Excel</a></div>
          
        </div>
        
        <div class="estadisticas_precios">
          <?php if(!empty($form['globales']['precio_promedio']['#value'])) { ?>
            <div class="precio_cards">
              <h3>Ventas</h3>
              <?php foreach($form['globales']['precio_promedio']['#value'] as $precios_promedio) { ?>
                <li class="tc_card">
                  <div class="tc_inner_card tilt_right" style="border-bottom: 3px solid <?php print $precios_promedio['color']; ?>;"><span class="tc_shadow"></span>
                    <div class="tc_front">
                      <div class="tc_click_target">
                        <?php print $precios_promedio['tipologia']; ?>
                        <p style="color: <?php print $precios_promedio['color']; ?>">$ <?php print $precios_promedio['promedio']; ?></p>
                      </div>
                    </div>
                  </div>
                </li>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
        
        <div class="globalLoadingPrecios"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
        
      </div>
    <?php } ?>
  </div>
  
</div>
<input type="hidden" name="interna" value="autos" />
