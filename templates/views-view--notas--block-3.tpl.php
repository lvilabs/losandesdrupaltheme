<?php
// $Id: views-view.tpl.php,v 1.13.2.2 2010/03/25 20:25:28 merlinofchaos Exp $
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
?>
<div class="columnas"> 
  <header class="tit-modulo"><a href="/noticias">Noticias</a></header> 
  <section class="b6 clearfix">
    <div class="panel-instanciado clearfix ">
      <?php foreach($view->display_handler->default_display->view->style_plugin->rendered_fields as $nota){ 
        $con_video = 0;
        if(!empty($nota['field_nota_video_genoa_value']) || !empty($nota['field_aviso_video_embed']))
          $con_video = 1;
        $fecha_nota = clasificados_fecha_formato_nota($nota['created']);
      ?>
        <div class="panel nota comun <?php if(empty($nota['field_nota_imagenes_fid'])) print 'sin-foto'; ?>">
        <div class="panel-body">
          <article class="node-teaser node-teaser-card">
            <?php if(isset($nota['field_nota_imagenes_fid']) && !empty($nota['field_nota_imagenes_fid'])) { ?>
            <div class="foto">
              <?php if($con_video) { ?>
                <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
              <?php } ?>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
                <div class="gradiente"></div>
              <?php } ?>
              <?php print $nota['field_nota_imagenes_fid']; ?>
            </div>
            <?php } ?>
            <div class="contenido">
              <?php if(empty($nota['field_nota_imagenes_fid'])) { ?>
              <h2><?php print $nota['title']; ?></h2>
              <div class="bajada"><?php print strip_tags($nota['field_nota_resumen_value'],'<p>'); ?></div>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
              <div class="panelAuthor" rel="author">
                <span class="byAuthor">por</span> <?php print $nota['field_nota_autor_value']; ?>
              </div>
              <?php } ?>
              <?php } else { ?>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
              <div class="panelAuthor" rel="author">
                <span class="byAuthor">por</span> <?php print $nota['field_nota_autor_value']; ?>
              </div>
              <?php } ?>
              <h2><?php print $nota['title']; ?></h2>
              <?php } ?>
              <div class="footer-card">
                <div class="data">
                  <span class="nodeData">
                    <time datetime="<?php print $nota['created']; ?>"><?php print $fecha_nota; ?></time>&nbsp;•&nbsp;<?php print strtolower($nota['name']); ?>
                  </span>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
      <?php } ?>
    </div>
  </section>
</div>  