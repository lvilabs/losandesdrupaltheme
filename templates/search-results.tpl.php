<?php

/**
 * @file search-results.tpl.php
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependant to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $type: The type of search, e.g., "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 */
  session_start();
  global $user;
  //Bloques de ordenamiento y cantidad
  $block_sort = module_invoke('apachesolr_search', 'block', 'view', 'sort');
  $block_sort['subject'] = "";
  
  //Titulo del resultado de busqueda
  $get = $_GET;
  $argumento = arg(2);
  $resultado = _clasificados_apachesolr_obtener_titulo_buscador($argumento, $get);
  
  $es_producto = 0;
  if(isset($get['f'])){ 
    foreach($get['f'] as $filtro) {
      if($filtro == "im_taxonomy_vid_".CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID.":6106"){
        $es_producto = 1;
        break;
      } elseif(strpos($filtro, "uid") !== FALSE) {
        $user_id = explode("uid:", $filtro);
        if(ventas_es_vendedor($user_id[1])) {
          $es_producto = 1;
          break;
        }
      }
    }
  }
  
  $env_id = apachesolr_default_environment();
  $query = apachesolr_current_query($env_id);
  $response = apachesolr_static_response_cache($query->getSearcher());
  $cantidad = $response->response->numFound;
  
  $buscador_alojamientos = 0;
  if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, CLASIFICADOS_RUBROS_MIX_ALOJAMIENTOS_TID)) {
    $buscador_alojamientos = 1;
    
    // Si esta filtrado por fechas, las guardo en session para conservarlas en las fichas de avisos
    unset($_SESSION['search-checkin']);
    unset($_SESSION['search-checkout']);
    $get = $_GET;
    if(isset($get['fq']) && !empty($get['fq'])) {
      foreach($get['fq'] as $filter_fq){
        if(strpos($filter_fq, 'im_reserved') !== FALSE) {
          if(strpos($filter_fq, ':[') === FALSE) {
            $fechas_reserva = explode(':', $filter_fq);
            $fecha_llegada_anio = substr($fechas_reserva[1], 0, 4);
            $fecha_llegada_mes  = substr($fechas_reserva[1], 4, 2);
            $fecha_llegada_dia  = substr($fechas_reserva[1], 6, 2);
            $llegada_default = $fecha_llegada_anio.'-'.$fecha_llegada_mes.'-'.$fecha_llegada_dia;
          } else {
            $fechas_reserva = explode(':[', $filter_fq);
            $fecha_llegada = explode(' TO ', $fechas_reserva[1]);
            $fecha_llegada_anio = substr($fecha_llegada[0], 0, 4);
            $fecha_llegada_mes  = substr($fecha_llegada[0], 4, 2);
            $fecha_llegada_dia  = substr($fecha_llegada[0], 6, 2);
            $llegada_default = $fecha_llegada_anio.'-'.$fecha_llegada_mes.'-'.$fecha_llegada_dia;
            $fecha_salida = explode(']', $fecha_llegada[1]);
            $fecha_salida_anio = substr($fecha_salida[0], 0, 4);
            $fecha_salida_mes  = substr($fecha_salida[0], 4, 2);
            $fecha_salida_dia  = substr($fecha_salida[0], 6, 2);
            $salida_default = $fecha_salida_anio.'-'.$fecha_salida_mes.'-'.$fecha_salida_dia;
          }
          $_SESSION['search-checkin'] = $llegada_default;
          $_SESSION['search-checkout'] = $salida_default;
        }
      }
    }   
    
  }
    
  $rubro_autos = clasificados_rubros_obtener_rubro_raiz_tipo_aviso('aviso_auto');
  $rubros_autos = clasificados_rubros_obtener_rubros_hijos($rubro_autos->tid);
  $rubros_autos[$rubro_autos->tid] = $rubro_autos;
  $rubro_auto_aplicado = FALSE;
  foreach($rubros_autos as $tid => $rubro) {
    if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, $tid)) {
      $rubro_auto_aplicado = TRUE;
    }
  }
?>
<script>$('.box h2').eq(0).remove();</script>

<?php if(!$buscador_alojamientos): ?>

<div class="">
  <div class="Sombra clearfix">
    <div class="Left">
      <?php if(isset($promo_fiorani) && $promo_fiorani != '') { print $promo_fiorani; } else { ?>
        <h1 class="Titulo"><?php (empty($resultado))? print 'Resultado de búsqueda' : print $resultado; ?></h1>
        <?php if($cantidad > 0): ?><p class="cantidadResultados">Avisos encontrados <span class="Naranja">(<?php print $cantidad; ?> resultados)</span> </p><?php endif; ?>
      <?php } ?>
    </div>        
  </div>
  <div class="ColModelos FiltroModelos">
    <div class="Borde clearfix">
      <div class="Left"></div>
      <div class="Right Detalles">
        <?php if($block_sort['content'] != ""){ ?>
          <div id="orden"><?php print $block_sort['content']; //theme('block', (object) $block_sort); ?></div>
        <?php } else { ?>
          <div class="item-list">
            <ul>
              <?php if($rubro_auto_aplicado) { ?>
              <li>AÑO</li>
              <li>KM</li>
              <li class="combustible">Combustible</li>
              <?php } ?>
              <li>Precio</li>
            </ul>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="search-results <?php print $type; ?>-results">
  <?php print $search_results; ?>
</div>
<div class="ResultadoB">
  <div class="paginado <?php if($es_producto) print 'cards'; ?>">
    <?php print $pager; ?>
    <?php if($cantidad > 0): ?><p class="Gris"> (<?php print $cantidad; ?> resultados)</p><?php endif; ?>
  </div>
</div>

<?php else: ?>

<div class="search-main fl-left">
  <h1 class="search-title"><?php (empty($resultado))? print 'Resultado de búsqueda' : print $resultado; ?>: <?php print $cantidad; ?> alojamientos encontrados</h1>
  <div class="search-order">
    <p class="bold fl-left">Ordenar por:</p>
    <ul class="fl-left">
      <!-- <li class="fl-left"><a href="#">Mejor valoracion <i class="fa fa-chevron-down"></i> </a></li> -->
      <li class="fl-left"><a href="<?php print $_SERVER['REQUEST_URI']; ?>&solrsort=fs_precio%20asc">Precio mas bajo <i class="fa fa-chevron-down"></i> </a></li>
    </ul>
  </div>
  <?php print $search_results; ?>
  <div class="search-pagination">
    <?php print $pager; ?>
    <p>(<?php print $cantidad; ?> resultados)</p>
  </div>
</div>

<?php endif; ?>

<!-- Modal bienvenida alojamientos -->
<div style="display: none;">
  <div id="modal_bienvenida_buscador" class="v-modal modal-confirm">
    <div class="modal-title">
      Bienvenidos a Alojamientos de Clasificados LaVoz.com.ar
    </div>
    <div class="modal-content">
      <p>La categoría de Alquileres Temporarios de Inmuebles se convirtió en Alojamientos, una plataforma de reserva online donde cabañas, complejos turísticos, dueños particulares e inmobiliarias publican propiedades para alquiler temporario.
      </p>
    </div>
  </div>
</div>

<?php
if($user->uid)
  $user_profile =  user_load($user->uid);
$nombre_contacto = '';
if(isset($_SESSION['contactar_vendedor_form_nombre']))
  $nombre_contacto = $_SESSION['contactar_vendedor_form_nombre'];
elseif($user->uid)
  $nombre_contacto = $user_profile->profile_nombre.' '.$user_profile->profile_apellido;
$telefono_contacto = '';
if(isset($_SESSION['contactar_vendedor_form_telefono']))
  $telefono_contacto = $_SESSION['contactar_vendedor_form_telefono'];
elseif($user->uid)
  $telefono_contacto = $user_profile->profile_telefono_principal;
$mail_contacto = '';
if(isset($_SESSION['contactar_vendedor_form_email']))
  $mail_contacto = $_SESSION['contactar_vendedor_form_email'];
elseif($user->uid)
  $mail_contacto = $user_profile->mail;
$consulta_contacto = 'Estoy interesado en este aviso que vi en Clasificados La Voz y quisiera que me contacten. Gracias.';
if(isset($_SESSION['contactar_vendedor_form_consulta']))
  $consulta_contacto = $_SESSION['contactar_vendedor_form_consulta'];
?>
<div style="display: none;">
  <div id="modal-contactar-buscador" class="v-modal modal-confirm">
    <div class="contacto-header">Consultar por <span id="contacto-titulo"></span></div>
    <form action="" accept-charset="UTF-8" method="post" id="contactar-vendedor-formulario">
      <div class="messages status" style="display: none;"></div>
      <div class="form-item" id="edit-contactar-vendedor-nombre-wrapper">
       <input type="text" maxlength="128" name="contactar_vendedor_nombre" id="edit-contactar-vendedor-nombre" size="60" value="<?php print $nombre_contacto; ?>" placeholder="Nombre" class="form-text required">
      </div>
      <div class="form-item" id="edit-contactar-vendedor-telefono-wrapper">
       <input type="text" maxlength="128" name="contactar_vendedor_telefono" id="edit-contactar-vendedor-telefono" size="60" value="<?php print $telefono_contacto; ?>" placeholder="Teléfono" class="form-text required">
      </div>
      <div class="form-item" id="edit-contactar-vendedor-mail-wrapper">
       <input type="text" maxlength="128" name="contactar_vendedor_mail" id="edit-contactar-vendedor-mail" size="60" value="<?php print $mail_contacto; ?>" placeholder="E-mail" class="form-text required">
      </div>
      <div class="form-item" id="edit-contactar-vendedor-consulta-wrapper">
       <textarea cols="60" rows="5" name="contactar_vendedor_consulta" id="edit-contactar-vendedor-consulta" placeholder="Escriba su consulta aquí..." class="form-textarea resizable required"><?php print $consulta_contacto; ?></textarea>
      </div>
      <?php if(!user_is_logged_in()) { ?>
        <div class="g-recaptcha" data-sitekey="<?php print variable_get('api_key_recaptcha', ''); ?>" id="recaptcha_contactar"></div>
      <?php } ?>
      <input type="hidden" name="contactar_vendedor_estado" id="edit-contactar-vendedor-estado" value="1">
      <input type="hidden" name="contactar_vendedor_aviso_nid" id="edit-contactar-vendedor-aviso" value="">
      <input type="hidden" name="contactar_vendedor_buscador" id="edit-contactar-vendedor-buscador" value="1">
      <input type="submit" name="op" id="edit-contactar-vendedor-submit" value="Consultar" class="form-submit">
    </form>
  </div>
</div>