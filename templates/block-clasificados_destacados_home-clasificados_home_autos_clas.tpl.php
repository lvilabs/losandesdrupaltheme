<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="AvisosDestacadosSlide clearfix">
    <div class="Title">
    	<hr>
      <a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323" title="Avisos destacados de automotores" rel="search"><h2>Vehículos destacados</h2></a>
    </div>
    <div class="navigation">
      <ul>
        <li><strong>Buscar por</strong></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6324" title="Autos" rel="search">Autos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6327" title="Utilitarios y Camiones" rel="search">Utilitarios y Camiones</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6325" title="4x4 y Todo Terreno" rel="search">4x4 y Todo Terreno</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6329" title="Accesorios y Repuestos" rel="search">Accesorios y Repuestos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6326" title="Motos y Náutica" rel="search">Motos y Náutica</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323&f[1]=im_taxonomy_vid_34:6328" title="Planes de Ahorro" rel="search">Planes de Ahorro</a></li>
      </ul>
      <div class="destacados-ver-mas"><a href="/autos" title="Ver más avisos de autos">Ver Más</a></div>
    </div>
    <div class="accessible_news_slider slider_tienda">
<?php if(count($content['avisos'])>3) : ?>
      <div class="Botones">
        <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
      </div>
<?php endif; ?>
      <div class="lista">
        <ul class="clearfix bloque_avisos <?php (count($content['avisos'])>3)? print 'Slider' : print 'no-Slider';?>">
<?php for($i=0; $i<count($content['avisos']); $i++) : ?>
          <li>
            <div class="CajaAviso ">
              <a href="<?php print $content['avisos'][$i]['url']; ?>?cx_level=destacados_home_autos" title="<?php print $content['avisos'][$i]['titulo']; ?>">
                <div class="imgAviso">
                  <?php
                    $alt_img = explode('/', $content['avisos'][$i]['url']);
                    $alt_img = $alt_img[0].' de '.$alt_img[1].' '.$content['avisos'][$i]['titulo'].' a '.$content['avisos'][$i]['precio'];
                  ?>
                   <img src="<?php print $content['avisos'][$i]['foto']; ?>" alt="<?php print $alt_img; ?>" title="<?php print $content['avisos'][$i]['titulo']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211"> 
                </div>
                <div class="AvisoDescripcion">
                  <h4><?php print $content['avisos'][$i]['titulo']; ?></h4>
                  <p><?php  if(isset($content['avisos'][$i]['anio']) && $content['avisos'][$i]['anio'] != '') print $content['avisos'][$i]['anio'].' '; 
                            if(isset($content['avisos'][$i]['kilometros']) && $content['avisos'][$i]['kilometros'] != '') print $content['avisos'][$i]['kilometros'].' Km '; 
                            if(isset($content['avisos'][$i]['combustible']) && $content['avisos'][$i]['combustible'] != '') print $content['avisos'][$i]['combustible'];
                    ?></p>
                  <span class="precio"><?php print $content['avisos'][$i]['precio']; ?></span>
                </div>
              </a>
            </div>
          </li>
<?php endfor; ?>
        </ul>
      </div> 
    </div>
  </div>
</div>