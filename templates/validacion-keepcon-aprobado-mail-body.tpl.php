<tr>
  <td valign="top" style="padding:0cm 0cm 0cm 15.0pt; text-align:left;" class="bodyContent">
    <!-- // Begin Module: Standard Content \\ -->
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-family:Arial,sans-serif; font-size:12.0pt">¡Felicitaciones !usuario!</span></b></p>
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-family:Arial,sans-serif; font-size:12.0pt">&nbsp;</span></b></p>
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-family:Arial,sans-serif; font-size:12.0pt">Tu aviso se publicó correctamente en Clasificados Los Andes</span></b></p>
    <span style="font-family:Arial,sans-serif; font-size:12.0pt"><br><br></span>
    <!-- // End Module: Standard Content \\ -->
  </td>
</tr>
<tr>
  <td style="background:#CCE8F6;padding:7.5pt 0cm 0cm 15.0pt">
    <table border="0" cellpadding="0" width="82%">
      <tbody>
        <tr>
          <td style="padding:.75pt .75pt .75pt .75pt"></td>
          <td style="padding:.75pt .75pt .75pt .75pt">
            <p style="margin: 0 0 0.0001pt;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif; color: #4F81BD;">Inicia</span></p>
          </td>
          <td style="padding:.75pt .75pt .75pt .75pt">
            <p style="margin: 0 0 0.0001pt;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif; color: #4F81BD;">Finaliza</span></p>
          </td>
        </tr>
        <tr>
          <td style="padding:.75pt .75pt .75pt .75pt">
            <p style="margin: 0 0 0.0001pt;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif; color: #4F81BD;"><a href="!url_aviso"><img width="50" height="50" border="0" alt="!titulo_aviso" src="!foto_aviso">&nbsp;&nbsp;!titulo_aviso</a></span></p>
          </td>
          <td style="padding:.75pt .75pt .75pt .75pt">
            <p style="margin: 0 0 0.0001pt;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif; color: #4F81BD;">!fecha_inicio</span></p>
          </td>
          <td style="padding:.75pt .75pt .75pt .75pt">
            <p style="margin: 0 0 0.0001pt;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif; color: #4F81BD;">!fecha_fin</span></p>
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>
<tr>
  <td style="text-align:left;">
    <p style="margin: 0 0 0.0001pt;">&nbsp;</p>
    <p>
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
      <span><a href="!crear_url"><img border="0" width="159" height="49" src="!crear_img"></a></span>
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
      <span><a href="!editar_url"><img border="0" width="159" height="49" src="!editar_img"></a></span>
      <span></span>
    </p>
    <p style="margin: 0 0 0.0001pt;">&nbsp;</p>
    <p style="margin: 0 0 0.0001pt;">
      <span>
        <a href="!destacar_url"><img border="0" width="629" height="116" src="!destacar_img"></a>
      </span>
      <span></span>
    </p>
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-size:10.0pt; font-family: Arial,sans-serif;">Consejos para publicar mejor</span></b></p>
    <p style="margin: 0 0 0.0001pt;">
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-<span>&nbsp;&nbsp;&nbsp;&nbsp;
      <span style="font-size:10.0pt;font-family: Arial, sans-serif;">Publicá siempre tu aviso con fotos. Multiplicá tus posibilidades de visitas.</span>
    </p>
    <p style="margin: 0 0 0.0001pt;">
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-<span>&nbsp;&nbsp;&nbsp;&nbsp;
      <span style="font-size:10.0pt;font-family: Arial, sans-serif;">Controlá que el aviso esté publicado en el rubro correcto.</span>
    </p>
    <p style="margin: 0 0 0.0001pt;">
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-<span>&nbsp;&nbsp;&nbsp;&nbsp;
      <span style="font-size:10.0pt;font-family: Arial, sans-serif;">Compará tu aviso con otros que estén publicados para mejorar tu oferta.</span>
    </p>
    <p>&nbsp;</p>
  </td>
</tr>