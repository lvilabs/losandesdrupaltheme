<?php
  //Busco rubros de avisos
  $array_rubros = clasificados_rubros_obtener_rubros('aviso-auto');
  foreach($array_rubros as $rubro){
    if($rubro->title == 'Autos'){
      $rubro_auto = $rubro->tid;
    }
  }
  $rubro_padre = current(taxonomy_get_parents($rubro_auto));
?>
<div class="Content MasBuscados MasBuscados2Col clearfix">
	<div class="CI clearfix">
		<div class="CL">
			<div class="Sombra">
				<div class="Borde clearfix">
					<h2><strong>Lo más buscado</strong></h2>
				</div>
			</div>
			<div class="CajaBuscado">
				<ul>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1%3A3764&f[3]=im_taxonomy_vid_1%3A4044" title="Buscar Volskwagen Gol" rel="search">Volskwagen Gol</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3747&f[3]=im_taxonomy_vid_1:3902" title="Buscar Fiat Uno" rel="search">Fiat Uno</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3756&f[3]=im_taxonomy_vid_1:4167" title="Buscar Peugeot 207" rel="search">Peugeot 207</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3748&f[3]=im_taxonomy_vid_1:3908" title="Buscar Ford Ka" rel="search">Ford Ka</a></li>
					<!-- li><a href="/category/marca-modelo-autos/renault/duster" title="Buscar Renault Duster" rel="search">Renault Duster</a></li -->
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A6326" title="Buscar Motos" rel="search">Motos</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3748&f[3]=im_taxonomy_vid_1:4057" title="Buscar Ford Ecosport" rel="search">Ford Ecosport</a></li>
					<li><a href="/category/marca-modelo-autos/chevrolet/corsa" title="Buscar Chevrolet Corsa" rel="search">Chevrolet Corsa</a></li>
          <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A6328" title="Buscar planes de ahorros" rel="search">Super precios de planes de ahorros</a></li>
				</ul>
			</div>
		</div>
		<div class="CM">
			<div class="Sombra">
				<div class="Borde clearfix">
					<h2><strong>Promociones y financiación</strong></h2>
				</div>
			</div>
			<div class="CajaBuscado">
				<ul>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3757&f[3]=ss_usado_v:Nuevo" title="Promociones Renault" rel="search">Renault</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3756&f[3]=ss_usado_v:Nuevo" title="Promociones Peugeot" rel="search">Peugeot</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3741&f[3]=ss_usado_v:Nuevo" title="Promociones Chevrolet" rel="search">Chevrolet</a></li>
					<li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>&f[2]=im_taxonomy_vid_1:3748&f[3]=ss_usado_v:Nuevo" title="Promociones Ford" rel="search">Ford</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>