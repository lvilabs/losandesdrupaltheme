<?php
$seleccion = $form['seleccion_lugar']['#default_value'];
if(isset($form['#post']['seleccion_lugar'])) {
  $seleccion = $form['#post']['seleccion_lugar'];
}
if(empty($form['provincia']['#value'])) {
  $form['provincia']['#value'] = 'Córdoba';
}
if(empty($form['ciudad']['#value'])) {
  $form['ciudad']['#value'] = 'Córdoba';
}
?>

<div class="currentBuy">
  <h3>Estás comprando:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'ficha_aviso_120_90_sc', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
      <h2><?php print $form['nodo']['#title']; ?></h2>
      <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
    <div class="calculadora clear">
      <input type="hidden" id="precio_venta" value="<?php print $form['nodo']['#precio']; ?>">
      <input type="hidden" id="vendedor_uid" value="<?php print $form['vendedor_uid']['#value']; ?>">
      <div class="pagando"></div>
      <span class="cargando"></span>
      <div class="cuotas final" id="monto-cuotas-div" >
        <div class="contado DN">
          <div class="label-contado">Precio Contado</div>
          <div class="label-pagos">Efectivo, tarjeta de débito o tarjeta de crédito en 1 pago</div>
          <div class="precio-contado"></div>
        </div>
        <div class="tarjeta DN">
          <div class="label-cuotas">
            <div class="cantidad-cuota"></div> 
            <div class="costo-cuota"></div>
          </div>
          <div class="label-ptf"></div>
          <div class="label-cft"></div>
          <div class="label-costo">Costo financiero total</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix avance">
  <div class="item superado"><span class="numero">1</span> Datos de contacto</div>
  <div class="item activo"><span class="numero">2</span> Datos de entrega</div>
  <div class="item"><span class="numero">3</span> Confirmación</div>
</div>


<div class="consigna">
  <?php if(isset($form['seleccion_lugar']['#options']['sucursal']) && count($form['seleccion_lugar']['#options'])>1) : ?>
  Seleccione el lugar de entrega o complete la dirección a donde debe entregarse el pedido.
  <?php elseif(isset($form['seleccion_lugar']['#options']['sucursal'])) : ?>
  Seleccione el lugar de entrega donde retirará el pedido.
  <?php else : ?>
  Complete la dirección a donde debe entregarse el pedido.
  <?php endif; ?>
</div>
<div class="clearfix">
<div class="seleccion_lugar">
  <div class="form-item"><?php print drupal_render($form['seleccion_lugar']); ?></div>
</div>
<div id="datos_direccion" <?php print ($seleccion=='sucursal')?'class="DN"':''; ?> >
  <div class="form-item"><?php print drupal_render($form['direccion']); ?></div>
  <?php if(isset($form['calle_numero'])) : ?>
    <div class="form-item"><?php print drupal_render($form['calle_numero']); ?></div>
  <?php endif; ?>
  <div class="form-item"><?php print drupal_render($form['provincia']); ?></div>
  <div class="form-item"><?php print drupal_render($form['ciudad']); ?></div>
  <div class="form-item"><?php print drupal_render($form['cp']); ?></div>
</div>
<div id="datos_lugares" <?php print ($seleccion!='sucursal')?'class="DN"':''; ?>>
  <div class="form-item"><?php print drupal_render($form['lugar']); ?></div>
</div>
<?php if(isset($form['talle'])) : ?>
<div class="form-item"><?php print drupal_render($form['talle']); ?></div>
<?php endif; ?>
<div class="form-item"><?php print drupal_render($form['cantidad']); ?></div>
<div class="form-item"><?php print drupal_render($form['cupon_descuento']); ?></div>
<div class="form-item"><?php print drupal_render($form['submit']); ?></div>
<a href="<?php print url('comprar/'.$form['nodo']['#nid'].'/datos/'.$form['venta']['#venta_id']); ?>">Volver</a>
</div>
<?php
unset($form['seleccion_lugar']);
unset($form['horario']);
print drupal_render_children($form);