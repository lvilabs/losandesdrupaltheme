<div class="currentBuy">
  <h3>Su compra:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'logo_130_105', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
      <h2><?php print $form['nodo']['#title']; ?></h2>
      <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
  </div>
</div>
<div class="clearfix">

<div class="alerta">
  El pedido se encuentra en espera de pago.
</div>
<!--para cuando este ok el pago-->
<div class="mensajeOk">
  En cuanto el pago se acredite en la cuenta del vendedor, le llegará un email con esa notificación.
</div>
<!--fin pago -->
</div>
<?php
print drupal_render_children($form);