<?php
/**
 * @file views-bulk-operations-table.tpl.php
 * Template to display a VBO as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
if($view->name == 'listado_avisos_admin') {
  if($view->current_display == 'page_1') {
    include dirname(__FILE__).'/includes/views-view-bulk--listado-avisos-admin--page-1.tpl.php';
  } elseif($view->current_display == 'page_2') {
    include dirname(__FILE__).'/includes/views-view-bulk--listado-avisos-admin--page-2.tpl.php';
  }
} else if($view->name == 'contactos_recibidos') {
  if($view->current_display == 'page_1') {
    include dirname(__FILE__).'/includes/views-view-bulk--contactos-recibidos--page-1.tpl.php';
  } elseif($view->current_display == 'page_3') {
    include dirname(__FILE__).'/includes/views-view-bulk--contactos-recibidos--page-3.tpl.php';
  }
} else {
  echo 'Can\'t find an appropiate template for this view.';
}
