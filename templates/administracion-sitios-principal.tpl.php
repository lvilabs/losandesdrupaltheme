<?php global $user; $usuario = user_load($user->uid); ?>
<div class="main">
  <section id="inicio" class="section inicio">
    <div class="row">
      <div class="mobile-12 desk-7 columns info">
        <h1 data-sr="enter bottom over 1s and move 110px wait 0.3s">Ahora con Clasificados LaVoz.com.ar</h1>
        <h2 data-sr="enter top over 3s wait 0.5s">podés armar  <span>tu propio sitio web en pocos minutos.</span></h2>
       </div>
      <div class="mobile-12 desk-5 columns">
        <img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/monitor.png">
      </div>
    </div>
  </section>
  <section id="beneficios" class="section beneficios">
    <div class="row">
      <div data-sr="enter bottom over 1s and move 110px wait 0.3s" class="mobile-12 desk-3 columns">
        <div class="circulo"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/icon1.png"></div>
          <h2>Tu sitio Autogestionable</h2>
          <p>En pocos pasos, diseñá tu propio sitio</p>
        </div>
        <div data-sr="enter bottom over 1s and move 65px" class="mobile-12 desk-3 columns">
          <div class="circulo"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/icon2.png"></div>
          <h2>Tus avisos de ClasificadosLaVoz.com.ar en tu sitio</h2>
          <p>Todos tus avisos de ClasificadosLaVoz.com.ar se publicarán en tu sitio web</p>
        </div>
        <div data-sr="enter right wait 0.5s" class="mobile-12 desk-3 columns">
          <div class="circulo"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/icon3.png"></div>
          <h2>Tu micrositio en ClasificadosLaVoz.com.ar</h2>
          <p>Mejorá la presencia de tu negocio en Clasificadoslavoz.com.ar con un micrositio personalizado</p>
        </div>
        <div data-sr="enter top over 0.5s and move 140px" class="mobile-12 desk-3 columns">
          <div class="circulo"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/icon4.png"></div>
          <h2>Tu web también en plataforma mobile</h2>
          <p>Podrán navegar tu sitio desde tablets y celulares smartphones</p>
        </div>
    </div>
  </section>
  <section id="ventajas" class="section ventajas">
    <div class="row">
      <div class="mobile-12 desk-12 columns"><h1>Podrás tener una web con todo lo que tu negocio necesita</h1></div>
        <div class="mobile-12 desk-6 columns">
          <ul  data-sr="enter bottom over 1s and move 110px wait 0.3s" class="styled-list">
            <li>Administración de avisos desde ClasificadosLaVoz.com.ar</li>
            <li>Recibí todas tus consultas en tu bandeja de contactos de <br>ClasificadosLavoz.com.ar</li>
            <li>Fichas completas de cada producto</li>
            <li>Página principal con Avisos destacados</li>
            <li>Listado de avisos con Filtrado inteligente</li>
            <li>Formulario de contacto</li>
             <li>Integración con redes sociales</li>
            <li>Diseños adaptables a celulares y tablets</li>
          </ul>
        </div>
        <div class="mobile-12 desk-6 columns"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/tienda.png"></div>
    </div>
  </section>
  <section id="colores" class="section colores">
    <div class="row">
      <div class="wrapp-div">
        <div class="mobile-12 desk-6 columns">
          <h1>Elegí el diseño que mejor <br>se adapte a tu negocio.</h1>
          <p>Personalizá tu sitio web eligiendo la plantilla que más te guste
          para destacarte. No es necesario que tengas conocimientos en
          diseño ni programación, en pocos minutos podrás visualizar el 
          sitio que hayas armado para tu negocio.</p>
        </div>
        <div  class="mobile-12 desk-6 columns">
          <img  data-sr="enter top over 0.5s and move 140px" src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/colores1.png">
        </div>
      </div>
      <div class="wrapp-div">
            <div class="mobile-12 desk-6 columns">
               <img data-sr="enter bottom over 1s and move 65px" src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/colores2.png">
            </div>
            <div class="mobile-12 desk-6 columns">
               <h1>Dale color a tu web.</h1>
          <p>Una vez seleccionado un modelo, podrás elegir de 
          la paleta de colores la combinación que más se 
          relacione con tu marca y así diferenciarte de tu 
          competencia.</p>
            </div>
      </div>
      <div class="wrapp-div">
        <div class="mobile-12 desk-6 columns">
          <h1>Ingresá tus datos.</h1>
          <p>Agregá la información de tu comercio y personalizá tu web <br>
          ingresando todos los enlaces que ya tenés.
          Podés cargar tu logo, ingresar formularios de consulta, 
          personalizar tu propio menú de navegación, agregar links
           a redes sociales y mucho más.</p>
        </div>
        <div class="mobile-12 desk-6 columns">
          <img  data-sr="enter top over 0.5s and move 140px" src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/colores3.png">
        </div>
      </div>
    </div>
  </section>
  
  <section id="planes" class="section planes">
    <div class="row">
      <div class="mobile-12 desk-12 columns">
        <h1 data-sr="enter top over 3s wait 0.5s">Descubrí las diferentes opciones para <br>
        tener tu sitio desde $300 el mantenimiento mensual.</h1>
        <h2  data-sr="enter bottom over 1s and move 110px wait 0.3s">Probá los diseños disponibles <br>
        y en pocos minutos podrás ver cómo queda la web de tu inmobiliaria.</h2>
        <table>
          <tr>
            <th class="no-bg"></th>
            <th>Micrositio en <br> Clasificados La Voz</th>
            <th>Tu Web Autogestionale <br> Subdomimo Clasificados</th>
            <th>Tu Web + mobile <br> Autogestionable Dominio propio</th>
            <th>Una Web a tu <br> Medida + mobile</th> 
          </tr>
          <tr>
            <td>
              <div class="input-group">Micrositio en <br> Clasificados LaVoz.com.ar</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a tener una mejor presencia dentro de Clasificados LaVoz.com.ar con tu micrositio personalizado con más información institucional para diferenciarte de la competencia.">+</span>
            </td>
            <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
            <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
            <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
            <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Republicación automática <br> de tus avisos en <br> Clasificados LaVoz.com.ar</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Tus avisos se republicarán automáticamente una vez por día.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Información institucional</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Quiénes somos, sección de Contáctenos.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Subdominio Sitios de La Voz</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Podés tener tu diseño con una dirección web dentro de nuestros servidores. Por ejemplo, tuempresa.sitioslavoz.com.ar.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Dominio Propio<br> de tu empresa</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Podés tener tu diseño con tu dirección web propia Por ejemplo, www.tuempresa.com.ar.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Contenido Dinámico</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a tener los mismos filtros dinámicos que Clasificados dentro de tu sitio.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Home con avisos<br>Destacados</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a poder elegir qué avisos se muestran destacados en tu página de inicio.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Beneficios en Banneri <br> <a href="http://www.lavozglobal.com.ar/banneri/" target="_blanck" style="color:#2980B9;">(link)</a></div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Querés promocionar y posicionar tu sitio? Vas a tener condiciones especiales para contratar los servicios de Banneri y comunicar tu sitio en la red de La Voz y en Google.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Adaptación a mobile</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Tu sitio tendrá una versión mobile, tanto para celulares como para tabletas.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></td>
          </tr>
          <tr>
             <td>
              <div class="input-group">Migración de dominio actual<br> y servicio de mail</div>
              <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Si ya tenés tu sitio web y querés usar nuestro sistema, podemos migrar tu sitio y tus mail a nuestro sistema.">+</span>
             </td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></td>
             <td class="icon">Opcional</td>
             <td class="icon">Opcional</td>
          </tr>
          <!-- <tr>
             <td><strong>Alta inicial</strong></td>
             <td class="icon"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/lineIcon.jpg"></td>
             <td class="icon"><strong>$1500</strong></td>
             <td class="icon"><strong>$3000</strong></td>
             <td rowspan="2" class="icon"><strong>Variable</strong></td>
          </tr>
          <tr>
             <td><strong>Mantenimiento<br> mensual</strong></td>
             <td class="icon"><strong>$300</strong></td>
             <td class="icon"><strong>$600</strong></td>
             <td class="icon"><strong>$700</strong></td>
             
          </tr> -->
        </table>
        <?php if($usuario->uid != 0 && $usuario->profile_crear_sitio_web) { ?>
          <a id="accion-button" class="accion-button" href="/administrar/sitio/editor" data-sr="enter top over 3s wait 0.5s"><p>PROBÁ Y ARMÁ TU SITIO AHORA</p></a>
          <p>Elegí algunos de los paquetes disponibles y comunícate a clasificadosweb@lavozdelinterior.com.ar o al 4757145 para que podamos asesorarte y gestionar el alta del servicio.</p>
        <?php } else { ?>
          <!-- <a id="accion-button" class="accion-button inline" href="#content-form" data-sr="enter top over 3s wait 0.5s"><p>CONTACTANOS</p></a> -->
          <p>Elegí algunos de los paquetes disponibles y comunícate a clasificadosweb@lavozdelinterior.com.ar o al 4757145 para que podamos asesorarte y gestionar el alta del servicio.</p>
          <div id="content-form">
             <div class="message-system"><?php print $messages; ?></div>
            <?php 
              module_load_include('inc', 'contact', 'contact.pages');
              $content = drupal_get_form('contact_mail_page'); 
              print $content;
            ?>
          </div>
        <?php } ?>
      </div>
    </div>
    <footer>
      <div class="row">
        <div class="mobile-12 desk-6 columns hide-sm-small">
          <a href="http://www.lavoz.com.ar/lvilab/"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/logo-lavoz.png"></a><a href="http://www.mobydigital.com/es/">&nbsp;&nbsp;&nbsp;<img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/logo-mobydigital.png"></a>
        </div>
      </div>
    </footer>
  </section>
</div>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src='/sites/all/modules/custom_new/administracion_sitios/js/javascripts/scrollReveal.js'></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/principal.js"></script> 