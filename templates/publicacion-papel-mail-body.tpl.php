<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="620" cellspacing="0" cellpadding="0" border="1" style="width:465.0pt;background:white;border-collapse:collapse;border:none">
          <tbody>
            <tr>
              <td style="border:none;background:transparent;padding:0cm 0cm 0cm 0cm" colspan="2">
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">¡Felicitaciones!
                <u></u><u></u></span></b></p>
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!usuario, la versión web de tu aviso contratado en la receptoría se publicó correctamente.<u></u><u></u></span></b></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Información del aviso<u></u><u></u></span></p>
                <table width="95%" cellpadding="0" border="0" style="width:95.04%">
                  <tbody>
                    <tr>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Título<u></u><u></u></span></p>
                      </td>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Tipo de aviso<u></u><u></u></span></p>
                      </td>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Finaliza<u></u><u></u></span></p>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!titulo_aviso<u></u><u></u></span></p>
                      </td>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Aviso !tipo_aviso<u></u><u></u></span></p>
                      </td>
                      <td style="padding:.75pt .75pt .75pt .75pt">
                        <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!fecha_fin<u></u><u></u></span></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><br>
                </span><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Si querés editar, agregarle fotos o destacar tu aviso, ingresá al siguiente link y termina de completar tus datos.<u></u><u></u></span></p>
                <p><a target="_blank" href="!perfil_url"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!perfil_url</span></a><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">´<u></u><u></u></span></p>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Si ya eres usuario del sitio,
                </span><a target="_blank" href="!link_login"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">logueate</span></a><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> para administrarlo.<u></u><u></u></span></p>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">El aviso puede tardar hasta 2 horas en aparecer en la lista de resultados, y aparecerá al final de los resultados de búsqueda durante 30 días.<br>
                <br>
                <b>Si querés sumar fotos, mayor duración y ampliar tus posibilidades de ventas, no te olvides de destacar tu aviso para aparecer en los primeros lugares de las búsquedas. Cuanto más destacados, más chances tenés.</b><br>
                <br>
                <u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>
        <p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><u></u><u></u></span></p>
      </td>
    </tr>
  </tbody>
</table>