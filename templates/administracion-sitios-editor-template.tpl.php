<?php
  global $user;
  $datos_config = administracion_sitios_get_all($user->uid);
    
  //Seccion
  $array_seccion = explode('?sec=', $_SERVER['REQUEST_URI']); 
  $seccion = '';
  if(isset($array_seccion[1]))
    $seccion = $array_seccion[1];
?>

<?php print drupal_render($form); ?>
  
<!-- INICIO MENU ADMINISTRACION -->  
  
  <?php 
    switch($seccion){
      case 'fuentes':
  ?>
  <div>
    <iframe class="iframeWizard" id="iframeFuentes" name="" style="overflow:auto;z-index:9;width:100%;height:675px;position:relative; top:0; left:0; border: none; border-bottom: 7px solid #666;" src="/administrar/sitio/fuentes" scrolling="no"></iframe>
  </div>
  <?php 
        break;
      case 'multimedia':
  ?>
  <div>
    <iframe class="iframeWizard" id="iframeMultimedia" name="" style="overflow:auto;z-index:9;width:100%;height:788px;position:relative; top:0; left:0; border: none; border-bottom: 7px solid #666;" src="/administrar/sitio/multimedia" scrolling="no"></iframe>
  </div>
  <?php 
        break;
      case 'contactos':
  ?>
  <div>
    <iframe class="iframeWizard" id="iframeContactos" name="" style="overflow:auto;z-index:9;width:100%;height:675px;position:relative; top:0; left:0; border: none; border-bottom: 7px solid #666;" src="/administrar/sitio/contactos" scrolling="no"></iframe>
  </div>
  <?php 
        break;
      case 'alta':
  ?>
  <div>
    <iframe class="iframeWizard" id="iframeAlta" name="" style="overflow:auto;z-index:9;width:100%;height:737px;position:relative; top:0; left:0; border: none; border-bottom: 7px solid #666;" src="/administrar/sitio/alta" scrolling="no"></iframe>
  </div>
  <?php 
        break;
      default:
  ?>
  <div>
    <iframe class="iframeWizard" id="iframePlantilla" name="" style="overflow:auto;z-index:9;width:100%;height:675px;position:relative; top:0; left:0; border: none; border-bottom: 7px solid #666;" src="/administrar/sitio/plantillas" scrolling="no"></iframe>
  </div>  
  <?php
        break;
    }
  ?>
  <img class="mensaje-preview" src="/sites/all/modules/custom_new/administracion_sitios/img/texto_prevista.png" />
  <a href="#preview" alt="Previsualizá tu diseño" class="btn-preview"></a>
  <div id="loadingWizard"><img class="loading" src="/sites/all/modules/custom_new/administracion_sitios/img/ajax-loader.gif" /></div>
<!-- FIN MENU ADMINISTRACION -->  

<!-- INICIO CONTENIDO TEMPLATE --> 
<?php if(isset($datos_config['template']) && !empty($datos_config['template'])) { ?>
  <input type="hidden" value="<?php print $datos_config['template']; ?>" class="select-template" />
  <div id="content-site" style="box-sizing:border-box; width: 100%;">       
    <div id="splitContainer">
      <!-- <div id="preview"> -->
        <div id="rightSplitter">
          <!-- <div class="select-agent">
            <div class="user-web selected" id="user-web-click" title="Versión Web"></div>
            <div class="user-mobile" id="user-mobile-click" title="Versión Movil"></div>
            <div class="user-cabezal" id="user-cabezal-click" title="Cabezal Microsito Clasificados La Voz"></div>
          </div> -->
          <!-- <div id="contentMobile"> -->
            <div id="contetIframe">                
              <iframe id="preview" name="" style="overflow:auto;z-index:9;width:100%;position:relative;top:0;left:0;border: none;" src="/administrar/sitio/template/<?php print $datos_config['template']; ?>"></iframe>
            </div>
            <div id="loadingPreview"><img class="loading prev" src="/sites/all/modules/custom_new/administracion_sitios/img/ajax-loader.gif" /></div>
          <!-- </div> -->
        </div>
      <!-- </div> -->
    </div>                 
  </div>
  <!-- boton top -->
  <div class="scroll-top-wrapper">
    <span class="scroll-top-inner">
      <i class="fa fa-2x fa-arrow-circle-up"></i>
    </span>
  </div>
  <!-- boton top -->
<?php } ?>
<!-- FIN CONTENIDO TEMPLATE -->    
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/main.js"></script>