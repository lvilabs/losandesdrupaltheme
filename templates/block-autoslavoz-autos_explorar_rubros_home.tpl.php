<?php
$array_rubros = clasificados_rubros_obtener_rubros('aviso-auto');
foreach($array_rubros as $rubro){
  if($rubro->title == 'Accesorios y Repuestos')
    $rubro_repuestos = $rubro->tid;
  if($rubro->title == 'Planes de Ahorro')
    $rubro_plan = $rubro->tid;
  if($rubro->title == 'Autos')
    $rubro_auto = $rubro->tid;
  if($rubro->title == '4x4, 4x2 y todo terreno')
    $rubro_camionetas = $rubro->tid;
  if($rubro->title == 'Motos, cuadriciclos y náutica')
    $rubro_motos = $rubro->tid;
  if($rubro->title == 'Utilitarios, pick-up y camiones')
    $rubro_utilitarios = $rubro->tid;
}
$rubro_padre = current(taxonomy_get_parents($rubro_auto));
?>
<div class="explorar">
  <h2>Explorar Rubros</h2>
  <ul>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_auto; ?>" title="Explorar autos">Autos</a></li>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_camionetas; ?>" title="Explorar 4x4, 4x2 y todo terreno">4x4, 4x2 y todo terreno</a></li>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_repuestos; ?>" title="Explorar accesorios y repuestos">Accesorios y repuestos</a></li>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_plan; ?>" title="Explorar motos, cuadriciclos y náutica">Motos, cuadriciclos y náutica</a></li>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_plan; ?>" title="Explorar planes de Ahorro">Planes de Ahorro</a></li>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro_plan; ?>" title="Explorar utilitarios, pick-up y camiones">Utilitarios, pick-up y camiones</a></li>
  </ul>
</div>