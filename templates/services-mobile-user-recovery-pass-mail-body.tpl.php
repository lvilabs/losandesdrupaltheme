<p>!usermail,</p>
<p>Solicitaste restablecer tu contraseña para tu cuenta en Clasificados Los Andes.</p>
<p>Crea una contraseña nueva haciendo clic en este enlace o copiandolo y
pegandolo en su navegador:</p>
<p><a href="!link_recovery_pass">!link_recovery_pass</a></p>
<p>Este enlace puede ser utilizado una sola vez, expira después de un día y no
pasará nada si no se utiliza.</p>
