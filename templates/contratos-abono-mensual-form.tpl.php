<?php
global $user;

$espacios_disponibles = clasificados_obtener_espacios_disponibles();
$show_espacios = array();

//nos aseguramos que los espacios aparezcan en el orden fijado por field_espacio_orden_pub
foreach ($espacios_disponibles as $espacio_nid => $espacio_title) {
  $show_espacios[$espacio_nid] = $espacio_title;
}

$usuario_id = $user->uid;
if(module_exists('usuarios_colaboradores') && in_array('colaborador inmobiliaria', $user->roles)){
  $permisos = usuarios_colaboradores_get_permisos_user($user->uid);
  if(in_array('Utilizar espacios', $permisos)){
    $user_principal = usuarios_colaboradores_get_usuario_principal($user->uid);
    $usuario_id = $user_principal->uid;
  }
}
//obtenemos las cantidades disponibles y seteamos a cero lo que no esta disponible
$espacios = _publicacion_avisos_espacios_adquiridos_tipo($usuario_id);
$espacios_clin = array();
foreach($show_espacios as $espacio_nid => $label) {
  $espacio = array();
  if(isset($espacios[$espacio_nid])) {
    $espacio = $espacios[$espacio_nid];
    $espacio['usados'] = $espacio['total'] - $espacio['cantidad'];
  } else {
    $espacio['cantidad'] = 0;
    $espacio['usados'] = 0;
    $espacio['total'] = 0;
  }
  if(empty($espacio['cantidad'])) $espacio['cantidad'] = '-';
  if(empty($espacio['usados']))   $espacio['usados'] = '-';
  $espacios[$espacio_nid] = $espacio;
  if(isset($espacios[$espacio_nid]['cantidad_clin']) && $espacios[$espacio_nid]['cantidad_clin'] > 0)
    $espacios_clin[$espacio_nid] = array('label' => $show_espacios[$espacio_nid], 'cantidad' => $espacios[$espacio_nid]['cantidad_clin']);
}

//Solo mostrar los espacios que el usuario tenga asignado.
foreach($espacios as $espacio_nid => $espacio_data) {
  if($espacio_data['total'] == 0) {
    unset($show_espacios[$espacio_nid]);
  }
}

// Necesario para includes/detalle-espacios-usuario.tpl.php
$rubros = clasificados_rubros_obtener_rubros();

//obtener cantidad de espacios reusables y cantidad de utilizados en el día
$espacios_reusables = array();
if(in_array($user->uid, array(134061, 194264))) {
  $espacios_reusables = _publicacion_avisos_espacios_adquiridos_reusables($usuario_id);
  $show_espacios_reusables = array();
  foreach($espacios_reusables as $espacio_re_nid => $espacio_re_data) {
    $espacio_re_disponibles = $espacio_re_data['total'] - $espacio_re_data['cantidad'];
    $show_espacios_reusables[$espacio_re_nid] = array('label' => $show_espacios[$espacio_re_nid], 
                                                      'disponibles' => $espacio_re_disponibles, 
                                                      'usados' => $espacio_re_data['cantidad'], 
                                                      'total' => $espacio_re_data['total']);
  }
  krsort($show_espacios_reusables);
}

$i = 1;
?>
<div class="Content Estado clearfix">
  <div class="views_view view-compras">
    <div class="bloque-left clearfix">
      <div><h3>Espacios disponibles</h3></div>
<?php if(count($show_espacios) == 0): ?>
      <div class="row odd clearfix">
        <div>No tiene espacios asignados.</div>
      </div>
<?php else: ?>
      <div class="row TitEstado even clearfix">
        <div class="espacios header-left espacios-nombre">Tipo de Espacio</div>
        <div class="espacios header espacios-disponible">Disponibles</div>
        <div class="espacios header espacios-disponible"><?php (empty($espacios_reusables))? print 'Usados' : print 'Aplicados'; ?></div>
        <div class="espacios header espacios-total">Total</div>
      </div>
<?php endif; ?>
<?php foreach($show_espacios as $espacio_nid => $label): ?>
      <div class="row <?php print ($i%2) ? 'even' : 'odd'; $i++; ?> clearfix">
        <div class="espacios header-left espacios-nombre"><?php print $label; ?></div>
        <?php
          $cantidad = $espacios[$espacio_nid]['cantidad'];
          $usados = $espacios[$espacio_nid]['usados'];
          $usado_dia = 0;
          if(is_numeric($show_espacios_reusables[$espacio_nid]['disponibles']) && (int)$show_espacios_reusables[$espacio_nid]['disponibles'] < (int)$cantidad) {
            $usado_dia = 1;
            $cantidad = '<p class="msg-espacio">'.$show_espacios_reusables[$espacio_nid]['disponibles'].'</p>';
            if($show_espacios_reusables[$espacio_nid]['disponibles'] == 0)
              $usado_dia_mensaje = '* No tienes espacios '.$label.' disponibles ya que superaste las publicaciones permitidas en el día. Debes esperar hasta mañana para poder utilizarlos.';
            else
              $usado_dia_mensaje = '* Los espacios Disponibles corresponden a las publicaciones restantes con el destaque '.$label.' que tenés disponibles en el día de la fecha. Tenés que esperar hasta mañana para poder utilizar todos los espacios de tu cuenta. <a href="#">Más información</a>';
          }
        ?>
        <div class="espacios espacios-disponible"><?php print $cantidad; ?></div>
        <div class="espacios espacios-disponible"><?php print $usados; ?></div>
        <div class="espacios espacios-total"><?php print $espacios[$espacio_nid]['total']; ?></div>
        <div class="VerMas" data-detalle-id="<?php print 'detalle-' . $espacio_nid; ?>"><a></a></div>
      </div>
      <?php if($usado_dia) : ?>
      <p class="msg-importante"><?php print $usado_dia_mensaje; ?></p>
      <?php endif; ?>
<?php endforeach; ?>
      <?php if(clasificados_contratos_con_basicos_ilimitados($user->uid)): ?>
        <div class="row odd clearfix">
        <div class="espacios header-left espacios-nombre">Basico</div>
        <div class="espacios espacios-disponible">Ilimitados</div>
        <div class="espacios espacios-disponible">-</div>
        <div class="espacios espacios-total">-</div>
      </div>
      <?php endif; ?>
    </div>
    <div class="bloque-right">
<?php foreach($show_espacios as $espacio_nid => $label): ?>
      <div id="detalle-<?php print $espacio_nid; ?>" class="oculto GloboDetalle">
      <span class="punta"></span>
        <div class="Gris">
          <div><h3>Detalle de <i><?php print $label; ?></i> disponibles</h3></div>
<?php
        $espacios = _publicacion_avisos_espacios_adquiridos_rubro($usuario_id, $espacio_nid);
        include dirname(__FILE__).'/includes/detalle-espacios-usuario.tpl.php';
?>
        </div>
      </div>
<?php endforeach; ?>
    </div>

<?php if(!empty($espacios_clin)): ?>
    <div class="bloque-left clearfix">
      <div><h3>Espacios utilizados por avisos creados en Clin CPI</h3></div>
      <p class="msg-importante">Las siguientes cantidades representan a destaques de Clasificados La Voz que se utilizaron sobre avisos creados en el sitio de Clin CPI y figuran como usados en la tabla de Espacios disponibles.</p>
      <div class="row TitEstado even clearfix">
        <div class="espacios header-left espacios-nombre">Tipo de Espacio</div>
        <div class="espacios header espacios-disponible">Cantidad</div>
      </div>
    <?php foreach($espacios_clin as $espacio_nid => $espacio): ?>
      <div class="row <?php print ($i%2) ? 'even' : 'odd'; $i++; ?> clearfix">
        <div class="espacios header-left espacios-nombre"><?php print $espacio['label']; ?></div>
        <div class="espacios espacios-disponible"><?php print $espacio['cantidad']; ?></div>
      </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>
    
<?php if(!empty($espacios_reusables)): ?>
    <div class="msg-estado-cuenta">
      <h3>¿Cómo leer el nuevo estado de cuentas?</h3>
      <p>* La diferencia corresponde a las publicaciones permitidas de espacios en el día. Debes esperar hasta mañana para poder utilizar todos.</p>
    </div>
<?php endif; ?>
    
<?php
$creditos_papel_concesionaria = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID);
$creditos_papel_inmobiliaria = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID);
$creditos_papel_comercio = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID);
$creditos_papel_clasifoto = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_CLASIFOTO_NID);
if(!empty($creditos_papel_concesionaria) || !empty($creditos_papel_inmobiliaria) || !empty($creditos_papel_comercio) || !empty($creditos_papel_clasifoto)) :
?>
    <div class="separador"></div>
    <div class="bloque-left clearfix webpapel">
      <div class="">
        <h3>Mejoras (Créditos Web Papel)</h3>
      </div>
      <div class="row TitEstado even clearfix">
          <div class="espacios header-left espacios-nombre">Tipo de Mejora</div>
          <div class="espacios header espacios-disponible">Disponibles</div>
          <div class="espacios header espacios-usados-mes">Usados Mes Actual</div>
      </div>
      <?php if(!empty($creditos_papel_concesionaria)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Concesionaria</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_concesionaria[0]['cantidad']; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_concesionaria[0]['usados_mes']; ?></div>
      </div>
      <?php endif; ?>
      <?php if(!empty($creditos_papel_inmobiliaria)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Inmobiliaria</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_inmobiliaria[0]['cantidad']; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_inmobiliaria[0]['usados_mes']; ?></div>
      </div>
      <?php endif; ?>
      <?php if(!empty($creditos_papel_comercio)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Comercio</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_comercio[0]['cantidad']; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_comercio[0]['usados_mes']; ?></div>
      </div>
      <?php endif; ?>
      <?php if(!empty($creditos_papel_clasifoto)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Clasifoto</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_clasifoto[0]['cantidad']; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_clasifoto[0]['usados_mes']; ?></div>
      </div>
      <?php endif; ?>
    </div>
<?php endif; ?>
  </div>
  <div class="clear0">&nbsp;</div>
</div>