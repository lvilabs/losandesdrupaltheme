<?php $gmapkey =  variable_get('googlemap_api_key', 0); ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php print $gmapkey; ?>"></script>
<?php
  global $user;
  $edicion = in_array("edit", arg());
  $tipos_avisos_nodetypes = sitio_local_obtener_tipos_avisos_node_types();
  $segmento_vid = clasificados_node_obtener_segmento_vocabulary_id($form['#node']);

  //Si el Tipo de Aviso tiene Rango de Cuotas entonces hay que procesar los valores ingresados.
  if(isset($form['field_aviso_rango_cuotas'])) {
    $rangos = $form['field_aviso_rango_cuotas'][0]['#default_value']['value'];

    $rangos_matches = array();
    if(preg_match_all('/Cuota\s+([0-9]+)\s+a\s+([0-9]+)\s+\\$\s+([0-9\.]+)/um', $rangos, $rangos_matches)) {
      unset($rangos_matches[0]);
    } else {
      $rangos_matches = array();
    }
  }
  
  $show_informacion_secundaria = FALSE;
?>
<div class="Content CrearAnuncio clearfix">
  <?php 
    if($user->uid != 0) {
      if($edicion) {
  ?>
      <h3>Editar Aviso</h3>
  <?php
      } else if(arg(2)=='clone') {
  ?>
      <h3>Copiar Aviso</h3>
  <?php
      }
   } else {
  ?>
    
  <?php
  }
  ?>
  <div class="blanco">
    <?php if($user->uid != 0 && !$edicion){ ?>
      <div class="pasosbreadcrumb clearfix aviso">
        <div class="pasos clearfix">
          <div class="paso1 active">Datos del aviso</div>  
          <div class="paso2"><label>Publicación</label></div>  
        </div>
      </div>
    <?php } ?>
    <div class="avisolegal">Los campos marcados con un asterisco (<span style="color:red;">*</span>) son obligatorios.</div>
    <div class="FormCrear"> 
    </div>
    <div class="FormRegistro ContactoAutos Avisos">
      <fieldset>
        <div class="Row clearfix">
          <div id="tipos-contenido-wrapper" class="form-item tipos-contenido-wrapper">
          <label>Categoría: <span style="color:red;">*</span> </label>
          <?php
            if($edicion || arg(2)=="clone"){
              $tipo_rubro = str_replace('_', '-', $form['#node']->type);
              $rubro = clasificados_rubros_node_obtener_rubro_term($form['#node']);
          ?>
              <input type="text" value="<?php print htmlentities($rubro->name, ENT_QUOTES, 'utf-8'); ?>" readonly="readonly" class="readonly-text" style="width: 172px;" />
          <?php
            } else {
              $tipo_rubro = arg(2);
              $rubros = clasificados_rubros_obtener_rubros(arg(2));
?>
              <select name="tipos-contenido" class="" id="select-tipos-contenido">
              <?php foreach($rubros as $rubro) : ?>
                <?php if(!in_array($rubro->aviso_node_type, variable_get('clasificados_rubros_ocultos', array()))) : ?>
                  <option value="/node/add/<?php print str_replace('_', '-', $rubro->aviso_node_type); ?>" <?php if ($rubro->aviso_node_type == $form['#node']->type) print 'selected="selected"'; ?>><?php print htmlentities($rubro->title, ENT_QUOTES, 'utf-8'); ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
              </select>
<?php
            }
?>
          <div class="description" style="display: none;"><span class="Ingresa">Cargando formulario para <strong>tipo-contenido</strong>. Espere por favor...</span></div>
          </div>
        </div>
        
<?php if(isset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID])): ?>
        <div class="Row clearfix">


        <?php
          // Quitamos rubros via codigo
          switch($tipo_rubro) {
            /* case 'aviso-rural':
              unset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#options'][6001]);
              unset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#options'][6008]);
              unset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#options'][6015]);
              unset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#options'][6599]);
              break;
            case 'aviso-producto':
              unset($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#options'][8723]);
              foreach($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#childinfo'] as $key => $value) {
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#childinfo'][$key] = 0;
              }
              break; */
            case 'aviso-servicio':
              // $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID] = rionegro_principal_clear_rubros('aviso-servicio', $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]);
              
              if($_GET['res'] == 'empleos'){
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#default_value'][] = 13347;
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#value'][] = 13347;
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchy']['#value']->lineage[1] = 13347;
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchy']['#value']->lineage[2] = 'label_2';
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#default_value'] = 13347;
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#value'] = 13347;
                $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#return_value'][] = 13347;
              }
              /*
                if($_GET['rubro'] == 'solidaridad'){
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#default_value'][] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#value'][] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchy']['#value']->lineage[1] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#default_value'] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['hierarchical_select']['selects'][1]['#value'] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                    $form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]['#return_value'][] = CLASIFICADOS_SOLIDARIDAD_TAXONOMY_TID;
                }
              */
              break;
          }

          print drupal_render($form['taxonomy'][CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID]);
        ?>
        </div>
<?php if(isset($form['field_aviso_numero_matricula'])): ?>
        <div id="matricula-profesional" class="DN">
          <div class="msg-alojamientos-form">
            <span class="red">Evite rechazos en sus aviso</span>
            <br><br>
            <strong>Estimado usuario:</strong> Solicitamos a quienes sean <strong>"Profesionales"</strong> y deseen cargar su aviso en el sub- rubro de <strong>"Oficios y Profesiones"</strong> que deben ingresar su concreta identificación <strong>nombre y apellido</strong>, en el campo descripción, y <strong>número de matrícula</strong> al cargar su aviso, sino el mismo quedará rechazado en nuestro proceso de moderación sin ser publicado en nuestro sitio web.
          </div>
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['field_aviso_numero_matricula']); 
            ?>
          </div>
        </div>
<?php endif; ?>
<?php endif; ?>

      </fieldset>
      <fieldset>
        <div class="AyudaAlta DescripcionMix">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul><li><strong>Agregá toda la información extra</strong> sobre tu aviso. Tené en cuenta que las primeras palabras de la descripción son las que aparecen en la lista de los resultados, por lo que aprovechá para poner alguna característica llamadora.
</li></ul>
          </div>
        </div>
        <h3>Información Principal</h3>
<?php if(isset($form['field_aviso_operacion']) && $tipo_rubro != 'aviso-servicio'): ?>
          <div class="Row clearfix">
            <?php
              print drupal_render($form['field_aviso_operacion']); 
            ?>
          </div>
<?php endif; ?>
        <div class="Row clearfix">
          <?php 
            $form['title']['#title'] = 'Título del Aviso';
            $form['title']['#description'] = '<span class="Ingresa">Máximo de 80 caracteres.</span>';
            print drupal_render($form['title']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            $form['body_field']['body']['#rows'] = 5;
            $form['body_field']['body']['#description'] = '<span class="Ingresa">Ingrese todos los datos extras que desee agregar a su aviso.</span>';
            unset($form['body_field']['format'][1]);
            unset($form['body_field']['format'][5]);
            unset($form['body_field']['format'][4]['#description']);
            unset($form['body_field']['format']['#type']);
            unset($form['body_field']['format']['#title']);
            print drupal_render($form['body_field']); 
          ?>
        </div>
<?php if(isset($form['taxonomy'][CLASIFICADOS_MARCAS_PRODUCTOS_TAXONOMY_VID])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['taxonomy'][CLASIFICADOS_MARCAS_PRODUCTOS_TAXONOMY_VID]);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['taxonomy'][CLASIFICADOS_GENERO_PRODUCTOS_TAXONOMY_VID]);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['taxonomy'][CLASIFICADOS_DEPORTES_PRODUCTOS_TAXONOMY_VID]);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_usado'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_usado']['value']['#title'] = 'Estado';
            print drupal_render($form['field_aviso_usado']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
      <fieldset>
        <h3>Ubicación</h3>
        <div class="Row clearfix">
          <?php 
            if($edicion && !isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS])) {
              $provincia = '';
              $ciudad = '';
              $barrio = '';
              foreach($form['taxonomy'][5]['#default_value'] as $ubicacion){
                if (!clasificados_taxonomy_has_parent($ubicacion)) {
                  $provincia = taxonomy_get_term($ubicacion);
                } elseif(clasificados_taxonomy_es_ciudad($ubicacion)) {
                  $ciudad = taxonomy_get_term($ubicacion);
                } else {
                  $barrio = taxonomy_get_term($ubicacion);
                }
              }
              ?>
              <div class="form-item">
                <label>Provincia, Ciudad y Barrio: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
                <input type="text" value="<?php print $provincia->name; ?>" readonly="readonly" class="readonly-text" />
                <?php if($ciudad->name != ""){ ?>
                  <input type="text" value="<?php print $ciudad->name; ?>" readonly="readonly" class="readonly-text" />
                <?php } ?>
              </div>
            <?php  
            } else {
              $form['taxonomy'][5]['#title'] = 'Provincia y Ciudad';
              print drupal_render($form['taxonomy'][5]); 
            }
          ?>
        </div>
<?php if(isset($form['field_aviso_zona'])): ?>
        <div class="Row clearfix zona">
          <?php 
            print drupal_render($form['field_aviso_zona']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_calle'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_calle_altura'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle_altura']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mapa'])): ?>
        <div class="Boton">
          <input type="button" value="Marcar Dirección en Mapa" class="boton-mapa" onClick="javascript:agregar_mapa();">
        </div>
        <div class="opciones-geomap DN">
          <div>Encontramos mas de una posible dirección, selecciona la más cercana:</div>
        </div>
        <div class="recomendacion DN">
          <strong>Error en la geolocalización de la dirección</strong><br> 
          <span class="reco-message"></span>
        </div>
        <div class="Row clearfix">
          <label for="edit-picture-upload">  
            <?php print ''.$form['field_aviso_mapa']['#title'].': <br>(Opcional)'; ?>
          </label>
          <div id="multimedia_mapas" style="display: block; width: 58%; height: 300px; float: left;"></div>
          <?php 
            unset($form['field_aviso_mapa'][0]['locpick']['instructions']);
            print drupal_render($form['field_aviso_mapa'][0]['locpick']['map']);
          ?>
          <div class="description">
            <span class="Ingresa Mapa"><?php print drupal_render($form['field_aviso_mapa'][0]['locpick']['map_instructions']); ?></span>
            <?php 
            if(isset($form['field_aviso_mapa'][0]['delete_location'])){
              unset($form['field_aviso_mapa'][0]['delete_location']['#title']);
              $form['field_aviso_mapa'][0]['delete_location']['#description'] = "Eliminar ubicación en el mapa.";
              print drupal_render($form['field_aviso_mapa'][0]['delete_location']);
            } 
            ?>
          </div>
          
        </div>
<?php endif; ?>
      </fieldset>
      <fieldset>
        <h3>Forma de Pago</h3>
<?php if(isset($form['field_aviso_precio'])): ?>
		<div class="AyudaAlta">
          <span class="punta"></span>
          		<div class="Texto"> 
            		<ul>
              			<li><strong>Agregá el precio</strong>  y no quedes afuera cuando un usuario hace una búsqueda por Precio. En el caso de que quieras ocultarlo, hacé clic en Ocultar Precio.</li>	
            		</ul>	
                </div>
        </div>         
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_precio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_moneda'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_moneda']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_precio'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_precio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['precio_anterior_venta'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['precio_anterior_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_forma_pago'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_forma_pago']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['disponible_venta'])): ?>
        <div class="Row clearfix">
          <?php 
            unset($form['taxonomy']['tags'][37]['#description']);
            print drupal_render($form['taxonomy']['tags'][37]);
          ?>
        </div>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['disponible_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['cantidad_venta'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['cantidad_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['apedido_venta'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php
            print drupal_render($form['apedido_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['stock_talles'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['stock_talles']);
          ?>
        </div>
<?php endif; ?>
      </fieldset>

<?php if(isset($form['disponible_venta']) && isset($form['envio_andreani']) && $form['envio_andreani']): ?>
      <fieldset>
        <h3>Datos de envío (Aforo)</h3>
        <div class="AyudaAlta">
          <span class="punta"></span>
          <div class="Texto"> 
            <strong>Embalaje</strong>
            <ul>
              <li><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/aforo.jpg" /></li>	
            </ul>	
          </div>
        </div>
        <?php print drupal_render($form['envio_andreani']); ?>
        <?php if(isset($form['aforo_peso'])): ?>
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['aforo_peso']);
            ?>
          </div>
        <?php endif; ?>
        <?php if(isset($form['aforo_largo'])): ?>
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['aforo_largo']);
            ?>
          </div>
        <?php endif; ?>
        <?php if(isset($form['aforo_alto'])): ?>
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['aforo_alto']);
            ?>
          </div>
        <?php endif; ?>
        <?php if(isset($form['aforo_ancho'])): ?>
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['aforo_ancho']);
            ?>
          </div>
        <?php endif; ?>
      </fieldset> 
<?php endif; ?>
      <fieldset>
        <h3>Datos del Vendedor</h3>
        <div class="AyudaAlta completar-whatsapp">
          <span class="tag nuevo">Nuevo</span>
          <div class="Texto">
            <ul>
              <li><strong>¡Sumá tu WhatsApp!</strong>. Aumentá las chances de contacto agregando un teléfono por cada uno de tus avisos.</li>
            </ul>
            <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 </span>0 
            
            <?php if(empty($form['field_aviso_telefono_whatsapp'][0]['#value']['value'])): ?>
              <?php if(empty($user->profile_telefono_whatsapp)): ?>
                <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                15 <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php unset($form['field_aviso_ocultar_whatsapp']); ?>
              <?php else: ?>
                <?php $whatsapp = explode('+549', $user->profile_telefono_whatsapp); ?>
                <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
                <div class="ocultar-wa">
                  <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
                </div>
              <?php endif; ?>
            <?php else: ?>
              <?php $whatsapp = explode('+549', $form['field_aviso_telefono_whatsapp'][0]['#value']['value']); ?>
              <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
              <div class="ocultar-wa">
                <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
              </div>
            <?php endif; ?>
            
          </div>
        </div>
<?php if(isset($form['field_aviso_tel_vendedor'])): ?>
        <div class="Row clearfix telefonoContacto">
          <?php if(empty($form['field_aviso_tel_vendedor'][0]['#value']['value'])): ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" > /
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" > /
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php else: ?>
            <?php 
              $tel_1 = '';
              $tel_2 = '';
              $tel_3 = '';
              $array_tel = explode('/', $form['field_aviso_tel_vendedor'][0]['#value']['value']);
              foreach($array_tel as $key => $num_tel) {
                switch($key) {
                  case 0:
                    $tel_1 = trim($num_tel);
                    break;
                  case 1:
                    $tel_2 = trim($num_tel);
                    break;
                  case 2:
                    $tel_3 = trim($num_tel);
                    break;
                }
              }
            ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" value="<?php print $tel_1; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" value="<?php print $tel_2; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" value="<?php print $tel_3; ?>" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php endif; ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_tel'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_tel']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mail_vendedor'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_mail_vendedor']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_mail'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_mail']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_dormitorios'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_dormitorios']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
<?php if($show_informacion_secundaria): ?>
      <fieldset>
        <h3>Información Secundaria</h3>
      </fieldset>
<?php endif; ?>
      <fieldset>
      <div class="AyudaAlta">
          <span class="punta"></span>
          <?php if(user_is_logged_in()) { ?>
            <!-- :::AYUDA PARA USUARIOS LOGUEADOS:::-->
            
            <?php if($_GET['res'] == 'empleos'){ ?>
              <div class="Texto">
                <ul>
                  <li><strong>Subí el logo de tu empresa.</strong><br />
                    <br />
                    Solo sera visible en el resultado de búsqueda si tiene los siguientes destaques: <br />
                    <br />
                    - <strong>Aviso Estandar</strong><br />
                    - <strong>Aviso Premium</strong><br />
                  </li>
                </ul>
              </div>
            <?php } else { ?>
              <div class="Texto">
                <ul>
                  <li><strong>Subí tu foto</strong> y aumenta las posibilidades de visitas.<br />
                    <br />
                    Dependiendo el tipo de aviso que elijas para publicar tu vehículo, se verá diferente cantidad de imágenes: <br />
                    <br />
                    - <strong>Aviso Estandar:</strong> 6 fotos. <br />
                    - <strong>Aviso Premium:</strong> 12 fotos. <br />
                    - <strong>Aviso Super Premium y Vidriera: </strong> 25 fotos.<br />
                    <br />
                    Si contratas un destaque para el aviso, ten en cuenta que la 1ª foto que cargues es la que se verá en el listado de avisos.
                  </li>
                </ul>
              </div>
            <?php } ?>
          <?php } else { ?>
          <div class="Texto">
            <ul><li>Los usuarios quieren ver avisos con fotos. Subí tu foto y aumentá la cantidad de visitas.<br /><br /><strong> Tu aviso es Gratuito: SÓLO se verán las primeras 3 fotos cargadas.</strong> <br />Para que tu aviso tenga más fotos y más visitas, completá tu registración y destacá tu aviso.</li></ul>
          </div>
          <?php } ?>
        </div>
        <h3>Multimedia</h3>
        <div class="Row clearfix Fotos">
          <?php
            if($_GET['res'] == 'empleos'){
              $form['field_aviso_fotos']['#title'] = 'Logo';
              $form['field_aviso_fotos']['#plup']['dragdrop'] = 0;
              $form['field_aviso_fotos']['#plup']['multiple_queues'] = 0;
              $form['field_aviso_fotos']['#plup']['max_files'] = 1;
            }
            print drupal_render($form['field_aviso_fotos']);
          ?>
        </div>
        <?php if(isset($form['field_aviso_video'])) { ?>
        <div class="Row clearfix AgregarVideo">
          <?php 
            $form['field_aviso_video'][0]['embed']['#description'] = '<span class="Ingresa">Ingresa la URL de YouTube. Solo visibles en avisos Estandars, Premiums, Super Premiums y Vidriera</span>';
            unset($form['field_aviso_video'][0]['value_markup']);
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#title'] = 'Miniatura del Video:';
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#collapsible'] = 0;
            $form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['#title'] = '';
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['flags']);
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['description']);
            $form['field_aviso_video'][0]['emvideo']['delete']['#title'] = 'Eliminar Video';
            $form['field_aviso_video'][0]['emvideo']['delete']['#description'] = '<span class="Ingresa">Marque la casilla para eliminar el video.</span>';
            print drupal_render($form['field_aviso_video']);
          ?>
        </div>
        <?php } ?>
      </fieldset>
      <?php if(isset($form['captcha'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(isset($form['captcha_google'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha_google']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(!user_is_logged_in()) { ?>
      <fieldset>  
        <div class="Row clearfix condiciones">
          <?php
            print drupal_render($form['profile_condiciones']);
          ?>
        </div>
        <div class="Row clearfix ofertas">
          <?php
            print drupal_render($form['profile_recepcion_ofertas']);
          ?>
        </div>
      </fieldset>
      <?php } ?>
      <div class="alertaLegal">
        <div class="Texto"> 
          Está expresamente prohibido ofrecer o publicar material que quebrante cualquier legislación vigente (ejemplo Decreto Nacional 936/11). Tampoco está permitido publicar material inmoral, obsceno, pornográfico, discriminatorio, injurioso o cualquier otro contenido que afecte la privacidad de las personas, la propiedad intelectual o que sea contrario a las buenas costumbres. 
        </div>
      </div>
      <div class="Row clearfix">
        <div class="Ingresa">
          <div class="Boton">
            <div class="Cv Tl"></div>
            <div class="Cv Tr"></div>
            <div class="Cv Bl"></div>
            <div class="Cv Br"></div> 
            <?php 
              unset($form['buttons']['preview']);
              unset($form['buttons']['delete']);
              if($user->uid != 0){
                if(arg(1) == 'add')
                  $form['buttons']['submit']['#value'] = "Siguiente >";
                elseif(arg(2) == 'clone')
                  $form['buttons']['submit']['#value'] = "Copiar";
              }
              print drupal_render($form['buttons']['submit']); 
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-sin-mostrar" style="display:none;">
  <?php
    unset($form['language']);
    unset($form['workflow']);
    unset($form['taxonomy']['tags']);
    print drupal_render($form);
  ?>
  </div>
</div>
