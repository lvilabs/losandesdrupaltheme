<?php
/**
 * @file
 * Theme implementation to display a single FAQ page.
 *
 */

$matches = array();
preg_match('/<div class="faq-description">(.+?)<\/div>/ms', $content, $matches);
$description = $matches[1];
$content = preg_replace('/(<div class="faq-description">.+?<\/div>)/ms', '', $content);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>
  <div class="Header SinLoguear">
    <?php include dirname(__FILE__).'/includes/header.tpl.php'; ?>
  </div> 

<?php if($page_arriba): ?>
  <div style="margin-bottom:15px" class="Columnas clearfix">
  <?php print $page_arriba; ?>
  </div>
<?php endif; ?>
  
<div class="Contenido">

<?php if($is_front): ?>
  <div class="GradientDestacado">
    <div class="SombraDestacado">
      <div class="FondoAutoDestacado">
        <div class="CT clearfix">
          <div class="CR">
            
<!-- VENDE AHORA start -->

            <div class="Publicar clearfix">
                <h3 title="Vend&eacute; ahora"><span class="DN">Vend&eacute; ahora</span></h3>
                
                <div class="Fondo clearfix">
                    <div class="Online clearfix">
                        <h4>On-line</h4>
                        <p>Public&aacute; tus avisos Web</p>
                        <div class="Boton">
                            <div class="Cv Tl"></div>
                            <div class="Cv Tr"></div>
                            <div class="Cv Bl"></div>
                            <div class="Cv Br"></div>                                        
                           <a class="" href="/publicar.asp" title="Public&aacute; Gratis">Public&aacute; Gratis</a>
                        </div>
                    </div>
                </div>
                
                <div class="Impresa">
                    <h4>Edici&oacute;n impresa</h4>
                        <p>Compr&aacute; tu aviso en el diario:</p>
                        <span class="opciones">Por Tel&eacute;fono</span>
                        <span class="opNum">(0351) 4467777</span>
                        <span class="opciones"><a onclick="window.open('SITIO_CLASIFICADOS/ehelp/ehelp.html','filtro','status=false,scrollbars=yes,width=580,height=430')" href="#">Por Chat</a></span>
                        <span class="opciones"><a href="SITIO_CLASIFICADOS/Receptorias.asp">En receptor&iacute;as</a></span>
                </div>
            </div>

<!-- VENDE AHORA end -->

          </div>
          <div class="CD">
            <div class="Buscador">
              <h2 title="Qué buscas"><span class="DN">Qu&eacute; buscas</span></h2>

<!-- BUSCADOR start -->
              <?php
                $block_buscador = module_invoke('block', 'block', 'view', AUTOSLAVOZ_BUSCADOR_HOME_BID);
                print $block_buscador['content'];
              ?>
<!-- BUSCADOR end -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!--FIN Destacado Top-->
<?php endif; ?>

  <div class="Columnas clearfix">
    <div class="CD" style="float:left; width: 50%;">

<?php if(! $is_front): ?>
  <?php print $breadcrumb; ?> 
<?php endif; ?>

    </div>
  </div>

  <div class="Columnas clearfix">
    <div class="CD">

      <?php print $page_media; ?>

<div class="Content clearfix">
  <div class="ContactoAutos">
    <div class="Sombra">
      <div class="Borde clearfix">
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
      </div>
    </div>
    <div class="Promos">
      <div class="Borde SinBorde clearfix">
        <?php print $description; ?>
      </div>
    </div>
  </div>
</div>

      <div id="content">
        <div id="system-messages-wrapper" style="display:none">
          <div id="system-messages">
        <?php print $messages; ?>
          </div>
          <a class="system-messages-inline" href="#system-messages" rel="noindex">Mensaje de sistema</a>
        </div>

        <div id="content-area">
          <?php print $content; ?>
        </div>
      </div>

      <?php print $page_inferior; ?>      

    </div>

    <div class="CR">
      <?php print $page_columna_derecha; ?>

    </div>
  </div>
</div>

  <?php include dirname(__FILE__).'/includes/footer.tpl.php'; ?>
  
  <?php print $closure; ?>

</body>
</html>