<?php
// $Id: views-view-fields.tpl.php,v 1.6 2008/09/24 22:48:21 merlinofchaos Exp $
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$datos_sitio = administracion_sitios_get_all($row->uid);
if(!empty($datos_sitio) && $datos_sitio['estado_cabezal']  && $datos_sitio['wizard_completo']){
  $dominio_clvi = clasificados_apachesolr_obtener_usuario_url($row->uid);
  if(strpos($dominio_clvi, 'sitio/') !== FALSE){
    $usuario = user_load($row->uid);
    //$destacados = administracion_sitios_get_all_aviso_destacado($row->uid);
    if(ventas_es_vendedor($row->uid)) {
      $vendedor = ventas_get_vendedor($row->uid);
      include dirname(__FILE__).'/includes/cabezal-micrositio-tiendas.tpl.php';
      //include dirname(__FILE__).'/includes/destacados-micrositio-tiendas.tpl.php';
    } else {
      $usuario->profile_ciudad = taxonomy_get_term($usuario->profile_ciudad)->name;
      $usuario->profile_barrio = taxonomy_get_term($usuario->profile_barrio)->name;
      include dirname(__FILE__).'/includes/cabezal-micrositio.tpl.php';
      //include dirname(__FILE__).'/includes/destacados-micrositio.tpl.php';
    }
  }
} else {
  //Calle y n°
  $direccion = $row->profile_values_profile_calle_value." ".$row->profile_values_profile_altura_value;
  $ciudad = taxonomy_get_term($row->profile_values_profile_ciudad_value);
  $provincia = taxonomy_get_term($row->profile_values_profile_provincia_value);
  //Ciudad
  if($ciudad != ''){
    if($direccion == '')
      $direccion = $ciudad->name;
    else
      $direccion .= ' - '.$ciudad->name;
  }
  //Provincia
  if($provincia != ''){
    if($direccion == '')
      $direccion = $provincia->name;
    else
      $direccion .= ' - '.$provincia->name;
  }
  //$block_concesoinaria_gmap = module_invoke('views', 'block', 'view', 'concesionarias-block_3');
?>
<div class="fondoConcesionaria clearfix">
  <div class="Borde clearfix">
    <div class="Logo Left">
      <?php if($row->users_picture && $view->display_handler->default_display->view->style_plugin->rendered_fields[0]['rid'] != 'concesionaria web') print clasificados_imagecache('logo_200_160', $row->users_picture, $row->profile_values_profile_nombre_comercial_value, $row->profile_values_profile_nombre_comercial_value); ?>
    </div>
    <div class="Info Left">
      <h1><?php print $row->profile_values_profile_nombre_comercial_value; ?></h1>
      <h3><?php print $direccion; ?></h3>
      <h3><?php print $row->profile_values_profile_telefono_principal_value; ?></h3>
      <h3><?php print $row->profile_values_profile_telefono_secundario_value; ?></h3>
      <h3><a href="mailto:<?php print $row->users_mail; ?>"><?php print $row->users_mail; ?></a></h3>
      <?php if ($row->profile_values_profile_sitio_web_value != ""){ ?>
        <?php if (eregi("http://",$row->profile_values_profile_sitio_web_value)){ ?>
          <h3><a href="<?php print $row->profile_values_profile_sitio_web_value; ?>" target="_blank"><?php print $row->profile_values_profile_sitio_web_value; ?></a></h3>
        <?php } else { ?>
          <h3><a href="http://<?php print $row->profile_values_profile_sitio_web_value; ?>" target="_blank"><?php print $row->profile_values_profile_sitio_web_value; ?></a></h3>
        <?php } ?>
      <?php } ?>
      <h3></h3>
      <h3></h3>
      <h3></h3>
    </div>
    <div class="formAgencia Left">
      <?php //print $block_concesoinaria_gmap['content']; ?>
    </div>
  </div>
</div>
<?php } ?>