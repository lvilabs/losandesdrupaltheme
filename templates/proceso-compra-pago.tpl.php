<div class="currentBuy">
  <h3>Estás comprando:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'ficha_aviso_120_90_sc', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
      <h2><?php print $form['nodo']['#title']; ?></h2>
      <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
    <div class="calculadora clear">
      <input type="hidden" id="precio_venta" value="<?php print $form['nodo']['#precio']; ?>">
      <input type="hidden" id="vendedor_uid" value="<?php print $form['vendedor_uid']['#value']; ?>">
      <div class="pagando"></div>
      <span class="cargando"></span>
      <div class="cuotas final" id="monto-cuotas-div" >
        <div class="contado DN">
          <div class="label-contado">Precio Contado</div>
          <div class="label-pagos">Efectivo, tarjeta de débito o tarjeta de crédito en 1 pago</div>
          <div class="precio-contado"></div>
        </div>
        <div class="tarjeta DN">
          <div class="label-cuotas">
            <div class="cantidad-cuota"></div> 
            <div class="costo-cuota"></div>
          </div>
          <div class="label-ptf"></div>
          <div class="label-cft"></div>
          <div class="label-costo">Costo financiero total</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix avance">
  <div class="item superado"><span class="numero">1</span> Datos de contacto</div>
  <div class="item superado"><span class="numero">2</span> Datos de entrega</div>
  <div class="item activo"><span class="numero">3</span> Confirmación</div>
</div>



<div class="clearfix">
<div class="total clearfix">
<div class="itemT"><?php print $form['nodo']['#title']; ?>
<?php if(!empty($form['venta']['#talle'])): ?>
  <?php  print ' (talle '.$form['venta']['#talle'].')'; ?>
<?php endif; ?>
</div>
<div><span class="priceT">$ <?php print $form['nodo']['#precio']; ?></span>
  <?php if($form['venta']['#cantidad']>1): ?>
  x <?php print $form['venta']['#cantidad']; ?>
  <?php endif; ?>
</div>
<?php if($form['venta']['#descuento_porcentaje']>0): ?>
<div class="itemT">Descuento</div>
<div class="">-$ <?php print $form['venta']['#descuento']; ?> (<?php print $form['venta']['#descuento_porcentaje']; ?>%)</div>
<?php endif; ?>
<div class="itemT">Costo de envío</div>
<div class="priceT">$ <?php print $form['venta']['#costo_envio']; ?></div>
<div class="itemT destacado">Total</div>
<div class="destacado"><span class="priceT">$ <?php print $form['venta']['#compra_total']; ?></span>
    <?php if(isset($form['venta']['#detalle_medio_pago'])) print $form['venta']['#detalle_medio_pago']; ?>
</div>
</div>
<div class="alerta">Al confirmar la compra se dirigirá a la plataforma de pagos para que haga efectivo el pago. Recomendamos seleccionar la opción de pago con tarjeta para agilizar el proceso de pago.</div>

<div class="form-item">
<?php if($form['venta']['#cantidad']>$form['venta']['#stock']): ?>
<div class="total">
  <div class="priceT destacado">
  Hay solamente <?php print $form['venta']['#stock']; ?> productos en stock. 
  Vuelva a la pantalla anterior si desea cambiar la cantidad.
  </div>
</div>
<?php endif; ?>
<?php
if(isset($form['submit'])) { //verificar que plataformas de pago dispone el usuario.
  print drupal_render($form['submit']);
}
if(isset($form['submit_mp'])) {
  print drupal_render($form['submit_mp']);
}
if(isset($form['submit_pd'])) {
  print drupal_render($form['submit_pd']);
}
?>
  <a href="<?php print url('comprar/'.$form['venta']['#venta_id'].'/entrega'); ?>">Volver</a>
</div>
</div>
<?php
print drupal_render_children($form);