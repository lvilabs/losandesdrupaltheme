<div class="Content emprendimientos_relacionados clearfix">
  <div class="Titulos Sombra">
    <div class="Borde clearfix">
      <h4 class="datos color unidad"><?php print $header['type']; ?></h4>
      <h4 class="datos color"><?php print $header['field_aviso_operacion_value']; ?></h4>
      <h4 class="datos color"><?php print $header['field_aviso_superficie_cubierta_value']; ?></h4>
      <h4 class="datos color"><?php print $header['field_aviso_cantidad_dormitorios_value']; ?></h4>
      <h4 class="datos color precio"><?php print $header['field_aviso_precio_value']; ?></h4>
    </div>
  </div>

<?php foreach ($rows as $count => $row):
  $tid = clasificados_rubros_node_obtener_rubro_tid($row['type']);
  $rubro = taxonomy_get_term($tid);
?>
  <div class="opciones clearfix">
      <div class="datos color unidad"><a href="<?php print url('node/'.$row['nid']); ?>"><?php print $rubro->name; ?></a>&nbsp;</div>
      <div class="datos"><?php print $row['field_aviso_operacion_value']; ?>&nbsp;</div>
      <div class="datos"><?php print $row['field_aviso_superficie_cubierta_value']; ?>&nbsp;</div>
      <div class="datos"><?php print $row['field_aviso_cantidad_dormitorios_value']; ?>&nbsp;</div>
      <div class="datos color precio"><?php if(empty($row['field_aviso_ocultar_precio_value'])) print $row['field_aviso_moneda_value']." ".$row['field_aviso_precio_value']; else print t('Consultar'); ?>&nbsp;</div>
  </div>
<?php endforeach; ?>
</div>
