<?php
/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It should be placed within the <body> tag. When selecting through CSS
 *   it's recommended that you use the body tag, e.g., "body.front". It can be
 *   manipulated through the variable $classes_array from preprocess functions.
 *   The default values can be one or more of the following:
 *   - front: Page is the home page.
 *   - not-front: Page is not the home page.
 *   - logged-in: The current viewer is logged in.
 *   - not-logged-in: The current viewer is not logged in.
 *   - node-type-[node type]: When viewing a single node, the type of that node.
 *     For example, if the node is a "Blog entry" it would result in "node-type-blog".
 *     Note that the machine name will often be in a short form of the human readable label.
 *   - page-views: Page content is generated from Views. Note: a Views block
 *     will not cause this class to appear.
 *   - page-panels: Page content is generated from Panels. Note: a Panels block
 *     will not cause this class to appear.
 *   The following only apply with the default 'sidebar_first' and 'sidebar_second' block regions:
 *     - two-sidebars: When both sidebars have content.
 *     - no-sidebars: When no sidebar content exists.
 *     - one-sidebar and sidebar-first or sidebar-second: A combination of the
 *       two classes when only one of the two sidebars have content.
 * - $node: Full node object. Contains data that may not be safe. This is only
 *   available if the current page is on the node's primary url.
 * - $menu_item: (array) A page's menu item. This is only available if the
 *   current page is in the menu.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing the Primary menu links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title: The page title, for use in the actual HTML content.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $help: Dynamic help text, mostly for admin pages.
 * - $content: The main content of the current page.
 * - $feed_icons: A string of all feed icons for the current page.
 *
 * Footer/closing data:
 * - $footer_message: The footer message as defined in the admin settings.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * Regions:
 * - $content_top: Items to appear above the main content of the current page.
 * - $content_bottom: Items to appear below the main content of the current page.
 * - $navigation: Items for the navigation bar.
 * - $sidebar_first: Items for the first sidebar.
 * - $sidebar_second: Items for the second sidebar.
 * - $header: Items for the header region.
 * - $footer: Items for the footer region.
 * - $page_closure: Items to appear below the footer.
 *
 * The following variables are deprecated and will be removed in Drupal 7:
 * - $body_classes: This variable has been renamed $classes in Drupal 7.
 *
 * AUTOS LA VOZ
 * - $es_ficha: Determina si se esta viendo o no (TRUE / FALSE) la ficha de un aviso.
 * 
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess()
 * @see zen_process()
 */

$is_search = in_array(arg(0), array('search', 'taxonomy')); //taxonomy = category
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" xmlns:fb="http://ogp.me/ns/fb#">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php print $scripts; ?>
  <?php include(dirname(__FILE__).'/includes/dfp-google.php'); ?>
</head>
<body class="<?php print $classes; ?>" itemscope itemtype="http://schema.org/WebPage">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>

<div id="fb-root"></div>

<!--
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
-->
<?php
  $es_vendedor = 0;
  if(strpos($_SERVER['REQUEST_URI'], 'sitio/') !== FALSE) :
    $nombre_sitio = explode('/', $_SERVER['REQUEST_URI']);
    $nombre_sitio = explode('?', $nombre_sitio[2]);
    $usuario_id = administracion_sitios_get_url_sitio_uid($nombre_sitio[0]);
    $es_vendedor = ventas_es_vendedor($usuario_id);
  endif;
?>
  
  <div class="Header SinLoguear <?php if($es_vendedor) print 'HeaderTienda'; ?>">
    <?php include dirname(__FILE__).'/includes/header.tpl.php'; ?>
  </div>

<div id="alerta"><div class="CajaAmarilla"><div class="Mensaje"><div class="close"><a href="javascript:ocultar_alerta();" title="Cerrar alerta" rel="noindex">x</a></div><div id="warningbrowser"></div></div></div></div>

<?php if($page_arriba): ?>
  <div class="Columnas page-arriba <?php if($es_vendedor) print 'pageArribaTienda'; ?> clearfix">
  <?php print $page_arriba; ?>
  </div>
<?php endif; ?>
<?php
$micrositio = 0;
if($is_search) {
  $env_id = apachesolr_default_environment();
  if(apachesolr_has_searched($env_id)) {
    $query = apachesolr_current_query($env_id);
    $query_filtros = $query->getFilters();
    // Detecatmos que no sea micrositio
    $micrositio = 0;
    foreach($query_filtros as $filtro) {
      if($filtro['#name'] == 'is_uid') $micrositio = 1;
    }
  }
}
?>
<div class="Contenido">
<?php 
  if($is_front || $es_ficha || $es_interna || $is_search) {
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_anuncio_previo';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);

    $module = 'clasificados_banners';
    $delta = 'dfp_banner_topsite';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
    
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_zocalo';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
    
    if (!$es_ficha) {
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_lateral_1';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    }    
  }
  
  if($is_nota) {
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_topsite';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
  }
  
?>

<?php if ($es_ficha || $es_interna): 
    if($es_ficha) :
      $datos_sitio = administracion_sitios_get_all($node_info->uid);
    endif;
    if($es_interna):
      $usuario_id = administracion_sitios_get_url_sitio_uid(arg(1));
      $datos_sitio = administracion_sitios_get_all($usuario_id);
    endif;
    if(!empty($datos_sitio) && $datos_sitio['estado_cabezal'] && $datos_sitio['wizard_completo']):
      $dominio_clvi = clasificados_apachesolr_obtener_usuario_url($datos_sitio['uid']);
      if(strpos($dominio_clvi, 'sitio/') !== FALSE):
        $usuario = user_load($datos_sitio['uid']);
        //$destacados = administracion_sitios_get_all_aviso_destacado($datos_sitio['uid']);
        $vendedor = ventas_get_vendedor($datos_sitio['uid']);
        if(!empty($vendedor)) {
          include dirname(__FILE__).'/includes/cabezal-micrositio-tiendas.tpl.php';
          if($es_ficha) {
            $destacados = administracion_sitios_get_all_aviso_destacado($datos_sitio['uid']);
            include dirname(__FILE__).'/includes/destacados-micrositio-tiendas.tpl.php';
          }
        } else {
          $usuario->profile_ciudad = taxonomy_get_term($usuario->profile_ciudad)->name;
          $usuario->profile_barrio = taxonomy_get_term($usuario->profile_barrio)->name;
          include dirname(__FILE__).'/includes/cabezal-micrositio.tpl.php';
          //include dirname(__FILE__).'/includes/destacados-micrositio.tpl.php';
        }
      endif;
    endif;
  endif; 
?>

<?php if(! $is_front): ?>
  <?php if ($es_ficha): ?>
    <h2 class="breadcrumb"><?php print $breadcrumb_ficha; ?></h2>
  <?php else: ?>
    <?php print $breadcrumb; ?>
  <?php endif; ?>
<?php endif; ?>

<?php if($es_ficha): ?>
  <div itemscope itemtype="http://schema.org/Product"><!-- Inicio Microdata -->
<?php endif; ?>  
  
<?php if($es_ficha && isset($node_info) && $node_info->vendible): ?>
  <div class="Columnas ColTiendas clearfix">
<?php else: ?>  
  <div class="Columnas clearfix">
<?php endif; ?>  
  
<?php if($is_search): ?>
    
    <?php 
    $class_tiendas = '';
    $env_id = apachesolr_default_environment();
    if(apachesolr_has_searched($env_id)) {
      $query = apachesolr_current_query($env_id);
      if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, 6106)){
        $class_tiendas = 'filters-tiendas';
      }
    } ?>
    
    <div class="CL <?php print $class_tiendas; ?>">
      <?php print $page_columna_izquierda; ?>
      
      <?php 
        if(!$micrositio) {
          $module = 'clasificados_banners';
          $delta = 'dfp_banner_rectangle_1';
          $block = (object) module_invoke($module, 'block', 'view', $delta);
          $block->module = $module;
          $block->delta = $delta;
          print theme('block', $block);
        }
      ?>
      
    </div>
<?php endif; ?>

 <?php if ($es_ficha && isset($emprendimiento)): ?>
   <!-- COmienza emprendiemientos cabezal -->
  <div class="Emprendimiento">
   <div class="AvisoTitulo FichaAuto<?php if(isset($emprendimiento) && isset($emprendimiento['logo'])) { print ' TituloConLogo'; } ?>">
        <div class="Borde clearfix">
<?php   if(isset($emprendimiento)): ?>
<?php     if(isset($emprendimiento['logo'])): ?>
          <div class="emprendimiento_logo">
            <a itemprop="logo" href="<?php print url('node/'.$emprendimiento['nid']); ?>" rel="search"><?php print theme('imagecache', 'redimensionar_99x74', $emprendimiento['logo']['filepath'], 'Logo del emprendimiento', 'Logo del emprendimiento'); ?></a>
          </div>
<?php     endif; ?>
<div class="emprendimiento_info">
          <div class="emprendimiento_titulo">
            <?php print $emprendimiento['header']; ?>
          </div>
<?php   endif; ?>
            <h2 class="titulo-ficha" itemprop="name"><?php print truncate_utf8($node_info->title, 130, TRUE, FALSE); ?></h2>
<?php   if($node_info->precio != 'consultar'): ?>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
<?php       if($esta_publicado): ?>             
              <link itemprop="availability" href="http://schema.org/InStock" content="In Stock" />
<?php       endif; ?>
              <meta itemprop="priceCurrency" content="<?php print $node_info->moneda; ?>" />
<?php         if(isset($microdata_category)): ?>
<?php           print $microdata_category; ?>
<?php         endif; ?>
            </div>
<?php   endif; ?>
</div>
        </div>
    </div>
    </div>  
   
   <!-- Termina emprendiemientos cabezal -->
     <?php endif; ?>


    <div class="CD">

      <div id="content">
        <div id="system-messages-wrapper" style="display:none">
          <div id="system-messages">
        <?php print $messages; ?>
          </div>
          <a class="system-messages-inline" href="#system-messages" title="Mensaje de sistema" rel="noindex">Mensaje de sistema</a>
        </div>
        
        <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>
          
        <div id="content-area">
          <?php print $content; ?>
        </div>
      </div>
      
      <?php print $page_media; ?>
      
      <?php if($is_front): ?>
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_middle_1';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      <?php endif; ?>
      
      <?php print $page_inferior; ?>

      <?php if($is_front): ?>
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_middle_2';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      <?php endif; ?>
      
    </div>

<?php if(!$is_search): ?>
    <div class="CR">  
      <?php print $page_columna_derecha; ?>
    </div>
<?php endif; ?>

<?php if($con_banner): ?>
    <div class="CB">
      <?php print $block_banner_lateral['content']; ?>
    </div>
<?php endif; ?>

<?php if($page_footer): ?>
    <div class="Columnas clearfix">
      <?php print $page_footer; ?> 
    </div>
    
    <?php if($is_front): ?>
    <?php 
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_middle_3';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
    <?php endif; ?>
    
<?php endif; ?>  
  
  </div>

<?php if($es_ficha): ?>
  </div><!-- FIN Microdata -->
<?php endif; ?>  
  
</div>
  
  <?php include dirname(__FILE__).'/includes/footer.tpl.php'; ?>
  
  <?php print $closure; ?>

</body>
</html>