<!-- DETALLE DE COMPRA -->
<tr style="margin-bottom: 30px;display: block;">
	<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; padding:15px 25px; font-family:Arial, Helvetica, sans-serif; font-size:16px; text-align:center; font-weight:regular; text-align:center; color: #666;">!gracias_extra</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px">
		<tbody>
		<tr>
			<td valign="top">
				<table width="222" border="0" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
					<td style="padding: 15px 30px 30px;">
						<a href="!link_aviso"><img typeof="foaf:Image" src="!foto" width="222" height="151" alt="!titulo_aviso" title="!titulo_aviso"></a>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
			<td valign="top">
				<table width="512" border="0" cellspacing="0" cellpadding="0" style="margin-right:38px; margin-bottom:50px;">
				<tbody>
				<tr>
					<td style="text-align:left; font-family:Arial, Helvetica, sans-serif;  text-transform:uppercase; color: #000;">
						<h2 style="font-size:14px;">DETALLE DE COMPRA:</h2>
					</td>
				</tr>
        <tr>
					<td style="padding: 15px 0px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:regular; color: #666;">Adquiriste un productos en !comercio_vendedor - !razon_social_vendedor (CUIT !cuil_vendedor)</td>
				</tr>
				<tr>
					<td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:24px; ">
						<a href="!link_aviso" style="text-decoration:none; font-weight:100; color:#666666; text-transform:uppercase; ">!titulo_aviso</a>
					</td>
				</tr>
				<tr>
					<td>
						<h2 style="text-decoration:none; color:#000; font-family:Georgia, Serif;font-weight:bold; margin:0; padding:0; font-weight:400; text-align:left; color:#333; font-size:36px; margin:0;">Gracias por tu compra!</h2>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
			<!-- Fin seccion nota uno -->
			<!--FIN NOTAS SECCION-->
		</tr>        
		</tbody>
		</table>
	</td>
</tr>

<tr style="display: block;">
 	<td style="text-align:left; font-family:Arial, Helvetica, sans-serif;  text-transform:uppercase; color: #000; font-size: 14px; padding: 10px 30px; font-weight: bold;">!titulo_entrega:</td>
</tr>
<tr>
 	<td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding: 0 30px; ">!texto_entrega</td>
</tr>
<tr style="display: block;">
 	<td style="text-align:left; font-family:Arial, Helvetica, sans-serif;  text-transform:uppercase; color: #000; font-size: 14px; padding: 10px 30px; font-weight: bold;">Pago:</td>
</tr>
<tr>
 	<td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding:0 30px 20px; ">Abonaste a través de !medio_pago: <strong style="color: #FF0000; font-weight:100;">!monto</strong></td>
</tr>
<!--Fin detalle de compra-->