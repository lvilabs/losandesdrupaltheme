<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

  $cantidades = clasificados_obtener_cantidad_avisos_publicados_por_comercio();
  $filtered_fields = array();
  foreach ($view->display_handler->default_display->view->style_plugin->rendered_fields as $usuario) {
    $uid = $usuario['uid'];
    if(!isset($cantidades[$uid])) continue; //skip this user

    $field = $usuario;
    $field['url'] = clasificados_apachesolr_obtener_usuario_url($uid);
    $filtered_fields[] = $field;
  }
?>
<div class="Concesionarias Sombra">
  <div class="Content clearfix">
    <div class="Inner">
      <div class="Titulo"><h4>Inmobiliarias</h4></div>
    </div>      
    <form action="/search/apachesolr_search" method="get" id="form-comercios">
      <fieldset>
        <select class="select-negro"  id="select-comercio">
          <option value="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330&f[1]=ss_rol:Inmobiliaria" selected="selected">Todas</option>
<?php foreach ($filtered_fields as $usuario): ?>
          <option value="<?php print $usuario['url']; ?>"><?php print $usuario['value_1']; ?></option>
<?php endforeach; ?>
        </select>
      </fieldset>
      <div class="Boton">
        <div class="Cv Tl"></div>
        <div class="Cv Tr"></div>
        <div class="Cv Bl"></div>
        <div class="Cv Br"></div> 
        <input class="boton-naranja" type="submit" id="button3" value="Buscar" />
      </div>
    </form>
  </div>
</div>