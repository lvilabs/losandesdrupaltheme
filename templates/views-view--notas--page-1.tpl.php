<?php
// $Id: views-view.tpl.php,v 1.13.2.2 2010/03/25 20:25:28 merlinofchaos Exp $
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
 $get = $_GET;
 $pagina = 0;
 if(isset($get['page'])) $pagina = $get['page'];
?>
<div class="Content Notas clearfix">
  <header class="tit-seccion">Noticias de Clasificados La Voz</header>
  <div class="container">  
<?php 
  foreach($view->display_handler->default_display->view->style_plugin->rendered_fields as $nkey => $nota){ 
    $con_video = 0;
    if(!empty($nota['field_nota_video_genoa_value']) || !empty($nota['field_aviso_video_embed']))
      $con_video = 1;
    $fecha_nota = clasificados_fecha_formato_nota($nota['created']);
?>    
<?php if($nkey == 0 && $pagina == 0) { ?>
    <?php $imagen = theme('imagecache', 'nota_620_600', $nota['field_nota_imagenes_fid_1']); ?>
    <div class="nodos-destacados clearfix">    
      <div class="panel nota principal <?php if(empty($nota['field_nota_imagenes_fid'])) print 'sin-foto'; ?>">
        <div class="panel-body">
          <article class="node-teaser node-teaser-card">
            <?php if(isset($nota['field_nota_imagenes_fid']) && !empty($nota['field_nota_imagenes_fid'])) { ?>
            <div class="foto">
              <?php if($con_video) { ?>
                <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
              <?php } ?>
              <div class="gradiente"></div>
              <a href="<?php print $nota['path']; ?>" target="_self">
                <?php print $imagen; ?>
              </a>
            </div>
            <?php } ?>
            <div class="contenido">
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
              <div class="panelAuthor" rel="author">
                <!-- <a href="/users/hserafini" target="_self"> -->
                  <!-- <img src="http://wpc.72C72.betacdn.net/8072C72/lvi-images/sites/default/files/styles/box_100_100/public/pictures/picture-58819-1394122381.jpg"> -->
                  <span class="byAuthor">por</span> <?php print $nota['field_nota_autor_value']; ?>
                <!-- </a> -->
              </div>
              <?php } ?>
              <h1><?php print $nota['title']; ?></h1>
              <div class="bajada"><?php print strip_tags($nota['field_nota_resumen_value'],'<p>'); ?></div>
              <div class="footer-card">
                <div class="data">
                  <span class="nodeData">
                    <time datetime="<?php print $nota['created']; ?>"><?php print $fecha_nota; ?></time>&nbsp;•&nbsp;<!-- <a href="/temas/crisis-economica" target="_self"> --><?php print strtolower($nota['name']); ?><!-- </a> -->
                  </span>
                </div>      
              </div>
            </div>
          </article>
        </div>
      </div>
<?php } ?>
<?php if($nkey > 0 && $nkey < 5  && $pagina == 0) { ?>  
      <div class="panel nota comun <?php if(empty($nota['field_nota_imagenes_fid'])) print 'sin-foto'; ?>">
        <div class="panel-body">
          <article class="node-teaser node-teaser-card">
            <?php if(isset($nota['field_nota_imagenes_fid']) && !empty($nota['field_nota_imagenes_fid'])) { ?>
            <div class="foto">
              <?php if($con_video) { ?>
                <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
              <?php } ?>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
                <div class="gradiente"></div>
              <?php } ?>
              <?php print $nota['field_nota_imagenes_fid']; ?>
            </div>
            <?php } ?>
            <div class="contenido">
              <?php if(empty($nota['field_nota_imagenes_fid'])) { ?>
              <h2><?php print $nota['title']; ?></h2>
              <div class="bajada"><?php print strip_tags($nota['field_nota_resumen_value'],'<p>'); ?></div>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
              <div class="panelAuthor" rel="author">
                <!-- <a href="/users/hserafini" target="_self"> -->
                  <!-- <img src="http://wpc.72C72.betacdn.net/8072C72/lvi-images/sites/default/files/styles/box_100_100/public/pictures/picture-58819-1394122381.jpg"> -->
                  <span class="byAuthor">por</span> <?php print $nota['field_nota_autor_value']; ?>
                <!-- </a> -->
              </div>
              <?php } ?>
              <?php } else { ?>
              <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
              <div class="panelAuthor" rel="author">
                <!-- <a href="/users/hserafini" target="_self"> -->
                  <!-- <img src="http://wpc.72C72.betacdn.net/8072C72/lvi-images/sites/default/files/styles/box_100_100/public/pictures/picture-58819-1394122381.jpg"> -->
                  <span class="byAuthor">por</span> <?php print $nota['field_nota_autor_value']; ?>
                <!-- </a> -->
              </div>
              <?php } ?>
              <h2><?php print $nota['title']; ?></h2>
              <?php } ?>
              <div class="footer-card">
                <div class="data">
                  <span class="nodeData">
                    <time datetime="<?php print $nota['created']; ?>"><?php print $fecha_nota; ?></time>&nbsp;•&nbsp;<!-- <a href="/temas/crisis-economica" target="_self"> --><?php print strtolower($nota['name']); ?><!-- </a> -->
                  </span>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
<?php } ?>
<?php if($nkey == 5  && $pagina == 0) { ?>
    </div>
    <div class="nodos-list clearfix">
<?php } ?>
<?php if($pagina != 0 && $nkey == 0) { ?>
    <div class="nodos-list clearfix">
<?php } ?>
<?php if($nkey >= 5 || $pagina != 0) { ?>
      <article class="node-teaser node-teaser-list">
        <div class="nodo-contenido">
          <div class="info">
            <time datetime="<?php print $nota['created']; ?>"><?php print $fecha_nota; ?></time>
            <div class="tags">
              <!-- <a href="/temas/crisis-economica"> --><?php print strtolower($nota['name']); ?><!-- </a> -->
            </div>
            <?php if(isset($nota['field_nota_autor_value']) && !empty($nota['field_nota_autor_value'])) { ?>
            <div class="panelAuthor" rel="author">
              <!-- <a href="/users/jcanias"> -->
                <div class="nombre">
                  <span class="byAuthor">por</span><br><?php print $nota['field_nota_autor_value']; ?>
                </div>
                <!-- <div class="foto">
                  <img src="http://wpc.72C72.betacdn.net/8072C72/lvi-images/sites/default/files/styles/box_100_100/public/pictures/picture-65415-1379084839.jpg">
                </div> -->
              <!-- </a> -->
            </div>
            <?php } ?>
          </div>
          <main class="nota_periodistica <?php if(isset($nota['field_nota_imagenes_fid']) && !empty($nota['field_nota_imagenes_fid'])) print 'image'; ?>">
            <?php if(isset($nota['field_nota_imagenes_fid']) && !empty($nota['field_nota_imagenes_fid'])) { ?>
            <div class="foto">
              <?php if($con_video) { ?>
                <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
              <?php } ?>
              <?php print $nota['field_nota_imagenes_fid']; ?>
            </div>
            <?php } ?>
            <div class="contenido">
              <header>
                <h2><?php print $nota['title']; ?></h2>
              </header>
              <?php print strip_tags($nota['field_nota_resumen_value'],'<p>'); ?>
              <div class="footer-card"></div>
            </div>
          </main>
        </div>  
      </article>
<?php } ?>
<?php } ?>      
    </div>
  </div>
</div>
<?php if($pager != "") { ?>
  <div class="ResultadoB">
    <div class="paginado">
      <?php print $pager; ?>
    </div>
  </div>
<?php } ?>