<?php
$node = menu_get_object();
if(sitio_local_es_nodo_aviso($node) && $node->disponible_venta==1) :
?>
<div id="<?php print $block_html_id; ?>" class="accessible_news_slider Multimedia siempre_visible <?php print $classes; ?> clearfix">
<?php
$vendedor = user_load($node->uid);
$otros_avisos = publicacion_avisos_get_avisos_por_usuario($vendedor->uid, 10, array($node->nid), FALSE);
?>
  <div class='logo'>
    <?php print clasificados_imagecache('logo_130_105', $vendedor->picture, $vendedor->profile_nombre_comercial, $vendedor->profile_nombre_comercial); ?>
  </div>
  <div class="Botones">
    <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
    <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
  </div>
  <div class="otros_avisos">
    <div class="mascara">
      <ul class="Slider">
        <?php
        foreach($otros_avisos as $aviso) {
          if(empty($aviso['filepath']))
            $aviso['filepath'] = 'imagefield_default_images/RB_noimagen.jpg';
        ?>
        <li>
          <div class="aviso">
            <div class="foto">
              <a href="<?php print url('node/'.$aviso['nid'], array('alias'=>TRUE)); ?>">
                <?php print clasificados_imagecache('ficha_aviso_120_90', $aviso['filepath'], $aviso['title'], $aviso['title']); ?>
              </a>
            </div>
            <div class="titulo"><?php print l($aviso['title'], 'node/'.$aviso['nid']); ?></div>
            <div class="precio"><?php print $aviso['precio_view']; ?></div>
          </div>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>
<?php endif; ?>