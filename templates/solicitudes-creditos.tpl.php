<?php
print drupal_render_children($form);
?>
<br>
<h3 class="titulo">Términos y condiciones</h3>
<div class="ayuda clearfix"><a id="terminosycondiciones" class="DN">Términos y condiciones</a>
  
  La aprobación de los créditos están sujetos a las siguientes condiciones; edad del solicitante: mínimo 18, máximo 69.<br>
  Montos máximos a financiar: desde el 70% del valor del auto en InfoAuto según tipo de vehículo, modelo, año y entidad otorgante.<br>
  Las condiciones pueden variar de acuerdo a la entidad otorgante sin previo aviso.<br>
  El envío de la solicitud no significa la aprobación del crédito. <br>
  La comisión de $500 (iva incluido) será abonada al agenciero únicamente si el crédito se liquida.
</div>
<div class="faqs">
  <a name="faqs" href="#" class="DN">FAQs</a>
  <h3 class="titulo">Preguntas Frecuentes</h3>
  <span class="pregunta">¿Cuánto gano si el crédito se concreta?</span>
  <span class="respuesta">Por cada crédito concretado se gana un valor fijo de 500 pesos (IVA incluido).</span>
  <span class="pregunta">¿Cuándo recibo el pago?</span>
  <span class="respuesta">El pago se liquida únicamente cuando el crédito está concretado, a mes vencido.</span>
  <span class="pregunta">¿Cómo recibo el pago?</span>
  <span class="respuesta">Los pagos se realizan por medio de efectivo o cheque, presentándose en la sucursal de Fuerza de Ventas.</span>
  <span class="pregunta">¿Cómo es el proceso de facturación y liquidación de pago?</span>
  <span class="respuesta">El agenciero emite la correspondiente factura a:<br>
    Razón Social:  Fuerza de Ventas SRL<br>
    CUIT: 30-70890436-4<br>
    Dirección: Av. Sagrada Familia 1345<br>
    Una vez recibida la factura, el pago se realiza a un plazo no mayor a 30 días.</span>
  <span class="pregunta">¿Cuál es el beneficio para mi cliente de obtener su crédito vía Fuerza de Ventas?</span>
  <span class="respuesta">El mayor beneficio es la posibilidad de acceder a múltiples opciones de crédito con numerosas entidades bancarias, lo cual permite obtener el crédito en la gran mayoría de los casos.</span>
  <span class="pregunta">¿Fuerza de ventas tiene créditos para todos los modelos de autos? </span>
  <span class="respuesta">Sí, desde 0 KM hasta modelos del año 2000.</span>
  <span class="pregunta">¿Qué tipo de créditos otorgan?</span>
  <span class="respuesta">Se otorgan créditos con garantía prendaria.</span>
  <span class="pregunta">¿Qué datos se necesitan para solicitar un crédito?</span>
  <span class="respuesta">Es necesario contar con los datos de la persona que va a solicitar el crédito: nombre, apellido, teléfono, y datos del vehículo a adquirir. Se deben completar todos los campos de la solicitud para que luego Fuerza de ventas se comunique con el cliente y pueda pedirle más datos si fuera necesario.</span>
  <span class="pregunta">En caso de dudas sobre el crédito, ¿a quién se dirige el cliente?</span>
  <span class="respuesta">Para todo lo que tenga que ver con dudas o reclamos sobre el proceso de aprobación del crédito, el cliente deberá comunicarse directamente con Fuerza de Ventas. Una vez que el crédito es liquidado, el cliente debe comunicarse directamente con la entidad bancaria otorgante. </span>
  <span class="pregunta">¿Qué pasa si la misma operación ingresa por diferentes agencias?</span>
  <span class="respuesta">En ese caso se dará prioridad a la primera solicitud ingresada (cronológicamente), si el cliente desistiera de realizar esa operación, se pasará a la siguiente solicitud, y así sucesivamente. La decisión final de dónde comprar siempre la tendrá el cliente.</span>
  <span class="pregunta">¿Cómo sé si el crédito se concreta?</span>
  <span class="respuesta">Podrá fijarse en la pestaña “Créditos” el estado de la solicitud en todo momento. Clasificados La Voz cuenta con un sistema de gestión que controla y confirma si el crédito fue aprobado o no.</span>
  <span class="pregunta">¿Alguien se contacta con el cliente luego de enviar el formulario? </span>
  <span class="respuesta">Luego de enviar el formulario de solicitud, te llegará un mail de confirmación para que sepas que tu solicitud ingresó con éxito. Luego, Fuerza de Ventas se contactará inmediatamente con el cliente para asesorarlo. Dentro de horario laboral (lunes a viernes de 9 a 18 hs.) lo hará en el período de 30 min.</span>
  <span class="pregunta">¿Hay algún contacto de soporte administrativo?</span>
  <span class="respuesta">Para consultas administrativas contactarse con vprieto@fdvnet.com.ar</span>
  <span class="respuesta">Contacto: Verónica Prieto</span>
  <span class="respuesta">Oficina:  0351-482-4180 / 0351 – 481-6911</span>
</div>