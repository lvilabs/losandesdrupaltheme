<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
<?php
$node = menu_get_object();

$precio = $node->field_aviso_precio[0]['view'];
if($precio != 'consultar'){
  //Detectamos si es un precio desde - hasta
  if(strpos($precio, 'a') !== false){
    $precio_exp = explode('a', $precio);
    $node_info->precio_hasta = $precio_exp[1];
    $precio_desde = explode(' ', $precio_exp[0]);
    $precio = implode('', $precio_desde);
  } else {
    $precio_exp = explode(' ', $precio);
    $precio = implode('', $precio_exp);
  }
}
$node_info->precio = $precio;
if($node->field_aviso_moneda[0]['value'] == 2) $moneda = 'USD'; else $moneda = 'ARS';
if($node->field_aviso_moneda[0]['value'] == 2) $moneda_comun = 'U$S'; else $moneda_comun = '$';
$node_info->moneda = $moneda;
$vendedor = ventas_get_vendedor($node->uid);

if(isset($node->precio_anterior_venta) && $node->precio_anterior_venta > 0)
  $descuento = ventas_get_monto_descuento($node->field_aviso_precio[0]['value'], $node->precio_anterior_venta);

if(isset($node->field_aviso_precio_uva))
  $node_info->precio_uva = $node->field_aviso_precio_uva[0]['value'];
?>

<?php if($node->type == 'aviso_alquiler_temporario'): ?>
  <div class="booking-section">
    <div class="inner">
      <span class="col-2 fl-left">
        <h3 class="precio-alojamiento"><?php print $node_info->precio; ?></h3>
        <p class="aclara">(por noche)</p>
        <input type="hidden" id="moneda-alojamiento" value="<?php print $moneda_comun; ?>" />
      </span>
      <p class="display-in-block mt20 text">Tus fechas <strong class="mensaje-disponible"></strong></p>
      <div class="extras">       
    </div>
    <div class="inner confirmacion">
      <p class="align-center sizemed"><i class="fa fa-clock-o"></i>Confirmación dentro de las 24 hs.</p>
    </div>
  </div>
<?php else: ?>
  <!--Comienzo precio y titulo-->
  <div class="AvisoTitulo FichaAuto<?php if(isset($emprendimiento) && isset($emprendimiento['logo'])) { print ' TituloConLogo'; } ?>">
    <div class="Borde clearfix">
      <h1 class="titulo-ficha" itemprop="name"><?php print truncate_utf8($node->title, 130, TRUE, FALSE); ?></h1>
<?php   if($node_info->precio != 'consultar'): ?>
            <div class="ContentPrecio" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
<?php       if($node->_workflow==WORKFLOW_AVISOS_ESTADO_PUBLICADO): ?>
              <link itemprop="availability" href="http://schema.org/InStock" content="In Stock" />
<?php       endif; ?>
<?php       if(isset($node_info->precio_hasta)): ?>
              <h3 class="precio-visible emprendimiento"><span itemprop="price" content="<?php print $node->field_aviso_precio[0]['value']; ?>"><?php print $node_info->precio; ?><br />a<br /><?php print $moneda_comun; ?><?php print trim($node_info->precio_hasta); ?></span></h3>              
<?php       else: ?>
              <?php if($node->type == 'aviso_producto'): ?>
                <span class="mensajePago">Precio en 1 pago</span>
              <?php endif; ?>
              <h3 class="precio-visible"><span itemprop="price" content="<?php print $node->field_aviso_precio[0]['value']; ?>"><?php print $node_info->precio; ?></span></h3>             
<?php       if($node_info->precio_uva) : ?>
              <div class="precio-uva"></div>
<?php       endif; ?>
              
<?php       endif; ?>
              <meta itemprop="priceCurrency" content="<?php print $node_info->moneda; ?>" />
<?php         if(isset($microdata_category)): ?>
<?php           print $microdata_category; ?>
<?php         endif; ?>

        <div class="PrecioAnterior">
        <?php if(isset($node->precio_anterior_venta) && $node->precio_anterior_venta>0): ?>         
            <span class="field"></span><span> <?php print $node->field_aviso_moneda[0]['view'].$node->precio_anterior_venta; ?><br></span>          
          <?php endif; ?>
        </div>
        <?php if($node->disponible_venta && $node->_workflow==WORKFLOW_AVISOS_ESTADO_PUBLICADO): ?>
          <div class="comprar">
            <a href="<?php print url('comprar/'.$node->nid.'/datos', array('alias'=>TRUE)); ?>" class="form-submit boton_comprar"><span></span>Comprar</a>
          </div>
        <?php endif; ?>
        <?php if($node->_workflow==WORKFLOW_AVISOS_ESTADO_PUBLICADO && isset($descuento) && $descuento['monto'] > 0): ?>
          <div class="descuento">
            <?php print 'Ahorras '.$moneda_comun.$descuento['monto'].' <strong>('.$descuento['porcentaje'].'%)</strong>'; ?> 
          </div>
        <?php endif; ?>
            </div>
<?php   else: ?>
              <div class="ContentPrecio">
                <h3 class="precio-visible"><span><?php print $node_info->precio; ?></span></h3>
              </div>
<?php   endif; ?>      
    </div>
  </div>
  <!--Fin precio y titulo-->
<?php endif; ?>

<?php if($node->disponible_venta && $node->_workflow==WORKFLOW_AVISOS_ESTADO_PUBLICADO && $node->type=='aviso_producto'): ?>
  <div class="Modelo">
    <div class="pago clearfix no-print">
      <div class="Modelo">
        <ul>
          <?php if(!empty($vendedor->mp_client_id)) : ?>
          <li class="medios_pagos">
            <div class="cuotas">
              <span class="field"></span><span class="financiacion">Hasta 12 cuotas <span></span> <?php //print $node->field_aviso_moneda[0]['view'].round($node->field_aviso_precio[0]['value']/6, 2); ?></span>
              <span class="Link"><a id="link-calculadora" class="link-calculadora-cuotas" href="javascript:void(0);" rel="noindex">Ver cuotas según tu banco</a></span>
            </div>
            <div class="MercadoPago">
              <span class="Logo1"></span><br />
            </div>
          </li>
          <?php endif; ?>
          <?php //if(!empty($vendedor->tp_merchant)) : ?>
          <li class="medios_pagos">
              <div class="cuotas">
                <span class="field"></span><span class="financiacion">Hasta 12 cuotas <span></span> </span>
                <?php if($vendedor->uid == 261106) { ?>
                  <span class="Link linkTodoPago"><a id="link-calculadora" class="link-calculadora-cuotas" rel="noindex">Sin interés</a></span>
                <?php } ?>
              </div>
              <div class="TodoPago">
              <span class="Logo1"></span>
              </div>
          </li>
          <?php //endif; ?>
          <?php if(!empty($vendedor->dm_account)) : ?>
          <li class="medios_pagos">
              <div class="cuotas">
                <span class="field"></span><span class="financiacion">Hasta 12 cuotas <span></span> </span>
              </div>
              <div class="DineroMail">
              <span class="Logo1"></span>
              </div>
          </li>
          <?php endif; ?>
          <?php if($vendedor->entrega_domicilio==1 && $vendedor->pago_domicilio==1) : ?>
          <li class="medios_pagos">
          <div class="PagoDomicilio">
            <span></span><p>También podés pagar cuando recibas el producto.</p>
          </div>
          </li>
          <?php endif; ?>
          <li class="Envio">
          <?php if($vendedor->entrega_domicilio==1): ?>
            <span class="field"></span>
            <span>
                Tiene envío a domicilio<br>
                <a href="javascript:void(0);" class="link-forma-envio" rel="noindex">Ver opciones de envío</a>
            </span>
          <?php elseif($vendedor->envio_andreani): ?>
            <span class="field"></span>
            <span>
                Tiene envío a domicilio por Andreani
            </span>
          <?php else: ?>
            <span class="field"></span>
            <span>
                No tiene envío a domicilio<br>
            </span>
          <?php endif; ?>
            <br>
          <?php if($vendedor->entrega_sucursal==1): ?>
            <span>
                Tiene retiro en sucursal<br>
                <a href="javascript:void(0);" class="link-forma-envio" rel="noindex">Ver opciones de entrega</a>
            </span>
          <?php else: ?>
            <span>
                No tiene retiro en sucursal<br>
            </span>
          <?php endif; ?>
          </li>
          <?php if(isset($node->taxonomy[CLASIFICADOS_RUBROS_MODA_TID]) && isset($node->stock_talles) && !empty($node->stock_talles)): ?>
          <li class="Talles">
            <span>Talles: </span>
            <span class="listaTalles">
              <?php
              foreach($node->stock_talles as $talle) {
                print '<span>'.$talle['talle'].'</span>';
              }
              ?>
            </span>
          </li>
          <?php endif; ?>
          <li class="info">
            <a href="javascript:void(0);" class="btn-vendedor link-datos-comercio" rel="noindex">Datos del comercio</a>
            <a href="javascript:void(0);" class="btn-vendedor link-contacto-vendedor" rel="noindex">Consultar</a>
          </li>
          <li class="Disponibilidad">
            <span>Cantidad disponible: </span><span class="Cantidad"> <?php (is_numeric($node->cantidad_venta))? print $node->cantidad_venta.' items' : print $node->cantidad_venta ?> </span>
          </li>
        </ul>
      </div>     
            
    </div>
  </div>
<?php endif; ?>
</div>

<?php if($node->disponible_venta && $node->type=='aviso_producto') : 
      $datos_tienda = user_load($node->uid); 
      $dominio_clvi = clasificados_apachesolr_obtener_usuario_url($node->uid);
?>
<div id="block-info-comercio-vendedor" class="block block-clasificados clearfix">
    <div class="descripcion_vendedor">
      <div class="nombre_vendedor">
        Información sobre el vendedor
      </div>
      <hr>
      <hr class="red">
      <div class="datos_vendedor">
        <div class="logo_vendedor"><img alt="Logo de <?php print $datos_tienda->profile_nombre_comercial; ?>" title="<?php print $datos_tienda->profile_nombre_comercial; ?>" src="<?php print '/'.$datos_tienda->picture; ?>" width="150"></div>
        <ul>
          <li><span class="logo_tienda"></span><span class="strong"><?php print $datos_tienda->profile_nombre_comercial; ?></span> es una tienda oficial en Clasificados La Voz </li>
          <li><span class="logo_candado"></span><span class="field">Comprar en esta tienda es <span class="strong">seguro</span>.</span> <a href="javascript:void(0);" class="link-saber-mas" rel="noindex">Ver más</a></li>
        </ul>
        <div class="links_vendedor">
          <a href="<?php print $dominio_clvi; ?>" class="btn-vendedor">Ver sus productos</a>
          <a href="javascript:void(0);" class="btn-vendedor link-contacto-vendedor" rel="noindex">Contactar al vendedor</a>
        </div>
      </div>
    </div>
</div>
<div class="DN">
  <input type="hidden" id="vendedor_uid" value="<?php print $node->uid; ?>">
  <input type="hidden" id="precio_venta" value="<?php print round($node->field_aviso_precio[0]['value'], 2); ?>">
  <div id="modal-forma-envio" class="modal-general">
    <h3>Formas de Entrega Disponibles</h3>
    <?php if($vendedor->entrega_domicilio==1) : ?>
    <div align="ContentCalcular">
    	<h4>Envío a domicilio</h4>
        <p>Ingresá tu dirección para calcular el costo de envío </p>
        <input type="text" id="direccion_costo_envio" title="Ej: San Martín 578, Córdoba, Córdoba" placeholder="Ej: San Martín 578, Córdoba, Córdoba">
        <input type="hidden" id="vendedor_uid_costo_envio" value="<?php print $node->uid; ?>">
        <input type="button" value="Calcular" class="form-submit calcular_costo_envio">
        <div class="BuscadorDireccion">
        </div>     
    </div>
    <?php endif; ?>
    <?php if($vendedor->entrega_sucursal==1) : ?>
    <div class="ContentSucursales">
    	<h4>Retirar en sucursal</h4>
        <div class="FondoSucursales">
        	<div class="Mapa" id="mapa_envio">

          </div>
          <div class="Direcciones">
              <?php 
              $lugares = ventas_get_lugares_entrega($node->uid);
              foreach($lugares as $lugar) {
              ?>
                <a href="#" class="Links lugar_entrega" data-latitud="<?php print $lugar->latitud; ?>" data-longitud="<?php print $lugar->longitud; ?>" data-nombre="<?php print $lugar->nombre; ?>" data-direccion="<?php print $lugar->direccion; ?>" rel="noindex"><?php print $lugar->direccion; ?></a>
              <?php 
              }
              ?>
          </div>
        </div>
    </div>
    <?php endif; ?>
  </div>
  <div id="modal-saber-mas" class="modal-general">
  <h3>Compra segura</h3>
    Todos los productos ofrecidos a través de las tiendas virtuales de <strong>Clasificados La Voz</strong> pertenecen a comercios previamente autorizados y verificados. <br />Publicitan sus productos de manera oficial a través de nuestras tiendas. Puede comprar con seguridad y tranquilidad. <br /><br />El proceso de compra es muy simple y fácil: Abona el producto, le llegará un correo confirmado y luego se comunicarán del comercio para ajustar detalles de entrega y/o retiro.<br />
    La facturación, logística, entrega y/o distribución es responsabilidad del comercio que publicita el producto, ante cualquier consulta sobre el producto puede comunicarse con el comercio en forma directa a través del formulario de contacto ubicado en cada aviso.<br /><br />
    Para conocer más sobre el servicio, sugerencias u observaciones puede escribirnos a <strong>clasificadosweb@lavozdelinterior.com.ar</strong>
  </div>
  <div id="modal-datos-comercio" class="modal-general">
  <h3>Datos del comercio</h3>
    <?php print $vendedor->texto_info_comercio; ?>
  </div>
</div>
<?php endif; ?>
