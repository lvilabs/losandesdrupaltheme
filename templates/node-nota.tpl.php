<?php global $base_url; ?>
<?php 
  $fecha_nota = clasificados_fecha_formato_nota(date('d/m/Y - H:i', $node->created)); 
  $term_nota = '';
  foreach($node->taxonomy as $term) {
    if($term->vid == 36)
      $term_nota = $term->name;
  }
  $con_video = 0;
  if((isset($node->field_nota_video_genoa[0]['value']) && !empty($node->field_nota_video_genoa[0]['value'])) || (isset($node->field_aviso_video[0]['embed']) && !empty($node->field_aviso_video[0]['embed'])))
    $con_video = 1;
?>
<article class="clearfix"> 
  <div class="nodeInfo <?php if($con_video) print 'video'; ?>"> 
    <header>
      <?php if ($con_video) { ?>
        <div class="panel-body panel-slider">
          <div class="contenido">
            <div class="contenido-titulo">
              <h1><?php print $node->title; ?></h1>
              <div class="footer-card">
                <div class="data"> 
                  <span class="nodeData clearfix"> 
                    <time datetime="<?php print date('Y-m-d H:i', $node->created); ?>"><?php print $fecha_nota; ?></time> &nbsp;•&nbsp; <?php print $term_nota; ?>
                  </span>
                </div>
              </div>
            </div>
          </div> 
          <?php 
          if(isset($node->field_nota_video_genoa[0]['value']) && !empty($node->field_nota_video_genoa[0]['value'])) 
            print $node->field_nota_video_genoa[0]['value'];
          else
            print $node->field_aviso_video[0]['view'];
          ?>
        </div>
      <?php } elseif ($node->field_nota_imagenes[0]["view"] != "") { ?>
      <div class="panel-body panel-slider">
        <div class="carrusel">
          <div class="contenido">
            <div class="contenido-titulo">
              <h1><?php print $node->title; ?></h1>
            </div>
            <div class="footer-card">
              <div class="data"> 
                <span class="nodeData clearfix"> 
                  <time atetime="<?php print date('Y-m-d H:i', $node->created); ?>"><?php print $fecha_nota; ?></time> &nbsp;•&nbsp; <?php print $term_nota; ?>
                </span>
              </div>
            </div>
          </div> 
            <script type="text/javascript">
              stepcarousel.setup({
                galleryid: 'CA1', 
                beltclass: 'belt', 
                panelclass: 'panel', 
                autostep: {enable:false, moveby:1, pause:4000},
                panelbehavior: {speed:1, wraparound:false, persist:false},
                defaultbuttons: {enable: true, moveby:1, leftnav: ['/<?php print path_to_theme(); ?>/img/sprite_slider_izq.png', 10, 330], rightnav: ['/<?php print path_to_theme(); ?>/img/sprite_slider_der.png', -80, 330]},
                statusvars: ['statusA_foto', 'statusB_foto', 'statusC_foto'], 
                contenttype: ['inline'], 
                oninit:function(){
                }
              });
            </script>
            <div id="CA1" class="stepcarousel">
              <div class="belt">
              <?php
                foreach($node->field_nota_imagenes as $images){ ?>
                <div class="panel">
                  <?php print $images['view']; ?>
                  <div class="Not">
                    <div class="OP clearfix">
                      <p><?php print $images['data']['description']; ?></p>
                    </div>
                    <div class="BG"></div>
                  </div>
                </div>
                <?php }
              ?>
              </div>
            </div>
        </div>
        <div class="caption">
          <p ng-bind-html="caption" class="ng-binding"></p>
        </div>
      </div>
      <?php } else { ?>  
        <div class="contenido">
          <h1><?php print $node->title; ?></h1>
          <div class="footer-card">
            <div class="data"> 
              <span class="nodeData clearfix"> 
                <time datetime="<?php print date('Y-m-d H:i', $node->created); ?>"><?php print $fecha_nota; ?></time> &nbsp;•&nbsp; <?php print $term_nota; ?>
              </span>
            </div>
          </div>
        </div>
      <?php } ?>
    </header> 
  </div>
  <div class="nodeFull clearfix">
    <div> 
      <?php if(!empty($node->field_nota_autor)) { ?>
      <div class="author-collapsible">
        <div class="ng-scope">
          <span class="author-name"><span class="neutro">Por</span> <?php print $node->field_nota_autor[0]['value']; ?></span>
        </div>
      </div>
      <?php } ?>
      <main> 
        <div count="shareCount.facebook" class="ng-isolate-scope">
          <ul class="user-share">
            <li class="sfb ng-scope">
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print $base_url.'/'.$node->path; ?>" target="_blank" class="share-item ng-isolate-scope">
                <div class="sprite-compartir sprite-facebook">
                  <i class="fab fa-facebook-f"></i>
                </div>
              </a>
            </li>
            <li class="stw ng-scope">
              <a href="http://twitter.com/share?text=<?php print $node->title; ?>&url=<?php print $base_url.'/'.$node->path; ?>" target="_blank" class="share-item" >
                <div class="sprite-compartir sprite-twitter">
                  <i class="fab fa-twitter"></i>
                </div>
              </a>
            </li>
          </ul>
        </div>
        <div class="teaser">
          <?php print $node->field_nota_resumen[0]['value']; ?>
        </div> 
        <loginwall hide="1" class="ng-isolate-scope">
          <div class="body">
            <?php print $node->content['body']['#value']; ?>
          </div>
        </loginwall> 
        <div count="shareCount.facebook" class="ng-isolate-scope">
          <ul class="user-share">
            <li class="sfb ng-scope">
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print $base_url.'/'.$node->path; ?>" target="_blank" class="share-item ng-isolate-scope">
                <div class="sprite-compartir sprite-facebook">
                  <i class="fab fa-facebook-f"></i>
                </div>
              </a>
            </li>
            <li class="stw ng-scope">
              <a href="http://twitter.com/share?text=<?php print $node->title; ?>&url=<?php print $base_url.'/'.$node->path; ?>" target="_blank" class="share-item" >
                <div class="sprite-compartir sprite-twitter">
                  <i class="fab fa-twitter"></i>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </main>
    </div>
    
    <aside>
    <?php
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_rectangle_1';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      print theme('block', $block);
    ?>
    </aside>
    
  </div>
</article>