<?php
  //print_r($form);
  global $user;
  $conf = db_fetch_object(db_query("SELECT sc.estado_cabezal 
FROM clvi_admin_sitios_configuracion sc
WHERE sc.sitio_id = %d AND sc.uid = %d;", SITIO_LOCAL_ID, $user->uid));
?>
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/js/lib/colorbox/styles/default/colorbox_default_style.css" />
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/configuracion.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/font-awesome/css/font-awesome.min.css">
<!-- Page Content -->
<div id="main" class="container-fluid page-info">                        
  <!-- Boxed Container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="overflow: hidden;">
        <div class="title-section info">
          <h1>Dar de alta tu web</h1>
          <h4>Contamos con diferentes productos que pueden adaptarse a tus necesidades y a tu presupuesto.</h4>
        </div>                     
      </div>
      <div class="col-lg-12">
        <div class="price-list">
          <ul class="list-group col-lg-4 col-md-2 padding-right col-lg-offset-1 text-left">
            <li class="list-group-item empty"></li>
            <li class="list-group-item"><div class="input-group">Micrositio en <br> Clasificados LaVoz.com.ar</div>  <span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a tener una mejor presencia dentro de Clasificados LaVoz.com.ar con tu micrositio personalizado con más información institucional para diferenciarte de la competencia.">+</span></li>
            <li class="list-group-item"><div class="input-group">Republicación automática <br> de tus avisos en <br> Clasificados LaVoz.com.ar</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Tus avisos se republicarán automáticamente una vez por día.">+</span></li>
            <li class="list-group-item"><div class="input-group">Información <br> institucional</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Quiénes somos, sección de Contáctenos.">+</span></li>
            <!-- <li class="list-group-item"><div class="input-group">Subdominio <br> Sitios de La Voz</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Podés tener tu diseño con una dirección web dentro de nuestros servidores. Por ejemplo, tuempresa.sitioslavoz.com.ar.">+</span></li>
            <li class="list-group-item"><div class="input-group">Dominio Propio <br> de tu empresa</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Podés tener tu diseño con tu dirección web propia Por ejemplo, www.tuempresa.com.ar.">+</span></li>
            <li class="list-group-item"><div class="input-group">Contenido <br> Dinámico</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a tener los mismos filtros dinámicos que Clasificados dentro de tu sitio.">+</span></li>
            <li class="list-group-item"><div class="input-group">Home con <br> avisos Destacados</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Vas a poder elegir qué avisos se muestran destacados en tu página de inicio.">+</span></li>
            <li class="list-group-item"><div class="input-group">Beneficios en <br> Banneri <a href="http://www.lavozglobal.com.ar/banneri/" target="_blanck" style="color:#2980B9;">(link)</a></div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Querés promocionar y posicionar tu sitio? Vas a tener condiciones especiales para contratar los servicios de Banneri y comunicar tu sitio en la red de La Voz y en Google.">+</span></li>
            <li class="list-group-item"><div class="input-group">Adaptación a <br> mobile</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Tu sitio tendrá una versión mobile, tanto para celulares como para tabletas.">+</span></li>
            <li class="list-group-item margin-bottom"><div class="input-group">Migraciós de dominio <br> actual y servicio de mail</div><span class="input-group-btn" data-toggle="tooltip" data-placement="right" title="Si ya tenés tu sitio web y querés usar nuestro sistema, podemos migrar tu sitio y tus mail a nuestro sistema.">+</span></li> -->
            <!-- <li class="list-group-item margin-top margin-bottom centrado"><b>Alta inicial</b></li>
            <li class="list-group-item margin-top margin-bottom centrado"><b>Mantenimiento mensual</b></li> -->
          </ul>
          <ul class="list-group col-lg-4 col-md-2 padding-right padding-left text-center">
            <li class="list-group-item cabezal">Micrositio en <br> Clasificados La Voz</li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <!-- <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item margin-bottom"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li> --> 
            <!-- <li class="list-group-item margin-top margin-bottom"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/lineIcon.jpg"></li>
            <li class="list-group-item margin-top margin-bottom centrado"><b>$300</b></li> -->
            
            <?php if(empty($conf) || $conf->estado_cabezal == 0) { ?>
              <li class="list-group-item last"><a href="#mensaje-alta-op1" class="inline"><button class="btn btn-lg btn-default btn-blue">ME INTERESA</button></a></li>
            <?php } ?>
            
          </ul>
          <!-- <ul class="list-group col-lg-2 col-md-2 padding-right padding-left text-center">
            <li class="list-group-item cabezal">Tu Web Autogestionale <br> Subdomimo Clasificados</li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item margin-bottom"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>                    
            <li class="list-group-item margin-top margin-bottom centrado"><b>$1500</b></li>
            <li class="list-group-item margin-top margin-bottom centrado"><b>$600</b></li>
            <li class="list-group-item last"><a href="#mensaje-alta-op2" class="inline"><button class="btn btn-lg btn-default btn-blue">ME INTERESA</button></a></li>
          </ul> -->
          <!-- <ul class="list-group col-lg-2 col-md-2 padding-right padding-left text-center">
            <li class="list-group-item cabezal">Tu Web + mobile Autoges. <br> Dominio propio</li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item margin-bottom centrado">Opcional</li>                    
            <li class="list-group-item margin-top margin-bottom centrado"><b>$3000</b></li>
            <li class="list-group-item margin-top margin-bottom centrado"><b>$700</b></li>
            <li class="list-group-item last"><a href="#mensaje-alta-op3" class="inline"><button class="btn btn-lg btn-default btn-blue">ME INTERESA</button></a></li>
          </ul> -->
          <!-- <ul class="list-group col-lg-2 col-md-2 padding-left text-center">
            <li class="list-group-item cabezal">Una Web a tu <br> Medida + mobile</li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></i></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/redIcon.jpg"></li>   
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item"><img src="/<?php print drupal_get_path('module', 'administracion_sitios'); ?>/img/principal/greenIcon.jpg"></li>
            <li class="list-group-item margin-bottom centrado">Opcional</li>                    
            <li class="list-group-item margin-top margin-bottom centrado"><b>Variable</b></li>
            <li class="list-group-item margin-top margin-bottom centrado"><b>Variable</b></li>
            <li class="list-group-item last"><a href="#mensaje-alta-op4" class="inline"><button class="btn btn-lg btn-default btn-blue">ME INTERESA</button></a></li>
          </ul> -->                                                     
        </div>
      </div>                
    </div>
  </div>
  <!-- /.row -->
  <?php print drupal_render($form); ?>
</div>
<!-- /.container -->

<div style="display:none;">
  <div id="mensaje-alta-op1" class="contaco-comercial">
    <h4>Gracias por contactarnos: </h4>
    <p>
      Ya estás más cerca de tener tu sitio web! <br>
      Contactate con nosotros al (0351) 4757000 o hacé clic en Enviar y en breve se contactará uno de nuestros ejecutivos de ventas para informarte sobre cómo son los pasos a seguir para terminar de configurar tu sitio.
    </p>
    <a class="envio-contacto" href="Micrositio en Clasificados La Voz">Enviar</a>
    <div style="display:none;" class="contacto-mensaje"></div>
  </div>
  <div id="mensaje-alta-op2" class="contaco-comercial">
    <h4>Gracias por contactarnos: </h4>
    <p>
      Ya estás más cerca de tener tu sitio web! <br>
      Contactate con nosotros al (0351) 4757000 o hacé clic en Enviar y en breve se contactará uno de nuestros ejecutivos de ventas para informarte sobre cómo son los pasos a seguir para terminar de configurar tu sitio.
    </p>
    <a class="envio-contacto" href="Tu Web Autogestionale Sub-domimo Clasificados">Enviar</a>
    <div style="display:none;" class="contacto-mensaje"></div>
  </div>
  <div id="mensaje-alta-op3" class="contaco-comercial">
    <h4>Gracias por contactarnos: </h4>
    <p>
      Ya estás más cerca de tener tu sitio web! <br>
      Contactate con nosotros al (0351) 4757000 o hacé clic en Enviar y en breve se contactará uno de nuestros ejecutivos de ventas para informarte sobre cómo son los pasos a seguir para terminar de configurar tu sitio.
    </p>
    <a class="envio-contacto" href="Tu Web + mobile Autogestionale Dominio propio">Enviar</a>
    <div style="display:none;" class="contacto-mensaje"></div>
  </div>
  <div id="mensaje-alta-op4" class="contaco-comercial">
    <h4>Gracias por contactarnos: </h4>
    <p>
      Ya estás más cerca de tener tu sitio web! <br>
      Contactate con nosotros al (0351) 4757000 o hacé clic en Enviar y en breve se contactará uno de nuestros ejecutivos de ventas para informarte sobre cómo son los pasos a seguir para terminar de configurar tu sitio.
    </p>
    <a class="envio-contacto" href="Una Web a tu Medida + mobile">Enviar</a>
    <div style="display:none;" class="contacto-mensaje"></div>
  </div>
  <!-- <div id="mensaje-alta-op4" class="contaco-comercial">Mensaje Alta. 
    <a class="envio-contacto" href="opcion4">Enviar</a>
    <div style="display:none;" class="contacto-mensaje"></div>
  </div> -->
</div>

<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/lib/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/alta.js"></script>