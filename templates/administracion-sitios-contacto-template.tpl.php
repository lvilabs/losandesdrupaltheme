<div id="tabla_animada">
  <div class="pricing_table">
    <ul class="primero">
      <li class="tit_price">Básico</li>
      <li class="precio_li"><span class="cont_precio precio_uno">$359<span class="xmes">x mes + IVA</span></span></li>
      <li><a href="http://administrador.sumaprop.com/registro/" target="_blank" class="btn btn-success btn-large">Probar gratis</a></li>
      <li><span class="destacar">15</span> Propiedades</li>
      <li><span class="destacar">12</span> Modelos de Sitios Web</li>
      <li><span class="destacar">1</span> Agente</li>
      <li><span class="destacar"></span></li>
    </ul>
    
    <ul class="segundo">
      <li class="tit_price plus"><span>Plus</span></li>
      <li class="precio_li plus"><span class="cont_precio precio_dos">$469<span class="xmes">x mes + IVA</span></span></li>
      <li><a href="http://administrador.sumaprop.com/registro/" target="_blank" class="btn btn-success btn-large">Probar gratis</a></li>
      <li><span class="destacar">50</span> Propiedades</li>
      <li><span class="destacar">19</span> Modelos de Sitios Web</li>
      <li><span class="destacar">5</span> Agentes</li>
      <li><span class="destacar"></span></li>
    </ul>
    
    <ul class="tercero">
      <li class="tit_price premium"><span>Premium</span></li>
      <li class="precio_li premium"><span class="cont_precio precio_tres">Consultar</span></li>
      <li><a href="http://administrador.sumaprop.com/registro/" target="_blank" class="btn btn-success btn-large">Probar gratis</a></li>
      <li><span class="destacar">ILIMITADAS</span></li>
      <li><span class="destacar">22</span> Modelos de Sitios Web</li>
      <li><span class="destacar">Agentes Ilimitados</span></li>
      <li><span class="destacar">Sucursales Ilimitadas</span></li>
    </ul>
      
  </div><!--.pricing_table-->
</div>