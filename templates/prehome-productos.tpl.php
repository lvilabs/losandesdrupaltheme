<!-- Comienzo LOGOS TIENDAS-->
<?php
  //$logos_tienda = ventas_vendedores_logos_home();
  $logos_tienda = '';
  if(!empty($logos_tienda)) {
?>
<div class="BloqueListaTiendas AvisosDestacadosSlide clearfix">
  <div class="Title">
    <hr>
    <a href="#" title="Listado de comercios oficiales de Clasificados La Voz" rel="search"><h2>Conocé nuestros comercios oficiales</h2></a>
  </div>
  <div class="callbacks_container">
    <ul id="listaTiendas" class="rslidesNav">
      <?php
        $cont = 0;
        foreach($logos_tienda as $tienda) {
          if($cont == 0) { ?>
      <li>
        <?php } ?>
        <?php if(strpos($tienda['url'], 'search/apachesolr_search?f[0]=is_uid') === false)
            $url_cxense = $tienda['url'].'?cx_level=logo_tienda_home';
          else
            $url_cxense = $tienda['url'].'&cx_level=logo_tienda_home';
        ?>
        <a title="Listado de avisos de comercio <?php print $tienda['nombre']; ?>" href="<?php print $url_cxense; ?>" rel="search"><img title="Logo de comercio <?php print $tienda['nombre']; ?>" alt="<?php print $tienda['nombre']; ?> en productos La Voz" src="<?php print $tienda['logo']; ?>" ></a>
        <?php if($cont == 2) { ?>
      </li>
        <?php 
            $cont = 0; 
          } else { 
            $cont++; 
          }
        } 
        if($cont <> 0) { ?>
      </li>
        <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>
  

<!-- Comienzo CARRUCEL DE AVISOS-->
<?php
  $avisos_carrucel = clasificados_destacados_home_carrucel_productos();
  if(!empty($avisos_carrucel)) {
?>
<div class="BloqueListaProductos AvisosDestacadosSlide clearfix">
  <div class="Title">
    <hr>
    <h2>Productos Destacados</h2>
  </div>
  <div class="callbacks_container">
    <ul id="listaProductos" class="rslidesNav">
      <?php
        $cont = 0;
        foreach($avisos_carrucel as $aviso) {
          if($cont == 0) { ?>
      <li>
        <?php } ?>
        <div class="CajaAviso">
          <div class="imgAviso">
            <a href="/<?php print $aviso['url']; ?>" title="<?php print $aviso['titulo']; ?>">
              <span></span>
              <?php print theme('imagecache', 'ficha_aviso_173_115_sc', $aviso['imagen'], $aviso['titulo'].' en productos Los Andes', 'Inagen producto '.$aviso['titulo']); ?>
            </a>
          </div>
          <div class="AvisoDescripcion">
            <a href="/<?php print $aviso['url']; ?>" title="<?php print $aviso['titulo']; ?>"><h4><?php print $aviso['titulo']; ?></h4></a>
            <div><span class="precio"><?php print $aviso['precio']; ?></span></div>  
          </div>
        </div>
        <?php if($cont == 3) { ?>
      </li>
        <?php 
            $cont = 0; 
          } else { 
            $cont++; 
          }
        } 
        if($cont <> 0) { ?>
      </li>
        <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<?php
$module = 'clasificados_banners';
$delta = 'dfp_banner_middle_1';
$block = (object) module_invoke($module, 'block', 'view', $delta);
$block->module = $module;
$block->delta = $delta;
print theme('block', $block);
?>
