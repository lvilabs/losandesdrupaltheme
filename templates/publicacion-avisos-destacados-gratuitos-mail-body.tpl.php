<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="620" cellspacing="0" cellpadding="0" border="1" style="width:465.0pt;background:white;border-collapse:collapse;border:none">
          <tbody>
            <tr>
              <td style="border:none;background:transparent;padding:0cm 0cm 0cm 0cm" colspan="2">
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">!usuario_nombre, destacá tu aviso publicado en Clasificados Los Andes.<u></u><u></u></span></b></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Link a publicación: !link_publicacion<u></u><u></u></span></p>
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Fecha de publicación: !aviso_fecha_creacion<u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>
        <p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><u></u><u></u></span></p>
      </td>
    </tr>
  </tbody>
</table>