<table width="625" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;border-bottom:1px dotted #343434;">
        <table>
          <tbody>
            <tr>
              <td colspan="2"><span style="font-size:16.0pt;color:#343434;font-family:Arial,sans-serif;">Se acredit&oacute; el pago de !nombre_comprador por el siguiente aviso:</span>
              </td>
            </tr>
            <tr>
              <td valign="middle" width="173" style="text-align:center;">
                <img src="!foto" width="173" height="115">
              </td>
              <td valign="top" width="452">
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <span style="font-size:18.0pt;line-height:22pt;font-weight:bold;color:#343434;font-family:Arial,sans-serif;">!titulo_aviso</span>
                      </td>
                    </tr>
                    <tr>
                      <td><span style="font-size:10.0pt;color:#808080;">
                        
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td valign="top" >
        <br>
        <span style="font-size:16.0pt;color:#808080;text-align:center;font-family:Arial,sans-serif;">
          <center>
            <i>          
              Datos del comprador<br>
              Nombre: !nombre_comprador<br>
              Tel&eacute;fono: !telefono_comprador<br>
              Email: !email_comprador<br>
              Direcci&oacute;n: !direccion_comprador, !ciudad_comprador, !provincia_comprador<br>
            </i>
          </center>
        </span>
        <br>
      </td>
    </tr>
    <tr>
      <td valign="top" >
        <br>
        <span style="font-size:16.0pt;color:#808080;text-align:center;font-family:Arial,sans-serif;">
          <center>
            <i>          
              Datos de la compra<br>
              Aviso: !titulo_aviso<br>
              !talle
              Cantidad: !cantidad<br>
              Descuento: !descuento<br>
              Total: !monto<br>
              Medio de Pago: !medio_pago<br>
              M&eacute;todo de env&iacute;o: !metodo_envio.<br> 
              Código de seguimiento: !codigo_seguimiento.<br>
            </i>
          </center>
        </span>
        <br>
      </td>
    </tr>
    <tr>
      <td valign="top" height="30" style="padding:10px 0 0 0;color:#343434; background-color:#CCE8F6; text-align:center;font-size:10.0pt;font-family:Arial,sans-serif;">
        Ver aviso <a href="!link_aviso" style="color:#343434;" >!link_aviso</a>
      </td>
    </tr>
  </tbody>
</table>