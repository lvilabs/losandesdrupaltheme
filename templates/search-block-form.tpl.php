<?php

/**
 * @file search-block-form.tpl.php
 * Default theme implementation for displaying a search form within a block region.
 *
 * Available variables:
 * - $search_form: The complete search form ready for print.
 * - $search: Array of keyed search elements. Can be used to print each form
 *   element separately.
 *
 * Default keys within $search:
 * - $search['search_block_form']: Text input area wrapped in a div.
 * - $search['submit']: Form submit button.
 * - $search['hidden']: Hidden form elements. Used to validate forms when submitted.
 *
 * Since $search is keyed, a direct print of the form element is possible.
 * Modules can add to the search form so it is recommended to check for their
 * existance before printing. The default keys will always exist.
 *
 *   <?php if (isset($search['extra_field'])): ?>
 *     <div class="extra-field">
 *       <?php print $search['extra_field']; ?>
 *     </div>
 *   <?php endif; ?>
 *
 * To check for all available data within $search, use the code below.
 *
 *   <?php print '<pre>'. check_plain(print_r($search, 1)) .'</pre>'; ?>
 *
 * @see template_preprocess_search_block_form()
 */
$what_was_searched = $_GET['q'];

if(strpos($what_was_searched, 'search/apachesolr_search/') !== FALSE) {
  $what_was_searched = str_replace('search/apachesolr_search/', '', $what_was_searched);
} else {
  $what_was_searched = '';
}

?>
<div class="container-inline">
  <div class="form-item" id="edit-search-block-form-1-wrapper">
    <input type="text" maxlength="128" name="search_block_form" id="texto_busqueda" size="15" value="<?php print htmlentities($what_was_searched, ENT_QUOTES, 'utf-8') ;?>" title="" class="form-text select-negro" />
  </div>
  <input type="submit" name="op" id="edit-submit" value="Buscá ahora"  class="form-submit IcoBuscar" />
  <?php print $search['hidden']; ?>
</div>

