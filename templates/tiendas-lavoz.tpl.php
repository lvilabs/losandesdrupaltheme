<?php 
$get = $_GET; 
$filtro_ciudades = ventas_vendedores_ciudades_activos();
$filtro_rubros = ventas_vendedores_rubros_activos();
?>
<div class="tiendas">
  <div class="head_comercios">
    Estos son los Comercios Aliados con Clasificados La Voz para venta online de productos.
  </div>
  <form action="/tiendas_lavoz" accept-charset="UTF-8" method="get" id="filtros_comercios">
    <div class="views-exposed-form">  
      <div class="views-exposed-widget clear-block">
        <label for="edit-tienda">Nombre Comercial</label><br />
        <input type="text" maxlength="128" name="tienda" id="edit-tienda" size="30" value="<?php print $get['tienda']; ?>" class="form-text">
      </div>
      <div class="views-exposed-widget views-widget-filter-value_2">
        <label for="edit-ciudad">Ciudad</label><br />
        <select name="ciudad" class="form-select" id="edit-ciudad">
          <option value="" selected="selected">- Todos -</option>
          <?php foreach($filtro_ciudades as $kciudad => $ciudad) { ?>
            <option value="<?php print $kciudad; ?>" <?php if($kciudad == $get['ciudad']) print 'selected'; ?>><?php print $ciudad; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="views-exposed-widget views-widget-filter-value_3">
        <label for="edit-barrio">Rubro</label><br />
        <select name="rubro" class="form-select" id="edit-rubro">
          <option value="" selected="selected">- Todos -</option>
          <?php foreach($filtro_rubros as $krubro => $rubro) { ?>
            <option value="<?php print $krubro; ?>" <?php if($krubro == $get['rubro']) print 'selected'; ?>><?php print $rubro; ?></option>
          <?php } ?>
        </select>
      </div>
      <input type="submit" value="Filtrar" class="form-submit">
    </div>
  </form>
  
  <?php
  $ultimo_rubro = '';
  foreach($vendedores as $vendedor) {
    
    if(!$vendedor->estado)
      continue;
    if($vendedor->subrubro==0) {
      continue;
    }
    if($ultimo_rubro=='' || $ultimo_rubro!=$vendedor->subrubro) {
      print '<div class="Title"><hr><h3><strong>'.$vendedor->name.'</strong></h3></div>';
      $ultimo_rubro = $vendedor->subrubro;
    } else {
      print '<hr>';
    }
    $inmo = user_load($vendedor->uid);
    $inmo_picture = $inmo->picture;
    $inmo_nombre = $inmo->profile_nombre_comercial;
    /*$inmo_tel1 = $inmo->profile_telefono_principal;
    $inmo_mail = $inmo->mail;
    $inmo_web = $inmo->profile_sitio_web;*/
    $inmo_lat = $inmo->location['locpick']['user_latitude'];
    $inmo_long = $inmo->location['locpick']['user_longitude'];
  ?>
  <div class="fondoConcesionaria clearfix">
    <div class="Borde clearfix">
      <div class="Logo Left">
        <?php if($inmo_picture != '') print clasificados_imagecache('logo_200_160', $inmo_picture, $inmo_nombre, $inmo_nombre); ?>
      </div>
      <div class="Info Left tienda-<?php print $vendedor->uid; ?>">
        <div class="inmo-title"><?php print $inmo_nombre; ?></div>
        <div class="info-tienda tienda-<?php print $vendedor->uid; ?>"><?php print $vendedor->texto_info_comercio; ?></div>
        <?php if($vendedor->texto_info_comercio != '') { ?>
        <span class="<?php print $vendedor->uid; ?>"></span>
        <div class="descripcion_vendedor">
          <div class="nombre_vendedor"><?php print $inmo_nombre; ?></div>
          <p><?php print $vendedor->texto_info_comercio; ?></p>
        </div>
        <?php } ?>
        <div class="link-avisos"><a href="/search/apachesolr_search?f[0]=is_uid:<?php print $vendedor->uid; ?>" class="otrosAutos">Ver avisos de esta tienda</a></div>
      </div>
      <div class="formAgencia Left">
        <?php if($inmo_lat != '' && $inmo_long != ''){ ?>
          <img src="http://maps.google.com/maps/api/staticmap?center=<?php print $inmo_lat; ?>,<?php print $inmo_long; ?>&zoom=16&size=330x160&maptype=hybrid&markers=color:0xFFE800|<?php print $inmo_lat; ?>,<?php print $inmo_long; ?>&sensor=false" />
        <?php } ?>
      </div>
    </div>
  </div>
  <?php
  }
  ?>
</div>
<div class="clearfix"></div>