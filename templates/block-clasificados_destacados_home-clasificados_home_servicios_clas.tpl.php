<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="AvisosDestacadosSlide clearfix">
    <div class="Title">
    	<hr>
      <a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017" title="Avisos destacados de servicios" rel="search"><h3>Servicios destacados</h3></a>
    </div>
    <div class="navigation">
      <ul>
        <li><strong>Buscar por</strong></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6022" title="Construcción y Refacciones" rel="search">Construcción y Refacciones</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6048" title="Oficios y Profesiones" rel="search">Oficios y Profesiones</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6759" title="Salud, Bienestar y Estética" rel="search">Salud, Bienestar y Estética</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6039" title="Servicios Financieros" rel="search">Servicios Financieros</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6670" title="Gastronomía y Delivery" rel="search">Gastronomía y Delivery</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017&f[1]=im_taxonomy_vid_34:6033" title="Fiestas y Eventos" rel="search">Fiestas y Eventos</a></li>
      </ul>
      <div class="destacados-ver-mas"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017" title="Ver más avisos de servicios" rel="search">Ver Más</a></div>
    </div>
    <div class="accessible_news_slider slider_tienda">
<?php if(count($content['avisos'])>3) : ?>
      <div class="Botones">
        <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
      </div>
<?php endif; ?>
      <div class="lista">
        <ul class="clearfix bloque_avisos Slider">
<?php for($i=0; $i<count($content['avisos']); $i++) : ?>
          <li>
            <div class="CajaAviso ">
              <a href="<?php print $content['avisos'][$i]['url']; ?>?cx_level=destacados_home_servicios" title="<?php print $content['avisos'][$i]['titulo']; ?>">
                <div class="imgAviso">
                  <?php
                    $alt_img = explode('/', $content['avisos'][$i]['url']);
                    $alt_img = $alt_img[0].' de '.$alt_img[1].' '.$content['avisos'][$i]['titulo'].' a '.$content['avisos'][$i]['precio'];
                  ?>
                  <img src="<?php print $content['avisos'][$i]['foto']; ?>" alt="<?php print $alt_img; ?>" title="<?php print $content['avisos'][$i]['titulo']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211">
                </div>
                <div class="AvisoDescripcion">
                  <h4><?php print $content['avisos'][$i]['titulo']; ?></h4>
                  <p>
                  </p>
                  <span class="precio"><?php print $content['avisos'][$i]['precio']; ?></span>
                </div>
              </a>
            </div>
          </li>
<?php endfor; ?>
        </ul>
      </div> 
    </div>
  </div>
</div>