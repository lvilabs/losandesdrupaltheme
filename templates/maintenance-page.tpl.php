<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" xmlns:fb="http://ogp.me/ns/fb#">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <link media="screen" href="http://static.lavozdelinterior.com.ar/themes/lavoz/css/menu/superfish.css" type="text/css" rel="stylesheet" />
  <link media="screen" href="http://static.lavozdelinterior.com.ar/themes/lavoz/css/menu/superfish-navbar.css" type="text/css" rel="stylesheet" />
  <?php print $styles; ?>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
</head>
<body class="<?php print $classes; ?>">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>

  <div class="Header SinLoguear">
    <?php include dirname(__FILE__).'/includes/header.tpl.php'; ?>
  </div>

<?php if($page_arriba): ?>
  <div style="margin-bottom:15px" class="Columnas clearfix">
  <?php print $page_arriba; ?>
  </div>
<?php endif; ?>
  
<div class="Contenido">

  <div class="Columnas clearfix">
    <div class="CD">
      <?php print $page_media; ?>
      <div id="content">
        <?php print $messages; ?>
        
        <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>
          
        <div id="content-area">
          <?php print $content; ?>
        </div>
      </div>
    </div>
  </div>
</div>

  <?php include dirname(__FILE__).'/includes/footer.tpl.php'; ?>
  
  <?php print $closure; ?>

</body>
</html>