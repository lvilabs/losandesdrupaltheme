<?php
// $Id: views-view.tpl.php,v 1.13.2.2 2010/03/25 20:25:28 merlinofchaos Exp $
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
?>
<div class="Content Guia clearfix">
  <div class="ContactoAutos">
    <div class="Sombra">
      <div class="Borde clearfix"><h1>Guías Autos</h1></div>
    </div>
    <div class="blanco">
      <?php foreach($view->display_handler->default_display->view->style_plugin->rendered_fields as $nota){ ?>
        <div class="module-lista clearfix">
          <?php print $nota['field_nota_imagenes_fid']; ?>
          <h3><strong><?php print $nota['title']; ?></strong></h3>
          <p><?php print $nota['field_nota_resumen_value']; ?></p>
          <a target="_blank" href="<?php print $nota['field_nota_link_value']; ?>"><?php print $nota['field_nota_texto_link_value']; ?></a>          
        </div>
      <?php } ?>      
    </div>
  </div>
  <div class="clear0">&nbsp;</div>
</div>
<?php if($pager != "") { ?>
  <div class="ResultadoB">
    <div class="paginado">
      <?php print $pager; ?>
    </div>
  </div>
<?php } ?>