<?php
/**
 * @file
 * Theme implementation to display a view of messages.
 */
global $user;

$is_search = in_array(arg(0), array('search', 'taxonomy')); //taxonomy = category
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php print $scripts; ?>
  <?php include(dirname(__FILE__).'/includes/dfp-google.php'); ?>
</head>
<body class="<?php print $classes; ?>" itemscope itemtype="http://schema.org/WebPage">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>
<?php global $user; ?>

  <div class="Header SinLoguear">
    <?php include dirname(__FILE__).'/includes/header.tpl.php'; ?>
  </div>

<?php if($page_arriba): ?>
  <div style="margin-bottom:15px" class="Columnas clearfix">
  <?php print $page_arriba; ?>
  </div>
<?php endif; ?>

  <div class="Contenido">
    <div class="Columnas clearfix">
      <div class="CT">
<?php if(! $is_front): ?>
  <?php print $breadcrumb; ?>
<?php endif; ?>
      </div>
    </div>

    <div class="Columnas clearfix">

      <div class="CT">

      <?php print $page_media; ?>
      
        <div id="content">
          <div id="system-messages-wrapper" style="display:none">
            <div id="system-messages">
        <?php print $messages; ?>
            </div>
            <a class="system-messages-inline" href="#system-messages" rel="noindex">Mensaje de sistema</a>
          </div>
          
        <?php
          if(in_array('concesionaria terceros', $user->roles)){
            $module = 'clasificados_banners';
            $delta = 'dfp_banner_topsite';
            $block = (object) module_invoke($module, 'block', 'view', $delta);
            $block->module = $module;
            $block->delta = $delta;
            print theme('block', $block);
          }
        ?>
          
          <div id="content-area" class="MiCuenta clearfix">
            <?php if($user->uid!=0) { 
                if(in_array('colaborador inmobiliaria', $user->roles)){
                  include dirname(__FILE__).'/includes/menu-cuenta-solapas-colaborador.tpl.php';
                } else {
                  include dirname(__FILE__).'/includes/menu-cuenta-solapas.tpl.php';
                }
              }
            ?>
            <?php 
            /* if(arg(0) == 'user') 
              include dirname(__FILE__).'/includes/tags-edicion-cuenta.tpl.php'; 
            if(arg(1) == 'agenda') 
              include dirname(__FILE__).'/includes/tabs-agenda.tpl.php'; 
            */
            ?>
            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print $content; ?>
          </div>
        </div>

      <?php print $page_inferior; ?>
      </div>

    </div>
  </div>

  <?php include dirname(__FILE__).'/includes/footer.tpl.php'; ?>
  
  <?php print $closure; ?>

</body>
</html>