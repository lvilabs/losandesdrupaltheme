<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="AvisosDestacadosSlide clearfix">
    <div class="Title">
    	<hr>
      <a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106" title="Avisos destacados de productos" rel="search"><h2>Productos destacados</h2></a>
    </div>
    <div class="navigation">
      <ul>
        <li><strong>Buscar por</strong></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6218" title="Informática y Computación" rel="search">Informática y Computación</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6154" title="Electrodomésticos" rel="search">Electrodomésticos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6255" title="Celulares y Teléfonos" rel="search">Celulares y Teléfonos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6186" title="Hogar y Muebles" rel="search">Hogar y Muebles</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:7099" title="Accesorios para Vehículos" rel="search">Accesorios para Vehículos</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6609" title="Oficina y Comercio" rel="search">Oficina y Comercio</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6275" title="Ropa y Accesorios" rel="search">Ropa y Accesorios</a></li>
        <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106&f[1]=im_taxonomy_vid_34:6266" title="Delicatesen, Vinos y Bebidas" rel="search">Delicatesen, Vinos y Bebidas</a></li>
      </ul>
      <div class="destacados-ver-mas"><a href="/productos" title="Ver más avisos de productos">Ver Más</a></div>
    </div>
    <div class="accessible_news_slider slider_tienda">
<?php if(count($content['avisos'])>3) : ?>
      <div class="Botones">
        <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
      </div>
<?php endif; ?>
      <div class="lista">
        <ul class="clearfix bloque_avisos <?php (count($content['avisos'])>3)? print 'Slider' : print 'no-Slider';?>">
<?php for($i=0; $i<count($content['avisos']); $i++) : ?>
          <li>
            <div class="CajaAviso ">
              <a href="<?php print $content['avisos'][$i]['url']; ?>?cx_level=destacados_home_productos" title="<?php print $content['avisos'][$i]['titulo']; ?>">
                <div class="imgAviso">
                  <?php
                    $alt_img = explode('/', $content['avisos'][$i]['url']);
                    $alt_img = $alt_img[0].' de '.$alt_img[1].' '.$content['avisos'][$i]['titulo'].' a '.$content['avisos'][$i]['precio'];
                  ?>
                  <div class="contentImg">
                    <span></span>
                    <?php print theme('imagecache', 'ficha_aviso_173_115_sc', $content['avisos'][$i]['imagen'], $alt_img, $content['avisos'][$i]['titulo']); ?> 
                  </div>
                </div>
                <div class="AvisoDescripcion">
                  <h4><?php print $content['avisos'][$i]['titulo']; ?></h4>
                  <p>
                  </p>
                  <span class="precio"><?php print $content['avisos'][$i]['precio']; ?></span>
                  <span class="mensajePago">Precio en 1 pago</span>
                </div>
              </a>
              <?php if(isset($content['avisos'][$i]['disponible_venta']) && $content['avisos'][$i]['disponible_venta']) { ?><a class="btn-comprar" href="/comprar/<?php print $content['avisos'][$i]['nid']; ?>/datos?cx_level=destacados_home_productos" title="Comprar <?php print $content['avisos'][$i]['titulo']; ?>"><span></span>Comprar</a><?php } ?>
            </div>
          </li>
<?php endfor; ?>
        </ul>
      </div> 
    </div>
  </div>
</div>