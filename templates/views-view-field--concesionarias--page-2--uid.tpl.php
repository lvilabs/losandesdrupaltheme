<?php 
  $inmo = user_load($output);
  $inmo_picture = $inmo->picture;
  $inmo_nombre = $inmo->profile_nombre_comercial;
  $inmo_tel1 = $inmo->profile_telefono_principal;
  $inmo_mail = $inmo->mail;
  $inmo_web = $inmo->profile_sitio_web;
  $inmo_lat = $inmo->location['locpick']['user_latitude'];
  $inmo_long = $inmo->location['locpick']['user_longitude'];
  
  //Calle y n°
  if($inmo->profile_calle != '')
    $direccion = $inmo->profile_calle." ".$inmo->profile_altura;
  $ciudad = taxonomy_get_term($inmo->profile_ciudad);
  $barrio = taxonomy_get_term($inmo->profile_barrio);
  //Barrio
  if($barrio != ''){
    if($direccion == '')
      $direccion = $barrio->name;
    else
      $direccion .= ' - '.$barrio->name;
  }
  //Ciudad
  if($ciudad != ''){
    if($direccion == '')
      $direccion = $ciudad->name;
    else
      $direccion .= ' - '.$ciudad->name;
  }
?>
<hr>
<div class="fondoConcesionaria clearfix">
  <div class="Borde clearfix">
    <div class="Logo Left">
      <span></span><?php if($inmo_picture != '') print clasificados_imagecache('logo_200_160_sc', $inmo_picture, $inmo_nombre, $inmo_nombre); ?>
    </div>
    <div class="Info Left">
      <div class="inmo-title"><?php print $inmo_nombre; ?></div>
      <div><?php print $direccion; ?></div>
      <div><?php if($inmo_tel1 != '') print 'Tel: '.$inmo_tel1; ?></div>
      <div class="links"><a href="mailto:<?php print $inmo_mail; ?>" title="Mail de la concesionaria"><?php print $inmo_mail; ?></a></div>
      <?php if ($inmo_web != ""){ ?>
        <?php if (eregi("http://",$inmo_web)){ ?>
          <div class="links"><a href="<?php print $inmo_web; ?>" title="Web de la concesionaria" target="_blank"><?php print $inmo_web; ?></a></div>
        <?php } else { ?>
          <div class="links"><a href="http://<?php print $inmo_web; ?>" title="Web de la concesionaria" target="_blank"><?php print $inmo_web; ?></a></div>
        <?php } ?>
      <?php } ?>
      <div class="link-avisos"><a href="/search/apachesolr_search?f[0]=is_uid:<?php print $output; ?>" class="otrosAutos" title="Avisos de la concesionaria">Ver avisos de esta concesionaria</a></div>
    </div>
    <div class="formAgencia Left">
      <?php if($inmo_lat != '' && $inmo_long != ''){ ?>
        <!-- <img src="http://maps.google.com/maps/api/staticmap?center=<?php print $inmo_lat; ?>,<?php print $inmo_long; ?>&zoom=16&size=330x160&maptype=hybrid&markers=color:0xFFE800|<?php print $inmo_lat; ?>,<?php print $inmo_long; ?>&sensor=false" title="Mapa de la concesionaria" alt="Ubicación de la concesionaria" /> -->
      <?php } ?>
    </div>
  </div>
</div>