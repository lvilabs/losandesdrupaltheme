<div class="currentBuy">
  <h3>Cancelación de compra:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'ficha_aviso_120_90_sc', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
      <h2><?php print $form['nodo']['#title']; ?></h2>
      <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
  </div>
</div>
<div class="clearfix">

<div class="alerta">
  Su compra ha sido cancelada o ha existido algún error durante el proceso en la plataforma de pagos.  
</div>
<a href="<?php print url('comprar/'.$form['venta']['#venta_id'].'/pago'); ?>">Intentar comprar nuevamente</a>

<div class="form-item"><?php print drupal_render($form['submit']); ?></div>
</div>
<?php
print drupal_render_children($form);