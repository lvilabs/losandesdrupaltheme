<p><strong>Gracias !interesado por usar el portal Clasificados Los Andes.</strong></p>
<p>Queremos informarte que la consulta que realizaste sobre el anuncio <a href="!aviso_url">!aviso_title</a> ha sido
  enviada al vendedor y archivada en nuestro sistema.</p>
<p>Si querés contactarte de manera personal con el vendedor, aquí te dejamos sus datos de contacto:</p>
<p>Teléfono: !vendedor_telefono</p>
<p>E-mail: !vendedor_email</p>
<p>Link al Anuncio: <a href="!aviso_url">!aviso_url</a></p>
