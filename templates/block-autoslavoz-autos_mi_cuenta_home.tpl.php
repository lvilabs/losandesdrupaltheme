<?php
  global $user;
  //Se carga el nombre del usuario.
  profile_load_profile($user);
  $nombre_usuario = $user->name;
  if(! empty($user->profile_nombre)) {
    $nombre_usuario = $user->profile_nombre;
    if(! empty($user->profile_apellido)) {
      $nombre_usuario .= ' '.$user->profile_apellido;
    }
  }
  
  $url_avisos = url('administrar/mis-avisos.html', array('alias'=>TRUE));
  if(module_exists('usuarios_colaboradores') && in_array('colaborador inmobiliaria', $user->roles)){
    $url_avisos = url('administrar/mis-avisos-colaborador.html', array('alias'=>TRUE));
  }
  //Si es Concesionaria SDClass lo envío al resumen 
  if(array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles)){
    $url_avisos = '/administrar/principal';
  } else {
    //Si el usuario es Inmobiliaria o Concesionaria, aplicamos el filtro a la url
    if($user->profile_tipo_concesionaria == 'Inmobiliaria')
      $url_avisos = '/administrar/mis-avisos.html?term_rubros=6330';
    elseif($user->profile_tipo_concesionaria == 'Concesionaria')
      $url_avisos = '/administrar/mis-avisos.html?term_rubros=6323';
  }
?>
<div class="item-list UsuarioLogeado">
<div class="cajaUsuario clearfix"><span></span><a href="<?php print url('user/'. $user->uid); ?>" title="<?php print $nombre_usuario; ?>"><?php print truncate_utf8($nombre_usuario, 15, FALSE, TRUE); ?></a></div>
  <div class="menu">
    <a href="#" class="dropdown-toggle"></a>    

    <div class="dropdown-login">
		<div class="yamm-content">
			<div class="clearfix">
				<ul>
					<li class="leaf"><a href="<?php print $url_avisos; ?>" title="Mi Cuenta">Mi Cuenta</a></li>
    				<li class="leaf last"><a href="<?php print url('logout', array('alias'=>TRUE)); ?>" title="<?php print t('Logout'); ?>"><?php print t('Logout'); ?></a></li>
				</ul>
			</div>
		</div>
	</div> 
    
    </div>
</div>
