<div class="currentBuy">
  <h3>Su compra:</h3>
  <div class="producto clearfix">
    <?php 
    $imagen = '';
    if(!empty($form['nodo']['#fotos']))
      $imagen = theme('imagecache', 'logo_130_105', $form['nodo']['#fotos']);
    print $imagen;
    ?>
    <div class="descripcion">
      <h2><?php print $form['nodo']['#title']; ?></h2>
      <h4><?php print $form['nodo']['#teaser']; ?></h4>
    </div>
  </div>
</div>
<div class="clearfix">

<div class="alerta">
  
</div>
<!--para cuando este ok el pago-->
<div class="mensajeOk">
  Gracias por su compra.<br> El vendedor procederá a preparar su pedido.
</div>
<!--fin pago -->
<div class="form-item"><?php print drupal_render($form['submit']); ?></div>
</div>
<?php
print drupal_render_children($form);

if(SITIO_LOCAL_ID==SITIO_LOCAL_ID_CLASIFICADOS) { // Solo en Clasificadoss.
  global $taxonomias; // Hacemos visible la taxonomias desde el bloque.
  global $nid;
  $taxonomias = $form['nodo']['#taxonomy'];
  $nid = $form['nodo']['#nid'];

  $block = module_invoke('block', 'block', 'view', CLASIFICADOS_CODIGO_CONVERSION_ADWORDS_RUBRO_PRODUCTOS_BID);
  if(!empty($block['content'])) {
    print $block['content'];
  }
}
