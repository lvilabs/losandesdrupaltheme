<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>!header_title</title>
  </head>
  <body bgcolor="#FFFFFF" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="margin: 8px;">
    <center>
      <table width="650" border="0" cellpadding="0" cellspacing="0" height="100%" style=" mso-cellspacing:0cm;" id="backgroundTable">
        <tbody>
        <tr>
          <td align="center" valign="top">
            <!-- // Begin Template Preheader, Header and Body  -->
            <table border="0" cellpadding="0" cellspacing="0" width="650" id="templatePreheader">
              <tbody>
              <!-- // Begin Module: Standard Preheader  -->
              <!-- // End Module: Standard Preheader  -->
              <!-- // Begin Template Header  -->
              <tr>
                <td bgcolor="#093E6A" align="center" valign="center" class="headerContent" style="height: 60px;">
                  <a href="!site_url"><img src="!logo_top" width="650" style="width: 180px;"/></a>
                </td>
              </tr>
              <!-- // End Template Header -->
              <tr>
                <td style="padding:0cm 0cm 0cm 0cm;height:18.75pt"></td>
              </tr>
              <!-- // Begin Template Body -->
              
                  <?php print $body; ?>
                
              <!-- // End Template Body -->
              </tbody>
            </table>

            <!-- // Begin Template Footer -->
            <table border="0" cellpadding="0" cellspacing="0" width="650" id="templateContainer" style="background-color: #1c1c1c; font-family: Arial,sans-serif; ; font-size:12px;">
              <tbody>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td width="20" style="background-color: #1c1c1c;">&nbsp;</td>
                      <td valign="top" width="610" style=" background-color: #1c1c1c;">&nbsp;
                      </td>
                      <td width="20" style=" background-color: #1c1c1c;">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top" style="background-color: #1c1c1c;">
                  <table border="0" cellspacing="0" cellpadding="2" align="center">
                    <tbody>
                    <tr>

                      <td width="75" valign="top" align="center" style="background-color: #1c1c1c;">
                        <a href="!url_facebook" title="Los Andes FB"><img src="!logo_rn_facebook" width="30"></a>
                      </td>
                      <td width="75" valign="top" align="center" style="background-color: #1c1c1c; font-weight: bold;">
                        <a href="!url_twitter" title="Los Andes Twitter"><img src="!logo_twitter" width="30"></a>
                      </td>
                      <td width="75" valign="top" align="center" style="background-color: #1c1c1c; font-weight: bold;">
                        <a href="!url_instagram" title="Los Andes Instagram"><img src="!logo_instagram" width="30"></a>
                      </td>
                      <td width="75" valign="top" align="center" style="background-color: #1c1c1c;">
                        <a href="!url_youtube" title="Los Andes Youtube"><img src="!logo_youtube" width="30"></a>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td valign="top" width="650" style="border-top: 3px solid #343330; background-color: #1c1c1c;">
                        &nbsp;
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" style="color:#686868;line-height: 20px;">
                  Este email fue enviado desde una casilla automática. Por favor no responda.
                </td>
              </tr>
              <tr>
                <td align="center" style="color:#686868;line-height: 20px;">
                  <em>Copyright ©</em> !year - Diario Los Andes. Todos lo derechos reservados.
                </td>
              </tr>
              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>

                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">
                        &nbsp;
                      </td>
                    </tr>
                    <tr>
                      <td valign="top" width="650" style="background-color: #1c1c1c;">
                        &nbsp;
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              </tbody>
            </table>
            <!-- // End Template Footer  -->
            <br/>
          </td>
        </tr>
        </tbody>
      </table>
    </center>
  </body>
</html>

