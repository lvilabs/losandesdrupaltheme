<?php session_start; if(isset($_SESSION['error_sitio_multimedia'])) { ?>
  <div style='display:none'>
    <div class="msg-sitio-error"><?php print $_SESSION['error_sitio_multimedia']; ?></div>
    <div class="url-sitio-error"><?php print $_SESSION['url_sitio_multimedia']; ?></div>
  </div>
<?php unset($_SESSION['error_sitio_multimedia']); unset($_SESSION['url_sitio_multimedia']); } ?>
<?php
  switch($form['template']['#value']){
    case 'template3':
      $ancho_imagen = 1420;
      break;
    default:
      $ancho_imagen = 960;
  }
?>
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/colorbox.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/configuracion.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/font-awesome/css/font-awesome.min.css">
<!-- Page Content -->
<div id="main" class="container-fluid page-imagenes">                        
  <!-- Boxed Container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-6" style="overflow: hidden;">
        <div class="title-section imagenes">
          <h1>Imágenes</h1>
          <h4>Cargá las imágenes institucionales que necesitás para</br>personalizar tus sitios.</h4>
        </div>       
        <div class="col-md-4 imagenes-logo">
          <div class="logo-space" id="files-logo">
            <?php if(!empty($form['logo-img']['#value'])){ ?>
              <img width="133" height="54" src="/<?php print $form['logo-img']['#value']; ?>" />
            <?php } ?>
          </div>
          <button type="button" class="btn btn-default btn-upload" id="submit-logo">CARGAR LOGO</button>
          <p class="pie-texto"> Tamaño de imagen recomendada</br>136 x 106 pixeles resolución 300dpi</p>
        </div>
        <div class="col-md-1 imagenes-opciones">
          <!-- <div class="cut">
            <a href="#">
              <i class="fa fa-crop"></i>
            </a>
          </div>
          <div class="rotate">
            <a href="#">
              <i class="fa fa-adjust"></i>
            </a>
          </div> -->
        </div>
        
        <div class="col-md-7 imagenes-slider">
          <div class="slider-space" id="files-micrositio">
            <?php if(!empty($form['micrositio-img']['#value'])){ ?>
              <img width="300" height="41" src="/<?php print $form['micrositio-img']['#value']; ?>" />
            <?php } ?>
          </div>
          <button type="button" class="btn btn-default btn-upload" id="submit-imagen-micrositio">CARGAR IMAGEN MICROSITIO</button>
          <p class="pie-texto"> Cargá la imagen que va aparecer en el encabezado de tu Micrositio. El tamaño de la imagen debe ser de 960x130 píxeles en una resolución de 300dpi</p>
        </div>     
        
        <!-- <div class="col-md-7 imagenes-slider">
          <div class="slider-space" id="files-principal">
            <?php if(!empty($form['principal-img']['#value'])){ ?>
              <img width="233" height="100" src="/<?php print $form['principal-img']['#value']; ?>" />
            <?php } ?>
          </div>
          <button type="button" class="btn btn-default btn-upload" id="submit-imagen-principal">CARGAR IMAGEN PRINCIPAL</button>
          <p class="pie-texto"> Cargá la imagen principal que va aparecer en tu sitio web. El tamaño de la imagen  debe ser de <?php print $ancho_imagen; ?>x488 píxeles en una resolución de 300dpi</p>
        </div>
        <div class="section-micrositio">
          <h1>Microstio</h1>
          <div class="col-md-7 imagenes-slider">
            <div class="slider-space" id="files-micrositio">
              <?php if(!empty($form['micrositio-img']['#value'])){ ?>
                <img width="300" height="41" src="/<?php print $form['micrositio-img']['#value']; ?>" />
              <?php } ?>
            </div>
            <button type="button" class="btn btn-default btn-upload" id="submit-imagen-micrositio">CARGAR IMAGEN MICROSITIO</button>
            <p class="pie-texto"> Cargá la imagen que va aparecer en el encabezado de tu Micrositio. El tamaño de la imagen debe ser de 960x130 píxeles en una resolución de 300dpi</p>
          </div>        
        </div> -->
      </div>
      <div class="col-lg-6" style="margin-bottom: 30px;">
        <div class="title-section links">
          <h1>Links</h1>
          <!-- <h4>Agregá los link de tus redes sociales </br>para que tus clientes te sigan.</h4> -->
          <h4>Subdominio de tu micrositio </br>dentro de Clasificados La Voz.</h4>
        </div>         
        <form class="form-links">
          <!-- <div class="form-group">
            <?php print drupal_render($form['enlace-facebook']); ?>
          </div>
          <div class="form-group">
            <?php print drupal_render($form['enlace-twitter']); ?>
          </div>
          <div class="form-group">
            <?php print drupal_render($form['enlace-google-plus']); ?>
          </div>
          <div class="form-group">
            <?php print drupal_render($form['analytics']); ?>
          </div>
          <p class="pie-texto grande">Carga aquí el código Google Analytics y realizá el seguimiento de tus</br>conversiones.</p>             
          </br></br></br> -->
          <div class="form-group">
            <?php print drupal_render($form['enlace-micrositio']); ?>
          </div>
          <p class="pie-texto grande">Elegí el subdominio Clasificadoslavoz para tu Micrositio con el que los usuarios van a poder encontrarte. El dominio puede ser el nombre de tu empresa, y quedaría www.clasificadoslavoz.com.ar/sitio/tuempresa. Tenés que escribir sólo tu dominio, sin "http://www.clasificadoslavoz.com.ar/". La URL es permanente y no podrás editarla o eliminarla una vez elegida.</p>
        </form> 
      </div>                
    </div>
    <div class="col-md-4 col-md-offset-4 button-footer">
      <?php print drupal_render($form['multimedia-submit']); ?>
    </div>             
  </div>
  <!-- /.row -->
  <div style="display:none;"><?php print drupal_render($form); ?></div>
</div>
<!-- /.container -->
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/multimedia.js"></script>