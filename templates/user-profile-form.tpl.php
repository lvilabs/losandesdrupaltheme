<?php
  global $user;
    
  $errors = drupal_get_messages('error');
  //print_r($errors);
  if (!empty($errors)){
    foreach($errors['error'] as $error) { 
      if ($error == "El campo Nombre de usuario es obligatorio.")  $error = "El campo E-mail es obligatorio.";
      if ($error == "Debe proporcionar una dirección de correo electrónico.")  unset($error);
      if ($error == "Tu E-mail de usuario no es válido.")  unset($error);
      if ($error == "Seleccione una imagen.")  $error = "El Logo de la Concesionaria es requerido.";
      drupal_set_message($error, 'error');
    }
  }
?>
<div class="Content clearfix">
  <div class="ContactoAutos">
    <div class="blanco ">
      <?php if(in_array("particular", $user->roles) || in_array("colaborador inmobiliaria", $user->roles)){ ?>
      <div class="FormRegistro">
        <?php if(arg(3) == ''){ ?>
          <h3>Datos Usuario</h3>
          <fieldset>
            <input type="hidden" name="usuario" value="Particular" />
            <div class="Row clearfix">
              <label for="edit-mail">E-mail: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" value="<?php print $form['account']['mail']['#default_value']; ?>" readonly="readonly" class="readonly-text" />
              <span class="Ingresa">La dirección ingresada será cargada por defecto en los avisos que publique, pudiendo modificarla posteriormente.</span>
            </div>
            <div class="Row clearfix">
              <?php  
              $form[account][pass]['#description'] = '<span class="Ingresa">La contraseña debe tener entre 4 y 15 letras y/o números exceptuando signos y acentos</span>';
              $form[account][pass][pass1]['#title'] = "Contraseña";
              $form[account][pass][pass2]['#title'] = "Repetir Contraseña";
              print drupal_render($form[account][pass]); 
              ?>
            </div>
          </fieldset>
        <?php } elseif(arg(3) == 'Datos Personales') { ?>
          <h3>Datos Personales</h3>
          <fieldset>
            <div class="Row clearfix">
            <?php  
              print drupal_render($form['Datos Personales']['profile_nombre']);
            ?>
            </div>
            <div class="Row clearfix">
            <?php
              print drupal_render($form['Datos Personales']['profile_apellido']);
            ?>
            </div>
            <div class="Row clearfix">
              <?php
                print drupal_render($form['Datos Personales']['profile_telefono_principal']);
              ?>
            </div>
            <div class="Row clearfix">
              <?php
                print drupal_render($form['Datos Personales']['profile_telefono_whatsapp']);
              ?>
              <div class="form-item" id="telefono-whatsapp-wrapper">
                <label for="edit-profile-telefono-whatsapp">Teléfono para WhatsApp: </label>
                <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 </span>0 
                <?php if(empty($form['Datos Personales']['profile_telefono_whatsapp']['#default_value'])) { ?>
                  <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                  15 <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php } else { ?>
                  <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php } ?>
                <div class="description"><span class="Ingresa">Para recibir contactos por WhatsApp complete este campo.</span></div>
              </div>
            </div>
            <div class="Row Radiobutton clearfix sexo">
            <?php  
              print drupal_render($form['Datos Personales']['profile_sexo']);
            ?>
            </div>
            <div class="Row clearfix provincia">
            <?php  
              print drupal_render($form['Datos Personales']['profile_provincia']);
            ?>
            </div>
            <div class="Row clearfix ciudad">
            <?php  
              //print_r($form['Datos Personales']);
              print drupal_render($form['Datos Personales']['profile_ciudad']);
            ?>
            </div>
            <div class="Row clearfix">
              <div class="form-item"><label>Fecha Nacimiento:</label><div>
            <?php
              $form['Datos Personales']['profile_fecha_nacimiento']['month']['#options'] = array (1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
              print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['day']);
              print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['month']);
              print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['year']);
            ?>
            </div>
          </fieldset>
        <?php } ?>
        <?php
          $form['Datos Personales']['#title'] = "";
          $form['Datos Personales']['profile_fecha_nacimiento']['#title'] = "";
          $form['account']['#title'] = "";
          unset($form['account']['mail']);
          unset($form['Datos Personales']['profile_barrio']);
          unset($form['Datos Personales']['profile_nombre_comercial']);
          unset($form['Datos Personales']['profile_sucursal']);
          unset($form['Datos Personales']['profile_nombre_sucursal']);
          unset($form['Datos Personales']['profile_calle']);
          unset($form['Datos Personales']['profile_altura']);
          unset($form['Datos Personales']['profile_piso']);
          unset($form['Datos Personales']['profile_departamento']);
          unset($form['Datos Personales']['profile_cp']);
          unset($form['Datos Personales']['profile_tipo_concesionaria']);
          unset($form['Datos Personales']['profile_rubro']);
          unset($form['Datos Personales']['profile_telefono_secundario']);
          unset($form['Datos Personales']['profile_sitio_web']);
          unset($form['Datos Fiscales']);
          unset($form['picture']);
          unset($form['locations'][0]['country']);
          unset($form['locations'][0]['locpick']['user_latitude']);
          unset($form['locations'][0]['locpick']['user_longitude']);
          unset($form['locations'][0]['locpick']['instructions']);
          unset($form['locations'][0]['locpick']['map']);
          unset($form['locations'][0]['locpick']['map_instructions']);
          unset($form['locale']);
          unset($form['contact']);
          unset($form['mimemail']);
          unset($form['Datos Personales']['profile_bloquear']);
          unset($form['Datos Personales']['profile_codigo_topinmobiliario']);
          unset($form['Datos Personales']['profile_skype']);
          unset($form['Datos Personales']['profile_crear_sitio_web']);
          //Campos Inmuebles
          unset($form['Datos Personales']['profile_matricula']);
          unset($form['Datos Personales']['profile_matricula_numero']);
          unset($form['Datos Personales']['profile_zona']);
        ?>
        <?php
          $form['submit']['#value'] = "Guardar";
        ?>
        <div class="Row clearfix">
          <div class="Ingresa">
            <div class="Boton">
            
              <?php print drupal_render($form['submit']); ?>
            </div>
          </div>
        </div>
      </div>
      <?php
      //Datos Concesionaria
      } elseif(in_array("concesionaria web", $user->roles) || in_array("concesionaria terceros", $user->roles)) { 
      ?>
      
      <div class="FormRegistro">
        <?php if(arg(3) == ''){ ?>
          <h3>Datos Usuario</h3>
          <fieldset>
            <input type="hidden" name="usuario" value="Concesionaria" />
            <div class="Row clearfix">
              <label for="edit-mail">E-mail: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" value="<?php print $form['account']['mail']['#default_value']; ?>" readonly="readonly" class="readonly-text" />
              <span class="Ingresa">La dirección ingresada será cargada por defecto en los avisos que publique, pudiendo modificarla posteriormente.</span>
            </div>
            <div class="Row clearfix">		  
            <?php  
              $form[account][pass]['#description'] = '<span class="Ingresa">La contraseña debe tener entre 4 y 15 letras y/o números exceptuando signos y acentos</span>';
              $form[account][pass][pass1]['#title'] = "Contraseña";
              $form[account][pass][pass2]['#title'] = "Repetir Contraseña";
              print drupal_render($form[account][pass]); 
            ?>
            </div>
            <div class="Row clearfix">
            <?php
              if(in_array("concesionaria terceros", $user->roles)){
                $form['picture']['current_picture']['#value'] = '';
                $form['picture']['#title'] = "";
                $form['picture']['picture_upload']['#title'] = "Subir Logo";
                $form['picture']['picture_upload']['#size'] = 35;
                $form['picture']['picture_upload']['#description'] = '<span class="Ingresa">Seleccione la imagen del logo asociado a su concesionaria para que el mismo sea subido al servidor al momento de confirmar la modificación. Extensiones permitidas: JPG JPEG PNG GIF. Tamaño recomendado: 250px x 200px. Peso máximo de archivo: 100kb.</span>';
                $form['picture']['picture_delete']['#title'] = 'Eliminar logo';
                $form['picture']['picture_delete']['#description'] = '<span class="Ingresa">Marque esta casilla para borrar su logo actual.<span>';
              ?>  
                <label for="edit-picture-upload">  
                  <?php print 'Logo:'; ?>
                </label>
              <?php
                if(count($form['picture']['current_picture']) > 1){
                  print clasificados_imagecache('logo_200_160', $user->picture, 'Logo del vendedor', 'Logo del vendedor', NULL, TRUE, TRUE, TRUE); ?>
                  <div class="con-imagen">
                    <?php print drupal_render($form['picture']); ?>
                  </div>
                <?php } else {
                  print drupal_render($form['picture']);
                } ?>
            <?php } ?>
            </div>
            <div class="Row clearfix">
            <?php
              unset($form['locations'][0]['country']);
              unset($form['locations'][0]['locpick']['instructions']);
            ?>
            <label for="edit-picture-upload">  
              <?php print ''.$form['locations'][0]['#title'].':'; ?>
            </label>
            <?php print drupal_render($form['locations'][0]['locpick']['map']); ?>
            <div class="description">
              <span class="Ingresa"><?php print drupal_render($form['locations'][0]['locpick']['map_instructions']); ?></span>
            </div>
            <?php
              $form['locations'][0]['delete_location']['#description'] = '<span class="Ingresa">Marque esta casilla para eliminar la ubicación.<span>';
              print drupal_render($form['locations'][0]['delete_location']);
            ?>
            </div>
            <a name="respuesta"></a>
          </fieldset>
          
          <?php if(in_array("concesionaria terceros", $user->roles)) { ?>
            <h3>Respuesta Automática de Consultas (período de ausencia)</h3>
            <fieldset>
              <div class="recomendacion"><strong>¿Te vas de vacaciones?</strong> 
              <br> Ahora podes programar una respuesta automática a tus consultas para tu período de ausencia.</div>
              <div class="Row Radiobutton clearfix">
                <div class="form-item"><label>Activar autorespuesta:</label></div>
                <div>
                <?php  
                  $form['user_autorespuesta']['#title'] = '';
                  print drupal_render($form['user_autorespuesta']); 
                ?>
                </div>
              </div>
              <div class="Row clearfix">
                <?php  
                unset($form['user_autorespuesta_desde']['time']);
                unset($form['user_autorespuesta_desde']['date']['#description']);
                print drupal_render($form['user_autorespuesta_desde']); 
                ?>
              </div>
              <div class="Row clearfix">
                <?php  
                unset($form['user_autorespuesta_hasta']['time']);
                unset($form['user_autorespuesta_hasta']['date']['#description']);
                print drupal_render($form['user_autorespuesta_hasta']); 
                ?>
              </div>
              <div class="Row clearfix">
                <?php  
                print drupal_render($form['user_autorespuesta_texto']); 
                ?>
              </div>
            </fieldset>
          <?php } ?>
          
        <?php } elseif(arg(3) == 'Datos Personales') { ?>
          <h3>Datos Personales</h3>
          <fieldset>
            <div class="Row clearfix">
            <?php
              print drupal_render($form['Datos Personales']['profile_telefono_principal']);
            ?>
            </div>
            <div class="Row clearfix">
            <?php
              print drupal_render($form['Datos Personales']['profile_telefono_whatsapp']);
            ?>
              <div class="form-item" id="telefono-whatsapp-wrapper">
                <label for="edit-profile-telefono-whatsapp">Teléfono para WhatsApp: </label>
                <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 </span>0 
                <?php if(empty($form['Datos Personales']['profile_telefono_whatsapp']['#default_value'])) { ?>
                  <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                  15 <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php } else { ?>
                  <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php } ?>
                <div class="description"><span class="Ingresa">Para recibir contactos por WhatsApp complete este campo.</span></div>
              </div>
            </div>
            <div class="Row clearfix nombre-comercial">
            <?php         
              print drupal_render($form['Datos Personales']['profile_nombre_comercial']);
            ?>
            </div>
            <div class="Row Radiobutton clearfix sucursal">
            <?php  
              print drupal_render($form['Datos Personales']['profile_sucursal']);
            ?>
            </div>
            <div class="Row clearfix nombre-sucursal">
            <?php  
              print drupal_render($form['Datos Personales']['profile_nombre_sucursal']);
            ?>
            </div>
            <div class="Row clearfix">
              <div class="sucursal-calle">
                <?php  
                  print drupal_render($form['Datos Personales']['profile_calle']);
                ?>
              </div>
            </div>
            <div class="Row clearfix">
              <div class="sucursal-altura">        
                <?php
                  print drupal_render($form['Datos Personales']['profile_altura']);
                ?>
              </div>
            </div>
            <div class="Row clearfix">
              <div class="sucursal-piso">
                <?php
                  print drupal_render($form['Datos Personales']['profile_piso']);
                ?>
              </div>
            </div>
            <div class="Row clearfix">
              <div class="sucursal-dto">    
                <?php
                  print drupal_render($form['Datos Personales']['profile_departamento']);
                ?>
              </div>
            </div>
            <div class="Row clearfix cp">
            <?php  
              print drupal_render($form['Datos Personales']['profile_cp']);
            ?>
            </div>
            <div class="Row clearfix provincia">
            <?php  
              print drupal_render($form['Datos Personales']['profile_provincia']);
            ?>
            </div>
            <div class="Row clearfix ciudad">
            <?php  
              print drupal_render($form['Datos Personales']['profile_ciudad']);
            ?>
            </div>
            <div class="Row clearfix barrio">
            <?php  
              print drupal_render($form['Datos Personales']['profile_barrio']);
            ?>
            </div>
            <div class="Row clearfix tipo-concesionaria">
            <?php  
              print drupal_render($form['Datos Personales']['profile_tipo_concesionaria']);
            ?>
            </div>
            <div class="Row clearfix">
            <?php
              print drupal_render($form['Datos Personales']['profile_sitio_web']);
            ?>
            </div>
          </fieldset>
          <fieldset>
            <h3>Datos de Contacto</h3>
            <div class="Row clearfix">
            <?php  
              $form['Datos Personales']['profile_nombre']['#title'] = "Nombre Responsable";
              print drupal_render($form['Datos Personales']['profile_nombre']);
            ?>
            </div>
            <div class="Row clearfix">
            <?php
              $form['Datos Personales']['profile_apellido']['#title'] = "Apellido Responsable";
              print drupal_render($form['Datos Personales']['profile_apellido']);
            ?>
            </div>
            <div class="Row clearfix telefono-sec">
            <?php
              print drupal_render($form['Datos Personales']['profile_telefono_secundario']);
            ?>
            </div>
          </fieldset> 
       <?php } elseif(arg(3) == 'Datos Fiscales') { ?>   
          <h3>Datos Fiscales</h3>
          <fieldset>
            <div class="Row clearfix razon-social">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_razon_social']);
            ?>
            </div>
            <div class="Row clearfix">
              <label for="edit-profile-cuit">CUIT: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" value="<?php print $form['Datos Fiscales']['profile_cuit']['#value']; ?>" readonly="readonly" class="readonly-text" />
            </div>
            <div class="Row clearfix iva">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_condicion_iva']);
            ?>
            </div>
            <div class="Row clearfix">
              <div class="form-item"><label>Domicilio:</label></div>
              <div class="domicilio-mismo">
                <?php
                  $form['Datos Fiscales']['profile_domicilio_fiscal_mismo']['#title'] = '<div class="domicilio-mismo-label">Mismos datos de la concesionaria</div>';
                  print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_mismo']);
                ?>
              </div>
            </div>
            <div class="domicilio-fiscal">
              <div class="Row clearfix">
                <div class="sucursal-calle">
                  <?php  
                    print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_calle']);
                  ?>
                </div>
              </div>  
              <div class="Row clearfix">
                <div class="sucursal-altura">        
                  <?php
                    print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_altura']);
                  ?>
                </div>
              </div>  
              <div class="Row clearfix">
                <div class="sucursal-piso">
                  <?php
                    print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_piso']);
                  ?>
                </div>
              </div>  
              <div class="Row clearfix">
                <div class="sucursal-dto">    
                  <?php
                    print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_dto']);
                  ?>
                </div>
              </div>
              <div class="Row clearfix cp-fiscal">
              <?php
                print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_cp']);
              ?>
              </div>
              <div class="Row clearfix provincia-fiscal">
              <?php
                print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_provincia']);
              ?>
              </div>
              <div class="Row clearfix ciudad-fiscal">
              <?php
                print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_ciudad']);
              ?>
              </div>
              <div class="Row clearfix barrio-fiscal">
              <?php
                print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_barrio']);
              ?>
              </div>
            </div>
          </fieldset>
        <?php } 
        
          $form['Datos Personales']['#title'] = "";
          $form['Datos Usuario']['#title'] = "";
          $form['Datos Fiscales']['#title'] = "";
          unset($form['account']['mail']);
          unset($form['Datos Personales']['profile_fecha_nacimiento']);
          unset($form['Datos Personales']['profile_sexo']);
          unset($form['locale']);
          unset($form['contact']);
          unset($form['mimemail']);
          unset($form['Datos Personales']['profile_bloquear']);
          unset($form['Datos Personales']['profile_codigo_topinmobiliario']);
          unset($form['Datos Personales']['profile_skype']);
          unset($form['Datos Personales']['profile_crear_sitio_web']);
          //Campos Inmuebles
          unset($form['Datos Personales']['profile_matricula']);
          unset($form['Datos Personales']['profile_matricula_numero']);
          unset($form['Datos Personales']['profile_zona']);
          if(in_array("concesionaria web", $user->roles))
            unset($form['picture']);
          $form['account']['#title'] = "";
        ?>
        <?php
          $form['submit']['#value'] = "Guardar";
        ?>
        <div class="Row clearfix">
          <div class="Ingresa">
            <div class="Boton">

              <?php print drupal_render($form['submit']); ?>
            </div>
          </div>
        </div>
      </div>    
      <?php } ?>
      <div style="display:none;"><?php print drupal_render($form); ?></div>
    </div>
  </div>
</div>
<?php //print_r($form); ?>
