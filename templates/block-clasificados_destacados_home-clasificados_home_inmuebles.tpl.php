<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="Content AvisosDestacados clearfix">
    <div class="Sombra">
      <div class="clearfix">
        <h3><strong>Anuncios Destacados</strong></h3>
      </div>
    </div>
    <div class="clearfix contentAvisos">
    <?php foreach($content as $aviso): ?>
      <div class="CajaAviso new">
        <a href="<?php print $aviso['url']; ?>" title="<?php print str_replace('"', '', $aviso['title']); ?>">
          <div class="imgAviso">
            <?php
              $alt_img = explode('/', $aviso['url']);
              $alt_img = $alt_img[3].' de '.$alt_img[4].' '.str_replace('"', '', $aviso['titulo']).' a '.$aviso['precio'];
            ?>
            <img src="<?php print $aviso['foto']; ?>" alt="<?php print $alt_img; ?>" title="<?php print str_replace('"', '', $aviso['title']); ?>" class="imagecache imagecache-ficha_aviso_mobile_220_148" width="220" height="148">
          </div>
          <div class="AvisoDescripcion">
            <h4><?php print $aviso['title']; ?></h4>
            <span class="precio"><?php print $aviso['precio']; ?></span>
          </div>
        </a>
      </div>
  <?php endforeach; ?>
    </div>
  </div>
</div>