<?php
  //print $breadcrumb;  
  
  $errors = drupal_get_messages('error');
  //print_r($errors);
  if (!empty($errors)){
	//drupal_set_message('<h4>Tienes campos incompletos:</h4>', 'error');
    foreach($errors['error'] as $error) { 
      if ($error == "El campo Nombre de usuario es obligatorio.")  $error = "El campo E-mail es obligatorio.";
      if ($error == "Debe proporcionar una dirección de correo electrónico.")  unset($error);
      if ($error == "Tu E-mail de usuario no es válido.")  unset($error);
      if ($error == "Seleccione una imagen.")  $error = "El Logo de la Concesionaria es requerido.";
      drupal_set_message($error, 'error');
    }
	//drupal_set_message('Muchas gracias.', 'error');
  }
  
	/*$form['#attributes']['enctype'] = 'multipart/form-data';
	$form['picture'] = array('#type' => 'fieldset', '#title' => t('Picture'), '#weight' => 1);
	$form['picture']['picture_upload_register'] = array('#type' => 'file', '#title' => t('Upload picture'), '#description' => t('<br>Your virtual face or picture. Maximum dimensions are %dimensions and the maximum size is %size kB.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'), '%size' => variable_get('user_picture_file_size', '30'))) .' '. variable_get('user_picture_guidelines', ''));
	$form['picture']['pic_selected'] = array(
	  '#type' => 'hidden',
	  '#default_value' => 0
	);
	$form['picture']['picture_upload_register']['#required'] = 0; */
  
  $block_tabs_registro = module_invoke('block', 'block', 'view', 18);
?>
<div class="MiCuenta clearfix">
  <?php  print $block_tabs_registro['content']; ?>
  <div class="ContactoAutos">

        <?php if(ereg('registro-particular',$form['#action'])){?>

        <?php } else { ?>

        <?php } ?>


    <div class="blanco ">
	
      <?php if(ereg('registro-particular',$form['#action'])){?>
      <p>Si no tienes cuenta, <strong>regístrate aquí. </strong> Completá el formulario con tus datos</p>
      <div class="FormRegistro">
        <h3>Datos de Usuario</h3>
        <fieldset>
          <input type="hidden" name="usuario" value="Particular" />
          <div class="Row clearfix">
            <?php 
            //$form['usuario'] = "Particular"; 
            
            $form['account']['name']['#title'] = 'E-mail'; 
            $form['account']['name']['#description'] = '<span class="Ingresa">La dirección ingresada será cargada por defecto en los avisos que publique, pudiendo modificarla posteriormente.</span>';
            print drupal_render($form['account']['name']); 
            ?>
          </div>
          <div class="Row clearfix">
            <?php  
            $form['account']['pass']['#description'] = '<span class="Ingresa">La contraseña debe tener entre 4 y 15 letras y/o números exceptuando signos y acentos</span>';
            $form['account']['pass']['pass1']['#title'] = "Contraseña";
            $form['account']['pass']['pass2']['#title'] = "Repetir Contraseña";
            print drupal_render($form['account']['pass']); 
            ?>
          </div>
          <div class="Row clearfix">
            <?php
              print drupal_render($form['Datos Personales']['profile_telefono_principal']);
            ?>
          </div>
        </fieldset>
        <fieldset>
          <h3>Datos Personales</h3>
          <div class="Row clearfix">
          <?php  
            print drupal_render($form['Datos Personales']['profile_nombre']);
          ?>
          </div>
          <div class="Row clearfix">
          <?php
            print drupal_render($form['Datos Personales']['profile_apellido']);
          ?>
          </div>
          <div class="Row Radiobutton clearfix sexo">
          <?php  
            print drupal_render($form['Datos Personales']['profile_sexo']);
          ?>
          </div>
          <div class="Row clearfix provincia">
          <?php  
            print drupal_render($form['Datos Personales']['profile_provincia']);
          ?>
          </div>
          <div class="Row clearfix ciudad">
          <?php  
            print drupal_render($form['Datos Personales']['profile_ciudad']);
          ?>
          </div>
          <div class="Row clearfix">
            <div class="form-item"><label>Fecha Nacimiento:</label><div>
          <?php
            $form['Datos Personales']['profile_fecha_nacimiento']['month']['#options'] = array (1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
            print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['day']);
            print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['month']);
            print drupal_render($form['Datos Personales']['profile_fecha_nacimiento']['year']);
          ?>
          </div>
          <?php
            $form['Datos Personales']['#title'] = "";
            $form['Datos Personales']['profile_fecha_nacimiento']['#title'] = "";
            $form['account']['#title'] = "";
            unset($form['account']['mail']);
            unset($form['Datos Personales']['profile_barrio']);
            unset($form['Datos Personales']['profile_nombre_comercial']);
            unset($form['Datos Personales']['profile_sucursal']);
            unset($form['Datos Personales']['profile_nombre_sucursal']);
            unset($form['Datos Personales']['profile_calle']);
            unset($form['Datos Personales']['profile_altura']);
            unset($form['Datos Personales']['profile_piso']);
            unset($form['Datos Personales']['profile_departamento']);
            unset($form['Datos Personales']['profile_cp']);
            unset($form['Datos Personales']['profile_tipo_concesionaria']);
            unset($form['Datos Personales']['profile_rubro']);
            unset($form['Datos Personales']['profile_telefono_secundario']);
            unset($form['Datos Personales']['profile_sitio_web']);
            unset($form['Datos Personales']['profile_tipo_usuario']);
            unset($form['Datos Personales']['profile_crear_sitio_web']);
            //unset($form['Datos Usuario']);
            unset($form['Datos Fiscales']);
            unset($form['picture']);
            unset($form['locations'][0]['country']);
            unset($form['locations'][0]['locpick']['user_latitude']);
            unset($form['locations'][0]['locpick']['instructions']);
            unset($form['locations'][0]['locpick']['map']);
            unset($form['locations'][0]['locpick']['map_instructions']);
            //Campos Inmuebles
            unset($form['Datos Personales']['profile_matricula']);
            unset($form['Datos Personales']['profile_matricula_numero']);
            unset($form['Datos Personales']['profile_zona']);
          ?>
        </fieldset>
        <div class="Row Checked clearfix condiciones">
          <?php
            $form['Datos Personales']['profile_condiciones']['#title'] = '';
            print drupal_render($form['Datos Personales']['profile_condiciones']);
          ?>
          <label class="check-condiciones">Acepto las <a target="_blank" href="https://losandes.com.ar/page/terms-and-conditions">condiciones</a> de utilización del servicio.</label>
        </div>
        <div class="Row Checked clearfix ofertas">
          <?php
            print drupal_render($form['Datos Personales']['profile_recepcion_ofertas']);
          ?>
        </div>
        <?php
          $form['submit']['#value'] = "Registrarme";
        ?>
        <div class="Row clearfix">
          <div class="Ingresa">
            <div class="Boton">
              <?php print drupal_render($form['submit']); ?>
            </div>
          </div>
        </div>
      </div>
      <?php }else{ ?>
      
      <p>Si no tienes cuenta, <strong>regístrate aquí. </strong> Completá el formulario con tus datos</p>
      <div class="FormRegistro">
        <h3>Datos de Usuario</h3>
        <fieldset>
          <input type="hidden" name="usuario" value="Concesionaria" />
          <div class="Row clearfix">
          <?php 
            //$form['usuario'] = "Concesionaria";
            $form['account']['name']['#title'] = "E-mail";
            $form['account']['name']['#description'] = '<span class="Ingresa">La dirección ingresada será cargada por defecto en los avisos que publique, pudiendo modificarla posteriormente.</span>';
            print drupal_render($form['account']['name']);
          ?>
          </div>
          <div class="Row clearfix">		  
          <?php  
            $form['account']['pass']['#description'] = '<span class="Ingresa">La contraseña debe tener entre 4 y 15 letras y/o números exceptuando signos y acentos</span>';
            $form['account']['pass']['pass1']['#title'] = "Contraseña";
            $form['account']['pass']['pass2']['#title'] = "Repetir Contraseña";
            print drupal_render($form['account']['pass']); 
          ?>
          </div>
          <div class="Row clearfix">
          <?php
            print drupal_render($form['Datos Personales']['profile_telefono_principal']);
          ?>
          </div>
        </fieldset>
        <fieldset>
          <h3>Datos Comerciales</h3>
          <div class="Row clearfix nombre-comercial">
          <?php         
            print drupal_render($form['Datos Personales']['profile_nombre_comercial']);
          ?>
          </div>
          <div class="Row Radiobutton clearfix sucursal">
          <?php  
            print drupal_render($form['Datos Personales']['profile_sucursal']);
          ?>
          </div>
          <div class="Row clearfix nombre-sucursal">
          <?php  
            print drupal_render($form['Datos Personales']['profile_nombre_sucursal']);
          ?>
          </div>
          <div class="Row clearfix">
            <div class="sucursal-calle">
              <?php  
                print drupal_render($form['Datos Personales']['profile_calle']);
              ?>
            </div>
          </div>
          <div class="Row clearfix">
            <div class="sucursal-altura">        
              <?php
                print drupal_render($form['Datos Personales']['profile_altura']);
              ?>
            </div>
          </div>
          <div class="Row clearfix">
            <div class="sucursal-piso">
              <?php
                print drupal_render($form['Datos Personales']['profile_piso']);
              ?>
            </div>
          </div>
          <div class="Row clearfix">
            <div class="sucursal-dto">    
              <?php
                print drupal_render($form['Datos Personales']['profile_departamento']);
              ?>
            </div>
          </div>
          <div class="Row clearfix cp">
          <?php  
            print drupal_render($form['Datos Personales']['profile_cp']);
          ?>
          </div>
          <div class="Row clearfix provincia">
          <?php  
            print drupal_render($form['Datos Personales']['profile_provincia']);
          ?>
          </div>
          <div class="Row clearfix ciudad">
          <?php  
            print drupal_render($form['Datos Personales']['profile_ciudad']);
          ?>
          </div>
          <div class="Row clearfix barrio">
          <?php  
            print drupal_render($form['Datos Personales']['profile_barrio']);
          ?>
          </div>
          <div class="Row clearfix">
            <?php
              //print_r($form);
              unset($form['locations'][0]['country']);
              unset($form['locations'][0]['locpick']['instructions']);
            ?>
            <label for="edit-picture-upload">  
              <?php print ''.$form['locations'][0]['#title'].':'; ?>
            </label>
            <?php print drupal_render($form['locations'][0]['locpick']['map']); ?>
            <div class="description">
              <span class="Ingresa"><?php print drupal_render($form['locations'][0]['locpick']['map_instructions']); ?></span>
            </div>
          </div>
          <div class="Row clearfix tipo-concesionaria">
          <?php  
            print drupal_render($form['Datos Personales']['profile_tipo_concesionaria']);
          ?>
          </div>
          <!-- <div class="Row clearfix">
          <?php
            /*$form['picture']['#title'] = "";
            $form['picture']['picture_upload_register']['#title'] = "Subir Logo";
            $form['picture']['picture_upload_register']['#size'] = 35;
            $form['picture']['picture_upload_register']['#description'] = '<span class="Ingresa">Seleccione la imagen del logo asociado a su concesionaria para que el mismo sea subido al servidor al momento de confirmar la modificación. Extensiones permitidas: JPG JPEG BMP PNG GIF. Tamaño máximo de archivo: 100kb.</span>';
            print drupal_render($form['picture']);*/
          ?>
          </div> -->
          <div class="Row clearfix">
          <?php
            print drupal_render($form['Datos Personales']['profile_sitio_web']);
          ?>
          </div>
        </fieldset>
        <fieldset>
          <h3>Datos de Contacto</h3>
          <div class="Row clearfix">
          <?php  
            $form['Datos Personales']['profile_nombre']['#title'] = "Nombre Responsable";
            print drupal_render($form['Datos Personales']['profile_nombre']);
          ?>
          </div>
          <div class="Row clearfix">
          <?php
            $form['Datos Personales']['profile_apellido']['#title'] = "Apellido Responsable";
            print drupal_render($form['Datos Personales']['profile_apellido']);
          ?>
          </div>
          <div class="Row clearfix telefono-sec">
          <?php
            print drupal_render($form['Datos Personales']['profile_telefono_secundario']);
          ?>
          </div>
        </fieldset>  
        <fieldset>
          <h3>Datos Fiscales</h3>
          <div class="Row clearfix razon-social">
          <?php
            print drupal_render($form['Datos Fiscales']['profile_razon_social']);
          ?>
          </div>
          <div class="Row clearfix cuit">
          <?php
            print drupal_render($form['Datos Fiscales']['profile_cuit']);
          ?>
          </div>
          <div class="Row clearfix iva">
          <?php
            print drupal_render($form['Datos Fiscales']['profile_condicion_iva']);
          ?>
          </div>
          <div class="Row clearfix">
            <div class="form-item"><label>Domicilio:</label></div>
            <div class="domicilio-mismo">
              <?php
                $form['Datos Fiscales']['profile_domicilio_fiscal_mismo']['#title'] = '<div class="domicilio-mismo-label">Usar datos comerciales</div>';
                print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_mismo']);
              ?>
            </div>
          </div>
          <div class="domicilio-fiscal">
            <div class="Row clearfix">
              <div class="sucursal-calle">
                <?php  
                  print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_calle']);
                ?>
              </div>
            </div>  
            <div class="Row clearfix">
              <div class="sucursal-altura">        
                <?php
                  print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_altura']);
                ?>
              </div>
            </div>  
            <div class="Row clearfix">
              <div class="sucursal-piso">
                <?php
                  print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_piso']);
                ?>
              </div>
            </div>  
            <div class="Row clearfix">
              <div class="sucursal-dto">    
                <?php
                  print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_dto']);
                ?>
              </div>
            </div>
            <div class="Row clearfix cp-fiscal">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_cp']);
            ?>
            </div>
            <div class="Row clearfix provincia-fiscal">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_provincia']);
            ?>
            </div>
            <div class="Row clearfix ciudad-fiscal">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_ciudad']);
            ?>
            </div>
            <div class="Row clearfix barrio-fiscal">
            <?php
              print drupal_render($form['Datos Fiscales']['profile_domicilio_fiscal_barrio']);
            ?>
            </div>
          </div>
        </fieldset>
        <div class="Row Checked clearfix condiciones">
          <?php
            $form['Datos Personales']['profile_condiciones']['#title'] = '';
            print drupal_render($form['Datos Personales']['profile_condiciones']);
          ?>
          <label class="check-condiciones">Acepto las <a target="_blank" href="https://losandes.com.ar/page/terms-and-conditions">condiciones</a> de utilización del servicio.</label>
        </div>
        <div class="Row Checked clearfix ofertas">
          <?php
            print drupal_render($form['Datos Personales']['profile_recepcion_ofertas']);
          ?>
        </div>
        <?php
          $form['Datos Personales']['#title'] = "";
          $form['Datos Usuario']['#title'] = "";
          $form['Datos Fiscales']['#title'] = "";
          unset($form['account']['mail']);
          unset($form['Datos Personales']['profile_fecha_nacimiento']);
          unset($form['Datos Personales']['profile_sexo']);
          unset($form['Datos Personales']['profile_tipo_usuario']);
          unset($form['Datos Personales']['profile_crear_sitio_web']);
          unset($form['picture']);
          //Campos Inmuebles
          unset($form['Datos Personales']['profile_matricula']);
          unset($form['Datos Personales']['profile_matricula_numero']);
          unset($form['Datos Personales']['profile_zona']);
          unset($form['Datos Fiscales']['profile_cuenta_principal']);
          $form['account']['#title'] = "";
        ?>
        <?php
          $form['submit']['#value'] = "Registrarme";
        ?>
        <div class="Row clearfix">
          <div class="Ingresa">
            <div class="Boton">
              
              <?php print drupal_render($form['submit']); ?>
            </div>
          </div>
        </div>
      </div>    
      <?php } ?>
      <div style="display:none;"><?php print drupal_render($form); ?></div>
    </div>
  </div>
</div>
<?php //print_r($form); ?>
