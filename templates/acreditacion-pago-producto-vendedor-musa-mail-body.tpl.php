<!-- DETALLE DE COMPRA -->
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px">
		<tbody>
		<tr>
			<td valign="top">
				<table width="222" border="0" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
					<td style="padding: 15px 30px 30px;">
						<a href="!link_aviso"><img typeof="foaf:Image" src="!foto" width="222" height="151" alt="!titulo_aviso" title="!titulo_aviso"></a>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
			<td valign="top">
				<table width="512" border="0" cellspacing="0" cellpadding="0" style="margin-right:38px; margin-bottom:50px;">
				<tbody>
				<tr>
					<td style="text-align:left; font-family:Arial, Helvetica, sans-serif;  text-transform:uppercase; color: #000;">
						<h2 style="font-size:14px;">SE ACREDIT&Oacute; EL PAGO DEL SIGUIENTE AVISO:</h2>
					</td>
				</tr>
				<tr>
					<td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:24px; ">
						<a href="!link_aviso" style="text-decoration:none; font-weight:100; color:#666666; text-transform:uppercase; ">!titulo_aviso</a>
					</td>
				</tr>
        <tr style="display: block;">
          <td style="text-align:left; font-family:Arial, Helvetica, sans-serif;  text-transform:uppercase; color: #000; font-size: 14px; padding: 10px 30px; font-weight: bold;">DATOS DEL COMPRADOR:</td>
        </tr>
        <tr>
          <td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding: 0 30px; ">Nombre: !nombre_comprador</td>
        </tr>
        <tr>
          <td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding: 0 30px; ">Tel&eacute;fono: !telefono_comprador</td>
        </tr>
        <tr>
          <td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding: 0 30px; ">Email: !email_comprador</td>
        </tr>
        <tr>
          <td style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:16px; color: #666;padding: 0 30px; ">Direcci&oacute;n: !direccion_comprador, !ciudad_comprador, !provincia_comprador</td>
        </tr>
				</tbody>
				</table>
			</td>
			<!-- Fin seccion nota uno -->
			<!--FIN NOTAS SECCION-->
		</tr>        
		</tbody>
		</table>
	</td>
</tr>

<!--Fin detalle de compra-->