<?php

/**
 * @file search-result.tpl.php
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $type: The type of search, e.g., "node" or "user".
 *
 * Default keys within $info_split:
 * - $info_split['type']: Node type.
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 * - $info_split['upload']: Number of attachments output as "% attachments", %
 *   being the count. Depends on upload.module.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for their existance before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 *
 *   <?php if (isset($info_split['comment'])) : ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 *
 * To check for all available data within $info_split, use the code below.
 *
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 *
 * @see template_preprocess_search_result()
 */
  // No mostrar aviso de un usuario particular
  if($result['fields']['is_uid'] != SITIO_LOCAL_UID_COLECCIONES) {
  
  $get = $_GET;
  $es_producto = 0;
  if(isset($get['f'])){ 
    foreach($get['f'] as $filtro) {
      if($filtro == "im_taxonomy_vid_".CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID.":6106"){
        $es_producto = 1;
        break;
      } elseif(strpos($filtro, "uid") !== FALSE) {
        $user_id = explode("uid:", $filtro);
        if(ventas_es_vendedor($user_id[1])) {
          $es_producto = 1;
          break;
        }
      }
    }
  }
  
  $espacio_class = 'Espacio-All Espacio-'.$result['fields']['is_espacio_nid'];
  $espacio_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_SUPER_PREMIUM_NID, 'Superpremium', $espacio_class);
  $espacio_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_PREMIUM_NID, 'Premium', $espacio_class);
  $espacio_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_A_NID, 'Estandar', $espacio_class);
  $espacio_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_B_NID, 'Estandar', $espacio_class);
  $espacio_class = str_replace(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_C_NID, 'Estandar', $espacio_class);
  
  //Logo Usuario
  $path_img = "";
  if($result['fields']['ts_picture'] != "" && file_exists($result['fields']['ts_picture'])) {
    $path_img = $result['fields']['ts_picture'];
  }
  
  $env_id = apachesolr_default_environment();
  $query = apachesolr_current_query($env_id);
  
  $buscador_alojamientos = 0;
  if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, CLASIFICADOS_RUBROS_MIX_ALOJAMIENTOS_TID))
    $buscador_alojamientos = 1;
  
  //Imagen
  $alt_img = explode('/', $result['fields']['url']);
  $alt_img = $alt_img[3].' de '.$alt_img[4].' '.str_replace('"', '', $title).' a '.$result['fields']['ts_precio_v'];
  if(isset($result['fields']['ss_externo']) && $result['fields']['ss_externo']=='argenprop') {
    if(!empty($result['fields']['ts_imagen'])) {
      $imagen = '<img src="http://static1.argenprop.com/image.axd?url='.$result['fields']['ts_imagen'].'&Height=90&Width=120"/>';
    } else {
      $imagen = theme('imagecache', 'ficha_aviso_120_90', 'imagefield_default_images/RB_noimagen.jpg');
    }
  } else {
    if($buscador_alojamientos) {
      $imagen = theme('imagecache', 'ficha_aviso_262_199', $result['fields']['ts_imagen'], $alt_img, $title);
    } elseif(in_array($result['fields']['is_espacio_nid'], array(AUTOSLAVOZ_ESPACIO_SUPER_PREMIUM_NID, AUTOSLAVOZ_ESPACIO_VIDRIERA_NID)) || $es_producto) {
      $imagen = theme('imagecache', 'ficha_aviso_173_115_sc', $result['fields']['ts_imagen'], $alt_img, $title);
    } else {
      $imagen = theme('imagecache', 'ficha_aviso_120_90_sc', $result['fields']['ts_imagen'], $alt_img, $title);
    }
  }
  
  // Entrega de producto
  if($result['node']->is_disponible_venta) {
    if(!empty($result['node']->ss_tipo_entrega)) {
      $entrega_texto = $result['node']->ss_tipo_entrega;
      switch($result['node']->ss_tipo_entrega){
        case 'Envío gratuito':
          $entrega = 'gratuito';
          break;
        case 'Envío a domicilio':
          $entrega = 'domicilio';
          break;
        case 'Retiro en sucursal':
          $entrega = 'sucursal';
          break;
      }
    }
  }
  
  $url_aviso = $url;
  if(isset($result['fields']['ss_externo']) && $result['fields']['ss_externo']=='argenprop') {
    $url_aviso = url('argenprop/'.$result['fields']['is_id_externo']);
  }
  
  $rubro_autos = clasificados_rubros_obtener_rubro_raiz_tipo_aviso('aviso_auto');
  $rubros_autos = clasificados_rubros_obtener_rubros_hijos($rubro_autos->tid);
  $rubros_autos[$rubro_autos->tid] = $rubro_autos;
  $rubro_auto_aplicado = FALSE;
  foreach($rubros_autos as $tid => $rubro) {
    if($query->hasFilter('im_taxonomy_vid_'.CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID, $tid)) {
      $rubro_auto_aplicado = TRUE;
    }
  }
  
  // Url comercio
  if($result['node']->is_disponible_venta) {
    $user_url = $result['node']->ss_url_comercio;
  } else {
    $user_url = clasificados_apachesolr_obtener_usuario_url($result['fields']['is_uid']);
  }
  
  // Cantidad comentarios
  if($buscador_alojamientos) {
    $reputacion = clvi_reputacion_porcentaje_votos($result['node']->entity_id);
  }
?>

<?php if($es_producto) { ?>

<div class="CajaAviso">
  <?php if($result['fields']['ts_banderola'] != ''){ ?>
    <span class="etiqueta <?php print $result['fields']['ts_banderola']; ?>"></span>
  <?php } ?>
  <div class="imgAviso">
    <a href="<?php print $url_aviso; ?>" title="<?php print $title; ?>">
      <span></span><?php print $imagen; ?>
    </a>
  </div>
  <div class="AvisoDescripcion">
    <a href="<?php print $url_aviso; ?>" title="<?php print $title; ?>"><h4><?php print mb_substr(htmlspecialchars_decode($title, ENT_QUOTES), 0, 39); if(strlen($title)>39) print '...'; ?></h4></a>
    <?php if(isset($result['node']->ss_nombre_tienda) && $result['node']->ss_nombre_tienda != '') { ?>
      <a class="link-tienda" href="<?php print $user_url; ?>" title="<?php print $result['node']->ss_nombre_tienda; ?>"><?php print mb_strtoupper($result['node']->ss_nombre_tienda, 'UTF-8'); ?></a>
    <?php } ?>
    <span class="mensajePago">Precio en 1 pago</span>
    <div><span class="precio"><?php print $result['fields']['ts_precio_v']; ?></span></div>
    <?php if(isset($result['node']->ss_precio_anterior)) { ?>
      <span class="precio_anterior"><?php print $result['node']->ss_precio_anterior; ?></span>
      <?php if(isset($result['fields']['fs_oferta_monto'])) { ?>
        <span class="descuento"><?php print '<strong>'.$result['fields']['fs_oferta_porcentaje'].'%</strong> OFF'; ?></span>
      <?php } ?>
    <?php } ?>
    
    <a class="link-search-contacto search-producto" href="javascript:void(0);" data-nid="<?php print $result['fields']['entity_id']; ?>" data-title="<?php print mb_substr($title, 0, 55); if(strlen($title)>55) print '...'; ?>" title="Consulta por aviso <?php print $title; ?>">Consultar</a>
    
    <?php if($result['node']->is_disponible_venta) { ?>  
      <?php if(isset($entrega)) { ?>
        <div class="entrega <?php print $entrega; ?>"><span></span><p><?php print $entrega_texto; ?></p></div>
      <?php } ?>
      <a class="btn-comprar" href="/comprar/<?php print $result['node']->entity_id; ?>/datos" title="Comprar <?php print $title; ?>"><span></span>Comprar</a>
    <?php } ?>
    <?php
      if(isset($result['h2']) && $result['h2']) {
        print '<h2 class="result-descripcion">';
        print $result['fields']['ss_subrubro']; if(isset($result['fields']['ts_razon']) && !empty($result['fields']['ts_razon'])) print ' en '.$result['fields']['ts_razon']; if(isset($result['fields']['ts_precio_v']) && !empty($result['fields']['ts_precio_v'])) print ' por '.$result['fields']['ts_precio_v'];
        print '</h2>';
      } elseif(isset($result['h3']) && $result['h3']) {
        print '<h3 class="result-descripcion">';
        print $result['fields']['ss_subrubro']; if(isset($result['fields']['ts_razon']) && !empty($result['fields']['ts_razon'])) print ' en '.$result['fields']['ts_razon']; if(isset($result['fields']['ts_precio_v']) && !empty($result['fields']['ts_precio_v'])) print ' por '.$result['fields']['ts_precio_v'];
        print '</h3>';
      } else {
        print '<h5 class="result-descripcion">';
        print $result['fields']['ss_subrubro']; if(isset($result['fields']['ts_razon']) && !empty($result['fields']['ts_razon'])) print ' en '.$result['fields']['ts_razon']; if(isset($result['fields']['ts_precio_v']) && !empty($result['fields']['ts_precio_v'])) print ' por '.$result['fields']['ts_precio_v'];
        print '</h5>';
      }
    ?>
  </div>
</div>

<?php } else { ?>

<div class="BoxResultado Borde <?php print $espacio_class; if(in_array($result['fields']['is_espacio_nid'], array(AUTOSLAVOZ_ESPACIO_LOSANDES_GRATUITO_NID))) print ' '.$result['fields']['ts_destacado_color']; ?>">
<!-- >Nombre del tipo de clase "unico" "comoNuevo" "imperdible"<!-->
<?php if($result['fields']['ts_banderola'] != ''){ ?>
  <span class="etiqueta <?php print $result['fields']['ts_banderola']; ?>"></span>
<?php } ?>
  <div class="Left">
    <div class="foto">  
      <a href="<?php print $url_aviso; ?>" title="<?php print $title; ?>"><span></span><?php print $imagen; ?></a>
      <?php if(!empty($result['fields']['is_cantidad_fotos'])){ ?>
        <span class="cintillo"><?php print $result['fields']['is_cantidad_fotos']; ?></span>          
      <?php } ?>
      <?php if(!empty($result['fields']['is_disponible_venta'])){ ?>
        <span class="DisponibleVenta"></span>
      <?php } ?>
    </div>    
  </div>
  <?php if (in_array($result['fields']['ss_rol'], array('Comercio', 'Inmobiliaria', 'Concesionaria', 'Casa de repuestos y accesorios'))) { ?>
    <div class="Right">
      <div class="avatar">
        <div class="Banner">
          <?php if($path_img != ''){ ?>
          <a href="<?php print $user_url; ?>" title="Avisos de <?php print $result['fields']['ts_razon']; ?>">
              <?php print clasificados_imagecache('logo_66_48', $path_img, 'Logo de '.$result['fields']['ts_razon'], 'Logo de '.$result['fields']['ts_razon']); ?>
            </a>
          <?php } else { ?>
            <div class="concesionar-sin-avatar">&nbsp;</div>
          <?php } ?>
        </div>
<?php if($result['fields']['ss_rol'] != 'Casa de repuestos y accesorios'): ?>
        <p><a href="<?php print $user_url; ?>" title="Avisos de <?php print $result['fields']['ts_razon']; ?>"><?php print $result['fields']['ss_rol']; ?></a></p>
<?php else: ?>
        <p>&nbsp;</p>
<?php endif; ?>
      </div>
    </div>
    <?php } else { ?>
    <div class="Right">
      <div class="avatar">
        <div class="Banner">
          <?php print clasificados_imagecache('logo_66_48', $result['fields']['ts_picture'], $result['fields']['ss_ciudad'], $result['fields']['ss_ciudad']); ?>
        </div>
          <p><?php print $result['fields']['ss_rol']; ?></p>
      </div>
    </div>
    <?php } ?>                
  <div class="Info clearfix">
    
<?php if(isset($result['fields']['ts_marca'])): ?>
    <!-- pie autos -->
    <?php
      if(isset($result['h2']) && $result['h2']) {
        print '<h2 class="result-descripcion">'.$result['fields']['ts_marca'].' '.$result['fields']['ts_modelo'].' - '.$result['fields']['ss_ciudad'].'</h2>';
      } elseif(isset($result['h3']) && $result['h3']) {
        print '<h3>'.$result['fields']['ts_marca'].' '.$result['fields']['ts_modelo'].' - '.$result['fields']['ss_ciudad'].'</h3>';
      } else {
        print '<h5 class="result-descripcion">'.$result['fields']['ts_marca'].' '.$result['fields']['ts_modelo'].' - '.$result['fields']['ss_ciudad'].'</h5>';
      }
    ?>
<?php elseif(!in_array($result['fields']['bundle'], array('aviso_producto', 'aviso_servicio', 'aviso_rural'))): ?>
    <!-- pie inmuebles -->
    <?php
      if(isset($result['h2']) && $result['h2']) {
        print '<h2 class="result-descripcion">';
        print $result['fields']['ss_rubro'].' - '.$result['fields']['ss_operacion'].' - '; if(isset($result['fields']['ss_cantidad_dormitorios']) && !empty($result['fields']['ss_cantidad_dormitorios'])) print $result['fields']['ss_cantidad_dormitorios'].' - ';  print $result['fields']['ss_ciudad']; if(isset($result['fields']['ss_barrio']) && !empty($result['fields']['ss_barrio'])) print ' - '.$result['fields']['ss_barrio'];
        print '</h2>';
      } elseif(isset($result['h3']) && $result['h3']) {
        print '<h3>';
        print $result['fields']['ss_rubro'].' - '.$result['fields']['ss_operacion'].' - '; if(isset($result['fields']['ss_cantidad_dormitorios']) && !empty($result['fields']['ss_cantidad_dormitorios'])) print $result['fields']['ss_cantidad_dormitorios'].' - ';  print $result['fields']['ss_ciudad']; if(isset($result['fields']['ss_barrio']) && !empty($result['fields']['ss_barrio'])) print ' - '.$result['fields']['ss_barrio'];
        print '</h3>';
      } else {
        print '<h5 class="result-descripcion">';
        print $result['fields']['ss_rubro'].' - '.$result['fields']['ss_operacion'].' - '; if(isset($result['fields']['ss_cantidad_dormitorios']) && !empty($result['fields']['ss_cantidad_dormitorios'])) print $result['fields']['ss_cantidad_dormitorios'].' - ';  print $result['fields']['ss_ciudad']; if(isset($result['fields']['ss_barrio']) && !empty($result['fields']['ss_barrio'])) print ' - '.$result['fields']['ss_barrio'];
        print '</h5>';
      }
    ?>
<?php else: ?>
    <!-- pie general -->
    <?php
      if(isset($result['h2']) && $result['h2']) {
        print '<h2 class="result-descripcion">';
        print $result['fields']['ss_rubro']; if(isset($result['fields']['ss_subrubro']) && !empty($result['fields']['ss_subrubro'])) print ' - '.$result['fields']['ss_subrubro']; if(in_array($result['fields']['bundle'], array('aviso_producto')) && !empty($result['fields']['ss_usado_v'])) print ' - '.$result['fields']['ss_usado_v'];
        print '</h2>';
      } elseif(isset($result['h3']) && $result['h3']) {
        print '<h3>';
        print $result['fields']['ss_rubro']; if(isset($result['fields']['ss_subrubro']) && !empty($result['fields']['ss_subrubro'])) print ' - '.$result['fields']['ss_subrubro']; if(in_array($result['fields']['bundle'], array('aviso_producto')) && !empty($result['fields']['ss_usado_v'])) print ' - '.$result['fields']['ss_usado_v'];
        print '</h3>';
      } else {
        print '<h5 class="result-descripcion">';
        print $result['fields']['ss_rubro']; if(isset($result['fields']['ss_subrubro']) && !empty($result['fields']['ss_subrubro'])) print ' - '.$result['fields']['ss_subrubro']; if(in_array($result['fields']['bundle'], array('aviso_producto')) && !empty($result['fields']['ss_usado_v'])) print ' - '.$result['fields']['ss_usado_v'];
        print '</h5>';
      }
    ?>
<?php endif; ?>
    
    <div class="Left ItemGeneral">
      <?php if(!in_array($result['fields']['is_espacio_nid'], array(AUTOSLAVOZ_ESPACIO_SUPER_PREMIUM_NID, AUTOSLAVOZ_ESPACIO_VIDRIERA_NID))){ ?>
        <div class="Modelo<?php if(!$rubro_auto_aplicado) echo ' ModeloFull';?>">
          <a href="<?php print $url; ?>" title="<?php print $title; ?>"><h4><?php print mb_substr($title, 0, 55); if(strlen($title)>55) print '...'; ?></h4></a>
        </div>
      <?php } ?>
<?php if($rubro_auto_aplicado): ?>
        <div class="anio"><?php print (!empty($result['fields']['iss_anio'])) ? $result['fields']['iss_anio'] : '&nbsp;'; ?></div>
  <?php if(isset($result['fields']['fs_kilometros'])): ?>
        <div class="km"><?php print $result['fields']['fs_kilometros']." Km"; ?></div>
  <?php else: ?>
    <?php if(isset($result['fields']['ss_sin_kilometros']) && $result['fields']['ss_sin_kilometros'] == 'Si'): ?>
        <div class="km"><?php print 'No Especif'; ?></div>
    <?php else: ?>
        <div class="km">&nbsp;</div>
    <?php endif; ?> 
  <?php endif; ?>
      <div class="combustible"><?php print (!empty($result['fields']['ss_combustible_v'])) ? $result['fields']['ss_combustible_v'] : '&nbsp;'; ?></div>
<?php endif; ?>
      <div class="cifra">
        <?php print $result['fields']['ts_precio_v']; ?>
      </div>
    </div>
    <?php if(in_array($result['fields']['is_espacio_nid'], array(AUTOSLAVOZ_ESPACIO_SUPER_PREMIUM_NID, AUTOSLAVOZ_ESPACIO_VIDRIERA_NID))){ ?>
      <div class="Modelo"><a href="<?php print $url; ?>" title="<?php print $title; ?>"><h4><?php print mb_substr($title, 0, 55); if(strlen($title)>55) print '...'; ?></h4></a></div>
    <?php } ?>
    <div class="Descripcion"><p><?php if (strlen($result['fields']['ts_teaser']) > 80) print mb_substr($result['fields']['ts_teaser'], 0, 80, 'utf-8')."..."; else print $result['fields']['ts_teaser']; ?></p></div>
    <div class='pie-aviso-teaser'><?php print 'Publicado: '.$result['fields']['ts_publicacion']; ?></div>
  </div>
  
<?php if(isset($result['fields']['is_precio_uva']) && $result['fields']['is_precio_uva']) : ?>  
  <div class="search-precio-uva <?php print $result['fields']['entity_id']; ?>" data-precio="<?php print $result['fields']['fs_precio']; ?>" data-moneda="1" data-aviso="<?php print $result['fields']['entity_id']; ?>"></div>
<?php endif; ?>
  
  <a class="link-search-contacto" href="javascript:void(0);" data-nid="<?php print $result['fields']['entity_id']; ?>" data-title="<?php print mb_substr($title, 0, 55); if(strlen($title)>55) print '...'; ?>" title="Consulta por aviso <?php print $title; ?>">Consultar</a>
  
</div>

<?php } ?>

<?php
if(isset($result['banner_dfp'])) {
  print '<div class="banner">'.$result['banner_dfp'].'</div>';
}

}