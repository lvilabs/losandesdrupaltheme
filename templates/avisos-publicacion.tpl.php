<?php
  global $user;
  $nodo_nid = arg(1);
  
  //Obtener espacios y creditos del usuario
  $aviso = node_load($nodo_nid);
  //Obtengo rubro del aviso
  $rubro = clasificados_rubros_node_obtener_rubro_term($aviso);

  $usuario_id = $user->uid;
  if(module_exists('usuarios_colaboradores') && in_array('colaborador inmobiliaria', $user->roles)){
    $permisos = usuarios_colaboradores_get_permisos_user($user->uid);
    if(in_array('Utilizar espacios', $permisos)){
      $user_principal = usuarios_colaboradores_get_usuario_principal($user->uid);
      $usuario_id = $user_principal->uid;
    }
  }
  $array_espacios = _publicacion_avisos_espacios_adquiridos_usuario_aviso($usuario_id, $rubro->tid);
  $array_creditos = _publicacion_avisos_creditos_adquiridos_usuario_aviso($usuario_id, $rubro->tid);
  
  //Obtener pagos en espera
  $array_pago = compras_obtener_productos_compra($aviso->nid);
  
  //Quitamos espacios estandar para avisos que no sean servicios
  if($aviso->type != "aviso_servicio"){
    foreach($form['espacios'] as $key => $espacio) {
      if(strpos($espacio['#title'], 'Servicios') !== FALSE)
        unset($form['espacios'][$key]);
    }
  }
  
  
?>
<div class="Content CrearAnuncio Publicacion clearfix">
  <div class="blanco">
    <?php if (!isset($form['publicado'])){ ?>
      <div class="pasosbreadcrumb clearfix aviso">
        <div class="pasos clearfix">
          <div class="paso1">Datos del aviso</div>  
          <div class="paso2 active"><label>Publicación</label></div>  
        </div>
      </div>
    <?php } 
    foreach($array_pago as $pago){ 
      $node_producto = node_load($pago);?>
      <li><?php print ucfirst($node_producto->type).": <strong>".$node_producto->title."</strong>." ?></li>
    <?php } ?> 
    <div class="FormCrear"> 
    </div>
    <div class="FormRegistro ContactoAutos Avisos">
      <fieldset class="espacios">
        <h3>Espacios</h3>
        <div class="Row clearfix">
          <?php 
            $mostrando_titulo_oro = FALSE;
            $mostrando_titulo_false = FALSE;
            $grupo = '';
            $grupo_espacio_anterior = '';
            $form['espacios']['#title'] = "";
            foreach($form['espacios'] as $key => $valor){
              if (is_numeric($key)){
                //Datos del espacio
                $espacio = node_load($key);
                $total_espacio = (isset($array_espacios[$key])) ? count($array_espacios[$key]) : 0;
                $disp_espacio = 0;
                $reusable = 0;
                if($total_espacio > 0){
                  foreach($array_espacios[$key] as $nodo){
                    if($nodo["aviso"] == "" || $nodo["aviso"] == 0){
                      $disp_espacio++;
                      if($nodo["reusable"])
                        $reusable = 1;
                    } else if($nodo["aviso"] == $aviso->nid && $aviso->type == 'aviso_alquiler_temporario') {
                      // En caso de alquiler temporario, podemos reusar el mismo espacio que tenia asignado el aviso si aún no venció.
                      $disp_espacio++;
                    }
                    if($nodo["aviso"] == $aviso->nid && $nodo["reusable"])
                      $reusable = 1;
                    
                  }
                } ?>

<!-- No permitir publicar dos veces en el mismo dia un aviso con el mismo destaque reusable -->
<?php
$republicacion = 1;

?>

<!-- No permitir publicar un aviso en un destaque reusable si ya no posee por el uso del dia -->
<?php
$disponible_dia = array();
?>
            <?php

            if(strpos($form['espacios'][$key]['#title'], 'Oro')===0) {
              $grupo = 'oro';
            } else if(strpos($form['espacios'][$key]['#title'], 'Plata')===0) {
              $grupo = 'plata';
            } else {
              $grupo = '';
            }
            if($grupo_espacio_anterior!=='' && $grupo!==$grupo_espacio_anterior) {
            ?><!-- cierra grupo -->
            </div>
            <?php
            }
            $grupo_espacio_anterior = $grupo;
            if($mostrando_titulo_oro==FALSE && strpos($form['espacios'][$key]['#title'], 'Oro')===0) {
              $mostrando_titulo_oro = TRUE;
            ?>
            <div class="grupo">
                <div class="nombre">Aviso Oro</div><div class="VerEjemplo VerMas" data-detalle-id="<?php print 'detalle-' . $espacio->nid; ?>"><a class="MasInfo">+ Info</a></div>
            <?php
            } elseif($mostrando_titulo_plata==FALSE && strpos($form['espacios'][$key]['#title'], 'Plata')===0) {
              $mostrando_titulo_plata = TRUE;
            ?>
            <div class="grupo">
                <div class="nombre">Aviso Plata</div><div class="VerEjemplo VerMas" data-detalle-id="<?php print 'detalle-' . $espacio->nid; ?>"><a class="MasInfo">+ Info</a></div>
            <?php } ?>
                <!-- Div general del espacio --> 
                <div class="ContentEspacio <?php if($form['espacios']['#default_value'] == $key) print "seleccionado"; if(in_array($key, $array_pago)) print "comprado"; if(!$republicacion) print 'deshabilitado'; ?>">

<?php
if(clasificados_rubros_node_pertenece_al_grupo_mix($aviso)) {
  if($key == AUTOSLAVOZ_ESPACIO_ESTANDAR_A_NID && $aviso->type == 'aviso_servicio') {
    $form['espacios'][$key]['#title'] = 'Estandar 30 días';
  }
  if($key == AUTOSLAVOZ_ESPACIO_ESTANDAR_A_NID && $republicacion) {
    $espacio->body = 'Duración: 30 días. 6 fotos y foto en listado de avisos. Posición superior en el buscador.';
  }
  if($key == AUTOSLAVOZ_ESPACIO_LOSANDES_GRATUITO_NID && $aviso->type == 'aviso_servicio' && $republicacion) {
    $espacio->body = str_replace('30 días.', '180 días', $espacio->body);
  }
}
?>

                  <!-- Radio-button -->
                  <?php 
                  if($republicacion) { 
                    print drupal_render($form['espacios'][$key]); 
                  } else {
                    print '<div class="form-item">'.$form['espacios'][$key]['#title'].'</div>';
                    unset($form['espacios'][$key]);
                  }
                  ?>

                  <!-- Div del resto de la info del espacio --> 
                  <div class="PrecioDetalle">
                  <?php if($form['espacios']['#default_value'] == $key){ ?>
                    <div class="ElPrecio aplicado">Aplicado</div>
                  <?php } elseif($espacio->nid == AUTOSLAVOZ_ESPACIO_LOSANDES_GRATUITO_NID){ ?>
                    <div class="ElPrecio">Sin Costo</div>
                  <?php } elseif($espacio->nid == AUTOSLAVOZ_ESPACIO_LOSANDES_BASICO_NID){ ?>
                    <div class="ElPrecio">Ilimitado</div>
                  <?php } else { ?>
                    
                    <?php if(in_array($key, $array_pago)) { ?>
                      <ul class="alerta-compra">Pago no procesado. <br /><a href="/estado_cuenta">Confirmalo o cancelalo aquí</a>.
                      </ul>
                    
                    <?php } elseif($disp_espacio > 0){ 
                      $disponibles = $disp_espacio;
                      if(is_numeric($disponible_dia['usados'])){
                        $disponible_dia_libres = $disponible_dia['total'] - $disponible_dia['usados'];
                        if($disponible_dia_libres < $disponibles) $disponibles = $disponible_dia_libres;
                      }
                    ?>
                      <div class="ElPrecio disponibilidad espacio">Disponible: <?php print $disponibles; ?> de <?php print $total_espacio; ?></div>
                    <?php } elseif ($form['espacios']['#default_value'] != $key && !in_array($key, $array_pago)) { ?>
                      <?php
                        $precios_normales = array();
                        if(clasificados_rubros_node_pertenece_al_grupo_autos($aviso)) {
                          $precios_normales = array(7 => 65, 8 => 100);
                        }
                        else if(clasificados_rubros_node_pertenece_al_grupo_inmuebles($aviso)) {
                          $precios_normales = array(7 => 100, 8 => 120);
                        }
                        else {
                          $precios_normales = array(7 => 42);
                        }
                      ?>
                      <?php if(COMPRAS_ES_PERIODO_PROMO && isset($precios_normales[$espacio->nid])):?>
                                            <div class="ElPrecio promoContent">
                                              <div class="descuento"><strong>50</strong>%OFF</div>
                                              <div class="precios"><strong>$<?php print precios_obtener_precio_producto($espacio->nid, $rubro->tid); ?></strong>
                                              <span class="antes">antes $<?php print $precios_normales[$espacio->nid]; ?></span></div>
                                            </div>
                      <?php else: ?>
                                            <div class="ElPrecio">$<?php print precios_obtener_precio_producto($espacio->nid, $rubro->tid); ?></div>
                      <?php endif; ?>
                      <div class="no-reusable">No Reutilizable</div>
                    <?php } ?>
                    <?php if($reusable == 1){ ?>
                      <div class="reusable">Reutilizable</div>
                    <?php } ?>
                  <?php } ?>
                  </div>
                  
<?php
                    //Datos del ejemplo para espacios
                    $ejemplo_espacio_id = '';
                    $ejemplo_espacio_img = '';
                    switch($espacio->nid){
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_GRATUITO_NID:
                        $ejemplo_espacio_id = 'img-gratuito';
                        $ejemplo_espacio_img = 'ejemplos_destaques_gratuito.jpg';
                        if($aviso->type=='aviso_alquiler_temporario') { // Caso especial alojamientos
                          $ejemplo_espacio_img = 'ejemplos_destaques_gratuito_alojamiento.jpg';
                          $espacio->body = '<p>Ubicación: <span>Último</span></p>
                            <p>&nbsp;</p>
                            <p>3 Fotos</p>
                            <p>&nbsp;</p>
                            <p>Duración: <span>180 días</span></p>';
                        }
                        break;
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_BASICO_NID:
                        $ejemplo_espacio_id = 'img-basico';
                        $ejemplo_espacio_img = 'ejemplos_destaques_básico.jpg';
                        break;
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_A_NID:
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_B_NID:
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_C_NID:
                        $ejemplo_espacio_id = 'img-destacado';
                        $ejemplo_espacio_img = 'ejemplos_destaques_destacado.jpg';
                        break;
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_PREMIUM_NID:
                        $ejemplo_espacio_id = 'img-premium';
                        $ejemplo_espacio_img = 'ejemplos_destaques_premium.jpg';
                        break;
                      case AUTOSLAVOZ_ESPACIO_LOSANDES_SUPER_PREMIUM_NID:
                        $ejemplo_espacio_id = 'img-superpremium';
                        $ejemplo_espacio_img = 'ejemplos_destaques_superpremium.jpg';
                        break;
                    }
                    if(clasificados_rubros_node_pertenece_al_grupo_mix($aviso) && in_array($espacio->nid, array(AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_A_NID, AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_B_NID, AUTOSLAVOZ_ESPACIO_LOSANDES_ESTANDAR_C_NID))) {
                        $ejemplo_espacio_id = 'img-destacado-servicio';
                        $ejemplo_espacio_img = 'ejemplos_destaques_destacado.jpg';
                    }
?>

                  <div class="VerEjemplo VerMas" data-detalle-id="<?php print 'detalle-' . $espacio->nid; ?>"><a>+ Info</a></div>
                  <div id="detalle-<?php print $espacio->nid; ?>" class="oculto GloboDetalle" style="display: none;">
                    <div class="Gris">
                      <div class="detalle-body"><?php print $espacio->body; ?></div>
                      <!-- <img id="<?php print $ejemplo_espacio_id; ?>" src="/<?php print path_to_theme(); ?>/img/<?php print $ejemplo_espacio_img; ?>"/> -->
                      
                    </div>
                  </div>
                  
                  <div class="detalle-oculto">
                    <div id="titulo-<?php print $key; ?>"><?php print $espacio->title; ?></div>
                    <div id="precio-<?php print $key; ?>">
                      <?php if($espacio->nid == AUTOSLAVOZ_ESPACIO_LOSANDES_GRATUITO_NID || $espacio->nid == AUTOSLAVOZ_ESPACIO_LOSANDES_BASICO_NID) print '0.00'; else print precios_obtener_precio_producto($espacio->nid, $rubro->tid); ?></div>
                    <div id="disponibilidad-<?php print $key; ?>"><?php print $disp_espacio; ?></div>
                    <div id="reusable-<?php print $key; ?>"><?php print $reusable; ?></div>
                  </div>
                  
                </div>
                <!--Fin contenedor general-->
                
              <?php }
            }
            if($grupo!=='') {
            ?>
            </div>
            <?php
            }
            print drupal_render($form['espacios']); 
          ?>
        </div>
      </fieldset>
      
      <?php
      // Sugerimos el texto web-papel.
      $texto_sugerido = clasificados_web_papel_get_texto_sugerido($aviso);
      $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
      if($publicacion!=FALSE) {
        // Tomamos el texto de la ultima publicacion en papel, si es que hay.
        $texto_sugerido = $publicacion->texto;
      }
      $publicacion_papel_concesionaria = FALSE;
      $publicacion_papel_inmobiliaria = FALSE;
      $publicacion_papel_comercio = FALSE;
      $publicacion_papel_clasifoto = FALSE;
      ?>
      
      <?php //Si es Concesionaria SDClass y no tiene contratado un pack papel web, mostramos banner
        if(in_array('concesionaria terceros', $user->roles) && !isset($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]) && !isset($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID])) { ?>
        <!-- Banner Promocional
      <?php } ?>
      
      <!-- Credito Web-papel Concesionaria -->
      <?php 
      $credito_papel_disp = 0;
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID])){
        $credito_papel_total = count($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]);
        $reusable = 0;
        foreach($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID] as $key => $value){
          if(empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID][$key]['aviso'])){
            $credito_papel_disp++;
          }
        }
      }
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]) && 
      isset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]) &&
      $credito_papel_disp>0){
        $credito_papel_concesionaria = node_load(AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID);
        $check_papel_concesionaria = $form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID];
        $publicacion_papel_concesionaria = TRUE;
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-concesionaria" name="publicacion-papel-concesionaria">Publicar en Papel Concesionaria</a></h3>
            <div class="Row clearfix">
              <?php
                
                if((isset($check_papel_concesionaria['#attributes']['checked']) && $check_papel_concesionaria['#attributes']['checked'] == 'checked') || in_array($credito_papel_concesionaria->nid, $array_pago)){
                  $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                  if($publicacion!=FALSE) {
                    $form['text-papel-concesionaria']['#value'] = $publicacion->texto;
                    $primer_dia_semana = date("W", strtotime($publicacion->fecha_publicacion));
                    $anio_pub = date("o", strtotime($publicacion->fecha_publicacion));
                    if(intval(date('W')) >= intval($primer_dia_semana) || $anio_pub < date('o')) {
                      // Si ya pasó la semana, habilitamos para que pueda volver a publicar.
                      unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]['#attributes']['checked']);
                      unset($check_papel_concesionaria['#attributes']['checked']);
                    }
                  }
                } else {
                  if(empty($form['text-papel-concesionaria']['#value'])) // Si viene de la validación el texto ya existe.
                    $form['text-papel-concesionaria']['#value'] = $texto_sugerido;
                }
              ?>    
              <!-- Div general mejora papel -->
              <div class="<?php if(isset($check_papel_concesionaria['#attributes']['checked']) && $check_papel_concesionaria['#attributes']['checked'] == 'checked') print "seleccionado"; if(in_array($credito_papel_concesionaria->nid, $array_pago)) print "comprado"; ?> clearfix">
                <!-- Check-box --> 
                <?php print drupal_render($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]); ?>
                <?php if($credito_papel_disp > 0){ ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $credito_papel_disp; ?></div>
                  <?php if($reusable == 1){ ?>
                    <div class="reusable">Reutilizable</div>
                  <?php } ?>
                    <div class="disponibilidad credito check">&nbsp;</div>
                  </div>
                <?php } else { ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio">$<?php print precios_obtener_precio_producto($credito_papel_concesionaria->nid, $rubro->tid); ?></div>
                  </div>
                <?php } ?>
                <div id="content-papel-concesionaria" class="clearfix">
                  <?php
                    print drupal_render($form['text-papel-concesionaria']);
                  ?>
                  <div class="CajaPapel">
                    <?php
                      //Obtener días de publicación
                      $fechas = _administracion_avisos_obtener_fechas_papel('Concesionaria');
                      //Busco el texto agregado si está seleccionado o en espera de pago
                      if((isset($check_papel_concesionaria['#attributes']['checked']) && $check_papel_concesionaria['#attributes']['checked'] == 'checked') || in_array($credito_papel_concesionaria->nid, $array_pago)){
                        $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                        if($publicacion!=FALSE) {
                          $tiempo_viernes = strtotime($publicacion->fecha_publicacion);
                          $dia_domingo = date('d', $tiempo_viernes-432000);
                          $dia_lunes = date('d', $tiempo_viernes-345600);
                          $dia_martes = date('d', $tiempo_viernes-259200);
                          $dia_miercoles = date('d', $tiempo_viernes-172800);
                          $dia_jueves = date('d', $tiempo_viernes-86400);
                          $dia_viernes = date('d', $tiempo_viernes);
                          $dia_sabado = date('d', $tiempo_viernes+86400);
                          $form['text-papel-concesionaria']['#value'] = $publicacion->texto;
                        } else {
                          $dia_domingo = '';
                          $dia_lunes = '';
                          $dia_martes = '';
                          $dia_miercoles = '';
                          $dia_jueves = '';
                          $dia_viernes = '';
                          $dia_sabado = '';
                        }
                      } else {
                        if(isset($fechas['domingo'])) {
                          $dia_domingo = date('d', strtotime($fechas['lunes'])-86400);
                        } else {
                          $dia_domingo = date('d', strtotime($fechas['lunes'])-172800); // Es el sabado!
                        }
                        $dia_lunes = date('d', strtotime($fechas['lunes']));
                        $dia_martes = date('d', strtotime($fechas['lunes'])+86400);
                        $dia_miercoles = date('d', strtotime($fechas['miercoles']));
                        $dia_jueves = date('d', strtotime($fechas['miercoles'])+86400);
                        $dia_viernes = date('d', strtotime($fechas['viernes']));
                        $dia_sabado = date('d', strtotime($fechas['viernes'])+86400);
                        }
                    ?>
                    <p class="p_txt"><strong>Publica los días:</strong><br>
                    <table class="tabla_dias">
                      <tr>
                        <th><?php echo (isset($fechas['domingo']))?'Dom':'Sáb'; ?> <?php print $dia_domingo; ?></th>
                        <th>Lun <?php print $dia_lunes; ?></th>
                        <th>Mar <?php print $dia_martes; ?></th>
                        <th>Mié <?php print $dia_miercoles; ?></th>
                        <th>Jue <?php print $dia_jueves; ?></th>
                        <th>Vie <?php print $dia_viernes; ?></th>
                        <th>Sáb <?php print $dia_sabado; ?></th>
                      </tr>
                      <tr>
                        <td>X</td>
                        <td>X</td>
                        <td></td>
                        <td>X</td>
                        <td></td>
                        <td>X</td>
                        <td></td>
                      </tr>
                    </table>
                    <br><strong style="color:Red;">Las fechas pueden variar dependiendo de la acreditación del pago</strong></p>
                  </div>
                  <a class="papel-reco colorbox-inline" href="?width=490&height=485&inline=true#img-papel-reco">Ver recomendaciones</a>
                  <!-- Div del body mejora papel -->
                  <div class="detalle credito check"><?php print $credito_papel_concesionaria->body; ?></div>
                  <div style="display: none;"><img id="img-papel-reco" src="/<?php print path_to_theme(); ?>/img/recomendacion_papel.jpg"/></div>
                  <div id="textareaCallBack"></div>
                </div>
                <div class="papel-previsualizar">
                  <input type="button" value="Previsualizar" class="preview">
                  <div class="simulacion-papel"></div>
                </div>
                <?php
                $total_credito = 0;
                if(isset($array_creditos[$credito_papel_concesionaria->nid]))
                  $total_credito = count($array_creditos[$credito_papel_concesionaria->nid]);
                $disp_credito = 0;
                if($total_credito > 0){
                  foreach($array_creditos[$credito_papel_concesionaria->nid] as $nodo){
                    if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                      $disp_credito++;
                  }
                }
                $titulo_oculto = $credito_papel_concesionaria->title;
                $id_oculto = $credito_papel_concesionaria->nid;
                ?>
                <div class="detalle-oculto">
                  <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                  <div id="precio-<?php print $id_oculto; ?>"><?php print precios_obtener_precio_producto($credito_papel_concesionaria->nid, $rubro->tid); ?></div>
                  <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                </div>
              </div>
            </div>
        </fieldset>
      <?php } elseif(in_array('concesionaria terceros', $user->roles)) { 
        $creditos_papel_concesionaria = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID);
        if(!empty($creditos_papel_concesionaria) && $creditos_papel_concesionaria['cantidad'] == 0) {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]);
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-concesionaria" name="publicacion-papel-concesionaria">Publicar en Papel Concesionaria</a></h3>
          <div class="msg-publicacion-aviso"><span class="msg-red">Ya utilizaste</span> todos los créditos que tenías disponibles para publicar en este mes. No te quedes sin publicar y pedile a tu receptoría que te habilite más créditos</div>
        </fieldset>
      <?php } else {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID]);
        }
      } ?>
      <!-- Fin Credito Web-papel Concesionaria -->
      
      <!-- Credito Web-papel Inmobiliaria -->
      <?php 
      $credito_papel_disp = 0;
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID])){
        $credito_papel_total = count($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]);
        foreach($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID] as $key => $value){
          if(empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID][$key]['aviso']))
            $credito_papel_disp++;
        }
      }
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]) && 
        isset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]) && $credito_papel_disp>0){
        $credito_papel_inmobiliaria = node_load(AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID);
        $check_papel_inmobiliaria = $form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID];
        $publicacion_papel_inmobiliaria = TRUE;
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-inmobiliaria" name="publicacion-papel-inmobiliaria">Publicar en Papel Inmobiliaria</a></h3>
            <div class="Row clearfix">
              <?php
                if((isset($check_papel_inmobiliaria['#attributes']['checked']) && $check_papel_inmobiliaria['#attributes']['checked'] == 'checked') || in_array($credito_papel_inmobiliaria->nid, $array_pago)){
                  $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                  if($publicacion!=FALSE) {
                    $form['text-papel-inmobiliaria']['#value'] = $publicacion->texto;
                    $primer_dia_semana = date("W", strtotime($publicacion->fecha_publicacion));
                    $anio_pub = date("o", strtotime($publicacion->fecha_publicacion));
                    // 'o' toma el año correspondiende al numero de semana, funciona para la semana 'W', no para 'w'.
                    if(intval(date('W')) >= intval($primer_dia_semana) || $anio_pub < date('o')) {
                      // Si ya pasó la semana, habilitamos para que pueda volver a publicar.
                      unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]['#attributes']['checked']);
                      unset($check_papel_inmobiliaria['#attributes']['checked']);
                    }
                  }
                } else {
                  // Sugerimos el texto.
                  if(empty($form['text-papel-inmobiliaria']['#value'])) // Si viene de la validación el texto ya existe.
                    $form['text-papel-inmobiliaria']['#value'] = $texto_sugerido;
                }
              ?>    
              <!-- Div general mejora papel -->
              <div class="<?php if(isset($check_papel_inmobiliaria['#attributes']['checked']) && $check_papel_inmobiliaria['#attributes']['checked'] == 'checked') print "seleccionado"; if(in_array($credito_papel_inmobiliaria->nid, $array_pago)) print "comprado"; ?> clearfix">
                <!-- Check-box --> 
                <?php print drupal_render($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]); ?>
                <!-- Div del resto de la info mejora papel -->
                <?php if($credito_papel_disp > 0){ ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $credito_papel_disp; ?></div>
                  <?php if($reusable == 1){ ?>
                    <div class="reusable">Reutilizable</div>
                  <?php } ?>
                    <div class="disponibilidad credito check">&nbsp;</div>
                  </div>
                <?php } else { ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio">$<?php print precios_obtener_precio_producto($credito_papel_inmobiliaria->nid, $rubro->tid); ?></div>
                  </div>
                <?php } ?>
                <div id="content-papel-inmobiliaria" class="clearfix">
                  <?php 
                    print drupal_render($form['text-papel-inmobiliaria']); 
                  ?>
                  <div class="CajaPapel">
                    <?php
                      //Obtener días de publicación
                      $fechas = _administracion_avisos_obtener_fechas_papel('Inmobiliaria');
                      //Busco el texto agregado si está seleccionado o en espera de pago
                      if((isset($check_papel_inmobiliaria['#attributes']['checked']) && $check_papel_inmobiliaria['#attributes']['checked'] == 'checked') || in_array($credito_papel_inmobiliaria->nid, $array_pago)){
                        $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                        if($publicacion!=FALSE) {
                          $tiempo_sabado = strtotime($publicacion->fecha_publicacion);
                          $dia_domingo = date('d', $tiempo_sabado-518400);
                          $dia_lunes = date('d', $tiempo_sabado-432000);
                          $dia_martes = date('d', $tiempo_sabado-345600);
                          $dia_miercoles = date('d', $tiempo_sabado-259200);
                          $dia_jueves = date('d', $tiempo_sabado-172800);
                          $dia_viernes = date('d', $tiempo_sabado-86400);
                          $dia_sabado = date('d', $tiempo_sabado);
                          $form['text-papel-inmobiliaria']['#value'] = $publicacion->texto;
                        } else {
                          $dia_domingo = '';
                          $dia_lunes = '';
                          $dia_martes = '';
                          $dia_miercoles = '';
                          $dia_jueves = '';
                          $dia_viernes = '';
                          $dia_sabado = '';
                        }
                      } else {
                        $dia_domingo = date('d', strtotime($fechas['martes'])-172800);
                        $dia_lunes = date('d', strtotime($fechas['martes'])-86400);
                        $dia_martes = date('d', strtotime($fechas['martes']));
                        $dia_miercoles = date('d', strtotime($fechas['martes'])+86400);
                        $dia_jueves = date('d', strtotime($fechas['jueves']));
                        $dia_viernes = date('d', strtotime($fechas['jueves'])+86400);
                        $dia_sabado = date('d', strtotime($fechas['sabado']));
                      }
                    ?>
                    <table class="tabla_dias">
                      <tr>
                        <th>Dom <?php print $dia_domingo; ?></th>
                        <th>Lun <?php print $dia_lunes; ?></th>
                        <th>Mar <?php print $dia_martes; ?></th>
                        <th>Mié <?php print $dia_miercoles; ?></th>
                        <th>Jue <?php print $dia_jueves; ?></th>
                        <th>Vie <?php print $dia_viernes; ?></th>
                        <th>Sáb <?php print $dia_sabado; ?></th>
                      </tr>
                      <tr>
                        <td><?php if(isset($fechas['domingo'])) echo 'X'; ?></td>
                        <td><?php if(isset($fechas['lunes'])) echo 'X'; ?></td>
                        <td>X</td>
                        <td></td>
                        <td>X</td>
                        <td></td>
                        <td>X</td>
                      </tr>
                    </table>
                    <br><strong style="color:Red;">Las fechas pueden variar dependiendo de la acreditación del pago</strong></p>
                  </div>
                  <a class="papel-reco colorbox-inline" href="?width=490&height=485&inline=true#img-papel-reco">Ver recomendaciones</a>
                  <!-- Div del body mejora papel -->
                  <div class="detalle credito check"><?php print $credito_papel_inmobiliaria->body; ?></div>
                  <div style="display: none;"><img id="img-papel-reco" src="/<?php print path_to_theme(); ?>/img/recomendacion_papel.jpg"/></div>
                  <div id="textareaCallBack"></div>
                </div>
                <div class="papel-previsualizar">
                  <input type="button" value="Previsualizar" class="preview">
                  <div class="simulacion-papel"></div>
                </div>
                <?php
                $total_credito = 0;
                if(isset($array_creditos[$credito_papel_inmobiliaria->nid]))
                  $total_credito = count($array_creditos[$credito_papel_inmobiliaria->nid]);
                $disp_credito = 0;
                if($total_credito > 0){
                  foreach($array_creditos[$credito_papel_inmobiliaria->nid] as $nodo){
                    if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                      $disp_credito++;
                  }
                }
                $titulo_oculto = $credito_papel_inmobiliaria->title;
                $id_oculto = $credito_papel_inmobiliaria->nid;
                ?>
                <div class="detalle-oculto">
                  <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                  <div id="precio-<?php print $id_oculto; ?>"><?php print precios_obtener_precio_producto($credito_papel_inmobiliaria->nid, $rubro->tid); ?></div>
                  <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                </div>
              </div>
            </div>
        </fieldset>
      <?php } elseif(in_array('concesionaria terceros', $user->roles)) { 
        $creditos_papel_inmobiliaria = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID);
        if(!empty($creditos_papel_inmobiliaria) && $creditos_papel_inmobiliaria['cantidad'] == 0) {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]);
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-inmobiliaria" name="publicacion-papel-inmobiliaria">Publicar en Papel Inmobiliaria</a></h3>
          <div class="msg-publicacion-aviso"><span class="msg-red">Ya utilizaste</span> todos los créditos que tenías disponibles para publicar en este mes. No te quedes sin publicar y pedile a tu receptoría que te habilite más créditos</div>
        </fieldset>
      <?php } else {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID]);
        }
      } ?>
      <!-- Fin Credito Web-papel Inmobiliaria -->
      
      <!-- Credito Web-papel Comercio -->
      <?php 
      $credito_papel_disp = 0;
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID])){
        $credito_papel_total = count($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]);
        foreach($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID] as $key => $value){
          if(empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID][$key]['aviso']))
            $credito_papel_disp++;
        }
      }
      if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]) && 
        isset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]) && $credito_papel_disp>0){
        $credito_papel_comercio = node_load(AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID);
        $check_papel_comercio = $form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID];
        $publicacion_papel_comercio = TRUE;
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-comercio" name="publicacion-papel-comercio">Publicar en Papel Comercio</a></h3>
            <div class="Row clearfix">
              <?php
                if((isset($check_papel_comercio['#attributes']['checked']) && $check_papel_comercio['#attributes']['checked'] == 'checked') || in_array($credito_papel_comercio->nid, $array_pago)){
                  $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                  if($publicacion!=FALSE) {
                    $form['text-papel-comercio']['#value'] = $publicacion->texto;
                    $primer_dia_semana = date("W", strtotime($publicacion->fecha_publicacion));
                    $anio_pub = date("o", strtotime($publicacion->fecha_publicacion));
                    // 'o' toma el año correspondiende al numero de semana, funciona para la semana 'W', no para 'w'.
                    if(intval(date('W')) >= intval($primer_dia_semana) || $anio_pub < date('o')) {
                      // Si ya pasó la semana, habilitamos para que pueda volver a publicar.
                      unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]['#attributes']['checked']);
                      unset($check_papel_comercio['#attributes']['checked']);
                    }
                  }
                } else {
                  // Sugerimos el texto.
                  if(empty($form['text-papel-comercio']['#value'])) // Si viene de la validación el texto ya existe.
                    $form['text-papel-comercio']['#value'] = $texto_sugerido;
                }
              ?>    
              <!-- Div general mejora papel -->
              <div class="<?php if(isset($check_papel_comercio['#attributes']['checked']) && $check_papel_comercio['#attributes']['checked'] == 'checked') print "seleccionado"; if(in_array($credito_papel_comercio->nid, $array_pago)) print "comprado"; ?> clearfix">
                <!-- Check-box --> 
                <?php print drupal_render($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]); ?>
                <!-- Div del resto de la info mejora papel -->
                <?php if($credito_papel_disp > 0){ ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $credito_papel_disp; ?></div>
                  <?php if($reusable == 1){ ?>
                    <div class="reusable">Reutilizable</div>
                  <?php } ?>
                    <div class="disponibilidad credito check">&nbsp;</div>
                  </div>
                <?php } else { ?>
                  <div class="PrecioDetalle clearfix">
                    <div class="ElPrecio">$<?php print precios_obtener_precio_producto($credito_papel_comercio->nid, $rubro->tid); ?></div>
                  </div>
                <?php } ?>
                <div id="content-papel-comercio" class="clearfix">
                  <?php 
                    print drupal_render($form['text-papel-comercio']); 
                  ?>
                  <div class="CajaPapel">
                    <?php
                      //Obtener días de publicación
                      $fechas = _administracion_avisos_obtener_fechas_papel('Comercio');
                      //Busco el texto agregado si está seleccionado o en espera de pago
                      if((isset($check_papel_comercio['#attributes']['checked']) && $check_papel_comercio['#attributes']['checked'] == 'checked') || in_array($credito_papel_comercio->nid, $array_pago)){
                        $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                        if($publicacion!=FALSE) {
                          $tiempo_sabado = strtotime($publicacion->fecha_publicacion);
                          $dia_domingo = date('d', $tiempo_sabado-518400);
                          $dia_lunes = date('d', $tiempo_sabado-432000);
                          $dia_martes = date('d', $tiempo_sabado-345600);
                          $dia_miercoles = date('d', $tiempo_sabado-259200);
                          $dia_jueves = date('d', $tiempo_sabado-172800);
                          $dia_viernes = date('d', $tiempo_sabado-86400);
                          $dia_sabado = date('d', $tiempo_sabado);
                          $form['text-papel-comercio']['#value'] = $publicacion->texto;
                        } else {
                          $dia_domingo = '';
                          $dia_lunes = '';
                          $dia_martes = '';
                          $dia_miercoles = '';
                          $dia_jueves = '';
                          $dia_viernes = '';
                          $dia_sabado = '';
                        }
                      } else {
                        //Obtener proximo domingo y demas dias. Ahora el domingo NO lo tienen todos los web-papel.
                        if(isset($fechas['domingo'])) {
                          $time_domingo = strtotime($fechas['domingo']);
                        } else {
                          $time_lunes = strtotime($fechas['lunes']);
                          $time_domingo = $time_lunes-86400;
                        }
                        $dia_domingo = date('d', $time_domingo);
                        $dia_lunes = date('d', $time_domingo+86400);
                        $dia_martes = date('d', $time_domingo+172800);
                        $dia_miercoles = date('d', $time_domingo+259200);
                        $dia_jueves = date('d', $time_domingo+345600);
                        $dia_viernes = date('d', $time_domingo+432000);
                        $dia_sabado = date('d', $time_domingo+518400);
                      }
                    ?>
                    <table class="tabla_dias">
                      <tr>
                        <th>Dom <?php print $dia_domingo; ?></th>
                        <th>Lun <?php print $dia_lunes; ?></th>
                        <th>Mar <?php print $dia_martes; ?></th>
                        <th>Mié <?php print $dia_miercoles; ?></th>
                        <th>Jue <?php print $dia_jueves; ?></th>
                        <th>Vie <?php print $dia_viernes; ?></th>
                        <th>Sáb <?php print $dia_sabado; ?></th>
                      </tr>
                      <tr>
                        <td></td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                        <td></td>
                      </tr>
                    </table>
                    <br><strong style="color:Red;">Las fechas pueden variar dependiendo de la acreditación del pago</strong></p>
                  </div>
                  <a class="papel-reco colorbox-inline" href="?width=490&height=485&inline=true#img-papel-reco">Ver recomendaciones</a>
                  <!-- Div del body mejora papel -->
                  <div class="detalle credito check"><?php print $credito_papel_comercio->body; ?></div>
                  <div style="display: none;"><img id="img-papel-reco" src="/<?php print path_to_theme(); ?>/img/recomendacion_papel.jpg"/></div>
                  <div id="textareaCallBack"></div>
                </div>
                <div class="papel-previsualizar">
                  <input type="button" value="Previsualizar" class="preview">
                  <div class="simulacion-papel"></div>
                </div>
                <?php
                $total_credito = 0;
                if(isset($array_creditos[$credito_papel_comercio->nid]))
                  $total_credito = count($array_creditos[$credito_papel_comercio->nid]);
                $disp_credito = 0;
                if($total_credito > 0){
                  foreach($array_creditos[$credito_papel_comercio->nid] as $nodo){
                    if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                      $disp_credito++;
                  }
                }
                $titulo_oculto = $credito_papel_comercio->title;
                $id_oculto = $credito_papel_comercio->nid;
                ?>
                <div class="detalle-oculto">
                  <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                  <div id="precio-<?php print $id_oculto; ?>"><?php print precios_obtener_precio_producto($credito_papel_comercio->nid, $rubro->tid); ?></div>
                  <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                </div>
              </div>
            </div>
        </fieldset>
      <?php } elseif(in_array('concesionaria terceros', $user->roles)) { 
        $creditos_papel_comercio = publicacion_avisos_creditos_adquiridos_usuario_disponibles($user->uid, AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID);
        if(!empty($creditos_papel_comercio) && $creditos_papel_comercio['cantidad'] == 0) {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]);
      ?>
        <fieldset class="credito-papel">
          <h3><a href="#publicacion-papel-comercio" name="publicacion-papel-comercio">Publicar en Papel Comercio</a></h3>
          <div class="msg-publicacion-aviso"><span class="msg-red">Ya utilizaste</span> todos los créditos que tenías disponibles para publicar en este mes. No te quedes sin publicar y pedile a tu receptoría que te habilite más créditos</div>
        </fieldset>
      <?php } else {
          unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID]);
        }
      } ?>
      <!-- Fin Credito Web-papel Comercio -->
      
      <!-- Credito Web-papel -->
      <?php if(isset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_NID]) && !in_array($aviso->type, array('aviso_emprendimiento'))
        && $publicacion_papel_concesionaria==FALSE && $publicacion_papel_inmobiliaria==FALSE && $publicacion_papel_comercio==FALSE){
        $credito_papel = node_load(AUTOSLAVOZ_CREDITO_PAPELWEB_NID);
        $check_papel = $form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_NID];
      ?>
        <fieldset class="credito-papel credito_<?php echo AUTOSLAVOZ_CREDITO_PAPELWEB_NID; ?>">
          <h3><a href="#publicacion-papel" name="publicacion-papel">Publicar en Papel</a></h3>
            <div class="Row clearfix">
              <?php 
                $credito_papel_disp = 0;
                if(!empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_NID])){
                  $credito_papel_total = count($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_NID]);
                  foreach($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_NID] as $key => $value){
                    if(empty($array_creditos[AUTOSLAVOZ_CREDITO_PAPELWEB_NID][$key]['aviso']))
                      $credito_papel_disp++;
                  }
                }
                $form['text-papel']['#value'] = $texto_sugerido;
                $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                if($publicacion!=FALSE) {
                  $form['text-papel']['#value'] = $publicacion->texto;
                  $primer_dia_semana = date("W", strtotime($publicacion->fecha_publicacion));
                  $anio_pub = date("o", strtotime($publicacion->fecha_publicacion));
                  if(intval(date('W')) >= intval($primer_dia_semana) || $anio_pub < date('o')) {
                    // Si ya pasó la semana, habilitamos para que pueda volver a publicar.
                    unset($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_NID]['#attributes']['checked']);
                    unset($check_papel['#attributes']['checked']);
                  }
                }
              ?>    
              <!-- Div general mejora papel -->
              <div class="<?php if(isset($check_papel['#attributes']['checked']) && $check_papel['#attributes']['checked'] == 'checked') print "seleccionado"; if(in_array($credito_papel->nid, $array_pago)) print "comprado"; ?> clearfix">
                <!-- Check-box --> 
                <?php print drupal_render($form['creditos']['credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_NID]); ?>
                
                <!-- Div del resto de la info mejora papel -->
                <?php if($credito_papel_disp > 0){ ?>
                <div class="PrecioDetalle">
                  <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $credito_papel_disp; ?></div>
                  <div class="disponibilidad credito check">&nbsp;</div>
                </div>
                <?php } else { ?>
                <div class="PrecioDetalle">
                  <div class="ElPrecio">$<?php print precios_obtener_precio_producto($credito_papel->nid, $rubro->tid); ?></div>
                </div>
                <?php } ?>
                <div id="content-papel" style="display: none" class="clearfix">
                  <?php 
                    print drupal_render($form['text-papel']); 
                  ?>
                  <div class="CajaPapel">
                    <?php
                      //Busco el texto agregado si está seleccionado o en espera de pago
                      if((isset($check_papel['#attributes']['checked']) && $check_papel['#attributes']['checked'] == 'checked') || in_array($credito_papel->nid, $array_pago)){
                        $compra_papel = compras_get_compra_papel($aviso->nid);                                                                 
                        $fecha_domingo = date('d.m.Y', strtotime($compra_papel['fecha_pub_dom']));
                        $fecha_lunes = date('d.m.Y', strtotime($compra_papel['fecha_pub_lun']));
                        //$form['text-papel']['#value'] = $compra_papel['texto_papel'];
                        $publicacion = clasificados_web_papel_get_ultima_publicacion($aviso->nid);
                        if($publicacion!=FALSE) {
                          $tiempo_lunes = strtotime($publicacion->fecha_publicacion);
                          $dia_domingo = date('d', $tiempo_lunes-86400);
                          $dia_lunes = date('d', $tiempo_lunes);
                          $dia_martes = date('d', $tiempo_lunes+86400);
                          $dia_miercoles = date('d', $tiempo_lunes+172800);
                          $dia_jueves = date('d', $tiempo_lunes+259200);
                          $dia_viernes = date('d', $tiempo_lunes+345600);
                          $dia_sabado = date('d', $tiempo_lunes+432000);
                        } else {
                          $dia_domingo = '';
                          $dia_lunes = '';
                          $dia_martes = '';
                          $dia_miercoles = '';
                          $dia_jueves = '';
                          $dia_viernes = '';
                          $dia_sabado = '';
                        }
                      } else {
                        //Obtener proximo domingo y lunes
                        $fechas = _administracion_avisos_obtener_fechas_papel();
                        $fecha_domingo = date('d.m.Y', strtotime($fechas['domingo']));
                        $fecha_lunes = date('d.m.Y', strtotime($fechas['lunes']));
                        $dia_domingo = date('d', strtotime($fechas['domingo']));
                        $dia_lunes = date('d', strtotime($fechas['lunes']));
                        $dia_martes = date('d', strtotime($fechas['lunes'])+86400);
                        $dia_miercoles = date('d', strtotime($fechas['lunes'])+172800);
                        $dia_jueves = date('d', strtotime($fechas['lunes'])+259200);
                        $dia_viernes = date('d', strtotime($fechas['lunes'])+345600);
                        $dia_sabado = date('d', strtotime($fechas['lunes'])+432000);
                      }
                    ?>
                    <p class="p_txt"><strong>Fechas de publicación:</strong><br>
                    <table class="tabla_dias">
                      <tr>
                        <th><?php echo ($dia_domingo=='31' && date('Y')=='2016')?'Sáb':'Dom'; ?> <?php print $dia_domingo; ?></th>
                        <th>Lun <?php print $dia_lunes; ?></th>
                        <th>Mar <?php print $dia_martes; ?></th>
                        <th>Mié <?php print $dia_miercoles; ?></th>
                        <th>Jue <?php print $dia_jueves; ?></th>
                        <th>Vie <?php print $dia_viernes; ?></th>
                        <th>Sáb <?php print $dia_sabado; ?></th>
                      </tr>
                      <tr>
                        <td>X</td>
                        <td>X</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>
                    <br><strong style="color:Red;">Las fechas pueden variar dependiendo de la acreditación del pago</strong></p>
                  </div>
                  <a class="papel-reco colorbox-inline" href="?width=490&height=485&inline=true#img-papel-reco">Ver recomendaciones</a>
                  <!-- Div del body mejora papel -->
                  <div class="detalle credito check"><?php print $credito_papel->body; ?></div>
                  <div style="display: none;"><img id="img-papel-reco" src="/<?php print path_to_theme(); ?>/img/recomendacion_papel.jpg"/></div>
                  <div id="textareaCallBack"></div>
                </div>
                <div class="papel-previsualizar">
                  <input type="button" value="Previsualizar" class="preview">
                  <div class="simulacion-papel"></div>
                </div>
                <?php
                $total_credito = 0;
                if(isset($array_creditos[$credito_papel->nid]))
                  $total_credito = count($array_creditos[$credito_papel->nid]);
                $disp_credito = 0;
                if($total_credito > 0){
                  foreach($array_creditos[$credito_papel->nid] as $nodo){
                    if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                      $disp_credito++;
                  }
                }
                $titulo_oculto = $credito_papel->title;
                $id_oculto = $credito_papel->nid;
                ?>
                <div class="detalle-oculto">
                  <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                  <div id="precio-<?php print $id_oculto; ?>"><?php print precios_obtener_precio_producto($credito_papel->nid, $rubro->tid); ?></div>
                  <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                </div>
              </div>
            </div>
        </fieldset>
      <?php } ?>
      <!-- Fin Credito Web-papel -->
      
      <fieldset class="mejoras">
        <h3 class="TitleMejoras">Mejoras<span class="FlechaMejoras asc"></span></h3>
        <div class="Row clearfix ListaMejoras">
          <?php 
            foreach($form['creditos'] as $key => $valor){
              //No muestro el credito web a papel
              if (count($valor) > 1 && $key != '#post' && !in_array($key, array('credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_NID, 'credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_CONCESIONARIA_NID, 'credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_INMOBILIARIA_NID , 'credito_'.AUTOSLAVOZ_CREDITO_PAPELWEB_COMERCIO_NID))){
                //Datos del credito 
                ?> 
                <div class="ContentMejoras <?php print $key; ?>"> 
                <?php
                //Busco si el credito tiene un selec asociado
                if(!empty($form['creditos'][$key]['select_'.$key])){
                                        
                  $credito_nid = explode("_", $key);
                  $nombre = $credito_nid[1];
                  $array_selects = array();
                  $primer_option = 0;
                  $total_credito = 0;
                  $comprado = 0;
                  foreach($form['creditos'][$key]['select_'.$key]['#options'] as $mejora_id => $mejora_value){
                    if($primer_option == 0) {
                      $credito = node_load($mejora_id);
                      $credito_body = $credito->body;
                      $credito_precio = precios_obtener_precio_producto($mejora_id, $rubro->tid);
                      $primer_option = 1;
                    }
                    $cantidad_creditos = isset($array_creditos[$mejora_id]) ? count($array_creditos[$mejora_id]) : 0;
                    $total_credito = $total_credito + $cantidad_creditos;
                    if(in_array($mejora_id, $array_pago))
                      $comprado = 1;
                    $array_selects[] = $mejora_id;
                  }
                  $disp_credito = 0;
                  if($total_credito > 0){
                    foreach($array_selects as $value){
                      if(!empty($array_creditos[$value])){
                        foreach($array_creditos[$value] as $nodo){
                          if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                            $disp_credito++;
                        }
                      }
                    }
                  }
                  $titulo_oculto = $valor['select_'.$key]["#title"];
                  $id_oculto = strtolower($valor['select_'.$key]["#title"]);
                  ?>
                  <!-- Div general de la mejora --> 
                  <div class="ContentMejorasFirst <?php if(isset($form['creditos'][$key]['check_'.$key]['#attributes']['checked']) && $form['creditos'][$key]['check_'.$key]['#attributes']['checked'] == 'checked') print "seleccionado"; if($comprado == 1) print "comprado"; ?>">
                    <!-- Check-box --> 
                    <?php print drupal_render($form['creditos'][$key]['check_'.$key]); ?>
                    <!-- Select --> 
                    <?php $form['creditos'][$key]['select_'.$key]['#title'] = 'Texto';
                    print drupal_render($form['creditos'][$key]['select_'.$key]); ?>
                    <!-- Div del body de la mejora --> 
                    <div class="detalleFirst credito check"><?php print $credito_body; ?></div> 
                    <!-- Div del resto de la info de la mejora -->
                    <div class= "PrecioDetalle">
                      <?php if(isset($form['creditos'][$key]['check_'.$key]['#attributes']['checked']) && $form['creditos'][$key]['check_'.$key]['#attributes']['checked'] == 'checked'){ ?>
                        <div class="ElPrecio">Aplicado</div>
                      <?php } elseif($comprado == 1){ ?>
                        <ul class="alerta-compra">Pago no procesado. <br /><a href="/estado_cuenta">Confirmalo o cancelalo aquí</a>.
                        </ul>
                      <?php } elseif($disp_credito > 0){ ?>
                        <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $disp_credito; ?> de <?php print $total_credito; ?></div>
                      <?php } else { ?>
                        <div class="ElPrecio">$<?php print $credito_precio; ?></div>
                      <?php } ?>
                      <div class="detalle-oculto">
                        <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                        <div id="precio-<?php print $id_oculto; ?>"><?php print $credito_precio; ?></div>
                        <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                      </div>              
                    </div>
                    <?php
                      //Datos del ejemplo para mejoras con select
                      $ejemplo_mejora_select_id = '';
                      $ejemplo_mejora_select_img = '';
                      switch($key){
                        case "credito_banderola":
                          $ejemplo_mejora_select_id = 'img-banderola';
                          $ejemplo_mejora_select_img = 'banderola.jpg';
                          break;
                      }
                    ?>
                    <?php if($ejemplo_mejora_select_id != '') { ?>
                    <!-- <div class="VerEjemplo">
                      <a rel="nofollow" class="colorbox-inline" href="?width=665&height=595&inline=true#<?php print $ejemplo_mejora_select_id; ?>">Ver ejemplo</a>
                      <div style="display: none;"><img id="<?php print $ejemplo_mejora_select_id; ?>" src="/<?php print path_to_theme(); ?>/img/<?php print $ejemplo_mejora_select_img; ?>"/></div>
                    </div> -->
                    <?php } ?>
                  </div>
                <?php 
                //El credito no tiene select asociado
                } else {
                  $credito_nid = explode("_", $key);
                  $nid = $credito_nid[1];
                  $total_credito = isset($array_creditos[$nid]) ? count($array_creditos[$nid]) : 0;
                  $disp_credito = 0;
                  if($total_credito > 0){
                    foreach($array_creditos[$nid] as $nodo){
                      if($nodo["aviso"] == "" || $nodo["aviso"] == 0)
                        $disp_credito++;
                    }
                  }
                  if($nid==AUTOSLAVOZ_CREDITO_WHATSAPP_NID && $disp_credito==0) {
                    $disp_credito = 1;
                    $total_credito = 1;
                  }
                  $credito = node_load($nid);
                  $titulo_oculto = $credito->title;
                  $id_oculto = $nid;
                  ?>
                  <!-- Div general de la mejora --> 
                  <div class="<?php if(isset($form['creditos'][$valor['#name']]['#attributes']['checked']) && $form['creditos'][$valor['#name']]['#attributes']['checked'] == 'checked') print "seleccionado"; if(in_array($nid, $array_pago)) print "comprado"; ?>">
                    <!-- Check-box --> 
                    <?php print drupal_render($form['creditos'][$key]); ?>
                    <!-- Div del body de la mejora --> 
                    <div class="detalle credito check"><?php print $credito->body; ?></div>
                    <!-- Div del resto de la info de la mejora -->
                    <div class="PrecioDetalle">
                       <?php if(isset($form['creditos'][$valor['#name']]['#attributes']['checked']) && $form['creditos'][$valor['#name']]['#attributes']['checked'] == 'checked'){ ?>
                        <div class="ElPrecio aplicado">Aplicado</div>
                      <?php } elseif(in_array($nid, $array_pago)){ ?>
                        <ul class="alerta-compra">Pago no procesado. <br /><a href="/estado_cuenta">Confirmalo o cancelalo aquí</a>.
                        </ul>
                      <?php } elseif($disp_credito > 0){ ?>
                       <div class="ElPrecio disponibilidad credito check">Disponible: <?php print $disp_credito; ?> de <?php print $total_credito; ?></div>
                       <?php } else { ?>
                       <div class="ElPrecio">$<?php print precios_obtener_precio_producto($nid, $rubro->tid); ?></div>
                      <?php } ?>
                       <div class="detalle-oculto">
                       <div id="titulo-<?php print $id_oculto; ?>"><?php print $titulo_oculto; ?></div>
                         <div id="precio-<?php print $id_oculto; ?>"><?php print precios_obtener_precio_producto($nid, $rubro->tid); ?></div>
                         <div id="disponibilidad-<?php print $id_oculto; ?>"><?php print $disp_credito; ?></div>
                         </div>
                      </div>
                      <?php
                        //Datos del ejemplo para mejoras con select
                        $ejemplo_mejora_id = '';
                        $ejemplo_mejora_img = '';
                        switch($key){
                          case "credito_1011":
                            $ejemplo_mejora_id = 'img-recuadro';
                            $ejemplo_mejora_img = 'recuadro.jpg';
                            break;
                        }
                      ?>
                      <?php if($ejemplo_mejora_id != '') { ?>
                      <div class="VerEjemplo">
                        <a class="colorbox-inline" href="?width=680&height=640&inline=true#<?php print $ejemplo_mejora_id; ?>">Ver ejemplo</a>
                        <div style="display: none;"><img id="<?php print $ejemplo_mejora_id; ?>" src="/<?php print path_to_theme(); ?>/img/<?php print $ejemplo_mejora_img; ?>"/></div>
                      </div>
                      <?php } ?>
                  </div>
                <?php } ?>
              </div>
            <?php }
            } ?>
        </div>
      </fieldset>
      
      <div style="display: none;">
        <div id="modal-confirmacion-publicacion-papel">
          <h3>Confirmación de aviso papel</h3>
          Verifique que el texto para la publicación en papel sea el correcto.<br>
          Revise que el <strong>número de teléfono</strong> esté completo.<br> Simulación del Texto:
          <div class="botones">
            <div id="confirmacion-texto"></div>
            <input id="confirmacion-publicar" type="button" value="Publicar" class="form-submit" />
            <input id="confirmacion-cancelar" type="button" value="Cancelar" class="form-submit" />
          </div>
        </div>
      </div>
            
      <div style="display:none;"><?php print drupal_render($form['creditos']); ?></div>
      
      
      <?php if(isset($form['sitios_externos'])) { ?> 
      <fieldset class="sitios-externos">
        <h3>Publicar en otros sitios</h3>
        
        <?php
        /*if(!isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS])) {
          // Solo dejamos Trovit para el resto de los usuarios
          foreach($form['sitios_externos'] as $ksitio_externo => $sitio_externo) {
            if(strpos($ksitio_externo, '#') === FALSE && $ksitio_externo != 'trovit')
              unset($form['sitios_externos'][$ksitio_externo]);
          }
        }*/
        if(isset($form['sitios_externos']) && count($form['sitios_externos'])>0){   
          unset($form['sitios_externos']['#title']); ?>
          <div class="contentSitiosExternos">
            <h4 class="titleOtrosSitios">Otros sitios</h4>
            <div class="Row clearfix">
              <div class="ContentEspacio OtrosSitios clearfix">
                <?php print drupal_render($form['sitios_externos']); ?>
                <?php //if(clasificados_rubros_node_pertenece_al_grupo_autos($aviso)): ?>
                <div class="info_publicacion">Nota: Los avisos se publicarán al día siguiente en los sitios que hayas seleccionado.</div>
                <!-- <div class="info"><span class="beta">BETA</span><strong>	Compartí tus avisos con otros sitios!</strong>
                  ClasificadosLaVoz.com.ar pone a tu disposición una nueva herramienta para hacer tu vida más fácil!
                  Ahora podrás replicar sencillamente tus avisos en Olx, Mitula y Trovit con un solo tilde.
                  Esta funcionalidad aún está en versión Beta y sólo algunos usuarios seleccionados pueden utilizarla,
                  iremos agregando más sitios para que puedas compartir tus vehículos y llegar a la mayor cantidad de audiencia posible.
                  <strong>Aprovechá para probarla hasta el 31 de enero!!</strong>'
                  Versión Beta sólo disponible en Rubro Autos.
                </div> -->
                <?php //endif; ?>
              </div>
            </div>
          </div>
        <?php } ?>
        
        <?php if(isset($form['mercadolibre']) && count($form['mercadolibre'])>0){   
          unset($form['mercadolibre']['#title']); ?>
          <div class="contentSitiosExternos">
            <h4 class="titleOtrosSitios">Publicar en Mercado Libre</h4>
            <div class="Row clearfix">
              <div class="ContentEspacio OtrosSitios clearfix">
                <?php print drupal_render($form['mercadolibre']); ?>
              </div>
            </div>
            
            <div class="vinculacion">
              
              <a href="#" id="addAviso">Crear Aviso</a>
              <br>
              <br>
              <a href="#" id="setPackUsuario">Crear Pack</a>
              <br>
              <br>
              <div class="estado">Estás conectado a Mercado Libre</div>
              <div class="aviso"> 
                <!-- <a class="itemsImage" href="http://auto.mercadolibre.com.ar/MLA-682789526-item-de-testeo-clasificados-_JM">
                  <img src="https://mla-s2-p.mlstatic.com/803817-MLA25980840605_092017-I.jpg" width="45" height="45">
                </a>
                <div class="">
                  <h3 class="itemsPrimaryInfo "><a class="itemsTitle" href="http://auto.mercadolibre.com.ar/MLA-682789526-item-de-testeo-clasificados-_JM">Item De Testeo Clasificados</a></h3>
                  <p class="itemsKind">
                    <span>Publicación <span class="listingType">Plata</span></span>
                    <span class="itemId">#682789526</span><br>
                  </p>
                  <div class="itemsSecondaryInfo">
                    <span class="price">$ 122.000</span>
                    <li id="visitsAndSalesInfo-MLA682789526"><li>19 visitas</li></li>
                  </div>
                  <div class="itemsActions">Finaliza en 59 días.</div>
                </div> -->
              </div>
              <div>
                <div class="contentSelectCategoria oculto">
                  <label>Selecciona la categoría donde publicar tu aviso</label>
                  <select name="categoria_ml" id="selectCategoriaMl"></select>
                </div>
                <div class="contentSelectMarca oculto">
                  <label>Selecciona la marca donde publicar tu aviso</label>
                  <select name="marca_ml" id="selectMarcaMl"></select>
                </div>
                <div class="contentSelectModelo oculto">  
                  <label>Selecciona el modelo donde publicar tu aviso</label>
                  <select name="modelo_ml" id="selectModeloMl"></select>
                </div>
              </div>
            </div>            
          </div>
        <?php } ?>
        
      <?php if(isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS])) { ?>  
        
        <?php if(isset($form['destaque_sitio'])){ ?>        
          <div class="contentSitioWeb">
            <h4 class="titleSitioWeb">Mi página web</h4>
            <div class="Row clearfix destaque-mi-sitio">
              <?php print drupal_render($form['destaque_sitio']); ?>
            </div>
            <div class="detalle credito check destaque-mi-sitio">Todavía no tenés tu sitio web en Clasificados?<br /><a href="/administrar/sitio/principal" target="_blank">Entrá acá y armá tu sitio web conectado a Clasificados en 5 minutos</a></div>
          </div>
        <?php } ?>
        
      <?php } ?>  
      </fieldset>
      <?php } ?>
      
      
      <fieldset class="detalle-seleccion" style="display:none;">
        <h3>Detalle de Compra</h3>
        <div class="Row clearfix">
          <div class="contentLeft">
            <h4>Espacio</h4>
            <div class="lista-detalle-espacios">
              <!-- <div class="lista-seleccion">Gratuito <span class="detalle-precio">Sin costo</span></div> -->
            </div>
          </div>
          <div class="contentRight">
            <h4>Creditos</h4>
            <div class="lista-detalle-creditos">
              <!-- <div class="detalle">Foto en el listado $<span class="detalle-precio">0.00</span></div> -->
            </div>
          </div>
        </div>
        <div class="total">Total $<span class="total-precio">0.00</span></div>
      </fieldset>
      
      <div class="Row clearfix">
        <div class="Ingresa">
          <div class="Boton">
            <div class="Cv Tl"></div>
            <div class="Cv Tr"></div>
            <div class="Cv Bl"></div>
            <div class="Cv Br"></div> 
            <?php 
              print drupal_render($form['submit-publicacion']); 
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-sin-mostrar" style="display:none;">
  <?php
    print drupal_render($form);
  ?>
  </div>
</div>