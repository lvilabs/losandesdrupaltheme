<?php
  $tree_col_der = $block->rubros_columna_derecha;
  $tree_col_izq = $block->rubros_columna_izquierda;
?>
<ul class="col">
<?php  foreach ($tree_col_izq as $i => $term): ?>
<?php    if($term->depth == 0): ?>
    <ul class="navigation">
<?php      if($term->is_empty): ?>
      <li><h2><strong><?php print $term->name; ?></strong></h2></li>
<?php      else: ?>
      <li><h2><a href="<?php print $term->search_url; ?>" target="<?php print $term->target; ?>"><strong><?php print $term->name; ?></strong></a></h2></li>
<?php      endif; ?>
<?php    else: ?>
      <li><a href="<?php print $term->search_url; ?>" target="<?php print $term->target; ?>"><?php print $term->name; ?></a></li>
<?php    endif; ?>
<?php    if(isset($term->edicion_papel_url)): ?>
      <li class="link-papel"><a href="<?php print $term->edicion_papel_url; ?>" target="_blank">Avisos Edición papel</a></li>
<?php    endif; ?>
<?php    if($term->is_last): ?>
    </ul>
<?php    endif; ?>
<?php  endforeach; ?>
</ul>

<ul class="col">
<?php  foreach ($tree_col_der as $i => $term): ?>
<?php    if($term->depth == 0): ?>
    <ul class="navigation">
<?php      if($term->is_empty): ?>
      <li><h2><strong><?php print $term->name; ?></strong></h2></li>
<?php      else: ?>
      <li><h2><a href="<?php print $term->search_url; ?>" target="<?php print $term->target; ?>"><strong><?php print $term->name; ?></strong></a></h2></li>
<?php      endif; ?>
<?php    else: ?>
      <li><a href="<?php print $term->search_url; ?>" target="<?php print $term->target; ?>"><?php print $term->name; ?></a></li>
<?php    endif; ?>
<?php    if(isset($term->edicion_papel_url)): ?>
      <li class="link-papel"><a href="<?php print $term->edicion_papel_url; ?>" target="_blank">Avisos Edición papel</a></li>
<?php    endif; ?>
<?php    if($term->is_last): ?>
    </ul>
<?php    endif; ?>
<?php  endforeach; ?>
</ul>
