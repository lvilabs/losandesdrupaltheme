<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> content principal">
	<div class="TitImg"></div>
	<div class="accessible_news_slider">
			<div class="TitOferta">
				<h2><strong>Ofertas del día</strong></h2>
			</div>
			<!--<div class="Botones">
				<p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
			</div>-->
			<div class="lista">
				<ul class="clearfix bloque_avisos avisos_navidad">
<?php for($i=0; $i<count($content); $i++) { ?>
					<li>
					<div class="CajaAviso ">
            <!-- <span class="Carrito"></span> -->
						<div class="imgAviso">
              <a href="<?php print $content[$i]['url']; ?>?cx_level=promocion_home" title="<?php print $content[$i]['titulo']; ?>">
                <img src="http://staticcl.lavozdelinterior.com.ar<?php print $content[$i]['foto']; ?>" alt="Imagen del aviso" title="<?php print $content[$i]['titulo']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211">
              </a>
						</div>
						<div class="AvisoDescripcion">
							<h4><a href="<?php print $content[$i]['url']; ?>?cx_level=promocion_home" title="<?php print $content[$i]['titulo']; ?>">
                <?php print $content[$i]['titulo']; ?>
              </a></h4>
							<p>
							</p>
							<span class="precio"><?php print $content[$i]['field_aviso_moneda_value'].$content[$i]['field_aviso_precio_value']; ?></span>
<?php if (isset($content[$i]['precio_anterior']) && $content[$i]['precio_anterior']>0): ?>
              <p>Antes</p> <span class="precioOferta">$<?php print $content[$i]['precio_anterior']; ?> </span>
<?php endif; ?>
						</div>
					</div>
					</li>
<?php
      }
?>
				</ul>
			</div>
	</div>
</div>