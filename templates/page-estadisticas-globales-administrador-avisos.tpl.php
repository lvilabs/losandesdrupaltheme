<?php
$color_grafico2 = array(0 => '#F7464A', 1 => '#46BFBD', 2 => '#FDB45C', 3 => '#949FB1', 4 => '#4D5360', 5 => '#86B2DB', 6 => '#F7D443', 7 => '#9392C8', 8 => '#2869B5', 9 => '#B070AD');
$highlight_grafico2 = array(0 => '#FF5A5E', 1 => '#5AD3D1', 2 => '#FFC870', 3 => '#A8B3C5', 4 => '#616774', 5 => '#96C3EA', 6 => '#FFE16B', 7 => '#A3A2DB', 8 => '#2E7AD1', 9 => '#C48DC1');
?>
<script>
  var pieData = [
    <?php $i = 0; foreach($form['globales']['cantidad_tipologia']['#value'] as $tipo) { 
      $legendPie[] = array('label' => $tipo['tipologia'],
                            'color' => $color_grafico2[$i],
                            'cantidad' => $tipo['cantidad']
                          );
    ?>
    {
      value: <?php print $tipo['cantidad']; ?>,
      color:"<?php print $color_grafico2[$i]; ?>",
      highlight: "<?php print $highlight_grafico2[$i]; ?>",
      label: "<?php print $tipo['tipologia']; ?>"
    },
    <?php $i++; } ?>
  ];
  window.onload = function(){
		<?php if(!empty($form['globales']['cantidad_tipologia']['#value'])) { ?>
      var ctx2 = document.getElementById("canvas2").getContext("2d");
      window.myPie = new Chart(ctx2).Pie(pieData);
    <?php } ?>
	}
</script>

<div class="solapa-estadisticas">
  <a href="/administrar/principal">Estadísticas de Mi Cuenta</a>
  <a class="int" href="/administrar/estadisticas_globales">Estadísticas Globales</a>
</div>

<input type="hidden" id="tipoReporte" name="reporte" value="" />
<div class="Columnas MiCuentaAdmin clearfix">
  <div class="CD">
<?php if(!empty($form['globales']['mas_vistas_tipologia']['#value'])) { ?>
<!--Modulo-->
    <div class="panel-block conBorde masVistas">
      
      <h2 class="content-cantidad-consultas">Tipologias más Vistas</h2>
      
      <div id="filtroDiasMasVisto" class="filtros_dias">
        <a href="#" data-dias="1">Ayer</a>
        <a href="#" data-dias="7">7 días</a>
        <a href="#" data-dias="15">15 días</a>
        <a href="#" data-dias="30" class="int">30 días</a>
      </div>
      <input type="hidden" id="hiddenDiasMasVisto" name="diasMasVisto" value="30" />
      
      <div class="recomendacion globales open">
        <strong>Detalle de esta estadística</strong>
        <p>Conocé qué es lo más buscado en nuestro sitio para mejorar tus publicaciones.</p>
      </div>
      
      <div class="filtros_selects">
        <?php if(!empty($form['globales']['select_localidades']['#value'])) { ?>
          <dl id="selectLocalidadMasVisto" class="dropdown"> 
            <dt>
              <a id="multiLocalidadMasVisto" data-select="LocalidadMasVisto" href="#">
                <span class="hida">Localidades de Mendoza</span>
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelect">
                <ul id="LocalidadMasVisto">
                  <li class="quitarAll" data-content="multiLocalidadMasVisto"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_localidades']['#value'] as $localidad) { ?>
                    <li><label for="<?php print strtolower(str_replace(' ','-', $localidad)); ?>"><input type="checkbox" data-content="multiLocalidadMasVisto" name="localidadMasVisto_<?php print strtolower(str_replace(' ','-', $localidad)); ?>" data-name="<?php print $localidad; ?>" id="<?php print strtolower(str_replace(' ','-', $localidad)); ?>" value="<?php print $localidad; ?>" /><?php print $localidad; ?></label>
                    
                      <?php if($localidad == 'Córdoba' && !empty($form['globales']['select_barrios']['#value'])) { ?>
                        <?php foreach($form['globales']['select_barrios']['#value'] as $barrio) { ?>
                          <li class="barriosCordoba"><label for="<?php print strtolower(str_replace(' ','-', $barrio)); ?>"><input type="checkbox" data-content="multiLocalidadMasVisto" name="barrioMasVisto_<?php print strtolower(str_replace(' ','-', $barrio)); ?>" data-name="<?php print $barrio; ?>" id="<?php print strtolower(str_replace(' ','-', $barrio)); ?>" value="<?php print $barrio; ?>" /><?php print $barrio; ?></label></li>
                        <?php } ?>
                      <?php } ?>
                      
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_tipologias']['#value'])) { ?>
          <dl id="selectTipologiaMasVisto" class="dropdown"> 
            <dt>
              <a id="multiTipologiaMasVisto" data-select="TipologiaMasVisto" href="#">
                <span class="hida">Tipologías</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelect">
                <ul id="TipologiaMasVisto">
                  <li class="quitarAll" data-content="multiTipologiaMasVisto"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_tipologias']['#value'] as $tipologia) { ?>
                    <li><label for="<?php print $tipologia['id']; ?>"><input type="checkbox" data-content="multiTipologiaMasVisto" name="tipologiaMasVisto_<?php print $tipologia['id']; ?>" data-name="<?php print $tipologia['tipologia']; ?>" id="<?php print $tipologia['id']; ?>" value="<?php print $tipologia['id']; ?>" /><?php print $tipologia['tipologia']; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_operaciones']['#value'])) { ?>
          <dl id="selectOperacionMasVisto" class="dropdown"> 
            <dt>
              <a id="multiOperacionMasVisto" data-select="OperacionMasVisto" href="#">
                <span class="hida">Operaciones</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelect">
                <ul id="OperacionMasVisto">
                  <li class="quitarAll" data-content="multiOperacionMasVisto"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_operaciones']['#value'] as $operacion) { ?>
                    <li><label for="operacion-<?php print $operacion['id']; ?>"><input type="checkbox" data-content="multiOperacionMasVisto" name="operacionMasVisto_<?php print $operacion['id']; ?>" data-name="<?php print $operacion['operacion']; ?>" id="operacion-<?php print $operacion['id']; ?>" value="<?php print $operacion['id']; ?>" /><?php print $operacion['operacion']; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <a class="linkFiltro" id="filtroMasVisto" href="#"><i class="fa fa-search"></i></a>
      </div>
      
      <div class="consultas-sin-leer">
        <table id="listadoMasVistos" class="consultasResumen" width="100%">
          <tr>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          <?php $i = 1; $position_class = ''; foreach($form['globales']['mas_vistas_tipologia']['#value'] as $mas_vistas) { ?>
            <?php if($i == 1) {
                $position_class = 'first';
              } elseif($i == 2) {
                $position_class = 'second';
              } else {
                $position_class = '';
              }
            ?>
            <tr class="<?php print $position_class; ?>">
              <td class="contacto"><?php print $mas_vistas['posicion']; ?></td>
              <td class="consulta"><?php print $mas_vistas['tipologia']; ?></td>
              <td class="consulta"><?php print number_format($mas_vistas['cantidad'], 0, '', '.'); ?></td>
            </tr>
          <?php $i++; } ?>
        </table>
        <div class="globalLoading"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
      </div>
      
      <div class="acciones_footer">
        <div class="select_cantidad">
          <select id="filtroCantidadMasVisto">
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
          <input type="hidden" id="hiddenCantidadMasVisto" name="cantidadMasVisto" value="3" />
        </div>
        <div class="link_exportacion"><a id="exportarMasVistos" href="#">Exportar a Excel</a></div>
      </div>
      
    </div>
<!--fin Modulo-->
<?php } ?>
<?php if(!empty($form['globales']['mas_destacados']['#value'])) { ?>
<!--Modulo-->
    <div class="panel-block conBorde masVistas">
      
      <h2 class="content-cantidad-consultas">Tipologias mejor Posicionadas</h2>
      
      <div class="recomendacion globales open">
        <strong>Detalle de esta estadística</strong>
        <p>Aplicá de manera eficiente tus destaques consultando este reporte y hacé que tu aviso esté vigente en las búsquedas. ¡Recordá la importancia de la republicación de tus propiedades!</p>
      </div>
      
      <div class="filtros_selects">
        <?php if(!empty($form['globales']['select_localidades']['#value'])) { ?>
          <dl id="selectLocalidadMasDestacada" class="dropdown"> 
            <dt>
              <a id="multiLocalidadMasDestacada" data-select="LocalidadMasDestacada" href="#">
                <span class="hida">Localidades de Mendoza</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelectDestacada">
                <ul id="LocalidadMasDestacada">
                  <li class="quitarAll" data-content="multiLocalidadMasDestacada"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_localidades']['#value'] as $localidad) { ?>
                    <li><label for="destacada-<?php print strtolower(str_replace(' ','-', $localidad)); ?>"><input type="checkbox" data-content="multiLocalidadMasDestacada" name="localidadMasDestacada_<?php print strtolower(str_replace(' ','-', $localidad)); ?>" data-name="<?php print $localidad; ?>" id="destacada-<?php print strtolower(str_replace(' ','-', $localidad)); ?>" value="<?php print $localidad; ?>" /><?php print $localidad; ?></label>
                    
                      <?php if($localidad == 'Mendoza' && !empty($form['globales']['select_barrios']['#value'])) { ?>
                        <?php foreach($form['globales']['select_barrios']['#value'] as $barrio) { ?>
                          <li class="barriosCordoba"><label for="destacada-<?php print strtolower(str_replace(' ','-', $barrio)); ?>"><input type="checkbox" data-content="multiLocalidadMasDestacada" name="barrioMasDestacada_<?php print strtolower(str_replace(' ','-', $barrio)); ?>" data-name="<?php print $barrio; ?>" id="destacada-<?php print strtolower(str_replace(' ','-', $barrio)); ?>" value="<?php print $barrio; ?>" /><?php print $barrio; ?></label></li>
                        <?php } ?>
                      <?php } ?>
                      
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_tipologias']['#value'])) { ?>
          <dl id="selectTipologiaMasDestacada" class="dropdown"> 
            <dt>
              <a id="multiTipologiaMasDestacada" data-select="TipologiaMasDestacada" href="#">
                <span class="hida">Tipologías</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelectDestacada">
                <ul id="TipologiaMasDestacada">
                  <li class="quitarAll" data-content="multiTipologiaMasDestacada"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_tipologias']['#value'] as $tipologia) { ?>
                    <li><label for="destacada-<?php print $tipologia['id']; ?>"><input type="checkbox" data-content="multiTipologiaMasDestacada" name="tipologiaMasDestacada_<?php print $tipologia['id']; ?>" data-name="<?php print $tipologia['tipologia']; ?>" id="destacada-<?php print $tipologia['id']; ?>" value="<?php print $tipologia['id']; ?>" /><?php print $tipologia['tipologia']; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <?php if(!empty($form['globales']['select_operaciones']['#value'])) { ?>
          <dl id="selectOperacionMasDestacada" class="dropdown"> 
            <dt>
              <a id="multiOperacionMasDestacada" data-select="OperacionMasDestacada" href="#">
                <span class="hida">Operaciones</span>    
                <p class="multiSel"></p>  
              </a>
            </dt>
            <dd>
              <div class="mutliSelectDestacada">
                <ul id="OperacionMasDestacada">
                  <li class="quitarAll" data-content="multiOperacionMasDestacada"><label>Quitar Selecciones</label></li>
                  <?php foreach($form['globales']['select_operaciones']['#value'] as $operacion) { ?>
                    <li><label for="operacionDestacada-<?php print $operacion['id']; ?>"><input type="checkbox" data-content="multiOperacionMasDestacada" name="operacionMasDestacada_<?php print $operacion['id']; ?>" data-name="<?php print $operacion['operacion']; ?>" id="operacionDestacada-<?php print $operacion['id']; ?>" value="<?php print $operacion['id']; ?>" /><?php print $operacion['operacion']; ?></label></li>
                  <?php } ?>
                </ul>
              </div>
            </dd>
          </dl>
        <?php } ?>
        <a class="linkFiltro" id="filtroMasDestacada" href="#"><i class="fa fa-search"></i></a>
      </div>
      
      <div class="consultas-sin-leer">
        <table id="listadoMasDestacada" class="consultasResumen" width="100%">
          <tr>
            <th></th>
            <th></th>
            <th>SPremium</th>
            <th>Premium</th>
            <th>Estandar</th>
            <th>Basico</th>
          </tr>
          <?php $i = 1; $position_class = ''; foreach($form['globales']['mas_destacados']['#value'] as $mas_vistas) { ?>
            <?php if($i == 1) {
                $position_class = 'first';
              } elseif($i == 2) {
                $position_class = 'second';
              } else {
                $position_class = '';
              }
            ?>
            <tr class="<?php print $position_class; ?>">
              <td class="contacto"><?php print $mas_vistas['posicion']; ?></td>
              <td class="consulta"><?php print $mas_vistas['tipologia']; ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['superpremium'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['premium'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['estandar'], 0, '', '.'); ?></td>
              <td class="acciones"><?php print number_format($mas_vistas['basico'], 0, '', '.'); ?></td>
            </tr>
          <?php $i++; } ?>
        </table>
        <div class="globalLoadingDestacada"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
      </div>
      
      <div class="acciones_footer">
        <div class="select_cantidad">
          <select id="filtroCantidadMasDestacada">
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
          <input type="hidden" id="hiddenCantidadMasDestacada" name="cantidadMasDestacada" value="3" />
        </div>
        <div class="link_exportacion"><a id="exportarMasDestacada" href="#">Exportar a Excel</a></div>
      </div>
      
    </div>
<!--fin Modulo-->
<?php } ?>
  </div>
  
  <?php if(!empty($form['globales']['mas_vistas']['#value'])) { ?>
    <div class="CR">
      <div class="panel-block conBorde visitasTotales">
        <div class="textoTotal">Total vistas avisos de <span class="visitasTiempo">Ayer</span></div>
        <div class="contadorTotal"><?php print number_format($form['globales']['mas_vistas']['#value'][0]['cantidad'], 0, '', '.'); ?></div>
        <div id="filtroVisitasTotales" class="filtros_dias totales">
          <a href="#" data-dias="1" class="int">Ayer</a>
          <a href="#" data-dias="7">7 días</a>
          <a href="#" data-dias="15">15 días</a>
          <a href="#" data-dias="30" class="end">30 días</a>
        </div>
        <input type="hidden" id="hiddenDiasVisitasTotales" name="diasVisitasTotales" value="1" />
      </div>
    </div>
  <?php } ?>
  
  <div class="CR">
    
    <?php if(!empty($form['globales']['cantidad_tipologia']['#value'])) { ?>
      <div class="panel-block conBorde">
        <h2>Tipologias Publicadas</h2>
        
        <div class="recomendacion globales right open">
          <strong>Detalle de esta estadística</strong>
          <p>Conocé cuántas tipologías similares a la tuya están publicadas en el sitio.</p>
        </div>
        
        <div class="filtros_selects">
          <?php if(!empty($form['globales']['select_tipologias']['#value'])) { ?>
            <dl id="selectTipologiaPublicadas" class="dropdown"> 
              <dt>
                <a id="multiTipologiaPublicadas" data-select="TipologiaPublicadas" href="#">
                  <span class="hida">Tipologías</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPublicadas">
                  <ul id="TipologiaPublicadas">
                    <li class="quitarAll" data-content="multiTipologiaPublicadas"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_tipologias']['#value'] as $tipologia) { ?>
                      <li><label for="publicada_<?php print $tipologia['id']; ?>"><input type="checkbox" data-content="multiTipologiaPublicadas" name="tipologiaPublicadas_<?php print $tipologia['id']; ?>" data-name="<?php print $tipologia['tipologia']; ?>" id="publicada_<?php print $tipologia['id']; ?>" value="<?php print $tipologia['id']; ?>" /><?php print $tipologia['tipologia']; ?></label></li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <?php if(!empty($form['globales']['select_operaciones']['#value'])) { ?>
            <dl id="selectOperacionPublicadas" class="dropdown"> 
              <dt>
                <a id="multiOperacionPublicadas" data-select="OperacionPublicadas" href="#">
                  <span class="hida">Operaciones</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPublicadas">
                  <ul id="OperacionPublicadas">
                    <li class="quitarAll" data-content="multiOperacionPublicadas"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_operaciones']['#value'] as $operacion) { ?>
                      <li><label for="operacionPublicada-<?php print $operacion['id']; ?>"><input type="checkbox" data-content="multiOperacionPublicadas" name="operacionPublicadas_<?php print $operacion['id']; ?>" data-name="<?php print $operacion['operacion']; ?>" id="operacionPublicada-<?php print $operacion['id']; ?>" value="<?php print $operacion['id']; ?>" /><?php print $operacion['operacion']; ?></label></li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <a class="linkFiltro" id="filtroPublicadas" href="#"><i class="fa fa-search"></i></a>
        </div>
        
        <div id="contentGrafico"><canvas id="canvas2" height="200" width="280"></canvas></div>
        <div id="legendDiv">
          <ul>
          <?php foreach($legendPie as $legend) { ?>
            <li><span style="background-color:<?php print $legend['color']; ?>;"></span><?php print $legend['label']; ?> - <?php print $legend['cantidad']; ?></li>
          <?php } ?>
          </ul>
        </div>
      </div>
    <?php } ?>
    
  </div>
  
  
  <div class="CT">
    <?php if(!empty($form['globales']['precio_promedio_venta']['#value']) || !empty($form['globales']['precio_promedio_alquiler']['#value']) || !empty($form['globales']['precio_promedio_alquiler_tmp']['#value'])) { ?>
      <div class="panel-block conBorde" style="float: left;">
        <h2>Precios Promedio por Tipología</h2>
        
        <div class="recomendacion globales open">
          <strong>Detalle de esta estadística</strong>
          <p>Conocé el valor promedio de las propiedades de acuerdo a tipologías, operaciones, zonas o barrios. ¡Es una gran referencia para conocer cómo estás compitiendo con el valor de tu propiedad!</p>
        </div>
        
        <div class="filtros_selects reportePrecios">
          <?php if(!empty($form['globales']['select_tipologias']['#value'])) { ?>
            <dl id="selectTipologiaPrecios" class="dropdown"> 
              <dt>
                <a id="multiTipologiaPrecios" data-select="TipologiaPrecios" href="#">
                  <span class="hida">Tipologías</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPrecios">
                  <ul id="TipologiaPrecios">
                    <li class="quitarAll" data-content="multiTipologiaPrecios"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_tipologias']['#value'] as $tipologia) { ?>
                      <li><label for="precios_<?php print $tipologia['id']; ?>"><input type="checkbox" data-content="multiTipologiaPrecios" name="tipologiaPrecios_<?php print $tipologia['id']; ?>" data-name="<?php print $tipologia['tipologia']; ?>" id="precios_<?php print $tipologia['id']; ?>" value="<?php print $tipologia['id']; ?>" /><?php print $tipologia['tipologia']; ?></label></li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <?php if(!empty($form['globales']['select_localidades']['#value'])) { ?>
            <dl id="selectLocalidadPrecios" class="dropdown"> 
              <dt>
                <a id="multiLocalidadPrecios" data-select="LocalidadPrecios" href="#">
                  <span class="hida">Localidades de Mendoza</span>    
                  <p class="multiSel"></p>  
                </a>
              </dt>
              <dd>
                <div class="mutliSelectPrecios">
                  <ul id="LocalidadPrecios">
                    <li class="quitarAll" data-content="multiLocalidadPrecios"><label>Quitar Selecciones</label></li>
                    <?php foreach($form['globales']['select_localidades']['#value'] as $localidad) { ?>
                      <li><label for="precios-<?php print strtolower(str_replace(' ','-', $localidad)); ?>"><input type="checkbox" data-content="multiLocalidadPrecios" name="localidadPrecios_<?php print strtolower(str_replace(' ','-', $localidad)); ?>" data-name="<?php print $localidad; ?>" id="precios-<?php print strtolower(str_replace(' ','-', $localidad)); ?>" value="<?php print $localidad; ?>" /><?php print $localidad; ?></label>
                      
                        <?php if($localidad == 'Mendoza' && !empty($form['globales']['select_barrios']['#value'])) { ?>
                          <?php foreach($form['globales']['select_barrios']['#value'] as $barrio) { ?>
                            <li class="barriosCordoba"><label for="precios-<?php print strtolower(str_replace(' ','-', $barrio)); ?>"><input type="checkbox" data-content="multiLocalidadPrecios" name="barrioPrecios_<?php print strtolower(str_replace(' ','-', $barrio)); ?>" data-name="<?php print $barrio; ?>" id="precios-<?php print strtolower(str_replace(' ','-', $barrio)); ?>" value="<?php print $barrio; ?>" /><?php print $barrio; ?></label></li>
                          <?php } ?>
                        <?php } ?>
                        
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </dd>
            </dl>
          <?php } ?>
          <a class="linkFiltro" id="filtroPrecios" href="#"><i class="fa fa-search"></i></a>
          
          <div class="link_exportacion right"><a id="exportarPrecios" href="#">Exportar a Excel</a></div>
          
        </div>
        
        <div class="estadisticas_precios">
          <?php if(!empty($form['globales']['precio_promedio_venta']['#value'])) { ?>
            <div class="precio_cards">
              <h3>Ventas</h3>
              <?php foreach($form['globales']['precio_promedio_venta']['#value'] as $precios_promedio) { ?>
                <li class="tc_card">
                  <div class="tc_inner_card tilt_right" style="border-bottom: 3px solid <?php print $precios_promedio['color']; ?>;"><span class="tc_shadow"></span>
                    <div class="tc_front">
                      <div class="tc_click_target">
                        <?php print $precios_promedio['tipologia']; ?>
                        <p style="color: <?php print $precios_promedio['color']; ?>">$ <?php print $precios_promedio['promedio']; ?></p>
                      </div>
                    </div>
                  </div>
                </li>
              <?php } ?>
            </div>
          <?php } ?>
          
          <?php if(!empty($form['globales']['precio_promedio_alquiler']['#value'])) { ?>
            <div class="precio_cards">
              <h3>Alquileres</h3>
              <?php foreach($form['globales']['precio_promedio_alquiler']['#value'] as $precios_promedio) { ?>
                <li class="tc_card">
                  <div class="tc_inner_card tilt_right" style="border-bottom: 3px solid <?php print $precios_promedio['color']; ?>;"><span class="tc_shadow"></span>
                    <div class="tc_front">
                      <div class="tc_click_target">
                        <?php print $precios_promedio['tipologia']; ?>
                        <p style="color: <?php print $precios_promedio['color']; ?>">$ <?php print $precios_promedio['promedio']; ?></p>
                      </div>
                    </div>
                  </div>
                </li>
              <?php } ?>
            </div>
          <?php } ?>
          
          <?php if(!empty($form['globales']['precio_promedio_alquiler_tmp']['#value'])) { ?>
            <div class="precio_cards">
              <h3>Alquileres Temporarios</h3>
              <?php foreach($form['globales']['precio_promedio_alquiler_tmp']['#value'] as $precios_promedio) { ?>
                <li class="tc_card">
                  <div class="tc_inner_card tilt_right" style="border-bottom: 3px solid <?php print $precios_promedio['color']; ?>;"><span class="tc_shadow"></span>
                    <div class="tc_front">
                      <div class="tc_click_target">
                        <?php print $precios_promedio['tipologia']; ?>
                        <p style="color: <?php print $precios_promedio['color']; ?>">$ <?php print $precios_promedio['promedio']; ?></p>
                      </div>
                    </div>
                  </div>
                </li>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
        
        <div class="globalLoadingPrecios"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/plupload_loading.gif" /></div>
        
      </div>
    <?php } ?>
  </div>

</div>
<input type="hidden" name="interna" value="inmuebles" />