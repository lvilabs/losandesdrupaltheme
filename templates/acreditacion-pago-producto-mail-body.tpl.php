<table width="625" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;border-bottom:1px dotted #343434;">
        <table>
          <tbody>
            <tr>
              <td valign="middle" width="173" style="text-align:center;">
                <img src="!foto" width="173" height="115">
              </td>
              <td valign="top" width="452" style="padding:0cm 0cm 0cm 0cm;text-align:center;font-size:16.0pt;font-family:Arial,sans-serif;">
                <strong>Detalle de compra de:</strong><br><a href="!link_aviso">!titulo_aviso</a>
                <br>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <br>
                <span style="font-size:16.0pt;color:#343434; font-family:Arial,sans-serif;">Gracias por tu compra! !gracias_extra</span>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;text-align:center;font-size:16.0pt;font-family:Arial,sans-serif;">
        <strong>Detalle del Envío:</strong><br>
        !texto_entrega
        <br><br>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;text-align:center;font-size:16.0pt;font-family:Arial,sans-serif;">
        <strong>Pago:</strong><br>
        Abonaste a través de: !medio_pago.<br>
        M&eacute;todo de env&iacute;o: !metodo_envio.<br>
        Código de seguimiento: !codigo_seguimiento.<br>
        Cantidad: !cantidad<br>
        !talle
        !descuento<br>
        Total: !monto
        <br>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;text-align:center;font-size:16.0pt;font-family:Arial,sans-serif;">
        <strong>Si tenés alguna duda o consulta podes comunicarte con el comercio:</strong><br>
        !texto_contacto_vendedor
        <br><br>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm;text-align:center;font-size:16.0pt;font-family:Arial,sans-serif;">
        <strong>Información general del comercio:</strong><br>
        !texto_vendedor
        <br><br>
      </td>
    </tr>
  </tbody>
</table>