<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <style type="text/css">
    .no-print {display: none;}
  </style>
</head>
<body class="<?php print $classes; ?>">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>
<div class="Contenido">

  <div class="Columnas clearfix">
    <div class="CD">

    <div class="AvisoTitulo FichaAuto">
        <div class="Borde clearfix">
            <h1><?php print $node_info->title; ?></h1>
            <h2><?php print $node_info->precio; ?></h2>
        </div>
    </div>

      <div id="content">
        
        <div id="content-area">
<?php if(isset($aviso)): ?>
<?php print $aviso; ?>
<?php else: ?>
<?php print t('El contenido que desea imprimir no existe.'); ?>
<?php endif; ?>
        </div>
      </div>

    </div>

  </div>
</div>

  <?php print $closure; ?>

</body>
</html>