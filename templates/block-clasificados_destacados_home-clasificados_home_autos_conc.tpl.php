<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="AvisosDestacadosSlide BloqueTiendas clearfix">
    <div class="Title">
    	<hr>
      <h2>Concesionarias destacadas</h2>
    </div>
    <div class="accessible_news_slider slider_tienda">
<?php if(count($content)>3) : ?>
      <div class="Botones">
        <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
        <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
      </div>
<?php endif; ?>
      <div class="lista">
        <ul class="clearfix bloque_avisos <?php (count($content)>3)? print 'Slider' : print 'no-Slider';?>">
<?php for($i=0; $i<count($content); $i++) : ?>
          <li>
<?php if(isset($content[$i]['conc_logo']) && !empty($content[$i]['conc_logo'])) : ?>
            <div class="LogoTienda">
              <a href="<?php print $content[$i]['conc_url']; ?>" title="Logo de la concesionaria"><?php print $content[$i]['conc_logo']; ?></a>
            </div>
<?php endif; ?>
            <div class="CajaAviso ">
              <div class="imgAviso">
                <a href="<?php print $content[$i]['url']; ?>?cx_level=dest_auto_conc" title="<?php print $content[$i]['titulo']; ?>"> <img src="<?php print $content[$i]['foto']; ?>" alt="Imagen del aviso" title="<?php print $content[$i]['titulo']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211"> </a>
              </div>
              <div class="AvisoDescripcion">
                <h5><a href="<?php print $content[$i]['url']; ?>?cx_level=dest_auto_conc" title="<?php print $content[$i]['titulo']; ?>"><?php print $content[$i]['titulo']; ?></a></h5>
                <p>
                </p>
                <span class="precio"><?php print $content[$i]['precio']; ?></span>
              </div>
            </div>
          </li>
<?php endfor; ?>
        </ul>
      </div> 
    </div>
  </div>
</div>