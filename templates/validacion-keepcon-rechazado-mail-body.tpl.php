<tr>
  <td valign="top" style="padding:0cm 0cm 0cm 15.0pt; text-align:left;" class="bodyContent">
    <!-- // Begin Module: Standard Content \\ -->
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-family:Arial,sans-serif; font-size:12.0pt">Hola !usuario</span></b></p>
    <p style="margin: 0 0 0.0001pt;"><b><span style="font-family:Arial,sans-serif; font-size:12.0pt">&nbsp;</span></b></p>
    <p style="margin: 0 0 0.0001pt;"><span style="font-family:Arial,sans-serif; font-size: 10.5pt;">La razón por la que tu aviso no se publicó se debe a que el contenido del mismo es inapropiado de acuerdo a las normas de publicación del sitio.</span></p>
    <br>
    <p style="margin: 0 0 0.0001pt;"><span style="font-family:Arial,sans-serif; font-size: 10.5pt;">Código del rechazo: !motivo</span></p>
    <span style="font-family:Arial,sans-serif; font-size:12.0pt"><br><br></span>
    <!-- // End Module: Standard Content \\ -->
  </td>
</tr>