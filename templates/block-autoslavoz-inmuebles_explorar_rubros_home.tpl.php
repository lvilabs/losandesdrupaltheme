<?php
$array_rubros = clasificados_rubros_obtener_rubros('aviso_casa');
$rubro_padre = clasificados_rubros_obtener_rubro_raiz_tipo_aviso('aviso_casa');
?>
<div class="explorar">
  <h4><strong>Explorar Rubros</strong></h4>
  <ul>
    <?php
    foreach($array_rubros as $rubro) {
    ?>
    <li><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34%3A<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34%3A<?php print $rubro->tid; ?>" title="<?php print $rubro->title; ?>" rel="search"><?php print $rubro->title; ?></a></li>
    <?php
    }
    ?>
  </ul>
</div>