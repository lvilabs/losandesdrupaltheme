<div class="Content clearfix">
  <div class="ContactoAutos">
    <div class="Sombra" style="background: none;">
      <div class="Borde clearfix" style="border-bottom: 1px solid #ccc; padding: 0px;   height: initial;">
        <h1>Estimado Usuario:</h1>
      </div>
    </div>
    <div class="blanco clearfix" style="background: #FFFFCC; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; padding: 10px; color: #666; font-size: 13px; margin: 10px 0;">
      <div style="float: left; width: 75%;">
        <p>Sólo por hoy, <strong><u>desde las 07 a las 09 y desde las 13 a las 15 hs.</u></strong> estaremos realizando tareas de mantenimiento en nuestros servidores para brindarle mayor rapidez al sitio.</p>
        <p>No podrás ingresar a tu usuario ni publicar avisos, pero los motores de búsqueda permanecen activos para que tus clientes puedan navegar, visualizar y consultar tus avisos ya publicados.</p>
        <br>
        <p>Lamentamos los inconvenientes que esto pueda ocasionarte.</p>
        <br>
        <p><strong>Equipo de Clasificados La Voz</strong></p>
      </div>
      <div style="float: right;">
        <img style="width: 118px;" src="/sites/clasificadoslavoz.com.ar/themes/principal/img/mantenimiento.png" />
      </div>
    </div>
  </div>
</div>