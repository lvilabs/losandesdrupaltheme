<?php
  global $user;
  $edicion = in_array("edit", arg());
  $tipos_avisos_nodetypes = sitio_local_obtener_tipos_avisos_node_types();
  $segmento_vid = clasificados_node_obtener_segmento_vocabulary_id($form['#node']);

  //Si se intenta igresar al formulario de publicacion de Emprendimientos sin tener espacios para el tipo de aviso redireccionamos a casa
  if(arg(2) == 'aviso-emprendimiento' && (!user_is_logged_in() || (!isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS]) && !isset($user->roles[AUTOSLAVOZ_ROL_COLABORADOR]))) ){
    drupal_goto('node/add/aviso-casa');
  }
  
  //Si el Tipo de Aviso tiene Rango de Cuotas entonces hay que procesar los valores ingresados.
  if(isset($form['field_aviso_rango_cuotas'])) {
    $rangos = $form['field_aviso_rango_cuotas'][0]['#default_value']['value'];

    $rangos_matches = array();
    if(preg_match_all('/Cuota\s+([0-9]+)\s+a\s+([0-9]+)\s+\\$\s+([0-9\.]+)/um', $rangos, $rangos_matches)) {
      unset($rangos_matches[0]);
    } else {
      $rangos_matches = array();
    }
  }
?>
<div class="Content CrearAnuncio clearfix">
  <?php 
    if($user->uid != 0) {
      if($edicion) {
  ?>
      <h3>Editar Aviso</h3>
  <?php
      } else if(arg(2)=='clone') {
  ?>
      <h3>Copiar Aviso</h3>
  <?php
      }
   } else {
  ?>
    
  <?php
  }
  ?>
  <div class="blanco">
    <?php if($user->uid != 0 && !$edicion){ ?>
      <div class="pasosbreadcrumb clearfix aviso">
        <div class="pasos clearfix">
          <div class="paso1 active">Datos del aviso</div>  
          <div class="paso2"><label>Publicación</label></div>  
        </div>
      </div>
    <?php } ?>
    <div class="avisolegal">Los campos marcados con un asterisco (<span style="color:red;">*</span>) son obligatorios.</div>
    <div class="FormCrear"> 
    </div>
    <div class="FormRegistro ContactoAutos Avisos">
      <fieldset>        
        <div class="Row clearfix">
          <div id="tipos-contenido-wrapper" class="form-item tipos-contenido-wrapper">
          <label>Categoría: <span style="color:red;">*</span> </label>
          <?php            
            if($edicion || arg(2)=="clone"){
              $rubro = clasificados_rubros_node_obtener_rubro_term($form['#node']);
          ?>
              <input type="text" value="<?php print htmlentities($rubro->name, ENT_QUOTES, 'utf-8'); ?>" readonly="readonly" class="readonly-text" style="width: 172px;"/>
          <?php
            } else {
              $rubros = clasificados_rubros_obtener_rubros(arg(2));
              
              //Excluimos tipo de aviso Emprendimientos si el usuario es anonimo o no es Comercio de Terceros.
              if(!user_is_logged_in() || (!isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS]) && !isset($user->roles[AUTOSLAVOZ_ROL_COLABORADOR]))){
                foreach($rubros as $rubro){
                  if($rubro->aviso_node_type == 'aviso_emprendimiento'){
                    unset($rubros[$rubro->tid]);
                  }
                }
              }
?>
              <select name="tipos-contenido" class="" id="select-tipos-contenido">
              <?php foreach($rubros as $rubro) : ?>
                <?php if(!in_array($rubro->aviso_node_type, variable_get('clasificados_rubros_ocultos', array()))) : ?>
                  <option value="/node/add/<?php print str_replace('_', '-', $rubro->aviso_node_type); ?>" <?php if ($rubro->aviso_node_type == $form['#node']->type) print 'selected="selected"'; ?>><?php print htmlentities($rubro->title, ENT_QUOTES, 'utf-8'); ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
              </select>
<?php
            }
?>
          <div class="description" style="display: none;"><span class="Ingresa">Cargando formulario para <strong>tipo-contenido</strong>. Espere por favor...</span></div>
          </div>
        </div>
      </fieldset>
      <fieldset>
        <div class="AyudaAlta Descripcion">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul><li><strong>Agregá toda la información extra</strong> sobre tu inmueble. Tené en cuenta que las primeras palabras de la descripción son las que aparecen en la lista de los resultados, por lo que aprovechá para poner alguna característica llamadora.
            </li></ul>
          </div>
        </div>
        <h3>Tipo de Inmueble y Operación</h3>
<?php if(isset($form['field_aviso_tipo_emprendimiento'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['field_aviso_tipo_emprendimiento']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_unidad'])): ?>        
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_unidad']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_terreno'])): ?>        
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_terreno']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_unidad_dpto'])): ?>        
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_unidad_dpto']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_campo'])): ?>        
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_campo']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_operacion'])): ?>
        <div class="Row clearfix <?php print ($form['#node']->type=='aviso_emprendimiento')?' DN':''; ?>">
          <?php
            if(in_array($form['type']['#value'], array('aviso_casa', 'aviso_departamento'))):
              unset($form['field_aviso_operacion']['value']['#options'][5]);
            endif;
            print drupal_render($form['field_aviso_operacion']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_dormitorios'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_dormitorios']); 
          ?>
        </div>
<?php endif; ?>
        <div class="Row clearfix">
          <?php 
            $form['title']['#title'] = 'Título del Aviso';
            if(($form['#node']->type=='aviso_emprendimiento'))
              $form['title']['#title'] = 'Nombre de Emprendimiento';
            $form['title']['#description'] = '<span class="Ingresa">Máximo de 80 caracteres.</span>';
            print drupal_render($form['title']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php
            $form['body_field']['body']['#rows'] = 5;
            $form['body_field']['body']['#description'] = '<span class="Ingresa">Ingrese todos los datos extras que desee agregar a su aviso.</span>';
            unset($form['body_field']['format'][1]);
            unset($form['body_field']['format'][5]);
            unset($form['body_field']['format'][4]['#description']);
            unset($form['body_field']['format']['#type']);
            unset($form['body_field']['format']['#title']);
            print drupal_render($form['body_field']); 
          ?>
        </div>
<?php if(isset($form['field_aviso_apto_escritura'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_apto_escritura']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tiempo_entrega'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tiempo_entrega']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_superficie'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_superficie']); 
          ?>
        </div>
<?php endif; ?>
<?php if($form['#node']->type == 'aviso_emprendimiento'): ?>
  <?php if(isset($form['field_aviso_etapa_construccion'])): ?>
        <div class="Row clearfix etapa-construccion">
          <?php
            print drupal_render($form['field_aviso_etapa_construccion']);
          ?>
        </div>
  <?php endif; ?>
<?php endif; ?>
<?php if(isset($form['field_aviso_emprendimiento'])): ?>
        <div class="Row clearfix">
          <?php
            $form['field_aviso_emprendimiento']['nid']['nid']['#description'] = '<span class="Ingresa">Si esta propiedad forma parte de un emprendimiento cargado, asocialo.</span>';
            print drupal_render($form['field_aviso_emprendimiento']); 
          ?>
        </div>
        
<?php endif; ?>
      </fieldset>
      <fieldset>
        <h3>Ubicación Propiedad</h3>
        <div class="Row clearfix">
          <?php 
            if($edicion && !isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS])) {
              $provincia = '';
              $ciudad = '';
              $barrio = '';
              foreach($form['taxonomy'][5]['#default_value'] as $ubicacion){
                if (!clasificados_taxonomy_has_parent($ubicacion)) {
                  $provincia = taxonomy_get_term($ubicacion);
                } elseif(clasificados_taxonomy_es_ciudad($ubicacion)) {
                  $ciudad = taxonomy_get_term($ubicacion);
                } else {
                  $barrio = taxonomy_get_term($ubicacion);
                }
              }
              ?>
              <div class="form-item">
                <label>Provincia, Ciudad y Barrio: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
                <input type="text" value="<?php print $provincia->name; ?>" readonly="readonly" class="readonly-text" />
                <?php if($ciudad->name != ""){ ?>
                  <input type="text" value="<?php print $ciudad->name; ?>" readonly="readonly" class="readonly-text ciudad" />
                <?php } ?>
                <?php if($barrio->name != ""){ ?>
                  <input type="text" value="<?php print $barrio->name; ?>" readonly="readonly" class="readonly-text" />
                <?php } ?>
              </div>
            <?php  
            } else {
              $form['taxonomy'][5]['#title'] = 'Provincia, Ciudad y Barrio';
              print drupal_render($form['taxonomy'][5]); 
            }
          ?>
        </div>
<?php if(isset($form['field_aviso_tipo_barrio'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_barrio']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_zona'])): ?>
        <div class="Row clearfix zona">
          <?php 
            print drupal_render($form['field_aviso_zona']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_zona_turistica'])): ?>
        <div class="Row clearfix DN zona_turistica">
          <?php
            print drupal_render($form['field_aviso_zona_turistica']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_calle'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_calle_altura'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle_altura']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_confidencial'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_confidencial']['value'][1]['#title'] = '';
            $form['field_aviso_confidencial']['#description'] = '<span class="Ingresa">Se ocultaran los campos de Calle y Altura en la ficha del aviso.</span>';
            print drupal_render($form['field_aviso_confidencial']);
            print '<div class="form-item"><div class="description"><span class="Ingresa">Se ocultaran los campos de Calle y Altura en la ficha del aviso.</span></div></div>';
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_num_manzana'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_num_manzana']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_num_lote'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_num_lote']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mapa'])): ?>
        <div class="Boton">
          <input type="button" value="Marcar Dirección en Mapa" class="boton-mapa" onClick="javascript:agregar_mapa();">
        </div>
        <div class="opciones-geomap DN">
          <div>Encontramos mas de una posible dirección, selecciona la más cercana:</div>
        </div>
        <div class="recomendacion DN">
          <strong>Error en la geolocalización de la dirección</strong><br> 
          <span class="reco-message"></span>
        </div>
        <div class="Row clearfix">
          <label for="edit-picture-upload">  
            <?php print ''.$form['field_aviso_mapa']['#title'].': <br>(Opcional)'; ?>
          </label>
          <div id="multimedia_mapas" style="display: block; width: 58%; height: 300px; float: left;"></div>
          <?php 
            unset($form['field_aviso_mapa'][0]['locpick']['instructions']);
            print drupal_render($form['field_aviso_mapa'][0]['locpick']['map']);
          ?>
          <div class="description">
            <span class="Ingresa Mapa"><?php print drupal_render($form['field_aviso_mapa'][0]['locpick']['map_instructions']); ?></span>
            <?php 
            if(isset($form['field_aviso_mapa'][0]['delete_location'])){
              unset($form['field_aviso_mapa'][0]['delete_location']['#title']);
              $form['field_aviso_mapa'][0]['delete_location']['#description'] = "Eliminar ubicación en el mapa.";
              print drupal_render($form['field_aviso_mapa'][0]['delete_location']);
            } 
            ?>
          </div>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_usado_anio'])): ?>
        <div class="Row clearfix">
          <?php 
            $form['field_aviso_usado_anio'][0]['value']['#title'] = '';
            print drupal_render($form['field_aviso_usado_anio']);
          ?>
        </div>
<?php endif; ?>
      </fieldset>
      <fieldset>
        <h3>Datos del Vendedor</h3>
        <div class="AyudaAlta completar-whatsapp">
          <span class="tag nuevo">Nuevo</span>
          <div class="Texto">
            <ul>
              <li><strong>¡Sumá tu WhatsApp!</strong>. Aumentá las chances de contacto agregando un teléfono por cada uno de tus avisos.</li>
            </ul>
            <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 </span>0 
            
            <?php if(empty($form['field_aviso_telefono_whatsapp'][0]['#value']['value'])): ?>
              <?php if(empty($user->profile_telefono_whatsapp)): ?>
                <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                15 <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php unset($form['field_aviso_ocultar_whatsapp']); ?>
              <?php else: ?>
                <?php $whatsapp = explode('+549', $user->profile_telefono_whatsapp); ?>
                <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
                <div class="ocultar-wa">
                  <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
                </div>
              <?php endif; ?>
            <?php else: ?>
              <?php $whatsapp = explode('+549', $form['field_aviso_telefono_whatsapp'][0]['#value']['value']); ?>
              <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
              <div class="ocultar-wa">
                <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
              </div>
            <?php endif; ?>
            
          </div>
        </div>
<?php if(isset($form['field_aviso_tel_vendedor'])): ?>
        <div class="Row clearfix telefonoContacto">
          <?php if(empty($form['field_aviso_tel_vendedor'][0]['#value']['value'])): ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" > /
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" > /
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php else: ?>
            <?php 
              $tel_1 = '';
              $tel_2 = '';
              $tel_3 = '';
              $array_tel = explode('/', $form['field_aviso_tel_vendedor'][0]['#value']['value']);
              foreach($array_tel as $key => $num_tel) {
                switch($key) {
                  case 0:
                    $tel_1 = trim($num_tel);
                    break;
                  case 1:
                    $tel_2 = trim($num_tel);
                    break;
                  case 2:
                    $tel_3 = trim($num_tel);
                    break;
                }
              }
            ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" value="<?php print $tel_1; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" value="<?php print $tel_2; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" value="<?php print $tel_3; ?>" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php endif; ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_tel'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_tel']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mail_vendedor'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_mail_vendedor']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_mail'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_mail']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_web'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_web']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
<?php if(isset($form['field_aviso_constructora']) || isset($form['field_aviso_financiadora'])): ?>
      <fieldset>
        <h3>Firmas Responsables</h3>
        <?php if(isset($form['field_aviso_constructora'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_constructora']); 
          ?>
        </div>
        <?php endif; ?>
        <?php if(isset($form['field_aviso_financiadora'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_financiadora']); 
          ?>
        </div>
        <?php endif; ?>
      </fieldset>
<?php endif; ?>
      <fieldset>
        <h3>Forma de Pago</h3>
        <?php if(isset($form['field_aviso_precio'])): ?>
        <div class="AyudaAlta">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul>
              <li><strong>Agregá el precio</strong>  y no quedes afuera cuando un usuario hace una búsqueda por Precio. En el caso de que quieras ocultarlo, hacé clic en Ocultar Precio.</li>	
            </ul>	
          </div>
        </div>         
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_precio']); 
          ?>
        </div>
        <?php if(isset($form['field_aviso_precio_hasta'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_precio_hasta']); 
          ?>
        </div>
        <?php endif; ?>
<?php endif; ?>
<?php if(isset($form['field_aviso_moneda'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_moneda']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_precio'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_precio']); 
          ?>
        </div>
<?php endif; ?>

<?php if(isset($form['field_aviso_precio_uva'])): ?>
        
        <div class="AyudaAlta CotizacionUva DN">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul>
              <li><strong>Valor UVA <?php print date('d-m-Y'); ?></strong> <span><?php print clasificados_precio_uva(); ?></span></li>	
            </ul>	
          </div>
        </div>
        
        <div class="Row Radiobutton clearfix precio-uva DN">
          <?php 
            $form['field_aviso_precio_uva']['value']['#description'] = '<span class="Ingresa">'.$form['field_aviso_precio_uva']['value']['#description'].'</span>';
            print drupal_render($form['field_aviso_precio_uva']); 
          ?>
          <div class="mont-convertido-uva"></div>
        </div>
<?php endif; ?>

<?php if(isset($form['field_aviso_recibe_menor'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_recibe_menor']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_forma_pago'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_forma_pago']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_apto_credito'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_apto_credito']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['disponible_venta'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php
            print drupal_render($form['disponible_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['cantidad_venta'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['cantidad_venta']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['precio_anterior_venta'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['precio_anterior_venta']);
          ?>
        </div>
<?php endif; ?>
      </fieldset>

<?php if($form['#node']->type!='aviso_emprendimiento'): ?>
      <fieldset>
        <h3>Características del Inmueble</h3>
<?php if(isset($form['field_aviso_antiguedad'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_antiguedad']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_estrenar'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_estrenar']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_estrenar']); 
          ?>
        </div>
<?php endif; ?>
<?php if($form['#node']->type != 'aviso_emprendimiento'): ?>
  <?php if(isset($form['field_aviso_etapa_construccion'])): ?>
        <div class="Row clearfix etapa-construccion">
          <?php 
            print drupal_render($form['field_aviso_etapa_construccion']); 
          ?>
        </div>
  <?php endif; ?>
<?php endif; ?>
<?php if(isset($form['field_aviso_ubicacion'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_ubicacion']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_privados'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_privados']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_piso_numero'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_piso_numero']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_plantas'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_plantas']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_banios'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_banios']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_banio'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_banio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_techo'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_techo']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_porton'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_porton']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_cocheras'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_cocheras']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cobertura_cochera'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cobertura_cochera']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_tipo_cochera'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_cochera']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_superficie_total'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_superficie_total']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_superficie_cubierta'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_superficie_cubierta']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ancho_entrada'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_ancho_entrada']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_alto_entrada'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_alto_entrada']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_columnas'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_columnas']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_naves'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_naves']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_patron_municipalidad'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_patron_municipalidad']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_expensas'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_expensas']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_apto_profecional'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_apto_profecional']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_en_edificio'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_en_edificio']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_en_edificio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_galeria_shopping'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_galeria_shopping']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_galeria_shopping']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_distancia_pavimento'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_distancia_pavimento']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_hectarias'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_hectarias']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ganaderia'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_ganaderia']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_ganaderia']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_agricultura'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_agricultura']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_agricultura']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_casa_principal'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_casa_principal']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_casa_principal']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_casa_casero'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_casa_casero']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_casa_casero']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_vigilancia'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_vigilancia']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_vigilancia']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_baulera'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_baulera']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_baulera']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_propiedad_ocupada'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_propiedad_ocupada']['value'][1]['#title'] = '';
            print drupal_render($form['field_aviso_propiedad_ocupada']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_superficie_terreno'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_superficie_terreno']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_metros_frente'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_metros_frente']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_metros_fondo'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_metros_fondo']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_superficie_construib'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_superficie_construib']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ultima_actividad'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_ultima_actividad']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
<?php endif; ?>
      
<?php if($form['type']['#value'] == 'aviso_departamento') : ?>
      <fieldset>
        <h3>Información del Edificio</h3>
<?php if(isset($form['field_aviso_tipo_edificio'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_edificio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_condicion'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_condicion']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_dpto_piso'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_dpto_piso']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_cantidad_pisos'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_cantidad_pisos']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
<?php endif; ?>
      
      <fieldset>
      <div class="AyudaAlta">
          <span class="punta"></span>
          <?php if(user_is_logged_in()) {
            
                    if($form['#node']->type=='aviso_emprendimiento'){
                      /*:::AYUDA PARA AVISOS EMPRENDIMIENTOS.:::*/
                      ?>
                          <div class="Texto"> 
                            <ul>
                              <li>
                                Los usuarios buscan toda la infomación posible para elegir una propiedad. <br /><br />
                                <strong>Publicá fotos, renders, planos y videos</strong> para que los usuarios se interesen más por tu emprendimiento.<br />
                            
                              </li>
                            </ul>
                          </div>
                      <?php  }else{ ?>
                          <!-- :::AYUDA PARA USUARIOS LOGUEADOS:::-->
                          <div class="Texto">
                            <ul>
                              <li><strong>Subí tu foto</strong> y aumenta las posibilidades de visitas.<br />
                                <br />
                                Dependiendo el tipo de aviso que elijas para publicar tu vehículo, se verá diferente cantidad de imágenes: <br />
                                <br />
                                - <strong>Aviso Estandar:</strong> 6 fotos. <br />
                                - <strong>Aviso Premium:</strong> 12 fotos. <br />
                                - <strong>Aviso Super Premium y Vidriera:</strong> 25 fotos.<br />
                                <br />
                                Si contratas un destaque para el aviso, ten en cuenta que la 1ª foto que cargues es la que se verá en el listado de avisos.
                              </li>
                            </ul>
                          </div>
          <?php
          }
          } else { ?>
          <div class="Texto">
            <ul><li>Los usuarios quieren ver avisos con fotos. Subí tu foto y aumentá la cantidad de visitas.<br /><br /><strong> Tu aviso es Gratuito: SÓLO se verá la primer foto cargada.</strong> <br />
            
            Para que tu aviso tenga más fotos y más visitas, completá tu registración y destacá tu aviso.</li></ul>
          </div>
          <?php } ?>
        </div>
        <h3>Multimedia</h3>
        <div class="Row clearfix Fotos">
          <?php 
            print drupal_render($form['field_aviso_fotos']); 
          ?>
        </div>
        <?php if(isset($form['field_aviso_video'])) { ?>
        <div class="Row clearfix AgregarVideo">
          <?php
            $form['field_aviso_video'][0]['embed']['#description'] = '<span class="Ingresa">Ingresa la URL de YouTube. Solo visibles en avisos Estandars, Premiums, Super Premiums y Vidriera</span>';
            unset($form['field_aviso_video'][0]['value_markup']);
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#title'] = 'Miniatura del Video:';
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#collapsible'] = 0;
            $form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['#title'] = '';
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['flags']);
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['description']);
            $form['field_aviso_video'][0]['emvideo']['delete']['#title'] = 'Eliminar Video';
            $form['field_aviso_video'][0]['emvideo']['delete']['#description'] = '<span class="Ingresa">Marque la casilla para eliminar el video.</span>';
            print drupal_render($form['field_aviso_video']);
          ?>
        </div>
        <?php } ?>
        <?php if(isset($form['field_aviso_emprendimiento_logo'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_emprendimiento_logo']); 
          ?>
        </div>
        <?php endif; ?>
        <?php if(isset($form['field_aviso_renders'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_renders']); 
          ?>
        </div>
        <?php endif; ?>
        <?php if(isset($form['field_aviso_planos'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_planos']); 
          ?>
        </div>
        <?php endif; ?>
        <?php unset($form['field_aviso_recorrido_360']); if(isset($form['field_aviso_recorrido_360'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_recorrido_360']); 
          ?>
        </div>
        <?php endif; ?>
      </fieldset>
<?php if(isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_CASAS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_DEPARTAMENTOS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_LOCALES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_OFICINAS_TAXONOMY_VID])): ?>
      <fieldset class="confort">
        <h3>Ambientes</h3>
        <div class="Row clearfix">
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_CASAS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMBIENTES_CASAS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMBIENTES_CASAS_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_DEPARTAMENTOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMBIENTES_DEPARTAMENTOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMBIENTES_DEPARTAMENTOS_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_LOCALES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMBIENTES_LOCALES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMBIENTES_LOCALES_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_AMBIENTES_OFICINAS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMBIENTES_OFICINAS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMBIENTES_OFICINAS_TAXONOMY_VID]);
            }
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_LOCALES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_OFICINAS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_GALPONES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_CAMPOS_TAXONOMY_VID])): ?>
      <fieldset class="confort">
        <h3>Instalaciones</h3>
        <div class="Row clearfix">
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_INSTALACIONES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_INSTALACIONES_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_LOCALES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_INSTALACIONES_LOCALES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_INSTALACIONES_LOCALES_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_OFICINAS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_INSTALACIONES_OFICINAS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_INSTALACIONES_OFICINAS_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_GALPONES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_INSTALACIONES_GALPONES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_INSTALACIONES_GALPONES_TAXONOMY_VID]);
            }
            if(isset($form['taxonomy'][CLASIFICADOS_INSTALACIONES_CAMPOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_INSTALACIONES_CAMPOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_INSTALACIONES_CAMPOS_TAXONOMY_VID]);
            }
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_AMENITIES_DEPARTAMENTOS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_AMENITIES_EMPRENDIMIENTOS_TAXONOMY_VID])): ?>
      <fieldset class="confort">
        <h3>Amenities</h3>
        <div class="Row clearfix">
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_AMENITIES_DEPARTAMENTOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMENITIES_DEPARTAMENTOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMENITIES_DEPARTAMENTOS_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_AMENITIES_EMPRENDIMIENTOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_AMENITIES_EMPRENDIMIENTOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_AMENITIES_EMPRENDIMIENTOS_TAXONOMY_VID]);
            }
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_CASAS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_DEPARTAMENTOS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_LOCALES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_OFICINAS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_GALPONES_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_CAMPOS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_EMPRENDIMIENTOS_TAXONOMY_VID])): ?>
      <fieldset class="confort">
        <h3>Servicios</h3>
        <div class="Row clearfix">
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_CASAS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_CASAS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_CASAS_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_DEPARTAMENTOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_DEPARTAMENTOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_DEPARTAMENTOS_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_LOCALES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_LOCALES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_LOCALES_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_OFICINAS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_OFICINAS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_OFICINAS_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_GALPONES_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_GALPONES_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_GALPONES_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_CAMPOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_CAMPOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_CAMPOS_TAXONOMY_VID]);
            }
          ?>
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_SERVICIOS_EMPRENDIMIENTOS_TAXONOMY_VID])){
              $form['taxonomy'][CLASIFICADOS_SERVICIOS_EMPRENDIMIENTOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_SERVICIOS_EMPRENDIMIENTOS_TAXONOMY_VID]);
            }
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['taxonomy'][CLASIFICADOS_EQUIPAMIENTO_ALQUILERES_TEMPORARIOS_TAXONOMY_VID]) || isset($form['taxonomy'][CLASIFICADOS_OPCIONES_ALQUILERES_TEMPORARIOS_TAXONOMY_VID])): ?>
      <fieldset class="confort DN equipamiento_temporario">
        <h3>Equipamiento</h3>
        <div class="Row clearfix">
          <?php
            if(isset($form['taxonomy'][CLASIFICADOS_EQUIPAMIENTO_ALQUILERES_TEMPORARIOS_TAXONOMY_VID])) {
              $form['taxonomy'][CLASIFICADOS_EQUIPAMIENTO_ALQUILERES_TEMPORARIOS_TAXONOMY_VID]['#title'] = '';
              print drupal_render($form['taxonomy'][CLASIFICADOS_EQUIPAMIENTO_ALQUILERES_TEMPORARIOS_TAXONOMY_VID]);
            }
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['captcha'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(isset($form['captcha_google'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha_google']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(!user_is_logged_in()) { ?>
      <fieldset>
        <div class="Row clearfix condiciones">
          <?php
            print drupal_render($form['profile_condiciones']);
          ?>
        </div>
        <div class="Row clearfix ofertas">
          <?php
            print drupal_render($form['profile_recepcion_ofertas']);
          ?>
        </div>
      </fieldset>
<?php } ?>
<?php if(user_is_logged_in() && isset($form['datos_privados']['propietario'])) { ?>
      <fieldset class="datos_privados">
        <h3>Datos de Agenda de Contactos (Datos Privados)</h3>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['propietario']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['vendedor']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['estado_inmueble']); 
          ?>
        </div>
        <div class="Row Radiobutton clearfix">
          <div class="form-checkboxes">
            <label><?php print $form['datos_privados']['cartel']['#title'];?>: </label>
            <?php
              $form['datos_privados']['cartel']['#title'] = '';
              print drupal_render($form['datos_privados']['cartel']); 
            ?>
          </div>
        </div>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['comentario']); 
          ?>
        </div>
      </fieldset>
<?php } ?>
      <div class="alertaLegal">
        <div class="Texto"> 
          Está expresamente prohibido ofrecer o publicar material que quebrante cualquier legislación vigente (ejemplo Decreto Nacional 936/11). Tampoco está permitido publicar material inmoral, obsceno, pornográfico, discriminatorio, injurioso o cualquier otro contenido que afecte la privacidad de las personas, la propiedad intelectual o que sea contrario a las buenas costumbres. 
        </div>
      </div>
      <div class="Row clearfix">
        <div class="Ingresa">
          <div class="Boton">
            <div class="Cv Tl"></div>
            <div class="Cv Tr"></div>
            <div class="Cv Bl"></div>
            <div class="Cv Br"></div> 
            <?php 
              unset($form['buttons']['preview']);
              unset($form['buttons']['delete']);
              if($user->uid != 0){
                if(arg(1) == 'add')
                  $form['buttons']['submit']['#value'] = "Siguiente >";
                elseif(arg(2) == 'clone')
                  $form['buttons']['submit']['#value'] = "Copiar";
              }
              print drupal_render($form['buttons']['submit']); 
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-sin-mostrar" style="display:none;">
  <?php
    unset($form['language']);
    unset($form['workflow']);
    print drupal_render($form);
  ?>
  </div>
</div>

<?php 
if (isset($_COOKIE['infoAdminPrecioUVA'])) {
  unset($_COOKIE['infoAdminPrecioUVA']);
  setcookie('infoAdminPrecioUVA', null, -1, '/');
}
?>

<!-- Mensaje ayuda -->
<?php /* if(!$edicion || $form['field_aviso_operacion']['#default_value'][0]['value'] == 2){ ?>
  <?php if(!isset($_COOKIE['infoAdminPrecioUVA'])){ ?>
    <div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
    <div id="popover" class="popover bottom Agenda" style="top: 2150px; left: 324px;">
      <div class="arrow"></div>
      <h3 class="popover-title">¡Nuevas funcionalidades!</h3>
      <div class="popover-content"><strong>Precios UVA:</strong><br>Ahora podrás marcar tus avisos de venta de inmuebles para que muestren su costo en UVAS.<br>El mismo será visible tanto en la ficha como en el resultado de búsqueda. <br> 
      </div>
      <!-- <div class="popover-navigation txtRight"><button class="button" data-role="next">Aceptar</button></div> -->
    </div>
    <?php setcookie('infoAdminPrecioUVA', 1, time() + 86400, "/"); //Caduca en 1 dias ?>
  <?php } ?>
<?php }*/ ?>
