<?php
  //print_r($form);
  $form['#redirect'] = "/home";
?>
<div class="Content FormRegistro clearfix">
  <div class="ContactoAutos">
    <div class="Sombra">
      <div class="Borde clearfix">
        <h1>Recuperar clave</h1>
      </div>
    </div>
    <div class="blanco ">
      <p>Para recuperar tu clave deber&aacute;s ingresar tu usuario, recibir&aacute;s un link en tu casilla de correo para poder generarla nuevamente.</p>
      <div class="clear0">&nbsp;</div>
        <div class="Entry">
          <div class="Row clearfix">
            <?php 
              print drupal_render($form['name']); 
            ?>
            <div class="Row LeftCol clearfix ">
              <div class="Boton">
                <div class="Cv Tl"></div>
                <div class="Cv Tr"></div>
                <div class="Cv Bl"></div>
                <div class="Cv Br"></div> 
                <?php 
                  $form['submit']['#value'] = "Aceptar";
                  print drupal_render($form['submit']); 
                ?>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="clear0">&nbsp;</div>
  </div>
</div>
<?php
  print drupal_render($form);
?>