<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="620" cellspacing="0" cellpadding="0" border="1" style="width:465.0pt;background:white;border-collapse:collapse;border:none">
          <tbody>
            <tr>
              <td style="border:none;background:transparent;padding:0cm 0cm 0cm 0cm" colspan="2">
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Hola !comercio_vendedor, han intentado realizar una compra on line en tu Tienda Virtual de Clasificados Los Andes.<u></u><u></u></span></b></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">El producto es: <a href="!link_aviso">!titulo_aviso</a><u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><img src="!foto" /><u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Monto: !monto<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Cantidad: !cantidad<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Descuento: !descuento, !cupon_descuento<u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><b>Los datos del potencial comprador son:</b><u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Comprador: !nombre_comprador<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Teléfono: !telefono_comprador<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">E-mail: !email_comprador<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Ubicación: !ciudad_comprador, !provincia_comprador<u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>  
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>    
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><b>Observaciones:</b><u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Comentario: !comentario_comprador<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Méto de envío seleccionado: !metodo_envio<u></u><u></u></span></p>
              </td>
            </tr>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">
              <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222">Medio de pago seleccionada: !medio_pago<u></u><u></u></span></p>
              </td>
            </tr>
          </tbody>
        </table>
        <p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><u></u><u></u></span></p>
      </td>
    </tr>
  </tbody>
</table>