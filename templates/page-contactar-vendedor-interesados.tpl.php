<?php
  //print_r($form['interesados']);
?>

<div class="recomendacion destacada"><strong>IMPORTANTE (NO ES UN CONTACTO)</strong><br><br>
Utiliza este listado para contactarte con métodos poco invasivos como un envío de correo electrónico con la propiedad por la que estuvo interesado y otras similares.</div>

<div class="main-content col-full mt-20 reservas-content">
  <div class="border-wrapper edition-tabs mt-20">
    <div class="edit-section">
      <div class="title">
        <h4><i class="fa fa-inbox"></i>Interesados por aviso - últimos 15 días</h4>
        <div class="export"><a href="/administrar/contactos/interesados/scv" title="Exportar a Excel"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/logo_excel.png" alt="Exportar listado a Excel" width="30" height="30"></a></div>
        <div class="total">Total: <?php print $form['interesados']['#total']; ?></div>
      </div>
      
      <?php foreach($form['interesados']['#listado'] as $aviso) { ?>
      <div class="reserva-details">
        
        <div class="fl-left colapsed on-<?php print $aviso['nid']; ?>">
          <a class="desplegar-interesados" href="javascript: void(0);" data-id="<?php print $aviso['nid']; ?>">
            <h5><i class="fa fa-arrow-alt-circle-right"></i><?php print $aviso['titulo']; ?></h5>
            <div class="reserva-rubro"> - <?php print $aviso['rubro']; ?></div>
            <img class="colapsed-mas" src="/sites/clasificadoslavoz.com.ar/themes/principal/img/icons/add-button.png" >
            <div class="cantidad-aviso">Interesados: <?php print $aviso['total']; ?></div>
          </a>
        </div>
        
        <div class="DN off-<?php print $aviso['nid']; ?>"> 
          <div class="col-left fl-left">
            <div class="cantidad-aviso"><?php print $aviso['total']; ?></div>
            <h5><i class="fa fa-arrow-alt-circle-right"></i><?php print $aviso['titulo']; ?></h5>
            <div class="reserva-rubro"><?php print $aviso['rubro']; ?></div>
            <a href="/<?php print $form['aviso']['#path']; ?>" class="hightlited"><?php print $form['aviso']['#title']; ?>
              <img src="<?php print $aviso['imagen']; ?>" alt="<?php print $aviso['titulo']; ?>" title="<?php print $aviso['titulo']; ?>" class="imagecache">
            </a>
          </div>
          <div class="col-right fl-left">
            <div class="top-table">
              <span class="large">Nombre</span>
              <span class="large">E-Mail</span>
              <span>Visto</span>
              <span>Estado</span>
              <a class="colapsar-interesados" href="javascript: void(0);" data-id="<?php print $aviso['nid']; ?>">
                <img class="colapsed-menos" src="/sites/clasificadoslavoz.com.ar/themes/principal/img/icons/minus-shape.png" >
              </a>
            </div>
            <?php foreach($aviso['interesados'] as $interesado) { ?>
              <div id="row-table-<?php print $interesado['id']; ?>" class="row-table <?php print str_replace(' ', '-', $interesado['estado']); ?>">
                <span class="large" alt="<?php print $interesado['contacto_nombre']; ?>"><?php print $interesado['contacto_nombre']; ?></span>
                <span class="large"><?php print $interesado['contacto_email']; ?></span>
                <span class="fonts-middle"><?php print $interesado['created']; ?></span>
                <span class="fonts-middle" id="estado-<?php print $interesado['id']; ?>"><?php print $interesado['estado']; ?></span><span class="img-select" data-id="<?php print $interesado['id']; ?>"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/ayuda/ico_down.gif" /></span>
                
                <select class="estado-interesado" id="select-estado-<?php print $interesado['id']; ?>" data-id="<?php print $interesado['id']; ?>">
                  <option value="0">No visto</option>
                  <option value="1">Visto</option>
                  <option value="3">Eliminar</option>
                </select>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } ?>
      
    </div>
  </div>
</div>

<?php $paginado = $form['interesados']['#paginado']; ?>
<?php if($paginado['paginas'] != 1) { ?>
<div class="item-list">
  <ul class="pager">
    <?php if($paginado['actual'] != 0) { ?>
      <li class="pager-first first"><a href="/administrar/contactos/interesados" title="Ir a la primera página" class="active">« primera</a></li>
      <li class="pager-previous"><a href="/administrar/contactos/interesados?page=<?php print $paginado['actual']-1; ?>" title="Ir a la página anterior" class="active">‹ anterior</a></li>
    <?php } ?>
    
    <?php for($i = 0; $i < $paginado['paginas']; $i++) { ?>
      <?php if($i == $paginado['actual']) { ?>
        <li class="pager-current"><?php print $paginado['actual']+1; ?></li>
      <?php } else { ?>
        <li class="pager-item"><a href="/administrar/contactos/interesados?page=<?php print $i; ?>" title="Ir a la página <?php print $i+1; ?>" class="active"><?php print $i+1; ?></a></li>
      <?php } ?>
    <?php } ?>
    
    <?php if($paginado['actual']+1 != $paginado['paginas']) { ?>
      <li class="pager-next"><a href="/administrar/contactos/interesados?page=<?php print $paginado['actual']+1; ?>" title="Ir a la página siguiente" class="active">siguiente ›</a></li>
      <li class="pager-last last"><a href="/administrar/contactos/interesados?page=<?php print $paginado['paginas']-1; ?>" title="Ir a la última página" class="active">última »</a></li>
    <?php } ?>
  </ul>
</div>
<?php } ?>