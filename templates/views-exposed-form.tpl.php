<?php
/**
* @file views-exposed-form.tpl.php
*
* This template handles the layout of the views exposed filter form.
*
* Variables available:
* - $widgets: An array of exposed form widgets. Each widget contains:
* - $widget->label: The visible label to print. May be optional.
* - $widget->operator: The operator for the widget. May be optional.
* - $widget->widget: The widget itself.
* - $sort_by: The select box to sort the view using an exposed form.
* - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
* - $items_per_page: The select box with the available items per page. May be optional.
* - $offset: A textfield to define the offset of the view. May be optional.
* - $reset_button: A button to reset the exposed filter applied. May be optional.
* - $button: The submit button for the form.
*
* @ingroup views_templates
*/
if(strpos($_SERVER['REQUEST_URI'], 'mis-avisos.html') !== FALSE || strpos($_SERVER['REQUEST_URI'], 'mis-avisos-colaborador.html') !== FALSE  || strpos($_SERVER['REQUEST_URI'], 'avisos_soporte_fotografico') !== FALSE) {
  $rubro_autos = clasificados_rubros_obtener_rubro_padre_tipo_aviso('aviso_moto');
  $array_hijos_autos = clasificados_rubros_obtener_rubros('aviso_moto');
  foreach($array_hijos_autos as $krubro => $drubro){
    $rubros_hijos_autos[$drubro->aviso_node_type] = $krubro;
  }
  $rubro_inmuebles = clasificados_rubros_obtener_rubro_padre_tipo_aviso('aviso_casa');
  $array_hijos_inmuebles = clasificados_rubros_obtener_rubros('aviso_casa');
  foreach($array_hijos_inmuebles as $krubro => $drubro){
    $rubros_hijos_inmuebles[$drubro->aviso_node_type] = $krubro;
  }
  $rubros = array('auto_padre' => $rubro_autos->tid, 'auto_hijos' => $rubros_hijos_autos, 'inmuebles_padre' => $rubro_inmuebles->tid, 'inmuebles_hijos' => $rubros_hijos_inmuebles);

  //Filtros aplicados
  $url_filters = administracion_avisos_data_url_filters($_SERVER['REQUEST_URI']);
  $filtros_aplicados = array();
  foreach($url_filters as $filtro => $valor){
    if($filtro != 'url' && $filtro != 'page' && $valor != ''){
      //Si es campo hasta, no lo agrego
      if(strpos($filtro, '_hasta') === false){
        //Si es taxomonia busco posibles padres
        if(strpos($filtro, 'term_') !== false){
          $filtro_padre = taxonomy_get_parents($valor);
          if(!empty($filtro_padre)){
            foreach($filtro_padre as $pdata){
              $filtro_abuelo = taxonomy_get_parents($pdata->tid);
              if(!empty($filtro_abuelo)){
                foreach($filtro_abuelo as $adata){
                  $filtro_abuelo_url = administracion_avisos_get_url_sin_filtro($filtro, $url_filters, $adata->tid);
                  $filtro_abuelo_nombre = administracion_avisos_get_valor_textual_filtro($filtro, $adata->tid);
                  $filtros_aplicados[$filtro][] = array('nombre' => $filtro_abuelo_nombre, 'url' => $filtro_abuelo_url);
                }
              }
              $filtro_padre_url = administracion_avisos_get_url_sin_filtro($filtro, $url_filters, $pdata->tid);
              $filtro_padre_nombre = administracion_avisos_get_valor_textual_filtro($filtro, $pdata->tid);
              $filtros_aplicados[$filtro][] = array('nombre' => $filtro_padre_nombre, 'url' => $filtro_padre_url);
            }
          }
        }
        //Si es campo desde, armo un solo filtro con el campo hasta
        if(strpos($filtro, '_desde') !== false){
          $filtro_data = array($filtro, str_replace('_desde', '_hasta', $filtro));
        } else {
          $filtro_data = $filtro;
        }
        $filtro_url = administracion_avisos_get_url_sin_filtro($filtro_data, $url_filters, $valor);
        $filtro_nombre = administracion_avisos_get_valor_textual_filtro($filtro, $valor, $url_filters);
        $filtros_aplicados[$filtro][] = array('nombre' => $filtro_nombre, 'url' => $filtro_url);
      }
    }
  }
?>
<?php if(!empty($filtros_aplicados)){ ?>
<div class="box-filtros-aplicados">
  <div class="title-filtros-aplicados">Filtros Aplicados</div>
  <div>
<?php 
foreach($filtros_aplicados as $filtros) {
  $url_quitar_filtros = explode('?', $_SERVER['REQUEST_URI']);
  foreach($filtros as $filtro) {
?>
    <a class="filtro-aplicado" href="<?php print $filtro['url']; ?>"><?php print $filtro['nombre']; ?></a>
<?php } 
} ?>
    <div class="link-quitar-filtros"><a href="<?php print $url_quitar_filtros[0]; ?>">Quitar Filtros Aplicados</a></div>
  </div>
</div>
<?php } 
} ?>

<?php if (!empty($q)): ?>
<?php
// This ensures that, if clean URLs are off, the 'q' is added first so that
// it shows up first in the URL.
print $q;
?>
<?php endif; ?>
<div class="views-exposed-form">
<div class="views-exposed-widgets clear-block">
<?php foreach ($widgets as $id => $widget): 
  $visible = 1;
  $campo_hidden = 0;
  if(strpos($widget->widget, 'hidden') !== false){
    $campo_hidden = 1;
    $widget->label = '';
  }
?>


<?php if((in_array($id, array('filter-espacio_nid','filter-sid','filter-field_aviso_operacion_value_many_to_one','filter-term_node_tid_depth','filter-term_node_tid_depth_7','filter-field_aviso_usado_value_many_to_one','filter-field_aviso_combustible_value_many_to_one','filter-term_node_tid_depth_2','filter-field_aviso_procreauto_value_many_to_one','filter-term_node_tid_depth_3','filter-term_node_tid_depth_1','filter-term_node_tid_depth_5','filter-term_node_tid_depth_4','filter-term_node_tid_depth_6','filter-field_aviso_zona_turistica_value_many_to_one','filter-field_aviso_tipo_barrio_value_many_to_one','filter-field_aviso_apto_credito_value_many_to_one','filter-field_aviso_apto_escritura_value_many_to_one','filter-field_aviso_tipo_campo_value_many_to_one','filter-field_aviso_cantidad_dormitorios_value_many_to_one','filter-field_aviso_tipo_unidad_value_many_to_one','filter-field_aviso_estrenar_value_many_to_one','filter-field_aviso_tipo_unidad_dpto_value_many_to_one','filter-field_aviso_ubicacion_value_many_to_one','filter-field_aviso_tipo_emprendimiento_value_many_to_one','filter-field_aviso_tipo_techo_value_many_to_one','filter-field_aviso_tipo_porton_value_many_to_one', 'filter-field_aviso_tipo_terreno_value_many_to_one', 'filter-pid', 'filter-vid', 'filter-eid', 'field_aviso_tipo_unidad_tmp_value','filter-field_aviso_tipo_unidad_tmp_value_many_to_one')) || in_array($id, array('filter-tid', 'filter-tid_7', 'filter-tid_1', 'filter-tid_2', 'filter-tid_3', 'filter-tid_4', 'filter-tid_5', 'filter-tid_6', 'filter-sid_1'))) && !$campo_hidden): 
    $visible = administracion_avisos_filtro_visible($id, $url_filters, $rubros);
?>
  
  <!-- Filtro como listado -->
  <div class="block-apachesolr_search" <?php if(!$visible) print 'style="display:none;"'; ?>>
    <h2 class="title"><?php print $widget->label; ?></h2>
    <div class="item-list">
      <ul>
      <?php print administracion_avisos_convertir_filtro_lista($widget->widget, $url_filters); ?>
      </ul>
    </div>  
  </div>
  
<?php else: ?>
  
  <?php if(in_array($widget->id, array('edit-field-aviso-precio-desde-value', 'edit-field-aviso-kilometros-desde-value', 'edit-field-aviso-superficie-terreno-desde-value')) || in_array($widget->label, array('Año del Modelo'))): ?>

    <?php if(in_array($widget->id, array('edit-field-aviso-kilometros-desde-value', 'edit-field-aviso-superficie-terreno-desde-value', '')))
      $visible = administracion_avisos_filtro_visible($widget->id, $url_filters, $rubros);
    ?>
    
    <!-- Filtro Desde/Hasta -->
    <div class="block-apachesolr_search" <?php if(!$visible) print 'style="display:none;"'; ?>>
      <h2 class="title"><?php print $widget->label; ?></h2>
      <div class="content">
        <?php print administracion_avisos_convertir_filtro_desde_hasta($widget->id, $url_filters); ?>
      </div>
    </div>
  
  <?php elseif(in_array($widget->id, array('edit-body'))): ?>
    <!-- Filto Texto -->
    <?php $url = administracion_avisos_get_url_sin_filtro('body', $url_filters, ''); ?>
    <?php $aplicado = array_key_exists('body', $url_filters); 
      if($aplicado)
        $valor_aplicado = urldecode($url_filters['body']);
    ?>
    <div class="block-apachesolr_search">
      <h2 class="title">Buscá por palabra</h2>
      <div class="views-widget">
        <div class="form-item" id="edit-body-wrapper">
          <input type="text" maxlength="128" name="body" id="edit-body" size="24" value="<?php print $valor_aplicado; ?>" class="form-text">
        </div>
        <input type="submit" name="edit-body" id="edit-submit-body" value="Aplicar" class="form-submit">
        <?php if($aplicado): ?>
          <input type="submit" name="edit-body" id="edit-reset-body" value="Quitar Filtro" class="form-submit">
        <?php endif; ?>
        <input type="hidden" name="url" id="edit-body-url" value="<?php print $url; ?>">
      </div>
    </div>
  <?php elseif(in_array($widget->id, array('edit-title'))): ?>
    <!-- Filto Texto -->
    <?php $url = administracion_avisos_get_url_sin_filtro('title', $url_filters, ''); ?>
    <?php $aplicado = array_key_exists('title', $url_filters); 
      if($aplicado)
        $valor_aplicado = urldecode($url_filters['title']);
    ?>
    <div class="block-apachesolr_search">
      <h2 class="title">Buscá por título</h2>
      <div class="views-widget">
        <div class="form-item" id="edit-title-wrapper">
          <input type="text" maxlength="128" name="title" id="edit-title" size="24" value="<?php print $valor_aplicado; ?>" class="form-text">
        </div>
        <input type="submit" name="edit-title" id="edit-submit-title" value="Aplicar" class="form-submit">
        <?php if($aplicado): ?>
          <input type="submit" name="edit-title" id="edit-reset-title" value="Quitar Filtro" class="form-submit">
        <?php endif; ?>
        <input type="hidden" name="url" id="edit-title-url" value="<?php print $url; ?>">
      </div>
    </div>
    
  <?php elseif(!in_array($widget->id, array('edit-field-aviso-precio-hasta-value', 'edit-field-aviso-kilometros-hasta-value','edit-field-aviso-superficie-terreno-hasta-value')) && !in_array($widget->label, array('Año del Modelo Hasta'))): ?>
    <div class="views-exposed-widget views-widget-<?php print $id; ?>" <?php if(!$visible) print 'style="display:none;"'; ?>>
    <?php if (!empty($widget->label)): ?>
    <label for="<?php print $widget->id; ?>">
    <?php print $widget->label; ?>
    </label>
    <?php endif; ?>
    <?php if (!empty($widget->operator)): ?>
    <div class="views-operator">
    <?php print $widget->operator; ?>
    </div>
    <?php endif; ?>
    <div class="views-widget">
    <?php print $widget->widget; ?>
    </div>
    </div>
  <?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>

<?php if (!empty($sort_by)): ?>
<div class="views-exposed-widget views-widget-sort-by">
<?php print $sort_by; ?>
</div>
<div class="views-exposed-widget views-widget-sort-order">
<?php print $sort_order; ?>
</div>
<?php endif; ?>
<?php if (!empty($items_per_page)): ?>
<div class="views-exposed-widget views-widget-per-page">
<?php print $items_per_page; ?>
</div>
<?php endif; ?>
<?php if (!empty($offset)): ?>
<div class="views-exposed-widget views-widget-offset">
<?php print $offset; ?>
</div>
<?php endif; ?>
<div class="views-exposed-widget views-submit-button">
<?php print $button; ?>
</div>
<?php if (!empty($reset_button)): ?>
<div class="views-exposed-widget views-reset-button">
<?php print $reset_button; ?>
</div>
<?php endif; ?>
</div>
</div> 