<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
  $avisos = array();
  foreach($view->display_handler->default_display->view->style_plugin->rendered_fields as $aviso) {
    if(preg_match('/RB_noimagen/', $aviso['field_aviso_fotos_fid'])) {
      $aviso['foto'] = theme('imagecache', 'ficha_aviso_80_60', 'imagefield_default_images/RB_noimagen.jpg');
    } else {
      $aviso['foto'] = $aviso['field_aviso_fotos_fid'];
    }
    $fnode = new stdClass(); //fake node
    $fnode->type = $aviso['type'];
    $fnode->field_aviso_precio[0]['value'] = $aviso['field_aviso_precio_value'];
    $fnode->field_aviso_ocultar_precio[0]['value'] = $aviso['field_aviso_ocultar_precio_value'];
    $fnode->field_aviso_moneda[0]['view'] = $aviso['field_aviso_moneda_value'];
    $fnode->field_aviso_precio_hasta[0]['value'] = $aviso['field_aviso_precio_hasta'];
    $aviso['precio'] = publicacion_avisos_obtener_precio_aviso_display($fnode);
    $avisos[] = $aviso;
  }
  $kilometraje = "";
?>
<div id="destacados-block">
  <div class="CajaBorde">
    <div class="Sombra clearfix">
      <h2>Más Destacado</h2>
        <?php foreach ($avisos as $aviso): ?>
          <div class="destacado padding5">
            <?php print $aviso['foto']; ?>
            <h3><?php print $aviso['title']; ?></h3>
            <?php
              //Kilometraje
              if($aviso['field_aviso_usado_value'] == "O Km"){
                $kilometraje = $aviso['field_aviso_usado_value'];
              } elseif($aviso['field_aviso_kilometros_value'] != "") {
                $kilometraje = $aviso['field_aviso_kilometros_value']." Km";
              }
            ?>
            <h6><?php print $aviso['field_aviso_usado_anio_value']."&nbsp; ".$kilometraje." ".$aviso['field_aviso_combustible_value']; ?></h6> 
            <h5><?php print $aviso['precio']; ?></h5>
          </div>
        <?php endforeach; ?>
    </div>
  </div>
</div>