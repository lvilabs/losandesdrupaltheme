<div class="Content Estado clearfix">
  <div class="views_view view-compras">
    <div class="separador"></div>
    <div class="">
      <h3>Compras de Espacios y Mejoras</h3>
    </div>
    <div class="BoxComprasRealizadas">
      <div class="clearfix BoxTit">
        <div class="compras compras-header-fecha">Fecha</div>
        <div class="compras compras-header-producto">Detalle</div>
        <div class="compras compras-header-codigo">Código</div>
        <div class="compras compras-header-precio">Precio</div>
        <div class="compras compras-header-procesado">Procesado</div>
        <div class="compras compras-header-aviso">Aviso</div>
        <div class="compras compras-header-link">DineroMail</div>
      </div>
      <div class="blanco">
        <?php
        if(isset($view->display_handler->default_display->view->style_plugin->rendered_fields)) {
          $i=0;
          foreach($view->display_handler->default_display->view->style_plugin->rendered_fields as $compra){ ?>
          <div class="module-lista <?php print ($i%2==0)?'odd':'even'; ?> clearfix">
            <div id="node-<?php print $compra['id_cd']; ?>" class="node-type-compra clearfix">
              <div class="content teaser clearfix">
                <div class="compra compra-fecha">
                <?php
                  print $compra['compra_fecha'];
                ?>
                </div>
                <div class="compra compra-detalle">
                  <?php 
                    $result = db_query('SELECT n.nid, n.title FROM {clasificados_compra_detalle} cd, {node} n WHERE cd.nid=n.nid AND n.type IN ("espacio", "credito",  "paquete") AND cd.id_cd = %s', $compra['id_cd']);
                    $output = '';
                    if (db_affected_rows($result) < 1)
                      $output = '--';
                    while ($detalle = db_fetch_array($result)) {
                      $output .= $detalle['title'].'<br />';
                    }
                    print $output;
                  ?>
                </div>
                <div class="compra compra-dineromail"><?php print $compra['codigo_terceros']; ?></div>
                <div class="compra compra-total"><?php print $compra['compra_total']; ?></div>
                <div class="compra compra-procesado"><?php print ($compra['compra_procesada']==1)?'Sí':'No'; ?></div>
                <div class="compra compra-aviso">
                  <?php
                  if($compra['type']!='paquete' && $compra['type']!='paquete_asignado' && is_numeric($compra['nid']))
                    print '('.$compra['nid'].') <a href="'.url('node/'.$compra['nid']).'" target="_blank">'.$compra['title'].'</a>';
                  ?>
                </div>
                <div class="compra compra-link">
                  <?php if($compra['compra_procesada']!=1) { ?>
                  <a href="<?php print url('compra/'.$compra['id_cd'].'/confirmar', array('alias'=>TRUE)); ?>" target="_blank">Retomar compra</a>
                    <?php if($compra['type']!='paquete' && is_numeric($compra['nid'])) { ?>
                    <br />
                    <a href="/compra/<?php print $compra['id_cd']; ?>/cancelar" title="Desvincular el aviso de la compra">Desvincular</a> 
                    <a class="colorbox-inline" href="?width=300&height=150&inline=true#ayuda_desvincular_aviso" title="Ayuda sobre la desvinculación del aviso">(?)</a>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        <?php 
            $i++;
          }
        }
        ?>
      </div>
      <div class="DN">
        <div id="ayuda_desvincular_aviso">Al desvincular el aviso de la compra, libera el aviso para volver a realizar otra compra.
          En caso de que se hubiese realizado el pago e ingrese después de realizar la desvinculación, 
          entonces se le habilitarán créditos disponibles de destaques y/o mejoras para utilizar con otros avisos del mismo tipo.
        </div>
      </div>
    </div>
    <?php if ($pager): ?>
      <?php print $pager; ?>
    <?php endif; ?>
  </div>  
  <div class="clear0">&nbsp;</div>
</div>