<table width="600" cellspacing="0" cellpadding="0" border="0" style="width:450.0pt;background:#fdfdfd;border-collapse:collapse">
  <tbody>
    <tr>
      <td valign="top" style="padding:0cm 0cm 0cm 0cm">
        <table width="620" cellspacing="0" cellpadding="0" border="1" style="width:465.0pt;background:white;border-collapse:collapse;border:none">
          <tbody>
            <tr>
              <td style="border:none;background:transparent;padding:0cm 0cm 0cm 0cm" colspan="2">
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Hola !usuario
                <u></u><u></u></span></b></p>
                <p style="margin-top:12.0pt"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">La mejora "!destaque" de tu aviso !titulo_aviso venció el !fecha_fin.<u></u><u></u></span></b></p>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="620" cellspacing="0" cellpadding="0" border="0" style="width:465.0pt">
          <tbody>
            <tr>
              <td style="padding:4.5pt 4.5pt 4.5pt 4.5pt">                
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222"><br>
                </span><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Hasta la fecha, tu aviso tuvo <strong>!cantidad_visitas</strong>.<u></u><u></u></span></p>
                <p><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Si estás logueado volvé a contratarlo <a target="_blank" href="!link_publicacion"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">aquí</span></a>, si no, logueate e ingresá a tu <a target="_blank" href="!link_login"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">administrador</span></a>.</span></p>
                <p>
                <u></u><u></u></p>
              </td>
            </tr>
          </tbody>
        </table>
        <p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><u></u><u></u></span></p>
      </td>
    </tr>
  </tbody>
</table>