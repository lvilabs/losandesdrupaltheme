<?php
  global $user;
  global $base_url;
  
  //Valido usuario logueado
  if($user->uid == 0){
    drupal_set_message("No tiene permiso para ingresar a esta sección.", "warning");
    drupal_goto("<front>");
  }
  //print_r($form);
?>
<?php //print drupal_render($form); ?>
<div class="Content Paquetes clearfix">
  <!-- <div class="Sombra">
    <div class="Borde clearfix"><h1>Compra de Paquete</h1></div>
  </div> -->
  <div class="blanco">
    <div class="FormCrear">
    </div>
    <div class="FormRegistro ContactoAutos Avisos">
      <fieldset>
        <div class="txt">
          <p>Si querés publicar varios avisos con destaques a precios preferencial, estos paquetes son para vos.</p>
        </div>
        <div class="Row clearfix">
  		  </div>
      </fieldset>
      
      <div class="ContentProd productos">
        <?php
        $grupos = array(); // Armamos una estructura de grupos para recorrerla y no repetir código.
        $grupos['autos']['titulo'] = 'Vehículos';
        $grupos['inmuebles']['titulo'] = 'Inmuebles';
        $grupos['productos']['titulo'] = 'Productos y Servicios';
        $grupos['autos']['paquetes'] = array();
        $grupos['inmuebles']['paquetes'] = array();
        $grupos['productos']['paquetes'] = array(3526592);
        foreach($grupos as $clave => $grupo) {
          print '<div class="select-categoria '.$clave.'"><a href="#">'.$grupo['titulo'].'</a></div>';
          print '<div class="paquete '.$clave.'">';
          reset($form['paquetes']);
          foreach($form['paquetes'] as $key => $valor) {
            if(is_numeric($key)) {
              $mostrar = FALSE;
              // Buscamos por nid los paquetes actuales.
              if(in_array($key, $grupo['paquetes']))
                $mostrar = TRUE;
              if($mostrar) {
                $paquete = node_load($key);
                $titulo_paquete = $form['paquetes'][$key]['#title'];
                $form['paquetes'][$key]['#title'] = '';
                ?>
                <div class="item">
                  <label>
                    <?php print drupal_render($form['paquetes'][$key]); ?>
                    <h3><?php print $titulo_paquete; ?></h3>
                    <div class="precio">$<?php print $paquete->field_paquete_precio[0]['value']; ?></div>
                    <ul>
                      <?php 
                      //Espacios
                      foreach($paquete->field_paqute_espacio as $espacios) {
                        if(!empty($espacios['value'])) {
                          $espacio = node_load($espacios['value']['field_espacio_paquete_espacio'][0]['nid']);
                        ?>
                          <li><?php print $espacios['value']['field_espacio_paquete_cantidad'][0]['value']; if ($espacios['value']['field_espacio_paquete_cantidad'][0]['value'] == 1) print ' crédito no reutilizable '; else print ' créditos no reutilizables '; print $espacio->title; ?></li>
                        <?php
                          } 
                      }
                      ?>
                      <?php 
                      //Creditos
                      foreach($paquete->field_paquete_credito as $creditos) {
                        if($creditos['value']['field_credito_paquete_credito'][0]['nid'] != '') {
                          $credito = node_load($creditos['value']['field_credito_paquete_credito'][0]['nid']);
                        ?>
                          <li><?php print $creditos['value']['field_credito_paquete_cantidad'][0]['value']; if ($creditos['value']['field_credito_paquete_cantidad'][0]['value'] == 1) print ' mejora '; else print ' mejoras '; print $credito->title; ?></li>
                        <?php
                        }
                      }
                      ?>
                      <li>Tiempo de uso: hasta 30 días de realizada la compra.</li>
                      <li><b>Duración: 30<?php //print $paquete->field_paquete_duracion[0]['value']; ?> días publicados.</b></li>
                    </ul>
                  </label>
                </div>
                <input type="hidden" value="<?php print $paquete->field_paquete_precio[0]['value']; ?>" class="precio-<?php print $paquete->nid; ?>" />
                <?php
              }
            }
          }
          //Mensaje SDClass Inmuebles

          print '</div>';
        }
        ?>
      </div>
      
      <div class="cerrarCompra clearfix">
        <fieldset class="detalle-seleccion">
          <div class="Cajapagar">
            <div class="costo">
              <span>Total</span>
              <label>$ 0.00</label>
            </div>
          </div>
        </fieldset>
        <div class="BotonComprar">
          <?php print drupal_render($form['wipe']); ?>
        </div>
      </div>
      <div class="mensaje-factura">Se emitirá comprobante por consumidor final. En el caso de que necesite una factura discriminada, contactarse a  <a href="mailto:clasificadosweb@losandes.com.ar">clasificadosweb@losandes.com.ar</a></div>
    </div>
  </div>
  <div class="form-sin-mostrar" style="display:none;">
  <?php print drupal_render($form); ?>
  </div>
</div>
