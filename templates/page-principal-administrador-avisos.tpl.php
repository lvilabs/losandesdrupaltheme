<?php
global $user;
/*foreach($form['principal']['grafico_consultas']['#value'] as $fecha => $cantidad){
  $labels_grafico1 .= '"'.$fecha.'",';
  $data_grafico1 .= count($cantidad).',';
}*/
global $user;

foreach($form['principal']['grafico_consultas']['#value'] as $fecha => $data_consultas){
  $labels_grafico1 .= '"'.$fecha.'",';
  $count_consulta = 0;
  $count_telefono = 0;
  $count_whatsapp = 0;
  foreach($data_consultas as $data_consulta){
    foreach($data_consulta as $tipo_contador => $id){
      if($tipo_contador == 'consultas')
        $count_consulta = count($id) + $count_consulta;
      if($tipo_contador == 'telefonos')
        $count_telefono = count($id) + $count_telefono;
      if($tipo_contador == 'whatsapp')
        $count_whatsapp = count($id) + $count_whatsapp;
    }
  }
  $data_grafico1 .= $count_consulta.',';
  $data_telefonos_grafico1 .= $count_telefono.',';
  $data_whatsapp_grafico1 .= $count_whatsapp.',';
}
$color_grafico2 = array(0 => '#F7464A', 1 => '#46BFBD', 2 => '#FDB45C', 3 => '#949FB1', 4 => '#4D5360', 5 => '#86B2DB', 6 => '#F7D443', 7 => '#9392C8', 8 => '#2869B5', 9 => '#B070AD');
$highlight_grafico2 = array(0 => '#FF5A5E', 1 => '#5AD3D1', 2 => '#FFC870', 3 => '#A8B3C5', 4 => '#616774', 5 => '#96C3EA', 6 => '#FFE16B', 7 => '#A3A2DB', 8 => '#2E7AD1', 9 => '#C48DC1');
foreach($form['principal']['grafico_visitas']['#value'] as $fecha => $tipo_contador){
  $labels_grafico3 .= '"'.$fecha.'",';
  if(isset($tipo_contador['visitas']))
    $data_visitas_grafico3 .= count($tipo_contador['visitas']).',';
  else 
    $data_visitas_grafico3 .= '0,';
}
?>
<script>
  var lineChartData = {
    labels : ["-",<?php print $labels_grafico1; ?>],
    datasets : [
      {
        fillColor : "rgba(0,138,205,0.2)",
        strokeColor : "rgba(0,138,205,1)",
        pointColor : "rgba(0,138,205,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(0,138,205,1)",
        data : [0,<?php print $data_grafico1; ?>]
      },
			{
				fillColor : "rgba(224,0,24,0.5)",
        strokeColor : "rgba(224,0,24,0.8)",
        pointColor : "rgba(224,0,24,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(224,0,24,1)",
        data : [0,<?php print $data_telefonos_grafico1; ?>]
			},
			{
				fillColor : "rgba(0,204,0,0.5)",
        strokeColor : "rgba(0,204,0,0.8)",
        pointColor : "rgba(0,204,0,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(0,204,0,1)",
        data : [0,<?php print $data_whatsapp_grafico1; ?>]
			}
    ]
  }
  var pieData = [
    <?php $i = 0; foreach($form['principal']['rubros_mas_consultados']['#value'] as $tipo) { 
      $legendPie[] = array('label' => clasificados_rubros_obtener_nombre_type($tipo['type']),
                            'color' => $color_grafico2[$i],
                            'cantidad' => $tipo['total']
                          );
    ?>
    {
      value: <?php print $tipo['total']; ?>,
      color:"<?php print $color_grafico2[$i]; ?>",
      highlight: "<?php print $highlight_grafico2[$i]; ?>",
      label: "<?php print clasificados_rubros_obtener_nombre_type($tipo['type']); ?>"
    },
    <?php $i++; } ?>
  ];
  var barChartData = {
		labels : [<?php print $labels_grafico3; ?>],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php print $data_visitas_grafico3; ?>]
			}
		]
	}
  var pieDataMarcas = [
    <?php $i = 0; foreach($form['principal']['marcas_mas_consultadas']['#value'] as $marca) { 
      $legendPieMarca[] = array('label' => $marca['marca'],
                            'color' => $color_grafico2[$i],
                            'cantidad' => $marca['total']
                          );
    ?>
    {
      value: <?php print $marca['total']; ?>,
      color:"<?php print $color_grafico2[$i]; ?>",
      highlight: "<?php print $highlight_grafico2[$i]; ?>",
      label: "<?php print $marca['marca']; ?>"
    },
    <?php $i++; } ?>
  ];
  var pieDataModelo = [
    <?php $i = 0; foreach($form['principal']['modelos_mas_consultados']['#value'] as $modelo) { 
      $legendPieModelo[] = array('label' => $modelo['modelo'],
                            'color' => $color_grafico2[$i],
                            'cantidad' => $modelo['total']
                          );
    ?>
    {
      value: <?php print $modelo['total']; ?>,
      color:"<?php print $color_grafico2[$i]; ?>",
      highlight: "<?php print $highlight_grafico2[$i]; ?>",
      label: "<?php print $modelo['modelo']; ?>"
    },
    <?php $i++; } ?>
  ];
	window.onload = function(){
		<?php if(!empty($form['principal']['grafico_consultas']['#value'])) { ?>
      var ctx = document.getElementById("canvas").getContext("2d");
      window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: false
      });
    <?php } ?>
    <?php if(!empty($form['principal']['rubros_mas_consultados']['#value'])) { ?>
      var ctx2 = document.getElementById("canvas2").getContext("2d");
      window.myPie = new Chart(ctx2).Pie(pieData);
    <?php } ?>
    <?php if(!empty($form['principal']['grafico_visitas']['#value'])) { ?>
      var ctx3 = document.getElementById("canvas3").getContext("2d");
      window.myBar = new Chart(ctx3).Line(barChartData, {
        responsive : false
      });
    <?php } ?>
    <?php if(!empty($form['principal']['marcas_mas_consultadas']['#value'])) { ?>
      var ctx4 = document.getElementById("canvas4").getContext("2d");
      window.myPie = new Chart(ctx4).Pie(pieDataMarcas);
    <?php } ?>
    <?php if(!empty($form['principal']['modelos_mas_consultados']['#value'])) { ?>
      var ctx5 = document.getElementById("canvas5").getContext("2d");
      window.myPie = new Chart(ctx5).Pie(pieDataModelo);
    <?php } ?>
	}
</script>

<div class="solapa-estadisticas">
  <a class="int" href="/administrar/principal">Estadísticas de Mi Cuenta</a>
<?php if($user->profile_tipo_concesionaria == 'Inmobiliaria'){ ?>  
  <div class="nueva-estadistica"><a class="link-estadisticas-globales" href="/administrar/estadisticas_globales">Estadísticas Globales</a> <span class="flecha-up"></span><span class="nueva-texto">Solo para Inmobiliarias</span></div>
<?php } ?>
<?php if($user->profile_tipo_concesionaria == 'Concesionaria'){ ?>  
  <div class="nueva-estadistica"><a class="link-estadisticas-globales" href="/administrar/estadisticas_globales_autos">Estadísticas Globales</a> <span class="flecha-up"></span><span class="nueva-texto">Solo para Concesionarias</span></div>
<?php } ?>   
</div>

<!--Modulo-->
<!-- <div class="panel-block banner"><?php print $form['principal']['banner-comercial']['#value'];?></div> -->
<div class="Columnas MiCuentaAdmin clearfix">
<div class="CD">
<?php if(!empty($form['principal']['cantidad_consultas_sin_leer']['#value'])) { ?>
<!--Modulo-->
  <div class="panel-block consultasSinLeer conBorde">
    <h2 class="content-cantidad-consultas"><strong class="cantidad-consultas-sin-leer"><?php print $form['principal']['cantidad_consultas_sin_leer']['#value']; ?></strong> <?php ($form['principal']['cantidad_consultas_sin_leer']['#value'] == 1)? print 'Contacto': print 'Contactos'; ?> sin leer</h2>
    <?php
    $link_contactos = '/contactos_recibidos';
    if(in_array('colaborador inmobiliaria', $user->roles)) {
      $link_contactos = '/contactos_recibidos_colaboradores';
    }
    ?>
    <a href="<?php print $link_contactos; ?>" target="_blank" class="botonAccion">Ver más</a>
    <?php if(!empty($form['principal']['consultas_sin_leer']['#value'])) { ?>
    <div class="consulta-loading" style="display:none;"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/ajax-loader.gif" /> Loading...</div>
    <div class="consultas-sin-leer">
      <table class="consultasResumen" width="100%">
        <tr>
          <th>Contacto</th>
          <th>Consulta</th>
          <th>Acciones</th>
        </tr>
        <?php foreach($form['principal']['consultas_sin_leer']['#value'] as $kconsulta => $consulta) { ?>
          <?php if(!empty($consulta->clasificados_contactos_vendedores_contacto_email)) {
              $aviso = node_load($consulta->clasificados_contactos_vendedores_nid, FALSE);
              $fecha = date('d/m/Y', $consulta->clasificados_contactos_vendedores_created);
              $title = trim(drupal_html_to_text($aviso->title));
              $body = trim("Consulta realizada el {$fecha} sobre el Aviso \"{$title}\":\n".drupal_html_to_text($consulta->clasificados_contactos_vendedores_contacto_body));
              $body = "\n\n".str_replace("\r\n", "\n", $body);
              $mail_to = 'mailto:'.$consulta->clasificados_contactos_vendedores_contacto_email.'?subject=Respuesta a tu consulta&body='.rawurlencode(utf8_decode($body));
            }
          ?>
          <tr>
            <td class="contacto"><?php print $consulta->clasificados_contactos_vendedores_contacto_nombre; ?></td>
            <td class="consulta"><?php print $consulta->clasificados_contactos_vendedores_contacto_body; ?></td>
            <td class="acciones"><a href="javascript:void(0);" class="contacto-leido-ajax" id="leido-<?php print $consulta->id_cv; ?>">Marcar como leído</a>
            <?php if(!empty($consulta->clasificados_contactos_vendedores_contacto_email)) { ?>
              <a href="<?php print $mail_to; ?>" class="contacto-respuesta-ajax" id="respuesta-<?php print $consulta->id_cv; ?>">Responder</a>
            <?php } ?>
            </td>
          </tr>
        <?php } ?>
      </table>
    </div>
    <?php } ?>
    <div class="recomendacion"><strong>Recomendamos responder las consultas</strong>
No pierdas oportunidades y respondé lo antes posibles las consultas.</div>
  </div>
<?php } ?>
<!--fin Modulo-->

<!--Modulo-->
  <div class="panel-block conBorde">
    <h2>Estadísticas</h2>
    <?php if(!empty($form['principal']['grafico_consultas']['#value'])) { ?>
      <h3>CONTACTOS Y VISTAS TELÉFONO POR DÍA <span>Últimos 30 días</span></h3>
      <canvas id="canvas" height="300" width="600"></canvas>
      <div id="legendDiv">
        <ul>
          <li><span style="background-color: rgba(0,138,205,0.5);"></span>Contactos</li>
          <li><span style="background-color: rgba(224,0,24,0.5);"></span>Vistas teléfonos</li>
          <li><span style="background-color: rgba(0,204,0,0.5);"></span>Consultas WhatsApp</li>
        </ul>
      </div>
    <?php } ?>
    <?php if(!empty($form['principal']['avisos_mas_vistos']['#value'])) { ?>
      <h3>AVISOS MÁS VISTOS <span>Últimos 7 días</span></h3>
      <div class="clearfix contentAvisos" style="display: block;">
        <?php $count = 0; foreach($form['principal']['avisos_mas_vistos']['#value'] as $aviso) { ?>
          <div class="CajaAviso new <?php if($count == 3) print 'last'; ?>">
            <div class="imgAviso">
              <a href="<?php print 'https://clasificados.losandes.com.ar/' . $aviso['url']; ?>" target="_blank">
                <img src="/<?php print $aviso['img']; ?>" alt="" title="" class="imagecache imagecache-ficha_aviso_mobile_220_148" width="220" height="148">
              </a>
            </div>
            <div class="AvisoDescripcion">
              <h4><a href="<?php print 'https://clasificados.losandes.com.ar/' . $aviso['url']; ?>" target="_blank"><?php print $aviso['title']; ?></a></h4>
              <p>  </p>
              <span class="numero"><?php print $aviso['total']; ?> <?php ($aviso['total'] == 1)? print 'visita': print 'visitas'; ?></span>
            </div>
          </div>
        <?php $count++; } ?>
      </div>
    <?php } ?>
    <?php if(!empty($form['principal']['avisos_mas_consultados']['#value'])) { ?>
      <h3>AVISOS MÁS CONTACTADOS <span>Últimos 15 días</span></h3>
      <div class="clearfix contentAvisos" style="display: block;">
        <?php $count = 0; foreach($form['principal']['avisos_mas_consultados']['#value'] as $aviso) { ?>
          <div class="CajaAviso new <?php if($count == 3) print 'last'; ?>">
            <div class="imgAviso">
              <a href="<?php print 'https://clasificados.losandes.com.ar/' . $aviso['url']; ?>" target="_blank">
                <img src="/<?php print $aviso['img']; ?>" alt="" title="" class="imagecache imagecache-ficha_aviso_mobile_220_148" width="220" height="148">
              </a>
            </div>
            <div class="AvisoDescripcion">
              <h4><a href="<?php print 'https://clasificados.losandes.com.ar/' . $aviso['url']; ?>" target="_blank"><?php print $aviso['title']; ?></a></h4>
              <p>  </p>
              <span class="numero"><?php print $aviso['total']; ?> <?php ($aviso['total'] == 1)? print 'contacto': print 'contactos'; ?></span>
            </div>
          </div>
        <?php $count++; } ?>
      </div>
    <?php } ?>
    <?php if(!empty($form['principal']['grafico_visitas']['#value'])) { ?>
      <h3>VISITAS POR DÍA <span>Últimos 30 días</span></h3>
      <canvas id="canvas3" height="300" width="600"></canvas>
    <?php } ?>
    <?php if(!empty($form['principal']['rubros_mas_consultados']['#value'])) { ?>
      <div class="grafico-torta <?php (!empty($form['principal']['marcas_mas_consultadas']['#value']))? print 'left': print ''; ?>">
        <h3>RUBROS MÁS CONTACTADOS <br /><span>Últimos 3 meses</span></h3>
        <canvas id="canvas2" height="200" width="320"></canvas>
        <div id="legendDiv">
          <ul>
          <?php foreach($legendPie as $legend) { ?>
            <li><span style="background-color:<?php print $legend['color']; ?>;"></span><?php print $legend['label']; ?> - <?php print $legend['cantidad']; ?></li>
          <?php } ?>
          </ul>
        </div>
      </div>
    <?php } ?>
    <?php if(!empty($form['principal']['marcas_mas_consultadas']['#value'])) { ?>
      <div class="grafico-torta right">
        <h3>MARCAS MÁS CONTACTADAS <br /><span>Últimos 3 meses</span></h3>
        <canvas id="canvas4" height="200" width="320"></canvas>
        <div id="legendDiv">
          <ul>
          <?php foreach($legendPieMarca as $legend) { ?>
            <li><span style="background-color:<?php print $legend['color']; ?>;"></span><?php print $legend['label']; ?> - <?php print $legend['cantidad']; ?></li>
          <?php } ?>
          </ul>
        </div>
      </div>
    <?php } ?>
    <?php if(!empty($form['principal']['modelos_mas_consultados']['#value'])) { ?>
      <div class="grafico-torta">
        <h3>MODELOS MÁS CONTACTADOS <br /><span>Últimos 3 meses</span></h3>
        <canvas id="canvas5" height="200" width="320"></canvas>
        <div id="legendDiv">
          <ul>
          <?php foreach($legendPieModelo as $legend) { ?>
            <li><span style="background-color:<?php print $legend['color']; ?>;"></span><?php print $legend['label']; ?> - <?php print $legend['cantidad']; ?></li>
          <?php } ?>
          </ul>
        </div>
      </div>
    <?php } ?>
  </div>
<!--fin Modulo-->
</div>
<div class="CR">
<!--Modulo-->
  <div class="panel-block conBorde">
    <h2>Estado de cuenta</h2>
    <div class="datos">
      <ul>
        <?php if(!empty($form['principal']['avisos_publicados']['#value'])) { ?>
        <li class="clearfix"><strong><?php print $form['principal']['avisos_publicados']['#value']->avisos;?></strong><span><?php ($form['principal']['avisos_publicados']['#value']->avisos == 1)? print 'aviso<br />publicado': print 'avisos<br />publicados'; ?></span></li>
        <?php } ?>
        <?php if(!empty($form['principal']['vistas']['#value'])) { ?>
          <li class="clearfix"><strong><?php print $form['principal']['vistas']['#value']['vistas_telefono'];?></strong><span><?php ($form['principal']['vistas']['#value']['vistas_telefono'] == 1)? print 'vista': print 'vistas'; ?><br />teléfono</span><span class="cant-dias">Últimos 15 días</span></li>
          <li class="clearfix"><strong><?php print $form['principal']['vistas']['#value']['vistas_whatsapp'];?></strong><span><?php ($form['principal']['vistas']['#value']['vistas_whatsapp'] == 1)? print 'consulta': print 'consultas'; ?><br />WhatsApp</span><span class="cant-dias">Últimos 15 días</span></li>
          <li class="clearfix"><strong><?php print $form['principal']['vistas']['#value']['vistas_mail_sms'];?></strong><span><?php ($form['principal']['vistas']['#value']['vistas_mail_sms'] == 1)? print 'consulta': print 'consulta'; ?><br />e-mail + SMS</span><span class="cant-dias">Últimos 15 días</span></li>
          <li class="clearfix"><strong><?php print $form['principal']['vistas']['#value']['visitas_totales'];?></strong><span><?php ($form['principal']['vistas']['#value']['visitas_totales'] == 1)? print 'visita': print 'visitas'; ?></span><span class="cant-dias">Últimos 15 días</span></li>
        <?php } ?>
      </ul>
    </div>
    <?php if(!empty($form['principal']['espacios_totales']['#value'])) { ?>
      <h3 class="destaques">DESTAQUES</h3>
      <table class="consultasResumen Destaques" width="100%">
        <tr>
          <th>Espacio</th>
          <th>Libres</th>
          <th>Usados</th>
          <th>Total</th>
        </tr>
        <?php foreach($form['principal']['espacios_totales']['#value'] as $kespacio => $vespacio) { ?>
        <tr>
          <td class="tipo"><?php print $kespacio; ?></td>
          <td><?php print $vespacio['espacios_libres']; ?></td>
          <td><?php print $vespacio['espacios_usados']; ?></td>
          <td><strong><?php print $vespacio['espacios_totales']; ?></strong></td>
        </tr>
        <?php } ?>
       </table>
    <?php } ?>
    <div class="recomendacion"><strong>Recomendación</strong> para aprovechar todo el potencial de Clasificados Los Andes sugerimos ir cambiando los anuncios y renovando la oferta</div>
  </div>
<!--fin Modulo-->
<!--Modulo-->
<?php if(isset($form['principal']['link-seguros'])) { ?>
  <div class="panel-block promoWidget conBorde">
    <h2>Seguros y créditos</h2>
    <div class="clearfix">
      <p>Obtené beneficios vendiendo la línea de seguros y créditos. Disponible para todas las agencias</p>
      <div class="icono"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/fd_seguros.png"  /></div>
    </div>
    <a href="<?php print $form['principal']['link-seguros']['#value'];?>" class="botonAccion" target="_blank">Más info</a>
  </div>
<?php } ?>
<!--fin Modulo-->  
<!--Modulo-->
<?php if(!empty($form['principal']['tips_consejos']['#value'])) { ?>
  <div class="panel-block listadoLinks conBorde">
    <h2>Tips y consejos</h2>
    <ul>
      <?php foreach($form['principal']['tips_consejos']['#value'] as $nota) { ?>
        <li><a href="<?php print 'https://clasificados.losandes.com.ar/faq'; ?>" target="_blank"><?php print $nota['titulo']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
<?php } ?>
<!--Modulo-->
<!--Modulo-->
<?php if(isset($form['principal']['exportacion_consultas_excel'])) { ?>
  <div class="panel-block promoWidget conBorde">
    <h2>Creá tu base de datos</h2>
    <div class="clearfix">
      <p>Exportá en Excel todas las consultas que te hiceron.</p>
      <div class="icono"><img src="/sites/clasificadoslavoz.com.ar/themes/principal/img/logo-excel.jpg"  /></div>
    </div>
    <a href="<?php print $form['principal']['exportacion_consultas_excel']['#value'];?>" class="botonAccion">Exportar</a>
  </div>
<?php } ?>
<!--fin Modulo-->
</div>
</div>