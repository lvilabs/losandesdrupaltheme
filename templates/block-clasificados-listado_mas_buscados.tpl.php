<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <h3>Más Buscados</h3>
  <div>
    <h4>Inmuebles</h4>
    <div class="navigation">
      <ul>
        <?php foreach($content['mas_vistos_inmuebles'] as $link) { ?>
          <li><a href="<?php print $link['link']; ?>" title="<?php print $link['texto']; ?>"><?php print str_replace('departamentos', 'deptos', $link['texto']); ?></a></li>
        <?php } ?>
        <li class="destacados-ver-mas"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6330" title="Ver mas avisos de inmuebles">Ver más</a></li>
      </ul>
    </div>
    <h4>Autos</h4>
    <div class="navigation">
      <ul>
        <?php foreach($content['mas_vistos_autos'] as $link) { ?>
          <li><a href="<?php print $link['link']; ?>" title="<?php print $link['texto']; ?>"><?php print $link['texto']; ?></a></li>
        <?php } ?>
        <li class="destacados-ver-mas"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6323" title="Ver mas avisos de autos">Ver más</a></li>
      </ul>
    </div>
    <h4>Empleos</h4>
    <div class="navigation">
      <ul>
        <?php foreach($content['mas_vistos_empleos'] as $link) { ?>
          <li><a href="http://empleos.clasificadoslavoz.com.ar/buscar" title="<?php print $link['texto']; ?>"><?php print $link['texto']; ?></a></li>
        <?php } ?>
        <li class="destacados-ver-mas"><a href="http://empleos.clasificadoslavoz.com.ar/buscar" title="Ver mas avisos de empleos">Ver más</a></li>
      </ul>
    </div>
    <h4>Productos</h4>
    <div class="navigation">
      <ul>
        <?php foreach($content['mas_vistos_productos'] as $link) { ?>
          <li><a href="<?php print $link['link']; ?>" title="<?php print $link['texto']; ?>"><?php print $link['texto']; ?></a></li>
        <?php } ?>
        <li class="destacados-ver-mas"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6106" title="Ver mas avisos de productos">Ver más</a></li>
      </ul>
    </div>
    <h4>Servicios</h4>
    <div class="navigation">
      <ul>
        <?php foreach($content['mas_vistos_servicios'] as $link) { ?>
          <li><a href="<?php print $link['link']; ?>" title="<?php print $link['texto']; ?>"><?php print $link['texto']; ?></a></li>
        <?php } ?>
        <li class="destacados-ver-mas"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:6017" title="Ver mas avisos de servicios">Ver más</a></li>
      </ul>
    </div>
  </div>
</div>