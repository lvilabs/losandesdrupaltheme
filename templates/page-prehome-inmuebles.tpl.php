<?php
/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It should be placed within the <body> tag. When selecting through CSS
 *   it's recommended that you use the body tag, e.g., "body.front". It can be
 *   manipulated through the variable $classes_array from preprocess functions.
 *   The default values can be one or more of the following:
 *   - front: Page is the home page.
 *   - not-front: Page is not the home page.
 *   - logged-in: The current viewer is logged in.
 *   - not-logged-in: The current viewer is not logged in.
 *   - node-type-[node type]: When viewing a single node, the type of that node.
 *     For example, if the node is a "Blog entry" it would result in "node-type-blog".
 *     Note that the machine name will often be in a short form of the human readable label.
 *   - page-views: Page content is generated from Views. Note: a Views block
 *     will not cause this class to appear.
 *   - page-panels: Page content is generated from Panels. Note: a Panels block
 *     will not cause this class to appear.
 *   The following only apply with the default 'sidebar_first' and 'sidebar_second' block regions:
 *     - two-sidebars: When both sidebars have content.
 *     - no-sidebars: When no sidebar content exists.
 *     - one-sidebar and sidebar-first or sidebar-second: A combination of the
 *       two classes when only one of the two sidebars have content.
 * - $node: Full node object. Contains data that may not be safe. This is only
 *   available if the current page is on the node's primary url.
 * - $menu_item: (array) A page's menu item. This is only available if the
 *   current page is in the menu.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing the Primary menu links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title: The page title, for use in the actual HTML content.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $help: Dynamic help text, mostly for admin pages.
 * - $content: The main content of the current page.
 * - $feed_icons: A string of all feed icons for the current page.
 *
 * Footer/closing data:
 * - $footer_message: The footer message as defined in the admin settings.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * Regions:
 * - $content_top: Items to appear above the main content of the current page.
 * - $content_bottom: Items to appear below the main content of the current page.
 * - $navigation: Items for the navigation bar.
 * - $sidebar_first: Items for the first sidebar.
 * - $sidebar_second: Items for the second sidebar.
 * - $header: Items for the header region.
 * - $footer: Items for the footer region.
 * - $page_closure: Items to appear below the footer.
 *
 * The following variables are deprecated and will be removed in Drupal 7:
 * - $body_classes: This variable has been renamed $classes in Drupal 7.
 *
 * AUTOS LA VOZ
 * - $es_ficha: Determina si se esta viendo o no (TRUE / FALSE) la ficha de un aviso.
 * 
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess()
 * @see zen_process()
 */

$is_search = in_array(arg(0), array('search', 'taxonomy')); //taxonomy = category
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" xmlns:fb="http://ogp.me/ns/fb#">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php print $scripts; ?>
  <?php include(dirname(__FILE__).'/includes/dfp-google.php'); ?>
</head>
<body class="<?php print $classes; ?>">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>

<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

  <div class="Header SinLoguear">
    <?php include dirname(__FILE__).'/includes/header.tpl.php'; ?>
  </div> 

<?php if($page_arriba): ?>
  <div class="Columnas page-arriba clearfix">
    <?php print $page_arriba; ?>
  </div>
<?php endif; ?>
  
<div class="Contenido">
  
  <?php 
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_anuncio_previo';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);

    $module = 'clasificados_banners';
    $delta = 'dfp_banner_topsite';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
    
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_lateral_1';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
    
    $module = 'clasificados_banners';
    $delta = 'dfp_banner_zocalo';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
  ?>
  
  <div class="GradientDestacado">
    <div class="SombraDestacado">
      <div class="FondoAutoDestacado">
        <div class="CT clearfix">
          <div class="CR">

<!-- VENDE AHORA start -->
            <hr class="hr-inmobiliarias" style="border:none;">
            <div class="link-inmobiliarias" style="padding:50px 0;">
              <div class="Content clearfix" >
                <a title="Buscador de inmobiliarias" href="/inmobiliarias" rel="search">Buscador de Inmobiliarias</a>
              </div>
            </div>
<!-- VENDE AHORA end -->

          </div>
          <div class="CD CI">
            <div class="CL">
              <div class="Buscador">
                <h2 title="Qué buscás"><strong>¿Qué buscás?</strong></h2>

<!-- BUSCADOR start -->
<?php
  //http://drupal.org/node/164799#comment-1179045
  $module = 'autoslavoz';
  $delta = 'inmuebles_buscador_home';
  $block = (object) module_invoke($module, 'block', 'view', $delta);
  $block->module = $module;
  $block->delta = $delta;
  print theme('block', $block);
?>
<!-- BUSCADOR end -->

              </div>
            </div>
            <div class="CM">
<?php
  //Busco rubros de avisos
  $array_rubros = clasificados_rubros_obtener_rubros('aviso-casa');
  foreach($array_rubros as $rubro){
    if($rubro->title == 'Casas')
      $rubro_casa = $rubro->tid;
    if($rubro->title == 'Departamentos')
      $rubro_depto = $rubro->tid;
    if($rubro->title == 'Terrenos y Lotes')
      $rubro_lote = $rubro->tid;
    if($rubro->title == 'Emprendimientos')
      $rubro_emprendimiento = $rubro->tid;
    if($rubro->title == 'Locales')
      $rubro_local = $rubro->tid;
    if($rubro->title == 'Oficinas')
      $rubro_oficina = $rubro->tid;
  }
  $rubro_padre = current(taxonomy_get_parents($rubro_casa));
?>
              <div class="BuscadorSegmento">
                <h2 title="B&uacute;squeda guiada"><strong>Búsqueda guiada</strong></h2>
                <ul class="ContentSegmento">
                  <li class="casas">
                    <div class="Icono"><a href="search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_casa; ?>" title="Buscar casas" rel="search"></a></div>
                    <p>Casas</p>
                    </li>
                  <li class="depto">
                    <div class="Icono"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_depto; ?>" title="Buscar departamentos" rel="search"></a></div>
                    <p>Departamentos</p>
                    </li>
                  <li class="lotes">
                    <div class="Icono"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_lote; ?>" title="Buscar lotes" rel="search"></a></div>
                    <p>Lotes</p>
                  </li>
                  <li class="emprendimientos">
                    <div class="Icono"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_emprendimiento; ?>" title="Buscar emprendimientos" rel="search"></a></div>
                    <p>Emprendimientos</p>
                  </li>
                  <li class="locales">
                    <div class="Icono"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_local; ?>" title="Buscar locales" rel="search"></a></div>
                    <p>Locales</p>
                  </li>
                  <li class="oficinas">
                    <div class="Icono"><a href="/search/apachesolr_search/?f[0]=im_taxonomy_vid_34:<?php print $rubro_padre->tid; ?>&f[1]=im_taxonomy_vid_34:<?php print $rubro_oficina; ?>" title="Buscar oficinas" rel="search"></a></div>
                    <p>Oficinas</p>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!--FIN Destacado Top-->
  
  <?php
    $module = 'views';
    $delta = 'notas-block_1';
    $block = (object) module_invoke($module, 'block', 'view', $delta);
    $block->module = $module;
    $block->delta = $delta;
    print theme('block', $block);
  ?>
  
  <div class="Columnas clearfix">
    <div class="CD AvisoInmobBread">

    </div>

  </div>
  
  <div class="Columnas clearfix">

    <div class="CD">
      
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_middle_1';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      
      <?php print $page_media; ?>
      
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_middle_2';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      
      <div id="content">
        <div id="system-messages-wrapper">
          <div id="system-messages">
        <?php print $messages; ?>
          </div>
          <a class="system-messages-inline" href="#system-messages" title="Mensaje de sistema" rel="noindex">Mensaje de sistema</a>
        </div>
        
        <?php if ($title): ?>
          <!-- <?php print $title; ?> -->
        <?php endif; ?>
          
        <div id="content-area">
          <?php print $content; ?>
        </div>
      </div>

      <?php print $page_inferior; ?>      
      
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_middle_3';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      
      <?php print $page_footer; ?> 
      
    </div>

<?php if(!$is_search): ?>
    <div class="CR">
      
      <?php 
        $module = 'clasificados_banners';
        $delta = 'dfp_banner_rectangle_1';
        $block = (object) module_invoke($module, 'block', 'view', $delta);
        $block->module = $module;
        $block->delta = $delta;
        print theme('block', $block);
      ?>
      
      <?php print $page_columna_derecha; ?>
    </div>
<?php endif; ?>

  </div>
</div>

  <?php include dirname(__FILE__).'/includes/footer.tpl.php'; ?>
  
  <?php print $closure; ?>

</body>
</html>