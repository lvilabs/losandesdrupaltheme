<p><strong>Hola! Un nuevo cliente está interesado por los Sitios:</strong></p>
<p>Nombre y Apellido del cliente: <strong>!sitio_contacto_nombre !sitio_contacto_apellido</strong></p>
<p>Teléfono principal: <strong>!sitio_contacto_telefono1</strong></p>
<p>Teléfono secundario: <strong>!sitio_contacto_telefono2</strong></p>
<p>Correo: <strong>!sitio_contacto_mail</strong></p>
<p>Tipo de cliente: <strong>!sitio_contacto_tipo_cliente</strong></p>
<p>Dominio: <strong>!sitio_contacto_dominio</strong></p>
<p>Template: <strong>!sitio_contacto_template</strong></p>
<p>Producto de interés: <strong>!sitio_contacto_opcion</strong></p>
