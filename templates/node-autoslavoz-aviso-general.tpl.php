<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $submitted: Themed submission information output from
 *   theme_node_submitted().
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $build_mode: Build mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $build_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * The following variable is deprecated and will be removed in Drupal 7:
 * - $picture: This variable has been renamed $user_picture in Drupal 7.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess()
 * @see zen_preprocess_node()
 * @see zen_process()
 */
  global $base_root;
  
  $mensajes = drupal_get_messages('status');
  if (!empty($mensajes['status'])){
   foreach($mensajes['status'] as $mensaje) {
      if (ereg('se ha creado.', $mensaje))  unset($mensaje);
      drupal_set_message($mensaje);
    }
  }
  
  if($teaser) {
    include dirname(__FILE__).'/includes/node-aviso-general-teaser.tpl.php';
    return;
  } else {
    //Si el aviso se encuntra pendiente o rechazado, no muestro su contenido
    if(isset($visual_aviso->no_accesible) && $visual_aviso->no_accesible){
      unset($node->content);
      //Si es el usuario creador, muestro mensaje con estado del aviso.
      if($user->uid == $node->uid){
        if($node->_workflow == WORKFLOW_AVISOS_ESTADO_RECHAZADO)
          drupal_set_message('Tu aviso ha sido rechazado.', 'error');
        elseif($node->_workflow == WORKFLOW_AVISOS_ESTADO_PENDIENTE)
          drupal_set_message('Tu aviso está siendo moderado.', 'status');
      }
      drupal_access_denied();
      exit();
    }
  }
  
  //Si es el usuario creador y es la primera vez que se ingresa al aviso, muestro el modal share
  $visitas = clasificados_contador_get_cantidad_nodo_statistics($node->nid);
  if($user->uid == $node->uid && $visitas >= 0 && $visitas <= 2){
    $url_aviso = $base_root.request_uri();
    _administracion_avisos_modal_share($url_aviso);
  }
  
  $preset_imagecache = 'ficha_aviso_628_418_sc'; // Es diferente el de auto e inmuebles que el resto.
  if(in_array($node->type, array('aviso_producto', 'aviso_servicio', 'aviso_rural')))
    $preset_imagecache = 'ficha_aviso_498_420_sc';
    
  //Configuracion y carga de Mapa
  if(isset($visual_aviso->multimedia['mapas']) || $node->disponible_venta==1) {
    
    //drupal_set_html_head('<script src="'. check_url(url('http://maps.google.com/maps/api/js?key='.variable_get('googlemap_api_key', ''))) .'" type="text/javascript"></script>');
    
    //drupal_set_html_head('<link type="text/css" rel="stylesheet" media="all" href="/sites/all/libraries/openLayer/ol.css">');
    //drupal_set_html_head('<script src="/sites/all/libraries/openLayer/ol.js" type="text/javascript"></script>');
    
    drupal_set_html_head('<script src="http://www.openlayers.org/api/OpenLayers.js"></script>');
    
  }
  if(isset($visual_aviso->multimedia['mapas'])) {
    $script = <<<SCRIPT_JS
var Clasificados = Clasificados || {};
Clasificados.GmapSetingsLatitude = {$visual_aviso->multimedia['mapas']['items'][0]['latitude']};
Clasificados.GmapSetingsLongitude = {$visual_aviso->multimedia['mapas']['items'][0]['longitude']};
Clasificados.GmapSetingsMarkername = '/sites/all/modules/contrib/gmap/markers/blue.png';
Clasificados.GmapSetingsZoom = 15;
SCRIPT_JS;
    /*
    if(!isset($visual_aviso->multimedia['fotos']) && !(arg(0)=='node' && arg(1)==AUTOSLAVOZ_PAGE_IMPRIMIR_NID)){;
      $script .= <<<SCRIPT_JS
$(document).ready(WebPage.GetMapaFicha);
SCRIPT_JS;
    }
    */
    drupal_add_js($script, 'inline', 'header');
    
  }
?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
<?php foreach ($visual_aviso->multimedia as $mkey => $media): ?>
<?php   if($media['type'] != 'image' || $media['quantity'] < 1 || !$visual_aviso->esta_publicado) continue; //skip slider because isn't a media image o si solo tiene una imagen ?>
<?php   foreach($media['items'] as $ikey => $foto): ?>
    WebPage.FotosMedium['<?php print $mkey.'_'.$ikey; ?>'] = '<a href="#" rel="popupImagen" class="poplight" data-media-content-id="<?php print 'multimedia_'.$mkey; ?>" data-foto-key="<?php print $mkey.'_'.$ikey; ?>" rel="noindex"><span></span><?php print theme('imagecache', $preset_imagecache, $foto['filepath'], $title.' #'.($ikey+1), $title.' #'.($ikey+1), NULL, FALSE); ?></a>';
<?php   endforeach; ?>
<?php endforeach; ?>
//--><!]]>
</script>

<!-- FichaAuto -->
<?php if(! $visual_aviso->esta_publicado): ?>
<div class="finalizado">
  <div class="leyenda">AVISO FINALIZADO</div>
  <div class="links">
    <span>
<?php   if(count($visual_aviso->links_ver_mas) > 0): ?>
      Buscá más
<?php     foreach ($visual_aviso->links_ver_mas as $k => $ver_mas): ?>
      <?php print l($ver_mas['value'], $ver_mas['link_settings']['path'], $ver_mas['link_settings']['options']); ?>
<?php       if (($k+1) < count($visual_aviso->links_ver_mas)): ?>
      o
<?php       endif; ?>
<?php     endforeach; ?>
<?php   endif; ?>
      &nbsp;
    </span>
  </div>
</div>
<?php endif; ?>

<div class="Content FichaAuto <?php if($node->disponible_venta) print 'FichaTienda'; ?> clearfix">
  <div class="Marco clearfix">
    
<!-- Multimedia -->            
  <div class="Multimedia">
<?php
  reset($visual_aviso->multimedia);
  $first_multimedia_key = key($visual_aviso->multimedia);
?>
<?php if(count($visual_aviso->multimedia) > 1): ?>
    <ul class="Solapas clearfix">
<?php $count = 0; ?>
<?php   foreach ($visual_aviso->multimedia as $key => $media): ?>
<?php     $count++; ?>
      <li data-media-content-id="<?php print 'multimedia_'.$key; ?>" data-media-content-type="<?php print $media['type']; ?>"<?php if($count == 1) print ' class="'.$key.' Act"'; else print 'class="'.$key.'"'; ?>>
        <div class="Cv Tl"></div>
        <div class="Cv Tr"></div>
        <a rel="noindex" title="<?php print htmlentities($media['label'], ENT_QUOTES, 'UTF-8'); ?>" href="javascript:;"><?php print htmlentities($media['label'], ENT_QUOTES, 'UTF-8'); ?><span> (<?php print $media['quantity']; ?>)</span></a>
      </li>
<?php   endforeach; ?>
    </ul>
<?php endif; ?>
    <div class="PlayerMultimedia">
      <div class="PM">
          <div class="Display">
            <div class="Holder">
              <div class="Nota Video" style="display: block;">
                <div class="Inner clearfix">
<?php foreach ($visual_aviso->multimedia as $key => $media): ?>
                  <?php 
                    $style = '';
                    if($key != $first_multimedia_key){ 
                      $style .= ' style="display:none;'; 
                      if($media['type'] == 'mapa') 
                        $style .= 'height:487px;"'; 
                      else 
                        $style .= '"';
                    } elseif($media['type'] == 'mapa'){ 
                      $style .= ' style="height:487px;"'; 
                    }?>
                  <div id="<?php print 'multimedia_'.$key; ?>" class="Video" <?php print $style;?>>
<?php   $first_media_item = reset($media['items']); ?>
<?php   if($media['type'] == 'image' &&  $media['quantity'] > 0 && $visual_aviso->esta_publicado): ?>
                    <a href="#" rel="popupImagen" class="poplight" data-foto-key="<?php print $key.'_'.key($media['items']); ?>" title="Carrucel de imágenes" rel="noindex"><span></span><?php print theme('imagecache', $preset_imagecache, $first_media_item['filepath'], $node->title, $node->title, array('itemprop' => 'image', 'class' => 'imagecache imagecache-'.$preset_imagecache), FALSE); ?></a>
<?php   elseif($media['type'] == 'image'): ?>
                    <a><span></span><?php print theme('imagecache', $preset_imagecache, $first_media_item['filepath'], $node->title, $node->title, array('itemprop' => 'image', 'class' => 'imagecache imagecache-'.$preset_imagecache), FALSE); ?></a>
<?php   elseif($media['type'] == 'video'): ?>
<?php     if(in_array($node->type, array('aviso_producto', 'aviso_servicio', 'aviso_rural'))) {
            $first_media_item['view'] = str_replace('width="628"', 'width="498"', $first_media_item['view']);
            $first_media_item['view'] = str_replace('height="418"', 'height="332"', $first_media_item['view']);
          }
          print $first_media_item['view'];
?>
<?php   elseif($media['type'] == 'flash'): ?>
                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="628" height="478">
                      <param name="movie" value="<?php print url($first_media_item['filepath'], array('absolute' => TRUE)); ?>" />
                      <param name="quality" value="high" />
                      <embed src="<?php print url($first_media_item['filepath'], array('absolute' => TRUE)); ?>" width="628" height="478" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>
                    </object>
<?php   endif; ?>
                  </div>
<?php endforeach; ?>
                </div>
              </div>
            </div>
          </div> <!--fin display-->
          
<?php foreach ($visual_aviso->multimedia as $mkey => $media): ?>
<?php   if($media['type'] != 'image' || $media['quantity'] <= 1) { continue; } //skip slider because isn't a media image o si solo tiene una imagen ?>
<?php   if(isset($media['items'][0]['default'])) { continue; } //default image means no images ?>
        <div class="accessible_news_slider" data-media-content-id="<?php print 'multimedia_'.$mkey; ?>"<?php if($mkey != $first_multimedia_key) print ' style="display:none;"'; else print ' style="display:block"'; ?>>
          <div class="Botones">
            <p class="back"><span class="slide-button"><span>Anterior</span></span></p>
            <p class="next"><span class="slide-button"><span>Siguiente</span></span></p>
          </div>
          
          <div class="Listado no-print">
            <ul class="Slider">
<?php   foreach($media['items'] as $ikey => $foto): ?>
              <li class="Foto"><a class="linkFotoChica" data-media-content-id="<?php print 'multimedia_'.$mkey; ?>" data-foto-key="<?php print $mkey.'_'.$ikey; ?>" rel="noindex"> <span></span><?php print theme('imagecache', 'ficha_aviso_76_57_sc', $foto['filepath'], $title.' #'.($ikey+1), $title.' #'.($ikey+1)); ?></a></li>
<?php   endforeach; ?>
            </ul>
          </div>
        </div>
<?php endforeach; ?>
      </div>
    </div>
  </div><!-- FIN Multimedia -->
  
<!-- Datos Aviso Producto -->  
<?php 
  if($node->disponible_venta && $node->type=='aviso_producto') : 
    include_once dirname(__FILE__).'/block-clasificados-datos_aviso.tpl.php';
    $block_contacto = module_invoke('contactar_vendedor', 'block', 'view', "formulario_contacto"); 
    print '<div class="DN"><div id="modal-contacto-vendedor"><div id="block-contactar_vendedor-formulario_contacto">'.$block_contacto['content'].'</div></div></div>';
  endif;
?>
<!-- Fin Datos Aviso Producto -->  

<!-- Nuevo Colorbox -->
<?php if(isset($node->content['recomendar_amigo_form'])): ?>
    <div class="clearfix no-print" style="display:none;">
      <div id="recomendar-amigo-form">
      <?php print recomendar_amigo_block_formulario_ajax(); ?>
      </div>
    </div>
<?php endif; ?>
<?php if(isset($node->content['reportar_abuso_form'])): ?>
    <div class="clearfix no-print" style="display:none;">
      <div id="reportar-abuso-form">
      <?php print abuso_block_formulario_ajax(); ?>
      </div>
    </div>
<?php endif; ?>
<?php if($node->disponible_venta) : ?>
    <div class="clearfix no-print" style="display:none;">
      <div id="calculadora-cuotas-form" class="calculadora">
        <form class="paymethod-selection new-paymethod-selection">
          <fieldset>
            <h4>Calcular las cuotas</h4>
            <div class="metodos-pagos-div">
              <div class="tarjetas-div clearfix">
                <span class="subtitulo">Tarjetas de crédito y otros medios:</span>
                <div class="contenedor clearfix">
                  <ol class="tarjetas"></ol>
                  <ol class="otros"></ol>
                </div>
                <div class="bancos-div DN">
                  <span class="subtitulo">Banco:</span>
                  <select name="bancos" class="bancos" id="bancos-select">
                    <option value="">Elegir</option>
                  </select>
                </div>
                <div class="cuotas-div DN">
                  <span class="subtitulo">Cuotas:</span>
                  <select name="cuotas" class="cuotas"></select>
                </div>
                <span class="cargando"></span>
              </div>
            </div>
            
            <div class="cuotas final DN" id="monto-cuotas-div" >
              <div class="contado">
                <div class="label-contado">Precio Contado</div>
                <div class="label-pagos">Efectivo, tarjeta de débito o tarjeta de crédito en 1 pago</div>
                <div class="precio-contado"></div>
              </div>
              <div class="tarjeta">
                <div class="label-cuotas">
                  <div class="cantidad-cuota"></div> 
                  <div class="costo-cuota"></div>
                </div>
                <div class="label-ptf"></div>
                <div class="label-cft"></div>
                <div class="label-costo">Costo financiero total</div>
              </div>
            </div>
            
            <!--
            <div id="monto-cuotas-div" class="final /*DN*/">
              <span>¡Pagas </span>
              <span id="cant_cuotas" class="cant_cuotas destacado"></span>
              <span class="destacado"> cuotas </span>
              <span> de </span>
              <span id="monto_cuota" class="monto destacado"></span>
              <span id="interes_cuota" class="interes_cuota"></span><span>!</span>
            </div>
            <div id="monto-total-div" class="final /*DN*/">
              <span>¡Pagas </span>
              <span class="monto destacado"></span><span>!</span>
            </div>
            -->
          </fieldset>
          <div class="acciones clearfix">
            <a href="https://www.mercadopago.com/mla/credit_card_promos.html" title="Promociones Mercado Pago" target="blank" rel="follow">Ver promociones y descuentos exclusivos</a>
            <p>
              <input type="button" class="boton" value="Aceptar" id="seleccion_forma_pago">
            </p>
          </div>
        </form>
      </div>
    </div>
<?php endif; ?>
  </div>
    
    <div class="Info Marco">
<?php if($visual_aviso->esta_publicado): ?>
      <h4 class="Titulo">Descripción</h4>
      <div id="aviso-descripcion" itemprop="description"><?php print $node->content['body']['#value']; ?></div>      
<?php endif; ?>
<?php if(!$node->disponible_venta) : ?>
      <div class="EspecificacionesA">
        <div class="Modelo">
          <!-- cxenseparse_start -->
            <ul>
  <?php foreach ($visual_aviso->caracteristicas_a AS $key => $item): 
          $microdata = '';
          switch($item['label']){
            case 'Estado':
              $estado = ($item['value'] == 'Nuevo') ? 'NewCondition' : 'UsedCondition' ;
              $microdata = 'itemprop="itemCondition" content="'.$estado.'"';
              break;
          }
  ?>
              <li class="<?php if($key % 2 == 0) print 'stripe'; ?>"><span class="field"><?php print empty($item['label']) ? '&nbsp;' : htmlentities($item['label'], ENT_QUOTES, 'utf-8').':'; ?></span> <span <?php print $microdata; ?> class="caract_direccion"><?php print isset($item['link_settings']) ? l($item['value'], $item['link_settings']['path'], $item['link_settings']['options']) : $item['value']; ?></span></li>
  <?php endforeach; ?>
            </ul>
          <!-- cxenseparse_end -->
        </div>
      </div>
<?php endif; ?>

      <!-- cxenseparse_start -->
      <div class="Marco">
<?php $count_group = 0; ?>
<?php $div_is_open = FALSE; ?>
<?php foreach ($visual_aviso->caracteristicas_b AS $item_group): ?>
<?php   if(count($item_group['items']) == 0) { continue; } ?>
          <div class="FeaturesList clearfix">
            <h4 class="Titulo"><?php print htmlentities($item_group['titulo'], ENT_QUOTES, 'utf-8'); ?></h4>
              <ul>
<?php     foreach ($item_group['items'] AS $item): ?>
<?php       if(!empty($item['label'])): 
              $microdata = '';
              switch($item['label']){
                case 'Color':
                  $microdata = 'itemprop="color"';
                  break;
              }
?>
                <li><span><?php print htmlentities($item['label'], ENT_QUOTES, 'utf-8'); ?>:</span> <span class="item-valor" <?php print $microdata; ?>><?php print isset($item['link_settings']) ? l($item['value'], $item['link_settings']['path'], $item['link_settings']['options']) : $item['value']; ?></span></li>
<?php       else: ?>
                <li><span><?php print isset($item['link_settings']) ? l($item['value'], $item['link_settings']['path'], $item['link_settings']['options']) : $item['value']; ?></span> </li>
<?php       endif; ?>
<?php     endforeach; ?>
              </ul>
          </div>
<?php endforeach; ?>

<?php   if($div_is_open): //cerramos el div si aún sigue abierto?>
<?php     $div_is_open = FALSE; ?>
      </div>
<?php   endif; ?>
        
    </div>
    <div class="Banner no-print">
<?php

if($es_nodo_nuevo_sin_login || $es_nodo_nuevo_con_login || $consulta_enviada) {
  //Si se crea un nodo o se envia una consulta agregamos el código de conversión
  //$block = module_invoke('block', 'block', 'view', AUTOSLAVOZ_CODIGO_CONVERSION_BID);
  //print $block['content'];
}
if($es_nodo_nuevo_sin_login || $es_nodo_nuevo_con_login || $consulta_enviada) {
  /*
  if(clasificados_rubros_node_pertenece_al_grupo_autos($node)) {
    $block = module_invoke('block', 'block', 'view', CLASIFICADOS_CODIGO_CONVERSION_ADWORDS_RUBRO_AUTOS_BID);
    print $block['content'];
  }
  if(clasificados_rubros_node_pertenece_al_grupo_inmuebles($node)) {
    $block = module_invoke('block', 'block', 'view', CLASIFICADOS_CODIGO_CONVERSION_ADWORDS_RUBRO_INMUEBLES_BID);
    print $block['content'];
  }
  if($node->type=='aviso_producto') {
    $block = module_invoke('block', 'block', 'view', CLASIFICADOS_CODIGO_CONVERSION_ADWORDS_RUBRO_PRODUCTOS_BID);
    print $block['content'];
  } else {
    $block = module_invoke('block', 'block', 'view', CLASIFICADOS_CODIGO_CONVERSION_ADWORDS_RUBROS_MIX_BID);
    print $block['content'];
  }
  */
}
?>
    </div>
        
        <!-- cxenseparse_end -->
<?php if($node->type == 'aviso_emprendimiento'):
        // Agregamos la lista de avisos relacionados al emprendimiento.
        print views_embed_view('relacionados_emprendimientos');
?>
<?php endif; ?>

    </div>
<?php if($node->disponible_venta): ?>
    <br style="clear: both;" />
<?php endif; ?>

<?php if($node->disponible_venta && $node->type=='aviso_producto') : ?>
    
    <div class="content-social">
      <div class="social-wrap clearfix">
        <a class="facebook-share facebook-square" href="http://www.facebook.com/sharer.php?u=<?php print url($node->path, array('absolute'=>'TRUE', 'alias'=>TRUE)); ?>" title="Recomendar aviso en Facebook" target="_blank" rel="noindex"><span class="icon icon-fb"></span><span class="count"></span><span> compartidas</span></a>
        <?php if(isset($node->content['recomendar_amigo_form'])): ?><a class="enviar-mail" href="javascript:void(0);" title="Recomendar a un amigo" rel="noindex"><span class="icono-mensaje"></span>Enviar por mail</a>
        <?php endif; ?>
        <div class="Herramientas no-print">
          <a href="<?php print url('imprimir', array('query' => 'nid='.$node->nid, 'alias'=>TRUE)); ?>" target="_blank" title="Imprimir" class="imprimir" rel="noindex"><span class="icono-impresora"></span>Imprimir</a>
        </div>
        <div class="Otros">
          <span>Fecha de actualización:</span>&nbsp;<span itemprop="releaseDate"><?php print $node->field_aviso_fecha_inicio[0]['view']; ?></span>&nbsp;&nbsp; <span>| &nbsp;  &nbsp; Visitas:</span> <span id="contador-visitas"></span>
        </div>
      </div>
    </div>
    
<?php else: ?>   
    <div class="Otros">
      <span>Fecha de actualización:</span>&nbsp;<span itemprop="releaseDate"><?php print $node->field_aviso_fecha_inicio[0]['view']; ?></span>&nbsp;&nbsp; <span>| &nbsp;  &nbsp; Visitas:</span> <span id="contador-visitas"></span>
    </div>
    <div class="Herramientas no-print">
      <span class="iconos"><a href="<?php print url('imprimir', array('query' => 'nid='.$node->nid, 'alias'=>TRUE)); ?>" target="_blank" title="Imprimir" class="imprimir" rel="noindex">Imprimir</a></span>
<?php if(isset($node->content['reportar_abuso_form'])): ?>
        <span class="iconos"><a href="javascript:void(0);" title="Reportar abuso" class="abuso" rel="noindex">Abuso</a></span>
<?php endif; ?>
    </div>
<?php endif; ?>   

    <!--<div class="HerramientasCompartir no-print">
    	<span class="iconos"> 
    	<span style="float: left">Compartí en: </span>
        	 <div class="fb"><fb:like send="false" layout="button_count" width="120" show_faces="false" action="like"></fb:like></div> 
          <div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=135978329756767&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
          <div class="fb-share-button" data-href="<?php print $base_root.$_SERVER['REQUEST_URI']; ?>" data-type="button_count"></div>
          <div class="tw">
            <a href="https://twitter.com/share" class="twitter-share-button" data-via="clasificadosLaVoz" data-lang="es">Twittear</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          </div>
          <div class="gmas"><div class="g-plusone"></div></div>
          <script>function cambiarTamano(){$('.g-plusone').attr("data-size","medium");};cambiarTamano();</script>
        </span>
     </div>-->
</div><!-- FIN FichaAuto -->
    
    <?php if(!$node->disponible_venta || $node->type!='aviso_producto') : ?>
    <!-- Comienzo iconos sociales -->  
    <div>
      <div class="social-wrap clearfix">
            <a class="facebook-share facebook-square" href="http://www.facebook.com/sharer.php?u=<?php print url($node->path, array('absolute'=>'TRUE', 'alias'=>TRUE)); ?>" title="Compartir aviso en Facebook" target="_blank" rel="noindex"><span class="icon icon-fb"></span><span class="count"></span><span> compartidas</span></a>
            <a href="https://twitter.com/share" class="twitter-square tweet" title="Compartir aviso en Twitter" data-via="clasificadosLaVoz" data-lang="es" rel="noindex"><span class="icon-tw"></span><span class="count"></span> twitter</a>
            <?php if(isset($node->content['recomendar_amigo_form'])): ?><a class="enviar-mail" href="javascript:void(0);" title="Recomendar a un amigo" rel="noindex"><span class="icon-mensaje"></span>Enviar por mail</a>
            <?php endif; ?>
      </div>
    </div>  
    <!-- Fin iconos sociales -->
    <?php endif; ?>   
    
<div style="display: none;">
<?php if($visual_aviso->esta_publicado): $dominio_img = clasificados_urls_static(); ?>
  <div id="aviso-nid"><?php print $node->nid; ?></div>
<?php endif; ?>
<!-- Galeria para ColorBox -->
<?php foreach ($visual_aviso->multimedia as $mkey => $media): ?>
<?php   if($media['type'] != 'image' || $media['quantity'] < 1 || !$visual_aviso->esta_publicado) { continue; } //skip slider because isn't a media image o si solo tiene una imagen ?>
<?php   foreach($media['items'] as $ikey => $foto): $key_random = array_rand($dominio_img, 1); ?>
<a href="<?php print $dominio_img[$key_random].$foto['filepath']; ?>" id="foto-oculta-<?php print $mkey.'_'.$ikey; ?>" class="gallery-ficha-oculta" title="<?php print check_plain($title.' #'.($ikey+1)); ?>"><?php print $title.' #'.($ikey+1); ?></a>
<?php   endforeach; ?>
<?php endforeach; ?>
</div>
