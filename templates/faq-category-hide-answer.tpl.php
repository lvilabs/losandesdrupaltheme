<?php
// $Id$

/**
 * @file
 * Template file for the FAQ page if set to show/hide categorized answers when
 * the question is clicked.
 */

/**
 * Available variables:
 *
 * $display_header
 *   Boolean value controlling whether a header should be displayed.
 * $header_title
 *   The category title.
 * $category_depth
 *   The term or category depth.
 * $description
 *   The current page's description.
 * $term_image
 *   The HTML for the category image. This is empty if the taxonomy image module
 *   is not enabled or there is no image associated with the term.
 * $display_faq_count
 *   Boolean value controlling whether or not the number of faqs in a category
 *   should be displayed.
 * $question_count
 *   The number of questions in category.
 * $nodes
 *   An array of nodes to be displayed.
 *   Each node stored in the $nodes array has the following information:
 *     $node['question'] is the question text.
 *     $node['body'] is the answer text.
 *     $node['links'] represents the node links, e.g. "Read more".
 * $use_teaser
 *   Whether $node['body'] contains the full body or just the teaser text.
 * $container_class
 *   The class attribute of the element containing the sub-categories, either
 *   'faq-qa' or 'faq-qa-hide'. This is used by javascript to open/hide
 *   a category's faqs.
 * $subcat_list
 *   An array of sub-categories.  Each sub-category stored in the $subcat_list
 *   array has the following information:
 *     $subcat['link'] is the link to the sub-category.
 *     $subcat['description'] is the sub-category description.
 *     $subcat['count'] is the number of questions in the sub-category.
 *     $subcat['term_image'] is the sub-category (taxonomy) image.
 * $subcat_list_style
 *   The style of the sub-category list, either ol or ul (ordered or unordered).
 * $subcat_body_list
 *   The sub-categories faqs, recursively themed (by this template).
 */

if ($category_depth > 0) {
  $hdr = 'h6';
}
else {
  $hdr = 'h5';
}

  $term_fields = term_fields_get_fields($term);
  if(isset($term_fields['faq_tipo_visualizacion'])) {
    $tipo_visualizacion = (int) $term_fields['faq_tipo_visualizacion'];
    $tipo_visualizacion = ($tipo_visualizacion <= 0) ? 1 : $tipo_visualizacion;
  } else {
    $tipo_visualizacion = 1;
  }

?>

  <?php if (count($nodes)): ?>
<div class="main-module">
<div class="blanco" style="">
<div class="info ayuda">
<div id="ColumnasABCTop">
<div id="ColumnaABTop">
<div class="ColumnaAB">
<div class="Tip Punteado pBottom">
<div class="info">
  <?php if ($display_header): ?>
    <div class="Title"><h2>
    <?php print $term_image; ?>
    <?php print $header_title; ?>
    <?php if ($display_faq_count): ?>
      (<?php print $question_count; ?>)
    <?php endif; ?>
    </h2></div>

  <?php else: ?>
    <?php print $term_image; ?>
  <?php endif; ?>
  <br clear="all" />

<?php if($tipo_visualizacion == 1): ?>
  <div class="borde">
    <?php foreach ($nodes as $i => $node): ?>
      <?php if(strpos($node['question'], 'Reposicionamiento de Avisos') !== false): ?>
        <a name="reposicionamiento"></a>
        <div class="pregunta2 opened"><?php print $node['question']; ?></div>
      <?php else: ?>
        <div class="pregunta2"><?php print $node['question']; ?></div>
      <?php endif; ?>
      
      <div class="texto">
      <?php print $node['body']; ?>
      <?php if (isset($node['links'])): ?>
        <?php print $node['links']; ?>
      <?php endif; ?>
      </div> <!-- Close div: faq-answer faq-dd-hide-answer -->
    <?php endforeach; ?>
  </div>
<?php else: ?>
    <?php foreach ($nodes as $i => $node): ?>
    <div class="<?php print ($i % 2) ? 'busqueda_right' : 'busqueda_left'; ?>">
      <div class="Linea"><span class="Blanco">&nbsp;</span></div>
      <a name="num6"></a>
      <div class="pregunta2"><?php print $node['question']; ?></div>

      <div class="texto">
      <?php print $node['body']; ?>
      <?php if (isset($node['links'])): ?>
        <?php print $node['links']; ?>
      <?php endif; ?>
      </div>
    </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<br clear="all" />
  <?php endif; ?>
