<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" xmlns:fb="http://ogp.me/ns/fb#">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php if($is_front): ?>
    <meta name="bitly-verification" content="cf14ef481693"/>
  <?php endif; ?>
  <?php print $styles; ?>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?> <?php if($es_sitios_principal) print 'demo'; ?>" itemscope itemtype="http://schema.org/WebPage">
<?php include(dirname(__FILE__).'/includes/comscore.php'); ?>

<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Header Content -->
<header class="<?php if($es_sitios_principal) print 'back'; ?>">
  <?php include dirname(__FILE__).'/includes/header-micrositios.tpl.php'; ?>
  <?php if(!$es_sitios_principal) { ?>
    <?php include_once dirname(__FILE__).'/includes/menu-micrositios.tpl.php'; ?>
  <?php } ?>
</header>

<div id="system-messages-wrapper" style="display:none"><?php print $messages; ?></div>

<!-- Page Content -->
<?php print $content; ?>
<!-- /.container -->

<!-- Footer -->
<?php if(!$es_sitios_principal) { ?>  
  <?php include dirname(__FILE__).'/includes/footer-micrositios.tpl.php'; ?>
<?php } ?>
  
<?php print $closure; ?>
</body>
</html>