<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix">
  <div class="Content AvisosDestacados clearfix">
     <div class="Sombra">
      <div class="clearfix">
        <h3><strong>Anuncios Destacados</strong></h3>
      </div>
    </div>
    <div class="clearfix contentAvisos">
  <?php foreach($content as $aviso): ?>
  <?php   $count++; ?>
      <div class="CajaAviso <?php print ($count%2==0?'Par':''); ?>">
        <a href="<?php print $aviso['url']; ?>" title="<?php print $aviso['title']; ?>">
          <div class="imgAviso">
            <?php
              $alt_img = explode('/', $aviso['url']);
              $alt_img = $alt_img[3].' de '.$alt_img[4].' '.str_replace('"', '', $aviso['titulo']).' a '.$aviso['precio'];
            ?>
            <img src="<?php print $aviso['foto']; ?>" alt="<?php print $alt_img; ?>" title="<?php print $aviso['title']; ?>" class="imagecache imagecache-ficha_aviso_314_211" width="314" height="211">
          </div>
          <div class="AvisoDescripcion">
            <h4><?php print $aviso['title']; ?></h4>
            <?php
              //Kilometraje
              $kilometraje = "";
              if($aviso['usado'] == "0 Km"){
                $kilometraje = $aviso['usado'];
              } elseif($aviso['kilometros'] != "") {
                $kilometraje = $aviso['kilometros']." Km";
              }
            ?>
            <p><?php print $aviso['anio']." ".$kilometraje." ".$aviso['combustible']; ?></p>
            <span class="precio"><?php print $aviso['precio']; ?></span>
          </div>
        </a>
      </div>
  <?php endforeach; ?>
    </div>
  </div>
</div>