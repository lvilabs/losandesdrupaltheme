<?php
  //print_r($form);
?>
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/configuracion.css">
<link rel="stylesheet" type="text/css" href="/sites/all/modules/custom_new/administracion_sitios/css/elementTransitions.css" />
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/font-awesome/css/font-awesome.min.css">
<!-- Page Content -->
<div id="main" class="container-fluid page-home">                        
  <!-- Boxed Container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="title-section home">
          <h1>Elegí una plantilla</h1>
          <h4> Seleccioná el diseño web que más te guste. </br>
            Tené en cuenta de considerar el tipo de información de tu negocio.
          </h4>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Fluid Container -->
  <div class="container-fluid">
    <div class="row">               
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <?php 
          $count = 1;
          $count_int = 1;
          foreach($form['templates'] as $ktemplate => $template) { 
            if(strpos($ktemplate, '#') === FALSE) {
          ?>
          <?php if($count%2 != 0) { ?>
          <div class="item <?php if($count == 1) print 'active'; ?>">
          <?php } ?>
            <div class="col-md-6 plantilla">
              <a href="#" class="select-plantilla" id="<?php print $template['#return_value']; ?>">
                <img class="img-responsive img-hover" src="/sites/all/modules/custom_new/administracion_sitios/img/templates/<?php print $template['#return_value'].'.png'; ?>" alt="">
              </a>
              <div class="col-md-12 selector" style="min-height: 44px;">
                <h2 class="selected-template" id="selected-<?php print $template['#return_value']; ?>" style="<?php if($template['#return_value'] != $template['#default_value']) print 'display:none;'; ?>">
                  <i class="fa fa-check-circle-o"></i>
                </h2>
              </div> 
            </div>
          <?php if($count_int == 2 || count($form['templates']['#options']) == $count) { ?>
          </div>
          <?php $count_int = 0; } ?>
          <?php 
              $count++;
              $count_int++;
            }
          } ?>
        </div>                  
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      
      <input type="hidden" value="<?php print $form['templates']['#default_value'] ?>" id="default-template" />
      <div class="col-md-4 col-md-offset-4 button-footer">
        <?php print drupal_render($form['templates-submit']); ?>
      </div>                
    </div>
  </div>
  <!-- /.row -->
  <div style="display:none"><?php print drupal_render($form); ?></div>
</div>
<!-- /.container -->
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/elementTransitions.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/templates.js"></script>