<table width="625" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
  
  <!--[if mso]>
  <body class="mso">
<![endif]-->
<!--[if !mso]><!-->
  <body class="no-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
<!--<![endif]-->
    <table class="wrapper" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #fff;" role="presentation" cellspacing="0" cellpadding="0"><tbody><tr><td>
      <div role="banner">
        <div class="header" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);" id="emb-email-header-container">
        <!--[if (mso)|(IE)]><table align="center" class="header" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="width: 600px"><![endif]-->
          <div class="logo emb-logo-margin-box" style="font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Arial, Helvetica, sans-serif;Margin-left: 20px;Margin-right: 20px;" align="center">
            <div class="logo-center" id="emb-email-header" align="center"><a href="http://colecciones.lavoz.com.ar"><img style="display: block;height: auto;width: 100%;border: 0;max-width: 323px;" src="http://colecciones.lavoz.com.ar/public/img/logo-header.png" alt="" width="323"></a></div>
          </div>
        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </div>
      </div>
      <div role="section">
      <div style="background-color: #004794;">
        <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #004794;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
            <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:7px;font-size:1px">&nbsp;</div>
    </div>
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #e31212;font-size: 26px;line-height: 34px;font-family: Arial, Helvetica, sans-serif;text-align: center;"><span style="color:#fff"><strong>Se acreditó el pago de !nombre_comprador por el siguiente aviso:</strong></span></h2>
    </div>
            
            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>
  
      <div style="background-color: #fff;">
        <div class="layout two-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #fff;"><td class="layout__edges">&nbsp;</td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:25px;font-size:1px">&nbsp;</div>
    </div>
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 450px;" alt="" src="!foto" width="260">
        </div>
      </div>
            
            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:25px;font-size:1px">&nbsp;</div>
    </div>
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h3 style="Margin-top: 0;Margin-bottom: 12px;font-style: normal;font-weight: normal;color: #281557;font-size: 20px;line-height: 26px;font-family: Arial, Helvetica, sans-serif;"><span style="color:#2C2C2C"><strong>!titulo_aviso</strong></span></h3>
    </div>
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div class="btn btn--flat btn--medium" style="text-align:left;">
        <!--[if !mso]--><a style="border-radius: 4px;display: inline-block;font-size: 12px;font-weight: bold;line-height: 22px;padding: 10px 20px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff !important;background-color: #004794;font-family: Arial, Helvetica, sans-serif;" href="!link_aviso" target="_blank">Ver Colección</a><!--[endif]-->
      <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="!link_aviso" style="width:97px" arcsize="10%" fillcolor="#004794" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Arial, Helvetica, sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">Ver Colección</center></v:textbox></v:roundrect><![endif]--></div>
    </div>
            
            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>
      
      <div style="line-height:50px;font-size:50px;">&nbsp;</div>
  
      <div style="background-color: #fff;">
        <div class="layout two-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #fff;"><td class="layout__edges">&nbsp;</td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h3 class="size-16" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #281557;font-size: 18px;line-height: 24px;font-family: Arial, Helvetica, sans-serif;" lang="x-size-16"><span style="color:#2C2C2C"><strong>Datos del comprador:</strong></span></h3><p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Nombre: <strong>!nombre_comprador</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Teléfono: <strong>!telefono_comprador</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Email: <strong>!email_comprador</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Documento: <strong>!documento_comprador</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Dirección: <strong>!direccion_comprador !calle_comprador, !ciudad_comprador, !provincia_comprador</strong>.</span></p>
            </div>
          
            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h3 class="size-16" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #281557;font-size: 18px;line-height: 24px;font-family: Arial, Helvetica, sans-serif;" lang="x-size-16"><span style="color:#2C2C2C"><strong>Datos de la compra:</strong></span></h3><p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Cantidad: <strong>!cantidad</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Descuento: <strong>!descuento</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Total: <strong>!monto</strong>.</span></p>
      
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Medio de Pago: <strong>!medio_pago</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Método de envío: <strong>!metodo_envio</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Kiosco de envío: <strong>!direccion_vendedor</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Horario de reparto: <strong>!horario</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Línea de transporte: <strong>!linea_transporte</strong>.</span></p>
      <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Código de seguimiento: <strong>!codigo_seguimiento</strong>.</span></p>
            </div>
         
            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>
  
      <div style="line-height:20px;font-size:20px;">&nbsp;</div>
  
      
      <div style="background-color: #f5f5f5;">
        <div class="layout two-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #f5f5f5;"><td class="layout__edges">&nbsp;</td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
            <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
              <a href="http://colecciones.lavoz.com.ar"><img style="border: 0;display: block;height: auto;width: 100%;max-width: 140px;" alt="" src="http://colecciones.lavoz.com.ar/public/img/logo-footer.png" width="140"></a>
            </div>
            
            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 300px" valign="top" class="w260"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: Arial, Helvetica, sans-serif;">
            
              <div style="Margin-left: 20px;Margin-right: 20px;">
              <p style="Margin-top: 12px;Margin-bottom: 0;"><span style="color:#004794">Copyright © !anio La Voz, Todos los derechos reservados.</span></p>
              </div>
            
            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>
  
    </div></td></tr></tbody></table>
</body>

</table>