<?php $gmapkey =  variable_get('googlemap_api_key', 0); ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php print $gmapkey; ?>"></script>
<?php
  global $user;
  $edicion = in_array("edit", arg());
  $tipos_avisos_nodetypes = sitio_local_obtener_tipos_avisos_node_types();
  $segmento_vid = clasificados_node_obtener_segmento_vocabulary_id($form['#node']);

  //Si el Tipo de Aviso tiene Rango de Cuotas entonces hay que procesar los valores ingresados.
  if(isset($form['field_aviso_rango_cuotas'])) {
    $rangos = $form['field_aviso_rango_cuotas'][0]['#default_value']['value'];

    $rangos_matches = array();
    if(preg_match_all('/Cuota\s+([0-9]+)\s+a\s+([0-9]+)\s+\\$\s+([0-9\.]+)/um', $rangos, $rangos_matches)) {
      unset($rangos_matches[0]);
    } else {
      $rangos_matches = array();
    }
  }

  $show_informacion_secundaria = ($form['#node']->type == 'aviso_repuesto') ? FALSE : TRUE;
?>
<div class="Content CrearAnuncio clearfix">
  <?php 
    if($user->uid != 0) {
      if($edicion) {
  ?>
      <h3>Editar Aviso</h3>
  <?php
      } else if(arg(2)=='clone') {
  ?>
      <h3>Copiar Aviso</h3>
  <?php
      }
   } else {
  ?>
    
  <?php
  }
  ?>
  <div class="blanco">
    <?php if($user->uid != 0 && !$edicion){ ?>
      <div class="pasosbreadcrumb clearfix aviso">
        <div class="pasos clearfix">
          <div class="paso1 active">Datos del aviso</div>  
          <div class="paso2"><label>Publicación</label></div>  
        </div>
      </div>
    <?php } ?>
    <div class="avisolegal">Los campos marcados con un asterisco (<span style="color:red;">*</span>) son obligatorios.</div>
    <div class="FormCrear"> 
    </div>
    <div class="FormRegistro ContactoAutos Avisos">
      <fieldset>
        <div class="Row clearfix">
          <div id="tipos-contenido-wrapper" class="form-item tipos-contenido-wrapper">
          <label>Categoría: <span style="color:red;">*</span> </label>
          <?php
            if($edicion || arg(2)=="clone"){
              $rubro = clasificados_rubros_node_obtener_rubro_term($form['#node']);
          ?>
              <input type="text" value="<?php print htmlentities($rubro->name, ENT_QUOTES, 'utf-8'); ?>" readonly="readonly" class="readonly-text" style="width: 172px;"/>
          <?php
            } else {
              $rubros = clasificados_rubros_obtener_rubros(arg(2));
?>
              <select name="tipos-contenido" class="" id="select-tipos-contenido">
              <?php foreach($rubros as $rubro) : ?>
                <?php if(!in_array($rubro->aviso_node_type, variable_get('clasificados_rubros_ocultos', array()))) : ?>
                  <option value="/node/add/<?php print str_replace('_', '-', $rubro->aviso_node_type); ?>" <?php if ($rubro->aviso_node_type == $form['#node']->type) print 'selected="selected"'; ?>><?php print htmlentities($rubro->title, ENT_QUOTES, 'utf-8'); ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
              </select>
<?php
            }
?>
          <div class="description" style="display: none;"><span class="Ingresa">Cargando formulario para <strong>tipo-contenido</strong>. Espere por favor...</span></div>
          </div>
        </div>
<?php
          $marca_modelo_vid = clasificados_node_obtener_marca_modelo_vocabulary_id($form['#node']);
          if($marca_modelo_vid !== FALSE) {
?>
        <div class="Row clearfix">
          <?php
              $marca_modelo_vocabulary = taxonomy_vocabulary_load($marca_modelo_vid);
              if($edicion) {
                $marca_default_term = _autoslavoz_node_obtener_marca_term($form['#node'], TRUE);
                $modelo_default_term = _autoslavoz_node_obtener_modelo_term($form['#node'], TRUE);
              ?>
              <div class="form-item edicion">
<?php if($marca_modelo_vocabulary->multiple): ?>
                <label>Marca y Modelo: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
                <input type="text" value="<?php print $marca_default_term->name; ?>" readonly="readonly" class="readonly-text" />
                <?php 
                  $form['taxonomy'][$marca_modelo_vid]['#title'] = '';
                  print drupal_render($form['taxonomy'][$marca_modelo_vid]); 
                ?>
<?php else: ?>
                <label>Marca: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
                <input type="text" value="<?php print $marca_default_term->name; ?>" readonly="readonly" class="readonly-text" />
<?php endif; ?>
              </div>
            <?php  
              } else {
                $form['taxonomy'][$marca_modelo_vid]['hierarchical_select']['selects'][0]['#options']['label_0'] = "- seleccione -";
                if($marca_modelo_vocabulary->multiple) {
                  $form['taxonomy'][$marca_modelo_vid]['#title'] = 'Marca y Modelo';
                } else {
                  $form['taxonomy'][$marca_modelo_vid]['#title'] = 'Marca';
                }
                print drupal_render($form['taxonomy'][$marca_modelo_vid]);
              }
          ?>
        </div>
<?php
          }
?>
<?php if(isset($form['taxonomy'][AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID])): ?>
        <div class="Row clearfix">
<?php
        if($edicion) {
          $subrubro_terms = taxonomy_node_get_terms_by_vocabulary($form['#node'], AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID);
          $subrubro_t1_default_term = FALSE;
          $subrubro_t2_default_term = FALSE;
          foreach($subrubro_terms as $tax) {
            if (clasificados_taxonomy_has_parent($tax->tid)) {
              $subrubro_t2_default_term = $tax;
            } else {
              $subrubro_t1_default_term = $tax;
            }
          }
?>
        <div class="form-item edicion">
          <label>Subrubro: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
          <input type="text" value="<?php print $subrubro_t1_default_term->name; ?>" readonly="readonly" class="readonly-text" />
          <?php 
            $form['taxonomy'][AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID]['#title'] = '';
            print drupal_render($form['taxonomy'][AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID]); 
          ?>
        </div>
      <?php
        } else {
          $form_subrubro =& $form['taxonomy'][AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID];
          $form_subrubro['#title'] = 'Subrubro';
          print drupal_render($form_subrubro);
        }
?>
        </div>
<?php endif; ?>
      </fieldset>
      <fieldset>  
        <div class="AyudaAlta DescripcionAuto">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul><li><strong>Agregá toda la información extra</strong> sobre tu vehículo. Tené en cuenta que las primeras palabras de la descripción son las que aparecen en la lista de los resultados, por lo que aprovechá para poner alguna característica llamadora.
</li></ul>
          </div>
        </div>
        <h3>Información Principal</h3>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_tipo_unidad_util']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            $form['title']['#title'] = 'Título del Aviso';
            $form['title']['#description'] = '<span class="Ingresa">Máximo de 80 caracteres.</span>';
            print drupal_render($form['title']); 
          ?>
          
        </div>
        <div class="Row clearfix">
          <?php 
            $form['body_field']['body']['#rows'] = 5;
            $form['body_field']['body']['#description'] = '<span class="Ingresa">Ingrese todos los datos extras que desee agregar a su aviso.</span>';
            unset($form['body_field']['format'][1]);
            unset($form['body_field']['format'][5]);
            unset($form['body_field']['format'][4]['#description']);
            unset($form['body_field']['format']['#type']);
            unset($form['body_field']['format']['#title']);
            print drupal_render($form['body_field']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            if($edicion && !isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS])) {
              $provincia = '';
              $ciudad = '';
              $barrio = '';
              foreach($form['taxonomy'][5]['#default_value'] as $ubicacion){
                if (!clasificados_taxonomy_has_parent($ubicacion)) {
                  $provincia = taxonomy_get_term($ubicacion);
                } elseif(clasificados_taxonomy_es_ciudad($ubicacion)) {
                  $ciudad = taxonomy_get_term($ubicacion);
                } else {
                  $barrio = taxonomy_get_term($ubicacion);
                }
              }
              ?>
              <div class="form-item">
                <label>Provincia y Ciudad: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
                <input type="text" value="<?php print $provincia->name; ?>" readonly="readonly" class="readonly-text" />
                <?php if($ciudad->name != ""){ ?>
                  <input type="text" value="<?php print $ciudad->name; ?>" readonly="readonly" class="readonly-text" />
                <?php } ?>
              </div>
            <?php  
            } else {
              $form['taxonomy'][5]['#title'] = 'Provincia y Ciudad';
              print drupal_render($form['taxonomy'][5]); 
            }
          ?>
        </div>
<?php if(isset($form['field_aviso_calle'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_calle_altura'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_calle_altura']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_confidencial'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_confidencial']['value'][1]['#title'] = '';
            $form['field_aviso_confidencial']['#description'] = '<span class="Ingresa">Se ocultaran los campos de Calle y Altura en la ficha del aviso.</span>';
            print drupal_render($form['field_aviso_confidencial']);
            print '<div class="form-item"><div class="description"><span class="Ingresa">Se ocultaran los campos de Calle y Altura en la ficha del aviso.</span></div></div>';
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mapa'])): ?>
        <div class="Boton">
          <input type="button" value="Marcar Dirección en Mapa" class="boton-mapa" onClick="javascript:agregar_mapa();">
        </div>
        <div class="opciones-geomap DN">
          <div>Encontramos mas de una posible dirección, selecciona la más cercana:</div>
        </div>
        <div class="recomendacion DN">
          <strong>Error en la geolocalización de la dirección</strong><br> 
          <span class="reco-message"></span>
        </div>
        <div class="Row clearfix">
          <label for="edit-picture-upload">  
            <?php print ''.$form['field_aviso_mapa']['#title'].': <br>(Opcional)'; ?>
          </label>
          <div id="multimedia_mapas" style="display: block; width: 58%; height: 300px; float: left;"></div>
          <?php 
            unset($form['field_aviso_mapa'][0]['locpick']['instructions']);
            print drupal_render($form['field_aviso_mapa'][0]['locpick']['map']);
          ?>
          <div class="description">
            <span class="Ingresa Mapa"><?php print drupal_render($form['field_aviso_mapa'][0]['locpick']['map_instructions']); ?></span>
            <?php 
            if(isset($form['field_aviso_mapa'][0]['delete_location'])){
              unset($form['field_aviso_mapa'][0]['delete_location']['#title']);
              $form['field_aviso_mapa'][0]['delete_location']['#description'] = "Eliminar ubicación en el mapa.";
              print drupal_render($form['field_aviso_mapa'][0]['delete_location']);
            } 
            ?>
          </div>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_usado'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            $form['field_aviso_usado']['value']['#title'] = 'Uso';
            print drupal_render($form['field_aviso_usado']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_usado_anio'])): ?>
        <div class="Row clearfix">
          <?php 
            $form['field_aviso_usado_anio'][0]['value']['#title'] = '';
            print drupal_render($form['field_aviso_usado_anio']);
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_sin_kilometros'])): ?>
        <div class="Row Radiobutton clearfix sin-kilometro">
          <?php 
            print drupal_render($form['field_aviso_sin_kilometros']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_kilometros'])): ?>
        <div class="Row clearfix">
          <?php 
            $form['field_aviso_kilometros'][0]['value']['#size'] = 6;
            print drupal_render($form['field_aviso_kilometros']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_combustible'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_combustible']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_precio'])): ?>
		<div class="AyudaAlta">
          <span class="punta"></span>
          		<div class="Texto"> 
            		<ul>
              			<li><strong>Agregá el precio</strong>  y no quedes afuera cuando un usuario hace una búsqueda por Precio. En el caso de que quieras ocultarlo, hacé clic en Ocultar Precio.</li>	
            		</ul>	
                </div>
        </div>         
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_precio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_moneda'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_moneda']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_precio'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_precio']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_recibe_menor'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_recibe_menor']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_forma_pago'])): ?>
        <?php if(module_exists('creditos') && $user->uid != 0 && array_key_exists(AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS, $user->roles) && $user->profile_tipo_concesionaria=='Concesionaria' && $form['#node']->type != 'aviso_repuesto' && $form['#node']->type != 'aviso_plan_ahorro') : ?>
        <div class="AyudaAlta FormaPago">
          <span class="punta"></span>
          <div class="Texto"> 
            <ul><li><strong>Solicitá un crédito para tu comprador. </strong> Para mayor información <a href="<?php print url('administrar/creditos/0', array('alias'=>TRUE)); ?>" target="_blank">Consulte aquí</a>
            </li></ul>
          </div>
        </div>
        <?php endif; ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_forma_pago']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_procreauto'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php
            print drupal_render($form['field_aviso_procreauto']);
            print '<div class="form-item"><div class="description"><span class="Ingresa">Es apto para crédito ProCreAuto.</span></div></div>';
          ?>
        </div>
<?php endif; ?>
      </fieldset>
      <fieldset>
        <h3>Datos del Vendedor</h3>
        
        <div class="AyudaAlta completar-whatsapp">
          <!-- <span class="tag nuevo">Nuevo</span> -->
          <div class="Texto">
            <ul>
              <li><strong>¡Sumá tu WhatsApp!</strong>. Aumentá las chances de contacto agregando un teléfono por cada uno de tus avisos.</li>
            </ul>
            <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 </span>0 
            <?php if(empty($form['field_aviso_telefono_whatsapp'][0]['#value']['value'])): ?>
              <?php if(empty($user->profile_telefono_whatsapp)): ?>
                <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                15 <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                <?php unset($form['field_aviso_ocultar_whatsapp']); ?>
              <?php else: ?>
                <?php $whatsapp = explode('+549', $user->profile_telefono_whatsapp); ?>
                <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
                <div class="ocultar-wa">
                  <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
                </div>
              <?php endif; ?>
            <?php else: ?>
              <?php $whatsapp = explode('+549', $form['field_aviso_telefono_whatsapp'][0]['#value']['value']); ?>
              <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
              <div class="ocultar-wa">
                <?php print drupal_render($form['field_aviso_ocultar_whatsapp']); ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
<?php if(isset($form['field_aviso_tel_vendedor'])): ?>
        <div class="Row clearfix telefonoContacto">
          <?php if(empty($form['field_aviso_tel_vendedor'][0]['#value']['value'])): ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" > /
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" > /
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php else: ?>
            <?php 
              $tel_1 = '';
              $tel_2 = '';
              $tel_3 = '';
              $array_tel = explode('/', $form['field_aviso_tel_vendedor'][0]['#value']['value']);
              foreach($array_tel as $key => $num_tel) {
                switch($key) {
                  case 0:
                    $tel_1 = trim($num_tel);
                    break;
                  case 1:
                    $tel_2 = trim($num_tel);
                    break;
                  case 2:
                    $tel_3 = trim($num_tel);
                    break;
                }
              }
            ?>
            <div class="form-item">
              <label for="edit-field-aviso-tel-vendedor-0-value">Teléfono del vendedor: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
              <input type="text" name="field_aviso_tel_vendedor_1" id="tel_vendedor_1" value="<?php print $tel_1; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_2" id="tel_vendedor_2" value="<?php print $tel_2; ?>" > <span class="separador">/</span>
              <input type="text" name="field_aviso_tel_vendedor_3" id="tel_vendedor_3" value="<?php print $tel_3; ?>" >
              <div class="description"><span class="Ingresa">Puedes ingresar hasta 3 teléfonos distintos.</span></div>
            </div>
          <?php endif; ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_tel'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_tel']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_mail_vendedor'])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['field_aviso_mail_vendedor']); 
          ?>
        </div>
<?php endif; ?>
<?php if(isset($form['field_aviso_ocultar_mail'])): ?>
        <div class="Row Radiobutton clearfix">
          <?php 
            print drupal_render($form['field_aviso_ocultar_mail']); 
          ?>
        </div>
<?php endif; ?>
      </fieldset>
<?php if(isset($form['group_aviso_detalle_plan'])): ?>
      <fieldset>
        <h3>Detalle del Plan</h3>
<?php   if(isset($form['field_aviso_tipo_plan'])): ?>
        <div class="Row clearfix">
<?php
            print drupal_render($form['field_aviso_tipo_plan']);
?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_valor_cuota'])): ?>
        <div class="Row clearfix">
<?php
            print drupal_render($form['field_aviso_valor_cuota']);
?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_valor_cuota_promedio'])): ?>
        <div class="Row clearfix">
<?php
            print drupal_render($form['field_aviso_valor_cuota_promedio']);
?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_valor_cuota_suscrip'])): ?>
        <div class="Row clearfix">
<?php
            print drupal_render($form['field_aviso_valor_cuota_suscrip']);
?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_rango_cuotas'])): ?>
        <div class="Row clearfix">

<!-- START -->
          <div id="edit-field-aviso-rango-cuotas-0-value-wrapper" class="form-item">
            <label>Rango de Cuotas: </label>
            <div id="rangos-cuotas">
              <div class="rango-cuota">
                <div class="rango-desde"><span>Desde:</span>
                  <select id="rango-desde-select">
                    <option value="0">-</option>
<?php   for($i=1;$i<=84;$i++): ?>
                    <option value="<?php print $i; ?>"><?php print $i; ?></option>
<?php   endfor; ?>
                  </select>
                </div>
                <div class="rango-hasta"><span>Hasta:</span>
                  <select id="rango-hasta-select">
                    <option value="0">-</option>
<?php   for($i=1;$i<=84;$i++): ?>
                    <option value="<?php print $i; ?>"><?php print $i; ?></option>
<?php   endfor; ?>
                  </select>
                </div>
                <div class="rango-precio"><span>$:</span>
                  <input id="rango-precio-text" size="10" maxlength="10" />
                </div>
                <div class="rango-accion"><a href="#" class="agregar-rango-cuota">Agregar</a></div>
              </div>
              <div class="clearfix"></div>
<?php   foreach($rangos_matches as $k => $v): ?>
              <div class="rango-cuota draggable">
                <div class="rango-desde"><span>Desde:</span> <?php print $rangos_matches[1][($k-1)]; ?></div>
                <div class="rango-hasta"><span>Hasta:</span> <?php print $rangos_matches[2][($k-1)]; ?></div>
                <div class="rango-precio"><span>$:</span> <?php print $rangos_matches[3][($k-1)]; ?></div>
                <div class="rango-accion"><a href="#" class="quitar-rango-cuota">Quitar</a></div>
              </div>
              <div class="clearfix"></div>
<?php   endforeach; ?>
            </div>
            <div class="description">Ingrese el rango de las cuotas y sus precios correspondientes en pesos.</div>
          </div>
<!-- END -->
        </div>
        <div class="Row clearfix" style="display: none;">
<?php
            print drupal_render($form['field_aviso_rango_cuotas']);
?>
        </div>
<?php   endif; ?>
      </fieldset>
<?php endif; ?>

<?php if($show_informacion_secundaria): ?>
      <fieldset>
        <h3>Información Secundaria</h3>
<?php   if($segmento_vid !== FALSE): ?>
        <div class="Row clearfix">
<?php
            $form['taxonomy'][$segmento_vid]['#title'] = 'Segmento';
            print drupal_render($form['taxonomy'][$segmento_vid]);
?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_version'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['field_aviso_version']); 
          ?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_cilindrada'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['field_aviso_cilindrada']); 
          ?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['taxonomy'][AUTOSLAVOZ_COLOR_TAXONOMY_VID])): ?>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['taxonomy'][AUTOSLAVOZ_COLOR_TAXONOMY_VID]);
          ?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_transmision'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['field_aviso_transmision']); 
          ?>
        </div>
<?php   endif; ?>
<?php   if(isset($form['field_aviso_cant_puertas'])): ?>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['field_aviso_cant_puertas']); 
          ?>
        </div>
<?php   endif; ?>
      </fieldset>
<?php endif; ?>
      <fieldset>
      <div class="AyudaAlta">
          <span class="punta"></span>
          <?php if(user_is_logged_in()) { ?>
          <!-- :::AYUDA PARA USUARIOS LOGUEADOS:::-->
          <div class="Texto"> 
            <ul><li><strong>Subí tu foto</strong> y aumenta las posibilidades de visitas.<br />
              <br />
Dependiendo el tipo de aviso que elijas para publicar tu vehículo, se verá diferente cantidad de imágenes: <br />
                <br />
                - <strong>Aviso Estandar:</strong> 6 fotos. <br />
                - <strong>Aviso Premium:</strong> 12 fotos. <br />
                - <strong>Aviso Super Premium y Vidriera:</strong> 25 fotos.
                <br /><br />
Si contratas un destaque para el aviso, ten en cuenta que la 1ª foto que cargues es la que se verá en el listado de avisos.</li></ul>
          </div>
          <?php } else { ?>
          <div class="Texto">
            <ul><li>Los usuarios quieren ver avisos con fotos. Subí tu foto y aumentá la cantidad de visitas.<br /><br /><strong> Tu aviso es Gratuito: SÓLO se verá la primer foto cargada.</strong> <br />Para que tu aviso tenga más fotos y más visitas, completá tu registración y destacá tu aviso.</li></ul>
          </div>
          <?php } ?>
        </div>
        <h3>Multimedia</h3>
        <div class="Row clearfix Fotos">
          <?php 
            print drupal_render($form['field_aviso_fotos']); 
          ?>
        </div>
        <?php if(isset($form['field_aviso_video'])) { ?>
        <div class="Row clearfix AgregarVideo">
          <?php 
            $form['field_aviso_video'][0]['embed']['#description'] = '<span class="Ingresa">Ingresa la URL de YouTube. Solo visibles en avisos Estandars, Premiums, Super Premiums y Vidriera</span>';
            unset($form['field_aviso_video'][0]['value_markup']);
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#title'] = 'Miniatura del Video:';
            $form['field_aviso_video'][0]['emthumb']['emthumb']['#collapsible'] = 0;
            $form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['#title'] = '';
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['flags']);
            unset($form['field_aviso_video'][0]['emthumb']['emthumb']['emthumb']['emthumb']['description']);
            $form['field_aviso_video'][0]['emvideo']['delete']['#title'] = 'Eliminar Video';
            $form['field_aviso_video'][0]['emvideo']['delete']['#description'] = '<span class="Ingresa">Marque la casilla para eliminar el video.</span>';
            print drupal_render($form['field_aviso_video']);
          ?>
        </div>
        <?php } ?>
      </fieldset>
<?php if(isset($form['taxonomy'][6])): ?>
      <fieldset class="confort">
        <h3>Confort</h3>
        <div class="Row clearfix">
          <?php
            $form['taxonomy'][6]['#title'] = '';
            print drupal_render($form['taxonomy'][6]);
          ?>
        </div>
      </fieldset>
<?php endif; ?>
<?php if(isset($form['taxonomy'][7])): ?>
      <fieldset class="seguridad">
        <h3>Seguridad</h3>
        <div class="Row clearfix">
          <?php
            $form['taxonomy'][7]['#title'] = '';
            print drupal_render($form['taxonomy'][7]);
          ?>
        </div>
      </fieldset>
<?php endif; ?>
      <?php if(isset($form['captcha'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(isset($form['captcha_google'])): ?>
      <fieldset>
        <div class="Row clearfix">
          <?php
            print drupal_render($form['captcha_google']);
          ?>
        </div>
      </fieldset>
      <?php endif; ?>
      <?php if(!user_is_logged_in()) { ?>
      <fieldset> 
        <div class="Row clearfix condiciones">
          <?php
            print drupal_render($form['profile_condiciones']);
          ?>
        </div>
        <div class="Row clearfix ofertas">
          <?php
            print drupal_render($form['profile_recepcion_ofertas']);
          ?>
        </div>
      </fieldset>
      <?php } ?>
      
      <?php if(user_is_logged_in() && isset($form['datos_privados']['propietario'])) { ?>
      <fieldset class="datos_privados">
        <h3>Datos de Agenda de Contactos (Datos Privados)</h3>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['vendedor']); 
          ?>
        </div>
        <div class="Row clearfix">
          <?php 
            print drupal_render($form['datos_privados']['comentario']); 
          ?>
        </div>
      </fieldset>
      <?php } ?>
      
      <div class="alertaLegal">
        <div class="Texto"> 
          Está expresamente prohibido ofrecer o publicar material que quebrante cualquier legislación vigente (ejemplo Decreto Nacional 936/11). Tampoco está permitido publicar material inmoral, obsceno, pornográfico, discriminatorio, injurioso o cualquier otro contenido que afecte la privacidad de las personas, la propiedad intelectual o que sea contrario a las buenas costumbres. 
        </div>
      </div>
      <div class="Row clearfix">
        <div class="Ingresa">
          <div class="Boton">
            <div class="Cv Tl"></div>
            <div class="Cv Tr"></div>
            <div class="Cv Bl"></div>
            <div class="Cv Br"></div> 
            <?php 
              unset($form['buttons']['preview']);
              unset($form['buttons']['delete']);
              if($user->uid != 0){
                if(arg(1) == 'add')
                  $form['buttons']['submit']['#value'] = "Siguiente >";
                elseif(arg(2) == 'clone')
                  $form['buttons']['submit']['#value'] = "Copiar";
              }
              print drupal_render($form['buttons']['submit']); 
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-sin-mostrar" style="display:none;">
  <?php
    unset($form['language']);
    unset($form['workflow']);
    print drupal_render($form);
  ?>
  </div>
</div>
