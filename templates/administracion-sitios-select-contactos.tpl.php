<?php
  //print_r($form);
?>
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/js/lib/colorbox/styles/default/colorbox_default_style.css" />
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/css/configuracion.css">
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/custom_new/administracion_sitios/font-awesome/css/font-awesome.min.css">
<!-- Page Content -->
<div id="main" class="container-fluid page-info">                        
  <!-- Boxed Container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-6" style="overflow: hidden;">
        <div class="title-section info">
          <h1>Info quienes somos</h1>
          <h4>Ingresá los datos de tu inmobiliaria para personalizar</br>toda la información que quieras publicar.</h4>
        </div>
          <div class="input-group">
            <?php print drupal_render($form['nombre-comercial']); ?>
            <span class="input-group-btn">
              <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
            </span>
          </div>
          <div class="form-group quienes-somos">
            <?php print drupal_render($form['quienes-somos']); ?>
          </div>                      
        <div class="row">
          <!-- <div class="col-lg-4 pull-right padding-left"><button type="button" class="btn btn-default btn-green">GUARDAR</button></div> -->
          <div class="col-lg-4 pull-right padding-left"><button type="button" class="btn btn-default btn-green off">BORRAR</button></div>
        </div>                                       
      </div>
      <div class="col-lg-6">
        <div class="title-section contactenos">
          <h1>Contáctenos</h1>
          <h4>Ingresá los datos de tu inmobiliaria para personalizar</br>toda la información que quieras publicar.</h4>
        </div>         
        <div class="input-group">
          <?php 
            $default_provincia = $form['provincia']['#default_value'];
            $default_provincia_nombre = (!empty($default_provincia)) ? $form['provincia']['#options'][$default_provincia] : '<span style="font-size:17px;color:#26AD5F;">Provincia</span>';
          ?>
          <button id="select-contacto-provincia" class="btn btn-default btn-lg dropdown-toggle select-contacto" type="button" data-toggle="dropdown" aria-expanded="false"><?php print $default_provincia_nombre; ?></button>
          <ul class="dropdown-menu lista-contacto" role="menu">
            <?php foreach($form['provincia']['#options'] as $kprovincia => $provincia) { ?>
              <li><a href="javascript:void(0);" class="contacto_provincia" id="provincia-<?php print $kprovincia; ?>"><?php print $provincia; ?></a></li>
            <?php } ?>
          </ul>
          <span class="input-group-btn">
            <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
          </span>                        
        </div>
        <div class="input-group">
          <?php 
            $default_localidad = $form['ciudad']['#default_value'];
            $default_localidad_nombre = (!empty($default_localidad)) ? $form['ciudad']['#options'][$default_localidad] : '<span style="font-size:17px;color:#26AD5F;">Localidad</span>';
          ?>
          <button id="select-contacto-localidad" class="btn btn-default btn-lg dropdown-toggle select-contacto" type="button" data-toggle="dropdown" aria-expanded="false"><?php print $default_localidad_nombre; ?></button>
          <ul class="dropdown-menu lista-contacto" role="menu">
            <?php foreach($form['ciudad']['#options'] as $kciudad => $ciudad) { ?>
              <li><a href="javascript:void(0);" class="contacto_localidad" id="localidad-<?php print $kciudad; ?>"><?php print $ciudad; ?></a></li>
            <?php } ?>
          </ul>
          <span class="input-group-btn">
            <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
          </span>                        
        </div>
        <div class="row">
          <div class="col-lg-5 padding-right">                      
            <div class="input-group">
              <?php 
                $default_barrio = $form['barrio']['#default_value'];
                $default_barrio_nombre = (!empty($default_barrio)) ? $form['barrio']['#options'][$default_barrio] : '<span style="font-size:17px;color:#26AD5F;">Barrio</span>';
              ?>
              <button id="select-contacto-barrio" class="btn btn-default btn-lg dropdown-toggle select-contacto" type="button" data-toggle="dropdown" aria-expanded="false"><?php print $default_barrio_nombre; ?></button>
              <ul class="dropdown-menu lista-contacto" role="menu">
                <?php foreach($form['barrio']['#options'] as $kbarrio => $barrio) { ?>
                  <li><a href="javascript:void(0);" class="contacto_barrio" id="barrio-<?php print $kbarrio; ?>"><?php print $barrio; ?></a></li>
                <?php } ?>
              </ul>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                         
            </div>
          </div>
          <div class="col-lg-4 padding-right padding-left">                      
            <div class="input-group">
              <?php print drupal_render($form['calle']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div>
          </div>
          <div class="col-lg-3 padding-left">
            <div class="input-group">
              <?php print drupal_render($form['altura']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 padding-right">                      
            <div class="input-group">
              <?php print drupal_render($form['piso']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div> 
          </div>
          <div class="col-lg-4 padding-right padding-left">                      
            <div class="input-group">
              <?php print drupal_render($form['dpto']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div> 
          </div>
          <div class="col-lg-4 padding-left">                      
            <div class="input-group">
              <?php print drupal_render($form['cp']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div> 
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 padding-right">
            <div class="input-group">
              <?php print drupal_render($form['telefono1']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div> 
          </div>
          <div class="col-lg-6 padding-left">
            <div class="input-group">
              <?php print drupal_render($form['telefono2']); ?>
              <span class="input-group-btn">
                <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
              </span>                        
            </div> 
          </div>
        </div>
        <div class="input-group">
          <?php print drupal_render($form['mail-contacto']); ?>
          <span class="input-group-btn">
            <button class="btn btn-default btn-edit" type="button"><i class="fa fa-pencil-square-o"></i></button>
          </span>                        
        </div> 
        <div class="input-group" style="margin-bottom: 30px;">
          <?php print drupal_render($form['mapa']); ?>
          <span class="input-group-btn">
            <button class="btn btn-default btn-edit inline" type="button" href="#content-mapa" id="edit-mapa-button"><i class="fa fa-pencil-square-o"></i></button>
          </span>                        
        </div>
        <!-- <button type="button" class="btn btn-default btn-green">AGREGAR NUEVA DIRECCIÓN</button> -->
      </div>                
    </div>
    <div class="col-md-4 col-md-offset-4 button-footer" style="margin-bottom: 50px;">
      <?php print drupal_render($form['contactos-submit']); ?>
    </div>             
  </div>
  <!-- /.row -->
  <div style="display:none;"><?php print drupal_render($form); ?></div>
  <div id="content-mapa">
    <div class="lista-mapa" id="map-canvas" style="width: 400px; height: 300px;"></div>
    <div>Has clíck en el mapa para marcar la nueva posición.</div>
  </div>
</div>
<!-- /.container -->
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/jquery.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/lib/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?key=<?php print variable_get('googlemap_api_key', ''); ?>"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/lib/gmaps.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/lib/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/modules/custom_new/administracion_sitios/js/javascripts/contactos.js"></script>
<script type="text/javascript">
  var map;
  function GetMapaFicha() {
    var latitud = <?php (!empty($form['map-latitud']['#default_value']))? print $form['map-latitud']['#default_value'] : print '-31.400'; ?>;
    var longitud = <?php (!empty($form['map-longitud']['#default_value']))? print $form['map-longitud']['#default_value'] : print '-64.183'; ?>;
    map = new GMaps({
      div: '#map-canvas',
      lat: latitud,
      lng: longitud
    });
    if(latitud != '' && longitud != '') {
      map.addMarker({
        lat: latitud,
        lng: longitud
      });
    }
    
    GMaps.on('click', map.map, function(event) {
      map.removeMarkers();
      var index = map.markers.length;
      var lat = event.latLng.lat();
      var lng = event.latLng.lng();
      $("#edit-map-latitud").val(lat);
      $("#edit-map-longitud").val(lng);
      $("#edit-mapa").val(lat + ', ' + lng);
      map.addMarker({
        lat: lat,
        lng: lng
      });
    });
    
  }
  $(document).ready(function(){
    GetMapaFicha();
    $("#edit-mapa-button").click(function(){
      $.colorbox({
        href:'#content-mapa',
        inline: true,
        width: "31%"
      });
      return false;
    });
  });
</script>