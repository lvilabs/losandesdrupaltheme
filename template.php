<?php
/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to losandes_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: losandes_breadcrumb()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/**
 * Implementation of HOOK_theme().
 */
function losandes_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  $hooks['user_register'] = array('arguments'=>array('form'=>null),'template'=>'templates/user-register');
  //$hooks['user_register'] = array('arguments'=>array('form'=>null),'template'=>'templates/user-login-en-mantenimiento');
  $hooks['user_pass'] = array('arguments'=>array('form'=>null),'template'=>'templates/user-pass');
  //$hooks['user_login'] = array('arguments'=>array('form'=>null),'template'=>'templates/user-login-en-mantenimiento');

  //forms hooks
  $nodetypes_selected = sitio_local_obtener_tipos_avisos_node_types();
  
  foreach($nodetypes_selected as $node_type) {
    $node_type_tmp = str_replace('_', '-', $node_type);
    //Determino que tipo de contenido estoy por crear para cargar su correspondiente tpl del formulario.
    //$template = 'templates/user-login-en-mantenimiento';
    $rubro_padre = clasificados_rubros_obtener_rubro_padre_tipo_aviso($node_type);
    if($rubro_padre){
      if(preg_match('/^vehículos$/i', trim($rubro_padre->name))) {
        $template = 'templates/node-autoslavoz-aviso-form';
      } elseif(preg_match('/^inmuebles$/i', trim($rubro_padre->name))) {
        $template = 'templates/node-inmuebleslavoz-aviso-form';
      } else {
        $template = 'templates/node-principallavoz-aviso-form';
      }
    } else {
      $template = 'templates/node-principallavoz-aviso-form';
    }
    if(file_exists(path_to_theme().'/'.$template.'.tpl.php')) {
      $hooks[$node_type.'_node_form'] = array('arguments'=>array('form'=>null),'template'=>$template);
    }
  }
  //--

  $hooks['publicacion_avisos_form_opciones_publicacion'] = array('arguments'=>array('form'=>null),'template'=>'templates/avisos-publicacion');
  $hooks['user_profile_form'] = array('arguments'=>array('form'=>null),'template'=>'templates/user-profile-form');
  $hooks['compra_paquete_form'] = array('arguments'=>array('form'=>null),'template'=>'templates/compra-paquete-form');
  $hooks['clasificados_contratos_abono_mensual'] = array('arguments'=>array('form'=>null),'template'=>'templates/contratos-abono-mensual-form');
  $hooks['clasificados_web_papel_lista_avisos'] = array('arguments'=>array('form'=>null),'template'=>'templates/web-papel-lista-avisos-form');
  
  $hooks['administracion_avisos_principal'] = array('arguments'=>array('form'=>null),'template'=>'templates/page-principal-administrador-avisos');
  $hooks['estadisticas_globales_reporte'] = array('arguments'=>array('form'=>null),'template'=>'templates/page-estadisticas-globales-administrador-avisos');
  $hooks['estadisticas_globales_reporte_autos'] = array('arguments'=>array('form'=>null),'template'=>'templates/page-estadisticas-globales-autos-administrador-avisos');
  $hooks['favoritos_historial_avisos'] = array('arguments'=>array('form'=>null),'template'=>'templates/page-favoritos-historial-avisos');
  
  //Administrador de sitios
  if(module_exists('administracion_sitios')){
    $hooks['administracion_sitios_principal'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-principal');
    $hooks['administracion_sitios_editor_template'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-editor-template');
    $hooks['administracion_sitios_select_templates'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-select-templates');
    $hooks['administracion_sitios_select_fuentes'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-select-fuentes');
    $hooks['administracion_sitios_select_multimedia'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-select-multimedia');
    $hooks['administracion_sitios_select_contactos'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-select-contactos');
    $hooks['administracion_sitios_alta'] = array('arguments'=>array('form'=>null),'template'=>'templates/administracion-sitios-alta');
  }
    
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function losandes_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function losandes_preprocess_page(&$vars, $hook) {
  global $user;
  global $base_root;
  //Cuando el theme es "losandes" se debe cargar siempre el colorbox
  if(module_exists('colorbox')) {
    $get_old = $_GET['q'];
    $_GET['q'] = '-cualquiera-/-cualquiera-'; //para ignorar el filtro de colorbox
    _colorbox_doheader();
    $_GET['q'] = $get_old; //importante!
  }
  
  //¿Estamos en la pre-home de autos?
  if($vars['title'] == 'Home Autos'){
    $vars['template_files'][] = 'page-prehome-autos';
  }
  //¿Estamos en la pre-home de inmuebles?
  if($vars['title'] == 'Home Inmuebles') {
    $vars['template_files'][] = 'page-prehome-inmuebles';
  }
  
  //Redirigir de ficha de usuario a formulario
  if(arg(0) == 'user' && $user->uid != 0 && arg(2) != 'edit' && $user->uid == arg(1)){
    drupal_goto('user/'.$user->uid.'/edit');
  }
  //Redirigir si viene de mobile
  //Redireccion buscador
  $arg = arg();
  if($arg[0] == 'search' && $arg[1] == 'result'){
    $key = '';
    $filter_text = '';
    $filters = array();
    $page = '';
    foreach($arg as $argum){
      if(strpos($argum,'filters=')!==false){
        $filters_array = explode('filters=', $argum);
        $filters_array = explode(' ', $filters_array[1]);
        foreach($filters_array as $key => $filtro) {
          $data_filtro = explode(':', $filtro);
          if(count($data_filtro) > 1) {
            // Si es taxonomia cambia el filtro
            if(strpos($filtro, 'tid:') !== FALSE) {
              $term = taxonomy_get_term($data_filtro[1]);
              $filters['f'][] = 'im_taxonomy_vid_'.$term->vid.':'.$term->tid;
            // Filtros especiales
            } elseif(in_array($data_filtro[0], array('fs_precio', 'is_moneda_k', 'fs_kilometros', 'fs_superficie_terreno', 'iss_anio'))) {
              // Si es desde - hasta
              if(strpos($filtro, ':[') !== FALSE) {
                $filtro = $filters_array[$key].' '.$filters_array[$key+1].' '.$filters_array[$key+2];
              }
              $filters['fq'][] = $filtro;
            // Filtro normal
            } else {
              $filtro = str_replace('"', '', $filtro);
              $filters['f'][] = $filtro;
            }
          }
          
        }
      } elseif(strpos($argum,'page=')!==false) {
        $argum = explode('page=', $argum);
        $filters['page'] = $argum[1];
      } elseif(!in_array($argum, array('search', 'result'))) {
        $key = $argum;
      }
    }
    drupal_goto('search/apachesolr_search/'.$key, $filters, null, 301);
  }
  
  $es_page_403 = FALSE;
  $es_page_404 = FALSE;
  if (strpos(drupal_get_headers(), '403 Forbidden') !== FALSE) {
    $es_page_403 = TRUE;
    $vars['page_columna_derecha'] = ''; // Ocultamos la columna derecha
    $vars['page_inferior'] = ''; // Ocultamos la regin inferior donde estan las ofertas similares
    $vars['head'] .= '<meta name="robots" content="noindex">';
  }
  if (strpos(drupal_get_headers(), '404 Not Found') !== FALSE) {
    $es_page_404 = TRUE;
    $vars['page_columna_derecha'] = ''; // Ocultamos la columna derecha
    $vars['page_inferior'] = ''; // Ocultamos la regin inferior donde estan las ofertas similares
  }
    
  $vars['es_ficha'] = FALSE; //Determina si se esta viendo o no la ficha de un aviso.
  $tipos_avisos_nodetypes = sitio_local_obtener_tipos_avisos_node_types();
  $pages_node_add = array();
  foreach($tipos_avisos_nodetypes as $nodetype) {
    $pages_node_add[] = 'page-node-add-'.str_replace('_', '-', $nodetype);
  }

  if(isset ($vars['template_files']) && array_intersect($pages_node_add, $vars['template_files'])) {
    $vars['classes_array'][] = 'page-autoslavoz-node-add-aviso'; //body class
  }

  //No Title
  $pages_show_no_title = array(
    'page-search', 'page-guias', 'page-noticias', 'page-front',
    'page-registro-particular', 'page-registro-comercio', 'page-taxonomy-term', 'page-user-password', 'page-node-confirmar-publicacion', 'section-comprar', 'page-user', 'page-administrar-historial'
  );
  $pages_show_no_title = array_merge($pages_show_no_title, $pages_node_add);
  $pages_show_no_title[] = 'page-node-5910'; //Condiciones de Uso (/legales)
  if((isset ($vars['template_files']) && array_intersect($pages_show_no_title, $vars['template_files']) && !$es_page_403) || $vars['node']->type == 'nota') {
    $vars['title'] = ''; //no title
  }
  
  if(isset($vars['template_files']) && in_array('page-front', $vars['template_files'])) {
    if(isset($_GET['publica']) && $_GET['publica']==1)
      drupal_add_js(array('mostrar_modal_publicacion_rubros' => TRUE), 'setting');
  }  
  
  // general a todo el sitio
  drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/font-awesome.min.css', 'theme');
  drupal_add_css(drupal_get_path('theme', 'losandes') . '/fonts/stylesheet.css', 'theme');
  $vars['head'] .= '<meta property="fb:pages" content="" />';
  
  //Home
  if(isset ($vars['template_files']) && (array_intersect(array('page-front'), $vars['template_files']) || in_array($vars['title'] , array('Home Autos', 'Home Inmuebles', 'Productos')))) {
    //Tengo que informarle al JavaScript home.js cuales Rubros poseen Modelos y cuales No.    
    $array_rubros = clasificados_rubros_obtener_rubros();
    $rubro_tiene_modelo = array();
    $nid_rubro_emprendimientos = -1;
    foreach ($array_rubros as $rubro) {
      $vocabulary_vid = clasificados_rubro_obtener_marca_modelo_vocabulary_id($rubro->tid);
      $vocabulary_mm = taxonomy_vocabulary_load($vocabulary_vid);
      if(isset($vocabulary_mm->multiple) && $vocabulary_mm->multiple) {
        $rubro_tiene_modelo[$rubro->tid] = TRUE;
      } else {
        $rubro_tiene_modelo[$rubro->tid] = FALSE;
      }
      if($rubro->title == 'Emprendimientos')
        $nid_rubro_emprendimientos = $rubro->tid;
    }
    drupal_add_js(array('rubro_tiene_modelo' => $rubro_tiene_modelo), 'setting');
    drupal_add_js(array('nid_rubro_emprendimientos' => $nid_rubro_emprendimientos), 'setting');
    //--
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.accessible-news-slider.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/destacados_home.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/responsiveslides.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/home.js', 'theme');
    if(array_intersect(array('page-front'), $vars['template_files'])) {
      drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.lockfixed.js', 'theme');
    }
    //Si es Prehome de Productos
    if($vars['title'] == 'Productos'){
        drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/home-productos.css', 'theme');
    }

    $vars['scripts'] = drupal_get_js();
  }
  
  //Nodos de Fichas  
  $es_ficha = isset ($vars['node']) && sitio_local_es_nodo_aviso($vars['node']->type) && !isset($vars['template_files'][2]);
  if($es_ficha && ($es_page_403 || $es_page_404)) {
    $es_ficha = FALSE;
  }
  if($es_ficha) {
      $node_info = new stdClass();
    $node_info->title  = $vars['node']->title;
    $node_info->type  = $vars['node']->type;
    $precio = $vars['node']->field_aviso_precio[0]['view'];    
    if($precio != 'consultar'){
      //Detectamos si es un precio desde - hasta
      if(strpos($precio, 'a') !== false){
        $precio_exp = explode('a', $precio);
        $node_info->precio_hasta = $precio_exp[1];
        $precio_desde = explode(' ', $precio_exp[0]);
        $precio = implode('', $precio_desde);
      } else {
        $precio_exp = explode(' ', $precio);
        $precio = implode('', $precio_exp);
      }
    }
    $node_info->precio = $precio;
    if($vars['node']->field_aviso_moneda[0]['value'] == 2) $moneda = 'USD'; else $moneda = 'ARS';
    $node_info->moneda = $moneda;
    $node_info->uid = $vars['node']->uid;
    if($vars['node']->disponible_venta) $vendible = 1; else $vendible = 0;
    $node_info->vendible = $vendible;
    $vars['title'] = ''; //no title
    $vars['node_info'] = $node_info;
    
    //Breadcrumb Ficha
    $tipo_aviso = '';
    $tipo_aviso_hijo = '';
    $marca = '';
    $modelo = '';
    $barrio = '';
    foreach($vars['node']->taxonomy as $kterm => $dterm){
      //Tipo de aviso
      if($dterm->vid == CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID){
        $padre = taxonomy_get_parents($kterm);
        if(empty($padre)) {
          $tipo_aviso = $dterm->name;
        } else {
          if(clasificados_rubros_node_pertenece_al_grupo_autos($vars['node']->type) || clasificados_rubros_node_pertenece_al_grupo_inmuebles($vars['node']->type)){
            $tipo_aviso_hijo = $dterm->name;
          } else {
            $hijos = taxonomy_get_children($kterm);
            if(!empty($hijos)){
              $tipo_aviso_hijo = $dterm->name;
            }
          }
        }
      }
      //Marca y Modelo
      if(in_array($dterm->vid, array(CLASIFICADOS_MARCAS_AUTOS_TAXONOMY_VID, CLASIFICADOS_MARCAS_MOTOS_TAXONOMY_VID, CLASIFICADOS_MARCAS_UTILITARIOS_TAXONOMY_VID, CLASIFICADOS_MARCAS_4X4_TAXONOMY_VID, CLASIFICADOS_MARCAS_PLAN_AHORRO_TAXONOMY_VID))){
        $padre = taxonomy_get_parents($kterm);
        if(empty($padre)) {
          $marca = $dterm->name;
        } else {
          $modelo = $dterm->name;
        }
      }
      //Barrio
      if(in_array($dterm->vid, array(AUTOSLAVOZ_PROVINCIA_TAXONOMY_VID)) && (clasificados_rubros_node_pertenece_al_grupo_inmuebles($vars['node']->type) || $vars['node']->type == 'aviso_alquiler_temporario')){
        $sin_hijos = taxonomy_get_children($kterm);
        if(empty($sin_hijos)){
          $barrio = $dterm->name;
        }
      }
    }
    $precio = $vars['node']->field_aviso_precio[0]['view'];
    if(clasificados_rubros_node_pertenece_al_grupo_autos($vars['node']->type)){
      $vars['breadcrumb_ficha'] = $tipo_aviso_hijo;
      if($marca != '')
        $vars['breadcrumb_ficha'] .= ' - '.$marca;
      if($modelo != '')
        $vars['breadcrumb_ficha'] .= ' - '.$modelo;
      if(!empty($vars['node']->field_aviso_usado_anio[0]['value'])){
        $anio = explode('-', $vars['node']->field_aviso_usado_anio[0]['value']);
        $vars['breadcrumb_ficha'] .= ' - '.$anio[0];
      }
      if(!empty($vars['node']->field_aviso_kilometros[0]['value']))
        $vars['breadcrumb_ficha'] .= ' - '.$vars['node']->field_aviso_kilometros[0]['view'].' km';
      $vars['breadcrumb_ficha'] .= ' - '.$precio;
    } elseif(clasificados_rubros_node_pertenece_al_grupo_inmuebles($vars['node']->type)) {
      $vars['breadcrumb_ficha'] = $tipo_aviso_hijo.' - '.$barrio.' - '.$vars['node']->field_aviso_operacion[0]['view'];
      if(isset($vars['node']->field_aviso_cantidad_dormitorios) && !empty($vars['node']->field_aviso_cantidad_dormitorios[0]['view'])){
        $vars['breadcrumb_ficha'] .= ' - '.$vars['node']->field_aviso_cantidad_dormitorios[0]['view'];
      }
      $vars['breadcrumb_ficha'] .= ' - '.$precio;
    } elseif($vars['node']->type == 'aviso_alquiler_temporario') {
      $vars['breadcrumb_ficha'] = $tipo_aviso.' - '.$barrio;
      if(isset($vars['node']->field_aviso_cantidad_dormitorios) && !empty($vars['node']->field_aviso_cantidad_dormitorios[0]['view'])){
        $vars['breadcrumb_ficha'] .= ' - '.$vars['node']->field_aviso_cantidad_dormitorios[0]['view'];
      }
      $vars['breadcrumb_ficha'] .= ' - '.$precio;
    } else {
      $vars['breadcrumb_ficha'] = $tipo_aviso.' - '.$tipo_aviso_hijo;
      if(isset($vars['node']->field_aviso_usado) && !empty($vars['node']->field_aviso_usado[0]['view'])){
        $vars['breadcrumb_ficha'] .= ' - '.$vars['node']->field_aviso_usado[0]['view'];
      }
      $vars['breadcrumb_ficha'] .= ' - '.$precio;
    }
    
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.lockfixed.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.accessible-news-slider.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/node-aviso.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/contacto_vendedor.js', 'theme');
    
    if(isset($vars['node']->disponible_venta) && $vars['node']->disponible_venta==1)
      drupal_add_js(drupal_get_path('module', 'ventas') . '/js/ventas.js', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/fichaAuto.css', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/block-formblock-contacto_vendedor.css', 'theme');

    $vars['styles'] = drupal_get_css();
    $vars['scripts'] = drupal_get_js();
    $vars['es_ficha'] = TRUE;
    $vars['esta_publicado'] = ($vars['node']->_workflow == WORKFLOW_AVISOS_ESTADO_PUBLICADO) && $vars['node']->visualizacion['sitio_id'] == SITIO_LOCAL_ID;
  }
  
  //Nodos de Buscador, Taxonomias y Listado de Administrador  
  if(isset ($vars['template_files']) && array_intersect(array('page-search', 'page-taxonomy', 'page-administrar'), $vars['template_files'])) {
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/aviso-teaser.css', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/search-result.css', 'theme');
    if(in_array('page-administrar', $vars['template_files'])) {
      drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/admin-buttons.css', 'theme');
      drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/admin-avisos.css', 'theme');
    } else {
      drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/node-search.js', 'theme');
      drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/block-apachesolr_search.css', 'theme');
    }
    if(module_exists('jquery_update')) {
      drupal_add_js(drupal_get_path('theme', 'losandes').'/js/jquery.ui.1.7.3/js/minified/ui.core.min.js');
      drupal_add_js(drupal_get_path('theme', 'losandes').'/js/jquery.ui.1.7.3/js/minified/ui.slider.min.js');
      drupal_add_css(drupal_get_path('theme', 'losandes').'/js/jquery.ui.1.7.3/css/ui-lightness/jquery-ui-1.7.3.custom.css');
    }

    $vars['scripts'] = drupal_get_js();
    $vars['styles'] = drupal_get_css();
  }
  
  //Nodos de Notas
  if(isset ($vars['template_files']) && ((isset($vars['node']) && $vars['node']->type == "nota") || in_array('page-guias', $vars['template_files']) || in_array('page-noticias', $vars['template_files']))) {
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/stepcarousel.js', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/nota.css', 'theme');
    $vars['scripts'] = drupal_get_js();
    $vars['styles'] = drupal_get_css();
  }
  
  //Paginas de usuarios (Inmobiliarias, Concesionarias y Tiendas)
  if(in_array('page-inmobiliarias', $vars['template_files'])) {
    $vars['title'] = 'Inmobiliarias que publican en Clasificados Los Andes';
    $vars['head_title'] = 'Inmobiliarias que publican en Clasificados Los Andes';
  }
  if(in_array($vars['title'] , array('Tiendas La Voz'))) {
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/home.js', 'theme');
  }
    
  //Páginas del administrador del usuario
  $array_admin = array();
  if($user->uid!=0) {
    $array_admin = array('page-contactos_recibidos', 'page-administrar', 'page-estado_cuenta', 'page-node-edit', 'page-node-clone', 'page-dineromail-confirmar', 'page-node-publicacion', 'page-compra-paquetes', 'page-user', 'page-node-eliminar', 'page-node-despublicar', 'page-lista-colaboradores', 'page-add-colaborador', 'page-edit-colaborador', 'page-contactos_recibidos_colaboradores', 'page-compra', 'page-abono-mensual', 'page-compra-papel', 'page-add-colaborador');
  }
  $array_admin = array_merge($array_admin, $pages_node_add);
  if(isset($vars['template_files']) && is_array($array_admin) && array_intersect($array_admin, $vars['template_files'])) {
    if(!array_intersect(array('page-node-eliminar', 'page-node-despublicar', 'page-compra'), $vars['template_files']))
      $vars['title'] = ''; //no title
    $vars['template_files'][] = 'page-admin';
    
    //Tooltips
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/opentip.css', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/opentip-jquery.js', 'theme');
    
    //Manejo de Cookies
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/js.cookie.js', 'theme'); 
    
    // Carga de mapa OpenLayer
    if(in_array('page-node-edit', $vars['template_files']) || in_array('page-node-add', $vars['template_files'])) {
      drupal_add_js('//--><!]]></script><script type="text/javascript" src="http://www.openlayers.org/api/OpenLayers.js"></script><script type="text/javascript"><!--//--><![CDATA[//><!--',  'inline');
    }
        
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/admin-avisos.css', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/modal-info.css', 'theme');
    $vars['styles'] = drupal_get_css();
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.lockfixed.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/admin.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/listado-admin-avisos.js', 'theme');
    $vars['scripts'] = drupal_get_js();
  }
  
  // Nuevo Colorbox
  // Cuando el theme es "losandes" se debe cargar siempre el colorbox
  drupal_add_js('sites/all/libraries/colorbox/colorbox/jquery.colorbox-min-1.6.3.js', 'theme');
  if(module_exists('colorbox')) {
    $get_old = $_GET['q'];
    $_GET['q'] = '-cualquiera-/-cualquiera-'; //para ignorar el filtro de colorbox
    _colorbox_doheader();
    $_GET['q'] = $get_old; //importante!
  }
    
  if (isset ($vars['node']) && $vars['node']->nid == AUTOSLAVOZ_PAGE_IMPRIMIR_NID) {

    //Al imprimir tambien se necesita el CSS de la ficha
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/fichaAuto.css', 'theme');
    $vars['styles'] = drupal_get_css();

    // Construct page title
    $snode = node_load($_GET['nid']);

    if($snode && sitio_local_es_nodo_aviso($snode->type)) {
      //Estos son los tipos de nodos que se pueden imprimir utilizando el page-imprimir.tpl.php
    }
    else {
      $snode = false;
    }
    $node_to_show = FALSE;
    
    if ($snode && !empty($snode->title)) {
      $head_title = array(strip_tags($snode->title), variable_get('site_name', 'Pressflow'));
      $node_to_show = node_view($snode, FALSE, TRUE);
      $node_info = new stdClass();
      $node_info->title  = $snode->title;
      $node_info->precio = $snode->field_aviso_precio[0]['view'];;
      $vars['node_info'] = $node_info;
    }
    else {
      $head_title = array(variable_get('site_name', 'Pressflow'));
      if (variable_get('site_slogan', '')) {
        $head_title[] = variable_get('site_slogan', '');
      }
    }
    $vars['head_title']       = implode(' | ', $head_title);

    $vars['node_to_show']     = $node_to_show;

    $vars['template_files']   = array();
    $vars['template_files'][] = 'page-imprimir';
  }
  
  if(in_array('page-widgetlvi', $vars['classes_array'])) {
    $vars['template_files'][] = 'page-widgetlvi';
  }
  // To remove a class from $classes_array, use array_diff().
  //$vars['classes_array'] = array_diff($vars['classes_array'], array('class-to-remove'));
  
  //Formularios de registro
  if(isset($vars['template_files']) && (in_array('page-registro-particular', $vars['template_files']) || in_array('page-registro-comercio', $vars['template_files']) || in_array('page-user-edit', $vars['template_files']))) {
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/form-register.js', 'theme');
    $vars['scripts'] = drupal_get_js();
  }
  
  //Formularios de avisos
  $form_templates = array();
  if(isset($vars['template_files'])) {
    $form_templates = array('page-node-edit','page-node-publicacion','page-node-clone');
    $form_templates = array_merge($form_templates, $pages_node_add);
  }
  if(array_intersect($form_templates, $vars['template_files'])) {
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/form-aviso.css', 'theme');
    $vars['styles'] = drupal_get_css();
    
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.textareaCounter.plugin.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/jquery.numeric.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/form-aviso.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/form-aviso-rango-cuotas.js', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/flash_detect.js', 'theme');
    $vars['scripts'] = drupal_get_js();
  }

  //FAQ page
  if(isset ($vars['template_files']) && (in_array('page-faq', $vars['template_files']))) {
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/style_reset.css', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/style_layout.css', 'theme');
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/style_fonts.css', 'theme');

    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/page-faq.js', 'theme');

    $vars['styles'] = drupal_get_css();
    $vars['scripts'] = drupal_get_js();
  }

  //Page Contact
  if(isset ($vars['template_files']) && in_array('page-contact', $vars['template_files'])) {
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/form-contact.css', 'theme');
    
    $vars['styles'] = drupal_get_css();
  }
  
  //Formulario compra de paquetes
  if(isset($vars['template_files']) && in_array('page-compra-paquetes', $vars['template_files'])) {
    drupal_add_css(drupal_get_path('theme', 'losandes') . '/styles/form-compra-paquetes.css', 'theme');
    drupal_add_js(drupal_get_path('theme', 'losandes') . '/js/form-compra-paquete.js', 'theme');
    $vars['styles'] = drupal_get_css();
    $vars['scripts'] = drupal_get_js();
  }
  $vars['es_nuevo_usuario_registrado'] = FALSE;
  if(isset($_SESSION['nuevo_usuario_registrado'][$user->uid])) {
    $vars['es_nuevo_usuario_registrado'] = TRUE;
    unset($_SESSION['nuevo_usuario_registrado'][$user->uid]);
    if(count($_SESSION['nuevo_usuario_registrado']) == 0) {
      unset($_SESSION['nuevo_usuario_registrado']);
    }
  }
  //quitamos un link rss en paginas de taxonomias
  if(isset($vars['template_files']) && in_array('page-taxonomy-term', $vars['template_files'])) {
    $vars['head'] = preg_replace('/<link rel="alternate" type="application\/rss\+xml" title=".*?" href=".*?" \/>/', '', $vars['head']);
  }
  $vars['styles'] .= $vars['conditional_styles'];
  
  // Cambios para optimización en buscadores (SEO).
  $nombre_sitio = variable_get('site_name', 'Clasificados Los Andes'); 
  if(isset($vars['node']->nid) && !in_array('page-front', $vars['template_files'])) {
    $nodo = $vars['node'];
    if($es_ficha) {
      $marca = '';
      $modelo = '';
      $prefijo = '';
      if(clasificados_rubros_node_pertenece_al_grupo_autos($nodo)) {
        // Es de Autos entonces buscamos marca/modelo año
        $marca_term = _autoslavoz_node_obtener_marca_term($nodo);
        if($marca_term !== FALSE) {
          $marca = $marca_term->name;
          if(stripos($vars['head_title'], $marca_term->name)===FALSE) {
            $prefijo = $marca_term->name.' ';
          }
          $modelo_term = _autoslavoz_node_obtener_modelo_term($nodo);
          if($modelo_term !== FALSE) {
            $modelo = $modelo_term->name;
            if(stripos($vars['head_title'], $modelo_term->name)===FALSE) {
              $prefijo = $prefijo.$modelo_term->name.' ';
            }
          }
          $anio = substr($nodo->field_aviso_usado_anio[0]['value'], 0, 4);
          if(stripos($vars['head_title'], $anio)===FALSE)
            $prefijo = $prefijo.$anio.' ';
        }
        
      }
      $sufijo = '';
      $ubicaciones = clasificados_taxonomy_get_node_terms_for_vocabulary($nodo, AUTOSLAVOZ_PROVINCIA_TAXONOMY_VID);
      if($ubicaciones !== FALSE) {
        $i=0;
        $provincia = FALSE;
        $ciudad = FALSE;
        foreach ($ubicaciones as $tid => $term) {
          if($i==0)
            $provincia = $term->name;
          else {
            $ciudad = $term->name;
            break;
          }
          $i++;
        }
        if($ciudad!=FALSE)
          $sufijo = $sufijo.' en '.$ciudad;
        else if($provincia!=FALSE)
          $sufijo = $sufijo.' en '.$provincia;
      }
      
      $operacion = '';
      if(isset($nodo->field_aviso_operacion[0]['view']))
        $operacion = $nodo->field_aviso_operacion[0]['view'].' de ';
      
      $vars['head_title'] = $prefijo.$nodo->title;
      if(strlen($vars['head_title'])>58)
        $vars['head_title'] = mb_substr($vars['head_title'], 0, 58);
      if(strlen($vars['head_title'])<30) {
        $vars['head_title'] = $vars['head_title'].$sufijo;
        if(strlen($vars['head_title'])<30)
          $vars['head_title'] = $vars['head_title'].' - '.$nombre_sitio;
      }
      
      $rubros = clasificados_taxonomy_get_node_terms_for_vocabulary($nodo, CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID);
      reset($rubros);
      $rubro_padre = current($rubros);
      next($rubros); //evitamos un foreach() para tomar el segundo elemento.
      $rubro = current($rubros);
      $rubro_texto = '';
      if($rubro!==FALSE)
        $rubro_texto = drupal_strtolower($rubro->name).' ';
      if($rubro_texto=='judicial') {
        $rubro_texto = 'aviso '.$rubro_texto.' ';
      }
      if($rubro_padre!==FALSE) {
        if(drupal_strtolower($rubro_padre->name)=='vehículos') {
          $todo = $marca.' '.$modelo;
          if(!empty($nodo->field_aviso_combustible[0]['view']))
            $prefijo .= $nodo->field_aviso_combustible[0]['view'].' ';
        } else {
          $todo = $rubro_texto;
        }
      } else {
        $todo = $rubro_texto;
      }
      $descripcion = $operacion.$rubro_texto.$prefijo.$nodo->title.$sufijo.', todo '.$todo.' en '.$nombre_sitio;
      if(strlen($descripcion)>154)
        $descripcion = mb_substr($descripcion, 0, 154);
      if(mb_strlen($descripcion)<70) // Es corta, agregamos algo extra.
        $descripcion .= '. Ingresá ahora y publicá tu aviso gratis.';
    } else {
      if($nodo->type=='nota') {
        $vars['head_title'] = preg_replace('/\| '.$nombre_sitio.'+/', '- '.$nombre_sitio, $vars['head_title']);
        if(key_exists(AUTOSLAVOZ_GUIA_TAXONOMY_TID, $nodo->taxonomy))
          $descripcion = 'Consejo. '.mb_substr(strip_tags($nodo->field_nota_resumen[0]['view']), 0, 100).' - '.$nombre_sitio;
        else
          $descripcion = 'Noticia. '.mb_substr(strip_tags($nodo->field_nota_resumen[0]['view']), 0, 100).' - '.$nombre_sitio;
      } else {
        if(in_array('page-prehome-autos', $vars['template_files'])) { // Pre home Autos
          $vars['head_title'] = 'Autos, compra y venta de vehículos - '.$nombre_sitio;
          $descripcion = 'Encontrar el auto nuevo o usado que estás buscando. Comprar y vender en Mendoza con'.$nombre_sitio;
        } else if(in_array('page-prehome-inmuebles', $vars['template_files'])) { // Pre home Inmuebles
          $vars['head_title'] = 'Inmuebles, compra, venta y alquileres - '.$nombre_sitio;
          $descripcion = 'Encontrar la casa, departamento o lote que buscas para comprar o alquilar. Conseguir la mayor oferta inmobiliaria de Mendoza con '.$nombre_sitio;
        }
        if(!empty($vars['head_title']))
          $vars['head_title'] = preg_replace('/\| '.$nombre_sitio.'+/', '- '.$nombre_sitio, $vars['head_title']);
      }
    }
  } else {    
    if(in_array('page-front', $vars['template_files'])) { // home
      $vars['head_title'] = 'Avisos clasificados de Autos, Inmuebles, Empleos en Mendoza';
      $descripcion = 'Comprar un auto o producto, alquilar un inmueble, buscar empleo o contratar un servicio en Mendoza con '.$nombre_sitio;
      
    } else if(in_array('page-noticias', $vars['template_files'])) {
      $descripcion = 'Noticias sobre la Industria Automotor, Inmobiliaria y otros en Argentina - '.$nombre_sitio;
      
    } else if(in_array('page-guias', $vars['template_files'])) {
      $descripcion = 'Consejos, cuidados y recomendaciones prácticas sobre el uso, mantenimiento, trámites, documentación y conducción de automóviles en Argentina - '.$nombre_sitio;
      
    } else if(in_array('page-faq', $vars['template_files'])) {
      $vars['head_title'] = 'Ayuda - Preguntas frecuentes de '.$nombre_sitio;
      $descripcion = 'Ayuda de Uso del Sitio - Preguntas más frecuentes (FAQs) de '.$nombre_sitio;
    
    } else if(in_array('page-productos', $vars['template_files'])) { // Pre home Productos
      $vars['head_title'] = 'Oferta de Productos - '.$nombre_sitio;
      $descripcion = 'Aprovechar las promociones y descuentos en productos de '.$nombre_sitio.'. Publicar aviso gratis y vender de todo.';
      
    } else if(in_array('page-ofertas', $vars['template_files'])) { // Pre home Productos
      $vars['head_title'] = 'Ofertas de productos '.$nombre_sitio;
      $descripcion = 'Oferta de productos online en Mendoza con descuentos de '.$nombre_sitio.'. Publicar aviso gratis y vender de todo';
    
    } else if(in_array('page-search', $vars['template_files']) || in_array('page-taxonomy', $vars['template_files'])) {

      $marca = '';
      $modelo = '';
      $provincia = '';
      $ciudad = '';
      $usado = '';
      $rubro = '';
      $subrubro = '';
      $texto_buscado = '';
      $comercio = '';
      $comercio_extra = '';
      $operacion = '';
      $dormitorios = '';
      
      $cxense_events = '';
      
      $env_id = apachesolr_default_environment();
      if(apachesolr_has_searched($env_id)) {
        
        $query = apachesolr_current_query($env_id);
        $termino_busqueda = $query->getParams();
        
        $vars['frase_busqueda'] = $termino_busqueda['q']; // Específico para Site Search en Analytics.
        $vocabularies_marca_modelo = configuraciones_variable_get('sitios_vocabularies_marca_modelo', array());
        $vids_marca_modelo = array_count_values($vocabularies_marca_modelo);
        $vids_marca_modelo[AUTOSLAVOZ_SUBRUBRO_REPUESTOS_TAXONOMY_VID] = 1;
        $rubros_raices = clasificados_rubros_obtener_grupos();
        
        $filtros = $query->getFilters();
        
        foreach($filtros as $filtro) {
          if(strpos($filtro['#name'], 'im_taxonomy_vid') !== FALSE) {
            $term = taxonomy_get_term($filtro['#value']);
            if($term->vid==AUTOSLAVOZ_PROVINCIA_TAXONOMY_VID) {
               if(clasificados_taxonomy_es_ciudad($term->tid)) {
                $ciudad = $term->name;
              } else {
                if(!clasificados_taxonomy_es_barrio($term->tid)) {
                  $provincia = $term->name;
                }
              }
            } else if(key_exists($term->vid, $vids_marca_modelo)) {
              if(clasificados_taxonomy_has_parent($term->tid))
                $modelo = $term->name.' ';
              else
                $marca = $term->name.' ';
            } else if($term->vid==CLASIFICADOS_RUBROS_MIX_TAXONOMY_VID) {
              $padres = taxonomy_get_parents($term->tid);
              if(empty($padres))
                $rubro = $term->name;
              else {
                $padre = current($padres);
                if(key_exists($padre->tid, $rubros_raices)) // Si el padre del término es el raíz entonces el term es el subrubro que buscamos.
                  $subrubro = $term->name;
              }
            }
          }
          
          if($filtro['#name'] == 'ss_usado_v')
            $usado = $filtro['#value'].' ';
          
          if($filtro['#name'] == 'ss_operacion')
            $operacion = $filtro['#value'].' de ';
          if($filtro['#name'] == 'ss_cantidad_dormitorios')
            $dormitorios = $filtro['#value'].' ';
          
          if($filtro['#name'] == 'ss_apto_credito' && $filtro['#value'] == 'Si')
            $cxense_events .= "<script>send_event_cx('apto_credito', '', 'gcl-search');</script>";
          
        }
        if(!empty($rubro))
          $vars['rubro_busqueda'] = $rubro; // Específico para Site Seach en Analytics.
        $texto_buscado = $termino_busqueda['q'];
        if(!empty($texto_buscado))
          $texto_buscado .= ' ';        
        
        $filtros_uid = $query->getFilters('si_uid');
        $filtro_uid = current($filtros_uid);
        if(!empty($filtro_uid)) {
          $usuario = user_load($filtro_uid['#value']);
          if(!empty($usuario)) {
            if(isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_SDCLASS]) || isset($user->roles[AUTOSLAVOZ_ROL_CONCESIONARIA_WEB])) {
              $comercio = $usuario->profile_tipo_concesionaria.' '.$usuario->profile_nombre_comercial.' ';
              $comercio_extra = $usuario->profile_calle.' '.$usuario->profile_altura.' ';
            }
          }
        }
      }
      $rubro_aplicado = 'Autos, inmuebles, rurales, productos y servicios ';
      if(!empty($subrubro))
        $rubro_aplicado = $subrubro.' ';
      else
        if(!empty($rubro))
          $rubro_aplicado = $rubro.' ';
      $ubicacion = '';
      if(!empty($ciudad))
        $ubicacion = 'en '.$ciudad.' ';
      else {
        if(!empty($provincia))
          $ubicacion = 'en '.$provincia.' ';
      }
      // Recomendado Autos: [Tipo de Vehiculo] [Marcas de Autos] [Modelo] [nuevo/usado] en [Provincias y Ciudades] en Clasificados La Voz
      // Recomendado Inmuebles: [Operacion] de [Unidad] [Cantidad de dormitorios] en [Provincias y Ciudades] en Clasificados La Voz
      $title = $operacion.$rubro_aplicado.$marca.$modelo.$dormitorios.$usado.$texto_buscado.$comercio.$ubicacion;
      if(mb_strlen($title) < 30) { // Es corta, agregamos algo extra.
        $title .= 'en resultado de búsqueda';
      }
      $vars['head_title'] = $title;
      // Recomendado: Encontrá [Tipo deVehiculo] [Marcas de Autos] [modelo] [nuevo/usado] en [Provincias y Ciudades] al mejor precio en Autos La Voz
      $descripcion = 'Encontrá '.$operacion.$rubro_aplicado.$marca.$modelo.$dormitorios.$usado.$texto_buscado.$comercio.$comercio_extra.$ubicacion;
      if(mb_strlen($descripcion) < 70) { // Es corta, agregamos algo extra.
        $descripcion .= 'en '.$nombre_sitio.'. Se publica de todo, se vende todo.';
      }
      
      // Tags cxense events
      $vars['cxense_events'] = $cxense_events;
      
      
    } else {
      if(!empty($vars['head_title']))
        $vars['head_title'] = preg_replace('/\| '.$nombre_sitio.'+/', '- '.$nombre_sitio, $vars['head_title']);
    }
  }
  if(!empty($descripcion))
    $vars['head'] .= '<meta name="description" content="'.$descripcion.'">';
  // Quitamos un <meta http-equiv="Content-Type" que está repetido. http://drupal.org/node/451304#comment-1711620
  $vars['head'] = preg_replace('/<meta http-equiv=\"Content-Type\"[^>]*>/', '', $vars['head']);
  if($es_ficha) {
    $vars['head'] .= '<link rel="canonical" href="'.url($vars['node']->path, array('absolute' => TRUE, 'alias' => TRUE)).'"/>'."\r\n";
  }
  //Agregar meta url mobil
  $url_mobile = clasificados_get_url_mobile(request_uri());
  if($url_mobile != ''){
    $url_mobile = LOSANDES_URL_MOBILE.$url_mobile;
    $vars['head'] .= '
<link rel="alternate" media="screen" href="'.$url_mobile.'" />';
  }
  //Microdata category
  if(isset($vars['node'])){
    $data_category = '';
    $micro_data_category = '';
    $categorias = clasificados_obtener_arbol_categorias_microdata($vars['node']);
    $data_category = $categorias['categoria'];
    $micro_data_category = $categorias['micro_categoria'];
    if($data_category != '')
      $vars['microdata_category'] = '<span itemprop="category" content="'.$micro_data_category.'" style="display:none;">'.$data_category.'</span>';
  }
  // Fin cambios para SEO.
	if($es_ficha || (isset($vars['template_files']) && array_intersect(array('page-front', 'page-prehome-autos', 'page-prehome-inmuebles', 'page-search'), $vars['template_files']))) {
		$vars['head'] .= '<meta http-equiv="refresh" content="600">';
	}
  
  //Administrador de sitios
  if(module_exists('administracion_sitios')){
    $vars['es_interna'] = FALSE;
    $vars['es_sitios_principal'] = FALSE;
    $array_admin_sitio = array('page-administrar-sitio-principal','page-administrar-sitio-editor');
    $array_admin_sitio_wizard = array('page-administrar-sitio-plantillas','page-administrar-sitio-fuentes','page-administrar-sitio-multimedia','page-administrar-sitio-contactos','page-administrar-sitio-alta');
    if(in_array('page-sitio', $vars['template_files']) && in_array($vars['title'], array('Quienes somos', 'Contacto'))){
      $vars['es_interna'] = TRUE;
      drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/internas.css', 'module');
    }
    if(isset($vars['template_files']) && is_array($array_admin_sitio) && array_intersect($array_admin_sitio, $vars['template_files'])) {
      $vars['template_files'][] = 'page-micrositios';
    
      if(in_array('page-administrar-sitio-principal', $vars['template_files'])){
        $vars['es_sitios_losandes'] = TRUE;
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/bootstrap.min.css', 'module');
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/jquery.fs.naver.css', 'module');
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/principal.css', 'module');
      }
      if(in_array('page-administrar-sitio-editor', $vars['template_files'])){
        drupal_set_html_head('<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>');
        drupal_set_html_head('<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>');
        $vars['head'] = drupal_get_html_head();
        
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/bootstrap.min.css', 'module');
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/component.css', 'module');
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/configuracion.css', 'module');
 
      }
    }
    if(isset($vars['template_files']) && is_array($array_admin_sitio_wizard) && array_intersect($array_admin_sitio_wizard, $vars['template_files'])) {
      $vars['template_files'][] = 'page-micrositios-wizard';
      if(in_array('page-administrar-sitio-templates', $vars['template_files'])){
        drupal_add_js(drupal_get_path('module', 'administracion_sitios') . '/js/javascripts/templates.js', 'module');
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/templates.css', 'module');
      }
      if(in_array('page-administrar-sitio-contactar', $vars['template_files'])){
        drupal_add_css(drupal_get_path('module', 'administracion_sitios') . '/css/contactos.css', 'module');
      }
    }
  }
  
  //Administrador de avisos
  if(in_array('page-administrar-principal', $vars['template_files']) || in_array('page-administrar-estadisticas_globales', $vars['template_files']) || in_array('page-administrar-estadisticas_globales_autos', $vars['template_files'])){
    drupal_add_js('sites/all/libraries/Chart/Chart.js');
  }
  
  // Notas
  $vars['is_nota'] = FALSE;
  if(isset($vars['node']) && $vars['node']->type == 'nota'){
    $vars['is_nota'] = TRUE;
  }
  
  $css = drupal_add_css();
  if(isset($vars['template_files']) && is_array($array_admin_sitio) && array_intersect($array_admin_sitio, $vars['template_files'])){
    unset($css['all']['theme']['sites/clasificados.losandes.com.ar/themes/losandes/styles/global.css']);
    unset($css['all']['theme']['sites/clasificados.losandes.com.ar/themes/losandes/styles/region-login-superior.css']);
    unset($css['all']['theme']['sites/clasificados.losandes.com.ar/themes/losandes/styles/admin-avisos.css']);
  }
  $vars['css'] = $css; // Necesario para módulos como CDN
  $vars['styles'] = drupal_get_css($css);
    
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function losandes_preprocess_node(&$vars, $hook) {
  // Optionally, run node-type-specific preprocess functions, like
  // losandes_preprocess_node_page() or losandes_preprocess_node_story().
  $nid = (integer) $vars['node']->nid;

  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars, $hook);
  }

  $vars['es_nodo_nuevo_sin_login'] = FALSE;
  $vars['es_nodo_nuevo_con_login'] = FALSE;
  $vars['consulta_enviada'] = FALSE;
  if(sitio_local_es_nodo_aviso($vars['node'])) {
    $vars['template_files'][] = 'node-autoslavoz-aviso-general';
    if(isset($_SESSION['nodo_nuevo_sin_login'][$nid])) {
      $vars['es_nodo_nuevo_sin_login'] = TRUE;
      unset($_SESSION['nodo_nuevo_sin_login'][$nid]);
      if(count($_SESSION['nodo_nuevo_sin_login']) == 0) {
        unset($_SESSION['nodo_nuevo_sin_login']);
      }
    }
    if(isset($_SESSION['nodo_nuevo_con_login'][$nid])) {
      $vars['es_nodo_nuevo_con_login'] = TRUE;
      unset($_SESSION['nodo_nuevo_con_login'][$nid]);
      if(count($_SESSION['nodo_nuevo_con_login']) == 0) {
        unset($_SESSION['nodo_nuevo_con_login']);
      }
    }
    if(isset($_SESSION['consulta_realizada'][$nid])) {
      $vars['consulta_enviada'] = TRUE;
      unset($_SESSION['consulta_realizada'][$nid]);
      if(count($_SESSION['consulta_realizada']) == 0) {
        unset($_SESSION['consulta_realizada']);
      }
    }
  }
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function losandes_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function losandes_preprocess_block(&$vars, $hook) {
  
  if($hook=='block') {
    /* Si mostramos el bloque del formulario de contacto de Argenprop,
     * le cambiamos el id al div del bloque para que tome los mismos estilos
     * del formulario de contacto normal.
     */
    if(isset($vars['block']) && isset($vars['block']->module) && $vars['block']->module=='argenprop_contactar_vendedor' && $vars['block']->delta=='formulario_contacto') {
      $vars['block_html_id']='block-contactar_vendedor-formulario_contacto';
    }
    $clvi_block_classes = variable_get('clvi_block_classes', '');
    if(isset($vars['block']->delta) && array_key_exists($vars['block']->delta, $clvi_block_classes)) {
      $vars['classes_array'][] = $clvi_block_classes[$vars['block']->delta];
    }
    
  }

}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function losandes_breadcrumb($breadcrumb) {
  
  //Eliminar breadcrumb para confirmacion de publicacion y editor de sitios webs
  if(arg(3) == 'confirmar-publicacion' || (arg(1) == 'sitio' && arg(2) == 'editor')){
    return false;
  }
    
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('zen_breadcrumb');
  $output = '';
  global $user;
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {
    $menu_item = menu_get_item();

    if($menu_item && $menu_item['map'][0] == 'node' && is_object($menu_item['map'][1]) && sitio_local_es_nodo_aviso($menu_item['map'][1]->type)) {
      //El breadcrumb para la ficha ya fue armado en el módulo publicacion_avisos
      $breadcrumb = clasificados_microdata_bredacrumb($breadcrumb);
      $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
      return $output;
    }
    //Argenprop
    if(arg(0) == 'argenprop') {
      $breadcrumb = clasificados_microdata_bredacrumb($breadcrumb);
      $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
      return $output;
    }

    //General
    $breadcrumb = array();
    $breadcrumb[0] = l('Clasificados Los Andes', CLASIFICADOS_LA_CLASIFICADOS, array('attributes' => array('title' => 'Clasificados Los Andes')));
    
    //Breadcrumb para el buscador
    if ($menu_item && $menu_item['map'][0] == 'search'){
      if (isset($breadcrumb[2]) && $breadcrumb[2] == 1)
        $breadcrumb[2] = "Resultado de Búsqueda";
      if (isset($breadcrumb[2]) && strip_tags($breadcrumb[2]) == 1){
        $search_url = str_replace(">1<", ">Resultado de Búsqueda<", $breadcrumb[2]);
        $breadcrumb[2] = $search_url;
      }
      foreach ($breadcrumb as $k => $v) {
        //Quitar los RANGOS del breadcrumb
        if(preg_match('/([\[\{](\S+) TO (\S+)[\]\}])/', $v)) {
          unset($breadcrumb[$k]);
        }
      }
    }
    //Breadcrumb para las notas
    if ($menu_item && ! empty($menu_item['page_arguments'][0]) && ($menu_item['page_arguments'][0] == 'notas' || (is_object($menu_item['page_arguments'][0]) && $menu_item['page_arguments'][0]->type == 'nota'))){
      if ($menu_item['page_arguments'][1] == 'page_2')
        $breadcrumb[] = "Guías Autos";
      if ($menu_item['page_arguments'][1] == 'page_1')
        $breadcrumb[] = "Noticias";
      if ($menu_item['page_arguments'][0]->taxonomy[AUTOSLAVOZ_GUIA_TAXONOMY_TID]->tid == AUTOSLAVOZ_GUIA_TAXONOMY_TID)
        $breadcrumb[] = l("Guías Autos", "guias");
      if ($menu_item['page_arguments'][0]->taxonomy[AUTOSLAVOZ_NOTICIA_TAXONOMY_TID]->tid == AUTOSLAVOZ_NOTICIA_TAXONOMY_TID)
        $breadcrumb[] = l("Noticias", "noticias");    }

    //Breadcrumb para FAQ
    if ($menu_item && $menu_item['map'][0] == 'faq'){
      $breadcrumb[] = l(variable_get('faq_title', 'Frequently Asked Questions'), 'faq');
    }
    
    //Breadcrumb para Carga de Avisos
    if ($menu_item && isset($menu_item['map'][2]) && sitio_local_es_nodo_aviso(str_replace("-", "_", $menu_item['map'][2]))){
      unset($breadcrumb[2]);
      $breadcrumb[] = l('Crear Aviso', 'node/add/aviso-auto');
    }

    //Breadcrumb para el administrador del usuario
    if($menu_item) {
      switch($menu_item['page_arguments'][0]) {
        case 'listado_avisos_admin':  $breadcrumb[] = l('Mis Avisos', 'administrar/mis-avisos.html');
                                      break;
        case 'compra_paquete_form':   $breadcrumb[2] = 'Compra de paquetes';
                                      break;
        case 'contactos_recibidos':
        case 'compras':
                                      $breadcrumb[2] = drupal_get_title();
                                      break;
      }
      switch($menu_item['map'][0]) {
        case 'user':                  $breadcrumb[2] = 'Mis Datos';
                                      break;
        case 'dineromail':            $breadcrumb[2] = 'DineroMail';
                                      break;
      }
    }
    
    //Breadcrumb micrositio
    if($menu_item['map'][0] == 'sitio'){
      if(!isset($menu_item['map'][2])){
        $breadcrumb[] = $menu_item['map'][1];
      } else {
        $breadcrumb[] = l($menu_item['map'][1], 'sitio/'.$menu_item['map'][1]);
        if($menu_item['map'][2] == 'quienes-somos'){
          $breadcrumb[] = 'Quienes somos';
        } elseif($menu_item['map'][2] == 'contacto') {
          $breadcrumb[] = 'Contacto';
        }
      }
    }
    
    if($menu_item && $menu_item['map'][0]=='node' && isset($menu_item['map'][2]) && in_array($menu_item['map'][2], array('edit', 'clone', 'eliminar', 'despublicar', 'publicacion'))) {
      unset($breadcrumb[2]);
      unset($breadcrumb[3]);
      unset($breadcrumb[4]);
      if($menu_item['map'][2] != 'publicacion')
        $breadcrumb[] = l('Mis Avisos', 'administrar/mis-avisos.html');
      switch($menu_item['map'][2]) {
        case 'edit':  $breadcrumb[] = 'Editar Aviso';
                      break;
        case 'clone': $breadcrumb[] = 'Copiar Aviso';
                      break;
        case 'eliminar': $breadcrumb[] = 'Eliminar Aviso';
                      break;
        case 'despublicar': $breadcrumb[] = 'Despublicar Aviso';
                      break;
        case 'publicacion': $breadcrumb[] = 'Publicar Aviso';
                      break;
      }
    }

    if(count($breadcrumb) == 1 && isset($menu_item['title'])) {
      $breadcrumb[] = $menu_item['title'];
    }
  }
  
  $breadcrumb = clasificados_microdata_bredacrumb($breadcrumb);
  $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
  return $output;
}

/**
 * Permite mostrar en el breadcrumb el Nombre Comercial de la concesionaria en lugar del Username.
 * Hook provisto por Apache Solr
 *
 * @param integer $userid
 * @return string
 */
function losandes_apachesolr_breadcrumb_uid($userid) {
  if(is_numeric($userid)) {
    $user = user_load($userid);
    if(!$user) {
      return '';
    }
    if(empty($user->profile_nombre_comercial)) {
      if(empty($user->profile_nombre))
        return 'Particular';
      else
        return $user->profile_nombre.' '.$user->profile_apellido;
    } else {
      return $user->profile_nombre_comercial;
    }
  }

  return '';
}

function losandes_tablesort_indicator($style) {
  if ($style == "asc") {
    return theme('image', path_to_theme().'/img/arrow-asc.png', t('sort icon'), t('sort ascending'));
  }
  else {
    return theme('image', path_to_theme().'/img/arrow-desc.png', t('sort icon'), t('sort descending'));
  }
}

/*
 * Cambiamos los textos de ayuda para la carga de imágenes
 */
function losandes_imagefield_widget($element) {
  if(in_array($element['#field_name'], array('field_aviso_fotos', 'field_aviso_planos', 'field_aviso_renders', 'field_aviso_emprendimiento_logo'))) {
    $dimensiones = '628px x 418px';
    if($element['#field_name']=='field_aviso_emprendimiento_logo')
      $dimensiones = '99px x 74px';
    $element['upload']['#description'] = 'Peso máximo de la imagen: <em>'.$element['#upload_validators']['filefield_validate_associate_field'][0]['widget']['max_filesize_per_file'].'</em><br />Tamaño recomendado: <em>'.$dimensiones.'</em><br />Extensiones permitidas: <em>'.$element['#upload_validators']['filefield_validate_associate_field'][0]['widget']['file_extensions'].'</em>';
    $pos = strpos($element['#children'], '<div class="description">');
    if($pos!==FALSE) {
      $pos_fin = strpos($element['#children'], '</div>', $pos);
      if($pos_fin!==FALSE) {
        $temp = substr($element['#children'], 0, $pos);
        $temp .= '<div class="description">'.$element['upload']['#description'];
        $temp .= substr($element['#children'], $pos_fin);
        $element['#children'] = $temp;
      }
    }
  }
  return theme('form_element', $element, $element['#children']);
}

/*
 * Hook swfupload_widget
 * Sobreescribimos el html que se genera para el swfupload.
 */
function losandes_swfupload_widget($element) {

  if(in_array($element['#field_name'], array('field_aviso_fotos', 'field_aviso_planos', 'field_aviso_renders', 'field_aviso_emprendimiento_logo'))) {
    $dimensiones = '628px x 418px';
    if($element['#field_name']=='field_aviso_emprendimiento_logo')
      $dimensiones = '99px x 74px';
    $especificaciones = 'Seleccioná hasta 5 fotos para subirlas todas juntas<br />';
    $especificaciones .= 'Peso máximo de la imagen: <em id="peso_maximo_'.$element['#field_name'].'"></em><br />Tamaño recomendado: <em>'.$dimensiones.'</em><br />Extensiones permitidas: <em id="extensiones_perimitidas_'.$element['#field_name'].'"></em>';
  }
  
  drupal_add_css(drupal_get_path('module', 'swfupload') .'/swfupload.css');

  // Force the classes swfupload_button and disabled to be added to the button
  _form_set_class($element, array('swfupload_button', 'disabled'));
  $element['#attributes']['class'] = str_replace(' error', ' swfupload-error', $element['#attributes']['class']);

  $title = ($element['#title']) ? $element['#title'] : t('Upload new !file', array('!file' => ($element['#max_files'] > 1 ? t('file(s)') : t('file'))));
  
  $output[] = '<div id="'. $element['#id'] .'" '. drupal_attributes($element['#attributes']) .'>';
  $output[] = '  <div class="swfupload-wrapper">';
  $output[] = '    <div id="'. $element['#name'] .'-swfwrapper">&nbsp;</div>';
  $output[] = '  </div>';
  $output[] = '  <div class="left">&nbsp;</div>';
  $output[] = '  <div class="center">'. $title .'</div>';
  $output[] = '  <div class="right">&nbsp;</div>';
  $output[] = '  <div class="especificaciones">'.$especificaciones.'</div>';
  $output[] = '</div>';

  if ($element['#description']) {
    $output[] = '  <div class="description">'. $element['#description'] .'</div>';
  }
  return join("\n", $output);
}

/**
 * Override or insert variables into the search results templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 */
function losandes_preprocess_search_results(&$variables) {
  $env_id = apachesolr_default_environment();
  if(apachesolr_has_searched($env_id))
    $query = apachesolr_current_query($env_id);
  
  $query_filtros = $query->getFilters();
  // Detecatmos que no sea micrositio
  $micrositio = 0;
  foreach($query_filtros as $filtro) {
    if($filtro['#name'] == 'is_uid') $micrositio = 1;
  }
  
  $cant = count($variables['results']);
  
  // SEO los cuatro primeros h2, los otros cuatro h3, el resto h5
  for($i = 0; $i < $cant; $i++){
    if($i < 4) {
      $variables['results'][$i]['h2'] = 1;
    } elseif($i < 8) {
      $variables['results'][$i]['h3'] = 1;
    }
  }
  
  // Agregamos un banner en medio de los resultados de búsqueda
  if($cant > 3) {
    if(!$micrositio) {
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_middle_1';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      $variables['results'][2]['banner_dfp'] = $block->content;
    }
  }
  if($cant > 6) {
    if(!$micrositio) {
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_middle_2';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      $variables['results'][5]['banner_dfp'] = $block->content;
    }
  }
  if($cant > 9) {
    if(!$micrositio) {
      $module = 'clasificados_banners';
      $delta = 'dfp_banner_middle_3';
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;
      $variables['results'][8]['banner_dfp'] = $block->content;
    }
  }
  
  $variables['search_results'] = '';
  foreach ($variables['results'] as $result) {
    $variables['search_results'] .= theme('search_result', $result, $variables['type']);
  }
}

function losandes_facetapi_link_active($variables) {
  $variables['options']['attributes']['rel'] = 'search';
  $variables['options']['attributes']['title'] = 'Resultado de búsqueda sin '.$variables['text'];
  return theme_facetapi_link($variables) . $variables['text'];
}

function losandes_facetapi_link_inactive($variables) {
  $variables['options']['attributes']['rel'] = 'search';
  $variables['options']['attributes']['title'] = 'Resultado de búsqueda de '.$variables['text'];
  $variables['text'] .= ' ' . theme('facetapi_count', $variables);
  // Forzamos filtro ubicacion a que filtre solo el tid que se seleccione y no sus parents
  if(isset($variables['options']['query']['f'])) {
    $filtro_ubicacion = end($variables['options']['query']['f']);
    if(strpos($filtro_ubicacion, 'im_taxonomy_vid_'.AUTOSLAVOZ_PROVINCIA_TAXONOMY_VID) !== FALSE) {
      foreach($variables['options']['query']['f'] as $key => $value) {
        if(strpos($value, 'im_taxonomy_vid_'.AUTOSLAVOZ_PROVINCIA_TAXONOMY_VID) !== FALSE)
          unset($variables['options']['query']['f'][$key]);
      }
      $variables['options']['query']['f'][] = $filtro_ubicacion;
    }
  }
  return theme_facetapi_link($variables);
}

